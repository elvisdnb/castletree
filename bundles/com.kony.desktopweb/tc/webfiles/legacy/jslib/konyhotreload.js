$KW.HotReload = (function() {
    

    var module = {}, formList = [], moduleList = [],
        isDefaultConnection = false, appGroupMap = {};

    function __flushModules() {
        var i;
        for(i = 0; i < moduleList.length; i++) {
            require.undef(moduleList[i]);
        }
    }

    function __reloadFormDefinition() {
        var currentForm = kony.application.getCurrentForm();
        var frmName = currentForm.id;
        var index = formList.indexOf(frmName);
        var friendlyName, controllerName, NavigationObj;

        __flushModules();
        if(index != -1) {
            frmName = appGroupMap[frmName];
            friendlyName = kony.mvc.registry.getFriendlyName(frmName);
            controllerName = kony.mvc.registry.getControllerName(friendlyName);
            kony.application.flushForm(friendlyName);
            moduleList.push(frmName);
            moduleList.push(controllerName);

            require(moduleList, function() {
                $KG.__hotReload = true
                NavigationObj = new kony.mvc.Navigation(friendlyName);
                NavigationObj.navigate();
                kony.web.logger('log', 'modules are reloaded');
            });

            formList.splice(index, 1);
        } else {
            require(moduleList, function () {
                kony.web.logger('log', 'modules are reloaded');
            })
        }
    }


    

    (function() {
        var appjsInterval = setInterval(function() {
            try {
                if(appConfig) {
                    $KW.HotReload.initializeHotReloadConnection();
                    $KW.HotReload.extendFormApis();
                    clearInterval(appjsInterval);
                }
            } catch (e) {
                kony.web.logger("warn", "unable to establish Hot reload preview connection");
            }

        }, 2000);
    }());

    module.initializeHotReloadConnection = function() {
        var connection = appConfig.hotReloadURL;
        var localConnection = 'ws://127.0.0.1:9099';


        window.WebSocket = window.WebSocket || window.MozWebSocket;
        connection = new WebSocket(connection);

        connection.onopen = function() {
            
            kony.web.logger('log', 'HotReload: Socket open');
        };

        connection.onerror = function (error) {
            
            kony.web.logger('warn', 'HotReload: Socket error');
            if(this.close) {
                this.close();
            }
        };

        connection.onmessage = function (message) {
            
            kony.web.logger('log', 'HotReload: Socket message recieved');
            var data = JSON.parse(message.data), frmName = null;
            if(data.eventName == 'HOT_RELOAD') {
                var payLoad = data.message;
                var modifiedForms = payLoad.modifiedForms;
                for(var i = 0; i < modifiedForms.length; i++) {
                    frmName = modifiedForms[i]['name'];
                    formList.push(frmName);
                    appGroupMap[frmName] = modifiedForms[i].formPath
                }

                var requireModules = payLoad.modifiedRequireModules;
                for(i = 0; i < requireModules.length; i++) {
                    moduleList.push(requireModules[i]['name']);
                }

                var globalModules = payLoad.modifiedGlobalModules;
                for(i = 0; i < globalModules.length; i++) {
                    moduleList.push(globalModules[i]['name']);
                }

                __reloadFormDefinition();
                moduleList = []; 
            }
        };

        connection.onclose = function(error) {
            if(isDefaultConnection) {
                localConnection = null;
                alert('HotReload connection is lost. Please check your network connection.')
            }
            if(error.code == 1006 && isDefaultConnection == false) {
                isDefaultConnection = true;
                localConnection = new WebSocket(localConnection);
                localConnection.onopen = connection.onopen;
                localConnection.onerror = connection.onerror;
                localConnection.onmessage = connection.onmessage;
                localConnection.onclose = connection.onclose;
                connection = null;
                kony.web.logger('log', 'HotReload: connection created on local ip');
            }


            kony.web.logger('log', 'HotReload: Socket close');
        };
    };

    module.extendFormApis = function() {
        kony.ui.Form2.prototype.flush = function() {
            this.removeAll();
            this.addWidgetsdone = false;
            this.initdone = false;
            clearInterval(this.scrollerTimer);
        };

        kony.application.flushForm = function(formFriendlyName) {
            var tmpControllerName = null, formModel;
            var formID = formFriendlyName;
            var tmpFormName = kony.mvc.registry.get(formID);
            var formPath = null, userController = null;
            if (null !== tmpFormName) {
                formID = tmpFormName;
            }
            var fileName = formID;
            if (formID in _kony.mvc.viewName2viewId) {
                formID = _kony.mvc.viewName2viewId[formID];
            }
            if (null !== formID) {
                if (formID in _kony.mvc.viewId2ControllerNameMap) {
                    var ctrlName = _kony.mvc.viewId2ControllerNameMap[formID];
                    if (ctrlName in _kony.mvc.ctrlname2ControllerMap) {

                        delete _kony.mvc.ctrlname2ControllerMap[ctrlName];
                        delete _kony.mvc.viewId2ControllerNameMap[formID];
                        if (fileName in _kony.mvc.viewName2viewId) {
                            delete _kony.mvc.viewName2viewId[fileName];
                        }
                    }

                    tmpControllerName = kony.mvc.registry.getControllerName(formFriendlyName);
                    if (null === tmpControllerName) {
                        tmpControllerName = formID + "Controller";
                    }
                    require.undef(tmpControllerName);
                    require.undef(tmpControllerName+"Actions");

                    formPath = appGroupMap[formID];
                    require.undef(formPath);

                    userController = tmpControllerName.replace(formID+'Controller', 'user'+formID+'Controller');
                    require.undef(userController);

                    formModel = kony.application.getCurrentForm();
                    formModel.flush();
                }
            }
        };
    };




    return module;
}());