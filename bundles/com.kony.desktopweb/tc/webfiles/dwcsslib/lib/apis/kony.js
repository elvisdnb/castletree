/* global kony */
/* global console */
/* global _konyAppProperties */
(function() {
    var $K = kony.$kwebfw$;

    var _getProperty = function(group, key) {
        var $K = kony.$kwebfw$, $KU = $K.utils, value = null;

        $KU.log({api:'kony.getProperty', enter:true});

        if(arguments.length === 2) {
            if(typeof _konyAppProperties === 'object' && _konyAppProperties
            && typeof key === 'string' && key) {
                value = _konyAppProperties[key] || null;
            }
        }

        $KU.log({api:'kony.getProperty', exit:true});

        return value;
    };


    //Available on SPA and DesktopWeb
    Object.defineProperty(kony, 'convertToBase64', {configurable:false, enumerable:false, writable:false, value:function(rawbytes) {
        var $K = kony.$kwebfw$, $KU = $K.utils;
        var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        var o1, o2, o3, h1, h2, h3, h4, bits, i = 0, ac = 0, enc='', tmp_arr = [];

        $KU.log({api:'kony.convertToBase64', enter:true});

        if(!rawbytes) {
            enc = rawbytes;
        } else if('btoa' in window) {
            enc = window.btoa(unescape(encodeURIComponent(rawbytes)));
        } else {
            do { //Pack three octets into four hexets
                o1 = rawbytes.charCodeAt(i++) & 0xff;
                o2 = rawbytes.charCodeAt(i++) & 0xff;
                o3 = rawbytes.charCodeAt(i++) & 0xff;

                bits = o1<<16 | o2<<8 | o3;

                h1 = bits>>18 & 0x3f;
                h2 = bits>>12 & 0x3f;
                h3 = bits>>6 & 0x3f;
                h4 = bits & 0x3f;

                tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
            } while(i < rawbytes.length);

            enc = tmp_arr.join('');

            switch(rawbytes.length % 3) {
            case 1: enc = enc.slice(0, -2) + '==';
                break;
            case 2: enc = enc.slice(0, -1) + '=';
                break;
            }
        }

        $KU.log({api:'kony.convertToBase64', exit:true});

        return enc;
    }});


    //Available on SPA but not on except DesktopWeb
    //On SPA, reading base64 from an image src is not supported.
    //But you can read the base64 from an image which is displayed through base64.
    Object.defineProperty(kony, 'convertToRawBytes', {configurable:false, enumerable:false, writable:false, value:function(base64String) {
        var $K = kony.$kwebfw$, $KU = $K.utils;

        $KU.log({api:'kony.convertToRawBytes', enter:true});
        $KU.log({api:'kony.convertToRawBytes', exit:true});

        return null; //Dummy implementation
    }});


    Object.defineProperty(kony, 'getError', {configurable:false, enumerable:false, writable:false, value:function(error) {
        var $K = kony.$kwebfw$, $KU = $K.utils;

        $KU.log({api:'kony.getError', enter:true});
        $KU.log({api:'kony.getError', exit:true});

        return error;
    }});


    Object.defineProperty(kony, 'screenshot', {configurable:false, enumerable:false, writable:false, value:function(config) {
        var $K = kony.$kwebfw$, $KD = $K.dom, $KW = $K.widget, $KA = $K.app,
            $KU = $K.utils, parent = $KA.currentForm, pel = $KD.parent($KW.el(parent, 'node')),
            filereader = new FileReader(), cssscript = '', viewportMain = $KW.el(parent, 'viewport'),
            viewportHeight = viewportMain.scrollHeight, viewportWidth = viewportMain.scrollWidth, i = 0, j = 0,
            data = '', svg = [], tempCSS = document.styleSheets, Script = '', formMain = null, csstext = '';

        if($KU.browser('name') === 'msie' || $KU.browser('name') === 'unknown') {
            return;
        }

        formMain = $KD.first(pel);
        csstext = formMain.style.cssText;
        $KD.setAttr(formMain, 'style', csstext +'height:'+ viewportHeight.toString() + "px !important;" + 'width:' +  viewportWidth.toString() + "px !important;");

        for(i = 0; i < tempCSS.length; i++) {
            if(tempCSS[i].href) {
                for(j = 0; j < tempCSS[i].rules.length; j++) {
                    cssscript = cssscript + tempCSS[i].rules[j].cssText;
                }
            }
        }

        for(i = 0; i < pel.childNodes.length; i++) {
            Script = Script + (new XMLSerializer).serializeToString(pel.childNodes[i]);
        }

        data =  '<svg xmlns="http://www.w3.org/2000/svg" width="' + viewportWidth + '" height="' + viewportHeight + '">' +
                '<foreignObject width="100%" height="100%">' +
                '<div xmlns="http://www.w3.org/1999/xhtml" style="font-size:40px" >' +
                '<style>' +
                cssscript +
                '</style>'+
                Script +
                '</div>' +
                '</foreignObject>' +
                '</svg>';
        svg = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
        filereader.readAsDataURL(svg);
        filereader.onload = function() {
            var img = $KD.create('IMG');
            this.onload = null;
            $KD.setAttr(img, 'crossOrigin', 'anonymous');
            $KD.setAttr(img, 'src', filereader.result);
            $KD.setAttr(img, 'height', viewportHeight);
            $KD.setAttr(img, 'width', viewportWidth);
            $KD.style(img, 'display', 'none');
            $KD.add($KD.body(), img);
            img.onload = function() {
                var canvas = $KD.create('canvas'),
                    ctx = [];
                this.onload = null;
                $KD.setAttr(canvas, 'src', filereader.result);
                $KD.setAttr(canvas, 'height', viewportHeight);
                $KD.setAttr(canvas, 'width', viewportWidth);
                $KD.style(canvas, 'display', 'none');
                $KD.add($KD.body(), canvas);
                ctx = canvas.getContext('2d');
                ctx.imageSmoothingEnabled = true;
                ctx.drawImage(img, 0, 0);
                $KD.remove(img);
                $KD.remove(canvas);
                config.callback && config.callback(canvas.toDataURL().split(",")[1]);
            }
        }

        $KD.setAttr(formMain, 'style', csstext);

    }});


    Object.defineProperty(kony, 'print', {configurable:false, enumerable:false, writable:true, value:function(str) {
        if(console && typeof console.log === 'function'
        && constants.PRINTSTUB !== 'true') {
            if(typeof str === 'string'
            || typeof str === 'number'
            || typeof str === 'boolean'
            || str === null || str === undefined) {
                console.log(str);
            } else if(typeof str === 'object' && str) {
                if(JSON) {
                    console.log(JSON.stringify(str));
                } else if(typeof str.toString === 'function') {
                    console.log(str.toString());
                }
            }
        }
    }});


    Object.defineProperty(kony, 'type', {configurable:false, enumerable:false, writable:false, value:function(variable) {
        var $K = kony.$kwebfw$, $KU = $K.utils, datatype = '';

        $KU.log({api:'kony.type', enter:true});

        if($KU.is(variable, 'string')) {
            datatype = 'string';
        } else if($KU.is(variable, 'number')) {
            datatype = 'number';
        } else if($KU.is(variable, 'function')) {
            datatype = 'function';
        } else if($KU.is(variable, 'null')) {
            datatype = 'null';
        } else if($KU.is(variable, 'widget')) {
            datatype = variable._kwebfw_.ns;
        } else {
            datatype = 'userdata';
        }

        $KU.log({api:'kony.type', exit:true});

        return datatype;
    }});


    $K.defKonyProp(kony.props, [
        {keey:'getProperty', value:_getProperty}
    ]);
}());