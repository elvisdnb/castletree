/* global kony */
(function() {
    var _clean = function(namespace) {
        var $K = kony.$kwebfw$, $KA = $K.app,
            $KU = $K.utils, store = $KU.getLocalStorage();

        if($KU.is(store, 'object') && store.migrated === true) {
            if(arguments[1] === true) {
                store[namespace] = [];
            } else if(store.ns[namespace]
            && store.ns[namespace].length) {
                store.ns[namespace] = [];
            }

            $K.store.put('local', $KA.id, JSON.stringify(store));
        } else {
            $K.store.clear('local'); //For backward compatibility
            $K.store.put('local', $KA.id, JSON.stringify($KU.createBlankLocalStorage()));
        }
    };


    var _delete = function(key, namespace) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KA = $K.app, data = null,
            store = $KU.getLocalStorage(), i = 0, index = -1, len = 0;

        if(arguments[2] === true) {
            data = store[namespace];
        } else {
            data = store.ns[namespace];
        }

        if($KU.is(data, 'array')) {
            len = data.length;

            for(i = 0; i < len; i++) {
                if(data[i].key === key) {
                    index = i;
                    break;
                }
            }
        }

        if(index >= 0) {
            data.splice(index, 1);
            $K.store.put('local', $KA.id, JSON.stringify(store));
        }
    };


    var _fetch = function(key, namespace) {
        var $K = kony.$kwebfw$, $KU = $K.utils,
            store = $KU.getLocalStorage(), i = 0,
            len = 0, item = null, data = null;

        if(arguments[2] === true) {
            data = store[namespace];
        } else {
            data = store.ns[namespace];
        }

        if($KU.is(data, 'array')) {
            len = data.length;

            for(i = 0; i < len; i++) {
                if(data[i].key === key) {
                    item = data[i].value;
                    break;
                }
            }
        }

        return item;
    };


    var _getLength = function(namespace) {
        var $K = kony.$kwebfw$, $KU = $K.utils,
            store = $KU.getLocalStorage(), data = null;

        if(arguments[1] === true) {
            data = store[namespace];
        } else {
            data = store.ns[namespace];
        }

        return ($KU.is(data, 'array')) ? data.length : 0;
    };


    var _keyAt = function(index, namespace) {
        var $K = kony.$kwebfw$, $KU = $K.utils,
            store = $KU.getLocalStorage(), data = null;

        if(arguments[2] === true) {
            data = store[namespace];
        } else {
            data = store.ns[namespace];
        }

        if($KU.is(data, 'array')) data = data[index];

        return(data) ? data.key : null;
    };


    var _put = function(key, value, namespace) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KA = $K.app, data = null,
            store = $KU.getLocalStorage(), i = 0, index = -1, len = 0;

        if(arguments[3] === true) {
            data = store[namespace];
        } else {
            if(!$KU.is(store.ns[namespace], 'array')) {
                store.ns[namespace] = [];
            }

            data = store.ns[namespace];
        }

        if($KU.is(data, 'array')) {
            len = data.length;

            for(i = 0; i < len; i++) {
                if(data[i].key === key) {
                    index = i;
                    break;
                }
            }

            if(index === -1) { //New key
                data.push({
                    key: key,
                    value: value
                });
            } else { //Existing key
                data[index].value = value;
            }

            $K.store.put('local', $KA.id, JSON.stringify(store));
        }
    };


    var _valueAt = function(index, namespace) {
        var $K = kony.$kwebfw$, $KU = $K.utils,
            store = $KU.getLocalStorage(), data = null;

        if(arguments[2] === true) {
            data = store[namespace];
        } else {
            data = store.ns[namespace];
        }

        data = data[index];

        return(data) ? data.value : null;
    };


    Object.defineProperty(kony, 'ds', {configurable:false, enumerable:false, writable:false, value:(function() {
        var _ns = {}, $K = kony.$kwebfw$;


        var _read = function(name, storeContext) {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.ds.read', enter:true});

            if(typeof arguments[1] === 'string' && arguments[1]) {
                $KU.log({api:'kony.ds.read', exit:true});
                return _fetch(name, arguments[1]);
            } else {
                $KU.log({api:'kony.ds.read', exit:true});
                return _fetch(name, 'ds', true);
            }
        };


        var _remove = function(name, storeContext) {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.ds.remove', enter:true});

            if(typeof arguments[1] === 'string' && arguments[1]) {
                _delete(name, arguments[1]);
            } else {
                _delete(name, 'ds', true);
            }

            $KU.log({api:'kony.ds.remove', exit:true});

            return true;
        };


        var _save = function(inputtable, name, metainfo, storeContext) {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.ds.save', enter:true});

            if(typeof metainfo === 'string' && metainfo) {
                _put(name, inputtable, metainfo);
            } else if(arguments.length === 2
            || (typeof metainfo === 'object' && metainfo)) {
                _put(name, inputtable, 'ds', true);
            }

            $KU.log({api:'kony.ds.save', exit:true});
        };


        $K.defKonyProp(_ns, [
            {keey:'read', value:_read},
            {keey:'remove', value:_remove},
            {keey:'save', value:_save}
        ]);


        return _ns;
    }())});


    Object.defineProperty(kony, 'store', {configurable:false, enumerable:false, writable:false, value:(function() {
        var _ns = {}, $K = kony.$kwebfw$;


        var _clear = function() {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.store.clear', enter:true});

            if(typeof arguments[0] === 'string' && arguments[0]) {
                _clean(arguments[0]);
            } else {
                _clean('store', true);
            }

            $KU.log({api:'kony.store.clear', exit:true});
        };


        var _getItem = function(key) {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.store.getItem', enter:true});

            if(typeof arguments[1] === 'string' && arguments[1]) {
                $KU.log({api:'kony.store.getItem', exit:true});
                return _fetch(key, arguments[1]);
            } else {
                $KU.log({api:'kony.store.getItem', exit:true});
                return _fetch(key, 'store', true);
            }
        };


        var _key = function(index) {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.store.key', enter:true});

            if(typeof arguments[1] === 'string' && arguments[1]) {
                $KU.log({api:'kony.store.key', exit:true});
                return _keyAt(index, arguments[1]);
            } else {
                $KU.log({api:'kony.store.key', exit:true});
                return _keyAt(index, 'store', true);
            }
        };


        var _length = function() {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.store.length', enter:true});

            if(typeof arguments[0] === 'string' && arguments[0]) {
                $KU.log({api:'kony.store.length', exit:true});
                return _getLength(arguments[0]);
            } else {
                $KU.log({api:'kony.store.length', exit:true});
                return _getLength('store', true);
            }
        };


        var _removeItem = function(keey) {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.store.removeItem', enter:true});

            if(typeof arguments[1] === 'string' && arguments[1]) {
                _delete(keey, arguments[1]);
            } else {
                _delete(keey, 'store', true);
            }

            $KU.log({api:'kony.store.removeItem', exit:true});
        };


        var _setItem = function(keey, value) {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            $KU.log({api:'kony.store.setItem', enter:true});

            if(typeof arguments[2] === 'string' && arguments[2]) {
                _put(keey, value, arguments[2]);
            } else {
                _put(keey, value, 'store', true);
            }

            $KU.log({api:'kony.store.setItem', exit:true});
        };


        $K.defKonyProp(_ns, [
            {keey:'clear', value:_clear},
            {keey:'getItem', value:_getItem},
            {keey:'key', value:_key},
            {keey:'length', value:_length},
            {keey:'removeItem', value:_removeItem},
            {keey:'setItem', value:_setItem}
        ]);


        return _ns;
    }())});
}());