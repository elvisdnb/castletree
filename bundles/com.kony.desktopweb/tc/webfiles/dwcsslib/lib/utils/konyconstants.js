/* global kony */
Object.defineProperty(kony.$kwebfw$, 'constants', {configurable:false, enumerable:false, writable:true, value:(function() {
    var _ns = {}, $K = kony.$kwebfw$;

    $K.defKonyProp(_ns, [
        {keey:'IMAGE_PATH', value:'images'},
        {keey:'RESOURCES_PATH', value:'resources'},
        {keey:'TRANSLATION_EXT', value:'js'},
        {keey:'TRANSLATION_PATH', value:'strings'}
    ]);

    return _ns;
}())});