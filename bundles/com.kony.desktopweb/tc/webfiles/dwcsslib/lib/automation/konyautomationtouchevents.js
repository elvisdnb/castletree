
(function () {
    var $K = kony.$kwebfw$;
    var widget_getWidgetProperty = function(widgetPath, propertyName) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
            node = null, widgetModel = null;

        $KU.log({api:'kony.automation.widget.getWidgetProperty', enter:true});

        if(arguments.length !== 2) {
            $KAUtils.throwExceptionInsufficientArguments();
        }

        //$KU.logExecutingWithParams('kony.automation.widget.getWidgetProperty', widgetPath, propertyName);
        if(!$KU.is(widgetPath, "array")  || typeof propertyName !== 'string') {
            $KAUtils.throwExceptionInvalidArgumentType();
        }

        widgetModel = $KAUtils.getWidgetModel(widgetPath);
        if(!widgetModel) {
            $KAUtils.throwExceptionWidgetPathNotFound();
        }
        $KU.log({api:'kony.automation.widget.getWidgetProperty', exit:true});

        return widgetModel[propertyName];
    };

    var widget_getProperty = function(widgetPath, propertyName) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
            node = null, widgetModel = null;

        $KU.log({api:'kony.automation.widget.getProperty', enter:true});
        if(arguments.length !== 2) {
            $KAUtils.throwExceptionInsufficientArguments();
        }

       //$KU.logExecutingWithParams('kony.automation.widget.getProperty', widgetPath, propertyName);
        if(!$KU.is(widgetPath, "array") || typeof propertyName !== 'string') {
            $KAUtils.throwExceptionInvalidArgumentType();
        }

        widgetModel = $KAUtils.getWidgetModel(widgetPath);
        if(!widgetModel) {
            $KAUtils.throwExceptionWidgetPathNotFound();
        }

        $KU.log({api:'kony.automation.widget.getProperty', exit:true});
        return widgetModel[propertyName];
    };

    var widget_touch = function(widgetPath, startPoint, movePoint, endPoint) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
            node = null, widgetModel = null, i, containerPath, context  = {};

        if(arguments.length !== 4) {
            $KAUtils.throwExceptionInsufficientArguments();
        }

        if(!$KU.is(widgetPath, "array")
        || (startPoint && (!$KU.is(startPoint, "array")  || startPoint.length !== 2))
        || (movePoint && (!$KU.is(movePoint, "array") || movePoint.length === 0))
        || (endPoint && (!$KU.is(endPoint, "array") || endPoint.length !== 2))) {
            $KAUtils.throwExceptionInvalidArgumentType();
        }

        widgetModel = $KAUtils.getWidgetModel(widgetPath);;
        if(!widgetModel) {
            $KAUtils.throwExceptionWidgetPathNotFound();
        }

        node = widgetModel._kwebfw_.view;
        containerPath = widgetModel._kwebfw_.ii;

        if(containerPath) {
            containerPath = containerPath.split(',');
            context = {
                'sectionIndex' : containerPath[0],
                'rowIndex' : containerPath[1],
                'widgetInfo' : $KW.omodel(widgetModel)
            }
        }

        if(startPoint && widgetModel.onTouchStart) {
            if(containerPath) {
                widgetModel.onTouchStart(widgetModel, startPoint[0], startPoint[1], context);
            } else {
                widgetModel.onTouchStart(widgetModel, startPoint[0], startPoint[1]);
            }

        }

        if(movePoint && widgetModel.onTouchMove) {
            for(var i = 0; i < movePoint.length; i++) {
                var point = movePoint[i];
                if(containerPath) {
                    widgetModel.onTouchMove(widgetModel, point[0], point[1], context);
                } else {
                    widgetModel.onTouchMove(widgetModel, point[0], point[1]);
                }

            }
        }

        if(endPoint && widgetModel.onTouchEnd) {
            if(containerPath) {
                widgetModel.onTouchEnd(widgetModel, endPoint[0], endPoint[1], context);
            } else {
                widgetModel.onTouchEnd(widgetModel, endPoint[0], endPoint[1]);
            }

        }
    };

    var widget_scroll = function() {
        if(arguments.length == 2 || arguments.length == 4) {
            if(arguments.length == 4) {
                _scrollByPoints(arguments[0], arguments[1], arguments[2], arguments[3]);
            } else {
                _scrollByDirection(arguments[0], arguments[1]);
            }
        } else {
            $KAUtils.throwExceptionInsufficientArguments();
        }
    };

    var _scrollByPoints = function(widgetPath, startPoint, movePoint, endPoint)  {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
            node = null, widgetModel = null, i, point;

        if(!$KU.is(widgetPath, "array")
            || (startPoint && (!$KU.is(startPoint, "array") || startPoint.length !== 2))
            || (movePoint && (!$KU.is(movePoint, "array") || movePoint.length === 0))
            || (endPoint && (!$KU.is(endPoint, "array") || endPoint.length !== 2))) {
                $KAUtils.throwExceptionInvalidArgumentType();
        }

        widgetModel = $KAUtils.getWidgetModel(widgetPath);
        if(!widgetModel) {
            $KAUtils.throwExceptionWidgetPathNotFound();
        }

        node = widgetModel._kwebfw_.view;
        if(startPoint) {
            node.scrollTo(startPoint[0], startPoint[1]);
        }

        if(movePoint) {
            for(i = 0; i < movePoint.length; i++) {
                point = movePoint[i];
                node.scrollTo(point[0], point[1]);
            }
        }

        if(endPoint) {
            node.scrollTo(endPoint[0], endPoint[1]);
        }
    };


    var _scrollByDirection = function(widgetPath, direction)  {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
            node = null, widgetModel = null, movement =100, sLeft, sTop;

        if(!$KU.is(widgetPath, "array") || typeof direction !== 'string') {
            $KAUtils.throwExceptionInvalidArgumentType();
        }

        widgetModel = $KAUtils.getWidgetModel(widgetPath);
        if(!widgetModel) {
            $KAUtils.throwExceptionWidgetPathNotFound();
        }

        node = widgetModel._kwebfw_.view;
        sLeft = node.scrollLeft;
        sTop = node.scrollTop;
        movement = 100; //default value is 100dp inherited from native android.

        if(direction == kony.automation.scrollDirection.Bottom) {
            node.scrollTop = sTop + movement;
        } else if(direction == kony.automation.scrollDirection.Top) {
            node.scrollTop = sTop - movement;
        } else if(direction == kony.automation.scrollDirection.Right) {
            node.scrollLeft = sLeft + movement;
        } else if(direction == kony.automation.scrollDirection.Left) {
            node.scrollLeft = sLeft - movement;
        }
    };


    var widget_canScroll = function(widgetPath, direction)  {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
            node = null, widgetModel = null, sTop, sLeft, isScroll = true;

      if(arguments.length !== 2) {
            $KAUtils.throwExceptionInsufficientArguments();
        }

        if(!$KU.is(widgetPath, "array") || typeof direction !== 'string') {
            $KAUtils.throwExceptionInvalidArgumentType();
        }

        widgetModel = $KAUtils.getWidgetModel(widgetPath);
        if(!widgetModel) {
            $KAUtils.throwExceptionWidgetPathNotFound();
        }

        node = widgetModel._kwebfw_.view;
        sTop = node.scrollTop;
        sLeft = node.scrollLeft;

        if(direction == kony.automation.scrollDirection.Bottom) {
            isScroll = node.scrollHeight > (node.clientHeight + node.scrollTop) ? true : false;
        } else if(direction == kony.automation.scrollDirection.Top) {
            isScroll = node.scrollTop > 0 ? true : false;
        } else if(direction == kony.automation.scrollDirection.Right) {
            isScroll = node.scrollWidth > (node.clientWidth + node.scrollLeft) ? true : false;
        } else if(direction == kony.automation.scrollDirection.Left) {
            isScroll = node.scrollLeft > 0 ? true : false;
        }

        return isScroll;
    };

    //All gesture API's in desktop are not supported
    var gesture_tap = function()  {

    };

    var gesture_swipe = function()  {

    };

    var gesture_longpress = function()  {

    };

    var gesture_rightTap = function() {

    };

    var gesture_pan = function() {

    };

    var gesture_rotation = function() {

    };

    var gesture_pinch = function() {

    };

    var playback_wait = function(delaytime)  {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget;
        $KU.log({api:'kony.automation.playback.wait', enter:true});
        if(arguments.length !== 1) {
            $KAUtils.throwExceptionInsufficientArguments();
        }

        //$KU.logExecutingWithParams('kony.automation.playback.wait', delaytime);
        if(typeof delaytime !== 'number') {
            $KAUtils.throwExceptionInvalidArgumentType();
        }

        $KU.log({api:'kony.automation.playback.wait', exit:true});

        return new Promise(function(resolve, reject) {
            setTimeout(function() {
                resolve();
            }, delaytime);
        });
    };

    var playback_waitFor = function(widgetPath, timeOut)  {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget;

        $KU.log({api:'kony.automation.playback.waitFor', enter:true});
        if(arguments.length !== 1 && arguments.length !== 2) { //timeout is optional
            $KAUtils.throwExceptionInsufficientArguments();
        }

        //$KU.logExecutingWithParams('kony.automation.playback.waitFor', widgetPath, timeOut);
        if(!$KU.is(widgetPath, "array") || (timeOut && typeof timeOut !== 'number')) {
            $KAUtils.throwExceptionInvalidArgumentType();
        }

        $KU.log({api:'kony.automation.playback.waitFor', exit:true});

        return new Promise(function(resolve, reject) {
            widgetPath = $KAUtils.getAllowedLeafWidgetPath(widgetPath);
            _waitForElement(widgetPath, timeOut, resolve, reject);
        });
    };

    var _waitForElement = function(widgetPath, timeOut, resolve, reject) {
        var waitTime;

        if(typeof timeOut !== 'undefined' && timeOut !== null) {
            waitTime = (timeOut < 1000) ? timeOut : 1000;
            timeOut = (timeOut < 1000) ? timeOut : (timeOut - 1000);
        } else {
            waitTime = 1000;
        }
        setTimeout(function() {
            var node = null, widgetModel;

            widgetModel = $KAUtils.getWidgetModel(widgetPath);
            if(widgetModel != null) {
                node = widgetModel._kwebfw_.view;
            }
            if(node) {
                resolve(true);
            } else {
                if(typeof timeOut !== 'undefined' && timeOut !== null  && timeOut === 0) {
                    resolve(false);
                } else {
                    _waitForElement(widgetPath, timeOut, resolve, reject);
                }
            }
        }, waitTime);
    };

    var playback_waitForAlert = function()  {
        //Not supported.
    };

    var device_rotate = function(orientation)  {
        //Not supported.
    };

    var device_deviceBack = function()  {
        history.go(-1);
        return kony.automation.playback.wait(100);
    };

    var _capture = function() {
        //Not supported.
    };

    var _scrollToWidget = function(widgetPath) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget;
        var widgetConfig, scrollToWidget;

        if(arguments.length !== 1) {
            $KAUtils.throwExceptionInsufficientArguments();
        }

        if(!$KU.is(widgetPath, "array")) {
            $KAUtils.throwExceptionInvalidArgumentType();
        }

        widgetPath = $KAUtils.getAllowedLeafWidgetPath(widgetPath);
        scrollToWidget = $KAUtils.getWidgetModel(widgetPath);

        if(!scrollToWidget) {
            $KAUtils.throwExceptionWidgetPathNotFound();
        }

        $KAUtils.scrollToWidgetRecursively(scrollToWidget);
        return kony.automation.playback.wait(100);
    };


    $K.defKonyProp(kony.automation, [
        {keey:'widget', value:{}, items:[
            {keey:'getWidgetProperty', value:widget_getWidgetProperty},
            {keey:'getProperty', value:widget_getProperty},
            {keey:'touch', value:widget_touch},
            {keey:'scroll', value:widget_scroll},
            {keey:'canScroll', value:widget_canScroll}

        ]},
        {keey: 'gesture', value: {}, items: [
            {keey:'tap', value: gesture_tap},
            {keey:'swipe', value: gesture_swipe},
            {keey:'longpress', value: gesture_longpress},
            {keey:'rightTap', value: gesture_rightTap},
            {keey:'pan', value: gesture_pan},
            {keey:'rotation', value: gesture_rotation},
            {keey:'pinch', value: gesture_pinch}
        ]},
        {keey: 'playback', value: {}, items: [
            {keey:'wait', value: playback_wait},
            {keey:'waitFor', value: playback_waitFor},
            {keey:'waitForAlert', value: playback_waitForAlert}
        ]},
        {keey: 'device', value: {}, items: [
            {keey:'deviceBack', value: device_deviceBack},
            {keey:'rotate', value: device_rotate}

        ]},
        {keey: 'capture', value: _capture},
        {keey: 'scrollDirection', value: {}, items: [
            {keey:'Top', value: "top"},
            {keey:'Bottom', value: "bottom"},
            {keey:'Left', value: "left"},
            {keey:'Right', value: "right"}
        ]},
        {keey: 'scrollToWidget', value: _scrollToWidget}
    ]);

}());

