/* global kony */
/* global constants */
(function() {
    var $K = kony.$kwebfw$;

    $K.defKonyProp($K.ui, [
        {keey:'DataGrid', value:{}, items:[
            {keey:'onCellKeyDown', value:function(evt) {
                var $K = kony.$kwebfw$, $KD = $K.dom, tr = null, td = null,
                    code = evt.keyCode || evt.which, index = -1;

                if([37, 38, 39, 40].indexOf(code) >= 0) {
                    if(code === 37) { //Left Arrow
                        td = $KD.prev(evt.target);
                    } else if(code === 38) { //Up Arrow
                        index = $KD.index(evt.target);
                        tr = $KD.prev($KD.parent(evt.target));
                        td = (tr) ? $KD.childAt(tr, index) : null;
                    } else if(code === 39) { //Right Arrow
                        td = $KD.next(evt.target);
                    } else if(code === 40) { //Down Arrow
                        index = $KD.index(evt.target);
                        tr = $KD.next($KD.parent(evt.target));
                        td = (tr) ? $KD.childAt(tr, index) : null;
                    }

                    if(td) {
                        $KD.preventDefault(evt);
                        $KD.focus(td);
                    }
                }

                return false;
            }},

            {keey:'onKeyDown', value:function(evt) {
                //TODO::

                return false;
            }},

            {keey:'onRowKeyDown', value:function(evt) {
                var $K = kony.$kwebfw$, $KD = $K.dom, tr = null,
                    code = evt.keyCode || evt.which;

                if([38, 40].indexOf(code) >= 0) {
                    if(code === 38) { //Up Arrow
                        tr = $KD.prev(evt.target);
                    } else if(code === 40) { //Down Arrow
                        tr = $KD.next(evt.target);
                    }

                    if(tr) {
                        $KD.preventDefault(evt);
                        $KD.focus(tr);
                    }
                }

                return false;
            }},

            {keey:'performSelection', value:function(target) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
                    _ = this._kwebfw_, prop = _.prop, tr = null, td = null, th = null,
                    cellId = '', cellIndex = -1, rowIndex = -1, el = null;

                if($KU.is(target, 'dom')) {
                    td = $KD.closest(target, 'tag', 'TD');

                    if(td) {
                        tr = $KD.parent(td);
                        cellIndex = $KD.index(td);
                        rowIndex = $KD.index(tr);

                        if(td && $KD.getAttr(td, 'kr') === 'item') {
                            cellId = prop.columnHeadersConfig[cellIndex].columnID;
                        }

                        if($KU.is(rowIndex, 'integer') && $KU.is(cellIndex, 'integer') && cellId) {
                           _updateItemsAndIndices.call(this, rowIndex, cellIndex, cellId);
                        }

                        if(cellId) {
                            if(prop.selectedIndices.indexOf(rowIndex) >= 0) {
                                if(prop.rowNormalSkin || prop.rowAlternateSkin) {
                                    tr.className = ''; //TODO:: set normal/alternate skin
                                }
                            } else {
                                el = $KW.el(this);

                                if(prop.rowFocusSkin) {
                                    tr.className = prop.rowFocusSkin;
                                }

                                if(el.tbody && !prop.isMultiSelect
                                && $KU.is(prop.selectedIndex, 'integer')) {
                                    tr = $KD.childAt(el.tbody, prop.selectedIndex);

                                    if(prop.rowNormalSkin || prop.rowAlternateSkin) {
                                        tr.className = ''; //TODO:: set normal/alternate skin
                                    } else if(prop.rowFocusSkin) {
                                        tr.className = '';
                                    }
                                }
                            }

                            //TODO:: Call $KU.selectedIndexModifier(prop.selectedIndices);
                            _applyRowsSkin.call(this);
                        }
                        $KW.fire(this, 'onRowSelected', this);
                    } else {
                        th = $KD.closest(target, 'tag', 'TH');
                        if(th) {
                            cellIndex = $KD.index(th);
                            if($KU.is(cellIndex, 'integer') && $KU.is(prop.columnHeadersConfig[cellIndex].columnOnClick, 'function')) {
                                $KW.fire(this, 'columnOnClick', this, {cellIndex:cellIndex});
                            }
                        }
                    }
                }

                return false;
            }}
        ]}
    ]);


    //This function will be called in the scope of widget instance
    var _applyHeaderSkin = function DataGrid$_applyHeaderSkin() {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
            _ = this._kwebfw_, prop = _.prop, el = $KW.el(this);

        if(el.thead) {
            $KU.each($KD.children(el.thead), function(tr) {
                tr.className = prop.headerSkin;
            }, this);
        }
    };

    //This function will be called in the scope of widget instance
    var _applyRowsSkin = function DataGrid$_applyRowsSkin() {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
            _ = this._kwebfw_, prop = _.prop, el = $KW.el(this);

        if(el.tbody) {
            $KU.each($KD.children(el.tbody), function(tr, index) {
                if((index % 2) === 0) {
                    tr.className = prop.rowNormalSkin;
                } else {
                    tr.className = prop.rowAlternateSkin;
                }

                if(prop.rowFocusSkin && prop.selectedIndices
                && prop.selectedIndices.indexOf(index) >= 0) {
                    tr.className = prop.rowFocusSkin;
                }
            }, this);
        }
    };


    //This function will be called in the scope of widget instance
    var _createHeaderCell = function DataGrid$_createHeaderCell(config) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, $KW = $K.widget, tpl = null,
            color = '', clonetemp = null, widgetDataMap = {},
            th = $KD.create('TH', {
                     kr:'item', tabindex:-1
                 }, {
                     width:(config.columnWidthInPercentage+'%')
                 });

        if(this.gridlineColor) {
            color = $KU.convertHexToRGBA(this.gridlineColor);
            $KD.style(th, 'border', ('1px solid '+color));
        }

        if(!config.columnHeaderTemplate) {
            if($KU.is(config.columnHeaderText, 'string')
            || $KU.is(config.columnHeaderText, 'number')) {
                if($KU.is(config.columnHeaderText, 'string')
                && config.columnHeaderText.toLowerCase().indexOf('kony.i18n.getlocalizedstring') !== -1) {
                    $KD.text(th, $KU.getI18Nvalue(config.columnHeaderText));
                } else {
                    $KD.text(th, config.columnHeaderText);
                }
                if($KU.is(config.columnContentAlignment, 'string')) {
                    $KD.addCls(th, '-kony-ca-' + config.columnContentAlignment);
                }
                _updatePadding.call(this, th);
            }
        } else if($KU.is(config.columnHeaderTemplate, 'object')) {
            if($KU.is(config.columnHeaderTemplate.data, 'object')
            && ($KU.is(config.columnHeaderTemplate.template, 'widget', 'FlexContainer')
            || $KU.is(config.columnHeaderTemplate.template, 'string'))
            && config.columnHeaderTemplate.data && config.columnHeaderTemplate.template) {
                tpl = $KW.getTemplate(this, config.columnHeaderTemplate.template);
                $KU.each(config.columnHeaderTemplate.data, function(value, keey) {
                    widgetDataMap[keey] = keey;
                });

                clonetemp = $KW.cloneTemplate(tpl, config.columnHeaderTemplate.data, widgetDataMap);
                tpl = clonetemp._render();
                $KD.style(tpl, {position: 'relative'});
                $KD.add(th, tpl);
            }
        }

        return th;
    };


    //This function will be called in the scope of widget instance
    var _createRow = function DataGrid$_createRow(data) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
            tr = arguments[1] || $KD.create('TR', {tabindex:-1}),
            _ = this._kwebfw_, prop = _.prop;

        //Don't delete this line. $KD.setAttr(tr, 'kwh-keydown', 'onRowKeyDown');

        if(arguments[1]) $KD.html(arguments[1], '');

        $KU.each(prop.columnHeadersConfig, function(config, index) {
            $KD.add(tr, _createRowCell.call(this, config, data));
        }, this);

        return tr;
    };


    //This function will be called in the scope of widget instance
    var _createRowCell = function DataGrid$_createRowCell(config, data) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, $KW = $K.widget, tpl = null,
            image = null, clonetemp = null, widgetDataMap = {},
            td = $KD.create('TD', {
                     kr:'item', tabindex:-1
                 }, {
                     width:(config.columnWidthInPercentage+'%')
                 });

        $KD.setAttr(td, 'kwh-keydown', 'onCellKeyDown');

        if(this.gridlineColor) {
            image = $KU.convertHexToRGBA(this.gridlineColor);
            $KD.style(td, 'border', ('1px solid '+image));
        }

        if(config.columnType === constants.DATAGRID_COLUMN_TYPE_TEXT) {
            if($KU.is(data[config.columnID], 'string')
            || $KU.is(data[config.columnID], 'number')
            || $KU.is(data[config.columnID], 'boolean')) {
                if($KU.is(data[config.columnID], 'string')
                && data[config.columnID].toLowerCase().indexOf('kony.i18n.getlocalizedstring') !== -1) {
                    $KD.text(td, $KU.getI18Nvalue(data[config.columnID]));
                } else {
                    $KD.text(td, data[config.columnID]);
                }
            }
            if($KU.is(config.columnContentAlignment, 'string')) {
                $KD.addCls(td, '-kony-ca-' + config.columnContentAlignment);
            }
            _updatePadding.call(this, td);
        } else if(config.columnType === constants.DATAGRID_COLUMN_TYPE_IMAGE) {
            if($KU.is(data[config.columnID], 'string') && data[config.columnID]) {
                image = $KD.create('IMG', {
                    tabindex:-1, loading:'lazy',
                    onmousedown:'return false;',
                    src:$KU.getImageURL(data[config.columnID])
                });
                $KD.add(td, image);
            }
            if($KU.is(config.columnContentAlignment, 'string')) {
                $KD.addCls(td, '-kony-ca-' + config.columnContentAlignment);
            }
            _updatePadding.call(this, td);
        } else if(config.columnType === constants.DATAGRID_COLUMN_TYPE_TEMPLATE) {
            tpl = $KW.getTemplate(this, config.columnDataTemplate);
            $KU.each(data[config.columnID], function(value, keey) {
                widgetDataMap[keey] = keey;
            });

            clonetemp = $KW.cloneTemplate(tpl, data[config.columnID], widgetDataMap);
            tpl = clonetemp._render();
            $KD.style(tpl, {position: 'relative'});
            $KD.add(td, tpl);
        }

        return td;
    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    var _dependentPropertiesValidationMessage = {};


    //This function will be called in the scope of widget instance
    var _flushClones = function DataGrid$_flushClones(clones) {
        var $K = kony.$kwebfw$, $KU = $K.utils;

        if($KU.is(clones, 'widget')) clones = [clones];

        if($KU.is(clones, 'array')) {
            $KU.each(clones, function(row) {
                //TODO::
            }, this);
        }
    };


    //This function will be called in the scope of widget instance
    var _getColumnIndexById = function DataGrid$_getColumnIndexById(id) {
        var $K = kony.$kwebfw$, $KU = $K.utils, index = -1,
            _ = this._kwebfw_, prop = _.prop;

        $KU.each(prop.columnHeadersConfig, function(config, count) {
            if(config.columnID === id) {
                index = count;
                return true;
            }
        }, this);

        return index;
    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    var _getter = {
        DataGrid: {
            columnHeadersConfig:  function DataGrid$_getter_columnHeadersConfig(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, config = [];

                $KU.each(value, function(v, k) {
                    var $K = kony.$kwebfw$, $KU = $K.utils, item = {};

                    if($KU.is(v.columnHeaderText, 'string')
                    && v.columnHeaderText.toLowerCase().indexOf('kony.i18n.getlocalizedstring') !== -1) {
                        item = $KU.getI18Nvalue(v);
                    } else {
                        item = v;
                    }

                    config.push(item);
                });

                return config;
            },

            data: function DataGrid$_getter_data(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils,
                    _ = this._kwebfw_, prop = _.prop, data = [];

                if(!prop.data) {
                    data = prop.data;
                } else {
                    $KU.each(prop.data, function(val) {
                        var $K  = kony.$kwebfw$, $KU = $K.utils, item = {};

                        $KU.each(val, function(v, k) {
                            var $K  = kony.$kwebfw$, $KU = $K.utils;

                            if($KU.is(v, 'string')
                            && v.toLowerCase().indexOf('kony.i18n.getlocalizedstring') !== -1) {
                                item[k] = $KU.getI18Nvalue(v);
                            } else {
                                item[k] = v;
                            }
                        });
                        data.push(item);
                    });
                }

                return data;
            }
        }
    };


    //All widget file must have this variable
    //This functions will be called in the scope of widget instance
    var _populateUnderscore = {
        DataGrid: function DataGrid$_populateUnderscore() {
            var $K = kony.$kwebfw$, $KU = $K.utils, _ = null;

            if(!$KU.is(this._kwebfw_, 'object')) {
                $KU.defineProperty(this, '_kwebfw_', {}, null);
            }
            _ = this._kwebfw_;

            //NOTE:: Any changes to _ (underscore) may need a change in
            //       _cleanUnderscore function of konyui.js file.
            if(!_.ns) $KU.defineProperty(_, 'ns', 'kony.ui.DataGrid', null);
            if(!_.name) $KU.defineProperty(_, 'name', 'DataGrid', null);
            if(!_.ui) $KU.defineProperty(_, 'ui', {}, null);
            if(!_.templates) $KU.defineProperty(_, 'templates', {}, null);
            $KU.defineProperty(_.ui, 'scroll', {x:0, y:0, width:-1, height:-1, minX:-1, maxX:-1, minY:-1, maxY:-1, status:'ended'}, true);
            if(typeof _.tabIndex !== 'number') {
                $KU.defineProperty(_, 'tabIndex', 0, true);
            }
        }
    };


    //All widget file must have this variable
    //This function will be called in the scope of widget instance
    var _postInitialization = {};


    //All widget file must have this variable
    //This functions will be called in the scope of widget instance
    var _relayoutActiveTriggerer = {
        DataGrid: function DataGrid$_relayoutActiveTriggerer() {
            return ['data'];
        }
    };


    //All widget file must have this variable
    //This functions will be called in the scope of widget instance
    var _relayoutPassiveTriggerer = {
        DataGrid: function DataGrid$_relayoutPassiveTriggerer() {
            return [];
        }
    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    var _setter = {
        DataGrid: {
            data: function DataGrid$_setter_data(old) {
                var prop = this._kwebfw_.prop;

                prop.rowCount = (prop.data) ? prop.data.length : 0;
            }
        }
    };

    //All the functions will be called in the scope of widget instance
    var _updateItemsAndIndices = function DataGrid$_updateItemsAndIndices(rowIndex, cellIndex, cellId) {
        var $K = kony.$kwebfw$, _ = this._kwebfw_, prop = _.prop,
            splicedIndex  = -1, updatedIndex = -1;

        prop.selectedIndex = rowIndex;
        prop.selectedItem = prop.data[rowIndex];
        prop.selectedCellIndex = [cellIndex, cellId];
        prop.selectedCellItem = prop.data[rowIndex][cellId];

        if(prop.isMultiSelect) {
            if(prop.selectedIndices.indexOf(rowIndex) >= 0) {
                splicedIndex = prop.selectedIndices.indexOf(rowIndex);
                prop.selectedIndices.splice(splicedIndex, 1);
                prop.selectedItems.splice(splicedIndex, 1);
                if(prop.selectedIndex === rowIndex) {
                    updatedIndex = prop.selectedIndices[prop.selectedIndices.length - 1];
                    prop.selectedIndex = (updatedIndex >= 0) ? updatedIndex : null;
                    prop.selectedItem = prop.data[updatedIndex] ? prop.data[updatedIndex] : null;
                }
            } else {
                prop.selectedIndices.push(rowIndex);
                prop.selectedItems.push(prop.data[rowIndex]);
            }
        } else {
            prop.selectedIndices = [rowIndex];
            prop.selectedItems = [prop.data[rowIndex]];
        }
    }

    var _updatePadding = function DataGrid$_updatePadding(el) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
            unit = '', padding = '';

        if(this.padding) {
            unit = (this.paddingInPixel ? 'px' : '%');
            padding += (this.padding[1] + unit + ' ' + this.padding[2] + unit + ' '
            + this.padding[3] + unit + ' ' + this.padding[0]+unit);

            $KD.style(el, "padding", padding);
        } else {
            $KD.style(el, "padding", null);
        }
    };

    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    //These function should always return a boolean value
    var _valid = {
        DataGrid: {
            allowColumnResize: function DataGrid$_valid_allowColumnResize(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            columnHeadersConfig: function DataGrid$_valid_columnHeadersConfig(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'array')) {
                    flag = true;

                    $KU.each(value, function(config) {
                        var $K = kony.$kwebfw$, $KU = $K.utils;

                        if(!$KU.is(config, 'object')) {
                            flag = false;
                            return true;
                        } else if(!($KU.is(config.columnID, 'string') && config.columnID
                        && [constants.DATAGRID_COLUMN_TYPE_TEXT,
                            constants.DATAGRID_COLUMN_TYPE_IMAGE,
                            constants.DATAGRID_COLUMN_TYPE_TEMPLATE].indexOf(config.columnType) >= 0
                        && ($KU.is(config.columnHeaderText, 'string')
                        || $KU.is(config.columnHeaderTemplate, 'widget', 'FlexContainer')
                        || ($KU.is(config.columnHeaderTemplate, 'string') && config.columnHeaderTemplate))
                        && $KU.is(config.columnWidthInPercentage, 'number')
                        && config.columnWidthInPercentage >= 0)) {
                            flag = false;
                            return true;
                        } else if(config.columnType === constants.DATAGRID_COLUMN_TYPE_TEMPLATE
                        && (!($KU.is(config.columnDataTemplate, 'widget', 'FlexContainer')
                        || ($KU.is(config.columnDataTemplate, 'string') && config.columnDataTemplate)))) {
                            flag = false;
                            return true;
                        } else if(config.hasOwnProperty('isColumnSortable')
                        && !$KU.is(config.isColumnSortable, 'boolean')) {
                            flag = false;
                            return true;
                        } else if(config.hasOwnProperty('columnOnClick')
                        && !$KU.is(config.columnOnClick, 'function')) {
                            flag = false;
                            return true;
                        } else if(config.hasOwnProperty('columnContentAlignment')
                        && [constants.CONTENT_ALIGN_TOP_LEFT,
                            constants.CONTENT_ALIGN_TOP_CENTER,
                            constants.CONTENT_ALIGN_TOP_RIGHT,
                            constants.CONTENT_ALIGN_MIDDLE_LEFT,
                            constants.CONTENT_ALIGN_CENTER,
                            constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                            constants.CONTENT_ALIGN_BOTTOM_LEFT,
                            constants.CONTENT_ALIGN_BOTTOM_CENTER,
                            constants.CONTENT_ALIGN_BOTTOM_RIGHT
                        ].indexOf(config.columnContentAlignment) === -1) {
                            flag = false;
                            return true;
                        }
                    });
                }

                return flag;
            },

            data: function DataGrid$_valid_data(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'array') || $KU.is(value, 'null')) {
                    flag = true;
                }

                return flag;
            },

            dockingHeader: function DataGrid$_valid_dockingHeader(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            enableScrollBar: function DataGrid$_valid_enableScrollBar(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false, options = [
                    constants.DATAGRID_SCROLLBAR_NONE,
                    constants.DATAGRID_SCROLLBAR_VERTICAL
                ];

                if(options.indexOf(value) >= 0) {
                    flag = true;
                }

                return flag;
            },

            gridlineColor: function DataGrid$_valid_gridlineColor(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'string')) {
                    value = value.toUpperCase();
                    flag = $KU.is(value, 'color');
                }

                return (flag ? [value, flag] : flag);
            },

            headerSkin: function DataGrid$_valid_headerSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            },

            isMultiSelect: function DataGrid$_valid_isMultiSelect(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            onRowSelected: function DataGrid$_valid_onRowSelected(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'function') || $KU.is(value, 'null')) {
                    flag = true;
                } else if(!flag && $K.F.EIWP) {
                    if($KU.is(value, 'undefined')) {
                        flag = [null, true];
                    }
                }

                return flag;
            },

            rowAlternateSkin: function DataGrid$_valid_rowAlternateSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            },

            rowCount: function DataGrid$_valid_rowCount(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'integer') && value >= 0) {
                    flag = true;
                }

                return flag;
            },

            rowFocusSkin: function DataGrid$_valid_rowFocusSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            },

            rowNormalSkin: function DataGrid$_valid_rowNormalSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            },

            selectedCellIndex: function DataGrid$_valid_selectedCellIndex(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'null')) {
                    flag = true;
                } else if($KU.is(value, 'array') && value.length === 2
                && $KU.is(value[0], 'integer') && value[0] >= 0
                && $KU.is(value[1], 'string')) {
                    flag = true;
                }

                return flag;
            },

            selectedCellItem: function DataGrid$_valid_selectedCellItem(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'string')
                || $KU.is(value, 'object')
                || $KU.is(value, 'null')) {
                    flag = true;
                }

                return flag;
            },

            selectedIndex: function DataGrid$_valid_selectedIndex(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'null')) {
                    flag = true;
                } else if($KU.is(value, 'integer') && value >= 0) {
                    flag = true;
                }

                return flag;
            },

            selectedIndices: function DataGrid$_valid_selectedIndices(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'array') || $KU.is(value, 'null')) {
                    flag = true;
                }

                return flag;
            },

            selectedItem: function DataGrid$_valid_selectedItem(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'null') || $KU.is(value, 'object')) {
                    flag = true;
                }

                return flag;
            },

            selectedItems: function DataGrid$_valid_selectedItems(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'array') || $KU.is(value, 'null')) {
                    flag = true;
                }

                return flag;
            },

            showColumnHeaders: function DataGrid$_valid_showColumnHeaders(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            }
        }
    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    //Any property here, which is set to "false", will not create a setter
    var _view = {
        DataGrid: {
            allowColumnResize: false,

            columnHeadersConfig: false,

            data: function DataGrid$_view_data(el, old) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                    _ = this._kwebfw_, prop  =_.prop;

                $KD.html(el.tbody, '');

                $KU.each(prop.data, function(data) {
                    var $K = kony.$kwebfw$, $KD = $K.dom;

                    $KD.add(el.tbody, _createRow.call(this, data));
                }, this);

                _applyRowsSkin.call(this);
            },

            dockingHeader: false,

            enableScrollBar: false,

            gridlineColor: function DataGrid$_view_gridlineColor(el, old) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
                    ths = $KD.find(el.node, 'TH'), tds = $KD.find(el.node, 'TD');

                $KU.each(ths, function(th) {
                    var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, color = '';

                    if(this.gridlineColor) {
                        color = $KU.convertHexToRGBA(this.gridlineColor);
                        $KD.style(th, 'border', ('1px solid '+color));
                    } else {
                        $KD.style(th, 'border', null);
                    }
                }, this);

                $KU.each(tds, function(td) {
                    var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, color = '';

                    if(this.gridlineColor) {
                        color = $KU.convertHexToRGBA(this.gridlineColor);
                        $KD.style(td, 'border', ('1px solid '+color));
                    } else {
                        $KD.style(td, 'border', null);
                    }
                }, this);
            },

            headerSkin: function DataGrid$_view_headerSkin(el, old) {
                _applyHeaderSkin.call(this);

            },

            isMultiSelect: function DataGrid$_view_isMultiSelect(el, old) {
                var $K = kony.$kwebfw$, $KU = $K.utils,
                    $KW = $K.widget, $KD = $K.dom;

                //TODO::
            },

            onRowSelected: true,

            rowAlternateSkin: function DataGrid$_view_rowAlternateSkin(el, old) {
                _applyRowsSkin.call(this);
            },

            rowCount: false,

            rowFocusSkin: function DataGrid$_view_rowFocusSkin(el, old) {
                _applyRowsSkin.call(this);
            },

            rowNormalSkin: function DataGrid$_view_rowNormalSkin(el, old) {
                _applyRowsSkin.call(this);
            },

            selectedCellIndex: false,

            selectedCellItem: false,

            selectedIndex: false,

            selectedIndices: false,

            selectedItem: false,

            selectedItems: false,

            showColumnHeaders: function DataGrid$_view_showColumnHeaders(el, old) {
                var $K = kony.$kwebfw$, $KD = $K.dom;

                if(this.showColumnHeaders) {
                    $KD.removeAttr(el.thead, 'hidden');
                } else {
                    $KD.setAttr(el.thead, 'hidden', true);
                }
            }
        }
    };


    Object.defineProperty(kony.ui, 'DataGrid', {configurable:false, enumerable:false, writable:false, value:(function() {
        var $K = kony.$kwebfw$;


        /**
         * kony.ui.DataGrid constructor.
         *
         * @class
         * @namespace   kony.ui
         * @extends     kony.ui.BasicWidget
         * @author      Goutam Sahu <goutam.sahu@kony.com>
         *
         * @param       {object} bconfig - An object with basic properties.
         * @param       {object} lconfig - An object with layout properties.
         * @param       {object} pspconfig - An object with platform specific properties.
         *
         * @throws      {InvalidArgumentException} - Invalid argument is passed.
         * @throws      {InvalidPropertyException} - Invalid property or invalid value of a property is passed.
         *
         * @classdesc   A brief description about the class.
         *              -
         *              -
         *
         * @todo        Anything that thought for but not yet implemented.
         *              -
         *              -
         */
        var DataGrid = function DataGrid(bconfig, lconfig, pspconfig) {
            var $K = kony.$kwebfw$, $KU = $K.utils, self = this,
                dependentPropertiesValidationMessage = '', prop = null;

            prop = {
                allowColumnResize: false, //Readonly
                columnHeadersConfig: [],
                data: null,
                dockingHeader: false, //Readonly
                enableScrollBar: constants.DATAGRID_SCROLLBAR_NONE, //Readonly
                gridlineColor: '',
                headerSkin: 'slDataGridHead',
                isMultiSelect: false,
                onRowSelected: null,
                rowAlternateSkin: 'slDataGridAltRow',
                rowCount: 0, //Readonly
                rowFocusSkin: 'slDataGridFocusedRow',
                rowNormalSkin: 'slDataGridRow',
                selectedCellIndex: null, //Readonly
                selectedCellItem: null, //Readonly
                selectedIndex: null, //Readonly
                selectedIndices: [], //Readonly
                selectedItem: null, //Readonly
                selectedItems: [], //Readonly
                showColumnHeaders: true
            };

            _populateUnderscore.DataGrid.call(this);

            if(!$KU.is(bconfig, 'object')) bconfig = {};
            if(!$KU.is(bconfig.id, 'string') || !bconfig.id) {
                bconfig.id = (this._kwebfw_.name+$KU.uid());
            }

            DataGrid.base.call(this, bconfig, lconfig, pspconfig);

            if($KU.is(_dependentPropertiesValidationMessage.DataGrid, 'function')) {
                dependentPropertiesValidationMessage = _dependentPropertiesValidationMessage.DataGrid.call(this, prop, bconfig, lconfig, pspconfig);
            }

            if(dependentPropertiesValidationMessage) {
                throw new Error(dependentPropertiesValidationMessage);
            } else {
                //Defaulting to platfom values specific to DataGrid
                $KU.each(prop, function(value, key) {
                    var $K = kony.$kwebfw$, $KU = $K.utils,
                        $KW = $K.widget, valid = false, message = '';

                    if(!bconfig.hasOwnProperty(key)) {
                        bconfig[key] = value;
                    } else if($KW.getNonConstructorProperties(self._kwebfw_.name).indexOf(key) >= 0) {
                        throw new Error('<'+key+'> is a non-constructor property of <'+self._kwebfw_.ns+'> class.');
                    } else if(!$KU.is(_valid.DataGrid[key], 'function')) {
                        throw new Error('<'+key+'> is available in default widget properties of <kony.ui.DataGrid>, but not in <_valid.DataGrid> namespace.');
                    } else {
                        valid = _valid.DataGrid[key].call(self, bconfig[key]);
                        if($KU.is(valid, 'array')) {bconfig[key] = valid[0]; valid = valid[1];}

                        if(valid === false || ($KU.is(valid, 'string') && valid)) {
                            message = ('Invalid value passed to property <'+key+'> of widget <'+self._kwebfw_.ns+'>.');

                            if($KU.is(valid, 'string')) {
                                message += ('\n' + valid);
                            }

                            throw new Error(message);
                        }
                    }
                });

                //Defining Getters/Setters specific to DataGrid
                $KU.each(_view.DataGrid, function(value, key) {
                    var $K = kony.$kwebfw$, $KU = $K.utils;

                    $KU.defineProperty(self._kwebfw_.prop, key, bconfig[key], {configurable:false, enumerable:true, writable:true});

                    $KU.defineGetter(self, key, function DataGrid$_getter() {
                        var $K = kony.$kwebfw$, $KU = $K.utils;

                        if($KU.is(_getter.DataGrid[key], 'function')) {
                            return _getter.DataGrid[key].call(this, this._kwebfw_.prop[key]);
                        } else {
                            return this._kwebfw_.prop[key];
                        }
                    }, true);

                    $KU.defineSetter(self, key, function DataGrid$_setter(val) {
                        var $K = kony.$kwebfw$, $KU = $K.utils, old = null,
                            valid = false, $KW = $K.widget, rmodel = null,
                            final = null, message = '', el = null;

                        if(value === false) {
                            throw new Error('<'+key+'> is a readonly property of <'+this._kwebfw_.ns+'> widget.');
                        } else if(this._kwebfw_.prop[key] !== val) {
                            rmodel = $KW.rmodel(this);

                            if(rmodel && rmodel._kwebfw_.is.template && !rmodel._kwebfw_.is.cloned) {
                                throw new Error('Cannot set any value of a widget, which is either a raw template or any of its widget.');
                            } else {
                                valid = _valid.DataGrid[key].call(this, val);
                                if($KU.is(valid, 'array')) {val = valid[0]; valid = valid[1];}

                                if(valid === false || ($KU.is(valid, 'string') && valid)) {
                                    message = ('Invalid value passed to property <'+key+'> of widget <'+self._kwebfw_.ns+'>.');

                                    if($KU.is(valid, 'string')) {
                                        message += ('\n' + valid);
                                    }

                                    throw new Error(message);
                                } else {
                                    old = this._kwebfw_.prop[key];
                                    this._kwebfw_.prop[key] = val;

                                    if($KU.is(_setter.DataGrid[key], 'function')) {
                                        _setter.DataGrid[key].call(this, old);
                                    }

                                    if(_relayoutActiveTriggerer.DataGrid().indexOf(key) >= 0) {
                                        $KW.markRelayout(this);
                                    }

                                    if(_relayoutPassiveTriggerer.DataGrid().indexOf(key) >= 0) {
                                        final = this._kwebfw_.flex.final;

                                        if(!(final.height && final.width)) {
                                            $KW.markRelayout(this);
                                        }
                                    }

                                    $KW.onPropertyChange(this, key, old);

                                    if($KU.is(value, 'function')) {
                                        el = $KW.el(this);
                                        el.node && value.call(this, el, old);
                                    }
                                }
                            }
                        }
                    }, false);
                });

                if($KU.is(_postInitialization.DataGrid, 'function')) {
                    _postInitialization.DataGrid.call(this);
                }
            }


            pspconfig = lconfig = bconfig = null; //For GC
        };


        $K.utils.inherits(DataGrid, kony.ui.BasicWidget);


        /**
         * Takes care of flushing out the widget reference to clean memory.
         *
         * @access      protected
         * @method      _flush
         * @memberof    kony.ui.DataGrid
         * @author      Goutam Sahu <goutam.sahu@kony.com>
         *
         * @returns     void
         */
        var datagrid__flush = function DataGrid$_flush() {
            var $super = kony.ui.DataGrid.base.prototype;

            _flushClones.call(this, this._kwebfw_.clones);
            $super._flush.call(this);
        };


        /**
         * Builds the view layer for kony.ui.DataGrid widget.
         *
         * @override
         * @access      protected
         * @method      _render
         * @memberof    kony.ui.DataGrid
         * @author      Goutam Sahu <goutam.sahu@kony.com>
         *
         * @returns     {HTMLElement} – DataGrid view.
         */
        var datagrid__render = function DataGrid$_render(tag) {
            var $super = kony.ui.DataGrid.base.prototype, el = null,
                _ = this._kwebfw_, $K = kony.$kwebfw$, $KU = $K.utils,
                $KW = $K.widget, $KD = $K.dom, view = _.view, tr = null,
                docker = null, table = null, thead = null, tbody = null,
                hScroll = null, vScroll = null;

            if(this.isVisible || $K.F.RIVW) {
                if(!$KU.is(_.view, 'dom')) {
                    docker = $KD.create('TABLE', {cellpadding:0, cellspacing:0}, {
                        position:'absolute', top:'0px', left:'0px'
                    }); $KD.setAttr(docker, 'hidden', true);
                    table = $KD.create('TABLE', {cellpadding:0, cellspacing:0});
                    thead = $KD.create('THEAD');
                    tbody = $KD.create('TBODY');
                    tr = $KD.create('TR', {tabindex:-1});

                    $KU.each(_.prop.columnHeadersConfig, function(config) {
                        var $K = kony.$kwebfw$, $KD = $K.dom;

                        $KD.add(tr, _createHeaderCell.call(this, config));
                    }, this);

                    $KD.add(thead, tr);
                    $KD.add(table, thead);
                    $KD.add(table, tbody);

                    view = $super._render.call(this, tag, [table, docker]);
                    $KD.setAttr(view, 'kwh-keydown', 'onKeyDown');

                    if(this.enableScrollBar === constants.DATAGRID_SCROLLBAR_VERTICAL) {
                        if($KU.scrollType() === 'native') {
                            $KD.style(view, {overflowX:'hidden', overflowY:'auto'});
                        } else {
                            hScroll = $KD.create('DIV', {kr:'h-scroll'});
                            vScroll = $KD.create('DIV', {kr:'v-scroll'});
                            $KD.add(view, hScroll); $KD.add(view, vScroll);
                        }
                    } else if($KU.scrollType() === 'native') {
                        $KD.style(view, {overflowX:'hidden', overflowY:'hidden'});
                    }
                }

                el = $KW.el(view);
                _view.DataGrid.headerSkin.call(this, el, this.headerSkin);
                _view.DataGrid.data.call(this, el, _.prop.data);
                _view.DataGrid.showColumnHeaders.call(this, el, _.prop.showColumnHeaders);
                //TODO:: $KW.accessibility(this);
            }

            return view;
        };


        var datagrid_addAll = function DataGrid$addAll(data) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
                el = $KW.el(this), prop = this._kwebfw_.prop;

            if($KU.is(data, 'array')) {
                $KU.each(data, function(row) {
                    var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom;

                    if($KU.is(row, 'object')) {
                        prop.data.push(row);
                        prop.rowCount += 1;
                        el.tbody && $KD.add(el.tbody, _createRow.call(this, row));
                    } else {
                        throw new Error('Invalid row data');
                    }
                }, this);

                _applyRowsSkin.call(this);
            } else {
                throw new Error('Invalid parameter');
            }
        };


        var datagrid_addDataAt = function DataGrid$addDataAt(data, index) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
                $KD = $K.dom, el = $KW.el(this), prop = this._kwebfw_.prop,
                pos = 0;

            if($KU.is(data, 'object') && $KU.is(index, 'integer')
            && index >= 0 && index < prop.data.length) {
                prop.data.splice(index, 0, data);
                prop.rowCount += 1;
                if(prop.selectedIndex >= index || prop.selectedIndices.indexOf(index) >= 0) {
                    prop.selectedIndex += 1;
                    for(pos = 0;pos<prop.selectedIndices.length;pos++) {
                        if(prop.selectedIndices[pos] >= index) {
                            prop.selectedIndices[pos] = prop.selectedIndices[pos] + 1;
                        }
                    }
                }
                el.tbody && $KD.addAt(el.tbody, _createRow.call(this, data), index);
                _applyRowsSkin.call(this);
            } else if ($KU.is(data, 'object') && $KU.is(index, 'integer') && index === prop.data.length) {
                prop.data.push(data);
                prop.rowCount += 1;
                el.tbody && $KD.add(el.tbody, _createRow.call(this, data));
                _applyRowsSkin.call(this);
            } else {
                throw new Error('Invalid parameter');
            }
        };


        var datagrid_applyCellSkin = function DataGrid$applyCellSkin(rowIndex, columnID, skin) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
                prop = this._kwebfw_.prop, tr = null, td = null, columnIndex = -1, el = null;

            if($KU.is(rowIndex, 'integer') && rowIndex >= 0 && rowIndex < prop.data.length
            && (($KU.is(skin, 'string') && skin.split(' ').length === 1) || $KU.is(skin, 'object'))) {
                columnIndex = _getColumnIndexById.call(this, columnID);

                if(columnIndex !== -1) {
                    el = $KW.el(this);

                    if(el.tbody) {
                        tr = $KD.childAt(el.tbody, rowIndex);
                        td = $KD.childAt(tr, columnIndex);

                        td.className = skin;
                    }
                } else {
                    throw new Error('Invalid column ID');
                }
            } else {
                throw new Error('Invalid parameter');
            }
        };


        var datagrid_removeAll = function DataGrid$removeAll() {
            var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                el = $KW.el(this), prop = this._kwebfw_.prop;

            prop.data = [];
            prop.rowCount = 0;
            prop.selectedIndex = null;
            prop.selectedItem = null;
            prop.selectedIndices = [];
            prop.selectedItems = [];
            prop.selectedCellIndex = null;
            prop.selectedCellItem = null;
            el.tbody && $KD.html(el.tbody, '');
        };


        var datagrid_removeAt = function DataGrid$removeAt(index) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
                el = $KW.el(this), prop = this._kwebfw_.prop, pos = null,
                updatedIndex = -1, indicesLength = -1;

            if($KU.is(index, 'number') && index >= 0 && index < prop.data.length) {
                prop.data.splice(index, 1);
                prop.rowCount -= 1;
                if(prop.selectedIndices.indexOf(index) >= 0) {
                    updatedIndex = prop.selectedIndices.indexOf(index);
                    prop.selectedIndices.splice(updatedIndex, 1);
                    prop.selectedItems.splice(updatedIndex, 1);
                }
                for(pos= 0; pos<prop.selectedIndices.length; pos++) {
                    if(prop.selectedIndices[pos] >= index) {
                        prop.selectedIndices[pos] = prop.selectedIndices[pos] - 1;
                    }
                }
                if(prop.selectedIndex === index) {
                    indicesLength = prop.selectedIndices.length - 1;
                    prop.selectedIndex = prop.selectedIndices[indicesLength] >= 0 ? prop.selectedIndices[indicesLength] : null;
                    prop.selectedItem = prop.selectedItems[indicesLength] ? prop.selectedItems[indicesLength] : null;
                    prop.selectedCellIndex = null;
                    prop.selectedCellItem = null;
                } else if(prop.selectedIndex > index) {
                    prop.selectedIndex -= 1;
                }
                el.tbody && $KD.removeAt(el.tbody, index);
                _applyRowsSkin.call(this);
            } else {
                throw new Error('Invalid parameter');
            }
        };


        var datagrid_selectAllRows = function DataGrid$selectAllRows(select) {
            var $K = kony.$kwebfw$, $KU = $K.utils, prop = null,
                $KW = $K.widget, $KD = $K.dom, el = $KW.el(this);

            if($KU.is(select, 'boolean')) {
                if(this.isMultiSelect) {
                    el = $KW.el(this);
                    prop = this._kwebfw_.prop;
                    //prop.selectedCellItem = null;
                    //prop.selectedCellIndex = null;
                    //prop.selectedItem = null;
                    //prop.selectedIndex = null;
                    prop.selectedIndices = [];
                    prop.selectedItems = [];

                    if(select) {
                        $KU.each(prop.data, function(row, index) {
                            prop.selectedItems.push(row);
                            prop.selectedIndices.push(index);
                        }, this);
                    }

                    if(el.tbody && this.rowFocusSkin) {
                        $KU.each($KD.children(el.tbody), function(tr) {
                            var $K = kony.$kwebfw$, $KD = $K.dom;

                            if(select) {
                                tr.className = this.rowFocusSkin;
                            } else if(this.rowNormalSkin || this.rowAlternateSkin) {
                                tr.className = ''; //TODO:: set normal/alternate skin
                            } else {
                                tr.className = '';
                            }
                        }, this);
                    }

                    _applyRowsSkin.call(this);
                }
            } else {
                throw new Error('Invalid parameter');
            }
        };


        var datagrid_setCellDataAt = function DataGrid$setCellDataAt(rowIndex, columnID, data) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
                prop = this._kwebfw_.prop, config = prop.columnHeadersConfig,
                tr = null, td = null, columnIndex = -1, indicesLength = -1, el = null,
                tpl = null, clonetemp = null, widgetDataMap = {};

            if($KU.is(rowIndex, 'integer') && rowIndex >= 0
            && rowIndex < prop.data.length
            && $KU.is(columnID, 'string') && columnID) {
                columnIndex = _getColumnIndexById.call(this, columnID);

                if(columnIndex !== -1) {
                    config = config[columnIndex];
                    prop.data[rowIndex][columnID] = data;
                    if(prop.selectedIndices.indexOf(rowIndex)>=0) {
                        if(prop.selectedIndex === rowIndex && $KU.is(prop.selectedCellIndex, 'equals', [columnIndex, columnID])) {
                            prop.selectedCellItem = null;
                            prop.selectedCellIndex = null;
                        }
                        prop.selectedItems.splice(prop.selectedIndices.indexOf(rowIndex), 1);
                        prop.selectedIndices.splice(prop.selectedIndices.indexOf(rowIndex), 1);
                        indicesLength = prop.selectedIndices.length - 1;
                        prop.selectedIndex = prop.selectedIndices[indicesLength] >= 0 ? prop.selectedIndices[indicesLength] : null;
                        prop.selectedItem = prop.selectedItems[indicesLength] ? prop.selectedItems[indicesLength] : null;
                        _applyRowsSkin.call(this);
                    }

                    if((config.columnType === constants.DATAGRID_COLUMN_TYPE_TEXT && $KU.is(data, 'string'))
                    || (config.columnType === constants.DATAGRID_COLUMN_TYPE_IMAGE && $KU.is(data, 'string') && data)
                    || (config.columnType === constants.DATAGRID_COLUMN_TYPE_TEMPLATE && $KU.is(data, 'object'))) {
                        el = $KW.el(this);

                        if(el.tbody) {
                            tr = $KD.childAt(el.tbody, rowIndex);
                            td = $KD.childAt(tr, columnIndex);

                            if(config.columnType === constants.DATAGRID_COLUMN_TYPE_TEXT) {
                                $KD.text(td, data);
                            } else if(config.columnType === constants.DATAGRID_COLUMN_TYPE_IMAGE) {
                                $KD.setAttr($KD.first(td), 'src', $KU.getImageURL(data));
                            } else if(config.columnType === constants.DATAGRID_COLUMN_TYPE_TEMPLATE) {
                                $KD.html(td, '');
                                tpl = $KW.getTemplate(this, config.columnDataTemplate);
                                $KU.each(data, function(value, keey) {
                                    widgetDataMap[keey] = keey;
                                });

                                clonetemp = $KW.cloneTemplate(tpl, data,widgetDataMap);
                                tpl = clonetemp._render();
                                $KD.style(tpl, {position: 'relative'});
                                $KD.add(td, tpl);
                            }
                        }
                    } else {
                        throw new Error('Invalid data');
                    }
                } else {
                    throw new Error('Invalid column ID');
                }
            } else {
                throw new Error('Invalid parameter');
            }
        };


        var datagrid_setData = function DataGrid$setData(data) {
            var prop = this._kwebfw_.prop;

            this.data = data;
            prop.selectedIndex = null;
            prop.selectedIndices = [];
            prop.selectedItems = [];
            prop.selectedItem = null;
            prop.selectedCellItem = null;
            prop.selectedCellIndex = null;
            _applyRowsSkin.call(this);
        };


        var datagrid_setDataAt = function DataGrid$setDataAt(data, index) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, tr = null,
                $KD = $K.dom, el = $KW.el(this), prop = this._kwebfw_.prop;

            if($KU.is(data, 'object') && $KU.is(index, 'integer')
            && index >= 0 && index < prop.data.length) {
                prop.data[index] = data;
                if(prop.selectedIndices.indexOf(index) >= 0) {
                    prop.selectedItems.splice(prop.selectedIndices.indexOf(index),1);
                    prop.selectedIndices.splice(prop.selectedIndices.indexOf(index),1);
                    if(prop.selectedIndex === index) {
                        prop.selectedItem = null;
                        prop.selectedIndex = null;
                        prop.selectedCellItem = null;
                        prop.selectedCellIndex = null;
                    }
                    _applyRowsSkin.call(this);
                }
                if(el.tbody) {
                    tr = $KD.childAt(el.tbody, index);
                    _createRow.call(this, data, tr);
                }
            } else {
                throw new Error('Invalid parameter');
            }
        };


        var datagrid_setHeaderCellDataAt = function DataGrid$setHeaderCellDataAt(columnID, data) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
                $KD = $K.dom, tr = null, th = null, columnIndex = -1, el = null;

            if($KU.is(columnID, 'string') && columnID
            && ($KU.is(data, 'string') || $KU.is(data, 'number'))) {
                columnIndex = _getColumnIndexById.call(this, columnID);

                if(columnIndex !== -1) {
                    el = $KW.el(this);

                    if(el.thead) {
                        tr = $KD.first(el.thead);
                        th = $KD.childAt(tr, columnIndex);

                        $KD.text(th, data);
                    }
                } else {
                    throw new Error('Invalid column ID');
                }
            } else {
                throw new Error('Invalid parameter');
            }
        };


        $K.defKonyProp(DataGrid.prototype, [
            {keey:'_flush', value:datagrid__flush},
            {keey:'_render', value:datagrid__render},
            {keey:'addAll', value:datagrid_addAll},
            {keey:'addDataAt', value:datagrid_addDataAt},
            {keey:'applyCellSkin', value:datagrid_applyCellSkin},
            {keey:'removeAll', value:datagrid_removeAll},
            {keey:'removeAt', value:datagrid_removeAt},
            {keey:'selectAllRows', value:datagrid_selectAllRows},
            {keey:'setCellDataAt', value:datagrid_setCellDataAt},
            {keey:'setData', value:datagrid_setData},
            {keey:'setDataAt', value:datagrid_setDataAt},
            {keey:'setHeaderCellDataAt', value:datagrid_setHeaderCellDataAt}
        ]);


        return DataGrid;
    }())});
}());