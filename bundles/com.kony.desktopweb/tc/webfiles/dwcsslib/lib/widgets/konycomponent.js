 (function() {
    Object.defineProperty(kony.$kwebfw$, 'ComponentWithContract', {configurable:false, enumerable:false, writable:false, value:(function() {
        var ComponentWithContract = function ComponentWithContract(bconfig, lconfig, pspconfig, template) {
            var $K = kony.$kwebfw$, $KU = $K.utils, proxy = null,
                args = [bconfig, lconfig, pspconfig], component = null;

            proxy = _kony.mvc.initializeMasterController(template, bconfig.id, args);
            proxy._kwebfw_.args = [
                ($KU.clone(bconfig, {function:false}) || {}),
                ($KU.clone(lconfig, {function:false}) || {}),
                ($KU.clone(pspconfig, {function:false}) || {})
            ];
            proxy._kwebfw_.prop.id = bconfig.id;
            proxy._kwebfw_.name = 'ComponentWithContract';

            $KU.each(bconfig, function(value, name) {
                if(name === 'height' && typeof value === 'undefined') {
                    proxy._kwebfw_.prop[name] = '';
                    proxy._kwebfw_.prop.autogrowMode = kony.flex.AUTOGROW_HEIGHT;
                } else if(proxy._kwebfw_.prop.hasOwnProperty(name)) {
                    proxy._kwebfw_.prop[name] = value;
                }
            }, proxy);

            $KU.each(lconfig, function(value, name) {
                if(proxy._kwebfw_.prop.hasOwnProperty(name)) {
                    proxy._kwebfw_.prop[name] = value;
                }
            }, proxy);

            $KU.each(pspconfig, function(value, name) {
                if(proxy._kwebfw_.prop.hasOwnProperty(name)) {
                    proxy._kwebfw_.prop[name] = value;
                }
            }, proxy);

            component = new kony.ui.UserWidget(bconfig, lconfig, pspconfig);

            component._kwebfw_.ns = proxy._kwebfw_.ns = template;
            $KU.defineProperty(component._kwebfw_, 'proxy', proxy, null);
            $KU.defineProperty(proxy._kwebfw_, 'uwi', component, null);
            component._konyControllerName = proxy._konyControllerName;
            _kony.mvc.setMasterContract(component, bconfig.id, template);
            delete component._konyControllerName;

            pspconfig = lconfig = bconfig = null; //For GC

            return component;
        };

        return ComponentWithContract;
    }())});


    Object.defineProperty(kony.$kwebfw$, 'ComponentWithoutContract', {configurable:false, enumerable:false, writable:false, value:(function() {
        var ComponentWithoutContract = function ComponentWithoutContract(bconfig, lconfig, pspconfig, template) {
            var $K = kony.$kwebfw$, $KU = $K.utils, model = null,
                args = [bconfig, lconfig, pspconfig], _ = null;

            model = _kony.mvc.initializeMasterController(template, bconfig.id, args);
            _ = model._kwebfw_;
            _.args = [
                ($KU.clone(bconfig, {function:false}) || {}),
                ($KU.clone(lconfig, {function:false}) || {}),
                ($KU.clone(pspconfig, {function:false}) || {})
            ];
            _.prop.id = bconfig.id;
            _.ns = template;
            _.name = 'ComponentWithoutContract';

            $KU.each(bconfig, function(value, name) {
                if(name === 'height' && typeof value === 'undefined') {
                    model._kwebfw_.prop[name] = '';
                    model._kwebfw_.prop.autogrowMode = kony.flex.AUTOGROW_HEIGHT;
                } else {
                    model._kwebfw_.prop[name] = value;
                }
            }, model);

            $KU.each(lconfig, function(value, name) {
                model._kwebfw_.prop[name] = value;
            }, model);

            $KU.each(pspconfig, function(value, name) {
                model._kwebfw_.prop[name] = value;
            }, model);

            _kony.mvc.setMasterContract(model, model.id, template);

            pspconfig = lconfig = bconfig = null; //For GC

            return model;
        };

        return ComponentWithoutContract;
    }())});
}());