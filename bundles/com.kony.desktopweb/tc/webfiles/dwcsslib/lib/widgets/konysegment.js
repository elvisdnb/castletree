/* global kony */
/* global constants */
(function() {
    var $K = kony.$kwebfw$, _extraHeightToRender = 200;


    $K.defKonyProp($K.ui, [
        {keey:'SegmentedUI2', value:{}, items:[
            {keey:'onKeyDown', value:function(evt) {
                var $K = kony.$kwebfw$, $KD = $K.dom,
                    model = this._kwebfw_.rows[0],
                    code = evt.keyCode || evt.which;

                if(model && model._kwebfw_.view) {
                    if([40].indexOf(code) >= 0) {
                        $KD.preventDefault(evt);

                        if(code === 40) { //Down Arrow
                            $KD.focus($KD.parent(model._kwebfw_.view));
                        }
                    }
                }

                return false;
            }},

            {keey:'doLayout', value:function(frame) {
                var $K = kony.$kwebfw$, $KW = $K.widget,
                    prop = this._kwebfw_.prop, el = $KW.el(this);

                if(prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                    _view.SegmentedUI2.data.call(this, el, prop.data);
                }
            }},

            {keey:'onRowClick', value:function(evt) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                    index = null, secIndex = -1, rowIndex = -1;

                index = $KD.closest(evt.target, 'kr', 'item');

                if(index) {
                    index = $KD.getAttr(index, 'kii').split(',');
                    secIndex = parseInt(index[0], 10);
                    rowIndex = parseInt(index[1], 10);

                    $K.apm.send(this, 'Touch', {type:(this._kwebfw_.name+'_Row_Click')});
                    $KW.fire(this, 'onRowClick', this, {secIndex:secIndex, rowIndex:rowIndex});
                }

                return false;
            }},

            {keey:'onRowKeyDown', value:function(evt) {
                var $K = kony.$kwebfw$, $KD = $K.dom, li = null,
                    code = evt.keyCode || evt.which;

                if([38, 40].indexOf(code) >= 0) {
                    if(code === 38) { //Up Arrow
                        li = $KD.prev(evt.target);
                    } else if(code === 40) { //Down Arrow
                        li = $KD.next(evt.target);
                    }

                    if(li) {
                        $KD.preventDefault(evt);
                        $KD.focus(li);
                    }
                }

                return false;
            }},

            {keey:'onRowKeyUp', value:function(evt) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                    code = evt.keyCode || evt.which, secIndex = -1, rowIndex = -1;

                if([13, 32].indexOf(code) >= 0) {
                    $KD.preventDefault(evt);

                    if(code === 13 || code === 32) { //Enter or Space
                        rowIndex = $KD.getAttr(evt.target, 'kii').split(',');
                        secIndex = parseInt(rowIndex[0], 10);
                        rowIndex = parseInt(rowIndex[1], 10);

                        $KW.fire(this, 'onRowClick', this, {secIndex:secIndex, rowIndex:rowIndex});
                    }
                }

                return false;
            }}
        ]}
    ]);


    //All the functions will be called in the scope of widget instance
    //Segment APIs actions - add, update and remove will be called
    var _action = {
        SegmentedUI2: {
            _remove: function SegmentedUI2$_action_remove(secIndex, rowIndex) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                    el = $KW.el(this), _ = this._kwebfw_, $KU = $K.utils,
                    data = _.prop.data, section = _isSectionDS(data[0]),
                    absIndex = _absoluteRowIndex.call(this, secIndex, rowIndex),
                    isRowRendered = _isRowRendered.call(this, secIndex, rowIndex),
                    removeSelectedIndex = -1, rowNode = null, rowToDelete = null;

                if(section) data[secIndex][1].splice(rowIndex, 1);
                else data.splice(rowIndex, 1);

                if(_.selectedRows.length > 0) {
                    $KU.each(_.selectedRows, function(row, index) {
                        if(row[0] === secIndex && row[1] > rowIndex) {
                            row[1] = row[1] - 1;
                        } else if(row[0] === secIndex && row[1] === rowIndex) {
                            removeSelectedIndex = index;
                        }
                    });
                    if(removeSelectedIndex !== -1) {
                        _.selectedRows.splice(removeSelectedIndex, 1);
                    }
                    _setSelectedRowsRelatedProperties.call(this);
                }

                if(el.node) {
                    if(section) {
                        _flushClones(_.clones[secIndex][1][rowIndex]);
                        _.clones[secIndex][1].splice(rowIndex, 1);
                    }
                    else {
                        _flushClones(_.clones[rowIndex]);
                        _.clones.splice(rowIndex, 1);
                    }

                    if(isRowRendered) {
                        _.rows.splice(absIndex, 1);
                        $KD.removeAt(el.scrolee, absIndex);
                    }
                    if(section) _searcher.SegmentedUI2.removeAtHeaderVisibility.call(this, secIndex);
                    _updateIndexes.call(this, 0, -1);
                }
                if(_.swipeContext) {
                    if(_.swipeContext.currentPage === 0){
                        _.swipeContext = null;
                    } else if(rowIndex <= _.swipeContext.currentPage) {
                        _.swipeContext.currentPage = _.swipeContext.currentPage - 1;
                    } else {
                        _.swipeContext.currentPage = _.swipeContext.currentPage + 1;
                    }
                }
            },

            _removeAll: function SegmentedUI2$_action_removeAll() {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                    $KU = $K.utils, el = null, _ = this._kwebfw_;

                _.prop.data = [];
                _.rows = [];
                _flushClones(_.clones);
                _.clones = [];
                _clearSelectedIndices.call(this);
                $KW.markRelayout(this);

                if($KU.is(_.view, 'dom')) {
                    el = $KW.el(this);
                    $KD.html(el.scrolee, '');

                    if(_.prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                        _setPageView.call(this, el);
                    }
                }
            },

            _removeSectionAt: function SegmentedUI2$_action_removeSectionAt(secIndex, rowIndex, updateIndicesFlag) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                    $KU =$K.utils, _ = this._kwebfw_, data = _.prop.data,
                    clones = null, el = $KW.el(this), removeSelectedSectionIndices = [],
                    absIndex = _absoluteRowIndex.call(this, secIndex, rowIndex);

                if(!$KU.is(updateIndicesFlag, 'boolean')) updateIndicesFlag = true;
                data.splice(secIndex, 1);

                if(updateIndicesFlag && _.selectedRows.length > 0) {
                    $KU.each(_.selectedRows, function(row, index) {
                        if(row[0] > secIndex) {
                            row[0] = row[0] - 1;
                        } else if(row[0] === secIndex) {
                            removeSelectedSectionIndices.push(index);
                        }
                    });
                    $KU.each(removeSelectedSectionIndices, function(index) {
                        _.selectedRows.splice(index, 1);
                    });
                    _setSelectedRowsRelatedProperties.call(this);
                }

                if(el.node) {
                    clones = _.clones[secIndex];
                    //header node removal of given section index
                    if(clones[0] && clones[0].isVisible) {
                        $KD.removeAt(el.scrolee, absIndex);
                        _.rows.splice(absIndex, 1);
                    }

                    //row nodes removal of given section index
                    $KU.each(clones[1], function(clone) {
                        if(clone && clone.isVisible) {
                            $KD.removeAt(el.scrolee, absIndex);
                            _.rows.splice(absIndex, 1);
                        }
                    });

                    _flushClones(_.clones[secIndex]);
                    _.clones.splice(secIndex, 1);
                    if(updateIndicesFlag) _updateIndexes.call(this, 0, -1);
                }
                if(_.swipeContext) _.swipeContext = null;
            },

            add: function SegmentedUI2$_action_add(secIndex, rowIndex, widgetdata, anim) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom, $KU = $K.utils,
                    clone = null, el = $KW.el(this), _ = this._kwebfw_,
                    data = _.prop.data, section = _isSectionDS(data[0]),
                    absIndex = _absoluteRowIndex.call(this, secIndex, rowIndex),
                    rowNode = null;

                if(section) data[secIndex][1].splice(rowIndex, 0, widgetdata);
                else data.splice(rowIndex, 0, widgetdata);

                if(_.selectedRows.length > 0) {
                    $KU.each(_.selectedRows, function(row) {
                        if(row[1] >= rowIndex) {
                            row[1] = row[1] + 1;
                        }
                    });

                    _setSelectedRowsRelatedProperties.call(this);
                }

                if(el.node) {
                    if(section) _.clones[secIndex][1].splice(rowIndex, 0, null);
                    else _.clones.splice(rowIndex, 0, null);

                    clone = _getClonedTemplate.call(this, [secIndex, rowIndex]);

                    if(clone) {
                        rowNode = _renderRows.call(this, [clone]);
                        if(absIndex === _.rows.length) {
                            _.rows.push(clone);
                            $KD.add(el.scrolee, rowNode);
                        } else {
                            _.rows.splice(absIndex, 0, clone);
                            $KD.addAt(el.scrolee, rowNode, absIndex);
                        }

                        if(secIndex === -1) secIndex = 0;
                        _searcher.SegmentedUI2.updateSearchText.call(this, [clone], section);

                        if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                            _animator.SegmentedUI2.onRowDisplayHandler.call(this, kony.segment.ADD, [clone]);
                            _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'adddataat', rowNode, rowIndex, secIndex, anim);
                        }
                    }
                    _updateIndexes.call(this, 0, -1);
                }
            },

            update: function SegmentedUI2$_action_update(secIndex, rowIndex, widgetdata, anim) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
                    clone = null, el = $KW.el(this), _ = this._kwebfw_, rowNode = null,
                    data = _.prop.data, section = _isSectionDS(data[0]), tplId = null,
                    absIndex = _absoluteRowIndex.call(this, secIndex, rowIndex),
                    isRowRendered = _isRowRendered.call(this, secIndex, rowIndex);


                if(section) data[secIndex][1][rowIndex] = widgetdata;
                else data[rowIndex] = widgetdata;

                if(_.selectedRows.length > 0) {
                    $KU.each(_.selectedRows, function(row, index) {
                        if(row[0] === secIndex && row[1] === rowIndex) {
                            _.selectedRows.splice(index, 1);
                            return true;
                        }
                    });
                    _setSelectedRowsRelatedProperties.call(this);
                }

                if(el.node) {
                    tplId = _deduceTemplate.call(this, [secIndex, rowIndex]);
                    if($KU.is(tplId, 'widget')) tplId = tplId.id;

                    if(section && _.clones[secIndex][1][rowIndex].id !== tplId) {
                        _flushClones(_.clones[secIndex][1][rowIndex]);
                        _.clones[secIndex][1][rowIndex] = null;
                    } else if(!section && _.clones[rowIndex].id !== tplId) {
                        _flushClones(_.clones[rowIndex]);
                        _.clones[rowIndex] = null;
                    }

                    clone = _getClonedTemplate.call(this, [secIndex, rowIndex]);

                    if(clone) {
                        rowNode = _renderRows.call(this, [clone])
                        if(isRowRendered) {
                            _.rows[absIndex] = clone;
                            $KD.replace(rowNode, $KD.childAt(el.scrolee, absIndex));
                        } else {
                            if(absIndex === _.rows.length) {
                                _.rows.push(clone);
                                $KD.add(el.scrolee, rowNode);
                            } else {
                                _.rows.splice(absIndex, 0, clone);
                                $KD.addAt(el.scrolee, rowNode, absIndex);
                            }
                        }

                        if(secIndex === -1) secIndex = 0;
                        _searcher.SegmentedUI2.updateSearchText.call(this, [clone], section);

                        if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                            _animator.SegmentedUI2.onRowDisplayHandler.call(this, kony.segment.UPDATE, [clone]);
                            _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'setdataat', rowNode, rowIndex, secIndex, anim);
                        }
                    }

                }
            },

            remove: function SegmentedUI2$_action_remove(secIndex, rowIndex, anim) {
                var $K = kony.$kwebfw$, $KD = $K.dom,
                    _ = this._kwebfw_, absIndex = null, rowNode = null;
                if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                    absIndex = _absoluteRowIndex.call(this, secIndex, rowIndex);
                    rowNode = $KD.parent(_.rows[absIndex]._kwebfw_.view);
                    _animator.SegmentedUI2.onRowDisplayHandler.call(this, kony.segment.REMOVE, [_.rows[absIndex]]);
                    _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'removeat', rowNode, rowIndex, secIndex, anim);
                } else {
                    _action.SegmentedUI2._remove.call(this, secIndex, rowIndex);
                }
            },

            addall: function SegmentedUI2$_action_addall(secIndex, rowIndex, newdata, anim) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                    clones = null, el = $KW.el(this), _ = this._kwebfw_,
                    data = _.prop.data, rows = null, prevLength;

                data.push.apply(data, newdata);

                if(el.node) {
                    clones = _getRenderableClones.call(this, [secIndex, rowIndex]);
                    if(clones.length > 0) {
                        prevLength = $KD.children(el.scrolee).length;
                        $KD.add(el.scrolee, _renderRows.call(this, clones));
                        _.rows.push.apply(_.rows, clones);

                        if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                            rows = [].slice.call($KD.children(el.scrolee), prevLength);
                            _animator.SegmentedUI2.onRowDisplayHandler.call(this, kony.segment.ADD, clones);
                            _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'addall', rows, -1, -1, anim);
                        }
                    }
                    _searcher.SegmentedUI2.updateSearchText.call(this, clones, _isSectionDS(data[0]));
                    //_updateIndexes.call(this, 0, -1);
                }
            },

            addsectionat: function SegmentedUI2$_action_addsectionat(secIndex, rowIndex, newdata, updateIndicesFlag, anim) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom, $KU = $K.utils,
                    clones = null, el = $KW.el(this), _ = this._kwebfw_, data = _.prop.data,
                    absIndex = _absoluteRowIndex.call(this, secIndex, rowIndex),
                    prevLength, rows = [], row = null;

                if(!$KU.is(updateIndicesFlag, 'boolean')) updateIndicesFlag = true;

                data.splice(secIndex, 0, newdata);

                if(updateIndicesFlag  && _.selectedRows.length > 0) {
                    $KU.each(_.selectedRows, function(row) {
                        if(row[0] >= secIndex) {
                            row[0] = row[0] + 1;
                        }
                    });

                    _setSelectedRowsRelatedProperties.call(this);
                }

                if(el.node) {
                    _.clones.splice(secIndex, 0, null);
                    clones = _getRenderableClones.call(this, [secIndex, rowIndex], [secIndex, newdata.length]);

                    if(absIndex === _.rows.length) {
                        if(clones.length > 0) {
                            prevLength = $KD.children(el.scrolee).length;
                            $KD.add(el.scrolee, _renderRows.call(this, clones));
                            _.rows.splice.apply(_.rows, [absIndex, 0].concat(clones));
                            if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                                rows = [].slice.call($KD.children(el.scrolee), prevLength);
                                _animator.SegmentedUI2.onRowDisplayHandler.call(this, updateIndicesFlag ? kony.segment.ADD: kony.segment.UPDATE, clones);
                                _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'addsectionat', rows, -1, -1, anim);
                            }
                        }
                    } else {
                        _.rows.splice.apply(_.rows, [absIndex, 0].concat(clones));
                        $KU.each(clones, function(clone, ii) {
                            row = _renderRows.call(this, [clone]);
                            rows.push(row);
                            $KD.addAt(el.scrolee, row, absIndex++);
                        }, this);
                        if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                            _animator.SegmentedUI2.onRowDisplayHandler.call(this, updateIndicesFlag ? kony.segment.ADD: kony.segment.UPDATE, clones);
                            _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'addsectionat', rows, -1, -1, anim);
                        }
                    }

                    _searcher.SegmentedUI2.updateSearchText.call(this, clones, true);

                    if(updateIndicesFlag) _updateIndexes.call(this, 0, -1);
                }
            },

            setsectionat: function SegmentedUI2$_action_setsectionat(secIndex, rowIndex, newdata, anim) {
                var $K = kony.$kwebfw$, $KW = $K.widget, el = $KW.el(this),
                    _ = this._kwebfw_;

                _action.SegmentedUI2.removesectionat.call(this, secIndex, rowIndex, false);
                _action.SegmentedUI2.addsectionat.call(this, secIndex, rowIndex, newdata, false, anim);

                if(_.selectedRows.length > 0) {
                    _.selectedRows.splice(0, _.selectedRows.length);
                    _setSelectedRowsRelatedProperties.call(this);
                }

                if(el.node) _updateIndexes.call(this, 0, -1);
            },

            removesectionat: function SegmentedUI2$_action_removesectionat(secIndex, rowIndex, updateIndicesFlag, anim) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                        $KU =$K.utils, _ = this._kwebfw_, clones = null, counter = 0,
                        el = $KW.el(this), absIndex = null,row, rows = [], cloneModels = [];

                if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                    absIndex = _absoluteRowIndex.call(this, secIndex, rowIndex);
                        if(el.node) {
                            clones = _.clones[secIndex];
                            //header node removal of given section index
                            if(clones[0] && clones[0].isVisible) {
                                rows.push($KD.childAt(el.scrolee, absIndex));
                                counter ++;
                            }

                            //row nodes removal of given section index
                            $KU.each(clones[1], function(clone) {
                                if(clone && clone.isVisible) {
                                    rows.push($KD.childAt(el.scrolee, absIndex + counter));
                                    cloneModels.push(clone);
                                    counter ++;
                                }
                            });
                        }
                        _animator.SegmentedUI2.onRowDisplayHandler.call(this, kony.segment.REMOVE, cloneModels);
                        _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'removesectionat', rows, rowIndex, secIndex, anim);
                } else {
                   _action.SegmentedUI2._removeSectionAt.call(this, secIndex, rowIndex, updateIndicesFlag);
                }
            }
        }
    };


    //All the functions will be called in the scope of widget instance
    var _animator = {
       SegmentedUI2: {
            applyRowsAnimationByAPI: function SegmentedUI2$_animator_applyRowsAnimationByAPI(action, listItems, rowIndex, secIndex, animObj) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, $KD = $K.dom,
                    animDef = null, el = $KW.el(this), segmodel = this, row = null,
                    rows = [],
                    _wrapRemoveAtCallback = function(callback) {
                        var wrapper = function() {
                            _action.SegmentedUI2._remove.call(segmodel, secIndex, rowIndex);
                            callback && callback.apply(this, arguments);
                        }
                        return wrapper;
                    },
                    _wrapRemoveAllCallback = function(callback) {
                        var wrapper = function() {
                            _action.SegmentedUI2._removeAll.call(segmodel);
                            callback && callback.apply(this, arguments);
                        }
                        return wrapper;
                    },
                    _wrapRemoveSectionAtCallback = function(callback) {
                        var wrapper = function() {
                            _action.SegmentedUI2._removeSectionAt.call(segmodel, secIndex, rowIndex, true);
                            callback && callback.apply(this, arguments);
                        }
                        return wrapper;
                    };

                animDef = animObj.definition;

                if( action === 'removeat') {
                    if(!animObj.callbacks) animObj.callbacks = {};
                    animObj.callbacks.animationEnd = _wrapRemoveAtCallback(animObj.callbacks.animationEnd);
                }

                if(action === 'removeall') {
                    if(!animObj.callbacks) animObj.callbacks = {};
                    animObj.callbacks.animationEnd = _wrapRemoveAllCallback(animObj.callbacks.animationEnd);
                }

                if(action === 'removesectionat') {
                    if(!animObj.callbacks) animObj.callbacks = {};
                    animObj.callbacks.animationEnd = _wrapRemoveSectionAtCallback(animObj.callbacks.animationEnd);
                }

                switch(action) {
                    case "adddataat":
                    case "setdataat":
                    case "removeat":
                        row = listItems.firstChild;
                        animDef.applyRowAnimation([row], animObj.config, animObj.callbacks);
                        break;

                    case "addall":
                    case "addsectionat":
                    case "removesectionat":
                        $KU.each(listItems, function(row) {
                            rows.push(row.firstChild);
                        });
                        animDef.applyRowAnimation(rows, animObj.config, animObj.callbacks);
                        break;

                    case "setdata":
                    case "removeall":
                        $KU.each(listItems, function(row) {
                            rows.push(row._kwebfw_.view);
                        });
                        animDef.applyRowAnimation(rows, animObj.config, animObj.callbacks);
                        break;
                }
            },

            animateRows: function SegmentedUI2$_animator_animateRows(animContext) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, animInfo = {},
                    el = $KW.el(this), rows = [], widgetId, widgets = [], elsToAnimate = [];

                if($KU.is(el.node, 'null')) {
                    return;
                }

                if(animContext) {
                    rows = animContext.context || animContext.rows;
                    animInfo = animContext.animation;
                    if(!rows || !$KU.is(rows, 'array') || !animInfo || !animInfo.definition) {
                        return;
                    }

                    widgetId = animContext.widgetID;
                    widgets = animContext.widgets || [];

                    if(widgetId) widgets.push(widgetId);

                    $KU.each(rows, function(rowContext, key) {
                        var absIndex = 0, model = null, templateModel = null,
                            rowAnimConfig = {}, el = null;

                        absIndex = _absoluteRowIndex.call(this, rowContext.sectionIndex, rowContext.rowIndex);
                        templateModel = this._kwebfw_.rows[absIndex];
                        if(templateModel._kwebfw_.view) {//template visible false case
                            if(widgets.length) {
                                $KU.each(widgets, function(widgetId, key) {
                                    model = $KU.get(widgetId, templateModel);
                                    el = model._kwebfw_.view;
                                    elsToAnimate.push(el);
                                });
                            } else {
                                model = templateModel;
                                el = model._kwebfw_.view;
                                elsToAnimate.push(el);
                            }
                        }
                    }, this);
                }
                animInfo.definition.applyRowAnimation(elsToAnimate, animInfo.config, animInfo.callbacks);
            },

            caf: function SegmentedUI2$_animator_caf(callback) {
                cancelAnimationFrame(callback);
            },

            canAnimate: function  SegmentedUI2$_animator_canAnimate(animObj) {
                var $K = kony.$kwebfw$, $KW = $K.widget, result = true;


                if(!animObj || !animObj.definition || !animObj.definition.applyRowAnimation) {
                    result = false;
                }

                if(!$KW.visible(this)) {
                    result = false;
                }

                return result;
            },

            getRowVisibleState: function SegmentedUI2$_animator_getRowVisibleState(li) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KU = $K.utils, $KD = $K.dom,
                    el = null, offsetTop, offsetHeight, scrollTop, segHeight, state;

                el = $KW.el(this);

                offsetTop = li.offsetTop;
                offsetHeight = li.offsetHeight;

                scrollTop = el.viewport.scrollTop;
                segHeight = el.viewport.offsetHeight;

                if(offsetTop + offsetHeight <= scrollTop) {
                    state = 'past';
                } else if(offsetTop >= scrollTop + segHeight) {
                    state = 'future';
                } else {
                    state = 'present';
                }

                return state;
            },

            handleStateAnimations: function SegmentedUI2$_animator_handleStateAnimations() {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KU = $K.utils, $KD = $K.dom,
                visibleFirstRow = null, visibleLastRow = null, _ = this._kwebfw_;

                if(_.prop.onRowDisplay) {
                    visibleFirstRow = _getVisibleRow.call(this, {firstRow: true, lastRow: false});
                    visibleLastRow = _getVisibleRow.call(this, {firstRow: false, lastRow: true});
                }

                $KU.each(_.rows, function(clone, index) {
                    var li = null, state = null, animObj = null, visibleState = null, currentRowContext = null;

                    if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                        li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');
                    }

                    if(li) {
                        state = _animator.SegmentedUI2.getRowVisibleState.call(this, li);
                        if(clone._kwebfw_.animState !== state) {
                            if(state === 'present') {
                                animObj = _.visibleAnim;
                                visibleState = kony.segment.VISIBLE;
                            } else {
                                animObj = _.invisibleAnim;
                                visibleState = kony.segment.INVISIBLE;
                            }

                            currentRowContext = _getRowContext(clone);
                            if(this._kwebfw_.prop.onRowDisplay && currentRowContext.rowIndex != -1) {
                                $KW.fire(this, 'onRowDisplay', this,
                                    {
                                        model: this,
                                        state : visibleState,
                                        currentRowContext: currentRowContext,
                                        startRowContext: visibleFirstRow,
                                        endRowContext: visibleLastRow
                                    }
                                );
                            }

                            if(animObj) {
                                animObj.definition.applyRowAnimation([clone], animObj.config, animObj.callbacks);
                            }
                            clone._kwebfw_.animState = state;
                        }
                    }
                }, this);

                this._kwebfw_.rafValue = _animator.SegmentedUI2.raf(_animator.SegmentedUI2.handleStateAnimations.bind(this));
            },

            onRowDisplayHandler: function SegmentedUI2$_animator_onRowDisplayHandler(action, rows) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom, $KU = $K.utils, _ = this._kwebfw_,
                    visibleLastRow = null, visibleLastRow = null,
                    config = {}, currentRowContext = null;

                if(_.prop.onRowDisplay) {
                    visibleFirstRow = _getVisibleRow.call(this, {firstRow: true, lastRow: false});
                    visibleLastRow = _getVisibleRow.call(this, {firstRow: false, lastRow: true});

                    $KU.each(rows, function(clone, index) {
                        currentRowContext = _getRowContext(clone);
                        config = {
                            model: this,
                            state : action,
                            currentRowContext: currentRowContext,
                            startRowContext: visibleFirstRow,
                            endRowContext: visibleLastRow
                        };
                        if(currentRowContext.rowIndex != -1) {
                            $KW.fire(this, 'onRowDisplay', this, config);
                        }
                    }, this);
                }
            },

            raf: function SegmentedUI2$_animator_raf (callback) {
                return requestAnimationFrame(callback);
            },

            scrolled: false,

            scrollEnd: function SegmentedUI2$_animator_scrollEnd() {
                this._kwebfw_.rafValue && _animator.SegmentedUI2.caf(this._kwebfw_.rafValue);
            },

            scrollStart: function SegmentedUI2$_animator_scrollStart() {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                el = null, timer = null, segNode = null, model = null;

                segNode = $KD.closest(this, 'kw');
                if(segNode) {
                    model = $KW.model(segNode);
                    el = $KW.el(model);

                    if(!_animator.SegmentedUI2.scrolled) {
                        if(model._kwebfw_.visibleAnim
                           || model._kwebfw_.invisibleAnim
                           || model._kwebfw_.prop.onRowDisplay) {
                            _animator.SegmentedUI2.setAnimationStates.call(model);
                            model._kwebfw_.rafValue = _animator.SegmentedUI2.raf(_animator.SegmentedUI2.handleStateAnimations.bind(model));
                        }
                        _animator.SegmentedUI2.scrolled = true;
                    }

                    if(_animator.SegmentedUI2.scrollTimer) {
                        clearTimeout(_animator.SegmentedUI2.scrollTimer);
                    }

                    _animator.SegmentedUI2.scrollTimer = setTimeout(function() {
                        _animator.SegmentedUI2.scrollEnd.call(model)
                        clearTimeout(_animator.SegmentedUI2.scrollTimer);
                        _animator.SegmentedUI2.scrollTimer = null;
                        _animator.SegmentedUI2.scrolled = false;
                    }, 250);
                }
            },

            scrollTimer: null,

            setAnimations: function SegmentedUI2$_animator_setAnimations(animInfo) {
                var animObj = null, _ = this._kwebfw_;

                _.visibleAnim = null;
                _.invisibleAnim = null;

                if(animInfo) {
                    if(animInfo.visible) {
                        animObj = animInfo.visible;
                        if(animObj && animObj.definition && animObj.definition.applyRowAnimation) {
                            _.visibleAnim = animObj;
                        }
                    }

                    if(animInfo.invisible) {
                        animObj = animInfo.invisible;
                        if(animObj && animObj.definition && animObj.definition.applyRowAnimation) {
                            _.invisibleAnim = animObj;
                        }
                    }
                }
            },

            setAnimationStates: function SegmentedUI2$_animator_setAnimationStates() {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KU = $K.utils, $KD = $K.dom;

                $KU.each(this._kwebfw_.rows, function(clone, index) {
                    var li = null, state = null;

                    if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                        li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');
                    }

                    if(li) {
                        state = _animator.SegmentedUI2.getRowVisibleState.call(this, li);
                        clone._kwebfw_.animState = state;
                    }

                }, this);
            }
       }
    };


    //This function will be called in the scope of widget instance
    //This function is not yet tested, specially when section index value is there
    var _absoluteIndex = function SegmentedUI2$_absoluteIndex(index) {
        var data = this._kwebfw_.prop.data, absIndex = -1, secIndex = -1, rowIndex = -1;

        index = _deduceIndex.call(this, index);
        secIndex = index[0]; rowIndex = index[1];

        if(!(secIndex === -1 && rowIndex === -1)) {
            if(_isSectionDS(data[0])) {
                if(secIndex >= 0) {
                    if(secIndex < data.length) {
                        for(index=0; index<=secIndex; index++) {
                            absIndex += 1;

                            if(index < secIndex) {
                                absIndex += data[index][1].length;
                            } else if(rowIndex > -1 && rowIndex < data[index][1].length) {
                                absIndex += (rowIndex + 1);
                            }
                        }
                    } else {
                        //SectionIndex exceeds its max boundary for a sectionable segment.
                    }
                } else {
                    //Negative SectionIndex found for a sectionable segment.
                }
            } else { //Non sectionable segment
                if(rowIndex >= 0) {
                    if(rowIndex < data.length) {
                        absIndex = rowIndex;
                    } else {
                        //RowIndex exceeds its max boundary for a non-sectionable segment.
                    }
                } else {
                    //Negative RowIndex found for a non-sectionable segment.
                }
            }
        }

        return absIndex;
    };


    //This function will be called in the scope of widget instance
    //This function is not yet tested, specially when section index value is there
    var _absoluteRowIndex = function SegmentedUI2$_absoluteRowIndex(secIndex, rowIndex) {
        var absIndex = 0, _ = this._kwebfw_, data = _.prop.data, clones = _.clones;

        var increment = function(clone, index) {
            if(clone && clone.isVisible) index++;

            return index;
        };

        if(clones.length > 0) {
            if(_isSectionDS(data[0])) {
                if(rowIndex === -1) {
                    secIndex--;
                    if(secIndex >= 0) rowIndex = data[secIndex][1].length - 1;
                } else {
                    rowIndex = rowIndex - 1;
                }
                while(secIndex < data.length && secIndex >= 0) {
                    if(rowIndex === -1) {
                        absIndex = increment(clones[secIndex][0], absIndex);
                        secIndex--;
                        if(secIndex >= 0) rowIndex = data[secIndex][1].length - 1;
                    } else if(rowIndex < data[secIndex][1].length && rowIndex > -1) {
                        absIndex = increment(clones[secIndex][1][rowIndex], absIndex);
                        rowIndex--;
                    }
                }
            } else {
                rowIndex = rowIndex - 1;
                while(rowIndex < data.length && rowIndex >= 0) {
                    absIndex = increment(clones[rowIndex], absIndex);
                    rowIndex--;
                }
            }
        }

        return absIndex;
    };


    //This function will be called in the scope of widget instance
    var _applyNodeStyles = function SegmentedUI2$_applyNodeStyles() {
        var $K = kony.$kwebfw$, $KU = $K.utils;

        $KU.each(this._kwebfw_.rows, function(clone, index) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                li = null;

                if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                    li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');
                }

            if(li) {
                _applyRowSeparator.call(this, li, clone);
                _applyRowAndHeaderSkin.call(this, li, clone, index);
            }
        }, this);
    };


    //This function will be called in the scope of widget instance
    var _applyRowSeparator = function SegmentedUI2$_applyRowSeparator(li, clone) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, color = '',
            index = _deduceIndex.call(this, clone), thickness = '',
            secIndex = index[0], rowIndex = index[1], prop = this._kwebfw_.prop;

        if(prop.separatorRequired && prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) {
            //If not the first row, then add separator
            if(!(( rowIndex === -1) || (rowIndex === 0))) {
                thickness = (prop.separatorThickness + 'px');
                color = $KU.convertHexToRGBA(prop.separatorColor);
                $KD.style(li, 'borderTop', (thickness+' solid '+(color || 'transparent')));
            } else {
                $KD.style(li, 'border-top', null);
            }
        } else {
            $KD.style(li, 'border-top', null);
        }
    };


    //This function will be called in the scope of widget instance
    var _applyRowAndHeaderSkin = function SegmentedUI2$_applyRowAndHeaderSkin(li, clone, index) {
        var $K = kony.$kwebfw$, $KD = $K.dom, ii = clone._kwebfw_.ii,
            prop = this._kwebfw_.prop, rowSkin = prop.rowSkin;

        if(ii.indexOf(',-1') !== -1) {
            // sectionHeaderSkin
            $KD.setAttr(li, 'class', prop.sectionHeaderSkin);
        } else {
            // Row Skin and alternate Row Skin
            if(prop.alternateRowSkin && prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) {
                if(index % 2 !== 0) rowSkin = prop.alternateRowSkin;
            }
            $KD.setAttr(li, 'class', rowSkin);
        }
    };


    //This function will be called in the scope of widget instance
    var _canRenderRow = function SegmentedUI2$_canRenderRow(index, clone, item, tpl) {
        var $K = kony.$kwebfw$, $KU = $K.utils, _ = this._kwebfw_,
            secIndex = -1, rowIndex = -1, flag = false;

        if($K.F.RIVW) {
            flag = true;
        } else {
            index = _deduceIndex.call(this, index);
            secIndex = index[0]; rowIndex = index[1];

            if(arguments.length === 1 && !(secIndex === -1 && rowIndex === -1)) {
                if(secIndex === -1) {
                    clone = _.clones[rowIndex];
                    item = _.prop.data[rowIndex];
                    tpl = this.rowTemplate;
                } else {
                    if(rowIndex === -1) {
                        clone = _.clones[secIndex][0];
                        item = _.prop.data[secIndex][0];
                        tpl = this.sectionHeaderTemplate;
                    } else if(!_isSectionCollapsed.call(this, secIndex)) {
                        clone = _.clones[secIndex][1][rowIndex];
                        item = _.prop.data[secIndex][1][rowIndex];
                        tpl = this.rowTemplate;
                    }
                }
            }

            if(secIndex !== -1 && rowIndex !== -1
            && _isSectionCollapsed.call(this, secIndex)) {
                flag = false;
            } else if($KU.is(clone, 'widget')) {
                flag = clone.isVisible;
            } else if($KU.is(item, 'object')
            && $KU.is(tpl, 'widget')
            && !$KU.is(item[_.prop.widgetDataMap[tpl.id]], 'undefined')
            && item[_.prop.widgetDataMap[tpl.id]].hasOwnProperty('isVisible')
            && $KU.is(item[_.prop.widgetDataMap[tpl.id]].isVisible, 'boolean')) {
                flag = item[_.prop.widgetDataMap[tpl.id]].isVisible;
            } else if($KU.is(tpl, 'widget')) {
                flag = tpl.isVisible;
            }
        }

        return flag;
    };

    //This function will be called in the scope of widget instance
    var _clearSelectedIndices = function SegmentedUI2$_clearSelectedIndices() {
        var _ = this._kwebfw_, rows = _.selectedRows;

        rows.splice(0, rows.length);
        _setSelectedRowsRelatedProperties.call(this);

        if(_.swipeContext) _.swipeContext = null;
    };


    //This function will be called in the scope of widget instance
    var _deduceIndex = function SegmentedUI2$_deduceIndex(index) {
        var $K = kony.$kwebfw$, $KU = $K.utils, secIndex = -1,
            data = this._kwebfw_.prop.data, rowIndex = -1;

        if(data && data.length > 0) {
            if($KU.is(index, 'widget')) {
                index = index._kwebfw_.ii;
            }

            if($KU.is(index, 'string') && index) {
                index = index.split(',');
                if(index.length === 2) {
                    secIndex = parseInt(index[0], 10);
                    rowIndex = parseInt(index[1], 10);

                    if(!$KU.is(secIndex, 'number')) {
                        secIndex = -1;
                    }
                    if(!$KU.is(rowIndex, 'number')) {
                        rowIndex = -1;
                    }
                }
            } else if($KU.is(index, 'number')) {
                secIndex = -1;
                rowIndex = index;
            } else if($KU.is(index, 'array')
            && $KU.is(index[0], 'number')
            && $KU.is(index[1], 'number')) {
                secIndex = index[0];
                rowIndex = index[1];
            }

            if(secIndex < -1) secIndex = -1;
            if(rowIndex < -1) rowIndex = -1;
        }

        return [secIndex, rowIndex];
    };


    //This function will be called in the scope of widget instance
    //This function assumes index passed in argument is a valid array
    var _deduceTemplate = function SegmentedUI2$_deduceTemplate(index) {
        var $K = kony.$kwebfw$, $KU = $K.utils, data = this._kwebfw_.prop.data,
            secIndex = index[0], rowIndex = index[1], item = null, tpl = null;

        if(_isSectionDS(data[0])) {
            if(rowIndex < 0) {
                item = data[secIndex][0];

                if($KU.is(item, 'string')) {
                    tpl = _getFrameworkHeaderTemplate();
                } else {
                    tpl = item.template || this.sectionHeaderTemplate;
                }
            } else {
                item = data[secIndex][1][rowIndex];
                tpl = item.template || this.rowTemplate;
            }
        } else {
            item = data[rowIndex];
            tpl = item.template || this.rowTemplate;
        }

        return tpl;
    };


    //This function will be called in the scope of widget instance
    //This <index> should be the this._kwebfw_.rows[0]
    var _deduceTop = function SegmentedUI2$_deduceTop(index) {
        var data = this._kwebfw_.prop.data, top = 0, secIndex = -1, rowIndex = -1;

        index = _deduceIndex.call(this, index);
        secIndex = index[0]; rowIndex = index[1];

        if(secIndex === -1 && rowIndex === -1) return top;

        if(secIndex === -1) {
            rowIndex--;

            while(rowIndex >= 0) {
                top += _getRowHeight.call(this, [secIndex, rowIndex]);
                rowIndex--;
            }
        } else {
            if(rowIndex === -1) {
                secIndex--;
                rowIndex = (data[secIndex][1].length - 1);
            } else {
                rowIndex--;
            }

            while(secIndex >= 0) {
                top += _getRowHeight.call(this, [secIndex, rowIndex]);

                if(rowIndex === -1) {
                    secIndex--;
                    rowIndex = (data[secIndex][1].length - 1);
                } else {
                    rowIndex--;
                }
            }
        }

        return top;
    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    var _dependentPropertiesValidationMessage = {
        SegmentedUI2: function SegmentedUI2$_dependentPropertiesValidationMessage(prop, bconfig, lconfig, pspconfig) {
            var $K = kony.$kwebfw$, $KU = $K.utils, flag = false, message = '',
                sectionable = false, data = bconfig.data || prop.data,
                index = bconfig.selectedRowIndex || prop.selectedRowIndex;

            if($KU.is(index, 'null')) {
                flag = true;
            } else if(data && data.length && $KU.is(index, 'array') && index.length === 2
            && $KU.is(index[0], 'number') && $KU.is(index[1], 'number')) {

                sectionable = _isSectionDS(data[0]);

                if(!sectionable) {
                    if(index[0] === 0 && index[1] >= 0 && index[1] < data.length) {
                        flag = true;
                    }
                } else if(index[0] >= 0 && index[0] < data.length) {
                    if(index[1] >= -1 && index[1] < data[index[0]][1].length) {
                        flag = true;
                    }
                }
            }

            if(!flag) {
                message += 'Segment <selectedRowIndex> value is invalid';
            }

            return message;
        }
    };


    //This function will be called in the scope of widget instance
    var _executeOnRow = function SegmentedUI2$_executeOnRow(index, callback, args) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, tpl = null,
            data = this._kwebfw_.prop.data, item = null, secIndex = -1, rowIndex = -1;

        index = _deduceIndex.call(this, index);
        secIndex = index[0]; rowIndex = index[1];

        if(secIndex === -1 && rowIndex === -1) return;

        if(_isSectionDS(data[0])) {
            if(secIndex >= 0 && secIndex < data.length) {
                if(rowIndex < 0) {
                    if($KU.is(callback, 'function')) {
                        if(!$KU.is(args, 'array')) args = [];
                        item = data[secIndex][0];
                        if($KU.is(item, 'string')) {
                            tpl = _getFrameworkHeaderTemplate();
                        } else {
                            tpl = item.template || this.sectionHeaderTemplate;
                        }

                        tpl = $KW.getTemplate(this, tpl);

                        if(!$KU.is(this._kwebfw_.clones[secIndex], 'array')) {
                            this._kwebfw_.clones[secIndex] = [null, []];
                        }

                        if(!$KU.is(this._kwebfw_.heights[secIndex], 'array')) {
                            this._kwebfw_.heights[secIndex] = [-1, []];
                        }

                        args.splice(0, 0, [secIndex, rowIndex]);
                        args.splice(1, 0, item);
                        args.splice(2, 0, tpl);
                        args.splice(3, 0, this._kwebfw_.clones[secIndex][0]);
                        args.splice(4, 0, this._kwebfw_.heights[secIndex][0]);

                        callback.apply(this, args);
                    }
                } else if(rowIndex >= 0 && rowIndex < data[secIndex][1].length) {
                    if($KU.is(callback, 'function')) {
                        if(!$KU.is(args, 'array')) args = [];
                        item = data[secIndex][1][rowIndex];
                        tpl = item.template || this.rowTemplate;
                        tpl = $KW.getTemplate(this, tpl);

                        if(!$KU.is(this._kwebfw_.clones[secIndex], 'array')) {
                            this._kwebfw_.clones[secIndex] = [null, []];
                        }
                        if($KU.is(this._kwebfw_.clones[secIndex][1][rowIndex], 'undefined')) {
                            this._kwebfw_.clones[secIndex][1][rowIndex] = null;
                        }

                        if(!$KU.is(this._kwebfw_.heights[secIndex], 'array')) {
                            this._kwebfw_.heights[secIndex] = [-1, []];
                        }
                        if($KU.is(this._kwebfw_.heights[secIndex][1][rowIndex], 'undefined')) {
                            this._kwebfw_.heights[secIndex][1][rowIndex] = -1;
                        }

                        args.splice(0, 0, [secIndex, rowIndex]);
                        args.splice(1, 0, item);
                        args.splice(2, 0, tpl);
                        args.splice(3, 0, this._kwebfw_.clones[secIndex][1][rowIndex]);
                        args.splice(4, 0, this._kwebfw_.heights[secIndex][1][rowIndex]);

                        callback.apply(this, args);
                    }
                }
            }
        } else if(secIndex < 0) {
            if(rowIndex >= 0 && rowIndex < data.length) {
                if($KU.is(callback, 'function')) {
                    if(!$KU.is(args, 'array')) args = [];
                    item = data[rowIndex];
                    tpl = item.template || this.rowTemplate;
                    tpl = $KW.getTemplate(this, tpl);

                    if(!$KU.is(this._kwebfw_.clones[rowIndex], 'undefined')) {
                        this._kwebfw_.clones[rowIndex] = null;
                    }

                    if(!$KU.is(this._kwebfw_.heights[rowIndex], 'undefined')) {
                        this._kwebfw_.heights[rowIndex] = -1;
                    }

                    args.splice(0, 0, [secIndex, rowIndex]);
                    args.splice(1, 0, item);
                    args.splice(2, 0, tpl);
                    args.splice(3, 0, this._kwebfw_.clones[rowIndex]);
                    args.splice(4, 0, this._kwebfw_.heights[rowIndex]);

                    callback.apply(this, args);
                }
            }
        }
    };


    //This function will be called in the scope of widget instance
    var _firstRenderableRow = function SegmentedUI2$_firstRenderableRow() {
        var data = this._kwebfw_.prop.data, row = null, secIndex = -1, rowIndex = -1;

        if(data) {
            if(!_isSectionDS(data[0])) {
                rowIndex = 0;

                while(rowIndex < data.length) {
                    if(_canRenderRow.call(this, [-1, rowIndex])) {
                        row = _getClonedTemplate.call(this, [-1, rowIndex], true);
                        break;
                    } else {
                        rowIndex++;
                    }
                }
            } else {
                secIndex = 0;

                while(secIndex < data.length) {
                    if(_canRenderRow.call(this, [secIndex, rowIndex])) {
                        row = _getClonedTemplate.call(this, [secIndex, rowIndex], true);
                        break;
                    } else {
                        if(rowIndex === (data[secIndex][1].length - 1)) {
                            secIndex++;
                            rowIndex = -1;
                        } else {
                            rowIndex++;
                        }
                    }
                }
            }
        }

        return row;
    };


    //This function will be called in the scope of widget instance
    var _flushClones = function SegmentedUI2$_flushClones(clones) {
        var $K = kony.$kwebfw$, $KU = $K.utils;

        if($KU.is(clones, 'widget')) clones = [clones];

        if($KU.is(clones, 'array')) {
            $KU.each(clones, function(section) {
                var $K = kony.$kwebfw$, $KU = $K.utils;

                if($KU.is(section, 'array')) {
                    if($KU.is(section[0], 'widget')) {
                        section[0]._flush(true);
                    }

                    $KU.each(section[1], function(row) {
                        var $K = kony.$kwebfw$, $KU = $K.utils;

                        if($KU.is(row, 'widget')) {
                            row._flush(true);
                        }
                    });
                } else if($KU.is(section, 'widget')) {
                    section._flush(true);
                }
            }, this);
        }
    };


    //This function will be called in the scope of widget instance
    var _getClonedTemplate = function SegmentedUI2$_getClonedTemplate(index, forced) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, template = null;

        _executeOnRow.call(this, index, function(index, item, tpl, clone, height) {
            var secIndex = index[0], rowIndex = index[1],
                prop = this._kwebfw_.prop, self = this;

            if($KU.is(clone, 'widget')) {
                template = $KW.cloneTemplate(clone, item, prop.widgetDataMap);
            } else {
                if(forced) {
                    template = $KW.cloneTemplate(tpl, item, prop.widgetDataMap, function(model, pmodel, windex) {
                        model._kwebfw_.ii = index.join(',');
                        _updateSpecialProperties.call(self, model);
                        _setBehaviorConfig.call(self, model);
                    });
                } else if(_canRenderRow.call(this, index, clone, item, tpl)) {
                    template = $KW.cloneTemplate(tpl, item, prop.widgetDataMap, function(model, pmodel, windex) {
                        model._kwebfw_.ii = index.join(',');
                        _updateSpecialProperties.call(self, model);
                        _setBehaviorConfig.call(self, model);
                    });
                }

                if(secIndex === -1) {
                    this._kwebfw_.clones[rowIndex] = template;
                } else {
                    if(rowIndex === -1) {
                        this._kwebfw_.clones[secIndex][0] = template;
                    } else {
                        this._kwebfw_.clones[secIndex][1][rowIndex] = template;
                    }
                }
            }
        });

        return template;
    };


    var _getFrameworkHeaderTemplate = function SegmentedUI2$_getFrameworkHeaderTemplate() {
        var flx = null, lbl = null,
            bconfig = {
                'id': 'flxkwebfwHeader',
                'layoutType': kony.flex.FREE_FORM,
                'height': kony.flex.USE_PREFERRED_SIZE,
                'autogrowMode': kony.flex.AUTOGROW_HEIGHT
            };

        flx = new kony.ui.FlexContainer(bconfig);
        lbl = new kony.ui.Label({'id': 'labelkwebfwHeader'});

        flx.add(lbl);

        return flx;
    };


    var _getIndex = function SegmentedUI2$_getIndex(find, list) {
        var position = -1, i = 0, ilen = list.length;

        for(i=0; i<ilen; i++) {
            if(list[i][0] === find[0] && list[i][1] === find[1]) {
                position = i;
                break;
            }
        }

        return position;
    };


    //This function will be called in the scope of widget instance
    var _getIndexedInfo = function SegmentedUI2$_getIndexedInfo(index, data) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, item = null,
            _ = this._kwebfw_, prop  =_.prop, secIndex = -1, rowIndex = -1;

        index = _deduceIndex.call(this, index);
        secIndex = index[0]; rowIndex = index[1];

        if(secIndex === -1 && rowIndex === -1) return null;

        if(_isSectionDS(prop.data[0])) {
            if(secIndex >= 0 && secIndex < prop.data.length) {
                if(rowIndex < 0) {
                    item = data[secIndex][0];
                } else if(rowIndex >= 0 && rowIndex < prop.data[secIndex][1].length) {
                    item = data[secIndex][1][rowIndex];
                }
            }
        } else if(secIndex < 0) {
            if(rowIndex >= 0 && rowIndex < prop.data.length) {
                item = data[rowIndex];
            }
        }

        return item;
    };


    var _getInvertedDataMap = function SegmentedUI2$_getInvertedDataMap(map) {
        var $K = kony.$kwebfw$, $KU = $K.utils, invertedMap = {};

        if($KU.is(map, 'object')) {
            $KU.each(map, function(value, key) {
                invertedMap[value] = key;
            });
        }

        return invertedMap;
    };


    //This function will be called in the scope of widget instance
    var _getRenderableClones = function SegmentedUI2$_getRenderableClones(startIndex, endIndex) {
        var data = this._kwebfw_.prop.data, rows = [], startSecIndex = startIndex[0],
            startRowIndex = startIndex[1], endSecIndex = -1, endRowIndex = -1,
            datalen = 0;

        var cloneRow = function(model, index) {
            var $K = kony.$kwebfw$, $KU = $K.utils,
                tpl = _getClonedTemplate.call(model, index);

            if($KU.is(tpl, 'widget') && _canRenderRow.call(model, tpl)) {
                rows.push(tpl);
            }
        };

        if(startSecIndex <= -1 && startRowIndex <= -1) return rows;

        if(data) {
            datalen = data.length - 1;

            if(!endIndex) {
                if(_isSectionDS(data[0])) endSecIndex = datalen;
                else endRowIndex = datalen;
            } else {
                endSecIndex = endIndex[0];
                endRowIndex = endIndex[1];
                if(_isSectionDS(data[0])) {
                    if(endSecIndex > datalen) endSecIndex = datalen;
                } else {
                    if(endRowIndex > datalen) endRowIndex = datalen;
                }
            }

            if(_isSectionDS(data[0])) {
                while(startSecIndex <= endSecIndex) {
                    if(startRowIndex >= data[startSecIndex][1].length) {
                        startSecIndex++;
                        startRowIndex = -1;
                    } else {
                        cloneRow(this, [startSecIndex, startRowIndex]);
                        startRowIndex++;
                    }
                }
            } else {
                while(startRowIndex <= endRowIndex && startRowIndex >= 0) {
                    cloneRow(this, [startSecIndex, startRowIndex]);
                    startRowIndex++;
                }
            }
        }

        return rows;
    };


    //This function will be called in the scope of widget instance
    var  _getRowContext = function SegmentedUI2$_getRowContext(clone) {
        var cindex = clone._kwebfw_.ii.split(','),
            secIndex = parseInt(cindex[0], 10),
            rowIndex = parseInt(cindex[1], 10);
        return {
            sectionIndex: (secIndex === -1 ? 0 :secIndex),
            rowIndex: rowIndex
        }
    };


    //This function will be called in the scope of widget instance
    var  _getRowCount = function SegmentedUI2$_getRowCount() {
        var prop = this._kwebfw_.prop, rowcount = 0,
            secIndex = -1, rowIndex = -1;

        if(prop.data && prop.data.length) {
            if(_isSectionDS(prop.data[0])) {
                secIndex = (prop.data.length - 1);
                rowIndex = (prop.data[secIndex][1].length - 1);
            } else {
                rowIndex = (prop.data.length - 1);
            }
            rowcount = (_absoluteIndex.call(this, [secIndex, rowIndex]) + 1);
        }

        return rowcount;
    };


    //This function will be called in the scope of widget instance
    var _getRowHeight = function SegmentedUI2$_getRowHeight(index) {
        var size = 0;

        _executeOnRow.call(this, index, function(index, item, tpl, clone, height) {
            var $K = kony.$kwebfw$, $KU = $K.utils;

            var updateHeightAndClone = function(index, height, clone) {
                var $K = kony.$kwebfw$, $KU = $K.utils,
                    secIndex = index[0], rowIndex = index[1];

                if(secIndex === -1) {
                    if(height !== -1) {
                        this._kwebfw_.heights[rowIndex] = height;
                    }
                    if($KU.is(clone, 'widget')) {
                        this._kwebfw_.clones[rowIndex] = clone;
                    }
                } else {
                    if(rowIndex === -1) {
                        if(height !== -1) {
                            this._kwebfw_.heights[secIndex][0] = height;
                        }
                        if($KU.is(clone, 'widget')) {
                            this._kwebfw_.clones[secIndex][0] = clone;
                        }
                    } else {
                        if(height !== -1) {
                            this._kwebfw_.heights[secIndex][1][rowIndex] = height;
                        }
                        if($KU.is(clone, 'widget')) {
                            this._kwebfw_.clones[secIndex][1][rowIndex] = clone;
                        }
                    }
                }
            };

            if(height !== -1) {
                size = height;
            } else {
                if($KU.is(clone, 'widget')) {
                    /* TODO:: Calculate "size" from clone
                    if(clone.height is autogrow) {
                        clone._render() and append to el.scrolee with css properties visibility:hidden;
                        then run clone.forceLayout() and size = clone.frame.height
                        then remove the DOM from view and update this._kwebfw_.minScroleeHeight += size
                        then set el.scrolee.style.height = this._kwebfw_.minScroleeHeight + 'px'
                    } else { //clone.height can not be in percent
                        //
                    }

                    updateHeightAndClone.call(this, index, size);
                    //*/
                } else {
                    if(!item.hasOwnProperty('height')) {
                        /* TODO:: Calculate "size" from tpl
                        if(tpl.height is autogrow) {
                            create a clone, then clone._render()
                            and append to el.scrolee with css properties visibility:hidden;
                            then run clone.forceLayout() and size = clone.frame.height
                            then remove the DOM from view and update this._kwebfw_.minScroleeHeight += size
                            then set el.scrolee.style.height = this._kwebfw_.minScroleeHeight + 'px'
                        } else { //tpl.height can not be in percent
                            //
                        }
                        //*/
                    } else {
                        /* TODO:: Calculate "size" from item
                        if(item.height is autogrow) {
                            create a clone, then clone._render()
                            and append to el.scrolee with css properties visibility:hidden;
                            then run clone.forceLayout() and size = clone.frame.height
                            then remove the DOM from view and update this._kwebfw_.minScroleeHeight += size
                            then set el.scrolee.style.height = this._kwebfw_.minScroleeHeight + 'px'
                        } else { //item.height can not be in percent
                            //
                        }
                        //*/
                    }

                    updateHeightAndClone.call(this, index, size, clone);
                }
            }
        });

        return size;
    };


    var _getVisibleRow = function SegmentedUI2$_getVisibleRow(config) {
        var $K = kony.$kwebfw$, $KW = $K.widget, $KU = $K.utils,
            $KD = $K.dom, el = $KW.el(this), visibleRow = null,
            scrollTop = 0, scrollBottom = 0, index = null, visibleIndex = null,
            prop = this._kwebfw_.prop;

        if(el.node && prop.data) {
            scrollTop = el.viewport.scrollTop;
            scrollBottom = scrollTop + el.scrolee.offsetHeight;

            $KU.each($KD.children(el.scrolee), function(li) {
                var rowTop = li.offsetTop, rowBottom = rowTop + li.offsetHeight;

                if(config.firstRow && scrollTop < rowBottom) {
                    visibleRow = li;
                    return true;
                }

                if(config.lastRow
                && (scrollBottom < rowTop || scrollBottom <= rowBottom + 1)) {
                    visibleRow = li;
                    return true;
                }
            });

            index = $KD.getAttr(visibleRow, 'kii');
            index = _deduceIndex.call(this, index);

            if(_isSectionDS(prop.data[0])) {
                visibleIndex = { sectionIndex: index[0]};
                if(index[1] !== -1) visibleIndex['rowIndex'] = index[1];
            } else {
                visibleIndex = {rowIndex: index[1]};
            }
        }

        return visibleIndex;

    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    var _getter = {
        SegmentedUI2: {
            contentOffset: function FlexScrollContainer$_getter_contentOffset(value) {
                return {x:value.x, y:value.y};
            },

            contentOffsetMeasured: function SegmentedUI2$_getter_contentOffsetMeasured(value) {
                var $K = kony.$kwebfw$, $KW = $K.widget, _ = this._kwebfw_,
                    prop = _.prop, scroll = _.ui.scroll, el = $KW.el(this),
                    rowIndex = -1;
                if(prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) {
                    value.x = scroll.x;
                    value.y = scroll.y;
                } else {
                    if(_.swipeContext && _.swipeContext.currentPage > -1) {
                        rowIndex = _.swipeContext.currentPage;
                    } else if(prop.selectedRowIndex && prop.selectedRowIndex.length === 2) {
                        rowIndex = prop.selectedRowIndex[1];
                    }
                    if(_.swipeContext && _.swipeContext.imageWidth) {
                        value.x = rowIndex * (_.swipeContext.imageWidth);
                    } else {
                        value.x = 0;
                    }
                    value.y = 0;
                }
                return {x:value.x, y:value.y};
            },

            data: function SegmentedUI2$_getter_data(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, data = [];

                if(!value) {
                    return value;
                } else {
                    if(!_isSectionDS(value)) {
                        data = value.slice(0);
                    } else {
                        $KU.each(value, function(section) {
                            data.push([section[0], section[1].slice(0)]);
                        });
                    }

                    return data;
                }
            },

            scrollingEvents: function SegmentedUI2$_getter_scrollingEvents(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils,
                    scrollingEvents = value ? {} : null;

                $KU.each(value, function(val, key) {
                    scrollingEvents[key] = val;
                });

                return scrollingEvents;
            },

            selectedRowIndex: function SegmentedUI2$_getter_selectedRowIndex(value) {
                var prop = this._kwebfw_.prop, index = null;

                if(prop.selectedRowIndex) {
                    if(value[0] === -1) {
                        index = [0, value[1]];
                    } else {
                        index = value.slice(0);
                    }
                }

                return index;
            },

            selectedRowIndices: function SegmentedUI2$_getter_selectedRowIndices(value) {
                var prop = this._kwebfw_.prop, indices = null, s = 0,
                    slen = 0, sindex = 0, rindexes = null;

                if(prop.selectedRowIndices) {
                    indices = [];
                    slen = prop.selectedRowIndices.length;

                    for(s=0; s<slen; s++) {
                        sindex = prop.selectedRowIndices[s][0];
                        rindexes = prop.selectedRowIndices[s][1];

                        if(sindex === -1) sindex = 0;
                        indices.push([sindex, rindexes.slice(0)]);
                    }
                }

                return indices;
            },

            widgetDataMap: function SegmentedUI2$_getter_widgetDataMap(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils;

                return $KU.clone(value);
            }
        }
    };


    //This function will be called in the scope of widget instance
    var _iterateOverData = function SegmentedUI2$_iterateOverData(data, callback) {
        var $K = kony.$kwebfw$, $KU = $K.utils,
            s = 0, r = 0, slen = 0, rlen = 0;

        if(!$KU.is(callback, 'function')) return;

        if($KU.is(data, 'array')) {
            if(_isSectionDS(data[0])) {
                slen = data.length;

                for(s=0; s<slen; s++) {
                    rlen = data[s][1].length;

                    if(callback.call(this, data[s][0], -1, s) === true) {
                        break;
                    }

                    for(r=0; r<rlen; r++) {
                        if(callback.call(this, data[s][1][r], r, s) === true) {
                            break;
                        }
                    }
                }
            } else {
                rlen = data.length;

                for(r=0; r<rlen; r++) {
                    if(callback.call(this, data[r], r, -1) === true) {
                        break;
                    }
                }
            }
        }
    };


    //This function will be called in the scope of widget instance
    var _isSectionCollapsed = function SegmentedUI2$_isSectionCollapsed(index) {
        var $K = kony.$kwebfw$, $KU = $K.utils, data = null,
            _ = this._kwebfw_, prop = _.prop, collapsed = false;

        if(prop.data) {
            if($KU.is(index, 'number') && index >= 0
            && index < prop.data.length) {
                data = prop.data[index][0];

                if($KU.is(data.metaInfo, 'object')
                && data.metaInfo.collapsed === true) {
                    collapsed = true;
                }
            }
        }

        return collapsed;
    };


    var _isSectionDS = function SegmentedUI2$_isSectionDS(data) {
        var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

        if($KU.is(data, 'array')
        && ($KU.is(data[0], 'object') || $KU.is(data[0], 'string'))
        && $KU.is(data[1], 'array')) {
            flag = true;
        }

        return flag;
    };


    //This function will be called in the scope of widget instance
    var _isRowRendered = function SegmentedUI2$_isRowRendered(secIndex, rowIndex) {
        var flag = false, clone = null, r = 0, _ = this._kwebfw_,
            clones = _.clones, rows = _.rows, rlen = rows.length;

        if(clones.length > 0) {
            if(_isSectionDS(_.prop.data[0])) {
                if(rowIndex <= -1) clone = clones[secIndex];
                else clone = clones[secIndex][1][rowIndex];
            } else {
                clone = clones[rowIndex];
            }

            if(clone) {
                for(r=0; r<rlen; r++) {
                    if(rows[r] === clone) {
                        flag = true;
                        break;
                    }
                }
            }
        }

        return flag;
    };


    //This function will be called in the scope of widget instance
    var _isScrollReachingBottom = function SegmentedUI2$_isScrollReachingBottom(scrollTop, offset) {
        var $K = kony.$kwebfw$, scroleeHeight = -1,
            viewportHeight = -1, result = -1, flag = false;

        scroleeHeight = this._kwebfw_.minScroleeHeight;
        viewportHeight = this.frame.height;
        result = scrollTop - scroleeHeight - viewportHeight - offset;

        flag = (result >= 0) ? true : false;

        return flag;
    };


    //This function will be called in the scope of widget instance
    var _isScrollReachingTop = function SegmentedUI2$_isScrollReachingTop(scrollTop, offset) {
        var flag = false, result = scrollTop - offset;

        flag = (result <= 0) ? true : false;

        return flag;
    };


    //This function will be called in the scope of widget instance
    var _lastRenderableRow = function SegmentedUI2$_lastRenderableRow() {
        var data = this._kwebfw_.prop.data, row = null, secIndex = -1, rowIndex = -1;

        if(data) {
            if(!_isSectionDS(data[0])) {
                rowIndex = data.length - 1;

                while(rowIndex >= 0) {
                    if(_canRenderRow.call(this, [-1, rowIndex])) {
                        row = _getClonedTemplate.call(this, [-1, rowIndex], true);
                        break;
                    } else {
                        rowIndex--;
                    }
                }
            } else {
                secIndex = data.length - 1;
                rowIndex = data[secIndex][1].length - 1;

                while(secIndex >= 0) {
                    if(_canRenderRow.call(this, [secIndex, rowIndex])) {
                        row = _getClonedTemplate.call(this, [secIndex, rowIndex], true);
                        break;
                    } else {
                        if(rowIndex === -1) {
                            secIndex--;
                            rowIndex = data[secIndex][1].length - 1;
                        } else {
                            rowIndex--;
                        }
                    }
                }
            }
        }

        return row;
    };


    //This function will be called in the scope of widget instance
    var _nextRenderableRows = function SegmentedUI2$_nextRenderableRows(index, count) {
        var $K = kony.$kwebfw$, $KU = $K.utils, data = this._kwebfw_.prop.data,
            rows = [], tpl = null, startSecIndex = -1, startRowIndex = -1,
            rowIndex = -1, lastRowIndex = -1, lastSecIndex = -1, secIndex = -1;

        index = _deduceIndex.call(this, index);
        startSecIndex = index[0];
        startRowIndex = index[1];

        if(startSecIndex === -1 && startRowIndex === -1) return rows;

        if(startSecIndex === -1) {
            rowIndex = startRowIndex + 1;
            lastRowIndex = (data.length - 1);

            while(count > 0 && rowIndex <= lastRowIndex) {
                tpl = _getClonedTemplate.call(this, [-1, rowIndex]);

                if($KU.is(tpl, 'widget') && _canRenderRow.call(this, tpl)) {
                    rows.push(tpl);
                    count--;
                }

                rowIndex++;
            }
        } else {
            if(startRowIndex === (data[startSecIndex][1].length - 1)) {
                secIndex = startSecIndex + 1;
                rowIndex = -1;
            } else {
                secIndex = startSecIndex;
                rowIndex = startRowIndex + 1;
            }

            lastSecIndex = (data.length - 1);

            while(count > 0 && secIndex <= lastSecIndex) {
                if(rowIndex === -1) {
                    tpl = _getClonedTemplate.call(this, [secIndex, -1]);

                    if($KU.is(tpl, 'widget') && _canRenderRow.call(this, tpl)) {
                        rows.push(tpl);
                        count--;
                    }

                    rowIndex++;
                } else {
                    if(rowIndex === data[secIndex][1].length) {
                        secIndex++;
                        rowIndex = -1;
                    } else {
                        tpl = _getClonedTemplate.call(this, [secIndex, rowIndex]);

                        if($KU.is(tpl, 'widget') && _canRenderRow.call(this, tpl)) {
                            rows.push(tpl);
                            count--;
                        }

                        rowIndex++;
                    }
                }
            }
        }

        return rows;
    };


    //This function will be called in the scope of widget instance
    var _nextTemplate = function SegmentedUI2$_nextTemplate(index) {
        var data = this._kwebfw_.prop.data, secIndex = -1, rowIndex = -1, next = null;

        index = _deduceIndex.call(this, index);

        if(!(secIndex === -1 && rowIndex === -1)) {
            if(secIndex >= 0) {
                if(secIndex < data.length) {
                    if(rowIndex >= 0) {
                        if(rowIndex < data[secIndex][1].length) {
                            if(rowIndex === (data[secIndex][1].length - 1)) {
                                next = _getClonedTemplate.call(this, [(secIndex + 1), -1], true);
                            } else {
                                next = _getClonedTemplate.call(this, [secIndex, (rowIndex + 1)], true);
                            }
                        }
                    } else {
                        if(data[secIndex][1].length > 0) {
                            next = _getClonedTemplate.call(this, [secIndex, 0], true);
                        }
                    }
                }
            } else if(rowIndex >= 0) {
                if(rowIndex < data.length) {
                    if(rowIndex !== (data.length - 1)) {
                        next = _getClonedTemplate.call(this, [-1, (rowIndex + 1)], true);
                    }
                }
            }
        }

        return next;
    };


    var _nextPage = function SegmentedUI2$_nextPage(el, swipeContext) {
        var $K = kony.$kwebfw$, $KD = $K.dom,
            noOfSwipePages = $KD.children(el.scrolee).length, currentPage = 0;

        currentPage = Math.min(swipeContext.currentPage + 1, noOfSwipePages - 1);
        _setPageViewIndicator.call(this, el, currentPage);
        swipeContext.ignoreRowSelection = true;
    };


    //This function will be called in the scope of widget instance
    var _onDataSet = function SegmentedUI2$_onDataSet() {
        var self = this;

        _resetBookKeepers.call(this, function(index) {
            var secIndex = index[0], rowIndex = index[1];

            if('TODO:: height of the segment can be calculated') {
                if(secIndex === -1) {
                    if(this._kwebfw_.heights[rowIndex] >= 0) {
                        self._kwebfw_.minScroleeHeight += this._kwebfw_.heights[rowIndex];
                    }
                } else {
                    if(rowIndex === -1) {
                        if(this._kwebfw_.heights[secIndex][0] >= 0) {
                            self._kwebfw_.minScroleeHeight += this._kwebfw_.heights[secIndex][0];
                        }
                    } else {
                        if(this._kwebfw_.heights[secIndex][1][rowIndex] >= 0) {
                            self._kwebfw_.minScroleeHeight += this._kwebfw_.heights[secIndex][1][rowIndex];
                        }
                    }
                }
            }
        });
    };


    //This function will be called in the scope of widget instance
    var _onRowChange = function SegmentedUI2$_onRowChange(secIndex, rowIndex, action, data, anim) {
        //<action> can be one of "add" / "remove" / "update"/ addsectionat, setsectionat, removesectionat, addall
        var $K = kony.$kwebfw$, $KW = $K.widget, el = $KW.el(this);

        if(secIndex === -1 && rowIndex === -1) return;

        if(['add', 'update', 'remove'].indexOf(action) !== -1
        && !_isSectionDS(this._kwebfw_.prop.data[0])) secIndex = -1;

        if(action === 'addsectionat') {
            _action.SegmentedUI2[action].call(this, secIndex, rowIndex, data, true, anim);
        } else {
            _action.SegmentedUI2[action].call(this, secIndex, rowIndex, data, anim);
        }

        if(el.node && this._kwebfw_.prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
            _setPageView.call(this, el);
        }

        _applyNodeStyles.call(this);
        $KW.markRelayout(this);

        //Update this._kwebfw_.clones
        //Update this._kwebfw_.heights
        //Update this._kwebfw_.rows
        //Update this._kwebfw_.top
        //Update this._kwebfw_.scrollTop
        //Update this._kwebfw_.minScroleeHeight
    };


    //This function will be called in the scope of widget instance
    var _onScroll = function SegmentedUI2$_onScroll(scrollTop) { //scrollTop can never be less than zero
        var $K = kony.$kwebfw$, $KW = $K.widget, el = $KW.el(this), delta = 0;

        if(this._kwebfw_.scrollTop === scrollTop) return;

        delta = Math.abs(this._kwebfw_.scrollTop - scrollTop);

        if(delta <= (_extraHeightToRender - 100)) return;

        if(delta > Math.floor(this._kwebfw_.prop.frame.height/3)) {
            _scrollToRenderNewRows.call(this, scrollTop, el);
        } else {
            if(scrollTop > this._kwebfw_.scrollTop) {
                _scrollDownWithDeltaRows.call(this, delta, el);
            } else {
                _scrollUpWithDeltaRows.call(this, delta, el);
            }
        }
    };


    //This function will be called in the scope of widget instance
    var _onSectionChange = function SegmentedUI2$_onSectionChange(index, action) {
        //<action> can be one of "add" / "remove" / "update"
        var $K = kony.$kwebfw$, $KU = $K.utils, data = this._kwebfw_.prop.data;

        if(!data || !($KU.is(index, 'number') && index >= 0 && index < data.length)) return;

        //Update this._kwebfw_.clones
        //Update this._kwebfw_.heights
        //Update this._kwebfw_.rows
        //Update this._kwebfw_.top
        //Update this._kwebfw_.scrollTop
        //Update this._kwebfw_.minScroleeHeight
    };


    //All widget file must have this variable
    //This functions will be called in the scope of widget instance
    var _populateUnderscore = {
        SegmentedUI2: function SegmentedUI2$_populateUnderscore() {
            var $K = kony.$kwebfw$, $KU = $K.utils, _ = null;

            if(!$KU.is(this._kwebfw_, 'object')) {
                $KU.defineProperty(this, '_kwebfw_', {}, null);
            }
            _ = this._kwebfw_;

            //NOTE:: Any changes to _ (underscore) may need a change in
            //       _cleanUnderscore function of konyui.js file.
            if(!_.ns) $KU.defineProperty(_, 'ns', 'kony.ui.SegmentedUI2', null);
            if(!_.name) $KU.defineProperty(_, 'name', 'SegmentedUI2', null);
            if(!_.selectedRows) $KU.defineProperty(_, 'selectedRows', [], null);
            if(!_.ui) $KU.defineProperty(_, 'ui', {}, null);
            $KU.defineProperty(_.ui, 'scroll', {x:0, y:0, width:-1, height:-1, minX:-1, maxX:-1, minY:-1, maxY:-1, status:'ended'}, true);
            if(typeof _.tabIndex !== 'number') {
                $KU.defineProperty(_, 'tabIndex', 0, true);
            }

            //This holds the searcher condition and config when searchText API invoked
            if(!_.searcher) $KU.defineProperty(_, 'searcher', null, true);

            //This holds the swipe context of page view
            if(!_.swipeContext) $KU.defineProperty(_, 'swipeContext', null, true);

            /* ====================== Book keeping properties starts here ====================== */

            //This holds the cloned templates, those will be rendered, and it is a flat list
            if(!_.rows) $KU.defineProperty(_, 'rows', [], true);

            //This holds the cloned templates, in the same DS as that of this.data
            if(!_.clones) $KU.defineProperty(_, 'clones', [], true);

            //This holds the height of cloned templates, in the same DS as that of this.data
            if(!_.heights) $KU.defineProperty(_, 'heights', [], true);

            //This holds the amount of top padding required for scroll behavior
            if(typeof _.top !== 'number') $KU.defineProperty(_, 'top', 0, true);

            //This holds the last scrolled position
            if(typeof _.scrollTop !== 'number') {
                $KU.defineProperty(_, 'scrollTop', 0, true);
            }

            //This needed for lazy loading, to determine the best size of scrollbar
            if(typeof _.minScroleeHeight !== 'number') {
                $KU.defineProperty(_, 'minScroleeHeight', 0, true);
            }

            if(typeof _.topCutOffAbsoluteIndex !== 'number') {
                $KU.defineProperty(_, 'topCutOffAbsoluteIndex', 0, true);
            }
            if(typeof _.bottomCutOffAbsoluteIndex !== 'number') {
                $KU.defineProperty(_, 'bottomCutOffAbsoluteIndex', 0, true);
            }

            /* ======================= Book keeping properties ends here ======================= */
        }
    };


    //All widget file must have this variable
    //This function will be called in the scope of widget instance
    var _postInitialization = {
        SegmentedUI2: function SegmentedUI2$_postInitialization() {
            this._kwebfw_.invertedDataMap = _getInvertedDataMap(this._kwebfw_.prop.widgetDataMap);
            _onDataSet.call(this);
        }
    };


    //This function will be called in the scope of widget instance
    var _prevRenderableRows = function SegmentedUI2$_prevRenderableRows(index, count) {
        var $K = kony.$kwebfw$, $KU = $K.utils, data = this._kwebfw_.prop.data,
            rows = [], tpl = null, startSecIndex = -1, startRowIndex = -1,
            rowIndex = -1, lastRowIndex = -1, lastSecIndex = -1, secIndex = -1;

        index = _deduceIndex.call(this, index);
        startSecIndex = index[0];
        startRowIndex = index[1];

        if(startSecIndex === -1 && startRowIndex === -1) return rows;

        if(startSecIndex === -1) {
            rowIndex = startRowIndex - 1;
            lastRowIndex = 0;

            while(count > 0 && rowIndex >= lastRowIndex) {
                tpl = _getClonedTemplate.call(this, [-1, rowIndex]);

                if($KU.is(tpl, 'widget') && _canRenderRow.call(this, tpl)) {
                    rows.splice(0, 0, tpl);
                    count--;
                }

                rowIndex--;
            }
        } else {
            if(startRowIndex === -1) {
                secIndex = startSecIndex - 1;
                rowIndex = (data[secIndex][1].length - 1);
            } else {
                secIndex = startSecIndex;
                rowIndex = startRowIndex - 1;
            }

            lastSecIndex = 0;
            lastRowIndex = 0;

            while(count > 0 && secIndex >= lastSecIndex) {
                if(rowIndex === -1) {
                    tpl = _getClonedTemplate.call(this, [secIndex, -1]);

                    if($KU.is(tpl, 'widget') && _canRenderRow.call(this, tpl)) {
                        rows.splice(0, 0, tpl);
                        count--;
                    }

                    secIndex--;
                    rowIndex = this._kwebfw_.clones[secIndex][1].length - 1;
                } else {
                    tpl = _getClonedTemplate.call(this, [secIndex, rowIndex]);

                    if($KU.is(tpl, 'widget') && _canRenderRow.call(this, tpl)) {
                        rows.splice(0, 0, tpl);
                        count--;
                    }

                    rowIndex--;
                }
            }
        }

        return rows;
    };


    //This function will be called in the scope of widget instance
    var _prevTemplate = function SegmentedUI2$_prevTemplate(index) {
        var data = this._kwebfw_.prop.data, secIndex = -1, rowIndex = -1, prev = null;

        index = _deduceIndex.call(this, index);

        if(!(secIndex === -1 && rowIndex === -1)) {
            if(secIndex >= 0) {
                if(secIndex < data.length) {
                    if(rowIndex >= 0) {
                        if(rowIndex < data[secIndex][1].length) {
                            if(rowIndex === 0) {
                                prev = _getClonedTemplate.call(this, [secIndex, -1], true);
                            } else {
                                prev = _getClonedTemplate.call(this, [secIndex, (rowIndex - 1)], true);
                            }
                        }
                    } else {
                        if(secIndex > 0 && data[(secIndex - 1)][1].length > 0) {
                            prev = _getClonedTemplate.call(this, [(secIndex - 1), 0], true);
                        }
                    }
                }
            } else if(rowIndex >= 0) {
                if(rowIndex < data.length) {
                    if(rowIndex > 0) {
                        prev = _getClonedTemplate.call(this, [-1, (rowIndex - 1)], true);
                    }
                }
            }
        }

        return prev;
    };


    //This function will be called in the scope of widget instance
    var _previousPage = function SegmentedUI2$_previousPage(el, swipeContext) {
        var currentPage = 0;

        currentPage = Math.max(swipeContext.currentPage - 1, 0);
        _setPageViewIndicator.call(this, el, currentPage);
        swipeContext.ignoreRowSelection = true;
    };


    //This function will be called in the scope of widget instance
    var _registerSwipeGesture = function SegmentedUI2$_registerSwipeGesture (scrolee) {
        var $K = kony.$kwebfw$, $KD = $K.dom, $KU = $K.utils;

        $KD.on(scrolee, 'swipe', 'segment', function(g) {
            var $K = kony.$kwebfw$, $KD = $K.dom, $KW = $K.widget,
                _ = this._kwebfw_, prop = _.prop, el = $KW.el(this);

            if(!$KW.interactable(this)) return; /* ---------------------------------------------- */
            if(prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) return;

            if(g.status === 'started') {
                $KD.style(el.scrolee, 'transition', null);
                if($KU.is(_.swipeContext, 'null')) {
                    _.swipeContext = {};
                    _.swipeContext.imageWidth = el.scrolee.firstChild.offsetWidth;
                    _.swipeContext.currentPage = 0;
                }
                //cnutodo need to update swipeContext whenever there is a change in segment width
            }
            if(g.status === 'moving') {
                _translatePage(el.scrolee,  (-(_.swipeContext.imageWidth * _.swipeContext.currentPage) + g.distance.x), 0);
            }
            if(g.status === 'ended') {
                if(g.distance.x <= -7) {
                    _nextPage.call(this, el, _.swipeContext);
                    $KW.fire(this, 'onSwipe', this, {rowIndex:_.swipeContext.currentPage});
                } else if(g.distance.x >= 7) {
                    _previousPage.call(this, el, _.swipeContext);
                    $KW.fire(this, 'onSwipe', this, {rowIndex:_.swipeContext.currentPage});
                } else {
                   _translatePage(el.scrolee,  (-(_.swipeContext.imageWidth * _.swipeContext.currentPage)), 0);
                }
            }
        }, {scope:this});
    };


    //All widget file must have this variable
    //This functions will be called in the scope of widget instance
    var _relayoutActiveTriggerer = {
        SegmentedUI2: function SegmentedUI2$_relayoutActiveTriggerer() {
            return ['data'];
        }
    };


    //All widget file must have this variable
    //This functions will be called in the scope of widget instance
    var _relayoutPassiveTriggerer = {
        SegmentedUI2: function SegmentedUI2$_relayoutPassiveTriggerer() {
            return [];
        }
    };


    //This function will be called in the scope of widget instance
    var _renderRows = function SegmentedUI2$_renderRows(clones) {
        var $K = kony.$kwebfw$, $KD = $K.dom, $KU = $K.utils,
            c = 0, clen = clones.length, fragment = null,
            createRow = function(clone) {
                var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
                    li = $KD.create('LI'), height = '', row = null,
                    index = _deduceIndex.call(this, clone),
                    absIndex = (_absoluteIndex.call(this, index) + 1);

                $KD.setAttr(li, 'kr', 'item'); //NOTE:: This attr/val has high importance
                $KD.setAttr(li, 'kii', index.join(','));
                $KD.setAttr(li, 'kwh-click', 'onRowClick');
                $KD.setAttr(li, 'kwh-keydown', 'onRowKeyDown');
                $KD.setAttr(li, 'kwh-keyup', 'onRowKeyUp');
                $KD.setAttr(li, 'role', 'row');
                $KD.setAttr(li, 'tabindex', -1);
                $KD.setAttr(li, 'aria-colcount', 1);
                $KD.setAttr(li, 'aria-rowindex', absIndex);

                row = clone._render();
                $KD.setAttr(row, 'role', 'gridcell');
                height = clone._kwebfw_.flex.final.height;

                $KW.iterate(clone, function(widget) {
                    var $K = kony.$kwebfw$, $KD = $K.dom,
                        view = widget._kwebfw_.view;

                    if(view) {
                        $KD.setAttr(view, 'aria-colindex', 1);
                        $KD.setAttr(view, 'aria-colcount', 1);
                        $KD.setAttr(view, 'aria-rowindex', absIndex);
                    }
                }, {tabs:false});

                if(height) {
                    $KD.style(row, 'height', '100%');
                    $KD.style(li, 'height', height);
                }

                $KD.add(li, row);

                return li;
            };

        if(clen === 1) {
            fragment = createRow.call(this, clones[0]);
        } else if(clen > 1) {
            fragment = $KD.create();

            for(c=0; c<clen; c++) {
                fragment.appendChild(createRow.call(this, clones[c]));
            }
        }

        return fragment;
    };


    //This function will be called in the scope of widget instance
    var _resetBookKeepers = function SegmentedUI2$_resetBookKeepers(callback) {
        var $K = kony.$kwebfw$, $KU = $K.utils, data = this._kwebfw_.prop.data;

        this._kwebfw_.top = 0; this._kwebfw_.scrollTop = 0; this._kwebfw_.minScroleeHeight = 0;
        this._kwebfw_.topCutOffAbsoluteIndex = 0; this._kwebfw_.bottomCutOffAbsoluteIndex = 0;
        _flushClones(this._kwebfw_.clones);
        this._kwebfw_.rows = []; this._kwebfw_.clones = []; this._kwebfw_.heights = [];

        if(data && data.length > 0 && _shouldLazyLoad.call(this)) {
            if(_isSectionDS(data[0])) {
                $KU.each(data, function(secItem, secIndex) {
                    this._kwebfw_.clones.push([null, []]);
                    this._kwebfw_.heights.push([-1, []]);
                    this._kwebfw_.heights[secIndex][0] = _getRowHeight.call(this, [secIndex, -1]);

                    if($KU.is(callback, 'function')) {
                        callback.call(this, [secIndex, -1]);
                    }

                    $KU.each(secItem[1], function(item, rowIndex) {
                        this._kwebfw_.clones[secIndex][1].push(null);
                        this._kwebfw_.heights[secIndex][1].push(-1);
                        this._kwebfw_.heights[secIndex][1][rowIndex] = _getRowHeight.call(this, [secIndex, rowIndex]);

                        if($KU.is(callback, 'function')) {
                            callback.call(this, [secIndex, rowIndex]);
                        }
                    }, this);
                }, this);
            } else if($KU.is(data[0], 'object')) {
                $KU.each(data, function(item, index) {
                    this._kwebfw_.clones.push(null);
                    this._kwebfw_.heights.push(-1);
                    this._kwebfw_.heights[index] = _getRowHeight.call(this, [-1, index]);

                    if($KU.is(callback, 'function')) {
                        callback.call(this, [-1, index]);
                    }
                }, this);
            }
        }
    };


    //This function will be called in the scope of widget instance
    var _scrollDownWithDeltaRows = function SegmentedUI2$_scrollDownWithDeltaRows(delta, el) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, cloneToAdd = null,
            cloneToRemove = null, deltaForAdd = 0, deltaForRemove = 0, count = 0,
            rowsToBeRemovedFromTop = [], rowsToBeAddedToBottom = [], removeIndex = -1;

        cloneToAdd = _nextRenderableRows.call(this, this._kwebfw_.rows[(this._kwebfw_.rows.length - 1)], 1)[0];
        cloneToRemove = this._kwebfw_.rows[0];

        while((cloneToAdd || cloneToRemove) && (delta > deltaForAdd || delta > deltaForRemove)) {
            if(cloneToAdd && delta > deltaForAdd) {
                deltaForAdd += _getRowHeight.call(this, cloneToAdd);
                rowsToBeAddedToBottom.push(cloneToAdd);
            }

            if(cloneToRemove && delta > deltaForRemove) {
                deltaForRemove += _getRowHeight.call(this, cloneToRemove);
                rowsToBeRemovedFromTop.push(cloneToRemove);
            }

            count++;

            cloneToAdd = _nextRenderableRows.call(this, this._kwebfw_.rows[(this._kwebfw_.rows.length - 1 - count)], 1)[0];
            cloneToRemove = this._kwebfw_.rows[count];
        }

        this._kwebfw_.scrollTop += delta;

        if(_absoluteIndex.call(this, rowsToBeRemovedFromTop[0]) <= this._kwebfw_.topCutOffAbsoluteIndex) {
            removeIndex = rowsToBeRemovedFromTop.length - 1;
            deltaForRemove = _getRowHeight.call(this, rowsToBeRemovedFromTop[removeIndex]);

            while(deltaForRemove <= _extraHeightToRender) {
                removeIndex--;
                deltaForRemove += _getRowHeight.call(this, rowsToBeRemovedFromTop[removeIndex]);
            }

            rowsToBeRemovedFromTop.splice((removeIndex + 1 /* 1 or 0 ??? */), (rowsToBeRemovedFromTop.length - removeIndex + 1 /* 1 or 0 ??? */));
        }

        $KU.each(rowsToBeRemovedFromTop, function(clone) {
            var $K = kony.$kwebfw$, $KD = $K.dom;
            $KD.removeAt(el.scrolee, 0);
            this._kwebfw_.top += _getRowHeight.call(this, clone);
            this._kwebfw_.rows.pop();
        }, this);

        $KU.each(rowsToBeAddedToBottom, function(clone) {
            this._kwebfw_.rows.push(clone);
        }, this);

        $KD.add(el.scrolee, _renderRows.call(this, rowsToBeAddedToBottom));

        el.scrolee.style.paddingTop = this._kwebfw_.top + 'px';
    };


    //This function will be called in the scope of widget instance
    var _scrollReachedBottom = function SegmentedUI2$_scrollReachedBottom() {
        //TODO::
    };


    //This function will be called in the scope of widget instance
    var _scrollReachedTop = function SegmentedUI2$_scrollReachedTop() {
        //TODO::
    };


    //This function will be called in the scope of widget instance
    var _scrollUpWithDeltaRows = function SegmentedUI2$_scrollUpWithDeltaRows(delta, el) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, cloneToAdd = null,
            cloneToRemove = null, deltaForAdd = 0, deltaForRemove = 0, count = 0,
            rowsToBeAddedToTop = [], rowsToBeRemovedFromBottom = [], removeIndex = -1;

        cloneToAdd = _prevRenderableRows.call(this, this._kwebfw_.rows[0], 1)[0];
        cloneToRemove = this._kwebfw_.rows[(this._kwebfw_.rows.length - 1)];

        while((cloneToAdd || cloneToRemove) && (delta > deltaForAdd || delta > deltaForRemove)) {
            if(cloneToAdd && delta > deltaForAdd) {
                deltaForAdd += _getRowHeight.call(this, cloneToAdd);
                rowsToBeAddedToTop.push(cloneToAdd);
            }

            if(cloneToRemove && delta > deltaForRemove) {
                deltaForRemove += _getRowHeight.call(this, cloneToRemove);
                rowsToBeRemovedFromBottom.push(cloneToRemove);
            }

            count++;

            cloneToAdd = _prevRenderableRows.call(this, this._kwebfw_.rows[count], 1)[0];
            cloneToRemove = this._kwebfw_.rows[(this._kwebfw_.rows.length - 1 - count)];
        }

        this._kwebfw_.scrollTop -= delta;

        $KU.each(rowsToBeAddedToTop, function(clone) {
            var $K = kony.$kwebfw$, $KD = $K.dom;

            this._kwebfw_.rows.splice(0, 0, clone);
            this._kwebfw_.top -= _getRowHeight.call(this, clone);
        }, this);

        if(_absoluteIndex.call(this, rowsToBeRemovedFromBottom[0]) >= this._kwebfw_.topCutOffAbsoluteIndex) {
            removeIndex = 0;
            deltaForRemove = _getRowHeight.call(this, rowsToBeRemovedFromBottom[removeIndex]);

            while(deltaForRemove <= _extraHeightToRender) {
                removeIndex++;
                deltaForRemove += _getRowHeight.call(this, rowsToBeRemovedFromBottom[removeIndex]);
            }

            rowsToBeRemovedFromBottom.splice((removeIndex + 1 /* 1 or 0 ??? */), (rowsToBeRemovedFromBottom.length - removeIndex + 1 /* 1 or 0 ??? */));
        }

        count = rowsToBeRemovedFromBottom.length;
        while(count > 0) {
            $KD.remove($KD.last(el.scrolee));
            this._kwebfw_.rows.splice((this._kwebfw_.rows.length-1), 1);
            count--;
        }

        $KD.addAt(el.scrolee, _renderRows.call(this, rowsToBeAddedToTop), 0);

        el.scrolee.style.paddingTop = this._kwebfw_.top + 'px';
    };


    //This function will be called in the scope of widget instance
    var _scrollToRenderNewRows = function SegmentedUI2$_scrollToRenderNewRows(scrollTop, el) {
        var $K = kony.$kwebfw$, $KD = $K.dom, height = 0, clone = null;

        clone = _firstRenderableRow.call(this);
        this._kwebfw_.top = 0;
        this._kwebfw_.rows = [];
        this._kwebfw_.scrollTop = scrollTop;

        //Populate this._kwebfw_.top
        scrollTop -= _extraHeightToRender;
        while(clone && this._kwebfw_.top <= scrollTop) {
            height = _getRowHeight(clone);
            this._kwebfw_.top += height;
            clone = _nextRenderableRows.call(this, clone, 1);
        }

        //Populate this._kwebfw_.rows
        scrollTop = this.frame.height + _extraHeightToRender;
        height = 0;
        while(clone && height <= scrollTop) {
            height += _getRowHeight(clone);
            this._kwebfw_.rows.push(clone);
            clone = _nextRenderableRows.call(this, clone, 1);
        }

        $KD.html(el.scrolee, '');
        $KD.add(el.scrolee, _renderRows.call(this, this._kwebfw_.rows));
        el.scrolee.style.paddingTop = this._kwebfw_.top + 'px';
    };


    var _searcher = {
        SegmentedUI2: {
            //This function will be called in the scope of widget instance
            clearSearchResult: function SegmentedUI2$_searcher_clearSearchResult() {

                _iterateOverData.call(this, this._kwebfw_.clones, function(clone) {
                    var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, li = null;

                    if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                        li = $KD.parent(clone._kwebfw_.view);
                        if(li) {
                            $KD.style(li, 'display', 'block');
                        }
                    }
                });

            },


            convertDateToNumber: function SegmentedUI2$_searcher_convertDateToNumber(dc) {
                var dateNumber, valid = false;

                var isLeapYear = function(year) {
                    var date = new Date(year, 1, 29, 0, 0, 0);
                    return (date.getMonth() === 2) ? false : true;
                };

                var isValidDate = function(date, month, year) {
                    if([1, 3, 5, 7, 8, 10, 12].indexOf(month) != -1
                    && date >= 1 && date <= 31) {
                        return true;
                    } else if([4, 6, 9, 11].indexOf(month) != -1
                    && date >= 1 && date <= 30) {
                        return true;
                    } else if(month === 2 && date >= 1) {
                        if(isLeapYear(year)) {
                            if(date <= 29) return true;
                        } else {
                            if(date <= 28) return true;
                        }
                    } else {
                        return false;
                    }
                };

                if(dc instanceof Array && [0, 3, 6].indexOf(dc.length) >= 0) {
                    if(dc.length === 0) {
                        return 0;
                    }

                    //NOTE:: Calendar widget has valid year range from 1900-2099
                    if(typeof dc[2] === 'number' && dc[2] >= 1900 && dc[2] <= 2099
                    && typeof dc[1] === 'number' && dc[1] >= 1 && dc[1] <= 12
                    && isValidDate(dc[0], dc[1], dc[2])) {
                        valid = true;
                        dateNumber = new Date(dc[2], dc[1], dc[0]).getTime();
                    } else return null;

                    if(valid && dc.length === 6
                    && typeof dc[3] === 'number' && dc[3] >= 0 && dc[3] <= 23
                    && typeof dc[4] === 'number' && dc[4] >= 0 && dc[4] <= 59
                    && typeof dc[5] === 'number' && dc[5] >= 0 && dc[5] <= 59) {
                        dateNumber = new Date(dc[2], dc[1], dc[0], dc[3], dc[4], dc[5]).getTime();
                    }
                } else if(dc === null) {
                    return 0;
                } else return null;

                return dateNumber;
            },


            convertStringToNumber: function SegmentedUI2$_searcher_convertStringToNumber(widgetData){
                var regx = new RegExp(/^\s*((\d+(\.\d+)?)|(\.\d+))\s*$/);

                if(regx.test(widgetData)) {
                    widgetData = parseFloat(widgetData);
                }

                return widgetData;
            },


            //This function will be called in the scope of widget instance
            generateExpression: function SegmentedUI2$_searcher_generateExpression(index, rowData, searchCondition) {
                var $K = kony.$kwebfw$, $KU = $K.utils, matchStr = '';

                $KU.each(searchCondition, function(condition) {
                    var operator = "||";

                    if($KU.is(condition, 'string')) {

                        if(condition === constants.SEGUI_SEARCH_CRITERIA_OPERATOR_AND) {
                            operator = "&&";
                        }
                        matchStr += (' ' + operator + ' ');

                    } else if($KU.is(condition, 'array')) {
                        matchStr += _searcher.SegmentedUI2.generateExpression.call(this, index, rowData, condition);
                    } else {
                        matchStr += _searcher.SegmentedUI2.isOperandPassed.call(this, index, rowData, condition);
                    }
                }, this);

                return ('(' + matchStr + ')');
            },


            //This function will be called in the scope of widget instance
            getFilteredData: function SegmentedUI2$_searcher_getFilteredData() {
                var filterList = [];

                _iterateOverData.call(this, this._kwebfw_.prop.data, function(rowData, rowIndex, secIndex) {
                    var index = null;

                    if(rowIndex !== -1) {
                        if(secIndex === -1) secIndex = 0;
                        index = [secIndex, rowIndex];

                        if(_searcher.SegmentedUI2.isRowMatchingSearchCriteria.call(this, index, rowData)) {
                            filterList.push(index);
                        }
                    }

                });

                return filterList;
            },


            //This function will be called in the scope of widget instance
            getWidgetIdsFromRowData: function SegmentedUI2$_searcher_getWidgetIdsFromRowData(rowData) {
                var rowDataIds = Object.keys(rowData), r = 0,
                    widgetdatamap = this._kwebfw_.prop.widgetDataMap,
                    rlen = rowDataIds.length, widgetId = '',
                    invertedDataMap = {}, widgetIds = [], rowDataId = '';

                for(widgetId in widgetdatamap) {
                    if(widgetdatamap.hasOwnProperty(widgetId)) {
                        rowDataId = widgetdatamap[widgetId];
                        invertedDataMap[rowDataId] = widgetId;
                    }
                }

                for(r = 0; r < rlen; r++) {
                    widgetIds.push(invertedDataMap[rowDataIds[r]]);
                }

                return widgetIds;
            },


            //This utility might not needed if similar function availbe in utils ??
            getValueFromObjectByPath: function SegmentedUI2$_searcher_getValueFromObjectByPath(keys, obj, delimiter) {
                var k=0, klen = 0, ref = null;

                if(!(typeof delimiter === 'string' && delimiter)) {
                    delimiter = '.';
                }

                if(typeof keys === 'string' && keys) {
                    keys = keys.split(delimiter);
                }

                if(keys instanceof Array && typeof obj === 'object' && obj) {
                    ref = obj;
                    klen = keys.length;

                    for(k=0; k<klen; k++) {
                        if(typeof ref === 'object' && ref && ref[keys[k]]) {
                            ref = ref[keys[k]];
                        } else {
                            break;
                        }
                    }
                }

                return ref;
            },


            //This function will be called in the scope of widget instance
            infoOfSearchableWidgetInRow: function SegmentedUI2$_searcher_infoOfSearchableWidgetInRow(index, rowData, rowDataId, widgetId) {
                var _ = this._kwebfw_,  secIndex = index[0], rowIndex = index[1],
                    info = {isValid: false}, cloneModel = null, widget = null, widgetData = null,
                    validWidgetList = ['Button', 'Label', 'TextArea2', 'TextBox2', 'Calendar'];

                if(typeof rowDataId === 'string' && rowDataId
                && typeof widgetId === 'string' && widgetId
                && rowData.hasOwnProperty(rowDataId)) {
                    widgetData = rowData[rowDataId];

                    if(_isSectionDS(_.prop.data[0])) {
                        cloneModel = _.clones[secIndex][1][rowIndex];
                    } else {
                        cloneModel = _.clones[rowIndex];
                    }

                    // clonedModel might be null incase of toplevel flx is invisible.
                    if(!cloneModel) {
                        cloneModel = rowData.template || this.rowTemplate;

                        if(typeof cloneModel === 'string') {
                            cloneModel = _kony.mvc.initializeSubViewController(cloneModel);
                        }
                    }

                    widget = _searcher.SegmentedUI2.getValueFromObjectByPath(widgetId, cloneModel);

                    if(widget) {
                        info.widgetType = widget._kwebfw_.name;

                        if(validWidgetList.indexOf(info.widgetType) !== -1) {
                            info.isValid = true;

                            if(info.widgetType === 'Calendar') {
                                info.widgetText = (widgetData instanceof Array)
                                                ? widgetData : widgetData.dateComponents;
                            } else {
                                info.widgetText = (typeof widgetData === 'string')
                                                ? widgetData : widgetData.text;
                            }
                        }
                    }
                }

                return info;
            },


            //This function will be called in the scope of widget instance
            isOperandPassed: function SegmentedUI2$_searcher_isOperandPassed(index, rowData, searchCondition) {
                var $K = kony.$kwebfw$, $KU = $K.utils, caseSensitive = true,
                    widgetList = null, searchFound = false,
                    searchType = constants.SEGUI_SEARCH_CRITERIA_CONTAINS;

                if(searchCondition.textToSearch) {

                    if($KU.is(searchCondition.caseSensitive, 'boolean')) {
                        caseSensitive = searchCondition.caseSensitive;
                    }

                    if(searchCondition.searchableWidgets) {
                        widgetList = searchCondition.searchableWidgets;
                    } else {
                        widgetList = _searcher.SegmentedUI2.getWidgetIdsFromRowData.call(this, rowData);
                    }

                    if(searchCondition.searchType) searchType = searchCondition.searchType;

                    $KU.each(widgetList, function(widget) {
                        if(_searcher.SegmentedUI2.isOperandPassedByWidgetId.call(
                            this, index, rowData, widget, caseSensitive, searchType, searchCondition.textToSearch)) {
                            searchFound = true;
                            return true;
                        }
                    }, this);

                }

                return searchFound;
            },


            //This function will be called in the scope of widget instance
            isOperandPassedByWidgetId: function SegmentedUI2$_searcher_isOperandPassedByWidgetId(index, rowData, widgetId, caseSensitive, searchType, searchText) {
                var passed = false, rowDataId = this._kwebfw_.prop.widgetDataMap[widgetId],
                    info = _searcher.SegmentedUI2.infoOfSearchableWidgetInRow.call(this, index, rowData, rowDataId, widgetId);

                if(info.isValid) {
                    passed = _searcher.SegmentedUI2.matchWithConditionalOperator(info.widgetType, searchType, searchText, info.widgetText, caseSensitive);
                }

                return passed;
            },


            //This function will be called in the scope of widget instance
            isRowMatchingSearchCriteria: function SegmentedUI2$_searcher_isRowMatchingSearchCriteria(index, rowData) {
                var macthStr = null, searchCondition = this._kwebfw_.searcher.searchCondition;

                macthStr = _searcher.SegmentedUI2.generateExpression.call(this, index, rowData, searchCondition);

                return eval(macthStr);
            },


            matchWithConditionalOperator: function SegmentedUI2$_searcher_matchWithConditionalOperator(widgetType, searchType, searchText, widgetText, caseSensitive) {
                var matched = false, substring = '';

                if(!caseSensitive && typeof searchText === 'string') {
                    searchText = searchText.toUpperCase();
                }

                if(!caseSensitive && typeof widgetText === 'string') {
                    widgetText = widgetText.toUpperCase();
                }

                switch(searchType) {
                    case constants.SEGUI_SEARCH_CRITERIA_CONTAINS :
                        if(typeof widgetText === 'string' && widgetText.indexOf(searchText) !== -1) {
                            matched = true;
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_ENDSWITH :
                        substring = typeof widgetText === 'string'
                                ? widgetText.substr(-searchText.length) : null;

                        if(searchText === substring) matched = true;
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_STARTSWITH :
                        substring = typeof widgetText === 'string'
                                ? widgetText.substr(0, searchText.length) : null;

                        if(searchText === substring) matched = true;
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_GREATER :
                        if(widgetType === 'Calendar' && searchText instanceof Array) {
                            widgetText = _searcher.SegmentedUI2.convertDateToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertDateToNumber(searchText);
                        } else {
                            widgetText = _searcher.SegmentedUI2.convertStringToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertStringToNumber(searchText);
                        }

                        if(typeof widgetText === 'number' && typeof searchText === 'number') {
                            matched = (widgetText > searchText) ? true : false;
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_GREATER_EQUAL :
                        if(widgetType === 'Calendar' && searchText instanceof Array) {
                            widgetText = _searcher.SegmentedUI2.convertDateToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertDateToNumber(searchText);
                        } else {
                            widgetText = _searcher.SegmentedUI2.convertStringToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertStringToNumber(searchText);
                        }

                        if(typeof widgetText === 'number' && typeof searchText === 'number') {
                            matched = (widgetText >= searchText) ? true : false;
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_LESSER :
                        if(widgetType === 'Calendar' && searchText instanceof Array) {
                            widgetText = _searcher.SegmentedUI2.convertDateToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertDateToNumber(searchText);
                        } else {
                            widgetText = _searcher.SegmentedUI2.convertStringToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertStringToNumber(searchText);
                        }

                        if(typeof widgetText === 'number' && typeof searchText === 'number') {
                            matched = (widgetText < searchText) ? true : false;
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_LESSER_EQUAL :
                        if(widgetType === 'Calendar' && searchText instanceof Array) {
                            widgetText = _searcher.SegmentedUI2.convertDateToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertDateToNumber(searchText);

                            if(typeof widgetText === 'number' && typeof searchText === 'number') {
                                matched = (widgetText <= searchText) ? true : false;
                            } else matched = true;
                        } else {
                            widgetText = _searcher.SegmentedUI2.convertStringToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertStringToNumber(searchText);

                            if(typeof widgetText === 'number' && typeof searchText === 'number') {
                                matched = (widgetText <= searchText) ? true : false;
                            }
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_STRICT_EQUAL :
                        if(widgetType === 'Calendar' && searchText instanceof Array) {
                            widgetText = _searcher.SegmentedUI2.convertDateToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertDateToNumber(searchText);

                            if(typeof widgetText === 'number' && typeof searchText === 'number') {
                                matched = (widgetText === searchText) ? true : false;
                            } else matched = true;
                        } else {
                            widgetText = _searcher.SegmentedUI2.convertStringToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertStringToNumber(searchText);

                            matched = (widgetText === searchText) ? true : false;
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_NOT_EQUAL :
                        if(widgetType === 'Calendar' && searchText instanceof Array) {
                            widgetText = _searcher.SegmentedUI2.convertDateToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertDateToNumber(searchText);

                            if(typeof widgetText === 'number' && typeof searchText === 'number') {
                                matched = (widgetText === searchText) ? true : false;
                            } else matched = true;
                        } else {
                            widgetText = _searcher.SegmentedUI2.convertStringToNumber(widgetText);
                            searchText = _searcher.SegmentedUI2.convertStringToNumber(searchText);

                            matched = (widgetText !== searchText) ? true : false;
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_NOT_CONTAINS :
                        if(typeof widgetText === 'string'
                        && widgetText.indexOf(searchText) === -1) {
                            matched = true;
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_NOT_ENDSWITH :
                        substring = typeof widgetText === 'string' ?
                                    widgetText.substr(-searchText.length) : null;

                        if(searchText !== substring) {
                            matched = true;
                        }
                    break;

                    case constants.SEGUI_SEARCH_CRITERIA_NOT_STARTSWITH :
                        substring = typeof widgetText === 'string' ?
                                    widgetText.substr(searchText.length) : null;

                        if(searchText !== substring) {
                            matched = true;
                        }
                    break;
                }

                return matched;
            },

             //This function will be called in the scope of widget instance
             removeAtHeaderVisibility: function SegmentedUI2$_searcher_removeAtHeaderVisibility(secIndex) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, _ = this._kwebfw_,
                    clones = _.clones[secIndex], found = false;

                if(_.searcher && _.searcher.config.updateSegment && _.searcher.config.showSectionHeaderFooter) {

                    $KU.each(clones[1], function (clone) {
                        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom;

                        if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                            li = $KD.parent(clone._kwebfw_.view);

                            if(li && $KD.style(li, 'display') === 'block') {
                                found = true;
                                return true;
                            }
                        }
                    });

                    // header visibility change
                    if(!$KU.is(clones[0], 'null') && clones[0]._kwebfw_.view) {
                        li = $KD.parent(clones[0]._kwebfw_.view);

                        if(found) {
                            display = 'block';
                        } else {
                            display = 'none';
                        }

                        if(li) $KD.style(li, 'display', display);
                    }
                }
             },


            //This function will be called in the scope of widget instance
            searchText: function SegmentedUI2$_searcher_searchText() {
                var $K = kony.$kwebfw$, $KU = $K.utils, _ = this._kwebfw_,
                    config = _.searcher.config, sections = [],
                    filterList = _searcher.SegmentedUI2.getFilteredData.call(this);

                if(config.updateSegment) {
                    _searcher.SegmentedUI2.setResultOnView.call(this, filterList);

                    if(_isSectionDS(_.prop.data[0])) {
                        $KU.each(this._kwebfw_.clones, function (record, index) {
                            sections.push(index);
                         });
                        _searcher.SegmentedUI2.updateHeaderVisibility.call(this, sections, filterList);
                    }

                }

                return filterList;
            },


            //All the functions will be called in the scope of widget instance
            setResultOnView: function SegmentedUI2$_searcher_setResultOnView(filteredResult) {

                _iterateOverData.call(this, this._kwebfw_.clones, function(clone, rowIndex, secIndex) {
                    var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, display = 'block';

                    if(rowIndex !== -1) {
                        if(secIndex === -1) secIndex = 0;

                        if(_getIndex([secIndex, rowIndex], filteredResult) === -1) {
                            display = 'none';
                        }

                        if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                            li = $KD.parent(clone._kwebfw_.view);
                            if(li) {
                                $KD.style(li, 'display', display);
                            }
                        }
                    }
                });
            },

            updateHeaderVisibility: function SegmentedUI2$_searcher_updateHeaderVisibility(sections, filteredResult) {
                var $K = kony.$kwebfw$, $KU = $K.utils, _ = this._kwebfw_,
                    config = _.searcher.config;

                var findSecIndex = function (find, list) {
                    var position = -1, i = 0, ilen = list.length;

                    for(i=0; i<ilen; i++) {
                        if(list[i][0] === find) {
                            position = i;
                            break;
                        }
                    }

                    return position;
                };

                $KU.each(this._kwebfw_.clones, function (clones, secIndex) {
                    var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                        li = null,  display = 'none', clone = clones[0];

                    if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                        if(sections.indexOf(secIndex) !== -1) {
                            li = $KD.parent(clone._kwebfw_.view);
                            if(config.showSectionHeaderFooter && findSecIndex(secIndex, filteredResult) !== -1) {
                                display = 'block';
                            }

                            if(li) $KD.style(li, 'display', display);
                        }
                    }
                });
            },


            //All the functions will be called in the scope of widget instance
            updateSearchText: function SegmentedUI2$_searcher_updateSearchText(clones, section) {
                var $K = kony.$kwebfw$, $KU = $K.utils, _ = this._kwebfw_, sections = [],
                    updateFilterList = [];

                var updateNodeVisibility = function(segmodel, clone, index, widgetdata) {
                    var $K = kony.$kwebfw$, $KD = $K.dom, display = 'none',
                        li = $KD.parent(clone._kwebfw_.view);

                    if(_searcher.SegmentedUI2.isRowMatchingSearchCriteria.call(segmodel, index, widgetdata)) {
                        updateFilterList.push(index);
                        display = 'block';
                    }

                    if(li) $KD.style(li, 'display', display);
                };

                if(_.searcher && _.searcher.config.updateSegment) {
                    $KU.each(clones, function(clone) {
                        var prop = this._kwebfw_.prop, data = null,
                            cindex = clone._kwebfw_.ii.split(','),
                            secIndex = parseInt(cindex[0], 10),
                            rowIndex = parseInt(cindex[1], 10);

                        if(rowIndex !== -1) {

                            if(section) {
                                sections.push(secIndex);
                                data = prop.data[secIndex][1][rowIndex];
                                updateNodeVisibility(this, clone, [secIndex, rowIndex], data);
                            } else {
                                data = prop.data[rowIndex];
                                updateNodeVisibility(this, clone, [0, rowIndex], data);
                            }
                        }

                    }, this);

                    if(section) {
                        _searcher.SegmentedUI2.updateHeaderVisibility.call(this, sections, updateFilterList);
                    }
                }
            }
        }
    };


    //All the functions will be called in the scope of widget instance
    var _setBehaviorConfig = function SegmentedUI2$_setBehaviorConfig(widget) {
        var $K = kony.$kwebfw$, $KU = $K.utils, prop = this._kwebfw_.prop,
            imgId = null, img = null, selImg = null, unSelImg = null;

        if(prop.selectionBehaviorConfig && prop.selectionBehavior !== constants.SEGUI_DEFAULT_BEHAVIOR) {
            imgId = prop.selectionBehaviorConfig.imageIdentifier;
            selImg = prop.selectionBehaviorConfig.selectedStateImage;
            unSelImg = prop.selectionBehaviorConfig.unselectedStateImage;

            if(widget.id === imgId  && $KU.is(widget, 'widget', 'Image2')) {
                img = unSelImg;
                $KU.each(this._kwebfw_.selectedRows, function (row) {
                    if(row.join(',') === widget._kwebfw_.ii) {
                        img = selImg;
                        return true;
                    }
                });

                widget._kwebfw_.prop.src = img;
            }
        }

    };


    //All the functions will be called in the scope of widget instance
    var _setPageView = function SegmentedUI2$_setPageView(el) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
            _ = this._kwebfw_,prop = _.prop, rows = _.rows, rowIndex = 0,
            datalen = rows.length;

        var navigationDotsHandler = function(event) {
            var rowIndex = event.target.getAttribute('index');

            rowIndex = parseInt(rowIndex, 10);
            _setPageViewIndicator.call(this, el, rowIndex);
        };


        $KD.setAttr(el.scrolee, 'kv', 'pageview');
        $KD.html(el.pageNav, '');

        if(datalen > 0) {

            $KU.each(rows, function(clone, index) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                    _ = this._kwebfw_, prop = _.prop, img = null,
                    src = prop.pageOffDotImage, li = null;

                img = $KD.create('IMG', {
                    loading: 'lazy',
                    onmousedown: 'return false;'
                });
                $KD.setAttr(img, 'src', $KU.getImageURL(src));
                $KD.setAttr(img, 'alt', '');
                $KD.setAttr(img, 'index', index);
                $KD.style(img, 'padding-left', '4px');
                $KD.on(img, 'click', 'image', navigationDotsHandler.bind(this));
                $KD.add(el.pageNav, img);

                if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                    li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');
                    if(li) {
                        $KD.style(li, 'width', (100/datalen) + '%');
                        _applyRowSeparator.call(this, li, clone);
                        _applyRowAndHeaderSkin.call(this, li, clone, index);
                    }
                }

            }, this);
            $KD.style(el.scrolee, 'width', (datalen * 100) + '%');

            if(_.swipeContext && _.swipeContext.currentPage) {
                rowIndex = _.swipeContext.currentPage;
            } else if(prop.selectedRowIndex && prop.selectedRowIndex.length === 2) {
                rowIndex = prop.selectedRowIndex[1];
            }
            _setPageViewIndicator.call(this, el, rowIndex);
        }

        if(prop.needPageIndicator) $KD.style(el.pageNav, 'display', 'block');
    };


    //All the functions will be called in the scope of widget instance
    var _setPageViewIndicator = function SegmentedUI2$_setPageViewIndicator(el, rowIndex) {
        var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom, _ = this._kwebfw_,
            rows = _.rows, value = 0;

        if(el.scrolee) {
            datalen = rows.length;
            value = -(rowIndex * (100/datalen));
            $KD.style(el.scrolee, 'transition', 'transform 0.5s ease 0s');
            $KD.style(el.scrolee, 'transform', 'translate3d(' + value + '%, 0, 0)');

            if(!$KU.is(_.swipeContext, 'null')) {
                _.swipeContext.currentPage = rowIndex;
            } else {
                _.swipeContext = {};
                _.swipeContext.imageWidth = el.scrolee.firstChild.offsetWidth;
                _.swipeContext.currentPage = rowIndex;
            }

            $KU.each($KD.children(el.pageNav), function(img, index) {
                var $K = kony.$kwebfw$, $KU = $K.utils,
                    src = null, prop = this._kwebfw_.prop;

                src = (rowIndex === index) ? prop.pageOnDotImage : prop.pageOffDotImage;

                if(img.src && img.src.substring(img.src.lastIndexOf("/") + 1) !== src) {
                img.src = $KU.getImageURL(src);
                }
            }, this);
        }
    };


    //All the functions will be called in the scope of widget instance
    var _setTableView = function SegmentedUI2$_setTableView(el) {
        var $K = kony.$kwebfw$, $KD = $K.dom, $KU = $K.utils,
            rows = this._kwebfw_.rows;

        if(rows.length > 0) {
            $KU.each(rows, function(clone, index) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                    li = null;

                if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                    li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');
                    if(li) {
                        $KD.style(li, 'width', null);
                        _applyRowSeparator.call(this, li, clone);
                        _applyRowAndHeaderSkin.call(this, li, clone, index);
                    }
                }

            }, this);
        }

        $KD.html(el.pageNav, '');
        $KD.style(el.pageNav, 'display', 'none');
        $KD.setAttr(el.scrolee, 'kv', 'tableview');
        $KD.style(el.scrolee, {'transition': null, 'transform': null, 'width': null});
    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    var _setter = {
        SegmentedUI2: {
            data: function SegmentedUI2$_setter_data(old) {
                _onDataSet.call(this);
            },

            selectedRowIndex: function SegmentedUI2$_setter_selectedRowIndex(old) {
                var _ = this._kwebfw_, prop = _.prop, index = -1, deSelectedRow = [];

                if(!_isSectionDS(prop.data[0])) {
                    prop.selectedRowIndex[0] = -1;
                }

                if(!prop.selectedRowIndex) {
                    deSelectedRow = _.selectedRows.splice(0, _.selectedRows.length);
                    if(prop.selectionBehavior !== constants.SEGUI_DEFAULT_BEHAVIOR) {
                        _updateBehaviorImgs.call(this, false, deSelectedRow);
                    }
                } else if(prop.viewType !== constants.SEGUI_VIEW_TYPE_TABLEVIEW
                || prop.selectionBehavior === constants.SEGUI_DEFAULT_BEHAVIOR) {
                    _.selectedRows.splice(0, _.selectedRows.length, prop.selectedRowIndex.slice(0));
                } else if(prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) {
                    if(prop.selectionBehavior === constants.SEGUI_SINGLE_SELECT_BEHAVIOR) {
                        deSelectedRow = _.selectedRows.splice(0, _.selectedRows.length, prop.selectedRowIndex.slice(0));
                        _updateBehaviorImgs.call(this, false, [deSelectedRow], [prop.selectedRowIndex.slice(0)]);
                    } else if(prop.selectionBehavior === constants.SEGUI_MULTI_SELECT_BEHAVIOR) {
                        index = _getIndex(prop.selectedRowIndex, _.selectedRows);

                        if(index !== -1) {
                            deSelectedRow = _.selectedRows.splice(index, 1);
                            _updateBehaviorImgs.call(this, false, [deSelectedRow]);
                        } else {
                            _.selectedRows.push(prop.selectedRowIndex.slice(0));
                            _updateBehaviorImgs.call(this, false, [deSelectedRow], [prop.selectedRowIndex.slice(0)]);
                        }
                    }
                }

                _setSelectedRowsRelatedProperties.call(this);
            },

            selectedRowIndices: function SegmentedUI2$_setter_selectedRowIndices(old) {
                var prop = this._kwebfw_.prop, rows = this._kwebfw_.selectedRows,
                    rowIndexes = null, s = 0, slen = 0, r = 0, rlen = 0,
                    deSelectedRows = rows.splice(0, rows.length),
                    rowIndices = prop.selectedRowIndices;

                if(rowIndices) {
                    slen = rowIndices.length;

                    for(s=0; s<slen; s++) {
                        rowIndexes = rowIndices[s][1];
                        rlen = rowIndexes.length;

                        for(r=0; r<rlen; r++) {
                            rows.push([rowIndices[s][0], rowIndexes[r]]);
                        }
                    }
                }
                _updateBehaviorImgs.call(this, false, deSelectedRows, rows);
                _setSelectedRowsRelatedProperties.call(this);
            },

            widgetDataMap: function SegmentedUI2$_setter_widgetDataMap(old) {
                this._kwebfw_.invertedDataMap = _getInvertedDataMap(old);
            }
        }
    };


    //This function will be called in the scope of widget instance
    var _setSelectedRowsRelatedProperties = function SegmentedUI2$_setSelectedRowsRelatedProperties() {
        var prop = this._kwebfw_.prop, rows = this._kwebfw_.selectedRows,
            section = false, indices = {}, r = 0,
            rlen = rows.length, sindex = -1, rindex = -1, key = '';

        prop.selectedRowItems = [];

        if(prop.data && prop.data.length > 0) {
            section = _isSectionDS(prop.data[0]);
            prop.selectedRowIndex = (!rlen) ? null : rows[(rlen-1)].slice(0);
            prop.selectedRowIndices = (!rlen) ? null : [];

            for(r=0; r<rlen; r++) {
                sindex = rows[r][0];
                rindex = rows[r][1];

                if(!section) {
                    if(sindex === 0) sindex = -1;
                    prop.selectedRowItems.push(prop.data[rindex]);
                } else {
                    prop.selectedRowItems.push(prop.data[sindex][1][rindex]);
                }

                key = sindex.toString();
                if(!indices.hasOwnProperty(key)) {
                    indices[key] = [];
                }
                indices[key].push(rindex);
            }

            for(key in indices) {
                prop.selectedRowIndices.push([parseInt(key, 10), indices[key]]);
            }
        } else {
            prop.selectedRowIndex = null;
            prop.selectedRowIndices = null;
            rows.splice(0, rlen);
        }
    };


    //This function will be called in the scope of widget instance
    var _shouldLazyLoad = function SegmentedUI2$_shouldLazyLoad() {
        var $K = kony.$kwebfw$, $KW = $K.widget, flag = false;

        if($KW.isFixedHeight(this)
        && this._kwebfw_.prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) {
            flag = true;
        }

        return false; //TODO:: return flag;
    };


    var _translatePage = function SegmentedUI2$_translatePage(el, distance, duration) {
        var $K = kony.$kwebfw$, $KD = $K.dom;

        //cnutodo handle duration

        var transformObj = kony.ui.makeAffineTransform();
        transformObj.translate(distance, 0);
        $KD.style(el, 'transform', transformObj.transform.translate);
    };


    //This function will be called in the scope of widget instance
    var _validateInputIndices = function SegmentedUI2$_validateInputIndices(secIndex, rowIndex, action) {
        var $K = kony.$kwebfw$, $KU = $K.utils, index = -1,
            data = this._kwebfw_.prop.data, rowIndexBoundary = 0,
            errorMessage = '', secIndexBoundary = 0;

        if(!$KU.is(rowIndex, 'number')) {
            errorMessage = 'Invalid row Index.';
        }

        if(!$KU.is(secIndex, 'undefined') && !$KU.is(secIndex, 'number')) {
            errorMessage = 'Invalid section Index.';
        }

        if(data && data.length > 0) {
            if(_isSectionDS(data[0])) {
                index = _deduceIndex.call(this, secIndex+','+rowIndex);
                secIndexBoundary = data.length;

                if(action === 'addsectionat') secIndexBoundary = secIndexBoundary + 1;

                if(index[0] === -1 || index[0] >= secIndexBoundary) {
                    errorMessage = 'Invalid section index.';
                } else if(['add', 'update', 'remove'].indexOf(action) !== -1) {
                    rowIndexBoundary = data[secIndex][1].length;
                    if(action === 'add') rowIndexBoundary = rowIndexBoundary + 1;
                }

            } else  {

                if(!$KU.is(secIndex, 'undefined') && secIndex !== 0) {
                    errorMessage = 'Invalid section index.';
                }

                index = _deduceIndex.call(this, '-1,'+rowIndex);
                rowIndexBoundary = data.length;
                if(action === 'add') rowIndexBoundary = rowIndexBoundary + 1;
            }

            if(action !== 'addsectionat' && errorMessage === ''
            && (index[1] === -1 || index[1] >= rowIndexBoundary)) {
                errorMessage = 'Invalid row index.';
            }

        } else {
            if(action !== 'add' && action !== 'addsectionat') {
                errorMessage = 'No data exists.';
            } else if(action === 'addsectionat' && secIndex !== 0) {
                // addsectionat action secIndex rather than ZERO not allowed
                errorMessage = 'Invalid section index.';
            } else if(action === 'add') {
                if(rowIndex !== 0) {
                    // add action rowIndex rather than ZERO not allowed
                    errorMessage = 'Invalid row index.';
                } else if(!$KU.is(secIndex, 'undefined') && secIndex !== 0) {
                    // add action secIndex should be either -1 or undefined
                    errorMessage = 'Invalid section index.';
                }
            }
        }

        return errorMessage;
    };


    //All the functions will be called in the scope of widget instance
    var _updateBehaviorImgs = function SegmentedUI2$_updateBehaviorImgs(deSelectedAll, deSelectedRows, selectedRows) {
        var $K = kony.$kwebfw$, $KU = $K.utils, imgId = null,
            _ = this._kwebfw_, prop = _.prop, rows = _.rows,
            deSelectedIndexes = [], selectedIndexes = [];

        var joinRowIndexAsString = function(fromArray, toArray) {
            $KU.each(fromArray, function(row) {
                toArray.push(row.join(','));
            });
        };

        var updateImage = function(clone, imgId, imgUrl) {
            var $K = kony.$kwebfw$, $KW = $K.widget, len = 0,
                imgPath = imgId.split("."), containerId = '';

            if(imgPath.length === 1) {
                imgId = imgPath[0];
                containerId = clone.id;
            } else { // incase the image is inside component
                len = imgPath.length;
                imgId = imgPath[len - 1];
                containerId = imgPath[len - 2];
            }

            if(clone) {
                $KW.iterate(clone, function (widget) {
                    var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
                        container = null, el = null;

                    if($KU.is(widget, 'widget', 'Image2')) {
                        if(widget.id === imgId) {
                            container = $KW.rmodel(widget);
                            if(container.id === containerId) {
                                widget._kwebfw_.prop.src = imgUrl;
                                el = $KW.el(widget);
                                if(el && el.image) el.image.src = $KU.getImageURL(imgUrl);
                            }
                        }
                    }
                }, {tabs:false});
            }
        };

        if(prop.selectionBehaviorConfig && prop.selectionBehavior !== constants.SEGUI_DEFAULT_BEHAVIOR) {
            imgId = prop.selectionBehaviorConfig.imageIdentifier;
            selImg = prop.selectionBehaviorConfig.selectedStateImage;
            unSelImg = prop.selectionBehaviorConfig.unselectedStateImage;

            if(deSelectedAll) {
                _iterateOverData.call(this, _.clones, function(clone) {
                    updateImage(clone, imgId, unSelImg);
                });
            } else {
                joinRowIndexAsString(selectedRows, selectedIndexes);
                joinRowIndexAsString(deSelectedRows, deSelectedIndexes);
                $KU.each(rows, function(clone) {
                    if(selectedIndexes.indexOf(clone._kwebfw_.ii) !== -1) {
                        updateImage(clone, imgId, selImg);
                    } else if(deSelectedIndexes.indexOf(clone._kwebfw_.ii) !== -1) {
                        updateImage(clone, imgId, unSelImg);
                    }
                });
            }
        }
    };


    //All the functions will be called in the scope of widget instance
    var _updateSelectionBehaviorConfig = function SegmentedUI2$_updateSelectionBehaviorConfig() {
        var $K = kony.$kwebfw$, $KU = $K.utils, _ = this._kwebfw_, imgId = null,
            prop = _.prop, rows = _.rows, unSelImg = null, imgPath = [], containerId = '',
            widgetDataMap = prop.widgetDataMap, len = 0;

        if(prop.selectionBehaviorConfig && prop.selectionBehavior !== constants.SEGUI_DEFAULT_BEHAVIOR) {
            imgId = prop.selectionBehaviorConfig.imageIdentifier;
            selImg = prop.selectionBehaviorConfig.selectedStateImage;
            unSelImg = prop.selectionBehaviorConfig.unselectedStateImage;
            imgPath = imgId.split(".");

            if(imgPath.length == 1) {
                imgId = imgPath[0];
                containerId = this.rowTemplate.id;
            } else { // incase the image is inside component
                len = imgPath.length;
                imgId = imgPath[len - 1];
                containerId = imgPath[len - 2];
            }
        }

        $KU.each(rows, function(clone) {
            var $K = kony.$kwebfw$, $KW = $K.widget;

            $KW.iterate(clone, function(widget) {
                var $K = kony.$kwebfw$, $KU = $K.utils, index = null,
                    secIndex = -1, rowIndex = -1, item = null, itemimg = null,
                    dataId = null, data = this._kwebfw_.prop.data,
                    img = unSelImg, container = null, el = null;

                    if($KU.is(widget, 'widget', 'Image2')) {
                        if(widget.id === imgId && prop.selectionBehavior !== constants.SEGUI_DEFAULT_BEHAVIOR) {
                            container = $KW.rmodel(widget);
                            if(container.id === containerId) {
                                $KU.each(_.selectedRows, function (row) {
                                    if(row === widget._kwebfw_.ii) {
                                        img = selImg;
                                        return true;
                                    }
                                });
                            }
                        } else  {
                            index = _deduceIndex.call(this, widget);
                            secIndex = index[0];
                            rowIndex = index[1];
                            if(secIndex === -1) item = data[rowIndex];
                            else item = data[secIndex][1][rowIndex];

                            dataId = widgetDataMap[widget.id];

                            if($KU.is(dataId, 'string') && dataId
                            && item.hasOwnProperty(dataId)) {
                                itemimg = item[dataId];
                            }

                            if($KU.is(itemimg, 'undefined')) {
                                tpl = item.template || this.rowTemplate;
                                tpl = $KW.getTemplate(this, tpl);
                                $KU.iterate(tpl, function(tplwidget) {
                                    if(tplwidget.id === widget.id) {
                                        img = tplwidget.src;
                                        return true;
                                    }
                                }, {tabs:false});
                            } else {
                                if($KU.is(itemimg, 'string')) {
                                    img = itemimg;
                                } else if($KU.is(itemimg, 'object')) {
                                    $KU.each(itemimg, function(value, key) {
                                        if(key === 'src') {
                                            img = value;
                                            return true;
                                        }
                                    });
                                }
                            }
                        }

                        widget._kwebfw_.prop.src = img;
                        el = $KW.el(widget);
                        if(el && el.image) el.image.src = $KU.getImageURL(img);
                    }
            }, {scope:this, tabs:false});
        }, this);

    };


    //All the functions will be called in the scope of widget instance
    var _updateSelectionBehavior = function SegmentedUI2$_updateSelectionBehavior() {
        _clearSelectedIndices.call(this);
        if(this._kwebfw_.prop.selectionBehavior !== constants.SEGUI_DEFAULT_BEHAVIOR) {
            _updateBehaviorImgs.call(this, true);
        } else {
            _updateSelectionBehaviorConfig.call(this);
        }

    }


    //This function will be called in the scope of widget instance
    var _updateBottomCutOffAbsoluteIndex = function SegmentedUI2$_updateBottomCutOffAbsoluteIndex() {
        var height = 0, clone = null;

        clone = _lastRenderableRow.call(this);

        while(clone && height < _extraHeightToRender) {
            height += _getRowHeight.call(this, clone);

            if(height < _extraHeightToRender) {
                clone = _prevRenderableRows.call(this, clone, 1)[0];
            }
        }

        this._kwebfw_.bottomCutOffAbsoluteIndex = _absoluteIndex.call(this, clone);
    };


    //This function will be called in the scope of widget instance
    var _updateIndexes = function SegmentedUI2$_updateIndexes(fromIndex, ofSectionIndex) {
        var $K = kony.$kwebfw$, $KW = $K.widget, $KD = $K.dom,
            prop = this._kwebfw_.prop, data = prop.data, r = 0, rlen = 0,
            clones = this._kwebfw_.clones, s = 0, slen = 0, el = null;

        var updateIndex = function(segmentmodel, clone, index) {
            var $K = kony.$kwebfw$, $KW = $K.widget, $KU = $K.utils,
                $KD = $K.dom, absIndex = null, li = null;

            if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');

                if(li) {
                    absIndex = (_absoluteIndex.call(segmentmodel, index) + 1);
                    $KD.setAttr(li, 'kii', index);
                    $KD.setAttr(li, 'aria-rowindex', absIndex);
                }
            }

            $KW.iterate(clone, function(model) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                    $KW = $K.widget;

                model._kwebfw_.ii = index;

                if($KU.is(model._kwebfw_.view, 'dom')) {
                    $KD.setAttr(model._kwebfw_.view, 'kwi', index);
                    $KW.replaceWAPIndex(model, index);
                    $KD.setAttr(model._kwebfw_.view, 'kwp', model._kwebfw_.wap);
                }
            }, {tabs:false});
        };

        if(data) {
            if(_isSectionDS(data[0])) {
                slen = clones.length;
                if(ofSectionIndex === -1) { //Section has changed
                    //Change index of items[s][0] and items[s][1][r]
                    for(s=fromIndex; s<slen; s++) {
                        updateIndex(this, clones[s][0], (s + ',-1'));

                        rlen = clones[s][1].length;
                        for(r=0; r<rlen; r++) {
                            updateIndex(this, clones[s][1][r], (s + ',' + r));
                        }
                    }
                } else { //Row of a particular section has changed
                    //Change index of items[r]
                    clones = clones[ofSectionIndex][1];

                    rlen = clones.length;
                    for(r=fromIndex; r<rlen; r++) {
                        updateIndex(this, clones[r], (ofSectionIndex + ',' + r));
                    }
                }
            } else { //Row of a non-sectionable segment has changed
                //Change index of items[r]
                rlen = clones.length;
                for(r=fromIndex; r<rlen; r++) {
                    updateIndex(this, clones[r], ('-1,' + r));
                }
            }

            el = $KW.el(this);
            $KD.setAttr(el.scrolee, 'aria-rowcount', _getRowCount.call(this));
        }
    };


    //This function will be called in the scope of widget instance
     var _updateSectionHeaderSkin = function SegmentedUI2$_updateSectionHeaderSkin() {
         var $K = kony.$kwebfw$, $KU = $K.utils, headerRows = [];

         $KU.each(this._kwebfw_.clones, function (record, index) {
            headerRows.push(record[0]);
         });

        $KU.each(headerRows, function (clone, index) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                li = null;

            if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');
            }

            if(li) {
                _applyRowAndHeaderSkin.call(this, li, clone, index);
            }
        }, this);
     };


    //This function will be called in the scope of widget instance
    var _updateRowSkin = function SegmentedUI2$_updateRowSkin() {
        var $K = kony.$kwebfw$, $KU = $K.utils,
            prop = this._kwebfw_.prop;

        if(prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) {
            $KU.each(this._kwebfw_.rows, function (clone, index) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                    li = null;

                if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                    li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');
                }

                if(li) {
                    _applyRowAndHeaderSkin.call(this, li, clone, index);
                }
            }, this);
        }
    };


    //This function will be called in the scope of widget instance
    var _updateSeparator = function SegmentedUI2$_updateSeparator() {
        var $K = kony.$kwebfw$, $KU = $K.utils,
            prop = this._kwebfw_.prop;

        if(prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) {
            $KU.each(this._kwebfw_.rows, function(clone, index) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                    li = null;

                 if(!$KU.is(clone, 'null') && clone._kwebfw_.view) {
                     li = $KD.closest(clone._kwebfw_.view, 'kr', 'item');
                 }

                if(li) {
                    _applyRowSeparator.call(this, li, index);
                }
            }, this);
        }
    };


    //This function will be called in the scope of widget instance
    var _updateSpecialProperties = function SegmentedUI2$_updateSpecialProperties(widget) {
        widget._kwebfw_.oid  = this._kwebfw_.uid;
        widget._kwebfw_.wap  = this._kwebfw_.wap + ('[' + widget._kwebfw_.ii + ']_') + widget._kwebfw_.wap;
    };


    //This function will be called in the scope of widget instance
    var _updateTopCutOffAbsoluteIndex = function SegmentedUI2$_updateTopCutOffAbsoluteIndex() {
        var height = 0, clone = null;

        clone = _firstRenderableRow.call(this);

        while(clone && height < _extraHeightToRender) {
            height += _getRowHeight.call(this, clone);

            if(height < _extraHeightToRender) {
                clone = _nextRenderableRows.call(this, clone, 1)[0];
            }
        }

        this._kwebfw_.topCutOffAbsoluteIndex = _absoluteIndex.call(this, clone);
    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    //These function should always return a boolean value
    var _valid = {
        SegmentedUI2: {
            alternateRowSkin: function SegmentedUI2$_valid_alternateRowSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            },

            bounces: function SegmentedUI2$_valid_bounces(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            contentOffset: function SegmentedUI2$_valid_contentOffset(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                && $KU.is(value.x, 'size')
                && $KU.is(value.y, 'size')) {
                    flag = true;
                }

                return flag;
            },

            contentOffsetMeasured: function SegmentedUI2$_valid_contentOffsetMeasured() {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                && $KU.is(value.x, 'integer') && value.x >= 0
                && $KU.is(value.y, 'integer') && value.y >= 0) {
                    flag = true;
                }

                return flag;
            },

            data: function SegmentedUI2$_valid_data(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'array') || $KU.is(value, 'null')) {
                    flag = true;
                }

                return flag;
            },

            dockSectionHeaders: function SegmentedUI2$_valid_dockSectionHeaders(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            needPageIndicator: function SegmentedUI2$_valid_needPageIndicator(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            onRowClick: function SegmentedUI2$_valid_onRowClick(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'function') || $KU.is(value, 'null')) {
                    flag = true;
                }

                if(!flag && $K.F.EIWP) {
                    if($KU.is(value, 'undefined')) {
                        flag = [null, true];
                    }
                }

                return flag;
            },

            onRowDisplay: function SegmentedUI2$_valid_onRowDisplay(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'function') || $KU.is(value, 'null')) {
                    flag = true;
                }

                return flag;
            },


            onSwipe: function SegmentedUI2$_valid_onSwipe(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'function') || $KU.is(value, 'null')) {
                    flag = true;
                }

                if(!flag && $K.F.EIWP) {
                    if($KU.is(value, 'undefined')) {
                        flag = [null, true];
                    }
                }

                return flag;
            },

            pageOffDotImage: function SegmentedUI2$_valid_pageOffDotImage(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'string')) {
                    flag = true;
                }

                return flag;
            },

            pageOnDotImage: function SegmentedUI2$_valid_pageOnDotImage(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'string')) {
                    flag = true;
                }

                return flag;
            },

            retainScrollPositionMode: function SegmentedUI2$_valid_retainScrollPositionMode(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false,
                    options = [constants.SEGUI_SCROLL_POSITION_DEFAULT, constants.SEGUI_SCROLL_POSITION_RETAIN,
                        constants.SEGUI_SCROLL_POSITION_TOP];

                if(options.indexOf(value) >= 0) {
                    flag = true;
                }

                return flag;
            },

            retainSelection: function SegmentedUI2$_valid_retainSelection(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            rowFocusSkin: function SegmentedUI2$_valid_rowFocusSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            },

            rowSkin: function SegmentedUI2$_valid_rowSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            },

            rowTemplate: function SegmentedUI2$_valid_rowTemplate(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'widget', 'FlexContainer') || $KU.is(value, 'string') || value === null) {
                    flag = true;
                }

                return flag;
            },

            scrollingEvents: function SegmentUI2$_valid_scrollingEvents(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false, subflag = true,
                    names = ['onPush', 'onPull', 'onReachingBegining', 'onReachingEnd'];

                if($KU.is(value, 'null')) {
                    flag = true;
                } else if($KU.is(value, 'object')) {
                    flag = true;

                    $KU.each(names, function(name) {
                        var $K = kony.$kwebfw$, $KU = $K.utils;

                        if(value.hasOwnProperty(name)
                        && !$KU.is(value[name], 'function')) {
                            subflag = false;
                            return true;
                        }
                    });
                }

                return (flag && subflag);
            },

            sectionHeaderSkin: function SegmentedUI2$_valid_sectionHeaderSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            },

            sectionHeaderTemplate: function SegmentedUI2$_valid_sectionHeaderTemplate(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'widget', 'FlexContainer') || $KU.is(value, 'string') || value === null) {
                    flag = true;
                }

                return flag;
            },

            selectedRowIndex: function SegmentedUI2$_valid_selectedRowIndex(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false,
                    prop = this._kwebfw_.prop;

                if($KU.is(value, 'array') || $KU.is(value, 'null')) {
                    flag = true;
                }

                if($KU.is(value, 'array') && prop.hasOwnProperty('data')
                && prop.hasOwnProperty('selectedRowIndex')) {
                    flag = _validIndex.call(this, value.slice(0), true);
                }

                return flag;
            },

            selectedRowIndices: function SegmentedUI2$_valid_selectedRowIndices(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, count = 0, flag = true,
                    s = 0, slen = 0, r = 0, rlen = 0, _ = this._kwebfw_,
                    prop = _.prop, secIndex = -1, rowIndexes = [];

                if($KU.is(value, 'array')) {
                    slen = value.length;
                    for(s=0; s<slen; s++) {
                        if(flag === false) {
                            break;
                        } else {
                            secIndex = value[s][0];
                            rowIndexes = value[s][1];

                            rlen = rowIndexes.length;
                            for(r=0; r<rlen; r++) {
                                flag = _validIndex.call(this, [secIndex, rowIndexes[r]]);
                                if(flag === false) break;
                                else count++;

                                if(!(prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW
                                && prop.selectionBehavior === constants.SEGUI_MULTI_SELECT_BEHAVIOR)) {
                                    if(count > 1) flag = false;
                                }
                            }
                        }
                    }
                    if(flag && value[0][0] === 0 && !_isSectionDS(prop.data[0])) {
                        value[0][0] = -1;
                        flag = [value, true];
                    }
                } else if(!$KU.is(value, 'null')) {
                    flag = false;
                }

                return flag;
            },

            selectedRowItems: function SegmentedUI2$_valid_selectedRowItems(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'array')) {
                    flag = true;
                }

                return flag;
            },

            selectionBehavior: function SegmentedUI2$_valid_selectionBehavior(value) {
                var flag = false, options = [
                    constants.SEGUI_DEFAULT_BEHAVIOR,
                    constants.SEGUI_MULTI_SELECT_BEHAVIOR,
                    constants.SEGUI_SINGLE_SELECT_BEHAVIOR
                ];

                if(options.indexOf(value) >= 0) {
                    flag = true;
                }

                return flag;
            },

            selectionBehaviorConfig: function SegmentedUI2$_valid_selectionBehaviorConfig(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')) {
                    flag = true;
                }

                return flag;
            },

            separatorColor: function SegmentedUI2$_valid_separatorColor(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'string')) {
                    value = value.toUpperCase();
                    flag = $KU.is(value, 'color');
                }

                return (flag ? [value, flag] : flag);
            },

            separatorRequired: function SegmentedUI2$_valid_separatorRequired(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            separatorThickness: function SegmentedUI2$_valid_separatorThickness(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'number') && value >= 0) {
                    flag = true;
                }

                return flag;
            },

            showScrollbars: function SegmentedUI2$_valid_showScrollbars(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'boolean')) {
                    flag = true;
                }

                return flag;
            },

            viewType: function SegmentedUI2$_valid_viewType(value) {
                var flag = false, options = [
                    constants.SEGUI_VIEW_TYPE_PAGEVIEW,
                    constants.SEGUI_VIEW_TYPE_TABLEVIEW
                ];

                if(options.indexOf(value) >= 0) {
                    flag = true;
                }

                return flag;
            },

            widgetDataMap: function SegmentedUI2$_valid_widgetDataMap(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')) {
                    flag = true;
                }

                return flag;
            },

            widgetSkin: function SegmentedUI2$_valid_widgetSkin(value) {
                var $K = kony.$kwebfw$, $KU = $K.utils, flag = false;

                if($KU.is(value, 'object')
                || ($KU.is(value, 'string') && value.split(' ').length === 1)) {
                    flag = true;
                }

                return flag;
            }
        }
    };


    //This function will be called in the scope of widget instance
    var _validIndex = function SegmentedUI2$_validIndex(index, mutate) {
        var $K = kony.$kwebfw$, $KU = $K.utils, flag = false,
            data = this._kwebfw_.prop.data, sectionable = false;

        if(data && data.length && $KU.is(index, 'array') && index.length === 2
        && $KU.is(index[0], 'number') && $KU.is(index[1], 'number')) {
            sectionable = _isSectionDS(data[0]);

            if(index[0] === 0 && !sectionable) {
                index[0] = -1;
            }

            if(index[0] < -1) index[0] = -1;
            if(index[1] < -1) index[1] = -1;

            if(!sectionable) {
                if(index[0] === -1 && index[1] >= 0 && index[1] < data.length) {
                    flag = true;
                }
            } else if(index[0] >= 0 && index[0] < data.length) {
                if(index[1] >= -1 && index[1] < data[index[0]][1].length) {
                    flag = true;
                }
            }
        }

        return (mutate === true && flag) ? [index, true] : flag;
    };


    //All widget file must have this variable
    //All the functions will be called in the scope of widget instance
    //Any property here, which is set to "false", will not create a setter
    var _view = {
        SegmentedUI2: {
            alternateRowSkin: function SegmentedUI2$_view_alternateRowSkin(el, old) {
                _updateRowSkin.call(this);
            },

            bounces: true,

            contentOffset: function SegmentedUI2$_view_contentOffset(el, old) {
                this.setContentOffset(this.contentOffset, true);
            },

            contentOffsetMeasured: false,

            data: function SegmentedUI2$_view_data(el, old) {
                var $K = kony.$kwebfw$, $KU = $K.utils, $KD = $K.dom,
                    prop = this._kwebfw_.prop, clone = null, height = 0;

                $KD.setAttr(el.scrolee, 'aria-rowcount', _getRowCount.call(this));
                $KD.html(el.scrolee, '');
                this._kwebfw_.rows = [];

                if(prop.data && prop.data.length) {
                    if(_shouldLazyLoad.call(this)) {
                        clone = _firstRenderableRow.call(this);
                        _updateTopCutOffAbsoluteIndex.call(this);
                        _updateBottomCutOffAbsoluteIndex.call(this);

                        if($KU.is(clone, 'widget')) {
                            this._kwebfw_.rows.push(clone);
                            height += _getRowHeight.call(this, clone);

                            while($KU.is(clone, 'widget') && height < (prop.frame.height + _extraHeightToRender)) {
                                clone = _nextRenderableRows.call(this, clone._kwebfw_.ii, 1)[0];

                                if($KU.is(clone, 'widget')) {
                                    this._kwebfw_.rows.push(clone);
                                    height += _getRowHeight.call(this, clone);
                                }
                            }
                        }


                        if(this._kwebfw_.rows.length) {
                            $KD.add(el.scrolee, _renderRows.call(this, this._kwebfw_.rows));
                        }

                        //TODO:: this._kwebfw_.top and this._kwebfw_.minScroleeHeight
                        $KD.style(el.scrolee, {paddingTop:(this._kwebfw_.top + 'px'), height:(this._kwebfw_.minScroleeHeight + 'px')});
                    } else {
                        _iterateOverData.call(this, prop.data, function(data, rowIndex, secIndex) {
                            clone = _getClonedTemplate.call(this, [secIndex, rowIndex]);
                            clone && this._kwebfw_.rows.push(clone);
                        });

                        if(this._kwebfw_.rows.length) {
                            $KD.add(el.scrolee, _renderRows.call(this, this._kwebfw_.rows));
                        }

                        if(prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                            _setPageView.call(this, el);
                        }
                        _applyNodeStyles.call(this);
                    }

                    if(this._kwebfw_.searcher) _searcher.SegmentedUI2.searchText.call(this);
                }
            },

            dockSectionHeaders: true,

            needPageIndicator: function SegmentedUI2$_view_needPageIndicator(el, old) {
                var $K = kony.$kwebfw$, $KD = $K.dom;

                if(this._kwebfw_.prop.needPageIndicator) {
                    $KD.style(el.pageNav, 'display', 'block');
                } else {
                    $KD.style(el.pageNav, 'display', 'none');
                }
            },

            onRowClick: true,

            onRowDisplay: true,

            onSwipe: true,

            pageOffDotImage: function SegmentedUI2$_view_pageOffDotImage(el, old) {
                if(this._kwebfw_.prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                    _setPageView.call(this, el);
                }
            },

            pageOnDotImage: function SegmentedUI2$_view_pageOnDotImage(el, old) {
                if(this._kwebfw_.prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                    _setPageView.call(this, el);
                }
            },

            retainScrollPositionMode: true,

            retainSelection: true,

            rowFocusSkin: true,

            rowSkin: function SegmentedUI2$_view_rowSkin(el, old) {
                _updateRowSkin.call(this);
            },

            rowTemplate: true,

            scrollingEvents: true,

            sectionHeaderSkin: function SegmentedUI2$_view_sectionHeaderSkin(el, old) {
                var prop = this._kwebfw_.prop;

                if(prop.data && prop.data.length > 0 && _isSectionDS(prop.data[0])) {
                    _updateSectionHeaderSkin.call(this);
                }
            },

            sectionHeaderTemplate: true,

            selectedRowIndex: function SegmentedUI2$_view_selectedRowIndex(el, old) {
                var $K = kony.$kwebfw$, $KW = $K.widget, _ = this._kwebfw_,
                    prop = _.prop, rowIndex = 0, selectedRow = null;

                if($KW.visible(this) && prop.selectedRowIndex) {
                    rowIndex = prop.selectedRowIndex[1];

                    //_.setFocus is set in konyevent.js _setOwnerSelectedIndex.SegmentedUI2 function
                    if(prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                        _setPageViewIndicator.call(this, el, rowIndex);
                        if(_.setFocus !== false) this.setFocus(true);
                    } else if(_.setFocus !== false) { //IMP:: Must compare with boolean false
                        selectedRow = _getIndexedInfo.call(this, prop.selectedRowIndex, _.clones);
                        selectedRow.setFocus(true); // selectedRow might be null ??

                        if($KW.isFixedHeight(this)) {
                            $KW.scrollElementToParent(selectedRow, {vx:'0%', vy:'0%', tx:'0%', ty:'0%'}, true);
                        }
                    }
                }
            },

            selectedRowIndices: true,

            selectedRowItems: false,

            selectionBehavior: function SegmentedUI2$_view_selectionBehavior(el, old) {
                _updateSelectionBehavior.call(this);
            },

            selectionBehaviorConfig: function SegmentedUI2$_view_selectionBehaviorConfig(el, old) {
                _updateSelectionBehaviorConfig.call(this);
            },

            separatorColor: function SegmentedUI2$_view_separatorColor(el, old) {
                _updateSeparator.call(this);
            },

            separatorRequired: function SegmentedUI2$_view_separatorRequired(el, old) {
                _updateSeparator.call(this);
            },

            separatorThickness: function SegmentedUI2$_view_separatorThickness(el, old) {
                _updateSeparator.call(this);
            },

            showScrollbars: true,

            viewType: function SegmentedUI2$_view_viewType(el, old) {
                var $K = kony.$kwebfw$, $KD = $K.dom, prop = this._kwebfw_.prop;

                if(prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                    _registerSwipeGesture.call(this, el.scrolee);
                } else {
                    $KD.off(el.scrolee, 'swipe', 'segment');
                }

                if(old !== prop.viewType) {
                    _clearSelectedIndices.call(this);
                    if(prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                        _setPageView.call(this, el);
                    } else {
                        _setTableView.call(this, el);
                    }
                }
            },

            widgetDataMap: true,

            widgetSkin: function SegmentedUI2$_view_widgetSkin(el, old) {
                var $K = kony.$kwebfw$, $KD = $K.dom, prop = this._kwebfw_.prop;

                if(old !== '') $KD.removeCls(el.node, old);
                if(prop.widgetSkin !== '') $KD.addCls(el.node, prop.widgetSkin);
            }
        }
    };


    Object.defineProperty(kony.ui, 'SegmentedUI2', {configurable:false, enumerable:false, writable:false, value:(function() {
        var $K = kony.$kwebfw$;


        /**
         * kony.ui.SegmentedUI2 constructor.
         *
         * @class
         * @namespace   kony.ui
         * @extends     kony.ui.BasicWidget
         * @author      Goutam Sahu <goutam.sahu@kony.com>
         *
         * @param       {object} bconfig - An object with basic properties.
         * @param       {object} lconfig - An object with layout properties.
         * @param       {object} pspconfig - An object with platform specific properties.
         *
         * @throws      {InvalidArgumentException} - Invalid argument is passed.
         * @throws      {InvalidPropertyException} - Invalid property or invalid value of a property is passed.
         *
         * @classdesc   A brief description about the class.
         *              -
         *              -
         *
         * @todo        Anything that thought for but not yet implemented.
         *              -
         *              -
         */
        var SegmentedUI2 = function SegmentedUI2(bconfig, lconfig, pspconfig) {
            var $K = kony.$kwebfw$, $KU = $K.utils, self = this,
                dependentPropertiesValidationMessage = '', prop = null;

            prop = {
                alternateRowSkin: '',
                bounces: true, //Only for TABLE_VIEW //In doc available, but in SPA code not available
                containerHeight: '', //In doc not available, but in SPA code available
                containerHeightReference: '', //In doc not available, but in SPA code available
                contentOffset: null, //In doc not available, but in SPA code available
                contentOffsetMeasured: {x:0, y:0},
                data: null,
                dockSectionHeaders: false,
                enableScrollBounce: true, //In doc not available, but in SPA code available
                needPageIndicator: true,
                onRowClick: null,
                onRowDisplay: null,
                onSwipe: null, //Only for PAGE_VIEW
                pageOffDotImage: 'blackdot.gif',
                pageOnDotImage: 'whitedot.gif',
                retainScrollPositionMode: constants.SEGUI_SCROLL_POSITION_DEFAULT, //In doc not available, but in SPA code available
                retainSelection: false,
                rowFocusSkin: 'seg2Focus',
                rowSkin: 'seg2Normal',
                rowTemplate: null,
                scrollingEvents: null,
                sectionHeaderSkin: '',
                sectionHeaderTemplate: null,
                selectedRowIndex: null,
                selectedRowIndices: null,
                selectedRowItems: [],
                selectionBehavior: constants.SEGUI_DEFAULT_BEHAVIOR,
                selectionBehaviorConfig: null,
                separatorColor: '00000000',
                separatorRequired: false,
                separatorThickness: 1,
                showScrollbars: true,
                viewType: constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                widgetDataMap: {},
                widgetSkin: ''
            };

            _populateUnderscore.SegmentedUI2.call(this);

            if(!$KU.is(bconfig, 'object')) bconfig = {};
            if(!$KU.is(bconfig.id, 'string') || !bconfig.id) {
                bconfig.id = (this._kwebfw_.name+$KU.uid());
            }

            SegmentedUI2.base.call(this, bconfig, lconfig, pspconfig);

            if($KU.is(_dependentPropertiesValidationMessage.SegmentedUI2, 'function')) {
                dependentPropertiesValidationMessage = _dependentPropertiesValidationMessage.SegmentedUI2.call(this, prop, bconfig, lconfig, pspconfig);
            }

            if(dependentPropertiesValidationMessage) {
                throw new Error(dependentPropertiesValidationMessage);
            } else {
                //Defaulting to platfom values specific to SegmentedUI2
                $KU.each(prop, function(value, key) {
                    var $K = kony.$kwebfw$, $KU = $K.utils,
                        $KW = $K.widget, valid = false, message = '';

                    if(!bconfig.hasOwnProperty(key)) {
                        bconfig[key] = value;
                    } else if($KW.getNonConstructorProperties(self._kwebfw_.name).indexOf(key) >= 0) {
                        throw new Error('<'+key+'> is a non-constructor property of <'+self._kwebfw_.ns+'> class.');
                    } else if(!$KU.is(_valid.SegmentedUI2[key], 'function')) {
                        throw new Error('<'+key+'> is available in default widget properties of <kony.ui.SegmentedUI2>, but not in <_valid.SegmentedUI2> namespace.');
                    } else {
                        valid = _valid.SegmentedUI2[key].call(self, bconfig[key]);
                        if($KU.is(valid, 'array')) {bconfig[key] = valid[0]; valid = valid[1];}

                        if(valid === false || ($KU.is(valid, 'string') && valid)) {
                            message = ('Invalid value passed to property <'+key+'> of widget <'+self._kwebfw_.ns+'>.');

                            if($KU.is(valid, 'string')) {
                                message += ('\n' + valid);
                            }

                            throw new Error(message);
                        }
                    }
                });

                //Defining Getters/Setters specific to SegmentedUI2
                $KU.each(_view.SegmentedUI2, function(value, key) {
                    var $K = kony.$kwebfw$, $KU = $K.utils;

                    $KU.defineProperty(self._kwebfw_.prop, key, bconfig[key], {configurable:false, enumerable:true, writable:true});

                    $KU.defineGetter(self, key, function SegmentedUI2$_getter() {
                        var $K = kony.$kwebfw$, $KU = $K.utils;

                        if($KU.is(_getter.SegmentedUI2[key], 'function')) {
                            return _getter.SegmentedUI2[key].call(this, this._kwebfw_.prop[key]);
                        } else {
                            return this._kwebfw_.prop[key];
                        }
                    }, true);

                    $KU.defineSetter(self, key, function SegmentedUI2$_setter(val) {
                        var $K = kony.$kwebfw$, $KU = $K.utils, old = null,
                            valid = false, $KW = $K.widget, rmodel = null,
                            final = null, message = '', el = null;

                        if(value === false) {
                            throw new Error('<'+key+'> is a readonly property of <'+this._kwebfw_.ns+'> widget.');
                        } else if(this._kwebfw_.prop[key] !== val) {
                            rmodel = $KW.rmodel(this);

                            if(rmodel && rmodel._kwebfw_.is.template && !rmodel._kwebfw_.is.cloned) {
                                throw new Error('Cannot set any value of a widget, which is either a raw template or any of its widget.');
                            } else {
                                valid = _valid.SegmentedUI2[key].call(this, val);
                                if($KU.is(valid, 'array')) {val = valid[0]; valid = valid[1];}

                                if(valid === false || ($KU.is(valid, 'string') && valid)) {
                                    message = ('Invalid value passed to property <'+key+'> of widget <'+self._kwebfw_.ns+'>.');

                                    if($KU.is(valid, 'string')) {
                                        message += ('\n' + valid);
                                    }

                                    throw new Error(message);
                                } else {
                                    old = this._kwebfw_.prop[key];
                                    this._kwebfw_.prop[key] = val;

                                    if($KU.is(_setter.SegmentedUI2[key], 'function')) {
                                        _setter.SegmentedUI2[key].call(this, old);
                                    }

                                    if(_relayoutActiveTriggerer.SegmentedUI2().indexOf(key) >= 0) {
                                        $KW.markRelayout(this);
                                    }

                                    if(_relayoutPassiveTriggerer.SegmentedUI2().indexOf(key) >= 0) {
                                        final = this._kwebfw_.flex.final;

                                        if(!(final.height && final.width)) {
                                            $KW.markRelayout(this);
                                        }
                                    }

                                    $KW.onPropertyChange(this, key, old);

                                    if($KU.is(value, 'function')) {
                                        el = $KW.el(this);
                                        el.node && value.call(this, el, old);
                                    }
                                }
                            }
                        }
                    }, false);
                });

                if($KU.is(_postInitialization.SegmentedUI2, 'function')) {
                    _postInitialization.SegmentedUI2.call(this);
                }
            }

            pspconfig = lconfig = bconfig = null; //For GC
        };


        $K.utils.inherits(SegmentedUI2, kony.ui.BasicWidget);


        /**
         * Takes care of flushing out the widget reference to clean memory.
         *
         * @access      protected
         * @method      _flush
         * @memberof    kony.ui.SegmentedUI2
         * @author      Goutam Sahu <goutam.sahu@kony.com>
         *
         * @returns     void
         */
        var segment2__flush = function SegmentedUI2$_flush() {
            var $super = kony.ui.SegmentedUI2.base.prototype;

            _flushClones.call(this, this._kwebfw_.clones);
            $super._flush.call(this);
        };


        /**
         * Builds the view layer for SegmentedUI2 widget.
         *
         * @override
         * @access      protected
         * @method      _render
         * @memberof    kony.ui.SegmentedUI2
         * @author      Goutam Sahu <goutam.sahu@kony.com>
         *
         * @returns     {HTMLElement} – SegmentedUI2 view.
         */
        var segment2__render = function SegmentedUI2$_render(tag, context) {
            var $super = kony.ui.SegmentedUI2.base.prototype, _ = this._kwebfw_,
                $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget,
                $KD = $K.dom, docker = null, view = _.view, blocker = null,
                scrolee = null, hScroll = null, vScroll = null, el = null,
                pageNav = null;

            if(!$KU.is(context, 'object')) context = {};

            if(this.isVisible || $K.F.RIVW) {
                if(!$KU.is(_.view, 'dom')) {
                    scrolee = $KD.create('UL', {role:'grid', 'kv': 'tableview'});
                    docker = $KD.create('DIV', {kr:'docker'}, {position:'absolute', top:'0px', left:'left', width:'100%'});
                    blocker = $KD.create('DIV', {kr:'blocker'}, {position:'absolute', top:'0px', left:'left', width:'100%', height:'100%'});

                    $KD.add(docker, blocker);

                    if($KU.scrollType() !== 'native') {
                        $KD.setAttr(scrolee, 'kr', 'scrolee');
                        hScroll = $KD.create('DIV', {kr:'h-scroll'});
                        vScroll = $KD.create('DIV', {kr:'v-scroll'});
                    }

                    pageNav = $KD.create('DIV', {align:'center'}, {display:'none', position:'absolute', bottom:'0px', width:'100%'});

                    view = $super._render.call(this, tag, [scrolee, docker, pageNav, hScroll, vScroll]);

                    $KD.setAttr(view, 'kwh-keydown', 'onKeyDown');
                    $KD.setAttr(scrolee, 'aria-colcount', 1);

                    el = $KW.el(view);
                    if(_.prop.viewType === constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                        _view.SegmentedUI2.viewType.call(this, el, _.prop.viewType);
                    } else {
                        $KW.registerNativeScrollEvent(this);
                        $KD.on(el.viewport, 'scroll', 'segment', _animator.SegmentedUI2.scrollStart, {scope: this});
                    }
                }

                el = $KW.el(view);

                if($KU.scrollType() !== 'native') {
                    $KD.style(el.node, {overflowX:'hidden', overflowY:'hidden'});
                } else {
                    $KD.style(el.viewport, {overflowX:'auto', overflowY:'auto'});
                    $KD.style(el.node, {overflowX:'hidden'});
                }

                if(_.prop.widgetSkin !== '') $KD.addCls(el.node, _.prop.widgetSkin);

                if(_.prop.viewType !== constants.SEGUI_VIEW_TYPE_PAGEVIEW) {
                    _view.SegmentedUI2.data.call(this, el, _.prop.data);
                }

                $KW.accessibility(this);
            }

            return view;
        };


        var segment2_addAll = function SegmentedUI2$addAll(data, anim) {
            var prop = this._kwebfw_.prop, secIndex = -1, rowIndex = -1;

            if(!_valid.SegmentedUI2.data.call(this, data)) {
                throw new Error('Invalid data.');
            }
            if(!prop.data) prop.data = [];

            if(prop.data.length === 0
            || (_isSectionDS(prop.data[0]) && _isSectionDS(data[0]))
            || (!_isSectionDS(prop.data[0]) && !_isSectionDS(data[0]))) {

                if(_isSectionDS(data[0])) secIndex = prop.data.length;
                else rowIndex = prop.data.length;

                _onRowChange.call(this, secIndex, rowIndex, 'addall', data, anim);

            } else {
                //Throw Error:: Existing data structure of Segment do not match with that of passed data
                throw new Error('Invalid data.');
            }
        };


        var segment2_addDataAt = function SegmentedUI2$addDataAt(data, rowIndex, secIndex, anim) {
            var $K = kony.$kwebfw$, $KU = $K.utils, errorMessage = null;

            if($KU.is(data, 'object')) {
                errorMessage = _validateInputIndices.call(this, secIndex, rowIndex, 'add');

                if(errorMessage === '') {
                    if(!$KU.is(secIndex, 'number')) secIndex = -1;
                    if(!this._kwebfw_.prop.data) this._kwebfw_.prop.data = [];
                    _onRowChange.call(this, secIndex, rowIndex, 'add', data, anim);
                } else {
                    throw new Error(errorMessage);
                }
            } else {
                throw new Error('Invalid data.');
            }
        };


        var segment2_addSectionAt = function SegmentedUI2$addSectionAt(data, index, anim) {
            var $K = kony.$kwebfw$, $KU = $K.utils,
                prop = this._kwebfw_.prop, errorMessage = '';

            if($KU.is(data, 'array') && !_isSectionDS(data)) {
                throw new Error('Invalid data.');
            }

            errorMessage = _validateInputIndices.call(this, index, 0, 'addsectionat');

            if(errorMessage === '') {
                if(!prop.data) prop.data = [];

                if(prop.data.length === 0 || _isSectionDS(prop.data[0])) {
                    _onRowChange.call(this, index, -1, 'addsectionat', data, anim);
                } else {
                    throw new Error('Invalid data.');
                }
            } else {
                throw new Error(errorMessage);
            }

        };


        var segment2_animateRows = function SegmentedUI2$animateRows(animContext) {
            _animator.SegmentedUI2.animateRows.call(this, animContext);
        };


        var segment2_clearSearch = function SegmentedUI2$clearSearch() {
            var _ = this._kwebfw_;

            if(_.searcher) {
                _.searcher = null;
                _searcher.SegmentedUI2.clearSearchResult.call(this);
            }
        };


        var segment2_getFirstVisibleRow = function SegmentedUI2$getFirstVisibleRow() {
            return _getVisibleRow.call(this, {firstRow:true, lastRow:false});

        };


        var segment2_getLastVisibleRow = function SegmentedUI2$getLastVisibleRow() {
            return _getVisibleRow.call(this, {firstRow:false, lastRow:true});
        };


        var segment2_getUpdatedSearchResults = function SegmentedUI2$getUpdatedSearchResults() {
            var _ = this._kwebfw_, filteredResult = null;

            if(_.searcher) {
                filteredResult = _searcher.SegmentedUI2.getFilteredData.call(this);
            }

            return filteredResult;
        };


        var segment2_removeAll = function SegmentedUI2$removeAll(anim) {
            if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                _animator.SegmentedUI2.onRowDisplayHandler.call(this, kony.segment.REMOVE, this._kwebfw_.rows);
                _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'removeall', this._kwebfw_.rows, -1, -1, anim);
            } else {
                _action.SegmentedUI2._removeAll.call(this);
            }
        };


        var segment2_removeAt = function SegmentedUI2$removeAt(rowIndex, secIndex, anim) {
            var $K = kony.$kwebfw$, $KU = $K.utils, errorMessage = null;

            errorMessage = _validateInputIndices.call(this, secIndex, rowIndex, 'remove');
            if(errorMessage === '') {
                if(!$KU.is(secIndex, 'number')) secIndex = -1;
                _onRowChange.call(this, secIndex, rowIndex, 'remove', anim);
            } else {
                throw new Error(errorMessage);
            }
        };


        var segment2_removeSectionAt = function SegmentedUI2$removeSectionAt(index, anim) {
            var prop = this._kwebfw_.prop;

            if(prop.data && prop.data.length > 0 && _isSectionDS(prop.data[0])) {
                if(index >= 0 && index < prop.data.length) {
                    _onRowChange.call(this, index, -1, 'removesectionat', prop.data, anim);
                } else {
                    throw new Error('Invalid index passed.');
                }
            } else {
                throw new Error('Invalid input or no data exists.');
            }
        };


        var segment2_searchText = function SegmentedUI2$searchText(searchCondition, config) {
            var $K = kony.$kwebfw$, $KU = $K.utils, _ = this._kwebfw_, filteredResult = null;

            if(!$KU.is(searchCondition, 'array')) {
                throw new Error('searchCondition is missing or not an array.');
            } else {

                if($KU.is(config, 'undefined') || !$KU.is(config, 'object')) {
                    config = {updateSegment: true, showSectionHeaderFooter: true};
                } else {
                    if(!$KU.is(config.updateSegment, 'boolean')) {
                        config.updateSegment = true;
                    }

                    if(!$KU.is(config.showSectionHeaderFooter, 'boolean')) {
                        config.showSectionHeaderFooter = true;
                    }
                }

                if(_.searcher) {
                    _searcher.SegmentedUI2.clearSearchResult.call(this);
                }

                _.searcher = {searchCondition: searchCondition, config: config};

                filteredResult = _searcher.SegmentedUI2.searchText.call(this);
            }

            return filteredResult;
        };


        var segment2_setAnimations = function SegmentedUI2$setAnimations(animInfo) {
            _animator.SegmentedUI2.setAnimations.call(this, animInfo);
        };


        var segment2_setContentOffset = function segmentedUI2$setContentOffset(offset, animate) {
            var $K = kony.$kwebfw$, $KW = $K.widget, _ = this._kwebfw_,
                prop = _.prop, rowIndex = -1, value = {}, view = _.view;
            if(view) {
                if(prop.viewType === constants.SEGUI_VIEW_TYPE_TABLEVIEW) {
                    $KW.setContentOffset(this, offset, animate);
                } else {
                    value = $KW.getContentOffsetValues(this, offset);
                    rowIndex = value.x && parseInt(value.x / _.swipeContext.imageWidth);
                    if(rowIndex < 0) {
                        rowIndex = 0;
                    } else if(rowIndex >= prop.data.length) {
                        rowIndex = prop.data.length - 1;
                    }
                    _setPageViewIndicator.call(this, $KW.el(this), rowIndex);
                }
            }
        };

        var segment2_setData = function SegmentedUI2$setData(data, anim) {
            _clearSelectedIndices.call(this);
            this.data = data;
            if(_animator.SegmentedUI2.canAnimate.call(this, anim)) {
                _animator.SegmentedUI2.onRowDisplayHandler.call(this, kony.segment.UPDATE, this._kwebfw_.rows);
                _animator.SegmentedUI2.applyRowsAnimationByAPI.call(this, 'setdata', this._kwebfw_.rows, -1, -1, anim);
            }
        };


        var segment2_setDataAt = function SegmentedUI2$setDataAt(data, rowIndex, secIndex, anim) {
            var $K = kony.$kwebfw$, $KU = $K.utils, $KW = $K.widget, widget = null , key = '',
                _ = this._kwebfw_, prop = _.prop, old = null, errorMessage = null, wap = [], widgetId = '';
                _constructObject = function(widget, data, key) {
                        var defaultProp = null, value = null;

                        if($KU.is(data, 'string')) {
                            defaultProp = $KW.getDefaultProperty(widget);
                            value = data;
                            data = {};
                            data[defaultProp] = value;
                        } else {
                            data = {};
                        }

                        data[key] = widget[key];
                        return data;
                };


            if(arguments.length === 5 && $KU.is(data, 'widget')) {
                //Will get into this, when setter of cloned model is called
                widget = data;
                key = rowIndex;
                old = secIndex;
                rowIndex = anim;
                secIndex = arguments[4];
                wap = $KW.getWidgetDataMapPath(widget);
                widgetId = this.widgetDataMap[wap];

                if(widgetId) {
                    if(secIndex === -1 && rowIndex !== -1) {
                        if($KU.is(prop.data[rowIndex][widgetId], 'object')) {
                            prop.data[rowIndex][widgetId][key] = widget[key];
                        } else {
                            prop.data[rowIndex][widgetId] = _constructObject(widget, prop.data[rowIndex][widgetId], key);
                        }
                    } else if(secIndex !== -1 && rowIndex === -1) {
                        if($KU.is(prop.data[secIndex][0][widgetId], 'object')) {
                            prop.data[secIndex][0][widgetId][key] = widget[key];
                        } else {
                            prop.data[secIndex][0][widgetId] = _constructObject(widget, prop.data[secIndex][0][widgetId], key);
                        }
                    } else if(secIndex !== -1 && rowIndex !== -1) {
                        if($KU.is(prop.data[secIndex][1][rowIndex][widgetId], 'object')) {
                            prop.data[secIndex][1][rowIndex][widgetId][key] = widget[key];
                        } else {
                            prop.data[secIndex][1][rowIndex][widgetId] = _constructObject(widget, prop.data[secIndex][1][rowIndex][widgetId], key);
                        }
                    }
                }
            } else { //This is actual implementation of setDataAt
                if($KU.is(data, 'object')) {
                    errorMessage = _validateInputIndices.call(this, secIndex, rowIndex, 'update');

                    if(errorMessage === '') {
                        if(!$KU.is(secIndex, 'number')) secIndex = -1;
                        _onRowChange.call(this, secIndex, rowIndex, 'update', data, anim);
                    } else {
                        throw new Error(errorMessage);
                    }
                } else {
                    throw new Error('Invalid data.');
                }

            }
        };


        var segment2_setSectionAt = function SegmentedUI2$setSectionAt(data, index, anim) {
            var prop = this._kwebfw_.prop;

            if(prop.data && prop.data.length > 0 && _isSectionDS(prop.data[0])) {
                if(index >= 0 && index < prop.data.length) {
                    _onRowChange.call(this, index, -1, 'setsectionat', data, anim);
                } else {
                    throw new Error('Invalid index passed.');
                }
            } else {
                throw new Error('Invalid input or no data exists.');
            }
        };


        $K.defKonyProp(SegmentedUI2.prototype, [
            {keey:'_flush', value:segment2__flush},
            {keey:'_render', value:segment2__render},
            {keey:'addAll', value:segment2_addAll},
            {keey:'addDataAt', value:segment2_addDataAt},
            {keey:'addSectionAt', value:segment2_addSectionAt},
            {keey:'animateRows', value:segment2_animateRows},
            {keey:'clearSearch', value:segment2_clearSearch},
            {keey:'getFirstVisibleRow', value:segment2_getFirstVisibleRow},
            {keey:'getLastVisibleRow', value:segment2_getLastVisibleRow},
            {keey:'getUpdatedSearchResults', value:segment2_getUpdatedSearchResults},
            {keey:'removeAll', value:segment2_removeAll},
            {keey:'removeAt', value:segment2_removeAt},
            {keey:'removeSectionAt', value:segment2_removeSectionAt},
            {keey:'searchText', value:segment2_searchText},
            {keey:'setAnimations', value:segment2_setAnimations},
            {keey:'setContentOffset', value:segment2_setContentOffset},
            {keey:'setData', value:segment2_setData},
            {keey:'setDataAt', value:segment2_setDataAt},
            {keey:'setSectionAt', value:segment2_setSectionAt}
        ]);


        return SegmentedUI2;
    }())});
}());
