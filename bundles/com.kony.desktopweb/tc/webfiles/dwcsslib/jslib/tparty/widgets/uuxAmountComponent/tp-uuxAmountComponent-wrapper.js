uuxAmountComponent = {

initializeWidget: function(parentNode, widgetModel, config) {
//Assign custom DOM to parentNode to render this widget.

var currentElement=document.createElement("uwc-amount-field");
//parentNode.innerHTML = "<uwc-amount-field='"+widgetModel.parent.id+widgetModel.id+" decimals='"+widgetModel.decimals+" disabled='"+widgetModel.disabled+" endAligned='"+widgetModel.endAligned+" label='"+widgetModel.labelDescription+" locale='"+widgetModel.locale+" placeholder='"+widgetModel.placeholder+" prefix='"+widgetModel.prefix+" readonly='"+widgetModel.readonly+" required='"+widgetModel.required+" suffix='"+widgetModel.suffix+" value='"+widgetModel.value+"'></uwc-amount-field>";
//var currentElement = parentNode.lastElementChild;

currentElement.decimals = widgetModel.decimals;
currentElement.disabled = widgetModel.disabled;
currentElement.endAligned = widgetModel.endAligned;
currentElement.label = widgetModel.labelDescription;
currentElement.locale = widgetModel.locale;
currentElement.placeholder = widgetModel.placeholder;
currentElement.prefix = widgetModel.prefix;
currentElement.readonly = widgetModel.readonly;
currentElement.required = widgetModel.required;
currentElement.suffix = widgetModel.suffix;
currentElement.value = widgetModel.value;

currentElement.addEventListener('change', (event) => {
widgetModel.value = event.target.value;
});
parentNode.append(currentElement);
},

modelChange: function(widgetModel, propertyChanged, propertyValue) {
//Handle widget property changes to update widget's view and
//trigger custom events based on widget state.


let element = document.getElementById(widgetModel.parent.id+widgetModel.id);

if(element)
{
if(widgetModel.onValueChange){

widgetModel.onValueChange(propertyValue);

}

}
}
};