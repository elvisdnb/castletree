uuxButton = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-button id='"+widgetModel.parent.id+widgetModel.id+"' label='"+widgetModel.labelText+"'  type='"+widgetModel.typeButton.selectedValue+"' size='"+widgetModel.size.selectedValue+"' icon='"+widgetModel.icon+"' contained='"+widgetModel.contained+"' ></uwc-button>";
    var element = parentNode.lastElementChild;
    //enables the button to make outlined
    element.outlined = widgetModel.outlined;
    //enables the button call to action
    element.cta = widgetModel.cta;
    //enables the button disable
    element.disabled = widgetModel.disabled;
    //enables the icon at trail
    element.trailingIcon = widgetModel.trailingIcon;
    //enables the button compact
    element.compact = widgetModel.compact;
    //element.addEventListener
    element.onclick = (evt)=>{
      if((typeof widgetModel.onclickEvent)==="function"){
        widgetModel.onclickEvent();
      }
    }


  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    let element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element)
    {
      if(propertyChanged==="disabled"){
        element.disabled = propertyValue;
      }
      if(propertyChanged==="cta"){
        element.cta = propertyValue;
      }
      if(propertyChanged==="outlined"){
        element.outlined = propertyValue;
      }
      if(propertyChanged==="compact"){
        element.compact = propertyValue;
      }

    }
  }
};