uuxRecurrenceDatePicker = {
  _domElement:null,
  initializeWidget: function(parentNode, widgetModel) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.style.overflow="visible";
    const widgetId = widgetModel.id;

    parentNode.innerHTML = "<uwc-date-recurrence-picker id="+widgetId+"></uwc-date-recurrence-picker>";
    const element = parentNode.lastChild;
    this._domElement = element;

    if(widgetModel.lableText){
      element.label = widgetModel.lableText;
    }
    
    var supportedProperties = ["dailyMax","monthlyDayMax","monthlyMax","value","weeklyMax","yearlyDayMax","yearlyMax","messagePrefix","formattedValue"];

    supportedProperties.forEach((propertyName)=>{
      element[propertyName] = widgetModel[propertyName];
    });

    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
      widgetModel.formattedValue = event.target.formattedValue;
    });
	
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    if(propertyChanged==="lableText"){
      this._domElement.label = propertyValue;
    }
    if(propertyChanged==="value"){
      widgetModel.onValueSelected(widgetModel);
    }
  }
};