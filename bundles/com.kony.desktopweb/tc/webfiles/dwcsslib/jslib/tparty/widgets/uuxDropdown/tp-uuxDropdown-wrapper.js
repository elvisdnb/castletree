uuxDropdown = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.style.overflow = 'visible';
    parentNode.innerHTML = "<uwc-select id='"+widgetModel.parent.id+"_"+widgetModel.id+"' label='"+widgetModel.labelText+"' ></uwc-select>";
    var element = parentNode.lastElementChild;
    if(widgetModel.options && widgetModel.options!==""){
      element.options = JSON.parse(widgetModel.options);
      if(widgetModel.value){
        element.value = widgetModel.value;
      }
    }
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    if(widgetModel.parent !== "" && widgetModel.parent !== undefined && widgetModel.parent!== null)
    {
    var element = document.getElementById(widgetModel.parent.id+"_"+widgetModel.id);
    }
    if(element){
      switch(propertyChanged){
        case "labelText" :{
          element.label = propertyValue;
          break;
        }
        case "value" :{
          element.value = propertyValue;
          if((typeof widgetModel.valueChanged)==="function"){
            widgetModel.valueChanged(propertyValue);
          }
          break;
        }
        case "options":{
          element.options = JSON.parse(propertyValue);
          break;
        }
        default:{
          element[propertyChanged] = propertyValue;
        }
      }
    }
  }
};