uuxProductSelector = {

    initializeWidget: function(parentNode, widgetModel, config) {
        //Assign custom DOM to parentNode to render this widget.
     const widgetId = widgetModel.id;
    parentNode.innerHTML += "<uwc-product-selector id="+widgetModel.parent.id+widgetModel.id+"></uwc-product-selector>";
    const element = parentNode.lastChild;
    this._domElement = element;

    if(widgetModel.productList)
    {
      element.productList = widgetModel.productList;
    }

    if (widgetModel.packageConfig)
    {
      element.packageConfig = widgetModel.packageConfig;
    }

    element.addEventListener('change', (event) => {
      widgetModel.packageConfig = event.target.packageConfig;
      widgetModel.productList = event.target.productList;
    });
        
    },

    modelChange: function(widgetModel, propertyChanged, propertyValue) {
        //Handle widget property changes to update widget's view and
        //trigger custom events based on widget state.
      
   let element =document.getElementById(widgetModel.parent.id+widgetModel.id);

    if(element)
    {
      if(propertyChanged==="packageConfig")
      {
        element.packageConfig = propertyValue;
      }

      if(propertyChanged==="productList")
      {
        element.productList = propertyValue;
      }
      
     
    }

  }

};