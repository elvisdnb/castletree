uuxProductDesignerAppearance = {
  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.  
    parentNode.innerHTML = "<uwc-product-designer-appearance id="+widgetModel.parent.id+widgetModel.id+"></uwc-product-designer-appearance>";
    var element = parentNode.lastElementChild;
    if(widgetModel.nodes){
      element.nodes = JSON.parse(widgetModel.nodes);
      widgetModel.treeState = widgetModel.nodes;
    }
     if(widgetModel.horizontal.selectedValue){
       element.horizontal = widgetModel.horizontal.selectedValue;
     }
    let otherSupportedProperties = ["buttonLabel","open","relativeToParent"];
    otherSupportedProperties.map((propertyName)=>{
      element[propertyName] = widgetModel[propertyName];
    });
    element.addEventListener('nodeChanged',(event)=>{
      widgetModel.treeState = event.detail.treeState;
    });
  },
  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element){
      if(propertyChanged==="nodes"){
		element.nodes = JSON.parse(propertyValue);
        widgetModel.treeState = propertyValue;
      }
    }
  }
};