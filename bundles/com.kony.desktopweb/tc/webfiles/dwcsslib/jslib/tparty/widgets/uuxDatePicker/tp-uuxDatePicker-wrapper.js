uuxDatePicker = {
  initializeWidget: function(parentNode, widgetModel) {
    parentNode.style.overflow = "visible";
    parentNode.innerHTML = "<uwc-date-picker id="+widgetModel.parent.id+widgetModel.id+"></uwc-date-picker>";
    const element = parentNode.lastChild;
    this._domElement = element;

    if(widgetModel.labelText){
      element.label = widgetModel.labelText;
    }

    var otherSupportedProperties = ["displayFormat", "dense","disabledDates","disabledDays","firstDayOfWeek","hideHeader","iconButtonTrailingAriaLabel","max","min","showWeekNumber","value","weekLabel","disabled"];
    otherSupportedProperties.forEach((propertyName)=>{
      element[propertyName] = widgetModel[propertyName];
    });

    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
    });

  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element){
      if(propertyChanged === "labelText"){
        element.label = propertyValue;
      }else element[propertyChanged] = propertyValue;
    }
  }
};