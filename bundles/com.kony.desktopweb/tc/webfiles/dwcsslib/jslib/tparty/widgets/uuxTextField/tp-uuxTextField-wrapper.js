uuxTextField = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-text-field id='"+widgetModel.parent.id+widgetModel.id+"' label='"+widgetModel.labelText+"' type='"+widgetModel.fieldType.selectedValue+"' value='"+widgetModel.value+"'></uwc-text-field>";
    var element = parentNode.lastElementChild;
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });

    element.validationMessage = "Invalid Value";

    element.validityTransform = (newValue,nativeValidity) => {
      let validity = widgetModel.isValid(newValue);
      return {
        valid : validity,
        customError : !validity
      };
    };    
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element){
      if(propertyChanged === "labelText"){
        element["label"] = propertyValue;  
      }else element[propertyChanged] = propertyValue;
      
      if(propertyChanged==="value"){
        if((typeof widgetModel.valueChanged)==="function"){
           widgetModel.valueChanged(propertyValue);
        }
      }
      
    }
  }
};