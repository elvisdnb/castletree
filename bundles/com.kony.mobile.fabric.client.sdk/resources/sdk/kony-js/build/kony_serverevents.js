/**
 * kony_serverevents version 9.1.14
 */
       
//#ifdef iphone
//#define PLATFORM_NATIVE_IOS
//#endif
//#ifdef ipad
//#define PLATFORM_NATIVE_IOS
//#endif

//#ifdef android
//#define PLATFORM_NATIVE_ANDROID
//#endif
//#ifdef tabrcandroid
//#define PLATFORM_NATIVE_ANDROID
//#endif

//#ifdef winphone8
//#define PLATFORM_NATIVE_WINDOWS
//#endif
//#ifdef windows8
//#define PLATFORM_NATIVE_WINDOWS
//#endif
//#ifdef desktop_kiosk
//#define PLATFORM_NATIVE_WINDOWS
//#endif

//#ifdef spaip
//#define PLATFORM_SPA
//#endif
//#ifdef spaan
//#define PLATFORM_SPA
//#endif
//#ifdef spabb
//#define PLATFORM_SPA
//#endif
//#ifdef spabbnth
//#define PLATFORM_SPA
//#endif
//#ifdef spawinphone8
//#define PLATFORM_SPA
//#endif
//#ifdef spawindows
//#define PLATFORM_SPA
//#endif
//#ifdef spatabwindows
//#define PLATFORM_SPA
//#endif
//#ifdef spaipad
//#define PLATFORM_SPA
//#endif
//#ifdef spatabandroid
//#define PLATFORM_SPA
//#endif
//#ifdef spaplaybook
//#define PLATFORM_SPA
//#endif
//#ifdef desktopweb
//#define PLATFORM_SPA
//#endif
/**
 * Created by Inderpreet Kaur on 3/1/2020.
 * Copyright © 2020 Kony. All rights reserved.
 */
if (typeof(kony) === "undefined") {
    kony = {};
}
if (typeof(kony.sdk) === "undefined") {
    kony.sdk = {};
}

// kony.serverEvents is a flag to know whether KonyServerEvents.js file is loaded or not.
kony.serverEvents = true;

/**
 * Publish events to the server.
 *
 * example : [{"topic" : "transaction/deposit",
                       "data" : { "amount": "1500",
                       "user": "clientevents",
                       "account": "1000",
                       "transaction": "deposit" }}]
 * @param events         events string to be published to the server
 * @param publishOptions additional options for publishing
 */

function initializeServerEvents() {
    kony.sdk.prototype.publishServerEvents = function (events, publishOptions) {
        if (!kony.sdk.isNetworkAvailable()) {
            kony.sdk.logsdk.error("KonyServerEvents::publishServerEvents:: No internet available, please check device connectivity.");
            return;
        }

        if (kony.sdk.util.isNullOrUndefinedOrEmptyObject(events)) {
            kony.sdk.logsdk.error("KonyServerEvents::publishServerEvents:: Events to be published are null or undefined or empty.");
            return;
        }

        try {
            events = '{"' + kony.sdk.websocket.constants.WEBSOCKET_PUBLISH_EVENTS + '" : ' + JSON.stringify(events) + '}';
        } catch (error) {
            kony.sdk.logsdk.error("KonyServerEvents::publishServerEvents:: Error : ", error);
            return;
        }

        kony.sdk.websocket.publishServerEvent(events, publishOptions);
    };

    /**
     * Subscribe to server events.
     *
     * example : ["transaction/deposit",
     *            "transaction/withdraw"]
     * @param events            events string to be sent to the server
     * @param subscribeOptions additional options for subscribing
     */
    kony.sdk.prototype.subscribeServerEvents = function (events, subscribeOptions) {
        var url = "";
        var authToken = "";

        if (!kony.sdk.isNetworkAvailable()) {
            kony.sdk.logsdk.error("No internet available, please check device connectivity.");
            kony.sdk.verifyAndCallClosure(subscribeOptions.onFailureCallback, "No internet available, please check device connectivity");
            return;
        }

        if (subscribeOptions && !subscribeOptions.onEventCallback) {
            kony.sdk.logsdk.error("KonyServerEvents::subscribeServerEvents:: onEvent callback is null or undefined");
            kony.sdk.verifyAndCallClosure(subscribeOptions.onFailureCallback, "onEvent callback is null or undefined");
            return;
        }

        if (kony.sdk.util.isNullOrUndefinedOrEmptyObject(events)) {
            kony.sdk.logsdk.error("KonyServerEvents::subscribeServerEvents:: Events to be subscribed for are null or undefined or empty.");
            kony.sdk.verifyAndCallClosure(subscribeOptions.onFailureCallback, "Events to be subscribed for are null or undefined or empty");
            return;
        }

        kony.sdk.claimsRefresh(function () {
            url = kony.sdk.websocket.generateServerEventsURL();

            try {
                events = '{"' + kony.sdk.websocket.constants.WEBSOCKET_SUBSCRIBE_EVENTS + '" : ' + JSON.stringify(events) + '}';
            } catch (error) {
                kony.sdk.logsdk.error("KonyServerEvents::subscribeServerEvents:: Error : ", error);
                kony.sdk.verifyAndCallClosure(subscribeOptions.onFailureCallback, error);
                return;
            }

            kony.sdk.websocket.subscribeServerEvent(url, events, subscribeOptions.onEventCallback, subscribeOptions.onFailureCallback, subscribeOptions);
        }, function (error) {
            kony.sdk.logsdk.error("KonyServerEvents::subscribeServerEvents::onFailure Error:", error);
            kony.sdk.verifyAndCallClosure(subscribeOptions.onFailureCallback, error);
        });
    };

    /**
     * Unsubscribe from server events.
     *
     * example : ["transaction/deposit",
     *            "transaction/withdraw"]
     * @param events             events string to unsubscribe
     * @param unSubscribeOptions additional options to unsubscribe
     */
    kony.sdk.prototype.unSubscribeServerEvents = function (events, unSubscribeOptions) {
        if (!kony.sdk.isNetworkAvailable()) {
            kony.sdk.logsdk.error("No internet available, please check device connectivity.");
            return;
        }

        if (kony.sdk.util.isNullOrUndefinedOrEmptyObject(events)) {
            kony.sdk.logsdk.error("KonyServerEvents::unSubscribeServerEvents:: Events to be unsubscribed for are null or undefined or empty.");
            return;
        }

        try {
            events = '{"' + kony.sdk.websocket.constants.WEBSOCKET_UNSUBSCRIBE_EVENTS + '" : ' + JSON.stringify(events) + '}';
        } catch (error) {
            kony.sdk.logsdk.error("KonyServerEvents::unSubscribeServerEvents:: Error : ", error);
            return;
        }

        kony.sdk.websocket.unSubscribeServerEvent(events, unSubscribeOptions.onCloseCallback, unSubscribeOptions);
    };

    // Initialise serverEvents
    initializeWebSocketConstants();
    initializeWebSocketHandler();

    //#ifdef PLATFORM_SPA
    initializeWebSocketManager();
    //#endif
}
/**
 * Created by Inderpreet Kaur on 3/1/2020.
 * Copyright © 2020 Kony. All rights reserved.
 */

function initializeWebSocketConstants() {
    if (typeof (kony) === "undefined") {
        kony = {};
    }
    if (typeof (kony.sdk) === "undefined") {
        kony.sdk = {};
    }
    kony.sdk.websocket = kony.sdk.websocket || {};
    kony.sdk.websocket.constants = kony.sdk.websocket.constants || {};

    kony.sdk.websocket.constants =
        {
            BOOLEAN_TRUE: true,
            X_KONY_AUTHORIZATION: "X-Kony-Authorization",
            WEBSOCKET_ONERROR_CALLBACK: "onError",
            WEBSOCKET_ONCLOSE_CALLBACK: "onClose",
            WEBSOCKET_ONMESSAGE_CALLBACK: "onMessage",
            WEBSOCKET_PUBLISH_EVENTS: "events",
            WEBSOCKET_SUBSCRIBE_EVENTS: "subscribe",
            WEBSOCKET_UNSUBSCRIBE_EVENTS: "unsubscribe"
        };
}
/**
 * Created by Inderpreet Kaur on 3/1/2020.
 * Copyright © 2020 Kony. All rights reserved.
 */

function initializeWebSocketHandler() {
    kony.sdk.websocket = kony.sdk.websocket || {};

    /**
     *    Generates and returns websocket url.
     */
    kony.sdk.websocket.generateServerEventsURL = function () {
        if (kony.sdk.util.isNullOrUndefinedOrEmptyObject(konyRef.serverEventsUrl)) {
            kony.sdk.logsdk.error("KonyWebSocketManager::generateServerEventsURL:: serverEventsUrl is null or undefined");
            return "";
        }
        var url = konyRef.serverEventsUrl;

        authToken = konyRef.currentClaimToken;
        if (!kony.sdk.util.isNullOrUndefinedOrEmptyObject(authToken)) {
            authToken = kony.sdk.getCurrentInstance().currentClaimToken;
            if (!kony.sdk.util.isNullOrUndefinedOrEmptyObject(authToken)) {
                url = url + "?" + kony.sdk.websocket.constants.X_KONY_AUTHORIZATION + "=" + authToken;
            } else {
                kony.sdk.logsdk.error("KonyWebSocketManager::generateServerEventsURL:: authToken is null or undefined");
                return "";
            }
        }
        return url;
    };

//#ifdef PLATFORM_SPA

    kony.sdk.websocket.publishServerEvent = function (events, publishOptions) {
        var LOG_PREFIX = "kony.sdk.webSocket.publishServerEvents";
        kony.sdk.logsdk.trace(" Entering " + LOG_PREFIX);

        kony.sdk.websocket.util.sendWebsocketMessage(events);
    };

    kony.sdk.websocket.subscribeServerEvent = function (url, events, onMessage, onError, subscribeOptions) {
        var LOG_PREFIX = "kony.sdk.webSocket.subscribeServerEvents";
        kony.sdk.logsdk.trace(" Entering " + LOG_PREFIX);

        function onMessageCallback(result) {
            kony.sdk.logsdk.perf("Executing kony.sdk.websocket.util.onMessage");
            kony.sdk.verifyAndCallClosure(onMessage, result);
            kony.sdk.logsdk.perf("Executing Finished kony.sdk.websocket.util.onMessage");
        }

        function onErrorCallback(error) {
            kony.sdk.logsdk.perf("Executing kony.sdk.websocket.util.onError");
            kony.sdk.logsdk.error("webSocketManager::subscribeServerEvents::onError Error:", error);
            kony.sdk.verifyAndCallClosure(onError, error);
            kony.sdk.logsdk.perf("Executing Finished kony.sdk.websocket.util.onError");
        }

        kony.sdk.websocket.util.setKonyWebsocketCallback({
            "onMessage": onMessageCallback,
            "onError": onErrorCallback
        });
        kony.sdk.websocket.util.openWebSocketHandler(url, events);
    };

    kony.sdk.websocket.unSubscribeServerEvent = function (events, onClose, unSubscribeOptions) {
        var LOG_PREFIX = "kony.sdk.webSocket.unSubscribeServerEvents";
        kony.sdk.logsdk.trace(" Entering " + LOG_PREFIX);

        function onCloseCallback() {
            kony.sdk.logsdk.perf("Executing kony.sdk.websocket.util.onClose");
            kony.sdk.verifyAndCallClosure(onClose);
            kony.sdk.logsdk.perf("Executing Finished kony.sdk.websocket.util.onClose");
        }

        kony.sdk.websocket.util.sendWebsocketMessage(events);

        kony.sdk.websocket.util.setKonyWebsocketCallback({
            "onClose": onCloseCallback
        });

        if (unSubscribeOptions &&
            unSubscribeOptions.closeConnection &&
            unSubscribeOptions.closeConnection === kony.sdk.websocket.constants.BOOLEAN_TRUE) {
            kony.sdk.websocket.util.closeWebSocket();
        }
    };
//#endif

//#ifdef PLATFORM_NATIVE_IOS
// iOS Web socket native bindings
    kony.sdk.websocket.subscribeServerEvent = function (url, events, onMessage, onError, subscribeOptions) {
        var KonyWebSocketObj = kony.sdk.KonyWebSocketClasses.import();
        var KonyWebSocketInstance = KonyWebSocketObj.KonyWebSocket.alloc().jsinit();
        KonyWebSocketInstance.subscribeServerEventsEventsToSendOnMessageOnErrorSubscribeOptions(url, events, onMessage, onError, subscribeOptions);
    };
    kony.sdk.websocket.publishServerEvent = function (events, publishOptions) {
        var KonyWebSocketObj = kony.sdk.KonyWebSocketClasses.import();
        var KonyWebSocketInstance = KonyWebSocketObj.KonyWebSocket.alloc().jsinit();
        KonyWebSocketInstance.publishServerEventsPublishOptions(events, publishOptions);
    };
    kony.sdk.websocket.unSubscribeServerEvent = function (events, onClose, unSubscribeOptions) {
        var KonyWebSocket = kony.sdk.KonyWebSocketClasses.import();
        var KonyWebSocketInstance = KonyWebSocket.KonyWebSocket.alloc().jsinit();
        KonyWebSocketInstance.unSubscribeServerEventsOnCloseCallbackUnSubscribeOptions(events, onClose, unSubscribeOptions);
    };
//#endif

//#ifdef PLATFORM_NATIVE_ANDROID
// android Web socket native bindings

// Converts json to HashMap
    kony.sdk.KonyWebSocketClasses.createHashMapFromJSONObject = function (json, logPrefix) {
        if (!json) {
            return null;
        }

        jsonString = JSON.stringify(json);
        kony.sdk.logsdk.debug(logPrefix + " : " + jsonString);
        JavaClasses = kony.sdk.JavaClasses.import();
        return new JavaClasses.Gson().fromJson(jsonString, JavaClasses.HashMap.class);
    };

    var KonyWebSocketClasses = null;
// Initialization
    kony.sdk.KonyWebSocketClasses.init = function () {
        if (KonyWebSocketClasses == null) {
            KonyWebSocketClasses = kony.sdk.KonyWebSocketClasses.import();
        }
    };

// Creates an instance of onMessageCallback
    kony.sdk.KonyWebSocketClasses.createOnMessageCallback = function (onMessageCallback, logMessage) {
        kony.sdk.KonyWebSocketClasses.init();

        onMessageMethod = function (res) {
            kony.sdk.logsdk.debug(logMessage);
            onMessageCallback(res);
        };

        omc = new KonyWebSocketClasses.OnMessage();
        omc.OnMessage = onMessageMethod;
        omc.onMessageLog = "onMessage";

        return omc;
    };

// Creates an instance of IOnErrorCallback
    kony.sdk.KonyWebSocketClasses.createOnErrorCallback = function (onErrorCallback, logMessage) {
        kony.sdk.KonyWebSocketClasses.init();

        onErrorMethod = function (err) {
            kony.sdk.logsdk.debug(logMessage);
            onErrorCallback(err);
        };

        ecb = new KonyWebSocketClasses.OnError();
        ecb.OnError = onErrorMethod;
        ecb.onErrorLog = "onError";
        return ecb;
    };

// Creates an instance of IOnCloseCallback
    kony.sdk.KonyWebSocketClasses.createOnCloseCallback = function (onCloseCallback, logMessage) {
        kony.sdk.KonyWebSocketClasses.init();

        onCloseMethod = function (res) {
            kony.sdk.logsdk.debug(logMessage);
            onCloseCallback(res);
        };

        ccb = new KonyWebSocketClasses.OnClose();
        ccb.OnClose = onCloseMethod;
        ccb.onCloseLog = "onClose";
        return ccb;
    };

    kony.sdk.websocket.publishServerEvent = function (events, publishOptions) {
        var LOG_PREFIX = "kony.sdk.webSocket.publishServerEvents";
        kony.sdk.logsdk.trace(" Entering " + LOG_PREFIX);

        kony.sdk.KonyWebSocketClasses.init();
        publishOptionsMap = kony.sdk.KonyWebSocketClasses.createHashMapFromJSONObject(publishOptions, "publishOptions");
        KonyWebSocketClasses.KonyWebSocketInterface.PublishServerEvents(events, publishOptionsMap);
    };

    kony.sdk.websocket.subscribeServerEvent = function (url, events, onMessage, onError, subscribeOptions) {
        var LOG_PREFIX = "kony.sdk.webSocket.subscribeServerEvent";
        kony.sdk.logsdk.trace(" Entering " + LOG_PREFIX);

        kony.sdk.KonyWebSocketClasses.init();
        onMessageCallback = kony.sdk.KonyWebSocketClasses.createOnMessageCallback(onMessage, "SubscribeServerEvents, onMessage Callback");
        onErrorCallback = kony.sdk.KonyWebSocketClasses.createOnErrorCallback(onError, "SubscribeServerEvents, onError Callback");

        subscribeOptionsMap = kony.sdk.KonyWebSocketClasses.createHashMapFromJSONObject(subscribeOptions, "subscribeOptions");
        KonyWebSocketClasses.KonyWebSocketInterface.SubscribeServerEvents(url, events, onMessageCallback, onErrorCallback, subscribeOptionsMap);
    };

    kony.sdk.websocket.unSubscribeServerEvent = function (events, onClose, unSubscribeOptions) {
        var LOG_PREFIX = "kony.sdk.webSocket.unSubscribeServerEvent";
        kony.sdk.logsdk.trace(" Entering " + LOG_PREFIX);

        kony.sdk.KonyWebSocketClasses.init();
        onCloseCallback = kony.sdk.KonyWebSocketClasses.createOnCloseCallback(onClose, "SubscribeServerEvents, onError Callback");

        unSubscribeOptionsMap = kony.sdk.KonyWebSocketClasses.createHashMapFromJSONObject(unSubscribeOptions, "unSubscribeOptions");
        KonyWebSocketClasses.KonyWebSocketInterface.UnSubscribeServerEvents(events, onCloseCallback, unSubscribeOptionsMap);
    };
//#endif

//#ifdef PLATFORM_NATIVE_WINDOWS
    kony.sdk.websocket.publishServerEvent = function () {
        kony.sdk.logsdk.warn("kony.sdk.websocket.publishServerEvents:: Websocket is not supported for windows");
        return null;
    };

    kony.sdk.websocket.subscribeServerEvent = function () {
        kony.sdk.logsdk.warn("kony.sdk.websocket.subscribeServerEvents:: Websocket is not supported for windows");
        return null;
    };

    kony.sdk.websocket.unSubscribeServerEvent = function () {
        kony.sdk.logsdk.warn("kony.sdk.websocket.unSubscribeServerEvents:: Websocket is not supported for windows");
        return null;
    };
//#endif
}
//#ifdef PLATFORM_SPA
/**
 * Created by Inderpreet Kaur on 3/1/2020.
 * Copyright © 2020 Kony. All rights reserved.
 */
function initializeWebSocketManager() {
    if (typeof(kony) === "undefined") {
        kony = {};
    }
    if (typeof(kony.sdk) === "undefined") {
        kony.sdk = {};
    }
    kony.sdk.websocket = kony.sdk.websocket || {};
    kony.sdk.websocket.util = kony.sdk.websocket.util || {};

    kony.sdk.websocket.onMessage = null;
    kony.sdk.websocket.onError = null;
    kony.sdk.websocket.onClose = null;

    var konyWebSocket = null;

    /**
     *    Defines callback for onMessage, onError and onClose for websockets.
     */
    kony.sdk.websocket.util.setKonyWebsocketCallback = function(callback) {
        kony.sdk.logsdk.perf("Executing kony.sdk.websocket.util.setKonyWebsocketCallback");
        for (var key in callback) {
            switch (key) {
                case kony.sdk.websocket.constants.WEBSOCKET_ONMESSAGE_CALLBACK:
                    kony.sdk.websocket.onMessage = callback[key];
                    break;
                case kony.sdk.websocket.constants.WEBSOCKET_ONERROR_CALLBACK:
                    kony.sdk.websocket.onError = callback[key];
                    break;
                case kony.sdk.websocket.constants.WEBSOCKET_ONCLOSE_CALLBACK:
                    kony.sdk.websocket.onClose = callback[key];
                    break;
                default:
                    kony.sdk.logsdk.warn("Invalid key passed while setting websocket callback : ", key);
            }
        }
        kony.sdk.logsdk.perf("Executing Finished kony.sdk.websocket.util.setKonyWebsocketCallback");
    };

    /**
     *    Opens and handles websocket after the connection is established.
     */
    kony.sdk.websocket.util.openWebSocketHandler = function(url, events) {
        kony.sdk.logsdk.perf("Executing kony.sdk.websocket.util.openWebSocketHandler");

        if ((!kony.sdk.isNullOrUndefined(konyWebSocket) && (konyWebSocket.readyState === WebSocket.OPEN))) {
            kony.sdk.logsdk.info("Websocket already initialized, sending message.");
            kony.sdk.websocket.util.sendWebsocketMessage(events);
            return;
        }

        if (window.WebSocket) {
            kony.sdk.logsdk.info("Websocket is supported by this browser!");
            try {
                konyWebSocket = new WebSocket(url);
            } catch (error) {
                kony.sdk.logsdk.error("openWebSocketHandler:: Error while opening websocket, Error : ", error);
                kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onError, error);
                return;
            }

            konyWebSocket.onopen = function(event) {
                kony.sdk.logsdk.perf("Executing Websocket onopen eventHandler.");
                kony.sdk.logsdk.info("WebSocket is connected.");
                kony.sdk.websocket.util.sendWebsocketMessage(events);
                kony.sdk.logsdk.perf("Executing Finished Websocket onopen eventHandler.");
            };

            konyWebSocket.onmessage = function(event) {
                kony.sdk.logsdk.perf("Executing Websocket onmessage eventHandler.");
                var message = event.data;
                kony.sdk.logsdk.info("Event Message Recieved.");
                kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onMessage, message);
                kony.sdk.logsdk.perf("Executing Finished Websocket onmessage eventHandler.");
            };

            konyWebSocket.onerror = function(error) {
                kony.sdk.logsdk.perf("Executing Websocket onerror eventHandler.");
                kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onError, error);
                kony.sdk.logsdk.perf("Executing Finished Websocket onerror eventHandler.");
            };

            konyWebSocket.onclose = function(event) {
                kony.sdk.logsdk.perf("Executing Websocket onclose eventHandler.");
                konyWebSocket = null;
                kony.sdk.logsdk.info("WebSocket is closed.");
                kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onClose);
                kony.sdk.logsdk.perf("Executing Finished Websocket onclose eventHandler.");
            };
        } else {
            kony.sdk.logsdk.error("openWebSocketHandler:: WebSocket is not supported by this browser.");
            kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onError, "WebSocket is not supported by this browser");
        }
        kony.sdk.logsdk.perf("Executing Finished kony.sdk.websocket.util.openWebSocketHandler");
    };

    /**
     *    Sends message through the WebSocket connection using WebSocket instance.
     */
    kony.sdk.websocket.util.sendWebsocketMessage = function(events) {
        kony.sdk.logsdk.perf("Executing kony.sdk.websocket.util.sendWebsocketMessage");
        if (kony.sdk.isNullOrUndefined(konyWebSocket)) {
            kony.sdk.logsdk.error("sendWebsocketMessage:: Websocket is not initialized.");
            kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onError, "Websocket is not initialized");
            return;
        } else {
            kony.sdk.logsdk.info("Websocket ready State is " + konyWebSocket.readyState);
            if (konyWebSocket.readyState === WebSocket.OPEN) {
                kony.sdk.logsdk.info("Sending events using webSocket.");
                konyWebSocket.send(events);
            } else {
                kony.sdk.logsdk.error("sendWebsocketMessage:: Websocket is not open.");
                kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onError, "Websocket is not open");
                return;
            }
        }
        kony.sdk.logsdk.perf("Executing Finished kony.sdk.websocket.util.sendWebsocketMessage");
    };

    /**
     *    Closes the WebSocket connection, if any. If the connection is already closed, this api does nothing.
     */
    kony.sdk.websocket.util.closeWebSocket = function() {
        kony.sdk.logsdk.perf("Executing Finished kony.sdk.websocket.util.ned) {closeWebSocket");
        if (kony.sdk.isNullOrUndefined(konyWebSocket)) {
            kony.sdk.logsdk.error("closeWebSocket:: Websocket is not initialized.");
            kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onError, "Websocket is not initialized");
            return;
        } else {
            kony.sdk.logsdk.info("Websocket ready State is " + konyWebSocket.readyState);
            if (konyWebSocket.readyState === WebSocket.OPEN) {
                kony.sdk.logsdk.info("Closing the Websocket.");
                konyWebSocket.close();
            } else {
                kony.sdk.logsdk.error("closeWebSocket:: Websocket is not open.");
                kony.sdk.verifyAndCallClosure(kony.sdk.websocket.onError, "Websocket is not open");
            }
        }
        kony.sdk.logsdk.perf("Executing Finished kony.sdk.websocket.util.closeWebSocket");
    };
}
//#endif
