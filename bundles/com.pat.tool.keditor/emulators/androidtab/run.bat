@echo off
SET P1=%1
SET P2=%2
SHIFT
SHIFT
:: %P1% - avd name
:: %P2% - APK name
:: %1 - Application name
:: %2 - command line args to emulator
:: %3 - package name
:: %4 - serialnumber
:: %5 - profileapp
:: %6 - development language
:: %7 - Web selected
:: %8 - Jetty IP
:: %9 - Jetty Port

echo AVD: %P1%
echo APK: %P2%
echo AppId: %1
echo Emulator arguments: %2
echo Package: %3
echo serialnumber: %4
echo profileapp: %5
echo devlang: %6
echo Web Selected: %7
 
:: Added static vals 
set avdname=%P1%
set apkname=%P2%
set appidname=%1
set cmdargs=%2
set cmdargs=%cmdargs:~1,-1%
set pakname=%3
set serialnumber=%4
set profileapp=%5
set devlang=%6
set web.selected=%7
set jetty.ipaddress=%8
set jetty.portnum=%9
set profile_file=profiler_%pakname%.txt  
 
echo adb.exe
echo %pakname%
echo %serialnumber% > device
findstr /r "emulator" device
if %ERRORLEVEL% == 1 goto DEVICE_READY 


set portno=%serialnumber:~9%
echo %portno%
adb.exe kill-server
echo Launching emulator

:START_DEVICE
start /min emulator.exe -avd %avdname% %cmdargs% -port %portno%
if %web.selected% == true goto START_WITHOUT_APK_CHECK 
IF NOT EXIST %apkname% (
echo apk does not exists
goto END
)
 
:START_WITHOUT_APK_CHECK  
adb.exe start-server
 
adb.exe -s %serialnumber% get-serialno | find "emulator"
if not errorlevel 1 goto DEVICE_READY 
adb.exe -s %serialnumber% get-serialno
 
echo Waiting for HOME to launch
:WAIT_FOR_DEVICE
echo Waiting.. 
ping 127.0.0.1 -n 2 -w 2000 > null

adb.exe -s %serialnumber% logcat -d *:* | find "HOME" 
if errorlevel 1 goto WAIT_FOR_DEVICE
if not errorlevel 1 goto DEVICE_READY
 
:DEVICE_READY
if %web.selected% == true goto START_WEBAPP 
echo Uninstalling kony application
adb.exe -s %serialnumber% uninstall %pakname%

:: Always try to delete profile file irrespective of end user selection.
adb -s %serialnumber% shell rm /sdcard/%profile_file%

::value is true from IDE in case of Profiler  push file to the sdcard.

IF "%profileapp%" == "true" goto SET_PROFILE
IF "%profileapp%" == "false" goto INSTALL_APP

:SET_PROFILE 
echo Setting Profiler application
echo %profile_file%
adb -s %serialnumber% shell "echo > /sdcard/%profile_file%"
goto INSTALL_APP

:INSTALL_APP
echo Installing kony application
adb.exe -s %serialnumber% install -r %apkname%
echo Starting kony application
if "%appidname%" == "Preview" (goto LAUNCH_PREVIEW_APP) else (goto LAUNCH_REGULAR_APP)

:LAUNCH_PREVIEW_APP
adb.exe -s %serialnumber% shell am start -a android.intent.action.MAIN -n %pakname%/%pakname%.%appidname% -e DevLang %devlang%
goto END

:LAUNCH_REGULAR_APP
adb.exe -s %serialnumber% shell am start -a android.intent.action.MAIN -n %pakname%/%pakname%.%appidname%
goto END
:START_WEBAPP
if "%appidname%" == "Preview" goto :START_PREVIEWWEBAPP
adb -s %serialnumber% shell am start -a android.intent.action.VIEW -d  %jetty.ipaddress%/%appidname%/p
goto END
:START_PREVIEWWEBAPP
adb -s %serialnumber% shell am start -a android.intent.action.VIEW -d http://%jetty.ipaddress%:%jetty.portnum%/preview/p
:END 
pause
 
exit
