#!/bin/bash
echo "Entered shell script"
# $1 - avd name
# $2 - APK name
# $3 - Application name
# $4 - command line args to emulator
# $5 - package name
# $6 - serialnumber
# $7 - profileapp
# $8 - development language


 function function_END()
{
echo "=======came to func_end======="
sleep 10s
echo "****it slept for 10 secs****"
exit
}



function function_1()
{
echo "=======came to func_1======="
echo "apk does not exist"
function_END
}



function function_LAUNCH_PREVIEW_APP()
{
echo "=======came to func_LAUNCH_PREVIEW_APP======="
adb -s $serialnumber shell am start -a android.intent.action.MAIN -n $pakname/$pakname.$appidname -e DevLang $devlang
function_END
}


function function_LAUNCH_REGULAR_APP()
{
echo "=======came to func_LAUNCH_REGULAR_APP======="
adb -s $serialnumber shell am start -a android.intent.action.MAIN -n $pakname/$pakname.$appidname
function_END
}



function function_INSTALL_APP()
{
echo "=======came to func_INSTALL_APP======="
echo "Installing kony application"
adb -s $serialnumber install -r $apkname
echo "Starting kony application"
if [ "$appidname" = "Preview" ]; then
function_LAUNCH_PREVIEW_APP
else 
function_LAUNCH_REGULAR_APP
fi
}

function function_START_WEBAPP()
{
echo "=======came to func_START_WEBAPP======="
if [ "$appidname" = "Preview" ]; then
function_START_PREVIEWWEBAPP
else
adb -s $serialnumber shell am start -a android.intent.action.VIEW -d "$jetty_ipaddress/$appidname/p"
function_END
fi
}

function function_START_PREVIEWWEBAPP()
{
adb -s $serialnumber shell am start -a android.intent.action.VIEW -d "http://$jetty_ipaddress:$jetty_portnum/preview/p"
function_END
}


function function_SET_PROFILE()
{
echo "=======came to func_SET_PROFILE======="
echo "Setting Profiler application"
echo $profile_file
# adb -s $serialnumber shell "echo > /sdcard/$profile_file"
function_INSTALL_APP
}



: '
function function_DEVICE_READY()
{
echo "=======came to func_DEVICE_READY======="
echo "Uninstalling kony application"
adb -s $serialnumber uninstall $pakname

# Always try to delete profile file irrespective of end user selection.
# adb -s $serialnumber shell rm /sdcard/$profile_file

#value is true from IDE in case of Profiler  push file to the sdcard.

if [ "$profileapp" = "true" ]; then
function_SET_PROFILE
fi

if [ "$profileapp" = "false" ]; then
function_INSTALL_APP
fi

function_END
}
'


function function_WAIT_FOR_DEVICE()
{
echo "=======came to func_WAIT_FOR_DEVICE======="
echo "Waiting.."
#ping 127.0.0.1 -n 2 -w 2000 > null

 adb -s $serialnumber logcat -d *:* | grep "HOME"
if [ $? != 0 ]; then 
function_WAIT_FOR_DEVICE
else
echo "*****Ready******"
function_DEVICE_READY
fi
}


function function_DEVICE_READY()
{
echo "=======came to func_DEVICE_READY======="
 
echo "Uninstalling kony application"



# adb -s $serialnumber uninstall $pakname


# Always try to delete profile file irrespective of end user selection.
#adb -s $serialnumber shell rm /sdcard/$profile_file

#value is true from IDE in case of Profiler  push file to the sdcard.

echo "********abt to call set profile*********"

if [ "$profileapp" = "true" ]; then
function_SET_PROFILE
fi

if [ "$profileapp" = "false" ]; then
echo "WEB SELECTED.. $web_selected"
if [ "$web_selected" = "true" ]; then
function_START_WEBAPP
else
function_INSTALL_APP
fi
fi

function_END

}




androidsdk=${11}
export PATH=$androidsdk/emulator:$androidsdk/tools:$androidsdk/platform-tools:$PATH
echo "AVD:" $1
echo "APK:" $2
echo "AppId:" $3
echo "Emulator arguments:" $4
echo "Package:" $5
echo "serialnumber:" $6
echo "profileapp:" $7
echo "devlang:" $8

#Added static vals
echo "******static vars*******"
avdname=$1
apkname=$2
appidname=$3
cmdargs=$4
echo $cmdargs
cmdargs=`echo $cmdargs | sed 's/^[ \t]*//;s/[ \t]*$//'`
cmdargs=$cmdargs
pakname=$5
serialnumber=$6
profileapp=$7
devlang=$8
web_selected=$9
jetty_ipaddress=${10}
jetty_portnum=${11}

echo "*******static vars set******"
echo "*******'$jetty_ipaddress/$appidname'******"
echo "*******$jetty_portnum ******"
profile_file=profiler_$pakname.txt

echo "********profile file set********"
echo "***** pt1*****"

echo adb
echo $pakname
echo $serialnumber > device

echo "******pt2******"
grep "emulator" device
if [ $? != 0 ]; then
echo "about to call func_device_ready"
function_DEVICE_READY
fi


portno=${serialnumber:9}
echo portnum:$portno
echo cmdargs:$cmdargs

echo "Launching emulator"
adb start-server


#START_DEVICE
emulator -avd $avdname $cmdargs -port $portno &

#if [! -f $apkname]; then
#function_1
#fi


echo "Starting avd"


#adb start-server

echo "Started avd printing devies ..."
adb devices

adb -s $serialnumber get-serialno | grep "emulator"
if [ $? = 0 ]; then 
echo "******Emulator found*******"
function_DEVICE_READY
fi
echo "else case"
#adb -s $serialnumber get-serialno
echo "Waiting for HOME to launch"
function_WAIT_FOR_DEVICE