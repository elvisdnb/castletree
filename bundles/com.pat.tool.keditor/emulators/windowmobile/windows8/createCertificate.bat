set PUBLISHERID=%~1

if "%PUBLISHERID%" == "" (
	goto failed 
)
if "%PUBLISHERID%" == "Kony" (
	goto failed
)
DEL *.cer /Q
DEL *.pvk /Q

Echo Finding Programfiles path
if "%PROCESSOR_ARCHITECTURE%"=="AMD64" goto 64bit
echo 32 bit OS
set ProgRoot=%ProgramFiles%
goto CreateCer

:64bit
echo 64 bit OS
set ProgRoot=%ProgramFiles(x86)%

:CreateCer
ver | findstr /i "6\.3\." > nul
IF %ERRORLEVEL% EQU 0 goto ver_Win81
goto verWin8OrWin7

:ver_Win81
Echo creating certificate on windows 8.1
set makeCertPath="%ProgRoot%\Windows Kits\8.1\bin\x86\makecert.exe"
goto certification

:verWin8OrWin7
ver | findstr /i "6\.2\." > nul
IF %ERRORLEVEL% EQU 0 goto ver_Win8
goto ver_Win7

:ver_Win8
Echo creating certificate on windows 8
set makeCertPath="%ProgRoot%\Windows Kits\8.0\bin\x86\makecert.exe"
goto certification

:ver_Win7
Echo creating certificate on windows 7
set makeCertPath="%ProgRoot%\Microsoft SDKs\Windows\v7.0A\Bin\makecert.exe"
goto certification

:certification
if NOT EXIST %makeCertPath% (
	echo makecert is not found at %makeCertPath%.
	goto failed
)

%makeCertPath% -n "CN=%PUBLISHERID%" -r -a sha1 -sv "%PUBLISHERID%.pvk" "%PUBLISHERID%.cer" -ss root
if errorlevel 1 goto failed

%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe -NonInteractive -File AddCert2Store.ps1
if errorlevel 1 goto failed

%makeCertPath% -a sha1 -iv "%PUBLISHERID%.pvk" -n "CN=%PUBLISHERID%" -ic "%PUBLISHERID%.cer" -sr currentuser -ss My
if errorlevel 1 goto failed
goto exit

:failed
echo Certificare creation is failed
exit /B 234
PAUSE

:exit
echo Certificare creation is completed