SET P1=%1
SHIFT
SET P2=%1
SHIFT
SET P3=%1
SHIFT
SET P4=%1
SHIFT
PUSHD %P1%
convert -size 15x%8   xc:"rgba(%1,%2,%3, %P3%)"   color_rgba_1.png
convert -size 15x%8   xc:"rgba(%4,%5,%6, %P4%)"   color_rgba_2.png
convert color_rgba_1.png[15x%7+0+0] color_rgba_2.png[15x%7+0+0] -append +repage color_rgba.png
convert -size 15x%7 xc:none -fill white -draw "roundRectangle 0,0 14,%7 0,0" color_rgba.png -compose SrcIn -composite %P2%.png
del color_rgba_1.png
del color_rgba_2.png
del color_rgba.png
POPD
GOTO:EOF