PUSHD %1
convert -size 15x%3 xc:none -fill white -draw "roundRectangle 0,0 14,%4 5,5" t_img.png -compose SrcIn -composite %2.png
del t_img.png
POPD
GOTO:EOF