export PATH=$PATH:$8/bin
export DYLD_LIBRARY_PATH="$8/lib/"
P1=$1
shift
P2=$1
shift
k=${P1//\\/\/}
cd $k
convert -size 5x$P2 gradient:'rgba('$2','$4')'-'rgba('$3','$5')' $1.png
