#!/bin/bash
echo ${10}
export PATH=$PATH:${10}/bin:/bin:/usr/bin
export DYLD_LIBRARY_PATH="${10}/lib/"
P1=$1
shift
P2=$1
shift
P3=$1
shift
P4=$1
shift
echo $P1
k=${P1//\\/\/}
echo $k
cd $k
convert -size 15x$4   xc:"rgba($1, $P3)"   color_rgba_1.png
convert -size 15x$4   xc:"rgba($2, $P4)"   color_rgba_2.png
convert color_rgba_1.png[15x$3+0+0] color_rgba_2.png[15x$3+0+0] -append +repage color_rgba.png
convert -size 15x$3 xc:none -fill white -draw "roundRectangle 0,0 14,$3 0,0" color_rgba.png -compose SrcIn -composite $P2.png
rm color_rgba_1.png
rm color_rgba_2.png
rm color_rgba.png