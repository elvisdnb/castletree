SET P1=%1
SHIFT
PUSHD %P1%
convert -size 15x%7 gradient:rgba(%1,%2,%3,%8)-rgba(%4,%5,%6,%9) t_img.png
POPD
GOTO:EOF