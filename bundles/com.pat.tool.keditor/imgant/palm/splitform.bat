SET P1=%1
SET P2=%2
SHIFT
SHIFT
PUSHD %P1%
convert -size 1x2 gradient:rgba(%1,%2,%3,%8)-rgba(%4,%5,%6,%9) -scale 320x400 %P2%.png
POPD 
GOTO:EOF


