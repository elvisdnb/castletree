@echo off
REM
REM  + %3	
REM     |---+ gad
REM		|--- + app
REM			|--- + assistants
REM			|--- + models
REM			|--- + views
REM			| <js files>
REM		|--- + images
REM		|--- + stylesheets
REM		|--- + resources
REM 			|-- + en_us
REM 				|-- strings.json
REM			    + es_es
REM				|-- strings.json
REM		|--- <app package related files>
REM
REM

REM  usage :-    palmgenpkg.bat  gad  tmp/output  tmp/build/luapalm

SETLOCAL ENABLEDELAYEDEXPANSION
echo appname %1 srcdir %2  dstdir %3 locales %4

set appname=%1
set srcdir=%2
set dstdir=%3
set locale=%4

if exist %3\%1 (
    if exist %3\%1_backup (
	rmdir /s/q %3\%1_backup
    )
    ren %3\%1 %1_backup
    rmdir /s/q %3\%1
)

REM -- create dir structure

mkdir %3\%1\app\assistants
mkdir %3\%1\app\models
mkdir %3\%1\app\views
mkdir %3\%1\app\views\templates
mkdir %3\%1\app\views\alert
mkdir %3\%1\app\views\splash
mkdir %3\%1\app\views\spinner

mkdir %3\%1\images
mkdir %3\%1\stylesheets



REM -- copy files

copy %2\*assistant.js %3\%1\app\assistants
copy %2\sources.json  %3\%1
copy %2\index.html    %3\%1
copy %2\konymodel.js  %3\%1\app\models
copy %2\kony.css      %3\%1\stylesheets
copy %2\appinfo.json     %3\%1

copy %3\inputdata\*.js     %3\%1\app
del %3\%1\app\stage-assistant.js

copy %3\inputdata\*-assistant.js %3\%1\app\assistants

copy %3\palm\bc.js %3\%1\app

copy %3\inputdata\framework_config.json %3\%1
copy %3\inputdata\icon.png %3\%1
copy %3\inputdata\listTemplate.html %3\%1\app\views\templates
copy %3\inputdata\divider.html %3\%1\app\views\templates
copy %3\inputdata\alert-scene.html %3\%1\app\views\alert
copy %3\inputdata\splash-scene.html %3\%1\app\views\splash
copy %3\inputdata\spinner-scene.html %3\%1\app\views\spinner
copy %3\inputdata\fade-arrow-left.png %3\%1\images
copy %3\inputdata\fade-arrow-right.png %3\%1\images
copy %3\inputdata\fade-arrow-up.png %3\%1\images
copy %3\inputdata\fade-arrow-down.png %3\%1\images
copy %3\inputdata\radiobutton-first.png %3\%1\images
copy %3\inputdata\radiobutton-last.png %3\%1\images
copy %3\inputdata\radiobutton-middle.png %3\%1\images
copy %3\inputdata\ticked.png %3\%1\images
copy %3\inputdata\unticked.png %3\%1\images
copy %3\inputdata\wait.gif %3\%1\images

REM -- Copy Image Files 

copy %3\images\*.png %3\%1
copy %3\images\*.gif %3\%1
copy %3\images\*.jpeg %3\%1
copy %3\images\*.jpg %3\%1

copy %3\bgimages\palm\*.* %3\%1

del %3\%1\app\*-assistant.js

REM  --  %2\*.html   %3\%1\app\views\*\*.html

move %2\*template.html %3\%1\app\views\templates

for /f %%x in ('dir /b %2\*.html') do (
	for /f "tokens=1 delims=-" %%a in ("%%~nx") do (
     		mkdir %3\%1\app\views\%%a
     		copy %2\%%x  %3\%1\app\views\%%a
	)
)

set locale=%~4
if not "%locale%"=="" CALL :Locali 
pause

:Locali
:: Create the resources directory and the locale specific dirs and copy corresponding strings.json
echo "$$$$$$$In locali"
echo locales = %4; >> %3\%1\app\models\konymodel.js
SET _SAMPLE=%locale%
CALL :LCase _SAMPLE locale

rmdir /s/q %dstdir%\%appname%\resources\
echo "Locale after case conversion is"
echo %locale%
mkdir %dstdir%\%appname%\resources\
for /f "tokens=1-20 delims=," %%a in ("%locale%") do ( 
if not "%%a"=="" CALL :rescopy %%a
if not "%%b"=="" CALL :rescopy %%b
if not "%%c"=="" CALL :rescopy %%c
if not "%%d"=="" CALL :rescopy %%d
if not "%%e"=="" CALL :rescopy %%e
if not "%%f"=="" CALL :rescopy %%f
if not "%%g"=="" CALL :rescopy %%g
if not "%%h"=="" CALL :rescopy %%h
if not "%%i"=="" CALL :rescopy %%i
)
GOTO:EOF

:LCase
:UCase
::Picked from URL:http://www.robvanderwoude.com/battech_convertcase.php
:: Converts to upper/lower case variable contents
:: Syntax: CALL :UCase _VAR1 _VAR2
:: Syntax: CALL :LCase _VAR1 _VAR2
:: _VAR1 = Variable NAME whose VALUE is to be converted to upper/lower case
:: _VAR2 = NAME of variable to hold the converted value
:: Note: Use variable NAMES in the CALL, not values (pass "by reference")

SET _UCase=A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
SET _LCase=a b c d e f g h i j k l m n o p q r s t u v w x y z
SET _Lib_UCase_Tmp=!%1!
IF /I "%0"==":UCase" SET _Abet=%_UCase%
IF /I "%0"==":LCase" SET _Abet=%_LCase%
FOR %%Z IN (%_Abet%) DO SET _Lib_UCase_Tmp=!_Lib_UCase_Tmp:%%Z=%%Z!
SET %2=%_Lib_UCase_Tmp%
GOTO:EOF

:rescopy
::Subroutine to create the locale dir and copy the resource file
echo "@@@@@@@@@in rescopy"
mkdir %dstdir%\%appname%\resources\%1 
copy %dstdir%\inputdata\%1.prop %dstdir%\%appname%\resources\%1\strings.json
copy %dstdir%\images\%1\*.* %dstdir%\%appname%\resources\%1
GOTO:EOF