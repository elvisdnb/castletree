export PATH=$PATH:$7/bin
export DYLD_LIBRARY_PATH="$7/lib/"
export P1=$1
shift
echo $P1
k=${P1//\\/\/}
echo $k
cd $k
c1='rgba('$1','$4')'
c2='rgba('$2','$5')'
convert -size 33x51 gradient:$c1-$c2 t_img.png