@echo on

echo appname %1 srcdir %2  dstdir %3

call "C:\Program Files\Palm\SDK\bin\palm-package" %1

mkdir "C:\palmprepkg\"

del /F com.kony.app.ipk
rename *.ipk com.kony.app.ipk

set appid=%1
for %%c in (Aa Bb Cc Dd Ee Ff Gg Hh Ii Jj Kk Ll Mm Nn Oo Pp Qq Rr Ss Tt Uu Vv Ww Xx Yy Zz) do call :convert %%c

copy com.kony.app.ipk "C:\palmprepkg\"
call "C:\Program Files\Palm\SDK\bin\palm-install" -r com.kony.%appid% 
call "C:\Program Files\Palm\SDK\bin\palm-install"  "C:\palmprepkg\com.kony.app.ipk"
call "C:\Program Files\Palm\SDK\bin\palm-launch" com.kony.%appid%
pause

:convert
set LETTER=%1
set UPPER=%LETTER:~0,1%
set LOWER=%LETTER:~1,2%
call set tSTRING=%%appid:%UPPER%=%LOWER%%%
set appid=%tSTRING%
goto :EOF
