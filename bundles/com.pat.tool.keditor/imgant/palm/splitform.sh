#!/bin/bash
export PATH=$PATH:$8/bin/
export DYLD_LIBRARY_PATH="$8/lib/"
P1=$1
P2=$2
shift
shift
echo $P1
k=${P1//\\/\/}
echo $k
cd $k
convert -size 15x$3 gradient:'rgba('$1','$4')'-'rgba('$2','$5')' -scale 320x400 $P2.png
