#!/bin/bash
export PATH=$PATH:$7/bin
export DYLD_LIBRARY_PATH="$7/lib/"
P1=$1
shift
echo $P1
k=${P1//\\/\/}
echo $k
cd $k
convert -size 7x40 gradient:'rgba('$2','$4')'-'rgba('$3','$5')' $1.png
