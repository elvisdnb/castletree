export PATH=$PATH:$7/bin
export DYLD_LIBRARY_PATH="$7/lib/"
P1=$1
shift
echo $P1
k=${P1//\\/\/}
echo $k
cd $k
convert -size 15x$3 gradient:'rgba('$1','$4')'-'rgba('$2','$5')' t_img.png