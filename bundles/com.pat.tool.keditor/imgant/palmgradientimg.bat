SET P1=%1
SHIFT
PUSHD %P1%
convert -size 5x30 gradient:rgba(%2,%3,%4,%8)-rgba(%5,%6,%7,%9) %1.png
POPD
GOTO:EOF