package com.pat.tool.keditor.dialogs;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.kony.studio.StudioContext;
import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.preferences.IPreferenceConstants;
import com.pat.tool.keditor.utils.KConstants;
import com.pat.tool.keditor.utils.KNUtils;
import com.pat.tool.keditor.utils.ProjectProperties;
import com.pat.tool.keditor.wizards.ProjectPropertiesWizardPage;

/**
 *@author Surendra
 *@since 7.2
 */
public class CopyActionsWarningDialog extends MessageDialog {

	private Button checkBox;
	public static boolean showDialog = false; 
	public static final String COPY_ACTIONS_WARNING = "copy_actions_warning";
	
	public CopyActionsWarningDialog(Shell parentShell, String dialogTitle,
			Image dialogTitleImage, String dialogMessage, int dialogImageType,
			String[] dialogButtonLabels, int defaultIndex) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage,
				dialogImageType, dialogButtonLabels, defaultIndex);
		
	}

	@Override
	protected Control createCustomArea(Composite parent) {
		checkBox = new Button(parent,SWT.CHECK);
		checkBox.setText("Do not show again.");
		GridData data = new GridData();
		data.verticalIndent = 6;
		data.horizontalIndent = 6;
		checkBox.setLayoutData(data);
		final IPreferenceStore prefStore = KEditorPlugin.getDefault().getPreferenceStore();
		checkBox.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean selection = checkBox.getSelection();
				prefStore.setValue(IPreferenceConstants.COPY_ACTIONS, !selection);
				
			}
		});
		return checkBox;
	}
	
	

}
