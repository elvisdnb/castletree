package com.pat.tool.keditor.dialogs;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import com.kony.studio.build.packager.KonyBuildManager;
import com.pat.tool.keditor.build.BuildConstants;
import com.pat.tool.keditor.utils.UIUtils;

public class BuildCompleteDialog extends MessageDialog {
	
	private Map<String, String> platformStatus = new LinkedHashMap<String, String>();
	private String fontName = "Arial";
	private int fontHeight = 10;
	private static String INFO_TITLE = "Information";

	public BuildCompleteDialog(Shell shell, String message, Map<String, String> platformStatus) {
		this(shell, INFO_TITLE, message, false);
		this.platformStatus = platformStatus;
		if (KonyBuildManager.universalIos) {
			if (platformStatus.containsKey("iPad")
					&& platformStatus.containsKey("iPhone")) {
				if (platformStatus.get("iPhone").equals(BuildConstants.SUCCESS)
						&& platformStatus.get("iPad").equals(
								BuildConstants.SUCCESS)) {
					platformStatus.put("iOS Universal", BuildConstants.SUCCESS);
					platformStatus.remove("iPhone");
					platformStatus.remove("iPad");
				} else {
					platformStatus.put("iOS Universal", BuildConstants.FAILED);
					platformStatus.remove("iPhone");
					platformStatus.remove("iPad");
				}
			}
		}
		if (KonyBuildManager.universalAndroid) {
			if (platformStatus.containsKey("Android")
					&& platformStatus.containsKey("Android Tablet")) {
				if (platformStatus.get("Android")
						.equals(BuildConstants.SUCCESS)
						&& platformStatus.get("Android Tablet").equals(
								BuildConstants.SUCCESS)) {
					platformStatus.put("Android Universal",
							BuildConstants.SUCCESS);
					platformStatus.remove("Android");
					platformStatus.remove("Android Tablet");
				}
				else{
					platformStatus.put("Android Universal",
							BuildConstants.FAILED);
					platformStatus.remove("Android");
					platformStatus.remove("Android Tablet");
				}
			}
		}
	}
	
	public BuildCompleteDialog(Shell shell, String title, String message, boolean disableRename) {
		super(shell, title, null, message, MessageDialog.INFORMATION, new String[]{"OK"}, IDialogConstants.OK_ID);
		setBlockOnOpen(true);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite comp = (Composite)super.createDialogArea(parent);
		Composite customArea = new Composite(comp, SWT.NONE & SWT.SHEET);
		GridLayoutFactory.fillDefaults().margins(5, 5).numColumns(4).spacing(20, 6).applyTo(customArea);
		Font systemFont = Display.getDefault().getSystemFont();
		FontData[] fontData = systemFont.getFontData();
		if(fontData != null && fontData.length > 0) {
			fontName = fontData[0].getName();
			fontHeight = fontData[0].getHeight();
		}
		if(!platformStatus.isEmpty()) {
			Label platHeader = new Label(customArea, SWT.LEFT |SWT.WRAP);
			GridDataFactory.fillDefaults().align(SWT.BEGINNING, SWT.BEGINNING).grab(true, false).span(3, 1).applyTo(platHeader);
			platHeader.setFont(new Font(platHeader.getDisplay(), new FontData(fontName, fontHeight+1, SWT.BOLD)));
			platHeader.setText("Build Operation(s)");
			Label statusHeader = new Label(customArea, SWT.WRAP);
			GridDataFactory.fillDefaults().align(SWT.BEGINNING, SWT.BEGINNING).grab(true, false).span(1, 1).applyTo(statusHeader);
			statusHeader.setFont(new Font(statusHeader.getDisplay(), new FontData(fontName, fontHeight+1, SWT.BOLD)));
			statusHeader.setText("Status");
		
			for(String plaform: platformStatus.keySet()) {
				Label cLabel = new Label(customArea, SWT.LEFT | SWT.WRAP);
				GridDataFactory.fillDefaults().align(SWT.BEGINNING, SWT.BEGINNING).grab(true, false).span(3, 1).applyTo(cLabel);
				cLabel.setText(plaform);
				String status = platformStatus.get(plaform);
				Label statusLabel = new Label(customArea, SWT.WRAP);
				GridDataFactory.fillDefaults().align(SWT.BEGINNING, SWT.BEGINNING).grab(true, false).span(1, 1).applyTo(statusLabel);
				statusLabel.setFont(new Font(statusLabel.getDisplay(), new FontData(fontName, fontHeight, SWT.BOLD)));
				statusLabel.setText(status);
			}
			Label createNote = UIUtils.createNote(parent, "Please see log for more details.", 4);
			createNote.setFont(new Font(createNote.getDisplay(), new FontData(fontName, fontHeight, SWT.ITALIC)));
		}
		return parent;
	}
	
	@Override
	protected Point getInitialLocation(Point initialSize) {
		Point shellCenter = UIUtils.getCenterPoint();
		return new Point(shellCenter.x - initialSize.x / 2, shellCenter.y - initialSize.y / 2);
	}
}
