package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Tree;

import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.utils.KUtils;

public class ServiceSelectionDialog extends TrayDialog {
	
	public static final int MODE_SHOW_NO_CONFLICTS = 0;
	public static final int MODE_SHOW_CONFLICTS = 1;
	
	private static final String OLD_PROP = "Old name";
	private static final String NEW_PROP = "New name";
	
	private Tree svcTree;
	private CheckboxTreeViewer svcTreeViewer;
	
	private Table table;
	private TableViewer tableViewer;
	
	private List<String> svcList;
	
	private List<String> inputList;
	
	private List<String> selectedList;
	
	private String title;
	private List existingList;
	
	private Label errorLbl;
	private String projName;
	private int mode;
	
	private ArrayList<String[]> conflictList = new ArrayList<String[]>();
	
	public ServiceSelectionDialog(Shell shell, String title, List existingList, String projName, int mode) {
		super(shell);
		this.title = title;
		this.existingList = existingList;
		this.projName = projName;
		this.mode = mode;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(title);
		//newShell.setSize(500, 500);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite control = (Composite) super.createDialogArea(parent);
		
		Composite mainComp = new Composite(control, SWT.NONE);
		mainComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		mainComp.setLayout(KUtils.makeMarginsTo(new GridLayout(), 0));
		
		createSvcSelectionComposite(mainComp);
		
		return control;
	}

	private void createSvcSelectionComposite(Composite mainComp) {
		if(mode == MODE_SHOW_CONFLICTS) {
			update();
		}
		
		Composite tableComp = new Composite(mainComp, SWT.NONE);
		GridData data = new GridData(GridData.FILL_BOTH);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		if(conflictList.size() > 0) {
			layout.makeColumnsEqualWidth = true;
		}
		tableComp.setLayoutData(data);
		tableComp.setLayout(layout);
		
		if(conflictList.size() > 0) {
			errorLbl = new Label(tableComp, SWT.NONE);
			errorLbl.setText("");
			data = new GridData(GridData.FILL_HORIZONTAL);
			data.horizontalSpan = 2;
			errorLbl.setLayoutData(data);
			errorLbl.setForeground(ColorConstants.red);
		}
		
		svcTree = new Tree(tableComp, SWT.BORDER | SWT.MULTI| SWT.H_SCROLL | SWT.V_SCROLL | SWT.CHECK);
		data = new GridData(GridData.FILL_BOTH);
		data.widthHint = 300;
		data.heightHint = 400;
		svcTree.setLayoutData(data);
		svcTreeViewer = new CheckboxTreeViewer(svcTree);
		svcTreeViewer.setLabelProvider(new ServiceTreeLabelProvider());
		svcTreeViewer.setContentProvider(new ServiceTreeContentProvider());
		List list = new ArrayList();
		list.add(new ServicesList(inputList));
		svcTreeViewer.setInput(list);
		svcTreeViewer.expandAll();
		svcTreeViewer.addCheckStateListener(new ICheckStateListener() {

			public void checkStateChanged(CheckStateChangedEvent event) {
				if(event.getElement() instanceof ServicesList) {
					svcTreeViewer.setSubtreeChecked(event.getElement(), event.getChecked());
				} else if(event.getElement() instanceof String) {
					Object obj = svcTreeViewer.getTree().getItem(0).getData();
					if(obj instanceof ServicesList && event.getChecked()) {
						boolean checked = false;
						for(Object str : (((ServicesList)obj).getArray())) {
							checked = svcTreeViewer.getChecked(str);
							if(!checked) break;
						}						
						svcTreeViewer.setChecked(obj, checked);
					} else {
						svcTreeViewer.setChecked(obj, false);
					}
				}
				update();
				if(tableViewer != null) {
					tableViewer.refresh(true);
				}
			}
		});
		
		
		if(conflictList.size() > 0) {
			Composite rightComp = new Composite(tableComp, SWT.NONE);
			data = new GridData(GridData.FILL_BOTH);
			rightComp.setLayoutData(data);
			layout = new GridLayout();
			layout.numColumns = 1;
			layout.marginHeight = 0;
			rightComp.setLayout(layout);
			
			Label noteLabel = new Label(rightComp, SWT.NONE);
			noteLabel.setText("Note: Following services already exist in '"+projName+"', \nThese services will be overwritten if a new name \nis not specified.");
			data = new GridData(GridData.FILL_HORIZONTAL);
			noteLabel.setLayoutData(data);
			noteLabel.setForeground(ColorConstants.blue);
			
			TableLayout tablelayout = new TableLayout();
			tablelayout.addColumnData(new ColumnWeightData(20, true));
			tablelayout.addColumnData(new ColumnWeightData(20, true));
			
			table = new Table(rightComp, SWT.FULL_SELECTION | SWT.V_SCROLL | SWT.BORDER);
			data = new GridData(GridData.FILL_BOTH);
			table.setLayoutData(data);
			table.setLayout(tablelayout);
			table.setLinesVisible(true);
			table.setHeaderVisible(true);
			
			tableViewer = new TableViewer(table);
			
			TableColumn tcol1 = new TableColumn(table, SWT.LEFT);
			tcol1.setText("Old name");
			
			TableColumn tcol2 = new TableColumn(table, SWT.LEFT);
			tcol2.setText("New name");
			
			tableViewer.setContentProvider(new IStructuredContentProvider() {
				public Object[] getElements(Object input) {
					if (input instanceof ArrayList) {
						ArrayList<String[]> list = (ArrayList)input;
						return list.toArray();
					}
					return null;
				}
				
				public void dispose() {}
				
				public void inputChanged (Viewer viewer, Object obj, Object obj1) {	}
			});
			
			tableViewer.setLabelProvider(new TextLabelProvider());
			
			attachCellEditors(tableViewer, table);
			TableViewerEditor.create(tableViewer,new ColumnViewerEditorActivationStrategy(tableViewer),ColumnViewerEditor.TABBING_HORIZONTAL|ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR|ColumnViewerEditor.TABBING_VERTICAL);
			update();
			tableViewer.setInput(conflictList);
		}
	}
	
	private void update() {
		for(String newsvc: inputList) {
			if(existingList.contains(newsvc)) {
				String[] row = new String[] {newsvc, ""};
				if(!isContains(newsvc)) {
					conflictList.add(row);
				}
			}
		}
	}
	
	private boolean isContains(String name) {
		boolean exists = false;
		for(String[] array: conflictList) {
			if(name.equals(array[0])) {
				exists = true;
				break;
			}
		}
		return exists;
	}
	
	private class TextLabelProvider extends LabelProvider implements ITableLabelProvider , ITableColorProvider{		
		
		public String getColumnText(Object element, int columnIndex) {
			switch(columnIndex) {
			case 0:
				return ((String[])element)[0];
			case 1:
				return ((String[])element)[1];
			default:
				return "Invalid column: " + columnIndex;
			}
		}
		
		public Image getColumnImage(Object arg0, int arg1) {
			return null;
		}
		
		public void addListener(ILabelProviderListener listener) {
			
		}
		
		public void dispose() {
		
		}
			
		public boolean isLabelProperty(Object element, String property) {
			return true;
		}
			
		public void removeListener(ILabelProviderListener listener) {
			
		}
			
				
		public Color getForeground(Object element, int columnIndex) {
			if(element instanceof String[]){
				String svc = ((String[])element)[0];
				if(svcTreeViewer.getChecked(svc)){
					return ColorConstants.black;
				}
			}
			return ColorConstants.gray;
		}
				
		@Override
		public Color getBackground(Object element, int columnIndex){
				return ColorConstants.white;
		}
	}
	
	private void attachCellEditors(final TableViewer tableViewer, Composite table) {
		tableViewer.setCellModifier(new ICellModifier() {
			public boolean canModify(Object element, String property) {
				if(NEW_PROP.equals(property)) {
					if(svcTreeViewer.getChecked( (((String[])element))[0]) ) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			}
			
			public Object getValue(Object element, String property) {
				if (OLD_PROP.equals(property))
					return ((String[])element)[0];
				else if (NEW_PROP.equals(property))
					return ((String[])element)[1];
				return null;
			}
			
			public void modify(Object element, String property, Object value) {
				TableItem tableItem = (TableItem)element;
				String[] data = (String[])tableItem.getData();
				
				if (OLD_PROP.equals(property)) {
					data[0] = value.toString();
				} else if (NEW_PROP.equals(property)) {
					data[1] = value.toString();
					boolean isValid = validate();
					if(isValid){
						getButton(OK).setEnabled(true);
					} else {
						getButton(OK).setEnabled(false);
					}
				} 
				tableViewer.refresh(data);
			}
		});
		
		tableViewer.setCellEditors(new CellEditor[] {
				null,
				new TextCellEditor(table)
		});
		
		tableViewer.setColumnProperties(new String[] {
				OLD_PROP,
				NEW_PROP
		});
	}
	
	private boolean validate(){
		Object[] checkedElements = svcTreeViewer.getCheckedElements();
		for(String[] row: conflictList) {
			String newName = row[1];
			if(newName.trim().length() > 0 && svcTreeViewer.getChecked(row[0])) {
				if(existingList.contains(newName)) {
					errorLbl.setText("Service '"+newName+ "' already exists");
					return false;
				} else {
					for(Object checked: checkedElements) {
						if(checked instanceof String && checked.equals(newName)) {
							errorLbl.setText("A service having name \""+newName+"\" already exists in the list of services to be imported.");
							return false;
						}
					}
				}
			}
		}
		errorLbl.setText("");
		return true;
	}
	
	private class ServicesList {
		public ServicesList(List<String> list) {
			svcList = list;
		}
		
		public Object[] getArray(){
			return svcList.toArray();
		}
	}
	
	public List<String> getSelectedList() {
		return selectedList;
	}

	public void setInputList(List<String> inputList) {
		this.inputList = inputList;
	}
	
	@Override
	protected void okPressed() {
		Object[] checkedElements = svcTreeViewer.getCheckedElements();
		selectedList = new ArrayList<String>();
		for(Object obj: checkedElements) {
			if(obj instanceof ServicesList) {
				Object[] array = ((ServicesList)obj).getArray();
				for(Object subObj: array) {
					selectedList.add((String) subObj);
				}
				break;
			} else {
				selectedList.add((String)obj);
			}
		}
		super.okPressed();
	}
	
	public ArrayList<String[]> getConflictList() {
		return conflictList;
	}
	
	private class ServiceTreeContentProvider implements ITreeContentProvider{

		@SuppressWarnings("unchecked")
		@Override
		public Object[] getChildren(Object parentElement) {
			if(parentElement instanceof List){
				return ((List<Object>)parentElement).toArray();	
			} else if(parentElement instanceof ServicesList) {
				return ((ServicesList)parentElement).getArray();
			} 
			return null;
		}

		@Override
		public Object getParent(Object element) {
			return null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean hasChildren(Object element) {
			Object[] children = getChildren(element);
			if(element instanceof List) {
				if(((List<Object>)element).size()>0)
					return true;
			}else if(element instanceof ServicesList) {
				if(((ServicesList)element).getArray().length >0 ) 
					return true;
			} else if (children != null) {
				if (children.length > 0) {
					return true;
				}
			}
			return false;
		}

		@Override
		public Object[] getElements(Object element) {
			return getChildren(element);
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub

		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			// TODO Auto-generated method stub

		}

	}

	private class ServiceTreeLabelProvider implements ILabelProvider{

		@Override
		public Image getImage(Object element) {
			if(element instanceof ServicesList){
				return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.servicesImage);
			} else {
				return ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.serviceImage);
			}
			//return null;
		}

		@Override
		public String getText(Object element) {
			if(element instanceof ServicesList){
				return "Services" + " " + "(" + ((ServicesList)element).getArray().length + ")";
			} else if (element instanceof String){
				return ((String)element);
			} 
			return null;
		}

		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
		}

	}
	
}
