/**
 * 
 */
package com.pat.tool.keditor.dialogs;

import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.kony.ant.task.OS;
import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.utils.UIUtils;
import com.pat.tool.keditor.utils.ValidationUtil;

/**
 * @author KH1960
 *
 */
public class AndroidDebuggerDialog extends TrayDialog {
	
	Composite composite;
	Button browseButton;
	private Text chromePath;
	private Label errorLabel;
	private CustomSelectionAdapter customSelectionAdapter;
	private ModifyEventHandler modifyListener;
	private boolean isChromePathFound = false;
	private String chromePathString = "";
	private String chromeExePath = "";

	public AndroidDebuggerDialog(Shell shell) {
		super(shell);
		setShellStyle((shell.getStyle() | SWT.SHEET) & ~SWT.RESIZE);
		modifyListener = new ModifyEventHandler();
		customSelectionAdapter = new CustomSelectionAdapter();
	}
	

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Android Chrome Configuration");
	}

	@Override
	protected int getShellStyle() {
		return super.getShellStyle();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = (Composite) super.createDialogArea(parent);	
		composite = new Composite(c, SWT.BORDER);
		GridLayout grid = new GridLayout(3, false);
		composite.setLayout(grid);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.minimumWidth = 500; 
		composite.setLayoutData(gridData);
		createComposite(composite);
		setHelpAvailable(false);
		return c;
	}
	
	private void createComposite(Composite composite){
		
		errorLabel = new Label(composite, SWT.NONE);
		errorLabel.setText("");
		GridData gridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false,3,1);
		errorLabel.setLayoutData(gridData);
		
		Label chromePathLabel = new Label(composite, SWT.NONE);
		chromePathLabel.setText("Google Chrome Location: ");
		
		chromePath = new Text(composite, SWT.BORDER|SWT.READ_ONLY);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		chromePath.setLayoutData(gridData);
		chromePath.addListener(SWT.Modify, modifyListener);
		
		browseButton = new Button(composite, SWT.PUSH);
		browseButton.setText("Browse");
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		browseButton.setLayoutData(gridData);
		browseButton.addSelectionListener(customSelectionAdapter);
	}
	
	private class CustomSelectionAdapter extends SelectionAdapter {
		
		@Override
		public void widgetSelected(SelectionEvent e) {
			if(browseButton != null && e.widget == browseButton) {
				browseButtonAction();
			}
		}
	}
	
	public void browseButtonAction() {
		try {
			String extension = "";
			if(OS.isWindowsOS()){
				extension = "*.exe";
			} else {
				extension = "*.app";
			}
			FileDialog fileDialog = new FileDialog(Display.getDefault().getActiveShell(), SWT.NONE);
			fileDialog.setText("Select Google Chrome Location.");
			fileDialog.setFilterExtensions(new String[]{extension});
			String selectedFile = fileDialog.open();
			if(selectedFile == null){
				return;
			}
			if(!selectedFile.toLowerCase().endsWith(".exe")){
				selectedFile = selectedFile+".exe";
			}
			 
			chromePath.setText(selectedFile);
		} catch (Exception exception) {
			KEditorPlugin.getDefault().logError("Error in opening the File Editor for Android Debugger.", exception);
		}
		
	}
	
	private class ModifyEventHandler implements Listener {
		@Override
		public void handleEvent(Event event) {
			validate();
		}
	}
	
	@Override
	protected void okPressed(){
		if(!validate()) {
			return;
		} else {
			if(chromePath.getText() != null && chromePath.getText().length() >0) 
				setChromeExePath(chromePath.getText());
		}
		super.okPressed();
	}
	
	public boolean validate() {
		boolean isValid = true;
		String chromePathStr = chromePathString;
		if(!isChromePathFound && chromePath != null) {
			chromePathStr = chromePath.getText();
		}
		
		if(OS.isWindowsOS() && !ValidationUtil.isNonEmptyString(chromePathStr)) {
			errorLabel.setForeground(Display.getDefault().getSystemColor(3));
			errorLabel.setText("Google Chrome Executable Path can't be empty");
			isValid = false;
		}
		if(OS.isWindowsOS() && !isValidChromePath(chromePathStr)) {
			errorLabel.setForeground(Display.getDefault().getSystemColor(3));
			errorLabel.setText("Google Chrome Executable Path is Invalid.");
			isValid = false;
		}
		if(isValid){
			errorLabel.setText("");
		} 
		return isValid;
	}
	
	private boolean isValidChromePath(String chromePathStr) {
		if(chromePathStr != null && !chromePathStr.isEmpty() && chromePathStr.toLowerCase().endsWith("chrome.exe")) {
			return true;
		}
		return false;
	}
	
	private void setChromeExePath(String chromeExePath) {
		this.chromeExePath = chromeExePath;
	}
	
	public String getChromeExePath() {
		return chromeExePath;
	}

}
