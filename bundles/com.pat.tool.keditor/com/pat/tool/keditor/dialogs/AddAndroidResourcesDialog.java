package com.pat.tool.keditor.dialogs;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IFolder;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.pat.tool.keditor.constants.NavigationViewConstants;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.ValidationUtil;

/**
 * 
 * @author Prasad
 * @modified Priya
 * 
 */

public class AddAndroidResourcesDialog extends Dialog {
	private static final String ORDER_MAP= "order_map";
	private static final String LOCALE_CATEGORY= "Language_and_region";
	private static final String BUNDLE_NAME = "com.pat.tool.keditor.dialogs.AndroidResourceTypes";
	private static final ResourceBundle ANDROID_RESOURCES_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	private static final String SEPARATOR = "-";
	private static final String COMMA_SEPARATOR = ",";
	public static final String ADD = "Add Resource";


	private Map<String, List<String>> resourceTypes;
	private String locales;
	private Label previewLabel;
	private HashMap<String, String> resourceFolders = new HashMap<>(3);
	private Label errorLabel;
	private Combo drawablesCombo;

	
	private List<CCombo> resourceCombos = new ArrayList<CCombo>();
	private Composite container;
	private Composite drawComposite;
	private IFolder parentFolder;
	private String selectedFolderName = KUtils.EMPTY_STRING;
	private HashMap<String, String> drawablesMap = new HashMap<>(3);
	private String prevDrawableKey = KUtils.EMPTY_STRING;
	private static boolean isDirty = false;
	
	public AddAndroidResourcesDialog(String locales, IFolder parentFolder) {
		super(Display.getDefault().getActiveShell());
		this.locales = locales;
		setShellStyle(getShellStyle() | SWT.RESIZE);
		this.parentFolder = parentFolder;
		initialize();
	}

	// http://developer.android.com/guide/topics/resources/providing-resources.html#AlternativeResources
	private void initialize() {
		resourceTypes = new LinkedHashMap<String, List<String>>();
		String orderedKeys = ANDROID_RESOURCES_BUNDLE.getString(ORDER_MAP);
		String[] keys = orderedKeys.split(COMMA_SEPARATOR);
		for (String key : keys) {
			String value = ANDROID_RESOURCES_BUNDLE.getString(key);
			if (key.equals(LOCALE_CATEGORY)) {
				value = locales;
			}
			if (ValidationUtil.isNonEmptyString(value)) {
				String[] split = value.split(COMMA_SEPARATOR);
				resourceTypes.put(key, new ArrayList<String>(Arrays.asList(split)));
			}
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
	//	getShell().setText("Add Android Resource Folder");
		GridLayoutFactory.fillDefaults().numColumns(1).margins(5, 5).applyTo(parent);
		FormToolkit toolkit = new FormToolkit(Display.getDefault());
		
		drawComposite = toolkit.createComposite(parent, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).hint(400, SWT.DEFAULT).applyTo(drawComposite);
		GridLayoutFactory.fillDefaults().numColumns(2).margins(5, 5).applyTo(drawComposite);
		
		Label dLabel = toolkit.createLabel(drawComposite, "Drawables : ", SWT.BOLD);
		dLabel.setFont(JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT));
		GridDataFactory.swtDefaults().applyTo(dLabel);
		
		drawablesCombo = new Combo(drawComposite, SWT.READ_ONLY | SWT.BORDER);
		toolkit.adapt(drawablesCombo);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(drawablesCombo);
		drawablesCombo.addSelectionListener(drawablesComboListener);
		drawablesCombo.setItems(loadDrawables().toArray(new String[0]));
		drawablesCombo.select(0);
		prevDrawableKey = drawablesCombo.getText();
		
		container = toolkit.createComposite(parent, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).hint(400, SWT.DEFAULT).applyTo(container);
		GridLayoutFactory.fillDefaults().numColumns(2).margins(5, 5).applyTo(container);

		LinkedList<String> selectedList = loadSelectedItems();
		
		for (Entry<String, List<String>> entry : resourceTypes.entrySet()) {
			Label label = toolkit.createLabel(container, entry.getKey().replace("_", " ") + ": ");
			GridDataFactory.swtDefaults().applyTo(label);

			CCombo combo = new CCombo(container, SWT.READ_ONLY | SWT.BORDER);
			toolkit.adapt(combo);
			GridDataFactory.fillDefaults().grab(true, false).applyTo(combo);
			List<String> items = entry.getValue();
			String key = entry.getKey();
			if(LOCALE_CATEGORY.equals(key)) {
				String LOCALE_PATH_REGEX = "([a-z]{2})-[A-Z]{2}";
				Pattern LOCALE_PATH_PATTERN = Pattern.compile(LOCALE_PATH_REGEX);
				for (int i = 0; i < items.size(); i++) {
					String item = items.get(i);
					String str = item.replace("_", "-");
					Matcher matcher = LOCALE_PATH_PATTERN.matcher(str);
					if(!matcher.find() || str.length() > 5) {
						items.remove(i);
					}
				}
			}
			items.add(0, KUtils.DEFAULT_NONE);
			combo.setItems(items.toArray(new String[0]));
			combo.select(0);
			combo.addSelectionListener(comboListener);
			String matchItem = matchAndGetComboItem(items, selectedList);
			if (matchItem != null) {
				combo.setText(matchItem);
			}
			
			resourceCombos.add(combo);
		}
		Label label = toolkit.createLabel(container, "Folder Preview: ");
		Font bold = JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT);
		label.setFont(bold);
		GridDataFactory.swtDefaults().applyTo(label);
		previewLabel = toolkit.createLabel(container, NavigationViewConstants.FOLDER_PREFIX_DRAWABLE);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(previewLabel);
		previewLabel.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLUE));
		previewLabel.setFont(bold);
		
		errorLabel = toolkit.createLabel(container, NavigationViewConstants.FOLDER_PREFIX_DRAWABLE);
		GridDataFactory.fillDefaults().grab(true, false).span(2,1).applyTo(errorLabel);
		errorLabel.setText("Resource folder already exists.");
		errorLabel.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_RED));
		
		return container;
	}
	
	
	private String matchAndGetComboItem(List<String> items, LinkedList<String> selectedList) {
		if (!selectedList.isEmpty()) {
			if (selectedList.size() >= 2) {
				String selectedItem = selectedList.get(0) + "-" + selectedList.get(1);
				if (items.contains(selectedItem)) {
					selectedList.removeFirst();
					selectedList.removeFirst();
					return selectedItem;
				}
			}

			if (items.contains(selectedList.peek())) {
				return selectedList.poll();
			}
		}
		return null;
	}

	private SelectionAdapter comboListener = new SelectionAdapter() {
		public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
			isDirty = true;
			updateUI();
		}
	};
	
	private SelectionAdapter drawablesComboListener = new SelectionAdapter() {
		public void widgetSelected(org.eclipse.swt.events.SelectionEvent e) {
			updateUIOnDrawableChange();
		}
	};
	
	public void create() {
		super.create();
		updateUI();
	};
 
	/**
	 * This is to create the output resource folder with the selected values
	 * http://developer.android.com/guide/topics/resources/providing-resources.html#table2
	 */
	private void updateUI() {
		StringBuilder builder = new StringBuilder();
		builder.append(NavigationViewConstants.FOLDER_PREFIX_DRAWABLE);
		List<String> resourceKeys = new ArrayList<String>(resourceTypes.keySet());
		int i = 0;
		for (CCombo combo : resourceCombos) {
			String text = combo.getText();
			if (!text.equals(KUtils.DEFAULT_NONE)) {
				if(resourceKeys.get(i).equals("Language_and_region")){
					text = text.replace("_", "-r") ; //The language is defined by a two-letter ISO 639-1 language code,optionally followed by a a two letter ISO 3166-1alpha-2 region code(*preceded by a lowercase "r")
				}
				builder.append(SEPARATOR);
				builder.append(text);
			}
			i++;
		}
 
		String resourceName = builder.toString();
		previewLabel.setText(resourceName);
		GridData layoutData = (GridData) errorLabel.getLayoutData();
		boolean resourceExists = false;
		for(String key : drawablesMap.keySet()){
			if(resourceName.equals(drawablesMap.get(key))){
				resourceExists = true;
				break;
			}
		}
		drawablesCombo.setEnabled(!resourceExists);
		errorLabel.setVisible(resourceExists);
		layoutData.exclude = !resourceExists;
		getButton(IDialogConstants.OK_ID).setEnabled(!resourceExists);
		
		layoutData = (GridData) container.getLayoutData();
		layoutData.widthHint = container.computeSize(SWT.DEFAULT, SWT.DEFAULT).x + 10;
		getShell().pack();
	};
	
	private void updateUIOnDrawableChange(){
		String folder = previewLabel.getText();
		String resourceName = drawablesCombo.getText();
		if(isDirty && !NavigationViewConstants.FOLDER_PREFIX_DRAWABLE.equals(folder)){
			isDirty = false;
			boolean save = MessageDialog.openQuestion(getShell(), "", "Previous drawable modification detected. Save changes?");
			if(save){
				if(ADD.equals(prevDrawableKey)){
					drawablesMap.put(folder, folder);
				}
				else {
					drawablesMap.put(prevDrawableKey, folder);
				}
				drawablesCombo.setItems(loadDrawables().toArray(new String[0]));
				drawablesCombo.setText(resourceName);
			}
		}
		
		LinkedList<String> selectedItem = loadSelectedItem();
		for (CCombo combo : resourceCombos) {
			String[] items = combo.getItems();
			String matchItem = matchAndGetComboItem(new ArrayList<>(Arrays.asList(items)), selectedItem);
			if (matchItem != null) {
				combo.setText(matchItem);
			} else {
				combo.select(0);
			}
		}
		for(String key : drawablesMap.keySet()){
			if(resourceName.equals(drawablesMap.get(key))){
				prevDrawableKey = key;
				break;
			}
		}
		if(ADD.equals(resourceName)){
			resourceName = NavigationViewConstants.FOLDER_PREFIX_DRAWABLE;
		}
		previewLabel.setText(resourceName);
		GridData layoutData = (GridData) errorLabel.getLayoutData();
		
		layoutData = (GridData) drawComposite.getLayoutData();
		layoutData.widthHint = drawComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT).x + 10;
		
		layoutData = (GridData) container.getLayoutData();
		layoutData.widthHint = container.computeSize(SWT.DEFAULT, SWT.DEFAULT).x + 10;
		getShell().pack();
	}
	
	@Override
	protected void okPressed() {
		String folder = previewLabel.getText();
		if(isDirty && !NavigationViewConstants.FOLDER_PREFIX_DRAWABLE.equals(folder)){
			if(ADD.equals(prevDrawableKey)){
				drawablesMap.put(folder, folder);
			}
			else {
				drawablesMap.put(prevDrawableKey, folder);
			}
		}
		isDirty = false;
		resourceFolders = drawablesMap;
		super.okPressed();
	}
	
	public String getResourceFolderName() {
		return (String) resourceFolders.values().toArray()[0];
	}
	
	public HashMap<String, String> getResourceFolders() {
		return resourceFolders;
	}
	
	public void setSelectedFolderName(String selectedFolderName){
		this.selectedFolderName = selectedFolderName;
	}
	
	private LinkedList<String> loadSelectedItems(){
		String folderName = selectedFolderName;
		LinkedList<String> itemsQueue = new LinkedList<String>();
		for(String locale : locales.split(COMMA_SEPARATOR)){
			locale = locale.replace("_","-r");
			if(selectedFolderName.contains(locale)){
				folderName = folderName.replace("-r", "_");
				break;
			}
		}
		
		for(String item : folderName.split("-")){
			if(NavigationViewConstants.FOLDER_PREFIX_DRAWABLE.equals(item)){
				continue;
			}
			itemsQueue.add(item);
		}
		
		return itemsQueue;
		
	}
	
	private LinkedList<String> loadSelectedItem(){
		String folderName = drawablesCombo.getText();
		LinkedList<String> itemsQueue = new LinkedList<String>();
				
		for(String item : folderName.split("-")){
			if(NavigationViewConstants.FOLDER_PREFIX_DRAWABLE.equals(item)){
				continue;
			}
			itemsQueue.add(item);
		}
		
		return itemsQueue;
	}
	
	private LinkedList<String> loadDrawables(){
		LinkedList<String> drawables = new LinkedList<String>();
		if(drawablesMap.isEmpty()){
			drawables.add(ADD);
			drawablesMap.put(ADD, ADD);
			File file = new File(parentFolder.getLocation().toOSString());
			String[] list = file.list(new FilenameFilter() {
				
				@Override
				public boolean accept(File dir, String name) {
					if(new File(dir, name).isDirectory() && name.startsWith(NavigationViewConstants.FOLDER_PREFIX_DRAWABLE)){
						return true;
					}
					return false;
				}
			});
			
			if(list != null){
				for(String folder : list){
					drawables.add(folder);
					drawablesMap.put(folder, folder);
				}
			}
		} else {
			for(String value : drawablesMap.values()){
				drawables.add(value);
			}
		}
		return drawables;
	}
}
