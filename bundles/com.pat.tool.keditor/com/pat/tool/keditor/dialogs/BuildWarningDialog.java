package com.pat.tool.keditor.dialogs;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.utils.KConstants;
import com.pat.tool.keditor.utils.KNUtils;

public class BuildWarningDialog extends MessageDialog {

	private Button checkBox;
	private IPreferenceStore store;
//	private String projectType = KConstants.PROJECT_TYPE_LUA;//Default Lua for Backward compatibility
	private String projectType = KConstants.PROJECT_TYPE_JS;//Default ass JS because lua is not supported in 7.0
	public static final String LUA_BUILD_WARNING = "build_warning";
	public static final String JS_BUILD_WARNING = "js_build_warning";
	
	
	public BuildWarningDialog(Shell parentShell, String dialogTitle,
			Image dialogTitleImage, String dialogMessage, int dialogImageType,
			String[] dialogButtonLabels, int defaultIndex,String projectName) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage,
				dialogImageType, dialogButtonLabels, defaultIndex);
		projectType = KNUtils.getProjectType(projectName);
	}
	
	public BuildWarningDialog(Shell parentShell, String dialogTitle,
			Image dialogTitleImage, String dialogMessage, int dialogImageType,
			String[] dialogButtonLabels, int defaultIndex) {
		super(parentShell, dialogTitle, dialogTitleImage, dialogMessage,
				dialogImageType, dialogButtonLabels, defaultIndex);
		
	}

	@Override
	protected Control createCustomArea(Composite parent) {
		store = KEditorPlugin.getDefault().getPreferenceStore();
		if(projectType.equals(KConstants.PROJECT_TYPE_LUA)){
			store.setDefault(LUA_BUILD_WARNING,false);
		}
		if(projectType == KConstants.PROJECT_TYPE_JS){
			store.setDefault(JS_BUILD_WARNING,false);
		}
		
		checkBox = new Button(parent,SWT.CHECK);
		checkBox.setText("Do not show again.");
		GridData data = new GridData();
		data.verticalIndent = 6;
		data.horizontalIndent = 6;
		checkBox.setLayoutData(data);
		
		checkBox.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean selection = checkBox.getSelection();
				if(projectType.equals(KConstants.PROJECT_TYPE_LUA)){
					store.setValue(LUA_BUILD_WARNING,selection);
				}
				if(projectType == KConstants.PROJECT_TYPE_JS){
					store.setValue(JS_BUILD_WARNING,selection);
				}
				
			}
		});
		return checkBox;
	}
	
	

}
