package com.pat.tool.keditor.dialogs;

import java.util.Arrays;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.editors.help.ContextSensitiveHelpConstants;
import com.pat.tool.keditor.editors.help.ContextSensitiveHelpUtil;
import com.pat.tool.keditor.portability.SkinCloneMetadata;
import com.pat.tool.keditor.portability.SkinCloneMetadataComposite.SkinCloneMetaDataDialog;
import com.pat.tool.keditor.utils.RendererHashMapData;
import com.pat.tool.keditor.widgets.IMobileChannel;
import com.pat.tool.keditor.widgets.MobileChannels;

public class CloneAllDialog extends TitleAreaDialog{

	private ClonePlatformPropertiesComposite composite;
	
	private IMobileChannel clonePlatform;
	private RendererHashMapData renderData;
	private SkinCloneMetadata skinCloneMetadata = new SkinCloneMetadata();
	private Button clonePropertiesButton;

	public CloneAllDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle() | SWT.RESIZE); 
		setHelpAvailable(false);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle("Clone All skins");
		getShell().setText("Clone Skin");
		
		Composite control = (Composite) super.createDialogArea(parent);
		
		composite = new ClonePlatformPropertiesComposite(control, SWT.NONE) {
			protected void updateComposite() {
				super.updateComposite();
				setMessage("Clone all " + getClonePlatform().getDisplayName() + " skins to the selected platforms");
			};

			protected Composite createExtendedComposite(Composite parent) {
				Composite buttonComposite = new Composite(parent, SWT.NONE);
				GridDataFactory.swtDefaults().applyTo(buttonComposite);
				GridLayoutFactory.fillDefaults().margins(5, 5).applyTo(buttonComposite);
			    clonePropertiesButton = new Button(buttonComposite, SWT.PUSH);
			    clonePropertiesButton.setText("Clone Properties");
			    clonePropertiesButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						SkinCloneMetaDataDialog skinCloneMetaDataDialog = new SkinCloneMetaDataDialog(skinCloneMetadata, getRenderData());
						int open = skinCloneMetaDataDialog.open();
						if (open == IDialogConstants.OK_ID) {
							skinCloneMetadata = skinCloneMetaDataDialog.getUpdatedMetadata();
						}
					}
					
				});
			    clonePropertiesButton.setEnabled(false);
				return buttonComposite;
			};
		};
		
		composite.addPlatformSelectionListener(new Listener() {
			@Override
			public void handleEvent(Event event) {
				clonePropertiesButton.setEnabled(composite.getRenderData().atleastOneSelected(Arrays.asList(MobileChannels.getAllChannels())));
			}
		});
		
        ContextSensitiveHelpUtil.hookHelp(control, ContextSensitiveHelpConstants.CLONE_SKINS);
		return control;
	}
	
	@Override
	protected void okPressed() {
		clonePlatform = composite.getClonePlatform();
		renderData = composite.getRenderData();
		super.okPressed();
	}

	public IMobileChannel getClonePlatform() {
		return clonePlatform;
	}

	public RendererHashMapData getRenderData() {
		return renderData;
	}
	
	public SkinCloneMetadata getSkinCloneMetadata() {
		return skinCloneMetadata;
	}

	
}
