package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.pat.tool.keditor.cellEditors.TemplateDataCellEditor;
import com.pat.tool.keditor.model.KMap;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.propertyDescriptor.TemplateSelectionCellEditor;
import com.pat.tool.keditor.table.AddColumnDataAction;
import com.pat.tool.keditor.table.DeleteRow;
import com.pat.tool.keditor.table.InsertAfterAction;
import com.pat.tool.keditor.table.InsertBeforeAction;
import com.pat.tool.keditor.table.MoveDownAction;
import com.pat.tool.keditor.table.MoveUpAction;
import com.pat.tool.keditor.table.MultipleImageSelectEditor;
import com.pat.tool.keditor.table.TableWithButtons;
import com.pat.tool.keditor.utils.BrowseImageWindow;
import com.pat.tool.keditor.utils.CalloutRow;
import com.pat.tool.keditor.utils.DisplayUtils;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.LocationData;
import com.pat.tool.keditor.utils.SkinEventUtil;
import com.pat.tool.keditor.utils.model.datagrid.TemplateData;
import com.pat.tool.keditor.widgets.MobileChannels;

public class LocationDataDialog extends TitleAreaDialog {


	TableViewer viewer;
	TableWithButtons locationTable;
	private final IProject project;
	private int formCategory;
	private final KMap widget;
	private int platform;
	LocationData locationData;

	public static final String LATITUDE = "Latitude";
	public static final String LONGITUDE = "Longitude";
	public static final String NAME = "Name";
	public static final String DESC = "Description";
	public static final String IMAGE = "Image";
	public static final String SHOWCALLOUT = "showcallout";
	public static final String TEMPLATE = "Template";
	public static final String TEMPLATE_DATA = "Template Data";
	
	public static final String[] HEADER_NAMES = {LATITUDE,LONGITUDE,NAME,DESC,IMAGE,TEMPLATE,TEMPLATE_DATA};
	
	public static final int LAT_INDEX = 0;
	public static final int LONG_INDEX = 1;
	public static final int NAME_INDEX = 2;
	public static final int DESC_INDEX = 3;
	public static final int IMAGE_INDEX = 4;
	public static final int SHOWCALLOUT_INDEX = 5;
	public static final int TEMP_INDEX = 6;
	public static final int TEMP_DATA_INDEX = 7;
	
	

	public LocationDataDialog(Shell parentShell, LocationData LocationData, KMap widget, IProject project,
			int formCategory, int platform) {
		super(parentShell);
		this.project = project;
		this.formCategory = formCategory;
		this.widget = widget;
		this.platform = platform;
		this.locationData = LocationData;

	}

	@Override
	protected Control createButtonBar(Composite parent) {

		setHelpAvailable(false);
		return super.createButtonBar(parent);
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Master Data");
		newShell.setSize(900,400);
	}

	@Override
	protected Control createContents(Composite parent) {
		Control createContents = super.createContents(parent);
		setTitle("Edit Location data for Map");
		setMessage("Define template data for each location");
		return createContents;

	}

	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));
		GridData data = new GridData(SWT.FILL, SWT.FILL, true, true);
		data.horizontalSpan = 2;

		locationTable = new TableWithButtons(composite, SWT.BORDER | SWT.FULL_SELECTION);
		locationTable.createControl();
		viewer = locationTable.getViewer();
		
		hookActions();
		
		TableLayout tablelayout = new TableLayout();
		Table table = viewer.getTable();
		GridData gridData = new GridData(GridData.FILL_BOTH);
		table.setLayoutData(gridData);
		table.setLayout(tablelayout);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		locationTable.getActionsManager().update(true);
		viewer.setColumnProperties(new String[]{LATITUDE});
		createColumnViewers(table);
		viewer.setContentProvider(new ArrayContentProvider());
		TableViewerEditor.create(viewer,new ColumnViewerEditorActivationStrategy(viewer),ColumnViewerEditor.TABBING_HORIZONTAL|ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR|ColumnViewerEditor.TABBING_VERTICAL);
		viewer.setInput(locationData.getRows());
		DisplayUtils.setWidthToColumnsInTableViewer(viewer, 50, TableDataDialog.SAMPLE_COLUMN_OBJECT);


		return composite;
	}

	public void hookActions() {
		locationTable.getActionsManager().add(new AddColumnDataAction(viewer, "Add"){
			@Override
			protected Object createNewRow(String firstCellVal) {
				
				CalloutRow row = new CalloutRow();
				StructuredViewer viewer = getViewer();
				ArrayList<CalloutRow> rowsList  = (ArrayList)viewer.getInput();
				rowsList.add((CalloutRow) row);
				viewer.setInput(rowsList);
				((TableViewer) viewer).getTable().select(rowsList.size()-1);
		    	return row;
				
			}
		});
		locationTable.getActionsManager().add(new DeleteRow(viewer, "Delete"));
		locationTable.getActionsManager().add(new MoveUpAction(viewer, "MoveUp"));
		locationTable.getActionsManager().add(new MoveDownAction(viewer, "MoveDown"));
		locationTable.getActionsManager().add(new InsertBeforeAction(viewer, "Insert Before"){
			
			@Override
			protected void insertBefore(int i) {

				viewer.setSorter(null);
				Object data = null;
				if (viewer.getInput() instanceof ArrayList) {
					ArrayList rowsList = (ArrayList) viewer.getInput();
					if (rowsList.get(i) instanceof CalloutRow)
						data = new CalloutRow();
					rowsList.add(i, data);
					viewer.setInput(rowsList);
				} 
				viewer.getTable().select(i);
			
			}
		});
		locationTable.getActionsManager().add(new InsertAfterAction(viewer, "Insert After"){
			
			@Override
			public void insertAfter(int idx) {

				viewer.setSorter(null);
				Object data = null; 
				if(viewer.getInput() instanceof ArrayList){
					ArrayList  choicesList  = (ArrayList)viewer.getInput();
					if(choicesList.get(idx) instanceof CalloutRow)
						data = new CalloutRow();
					choicesList.add(idx+1, data);
					viewer.setInput(choicesList);
				}
				viewer.getTable().select(idx+1);
			}
		});
	}

	private void createColumnViewers(Table table) {

		TableColumn latColumn = new TableColumn(table, SWT.LEFT);
		latColumn.setText(LATITUDE);
		TableViewerColumn latColumnViewer = new TableViewerColumn(viewer, latColumn);
		latColumnViewer.setEditingSupport(new LocationTableEditingSupport(viewer, LAT_INDEX));
		latColumnViewer.setLabelProvider(new LocationDataLabelProvider(LAT_INDEX));
		
		TableColumn longColumn = new TableColumn(table, SWT.LEFT);
		longColumn.setText(LONGITUDE);
		TableViewerColumn longColumnViewer = new TableViewerColumn(viewer, longColumn);
		longColumnViewer.setEditingSupport(new LocationTableEditingSupport(viewer, LONG_INDEX));
		longColumnViewer.setLabelProvider(new LocationDataLabelProvider(LONG_INDEX));
		
		TableColumn nameColumn = new TableColumn(table, SWT.LEFT);
		nameColumn.setText(NAME);
		TableViewerColumn nameColumnViewer = new TableViewerColumn(viewer, nameColumn);
		nameColumnViewer.setEditingSupport(new LocationTableEditingSupport(viewer, NAME_INDEX));
		nameColumnViewer.setLabelProvider(new LocationDataLabelProvider(NAME_INDEX));
		
		TableColumn descColumn = new TableColumn(table, SWT.LEFT);
		descColumn.setText(DESC);
		TableViewerColumn descColumnViewer = new TableViewerColumn(viewer, descColumn);
		descColumnViewer.setEditingSupport(new LocationTableEditingSupport(viewer, DESC_INDEX));
		descColumnViewer.setLabelProvider(new LocationDataLabelProvider(DESC_INDEX));
		
		TableColumn imageColumn = new TableColumn(table, SWT.LEFT);
		imageColumn.setText(IMAGE);
		TableViewerColumn imageColumnViewer = new TableViewerColumn(viewer, imageColumn);
		imageColumnViewer.setEditingSupport(new LocationTableEditingSupport(viewer, IMAGE_INDEX));
		imageColumnViewer.setLabelProvider(new LocationDataLabelProvider(IMAGE_INDEX));
		
		TableColumn showCalloutColumn = new TableColumn(table, SWT.LEFT);
		showCalloutColumn.setText(SHOWCALLOUT);
		TableViewerColumn showCalloutColumnViewer = new TableViewerColumn(viewer, showCalloutColumn);
		showCalloutColumnViewer.setEditingSupport(new LocationTableEditingSupport(viewer, SHOWCALLOUT_INDEX));
		showCalloutColumnViewer.setLabelProvider(new LocationDataLabelProvider(SHOWCALLOUT_INDEX));
		
		TableColumn templateColumn = new TableColumn(table, SWT.LEFT);
		templateColumn.setText(TEMPLATE);
		TableViewerColumn templateColumnViewer = new TableViewerColumn(viewer, templateColumn);
		templateColumnViewer.setEditingSupport(new LocationTableEditingSupport(viewer, TEMP_INDEX));
		templateColumnViewer.setLabelProvider(new LocationDataLabelProvider(TEMP_INDEX));
		
		TableColumn dataColumn = new TableColumn(table, SWT.LEFT);
		dataColumn.setText(TEMPLATE_DATA);
		TableViewerColumn dataColumnViewer = new TableViewerColumn(viewer, dataColumn);
		dataColumnViewer.setEditingSupport(new LocationTableEditingSupport(viewer, TEMP_DATA_INDEX));
		dataColumnViewer.setLabelProvider(new LocationDataLabelProvider(TEMP_DATA_INDEX));
		
	}

	@Override
	protected void setShellStyle(int newShellStyle) {
		super.setShellStyle(newShellStyle | SWT.RESIZE);
	}

	

	/**
	 * 
	 * @author Pardhu
	 *
	 */
	
	
	private class LocationDataLabelProvider extends ColumnLabelProvider {

		int index;

		public LocationDataLabelProvider(int index) {

			this.index = index;
		}

		@Override
		public String getText(Object element) {

			if (element instanceof CalloutRow) {

				CalloutRow row = (CalloutRow) element;

				switch (index) {

				case LAT_INDEX:
					return row.getLatitude();

				case LONG_INDEX:
					return row.getLongitude();

				case NAME_INDEX:
					return row.getName();

				case DESC_INDEX:
					return row.getDescription();

				case IMAGE_INDEX:
					return row.getImage();

				case SHOWCALLOUT_INDEX:
					return String.valueOf(row.getShowcallout());

				case TEMP_INDEX:
					return row.getTemplate();

				case TEMP_DATA_INDEX:{
					TemplateData data = row.getData();
					if(data == null || (data !=null && data.getWidgetDataList().isEmpty())){
						return "Not Defined";
					}
					else{
						return "Defined";
					}
				}


				}
			}

			return "...";
		}
	}

	/**
	 * 
	 * @author Pardhu
	 */
	
	private class LocationTableEditingSupport extends EditingSupport{

		int index;
		TextCellEditor textCell;
		CellEditor imageCellEditor;
		TemplateSelectionCellEditor tempEditor;
		TemplateDataCellEditor dataEditor;
		ComboBoxCellEditor comboEditor;
		
		
		public LocationTableEditingSupport(ColumnViewer viewer,int index) {
			super(viewer);
			Composite parent = (Composite)viewer.getControl();
			this.index = index;
			textCell = new TextCellEditor(parent,SWT.NONE);
			imageCellEditor = new MultipleImageSelectEditor(parent, "Browse Images", project.getName(),MobileChannels.getMobileChannel(platform),formCategory){
				
				@Override
				public Object openDialogBox(Control cellEditorWindow) {
					BrowseImageWindow window = new BrowseImageWindow("Browse Images",
							SkinEventUtil.getFolderName(MobileChannels.getMobileChannel(platform)), project.getName());
					window.setMultipleImageSelectionAllowed(false);
					window.open();
					doSetValue(window.getSelectedImages());
					return window.getImgName();

				}
				
			};
			
			tempEditor = new TemplateSelectionCellEditor(parent, KPage.MAP_MODE, project.getName());
			dataEditor = new TemplateDataCellEditor(parent, project,KPage.MAP_MODE , formCategory, platform);
			comboEditor = new ComboBoxCellEditor(parent, KUtils.booleanArray,SWT.READ_ONLY | SWT.SINGLE);
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			if(element instanceof CalloutRow){
				CalloutRow row = (CalloutRow)element;
			
			switch(index){
			case IMAGE_INDEX: return imageCellEditor;
			case TEMP_INDEX:  return tempEditor;
			case TEMP_DATA_INDEX:{ 
				dataEditor.setTemplateName(row.getTemplate());
				return dataEditor;}
			case SHOWCALLOUT_INDEX: return comboEditor;
			default:
				return textCell;
			}
			
			}
			return textCell;
		}

		@Override
		protected boolean canEdit(Object element) {

			
			if(element instanceof CalloutRow){
			 if(index == TEMP_DATA_INDEX){
				 CalloutRow row = (CalloutRow)element;
				 String template = row.getTemplate();
				 if(KUtils.DEFAULT_NONE.equals(template) || KUtils.EMPTY_STRING.equals(template)){
					return false;
				 }
				
				}
			 
			 return true;
			}
			
			return false;
			
		}

		@Override
		protected Object getValue(Object element) {

			if(element instanceof CalloutRow){
				CalloutRow row = (CalloutRow)element;
				
				switch(index){
				case LAT_INDEX: return row.getLatitude();
				case LONG_INDEX: return row.getLongitude();
				case NAME_INDEX: return row.getName();
				case DESC_INDEX: return row.getDescription();
				case IMAGE_INDEX: return row.getImage();
				case SHOWCALLOUT_INDEX:{
					
					if(row.getShowcallout() == false){
						return 0;
					}else{
						return 1;
					}
				}
				case TEMP_INDEX: return row.getTemplate();
				case TEMP_DATA_INDEX: return row.getData();
				}
			}
			return "";
			
		}

		@Override
		protected void setValue(Object element, Object value) {
			
			if(element instanceof CalloutRow){
			CalloutRow row = (CalloutRow)element;
			
				switch(index){
					case LAT_INDEX : row.setLatitude((String)value); break;
					case LONG_INDEX : row.setLongitude((String)value); break;
					case NAME_INDEX : row.setName((String)value); break;
					case DESC_INDEX : row.setDescription((String)value); break;
					case IMAGE_INDEX : row.setImage((String)value); break;
					case SHOWCALLOUT_INDEX : row.setShowcallout(new Boolean(KUtils.booleanArray[(Integer)value])); break;
					case TEMP_INDEX : row.setTemplate((String)value); break;
					case TEMP_DATA_INDEX : row.setData((TemplateData)value); break;
				}
				
				viewer.refresh();
			}
			
		}
		
	} 
}
