package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.model.IWidget;
import com.pat.tool.keditor.model.KForm;
import com.pat.tool.keditor.model.KMenuItem;
import com.pat.tool.keditor.model.KSeatMap;
import com.pat.tool.keditor.model.KSpacer;
import com.pat.tool.keditor.model.WidgetUtil;
import com.pat.tool.keditor.propertyDescriptor.TableLabelProvider;

public class WidgetSelectionDialog extends TrayDialog{
	
	private TableViewer srcViewer;
	private ArrayList<IWidget> totalwidgetList;
	private ArrayList<IWidget> widgetList;
	private ArrayList<IWidget> initialSelwidgetList;


	public ArrayList<IWidget> getWidgetList() {
		return widgetList;
	}

	public void setInitialSelwidgetList(ArrayList<IWidget> initialSelwidgetList) {
		this.initialSelwidgetList = initialSelwidgetList;
	}

	public WidgetSelectionDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle() | SWT.RESIZE); 
		setHelpAvailable(false);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		getShell().setText("Select widgets");
		
		Composite control = (Composite) super.createDialogArea(parent);
		
		Composite comp = new Composite(control, SWT.NONE);
		comp.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		
		GridLayout layout = new GridLayout(2,false);
		comp.setLayout(layout);
		
		Label origLabel = new Label(comp, SWT.NONE);
		origLabel.setText("Widget List:");
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		origLabel.setLayoutData(data);
		
		srcViewer = new TableViewer(comp, SWT.MULTI | SWT.BORDER|SWT.V_SCROLL);
		data = new GridData(GridData.FILL_BOTH);
		data.heightHint = 380;
		srcViewer.getControl().setLayoutData(data);
		srcViewer.setContentProvider(new ArrayContentProvider());
		srcViewer.setLabelProvider(new TableLabelProvider() {
			
			@Override
			public Image getColumnImage(Object element, int columnIndex) {
				if (element instanceof IWidget) {
					IWidget widget = (IWidget) element;
					return widget.getIconImage();
				}
				return null;
			}

			@Override
			public String getColumnText(Object element, int columnIndex) {
				if (element instanceof IWidget) {
					return ((IWidget) element).getName();
				}
				return null;
			}

		});
		
		totalwidgetList = new ArrayList<IWidget>();
		IWidget[] allWidgets = WidgetUtil.getAllWidgets();
		for(IWidget widget: allWidgets) {
			String name = widget.getName();
			if(name != null && (name.equals(KForm.DISPLAY_NAME) || name.equals(KSpacer.DISPLAY_NAME)) || 
					name.equals(KMenuItem.DISPLAY_NAME) || name.equals(KSeatMap.DISPLAY_NAME)) continue;
			totalwidgetList.add(widget);
		}
		srcViewer.setInput(totalwidgetList);
		
		IStructuredSelection sel = new StructuredSelection(initialSelwidgetList.toArray(new IWidget[initialSelwidgetList.size()]));
		srcViewer.setSelection(sel);
		
		return control;
	}
	
	@Override
	protected void okPressed() {
		ISelection selection = srcViewer.getSelection();
		List list = ((StructuredSelection)selection).toList();
		widgetList = new ArrayList<IWidget>();
		for(Object obj: list) {
			if(obj instanceof IWidget) {
				widgetList.add((IWidget)obj);
			}
		}
		super.okPressed();
	}

}
