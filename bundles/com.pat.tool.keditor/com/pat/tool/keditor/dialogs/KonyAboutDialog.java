package com.pat.tool.keditor.dialogs;

import java.io.File;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.kony.accounts.util.KonyAccountsAuthHelper;
import com.kony.accounts.util.KonyPaasAccountManager;
import com.kony.accounts.util.KonyUser;
import com.kony.ant.task.OS;
import com.kony.studio.StudioConstants;
import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.constants.Messages;
import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.mfintegration.MobileFabricUtils;
import com.pat.tool.keditor.utils.KUtils;
//import com.pat.tool.keditor.dialogs.PluginDetails;
/**
 * This is a license dialog, providing information about the current license. 
 * 
 * @author Sharath
 * @modified by Rakesh
 *
 */
public class KonyAboutDialog extends TrayDialog {
	
	private static final String KONY_ONE_STUDIO = StudioConstants.PRODUCT_NAME;
	private static final String COMMERCIAL = "Enterprise";
	private static final String EVALUATION = "Evaluation";
	private static final String STANDARD = "Standard";
	
	private static String formattedDate;
	private static String formattedText;
	private static KonyUser konyUser; 
	private static String imagePath = KUtils.getPluginLocation()+"src"+File.separator+"com"+
			File.separator+"pat"+File.separator+"tool"+File.separator+"keditor"+
			File.separator+"icons"+File.separator+"abtlic.png";
	
	private static final String licenseExpirySeg = "<th>License Expiry Date</th>";
	private static final String licenseExpColWidth = "<col width=\'25%\'>";
	private static final String licenseExpColValue = "<td>{3}</td>";
	
	//TODO following information must be picked from the file.
	//Deactivation note is not required ,as we are not supported: "<b>Note:</b> To de-activate Kony Visualizer License, please use the \'Command line licensing tool\' provided by Kony.This tool can be downloaded from <a href=#>http://developer.kony.com/KonyDocuments</a>"+
	
	private static final String aboutLicenseText = "<html><title>Kony License</title>"
			+ "<style type=\'text/css\'>"
			+ "	body {"
			+ "overflow: hidden;"
			+ "background-color: 00A1E4;"
			+ "color: #ffffff;"
			+ "font-family: Arial;"
			+ "font-size: 12px;"
			+ "}"
			+ "table {"
			+ "margin: 20px 0px 40px 0px;"
			+ "table-layout: fixed;"
			+ "}"
			+ "table th {"
			+ "font-weight: normal;"
			+ "font-size: 12px;"
			+ "text-align: left;"
			+ "}"
			+ "table td {"
			+ "font-size: 18px;"
			+ "}"
			+ "</style><body>"
			+ "<img src=\'{5}\'/>"
			+ "<table>"
			+ "<col width=\'25%\'>"
			+ "<col width=\'25%\'>"
			+ "<col width=\'25%\'>"
			+ licenseExpColWidth
			+ "<tr>"
			+ "<th>User Name</th>"
			+ "<th>Components</th>"
			+ "<th>Type of License</th>"
			+ licenseExpirySeg
			+ "</tr>"
			+ "<tr>"
			+ "<td>{0}</td>"
			+ "<td>"+ StudioConstants.PRODUCT_NAME+"</td>"
			+ "<td>{2}</td>"
			+ licenseExpColValue
			+ "</tr>"
			+ "</table>"
			+ "<div>Please visit <a style=\"color: #ffffff\" href=\'#\' onclick=javascript:window.open(\"{4}\")>{4}</a> to communicate with the Kony Developer Community and Product Support.</div>"
			+ "</body>"
			+ "</html>";
	
	// For Old version of license in *.txt format user name should not be shown. 
	//JSPFQA4733 -> User Name is not correct when we open "About Kony License"
	private static final String aboutLicTextWithoutUserName = "<html><title>Kony License</title>"
			+ "<style type=\'text/css\'>"
			+ "	body {"
			+ "overflow: hidden;"
			+ "background-color: 00A1E4;"
			+ "color: #ffffff;"
			+ "font-family: Arial;"
			+ "font-size: 12px;"
			+ "}"
			+ "table {"
			+ "margin: 20px 0px 40px 0px;"
			+ "table-layout: fixed;"
			+ "}"
			+ "table th {"
			+ "font-weight: normal;"
			+ "font-size: 12px;"
			+ "text-align: left;"
			+ "}"
			+ "table td {"
			+ "font-size: 18px;"
			+ "}"
			+ "</style><body>"
			+ "<img src=\'{5}\'/>"
			+ "<table>"
			+ "<col width=\'25%\'>"
			+ "<col width=\'25%\'>"
			+ "<col width=\'25%\'>"
			+ "<tr>"
			+ "<th>Components</th>"
			+ "<th>Type of License</th>"
			+ "<th>License Expiry Date</th>"
			+ "</tr>"
			+ "<tr>"
			+ "<td>"+ StudioConstants.PRODUCT_NAME+"</td>"
			+ "<td>{2}</td>"
			+ "<td>{3}</td>"
			+ "</tr>"
			+ "</table>"
			+ "<div>Please visit <a style=\"color: #ffffff\" href=\'#\' onclick=javascript:window.open(\"{4}\")>{4}</a> to communicate with the Kony Developer Community and Product Support.</div>"
			+ "</body>"
			+ "</html>";
	IStatus licenseStatus ;
	Button deactButton = null;
	Button activateBtn;
	Composite composite;
	Label labelNote2,label,labelNote1;
	boolean exitAfterDeactivation=false;
	private Image image ;
	private static boolean updateText;
	Browser browser ;
	static  {
		updateText();
	//	formattedText = formattedText.replace("{4}", applyFontType(customerEmail));
	//	formattedText = formattedText.replace("{5}", applyFontType(licenseNo));
	}
	
	private static void updateText() {
		
		KonyAccountsAuthHelper authHelper = KonyPaasAccountManager.getInstance().getAuthHelper();
		konyUser = authHelper != null ? authHelper.getActiveUser() : null;
		
		String licenseType = KEditorPlugin.isCloudMode() && MobileFabricUtils.isMFCloud() ? STANDARD : COMMERCIAL ;
		String customerName = KEditorPlugin.isCloudMode() && MobileFabricUtils.isMFCloud() ? (konyUser.getUserInfo() != null ? konyUser.getUserInfo().getName() : konyUser.getUserName()) : null;
		if(customerName == null)
			customerName = "";
		
		formattedDate = "";
		imagePath = OS.getOSSpecificPath(imagePath);
		if(imagePath.startsWith("\\") || imagePath.startsWith("/")) {
			imagePath = imagePath.substring(1, imagePath.length());
		}
		
		if (!licenseType.equals(STANDARD)) {
				licenseType="";
				customerName="";
		}
		
		formattedText = aboutLicenseText.replace("{0}",customerName);

//		formattedText = formattedText.replace("{1}", KONY_ONE_STUDIO);//Hard coded for Studio... -Rakesh
		formattedText = formattedText.replace("{2}", licenseType);
		if (konyUser != null) {
			formattedText = formattedText.replace(licenseExpColWidth, ""); // clear license expiry field
			formattedText = formattedText.replace(licenseExpirySeg, ""); // clear license expiry field
			formattedText = formattedText.replace(licenseExpColValue, ""); // clear license expiry field
		} else {
			formattedText = formattedText.replace("{3}", formattedDate);
		}
		formattedText = formattedText.replace("{5}", imagePath);
		
		String url = "http://developer.kony.com";
		if(KEditorPlugin.isCloudMode()) {
			url = "http://cloud.kony.com";
		}
		
		formattedText = formattedText.replace("{4}", url);
	}
	public KonyAboutDialog(Shell shell) {
		super(shell);
		setShellStyle((shell.getStyle() | SWT.SHEET) & (~SWT.RESIZE));
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(Messages.getMessage("LicenseDialog.shellTitle"));
	}

	@Override
	protected int getShellStyle() {
		return super.getShellStyle();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = (Composite) super.createDialogArea(parent);	
		composite = new Composite(c, SWT.BORDER);
		composite.setBackground(ColorConstants.white);
		GridLayout grid = new GridLayout(2, false);
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginWidth = 0;
		grid.marginHeight = 0;
		composite.setLayout(grid);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		composite.setLayoutData(gridData);
		createBrowserComposite(composite);
	
		setHelpAvailable(false);
		return c;
	}

	private void createBrowserComposite(Composite browsercomposite) {
		/*Composite composite = new Composite(browsercomposite, SWT.NONE);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.widthHint = 480;
		gridData.heightHint = 220;
		composite.setLayoutData(gridData);
		GridLayout grd = new GridLayout(1, false);
		grd.marginWidth = 0;
		grd.marginHeight = 0;
		composite.setLayout(grd);*/
		 
			updateText();// required when license has been changed throug manage option
			updateText = false;
		 
		
		browser = new Browser(composite, SWT.NONE | SWT.READ_ONLY );
		//new PluginDetails(browser, "openPluginDetailsDialog"); 
        GridData gridData2 = new GridData(SWT.FILL,SWT.FILL,true,true);
        gridData2.widthHint = 750;
        gridData2.heightHint = 220;
        browser.setLayoutData(gridData2);
        browser.setText(formattedText);
        
	}
	/**
	 * This is to hide the deactivate button and show the activate button
	 * 
	 */
	public void hideDeactivebtn() {
		 
//		deactButton.setVisible(false);
		labelNote1.setVisible(false);
	 
	
	}
	
	@Override
	protected void okPressed(){
	 
			super.okPressed();
	}
	@Override
	protected void cancelPressed() {
	 
			super.cancelPressed();
	}
	@Override
	protected void handleShellCloseEvent() {
		 
			super.handleShellCloseEvent();
	}
	private void createImageComposite(Composite composite) {
		Label label = new Label(composite, SWT.NONE );
		image = ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.license_kony_icon);
		label.setImage(image);
		GridData gridData = new GridData(SWT.CENTER, SWT.CENTER, false, false);
		gridData.horizontalIndent = 5;
		label.setLayoutData(gridData);
	}
}
