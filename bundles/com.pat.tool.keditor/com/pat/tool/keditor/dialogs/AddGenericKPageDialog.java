package com.pat.tool.keditor.dialogs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.cache.FilterPlatformCache;
import com.pat.tool.keditor.constants.IMobileChannelNameConstants;
import com.pat.tool.keditor.constants.NavigationViewConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.utils.FilterFormWindow;
import com.pat.tool.keditor.utils.FormPlatformObj;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.NScriptDialog;
import com.pat.tool.keditor.widgets.IMobileChannel;
import com.pat.tool.keditor.widgets.MobileChannels;
import com.pat.tool.keditor.widgets.TabletWrapperChannel;
import com.pat.tool.keditor.widgets.WrapperChannel;

public class AddGenericKPageDialog extends TrayDialog {

	private Map<String, List<String>> inputMap;
	private Map<String, List<FormPlatformObj>> comboMap;
	private String projectName;
	private int category;
	private ComboViewer catViewer;
	private ComboViewer formViewer;
	private Object[] selectedItem = new Object[2];

	public Object[] getSelectedItem() {
		return selectedItem;
	}

	public AddGenericKPageDialog(Shell shell, Map<String, List<String>> inputMap, String projectName, int category) {
		super(shell);
		setHelpAvailable(false);
		this.inputMap = inputMap;
		this.projectName = projectName;
		this.category = category;
		comboMap = constructComboMap();
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Add form/popup/header/footer");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		GridData data = new GridData(SWT.FILL,SWT.FILL,true,true);
		composite.setLayoutData(data);
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		composite.setLayout(layout);
		createComposite(composite);
		return composite;
	}

	private void createComposite(Composite composite) {
		Label catLabel = new Label(composite, SWT.NONE);
		catLabel.setText("Category: ");
		
		catViewer = new ComboViewer(composite);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		data.grabExcessHorizontalSpace = true;
		catViewer.getCombo().setLayoutData(data);
		catViewer.setContentProvider(new IStructuredContentProvider() {
			
			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}
			
			@Override
			public void dispose() {
			}
			
			@Override
			public Object[] getElements(Object inputElement) {
				if(inputElement instanceof Map<?,?>) {
					return ((Map<?,?>)inputElement).keySet().toArray();
				}
				return null;
			}
		});
		catViewer.setLabelProvider(new LabelProvider() {
			public String getText(Object element) {
				if(element instanceof String) {
					String str = (String) element;
					return (NavigationViewConstants.FOLDER_DIALOGS.equals(str)) ? NavigationViewConstants.FOLDER_POPUPS : str;
				}
				return "";
			}
		});
		catViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				formViewer.refresh(true);
				formViewer.getCombo().select(1);
				validate();
			}
		});
		catViewer.setInput(comboMap);
		catViewer.getCombo().select(0);
		
		Label resLabel = new Label(composite, SWT.NONE);
		resLabel.setText("Select resource: ");
		
		formViewer = new ComboViewer(composite);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = 80;
		formViewer.getCombo().setLayoutData(data);
		formViewer.setContentProvider(new IStructuredContentProvider() {
			
			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}
			
			@Override
			public void dispose() {
			}
			
			@Override
			public Object[] getElements(Object inputElement) {
				if(inputElement instanceof Map<?,?>) {
					StructuredSelection selection = (StructuredSelection) catViewer.getSelection();
					String key = (String) selection.getFirstElement();
					Map<String, List<FormPlatformObj>> input = (Map<String, List<FormPlatformObj>>) formViewer.getInput();
					return input.get(key).toArray();
				}
				return null;
			}
		});
		formViewer.setLabelProvider(new LabelProvider() {
			public String getText(Object element) {
				return ((FormPlatformObj)element).formName;
			}
		});
		formViewer.setInput(comboMap);
		formViewer.getCombo().select(1);
		formViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				validate();
			}
		});
		
		Button searchButton = new Button(composite, SWT.PUSH);
		searchButton.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.searchImage));
		data = new GridData(GridData.FILL_HORIZONTAL);
		searchButton.setLayoutData(data);
		searchButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				handleFormFilterButton();
			}
		});
		
		Label noteLabel = new Label(composite, SWT.NONE);
		noteLabel.setText("Note: Already existing items in tree will not be listed here.");
		noteLabel.setForeground(ColorConstants.blue);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 3;
		noteLabel.setLayoutData(data);
	}
	
	private void handleFormFilterButton() {
		ISelection selection = catViewer.getSelection();
		if(!(selection instanceof IStructuredSelection))
			return;
   		Object firstElement = ((IStructuredSelection) selection).getFirstElement();
   		String selectedCat = firstElement.toString();
		Map<String, List<String>> formsMap = new HashMap<String, List<String>>();
		List<String> namesList = inputMap.get(selectedCat);
		formsMap.put(IMobileChannelNameConstants.General, namesList);
		ArrayList<String> fList;
		List<IMobileChannel> disabledChannels =  FilterPlatformCache.getAllDisabledChannelsCache(projectName); 
		List<String> platNames = new ArrayList<String>();
		platNames.add(IMobileChannelNameConstants.General);
		for(int i=1; i<KUtils.FORK_PLATFORMS.length; i++) {
			IMobileChannel channel = MobileChannels.getMobileChannel(i);
			if(channel == null || (disabledChannels.contains(channel) || channel instanceof WrapperChannel || channel instanceof TabletWrapperChannel)) continue;
			String forkResource = "";
			if(i !=0) {	
				forkResource = KUtils.getResourceFolderName(channel);
			}
			fList = getForkedFormsByCat(forkResource, selectedCat, namesList);
			formsMap.put(channel.getDisplayName(), fList);
			platNames.add( channel.getDisplayName());
		}
		String result = KUtils.DEFAULT_NONE;
		FilterFormWindow win = new FilterFormWindow(getShell(),"Select ", namesList, "Form/Popup/Template");
		win.setSelectedItem(KUtils.DEFAULT_NONE);
		win.setPlatform(IMobileChannelNameConstants.General);
		win.setFormsMap(formsMap);
		win.setCategories(platNames.toArray(new String[platNames.size()]));
		int open = win.open();
    	if(open == Window.OK) {
			result = win.getSelectedItem();
			String platform = win.getPlatform();
			int platformID = IMobileChannelNameConstants.General.equals(platform) ? 0 :  MobileChannels.getMobileChannelByDisplayName(platform).getChannelID();
			FormPlatformObj obj = getForm(formViewer, platformID, result);
			StructuredSelection comboselection = new StructuredSelection(obj);
			formViewer.setSelection(comboselection);
		}
	}
	
	private FormPlatformObj getForm(ComboViewer inputViewer, int platform, String frmName) {
		FormPlatformObj resultobj = null;
		Map<String, List<FormPlatformObj>> input = (Map<String, List<FormPlatformObj>>) inputViewer.getInput();
		StructuredSelection selection = (StructuredSelection) catViewer.getSelection();
		String key = (String) selection.getFirstElement();
		ArrayList<FormPlatformObj> FORMS_LIST = (ArrayList<FormPlatformObj>)input.get(key);
		if(FORMS_LIST != null) {
			for (FormPlatformObj frmObj : FORMS_LIST) {
				if(frmObj.platform == platform && frmObj.formName.equals(frmName)) {
					resultobj = frmObj;
					break;
				}
			}
		}
		return resultobj;
	}
	
	private ArrayList<String> getForkedFormsByCat(String platform, String resCategory, List<String> namesList) {
		ArrayList<String> fList = new ArrayList<String> ();
		IProject project = KUtils.getProject(projectName);
		IFile file = null;
		String fName = null;
		if ((project != null) && project.exists()) {
			String frmCat = KUtils.getFormCategoryName(category);
			for(int i=1; i<namesList.size();i++) {
				fName = namesList.get(i);
				String resFolder = resCategory;
				if(NavigationViewConstants.FOLDER_HEADERS.equals(resCategory) || NavigationViewConstants.FOLDER_FOOTERS.equals(resCategory)) {
					resFolder = NavigationViewConstants.FOLDER_TEMPLATES + File.separator + resCategory;
				}
				file = project.getFile(File.separator+resFolder+File.separator+frmCat+File.separator+platform+File.separator+fName+".kl");
				if((file != null) && file.exists()) {
					fList.add(fName);
				}
			}
		}
		return fList;
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);
		validate();
	}
	
	@Override
	protected void okPressed() {
		selectedItem[0] = catViewer.getCombo().getItem(catViewer.getCombo().getSelectionIndex());
		selectedItem[1] = (FormPlatformObj)((StructuredSelection)formViewer.getSelection()).getFirstElement();
		super.okPressed();
	}
	
	private void validate() {
		int selectionIndex = formViewer.getCombo().getSelectionIndex();
		if(selectionIndex == -1) return;
		String item = formViewer.getCombo().getItem(selectionIndex);
		if(item == null || item.startsWith("--") || KUtils.DEFAULT_NONE.equals(item)) {
			getButton(IDialogConstants.OK_ID).setEnabled(false);
		} else {
			getButton(IDialogConstants.OK_ID).setEnabled(true);
		}
	}

	private Map<String, List<FormPlatformObj>> constructComboMap() {
		Map<String, List<FormPlatformObj>> comboMap = new LinkedHashMap<String, List<FormPlatformObj>>();
		for(String resFolder : inputMap.keySet()) {
			List<FormPlatformObj> formObjects = getformObjects(inputMap.get(resFolder), resFolder);
			comboMap.put(resFolder, formObjects);
		}
		return comboMap;
	}
	
	private List<FormPlatformObj> getformObjects(List<String> formList, String resFolder) {
		List<String> fList = new ArrayList<String> ();
		List<String> forkList;
		String displaystr = "";
		fList.addAll(formList);
		fList.add(0, "----General----");

		List<FormPlatformObj> FORMS_LIST = new ArrayList<FormPlatformObj>();
		for(int j=0;j<fList.size(); j++) {
			FormPlatformObj obj = new FormPlatformObj();
			obj.platform = 0;
			obj.formName = fList.get(j);
			FORMS_LIST.add(obj);
		}
		for(int i=1; i<KUtils.FORK_PLATFORMS.length; i++) {
			IMobileChannel channel = MobileChannels.getMobileChannel(i);
			if(channel == null) continue;
			String forkResource = "";
			if(i !=0) {
				forkResource = KUtils.getResourceFolderName(channel);
			}
			forkList = getForkedFormsByCat(formList, forkResource, resFolder);
			if(forkList.size() > 0) {
				displaystr = " Platform Specific";
				forkList.add(0,"----"+channel.getName()+displaystr+"----");
				for(int j=0;j<forkList.size(); j++) {
					FormPlatformObj obj = new FormPlatformObj();
					obj.platform = channel.getChannelID();
					obj.formName = forkList.get(j);
					FORMS_LIST.add(obj);
				}
			}
		}
		return FORMS_LIST;
	}

	private ArrayList<String> getForkedFormsByCat(List<String> formList, String platformFolder, String resFolder) {
		ArrayList<String> fList = new ArrayList<String> ();
		IProject project = KUtils.getProject(projectName);
		IFile file = null;
		String fName = null;
		if ((project != null) && project.exists()) {
			String folderName = resFolder;
			boolean isTemplete = NavigationViewConstants.FOLDER_HEADERS.equals(folderName) || NavigationViewConstants.FOLDER_FOOTERS.equals(folderName);
			if(isTemplete) {
				folderName = NavigationViewConstants.FOLDER_TEMPLATES + File.separator + folderName;
			}
			String frmCat = KUtils.getFormCategoryName(category);
			for(int i=1; i<formList.size();i++) {
				fName = formList.get(i);
				file = project.getFile(folderName+File.separator+frmCat+File.separator+platformFolder+File.separator+fName+".kl");
				if((file != null) && file.exists()) {
					fList.add(fName);
				}
			}
		}
		return fList;
	}

}
