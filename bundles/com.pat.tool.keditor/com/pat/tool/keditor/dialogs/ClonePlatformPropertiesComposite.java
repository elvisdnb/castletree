package com.pat.tool.keditor.dialogs;


import java.util.Collection;
import java.util.List;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import com.pat.tool.keditor.cache.FilterPlatformCache;
import com.pat.tool.keditor.platformselection.PlatformSelectionComposite;
import com.pat.tool.keditor.utils.RendererHashMapData;
import com.pat.tool.keditor.views.NavigationView;
import com.pat.tool.keditor.widgets.DesktopKioskChannel;
import com.pat.tool.keditor.widgets.IMobileChannel;
import com.pat.tool.keditor.widgets.MobileChannels;
import com.pat.tool.keditor.widgets.RichClientChannel;
import com.pat.tool.keditor.widgets.TabletRichClientChannel;
import com.pat.tool.keditor.widgets.TabletSPAChannel;
import com.pat.tool.keditor.widgets.ThinClientChannel;

public class ClonePlatformPropertiesComposite extends Composite {

	private static final String RC_CHANNELS = "---" + MobileChannels.MOBILE_RICH_CLIENT + "---";
	private static final String TC_CHANNELS = "---" + MobileChannels.MOBILE_THIN_CLIENT + "---";
	private static final String TABLET_RC_CHANNELS = "---" + MobileChannels.TABLET_RICH_CLIENT + "---";
	private static final String TABLET_WEB_CHANNELS = "---" + MobileChannels.TABLET_THIN_CLIENT + "---";
	private static final String DESKTOP_KIOSK_CHANNELS = "---" + MobileChannels.DESKTOP_KIOSK_CLIENT + "---";
	private Combo platCombo;
	private PlatformSelectionComposite platSelComp;
	private Collection<String> disabledCategories;
	private boolean hideLabelAndAllButton;
	
	
	public ClonePlatformPropertiesComposite(Composite parent, int style) {
		this(parent, style, null, false);
	}
	
	public ClonePlatformPropertiesComposite(Composite parent, int style, Collection<String> disabledCategories, boolean hideLabelAndAllButton) {
		super(parent, style);
		this.disabledCategories = disabledCategories;
		this.hideLabelAndAllButton = hideLabelAndAllButton;
		setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		GridLayout layout = new GridLayout(2,false);
	    setLayout(layout);
		createUI();
	}
	
	private void createUI() {
		Label platNameLabel = new Label(this, SWT.NONE);
		platNameLabel.setText("Source Platform: ");
		
		platCombo = new Combo(this, SWT.DROP_DOWN | SWT.READ_ONLY);
		platCombo.setLayoutData(new GridData(SWT.FILL,SWT.BEGINNING,true,false));
		platCombo.setVisibleItemCount(10);
		platCombo.addSelectionListener(new SelectionAdapter(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkComboText();
				updateComposite();
			}
		});
		
		int platformSize = 0;
		String projName ="";
		
		if(NavigationView.getInstance() != null){
			projName = NavigationView.getInstance().getProjectName();
		}
		List<IMobileChannel> disabledChannels = FilterPlatformCache.getAllDisabledChannelsCache(projName);
			  if(!MobileChannels.isRichClientDisabled(projName)){
				  platformSize++;
					for(IMobileChannel platform : RichClientChannel.values()){
						if (!disabledChannels.contains(platform)) {
							if(!(RichClientChannel.WINPHONE81S ==platform))
									platformSize++;
						}
					}
			  }
			  if(!MobileChannels.isTabletRichClientDisabled(projName)){
				  platformSize++;
					for(IMobileChannel platform : TabletRichClientChannel.values()){
						if (!disabledChannels.contains(platform)) {
							 platformSize++;
						}
					}
			  }
			  if(!MobileChannels.isTabletSPADisabled(projName)){
				  platformSize++;
					for(IMobileChannel platform : TabletSPAChannel.values()){
						if (!disabledChannels.contains(platform)) {
							 platformSize++;
						}
					}
			  }
			 
			  if(!MobileChannels.isThinClientDisabled(projName)){
				  platformSize++;
					for(IMobileChannel platform : ThinClientChannel.values()){
						if (!disabledChannels.contains(platform)) {
							 platformSize++;
						}
					}
			  }
			  if(!MobileChannels.isDesktopKioskDisabled(projName)){
				  platformSize++;
					for(IMobileChannel platform : DesktopKioskChannel.values()){
						if (!disabledChannels.contains(platform)) {
							 platformSize++;
						}
					}
			  }
		String[] platNames = new String[platformSize];
		RichClientChannel[] rc_platforms = RichClientChannel.values();
		TabletRichClientChannel[] trc_platforms = TabletRichClientChannel.values();
		TabletSPAChannel [] tweb_platforms = TabletSPAChannel.values();
		ThinClientChannel[] tc_platforms = ThinClientChannel.values();
		DesktopKioskChannel[] dc_platforms = DesktopKioskChannel.values();
		
		int i = 0;
		if (!MobileChannels.isRichClientDisabled(projName)) {
			platNames[i++] = RC_CHANNELS;
			for (int j = 0; j < rc_platforms.length; j++ ) {
				if (!disabledChannels.contains(rc_platforms[j])) {
					if(!(RichClientChannel.WINPHONE81S ==rc_platforms[j]))
						platNames[i++] = rc_platforms[j].getDisplayName();
				}
			}
		}
		if (!MobileChannels.isTabletRichClientDisabled(projName)) {
		platNames[i++] = TABLET_RC_CHANNELS;
		for(int j = 0 ; j < trc_platforms.length; j++){
			if (!disabledChannels.contains(trc_platforms[j])) {
			platNames[i++] = trc_platforms[j].getDisplayName();
			}
		}
	}
		if (!MobileChannels.isTabletSPADisabled(projName)) {
		platNames[i++] = TABLET_WEB_CHANNELS;
		for(int j = 0 ; j < tweb_platforms.length; j++){
			if (!disabledChannels.contains(tweb_platforms[j])) {
			platNames[i++] = tweb_platforms[j].getDisplayName();
			}
		}
		}
		
		if (!MobileChannels.isThinClientDisabled(projName)) {
		platNames[i++] = TC_CHANNELS;
		for(int j = 0 ; j < tc_platforms.length; j++ ){
			if (!disabledChannels.contains(tc_platforms[j])) {
				platNames[i++] = tc_platforms[j].getDisplayName();
			}
		}
		}
		if (!MobileChannels.isDesktopKioskDisabled(projName)) {
		platNames[i++] = DESKTOP_KIOSK_CHANNELS;
		for(int j = 0 ; j < dc_platforms.length; j++){
			if (!disabledChannels.contains(dc_platforms[j])) {
				platNames[i++] = dc_platforms[j].getDisplayName();
			}
			
		}
		}
		platCombo.setItems(platNames);
		platCombo.select(0);
		checkComboText();
		
		Group targetGroup = new Group(this, SWT.NONE);
		targetGroup.setText("Target Platforms");
		GridDataFactory.fillDefaults().grab(true, true).span(2, 1).applyTo(targetGroup);
		GridLayoutFactory.fillDefaults().applyTo(targetGroup);
		
		RendererHashMapData rendererData = new RendererHashMapData(false);
		platSelComp = new PlatformSelectionComposite(targetGroup, SWT.NONE, rendererData) {
			protected Composite createExtendedComposite(Composite parent) {
				return ClonePlatformPropertiesComposite.this.createExtendedComposite(parent);
			};
			
			protected boolean inlineExtendedComposite() {
				return ClonePlatformPropertiesComposite.this.inlineExtendedComposite();
			};
		};
		platSelComp.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,1,1));
		if (disabledCategories != null) {
			platSelComp.setDisabledCategories(disabledCategories);
		}
		if (hideLabelAndAllButton) {
			platSelComp.hideAllButtonAndMessageLabel();
		}
		platSelComp.init();
		
		updateComposite();
	}
	
	
	protected Composite createExtendedComposite(Composite parent) {
		return null;
	}
	
	protected boolean inlineExtendedComposite() {
		return true;
	};
	

	/**
	 * Updates the Platform Selection composite
	 */
	protected void updateComposite() {
		IMobileChannel platform = MobileChannels.getMobileChannelByDisplayName(platCombo.getText());
		platSelComp.setSelection(new RendererHashMapData(false));
		platSelComp.updateButtonsEnabled(new RendererHashMapData(true));
		platSelComp.updateButtonsEnabled(platform, false);
	}
	

	public IMobileChannel getClonePlatform() {
		return MobileChannels.getMobileChannelByDisplayName(platCombo.getText());
	}

	public RendererHashMapData getRenderData() {
		return platSelComp.getUpdatedRendererHashMapData();
	}

	private void checkComboText() {
		if (platCombo.getText().equals(RC_CHANNELS)
				|| platCombo.getText().equals(TC_CHANNELS) 
				|| platCombo.getText().equals(TABLET_RC_CHANNELS) 
					|| platCombo.getText().equals(TABLET_WEB_CHANNELS) 
				|| platCombo.getText().equals(DESKTOP_KIOSK_CHANNELS) 
				|| MobileChannels.getMobileChannelByDisplayName(platCombo.getText()) == null)
			platCombo.select(platCombo.getSelectionIndex() + 1);
	}
	
	public void addPlatformSelectionListener(Listener listener) {
		platSelComp.addValidationEventListener(listener);
	}
	

}
