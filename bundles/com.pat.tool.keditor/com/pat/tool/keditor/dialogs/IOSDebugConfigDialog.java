package com.pat.tool.keditor.dialogs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.kony.ant.task.OS;
import com.konylabs.middleware.common.EncryptDecryptText;
import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.utils.IOSBuildUtils;
import com.pat.tool.keditor.utils.UIUtils;
import com.pat.tool.keditor.utils.ValidationUtil;
/**
 * This is a About KonyStudio dialog, providing information about the KonyStudio. 
 * 
 * @author Lakshman K
 * Since 7.1
 *
 */
public class IOSDebugConfigDialog extends TrayDialog {
	
	
	Button buttonOK;
	Composite composite;
	private Text macConfigName;
	private Text macIP;
	private Text macPortNumber;
	private Text macUserName;
	private Text macPassword;
	private Text chromePath;
	private Button browseButton;
	private Button testConnection;
	private Label resultLabel;
	private Label connResultLabel;
	private Label errorLabel;
	private ModifyEventHandler modifyListener;
	private CustomSelectionAdapter customSelectionAdapter;
	private ConfigData configData;
	private boolean isChromePathFound = false;
	private String chromePathString = "";
	
	public IOSDebugConfigDialog(Shell shell) {
		super(shell);
		setShellStyle((shell.getStyle() | SWT.SHEET) & ~SWT.RESIZE);
		modifyListener = new ModifyEventHandler();
		customSelectionAdapter = new CustomSelectionAdapter();
		String chromeExePath = "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";
		File chromeExecutableFile = new File(chromeExePath);
		if(!chromeExecutableFile.exists()) {
			chromeExePath = "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
			chromeExecutableFile = new File(chromeExePath);
			if(!chromeExecutableFile.exists()) {
				isChromePathFound = false;
			} else {
				chromePathString = chromeExePath;
				isChromePathFound = true;
			}
		} else {
			chromePathString = chromeExePath;
			isChromePathFound = true;
		}
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("iOS Debug Configuration");
	}

	@Override
	protected int getShellStyle() {
		return super.getShellStyle();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = (Composite) super.createDialogArea(parent);	
		composite = new Composite(c, SWT.BORDER);
//		composite.setSize(800, 800);
		GridLayout grid = new GridLayout(3, false);
		/*grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginWidth = 0;
		grid.marginHeight = 0;*/
		composite.setLayout(grid);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.minimumWidth = 500; 
		composite.setLayoutData(gridData);
		createComposite(composite);
		setDefaultValues();
		setHelpAvailable(false);
		return c;
	}
	
	private void createComposite(Composite composite) {
		
		errorLabel = new Label(composite, SWT.NONE);
		errorLabel.setText("");
		GridData gridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false,3,1);
		errorLabel.setLayoutData(gridData);
		
		Label macName = new Label(composite, SWT.NONE);
		macName.setText("Config Name: ");
			
		macConfigName = new Text(composite, SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false,2,1);
		macConfigName.setLayoutData(gridData);
		macConfigName.addListener(SWT.Modify, modifyListener);
		
        Label macIPLabel = new Label(composite, SWT.NONE);
		macIPLabel.setText("Mac IP Address: ");
		
		macIP = new Text(composite, SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false,2,1);
		macIP.setLayoutData(gridData);
		macIP.addListener(SWT.Modify, modifyListener);

		Label macPortLabel = new Label(composite, SWT.NONE);
		macPortLabel.setText("Mac Port Number: ");
		
		macPortNumber = new Text(composite, SWT.BORDER );
		gridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false,2,1);
		macPortNumber.setLayoutData(gridData);
		macPortNumber.addListener(SWT.Modify, modifyListener);
		
		Label macIPUserName = new Label(composite, SWT.NONE);
		macIPUserName.setText("Mac Username: ");
		
		macUserName = new Text(composite, SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false,2,1);
		macUserName.setLayoutData(gridData);
		macUserName.addListener(SWT.Modify, modifyListener);
		
		Label macPasswordLabel = new Label(composite, SWT.NONE);
		macPasswordLabel.setText("Mac Password: ");
		
		macPassword = new Text(composite, SWT.BORDER|SWT.PASSWORD);
		gridData = new GridData(SWT.FILL, SWT.BEGINNING, true, false,2,1);
		macPassword.setLayoutData(gridData);
		macPassword.addListener(SWT.Modify, modifyListener);
		
		Label chromePathLabel = new Label(composite, SWT.NONE);
		chromePathLabel.setText("Google Chrome Location: ");
		
		
		chromePath = new Text(composite, SWT.BORDER);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		chromePath.setLayoutData(gridData);
		chromePath.addListener(SWT.Modify, modifyListener);
		
		browseButton = new Button(composite, SWT.PUSH);
		browseButton.setText("Browse");
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		browseButton.setLayoutData(gridData);
		browseButton.addSelectionListener(customSelectionAdapter);
		if(isChromePathFound) {
			UIUtils.excludeWidget(chromePathLabel, true);
			UIUtils.excludeWidget(chromePath, true);
			UIUtils.excludeWidget(browseButton, true);
		}
		
		
		Composite composite1 = new Composite(composite, SWT.NONE);
		GridLayout grid = new GridLayout(3, false);
		composite1.setLayout(grid);
		gridData = new GridData(SWT.FILL, SWT.FILL, true, false,3,1);
		composite1.setLayoutData(gridData);
		
		
		testConnection = new Button(composite1, SWT.PUSH);
		testConnection.setText("Test Connection");
		gridData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false);
		testConnection.setLayoutData(gridData);
		testConnection.addSelectionListener(customSelectionAdapter);
		testConnection.setEnabled(false);
		
		resultLabel = new Label(composite1, SWT.NONE);
		gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		resultLabel.setLayoutData(gridData);
		resultLabel.setText("Result: ");
		
		connResultLabel = new Label(composite1, SWT.NONE);
		gridData = new GridData(SWT.BEGINNING, SWT.CENTER, false, false);
		connResultLabel.setLayoutData(gridData);
		connResultLabel.setText("Test connection successful");
		
		UIUtils.excludeWidget(connResultLabel, true);
	}
	
	
	/*@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		GridLayout gridlayout = (GridLayout) parent.getLayout();
		gridlayout.marginHeight = 0;
	}*/
	
	@Override
	protected boolean isResizable() {
		return false;
	}
	
	
	@Override
	protected void okPressed(){
		if(!validate()) {
			return;
		} else {
			performOKAction();
		}
		super.okPressed();
	}
	private void performOKAction() {
		
		try {
			String configFileLocation = getConfigFileLocation();
			File configFile = new File(configFileLocation);
			if(configFile.exists()) {
				configFile.delete();
			}
			String location = KEditorPlugin.getWorkspaceLoc() + File.separator + "_ios";
			File file = new File(location);
			if(!file.exists()) {
				file.mkdirs();
			}
			configFile.createNewFile();
			/*JSONParser parser = new JSONParser();		 
			jsonObject = (JSONObject) parser.parse(new FileReader(file)); */
			
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObj = new JSONObject();
			
			String username = macUserName.getText();
			String password = EncryptDecryptText.encryptText(macPassword.getText());
			String ip = macIP.getText();
			String port = macPortNumber.getText();
			String macConfigarationName = macConfigName.getText();
			if(!isChromePathFound) {
				chromePathString = chromePath.getText();
			}
			
			jsonObj.put("configName", macConfigarationName);
			jsonObj.put("macIP", macIP.getText());
			jsonObj.put("macPort", port);
			jsonObj.put("macUserName", username);
			jsonObj.put("macPassword", password);
			jsonObj.put("chromePath", chromePathString);
			
			jsonArray.add(jsonObj);
			
			jsonObject.put("DATA",jsonArray);
			updateJSONFile(jsonObject);

			configData = new ConfigData();
			configData.setIp(ip);
			configData.setMacPortNumber(port);
			configData.setMacConfigarationName(macConfigarationName);
			configData.setPassword(password);
			configData.setUsername(username);
			configData.setChromePath(chromePathString);
			setConfigDataObject(configData);
			
		} catch (IOException e) {
			KEditorPlugin.logError(e.getMessage(), e);
		}
	}

	private void setDefaultValues() {
		try {
			String configFileLocation = getConfigFileLocation();
			File configFile = new File(configFileLocation);
			if(!configFile.exists()) {
				return;
			}
			JSONParser parser = new JSONParser();		 
			JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(configFile));
			JSONArray jsonArray = (JSONArray) jsonObject.get("DATA");
			for (Object object : jsonArray) {
				if(object instanceof JSONObject) {
					JSONObject obj = (JSONObject) object;
					macConfigName.setText((String) obj.get("configName"));
					macIP.setText((String) obj.get("macIP"));
					macPortNumber.setText((String) obj.get("macPort"));
					macUserName.setText((String) obj.get("macUserName"));
					macPassword.setText(EncryptDecryptText.decryptText((String) obj.get("macPassword")));
					if(!isChromePathFound && chromePath != null) {
						chromePath.setText((String) obj.get("chromePath"));
					}
				}
			}
			
		} catch (FileNotFoundException e) {
			KEditorPlugin.logError(e.getMessage(), e);
		} catch (IOException e) {
			KEditorPlugin.logError(e.getMessage(), e);
		} catch (ParseException e) {
			KEditorPlugin.logError(e.getMessage(), e);
		} 
	}
	
	private String getConfigFileLocation() {
		String workspaceLoc = KEditorPlugin.getWorkspaceLoc();
		String location = workspaceLoc + File.separator + "_ios" + File.separator+"debugConfigFile.json";
		return location;
	}
	/**
	 * @param configObj
	 * Updates the JSON file with new debug config information.
	 */
	private void updateJSONFile(JSONObject configObj) {
		FileWriter file = null;
		try {
			file = new FileWriter(getConfigFileLocation());
			file.write(configObj.toJSONString());        
			file.flush();
			file.close();
		} catch (Exception e) {
			KEditorPlugin.logError(e.getMessage(), e);
			
		} 
	}

	@Override
	protected void cancelPressed() {
		super.cancelPressed();
	}
	
	
	private class ModifyEventHandler implements Listener {
		@Override
		public void handleEvent(Event event) {
			validate();
		}
	}
	public boolean validate() {
		boolean isValid = true;
		String username = macUserName.getText();
		String password = macPassword.getText();
		String ip = macIP.getText();
		String port = macPortNumber.getText();
		String macConfigarationName = macConfigName.getText();
		String chromePathStr = chromePathString;
		if(!isChromePathFound && chromePath != null) {
			chromePathStr = chromePath.getText();
		}
		
		if(OS.isWindowsOS() && ValidationUtil.isNonEmptyString(username) && ValidationUtil.isNonEmptyString(password) && ValidationUtil.isNonEmptyString(ip) && 
				ValidationUtil.isNonEmptyString(macConfigarationName) && ValidationUtil.isNonEmptyString(port)) {
			errorLabel.setForeground(Display.getDefault().getSystemColor(3));
			errorLabel.setText("");
			testConnection.setEnabled(true);
		}
		
		if(OS.isWindowsOS() && (!ValidationUtil.isNonEmptyString(username) || !ValidationUtil.isNonEmptyString(password) || !ValidationUtil.isNonEmptyString(ip) || 
				!ValidationUtil.isNonEmptyString(macConfigarationName) || !ValidationUtil.isNonEmptyString(port)) ){
			errorLabel.setForeground(Display.getDefault().getSystemColor(3));
			errorLabel.setText("Username/Password/IP/Port Number/Config Name can't be empty");
			testConnection.setEnabled(false);
			isValid = false;
		}else if(OS.isWindowsOS() && !ValidationUtil.isValidIPAddress(ip)){
			errorLabel.setForeground(Display.getDefault().getSystemColor(3));
			errorLabel.setText("Invalid IP address");
			isValid = false;
		}
		if(OS.isWindowsOS() && !isValidPortNumber(port)) {
			errorLabel.setForeground(Display.getDefault().getSystemColor(3));
			errorLabel.setText("Invalid Port Number, Port number should be in between 9221 - 9321.");
			isValid = false;
		}
		if(OS.isWindowsOS() && !ValidationUtil.isNonEmptyString(chromePathStr)) {
			errorLabel.setForeground(Display.getDefault().getSystemColor(3));
			errorLabel.setText("Google Chrome Executable Path can't be empty");
			testConnection.setEnabled(false);
			isValid = false;
		}
		if(OS.isWindowsOS() && !isValidChromePath(chromePathStr)) {
			errorLabel.setForeground(Display.getDefault().getSystemColor(3));
			errorLabel.setText("Google Chrome Executable Path is Invalid.");
			testConnection.setEnabled(false);
			isValid = false;
		}
		if(isValid){
			errorLabel.setText("");
		} else {
			/*if(getOKButton() != null) {
				getOKButton().setEnabled(false);
			}*/
		}
		return isValid;
	}
	private boolean isValidChromePath(String chromePathStr) {
		if(chromePathStr != null && !chromePathStr.isEmpty() && chromePathStr.toLowerCase().endsWith("chrome.exe")) {
			return true;
		}
		return false;
	}

	public boolean isValidPortNumber(String port) {

		try {
			int i = Integer.parseInt(port);
			if (i <= 9220 || i >= 9322) {
				return false;
			}
		} catch (Exception e) {
			return false;
		}

		return true;
	}
	private class CustomSelectionAdapter extends SelectionAdapter {
		
		@Override
		public void widgetSelected(SelectionEvent e) {
			final String macIPText = macIP.getText();
			final String macUserText = macUserName.getText();
			final String macPasswordText = macPassword.getText();
			if(e.widget == testConnection) {
				testConnectionButtonAction(macIPText,macUserText,macPasswordText);
			} else if(browseButton != null && e.widget == browseButton) {
				browseButtonAction();
			}
		}
	}
	public boolean testConnectionButtonAction(String macIPAddress, String macUsername, String macPwd) {

		boolean validIPAddress = ValidationUtil.isValidIPAddress(macIPAddress);
		boolean connectToServer = validIPAddress;
		String errorMsg = "Test connection failure";
		if(validIPAddress) {
			boolean connectionTest = false;
			try {
				connectionTest = IOSBuildUtils.connectThroughCode(macIPAddress, macUsername, macPwd);
			} catch (Exception ex) {
				String exMessage = ex.getMessage();
				if(exMessage != null && exMessage.contains("Auth cancel")) {
					errorMsg = "Authentication Failed. Username/Password is incorrect";
				}
			}
			connectToServer = validIPAddress && connectionTest;
		}
		UIUtils.excludeWidget(connResultLabel, false);
		if(connectToServer) {
			connResultLabel.setForeground(Display.getDefault().getSystemColor(6));
			connResultLabel.setText("Test connection successful");
			connResultLabel.setVisible(true);
			connResultLabel.getParent().layout(new Label[]{connResultLabel});
			return true;
		} else {
			connResultLabel.setForeground(Display.getDefault().getSystemColor(3));
			if(validIPAddress) {
				connResultLabel.setText(errorMsg);
			} else {
				connResultLabel.setText("Invalid IP Address");
			}
			connResultLabel.setVisible(true);
		}
		connResultLabel.getParent().layout(new Label[]{connResultLabel});
		return false;
	}
	public void browseButtonAction() {
		try {
			FileDialog fileDialog = new FileDialog(Display.getDefault().getActiveShell(), SWT.NONE);
			fileDialog.setText("Select Google Chrome Location.");
			fileDialog.setFilterExtensions(new String[]{"*.exe"});
			String selectedFile = fileDialog.open();
			if(selectedFile == null){
				return;
			}
			if(!selectedFile.toLowerCase().endsWith(".exe")){
				selectedFile = selectedFile+".exe";
			}
			chromePath.setText(selectedFile);
		} catch (Exception exception) {
			KEditorPlugin.getDefault().logError("Error in opening the File Editor in iOS Debug Config.", exception);
		}
		
	}

	public ConfigData getConfigDataObject() {
		return configData;
	}
	
	public void setConfigDataObject(ConfigData configData) {
		this.configData = configData;
	}
	public class ConfigData {
		private String username ;
		private String password ;
		private String ip ;
		private String macConfigarationName ;
		private String macPortNumber ;
		private String chromePath ;
		
		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public String getMacConfigarationName() {
			return macConfigarationName;
		}

		public void setMacConfigarationName(String macConfigarationName) {
			this.macConfigarationName = macConfigarationName;
		}

		public String getMacPortNumber() {
			return macPortNumber;
		}
		
		public void setMacPortNumber(String macPortNumber) {
			this.macPortNumber = macPortNumber;
		}

		public String getChromePath() {
			return chromePath;
		}
		
		public void setChromePath(String chromePath) {
			this.chromePath = chromePath;
		}

		public ConfigData() {
			
		}
	}
	
}
