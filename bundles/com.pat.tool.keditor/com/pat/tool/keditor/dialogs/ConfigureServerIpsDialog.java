package com.pat.tool.keditor.dialogs;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CheckboxCellEditor;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.console.ConsoleDisplayManager;
import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.propertyDescriptor.CustomSelectionAdapter;
import com.pat.tool.keditor.propertyDescriptor.CustomTextCellEditor;
import com.pat.tool.keditor.propertyDescriptor.TableLabelProvider;
import com.pat.tool.keditor.table.TableWithButtons;
import com.pat.tool.keditor.table.TableWithButtonsDeleteRowAction;
import com.pat.tool.keditor.table.TableWithButtonsNewRowAction;
import com.pat.tool.keditor.utils.ValidationUtil;

/***
 * 
 * @author Manjula(KH080)
 * @date March 03, 2013
 * @since 5.0
 *
 */
public class ConfigureServerIpsDialog extends TitleAreaDialog {

	private static final String IP_SELECTED_PROP = "selected";
	private static final String IP_ADDRESS_PROP = "ip address";
	private static final String IP_HTTP_PORT_PROP = "http port";
	private static final String IP_HTTPS_PROP = "https port";
	
	public static final String NA_PORT = "NA";
	private TableWithButtons configIpTable;
	private Table table;
	
	private static List<UrlTableElement> localElms = new ArrayList<ConfigureServerIpsDialog.UrlTableElement>();
	private static List<UrlTableElement> configElms = new ArrayList<ConfigureServerIpsDialog.UrlTableElement>();

	private static List<String> selectedList = new ArrayList<String>();
	private static List<String> configList = new ArrayList<String>();
	
	private static final String MESSAGE = "Select all applicable Kony servers for publish";
	private static final String ERROR_MESSAGE = "IP Address or Port is not configured";
	
	/**
	 * 
	 * @param shell
	 * @param selectedList is combination of selected items from local system ips and configurable ips
	 * @param configList list of ips which are configurable (added/ removed by user)
	 */
	
	public ConfigureServerIpsDialog(Shell shell, List<String> selectedList1, List<String> configList1) {
		super(shell);
		selectedList = selectedList1;
		configList = configList1;
		localElms.clear();
		configElms.clear();
		setShellStyle(getShellStyle() | SWT.RESIZE);
		setHelpAvailable(false);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Configure Server Address List");
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		composite.setLayout(new GridLayout(1, false));
		setTitle("Configure Kony Server");
		setMessage(MESSAGE);
		constructInput();
		createViewerGroup(composite, "Local Kony Servers", false, localElms);
		
		if (KEditorPlugin.isCloudEnterpriseMode()) {
			
			configIpTable = createViewerGroup(composite, "External Kony Servers", true, configElms);
			addConfigTableActions(configIpTable);
			validate(configElms);
		}
		
		return composite;
	}

	private void addConfigTableActions(final TableWithButtons configIpTable) {
		TableWithButtonsNewRowAction addAction = new TableWithButtonsNewRowAction(configIpTable.getViewer(), "Add") {
			@Override
			protected String getNewRowFirstCellVal() {
				return null;
			}
			
			@Override
			protected Object createNewRow(String firstCellVal) {
				UrlTableElement urlTableElement = new UrlTableElement();
				urlTableElement.selected = true;
				configElms.add(urlTableElement);
				configIpTable.getViewer().refresh();
				configIpTable.getViewer().getTable().select(0);
				validate(configElms);
				return configIpTable;
			}
		};
		
		configIpTable.getActionsManager().add(addAction);
		TableWithButtonsDeleteRowAction deleteAction = new TableWithButtonsDeleteRowAction(configIpTable.getViewer(), "Remove") {
			@Override
			protected void deleteRows(IStructuredSelection selection) {
				int firstIndex = configElms.indexOf(selection.getFirstElement());
				for (Object element : selection.toArray()) {
					configElms.remove(element);
				}
				configIpTable.getViewer().refresh();
				configIpTable.getViewer().getTable().select(--firstIndex);
			};
		};
		configIpTable.getActionsManager().add(deleteAction);
	    configIpTable.getActionsManager().update(true);
	}

	private TableWithButtons createViewerGroup(Composite composite, String groupName, boolean isIpEditable, List<UrlTableElement> input) {
		Group group = new Group(composite, SWT.NONE);
		group.setText(groupName);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(group);
		GridLayoutFactory.fillDefaults().numColumns(isIpEditable ? 2 : 1).applyTo(group);
		
		TableWithButtons tableWithButtons = new TableWithButtons(group, SWT.BORDER|SWT.FULL_SELECTION| SWT.V_SCROLL|SWT.H_SCROLL);
		tableWithButtons.createControl();
		TableViewer viewer = tableWithButtons.getViewer();
		table = viewer.getTable();
		
		
		GridData gridData = new GridData(GridData.FILL_BOTH);
		gridData.widthHint = 500;
		gridData.heightHint = 300;
		composite.setLayoutData(gridData);
		
		TableLayout tablelayout = createLayout();

		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		table.setLayout(tablelayout);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		//table.addSelectionListener(new CustomSelectionAdapter());
		addColumns();
		setContentProvider(viewer);
		setLabelProvider(viewer);
		viewer.setInput(input);
		attachCellEditors(viewer, table, isIpEditable);
		TableViewerEditor.create(viewer,new ColumnViewerEditorActivationStrategy(viewer),
				ColumnViewerEditor.TABBING_HORIZONTAL|ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR|ColumnViewerEditor.TABBING_VERTICAL);
		return tableWithButtons;
	}

	public static void constructInput() {
		for (String ip : getLocalIps()) {
			localElms.add(new UrlTableElement(ip, "8080", NA_PORT));
		}
		for (String configUrl : configList) {
			UrlTableElement configElement = new UrlTableElement(configUrl);
			configElement.selected = selectedList.contains(configUrl);
			configElms.add(configElement);
		}
		
		for (UrlTableElement newURL : localElms) {
			
			for(String url:selectedList){
				int protocolEndIndex = url.indexOf("://");
				int portEndIdex = url.lastIndexOf(":");
				int beginIndex = protocolEndIndex != -1 ? protocolEndIndex + 3 : 0;
				int endIndex = portEndIdex != -1 ? portEndIdex : url.length();
				String ipAddress = url.substring(beginIndex, endIndex);
				
				String protocol = protocolEndIndex != -1 ? url.substring(0, protocolEndIndex) : "http";
				String portNo = portEndIdex != -1 ? url.substring(portEndIdex+1) : "https";
				if (ipAddress.equals(newURL.getIpAddress())) {
					newURL.selected = true;
					if ("http".equals(protocol)) {
						newURL.httpPort = portNo;
					} else {
						newURL.httpsPort = portNo;
					}
				}
			} 
		
		}
	}
	
	
	@Override
	protected void okPressed() {
		updateConfiguredList();
		super.okPressed();
	}

	@Override
	public void setErrorMessage(String message) {
		Button okButton = getButton(IDialogConstants.OK_ID);
        if (okButton != null){
            okButton.setEnabled(message == null);
        }
        super.setErrorMessage(message);
	} 
	
	@Override
	public void setMessage(String message) {
		Button okButton = getButton(IDialogConstants.OK_ID);
        if (okButton != null){
            okButton.setEnabled(true);
        }
        super.setMessage(message);
	}
	
	private boolean validate(List<UrlTableElement> elems){
		boolean isEnable = true;
		for (UrlTableElement url : elems) {
			if(url.selected && (isEmpty(IP_ADDRESS_PROP, url.ipAddress) || (isEmpty(IP_HTTP_PORT_PROP, url.httpPort) && isEmpty(IP_HTTPS_PROP, url.httpsPort)))){
				setErrorMessage("IP Address or Port is not configured.");
				isEnable = false;
				//return false;
			}
		}
		if(isEnable){
			setErrorMessage(null);
			setMessage(MESSAGE);
			//return true;
		}
		return isEnable;
	}
	
	private boolean isEmpty(String prop,String value){
		if(prop.equals(IP_HTTPS_PROP)){
			if(NA_PORT.equals(value)){
				value = "";
			}
		}
		if(value == null || "".equals(value.trim())){
			return true;
		}
		
		return false;
	}
	
	private void updateConfiguredList() {
        configList.clear();
        for (UrlTableElement configElement : configElms) {
			configList.add(configElement.toUrl());
		}
	}

	private TableLayout createLayout() {
		TableLayout tablelayout = new TableLayout();
		tablelayout.addColumnData(new ColumnWeightData(10, 0, true));
		tablelayout.addColumnData(new ColumnWeightData(30, 0, true));
		tablelayout.addColumnData(new ColumnWeightData(30, 0, true));
		tablelayout.addColumnData(new ColumnWeightData(30, 0, true));
		return tablelayout;
	}
	
	
	private void addColumns() {
		TableColumn nameColumn = new TableColumn(table, SWT.LEFT);
		nameColumn.setText("Active");

		TableColumn typeColumn = new TableColumn(table, SWT.LEFT);
		typeColumn.setText("IP Address");
		
		TableColumn httpPortColumn = new TableColumn(table, SWT.LEFT);
		httpPortColumn.setText("Http port");
		
		TableColumn httpsPortColumn = new TableColumn(table, SWT.LEFT);
		httpsPortColumn.setText("Https port");
	}
	
	private void setContentProvider(TableViewer viewer) {
		viewer.setContentProvider(new ArrayContentProvider());
	}
	
	private void setLabelProvider(TableViewer viewer) {
		viewer.setLabelProvider(new TableLabelProvider(){

			@Override
			public Image getColumnImage(Object element, int columnIndex) {
				if (element instanceof UrlTableElement) {
					UrlTableElement localSystemURL = (UrlTableElement) element;
					switch (columnIndex) {
					case 0:
						return localSystemURL.isSelected() ? ImageDescriptorRegistryUtils
								.getImage(ImageDescriptorConstants.checkedImage) : ImageDescriptorRegistryUtils
								.getImage(ImageDescriptorConstants.uncheckedImage);
					}
				}
				return null;
			}

			@Override
			public String getColumnText(Object element, int columnIndex) {
				if(element instanceof UrlTableElement){
					UrlTableElement localSystemURL = (UrlTableElement) element;
					switch(columnIndex){
					case 1: 
						return localSystemURL.getIpAddress();
					case 2:
						return localSystemURL.getHttpPort();
					case 3:
						return localSystemURL.getHttpsPort();
					}
				}
				return "";
			}
		});
	}
	
	private void attachCellEditors(TableViewer viewer, Table table, final boolean isIpEditable) {
		viewer.setCellEditors(new CellEditor[]{
				new CheckboxCellEditor(table),
				new ServerIpCellEditor(this, table, viewer, IP_ADDRESS_PROP),
				new ServerIpCellEditor(this, table, viewer, IP_HTTP_PORT_PROP),
				new ServerIpCellEditor(this, table, viewer, IP_HTTPS_PROP)
		});			

		viewer.setCellModifier(new ICellModifier(){
			@Override
			public boolean canModify(Object element, String property) {
				if(IP_ADDRESS_PROP.equals(property)) return isIpEditable;
				return true;
			}

			@Override
			public Object getValue(Object element, String property) {
				if(element instanceof UrlTableElement){
					UrlTableElement localSystemURL = (UrlTableElement) element;
					if(IP_SELECTED_PROP.equals(property)){
						return localSystemURL.isSelected();
					} else if(IP_ADDRESS_PROP.equals(property)){
						return localSystemURL.getIpAddress();
					} else if(IP_HTTP_PORT_PROP.equals(property)){
						return localSystemURL.getHttpPort();
					} else if(IP_HTTPS_PROP.equals(property)){
						return localSystemURL.getHttpsPort();
					}
				} 
				return null;
			}

			@Override
			public void modify(Object element, String property, Object value) {
				TableItem tableItem = (TableItem)element;
				if(tableItem.getData() instanceof UrlTableElement){
					UrlTableElement dataItem = (UrlTableElement)tableItem.getData();	
					
					if(IP_SELECTED_PROP.equals(property) && value instanceof Boolean){
						dataItem.selected = (Boolean) value;
					} else if (IP_ADDRESS_PROP.equals(property)) {
						dataItem.ipAddress = (String) value;
					} else if(IP_HTTP_PORT_PROP.equals(property)){
						dataItem.httpPort = (String) value;
					} else if(IP_HTTPS_PROP.equals(property)){
					    try {
							Integer.parseInt((String) value);
						} catch (NumberFormatException e) {
							value = NA_PORT;
						}
						dataItem.httpsPort = (String) value;
					}
					validate(configElms);
				}  
			}

		});

		viewer.setColumnProperties(new String[] {
				IP_SELECTED_PROP,
				IP_ADDRESS_PROP,
				IP_HTTP_PORT_PROP,
				IP_HTTPS_PROP,
		});		
	}
	
	public List<UrlTableElement> getUrls() {
		List<UrlTableElement> urls = new ArrayList<UrlTableElement>();
		urls.addAll(localElms);
		urls.addAll(configElms);
		return urls;
	}
	
	public static class UrlTableElement {
		private static final String PROTOCOL_HTTPS = "https";
		private static final String PROTOCOL_HTTP = "http";
		private static final String DEFAULT_PORT = "8080";
		private String ipAddress = "";
		private String httpsPort = "NA";
		private String httpPort = DEFAULT_PORT;
		private boolean selected = false;
		
		public UrlTableElement() {
			
		}
		
		public UrlTableElement(String url) {
			int protocolEndIndex = url.indexOf("://");
			int portEndIdex = url.lastIndexOf(":");
			int beginIndex = protocolEndIndex != -1 ? protocolEndIndex + 3 : 0;
			int endIndex = portEndIdex != -1 ? portEndIdex : url.length();
			this.ipAddress = url.substring(beginIndex, endIndex);
			
			String protocol = protocolEndIndex != -1 ? url.substring(0, protocolEndIndex) : PROTOCOL_HTTP;
			String portNo = portEndIdex != -1 ? url.substring(portEndIdex+1) : DEFAULT_PORT;
			if (PROTOCOL_HTTP.equals(protocol)) {
				this.httpPort = portNo;
			} else {
				this.httpsPort = portNo;
			}
		}
		
		public UrlTableElement(String ipAddress, String httpPort, String httpsPort) {
			this.ipAddress = ipAddress;
			if (httpPort != null) {
				this.httpPort = httpPort;
			}
			if (httpsPort != null) {
				this.httpsPort = httpsPort;
			}
		}
		
		public UrlTableElement(boolean selected, String ipAddress, String httpPort, String httpsPort) {
			this.selected = selected;
			this.ipAddress = ipAddress;
			if (httpPort != null) {
				this.httpPort = httpPort;
			}
			if (httpsPort != null) {
				this.httpsPort = httpsPort;
			}
		}
		
		public String validate() {
			if(httpPort == null || httpPort.trim().length() == 0) {
				return "http port can't be empty";
			} else {
				for(int index = 0; index < httpPort.length(); index++) {
					char charAt = httpPort.charAt(index);
					if(!Character.isDigit(charAt)) {
						return "http port is not valid.";
					}
				}
			}
			return "";
		}

		public String getIpAddress() {
			return ipAddress;
		}
		
		public String getHttpPort() {
			return httpPort;
		}
		
		public String getHttpsPort() {
			return httpsPort;
		}
		
		public boolean isSelected() {
			return selected;
		}
		
		public String toUrl() {
			String protocol = PROTOCOL_HTTP;
			String port = getHttpPort();
			String httpsPort = getHttpsPort();
			if (ValidationUtil.isNonEmptyString(httpsPort) && !ConfigureServerIpsDialog.NA_PORT.equals(httpsPort)) {
				protocol = PROTOCOL_HTTPS;
				port = httpsPort;
			}
			String url = protocol + "://" + getIpAddress() + ":" + port;
			return url;
		}
	}

	class ServerIpCellEditor extends CustomTextCellEditor{
		private final ConfigureServerIpsDialog dialog;
		private final TableViewer viewer;
		private final String  prop;
		
		public ServerIpCellEditor(ConfigureServerIpsDialog dialog, Composite parent, TableViewer viewer, String prop) {
			super(parent);
			this.dialog = dialog;
			this.viewer = viewer;
			this.prop = prop;
		}
		@Override
		protected void editOccured(ModifyEvent e) {
			List<UrlTableElement> extServers = new ArrayList<ConfigureServerIpsDialog.UrlTableElement>(configElms);
			String value = text.getText();
	        if (value == null) {
				value = "";
			}
	        boolean isSelected = false;
	        UrlTableElement elem = null;
	        
	        UrlTableElement selElem = null;
	        IStructuredSelection sel = ((IStructuredSelection)viewer.getSelection());
	        if(sel != null){
	        	if(sel.getFirstElement() instanceof UrlTableElement){
	        		selElem = (UrlTableElement)sel.getFirstElement();
					isSelected = selElem != null ? selElem.selected : false;
				}
	        }
	        int selIndex = viewer.getTable().getSelectionIndex();
	        if(selIndex != -1){
	        	extServers.remove(selIndex);
	        	if(prop == IP_ADDRESS_PROP){
					elem = new UrlTableElement(isSelected, value, selElem.httpPort, selElem.httpsPort);
				}
				else if (prop == IP_HTTP_PORT_PROP){
					elem = new UrlTableElement(isSelected, selElem.ipAddress, value, selElem.httpsPort);
				}else if(prop == IP_HTTPS_PROP){
					elem = new UrlTableElement(isSelected, selElem.ipAddress, selElem.httpPort, value);
				}
	        	extServers.add(elem);
	        }
	        dialog.validate(extServers);
		}
	}
	
	public static Set<String> getLocalIps() {
		Set<String> localIps = new LinkedHashSet<String>();
		try {
			/*
			 * InetAddress inetAddr = InetAddress.getLocalHost(); populateIps(localIps, inetAddr); */
			Enumeration<NetworkInterface> enumNI = NetworkInterface.getNetworkInterfaces();
			while (enumNI.hasMoreElements()) {
				NetworkInterface ifc = enumNI.nextElement();
				if (ifc.isUp()) {
					Enumeration<InetAddress> enumAdds = ifc.getInetAddresses();
					while (enumAdds.hasMoreElements()) {
						InetAddress inetAddr = enumAdds.nextElement();
						populateIps(localIps, inetAddr);
					}
				}
			}
		} catch (UnknownHostException e) {
			ConsoleDisplayManager.getDefault().printException(e, ConsoleDisplayManager.MSG_ERROR);
		} catch (SocketException e) {
			ConsoleDisplayManager.getDefault().printException(e, ConsoleDisplayManager.MSG_ERROR);
		}
		return localIps;
	}

	private static void populateIps(Set<String> localIps, InetAddress inetAddr) throws UnknownHostException {
		Class<? extends ConfigureServerIpsDialog> clazz = ConfigureServerIpsDialog.class;
		String localAddr = inetAddr.getHostAddress();
		ConsoleDisplayManager.logDebugInfo("---------Fetching multiple ip addresses for: " + localAddr + "------------", clazz);
		if (inetAddr instanceof Inet4Address) {
			ConsoleDisplayManager.logDebugInfo("Main Ipv4 address: " + localAddr, clazz);
			localIps.add(localAddr);
		} else {
			ConsoleDisplayManager.logDebugInfo("Main address not an ipv4 address: " + localAddr, clazz);
		}
		
		// Just in case this host has multiple IP addresses....
		InetAddress[] allMyIps = InetAddress.getAllByName(inetAddr.getCanonicalHostName());
		if (allMyIps != null && allMyIps.length > 1) {
			for (int i = 0; i < allMyIps.length; i++) {
				InetAddress multiInetAddr = allMyIps[i];
				String addr = multiInetAddr.getHostAddress();
				if (multiInetAddr instanceof Inet4Address) {
					ConsoleDisplayManager.logDebugInfo("Ipv4 address: " + addr, clazz);
					localIps.add(addr);
				} else {
					ConsoleDisplayManager.logDebugInfo("Not an ipv4 address: " + addr, clazz);
				}
			}
		} else {
			ConsoleDisplayManager.logDebugInfo("Unable to find multiple ip addresses for: " + localAddr, clazz);
		}
	}
	
	public static void main(String[] args) {
		try {
			  InetAddress localhost = InetAddress.getLocalHost();
			  System.out.println(" IP Addr: " + localhost.getHostAddress());
			  // Just in case this host has multiple IP addresses....
			  InetAddress[] allMyIps = InetAddress.getAllByName(localhost.getCanonicalHostName());
			  if (allMyIps != null && allMyIps.length > 1) {
			     System.out.println(" Full list of IP addresses:");
			    for (int i = 0; i < allMyIps.length; i++) {
			       InetAddress inetAddress = allMyIps[i];
			       if (inetAddress instanceof Inet4Address) {
			    	   System.out.println("    " + inetAddress);
			       } else  if (inetAddress instanceof Inet6Address) {
			    	   System.out.println(" Inet 6----------->   " + inetAddress);
			       }
			    }
			  }
			} catch (UnknownHostException e) {
			   System.out.println(" (error retrieving server host name)");
			}
			
	}
	
}
