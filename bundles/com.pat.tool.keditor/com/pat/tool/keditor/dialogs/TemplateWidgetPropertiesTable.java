package	com.pat.tool.keditor.dialogs;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnWeightData;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.IFontProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

import com.pat.tool.keditor.difftool.FillerWidgetNode;
import com.pat.tool.keditor.editors.help.ContextSensitiveHelpConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.model.KComponent;
import com.pat.tool.keditor.model.KContainer;
import com.pat.tool.keditor.model.KHBox;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.model.KWidget;
import com.pat.tool.keditor.propertyDescriptor.TableLabelProvider;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.UIUtils;
import com.pat.tool.keditor.utils.model.datagrid.GridTemplateWidgetData;
import com.pat.tool.keditor.utils.model.datagrid.TemplateData;
import com.pat.tool.keditor.utils.model.datagrid.WidgetPropertyInfo;

/**
 * Using existing piece of code to modify the value of the 
 * model object for data grid widget.
 * 
 * @author Rakesh
 */

public class TemplateWidgetPropertiesTable extends Composite {

	private TreeViewer propTableViewer;
	
	private Map<KPage, Map<KWidget, List<IPropertyDescriptor>>> modifiedData = new HashMap<KPage, Map<KWidget, List<IPropertyDescriptor>>>();
	protected Map<KPage, Map<KWidget, List<WidgetPropertyCategory>>> pagePropertiesMap = new HashMap<KPage, Map<KWidget, List<WidgetPropertyCategory>>>();
	protected Map<KPage, IFile> formFileMap = new HashMap<KPage, IFile>();
	private Map<Object, Object> compatibleObjects = new HashMap<Object, Object>();
    private Map<KWidget, FillerWidgetNode> fillerCompatibilityMap = new HashMap<KWidget, FillerWidgetNode>();
	private Map<KPage, Map<KWidget, Map<String, WidgetPropertyDescriptor>>> pagePropertyIdsMap = new HashMap<KPage, Map<KWidget, Map<String, WidgetPropertyDescriptor>>>();
	private Map<File, IResource> fileResourceMap = new HashMap<File, IResource>();

	private Text categoryFilterText;
	private Map<Object, Object[]> expandedElementsMap = new HashMap<Object, Object[]>();
	protected IFormModifyListener formModifyListener;
	protected TableLayout tableLayout;
	private static Set<String> unmodifiableEntries = new HashSet<String>();
	protected static final int TABLE_WIDTH = 700;
	private boolean popupMode;
	
	static {
		unmodifiableEntries.add(KWidget.ID_PROP);
		unmodifiableEntries.add(KWidget.SIZE_PROP);
		unmodifiableEntries.add(KWidget.SIZE_PERCENT_PROP);
		unmodifiableEntries.add(KWidget.LOCATION_PROP);
		unmodifiableEntries.add(KComponent.OVERRIDE_PROP);
		unmodifiableEntries.add(KHBox.USEPERCENT_PROP);
	}
	

	public TemplateWidgetPropertiesTable(Composite parent, int style) {
		this(parent, style, false);
	}
	
	public TemplateWidgetPropertiesTable(Composite parent, int style, boolean popupMode) {
		super(parent, style);
		this.popupMode = popupMode;
		init();
	}

	public void init() {
		GridLayout layout = new GridLayout();
		KUtils.makeMarginsTo(layout, 0);
		KUtils.makeSpacingTo(layout, 0);
		setLayout(layout);
		createViewerSection();
	}
	
	public void update(File leftFile) throws Exception {
		/*if (leftFile != null && leftFile.exists() && !leftFile.equals(this.leftFile)) {
			this.leftFile = leftFile;
			fileText.setText(leftFile.getAbsolutePath());
		}*/
	}

	/*private void addFileLocationListener(Button button, final Text text) {
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				GridTemplateSelectionDialog dialog = new GridTemplateSelectionDialog(null,KPage.GRID_MODE);
				if (dialog.open() == IDialogConstants.OK_ID) {
					IFile formFile = (IFile) dialog.getFirstResult();
					File nativeFile = new File(formFile.getLocationURI());
					String filePath = nativeFile.getPath();
					text.setText(filePath);
					text.setToolTipText(filePath);
					fileResourceMap.put(nativeFile, formFile);
					try {
						update(new File(fileText.getText()));
					} catch (Exception exc) {
						ExceptionHandler.logAndShowException(exc);
					}
				}
			}
		});
	}*/
	
	private void createViewerSection() {
		Composite treeComposite = new Composite(this, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(treeComposite);
		GridLayoutFactory.fillDefaults().margins(5, 5).numColumns(2).applyTo(treeComposite);

	/*	ViewForm viewForm = new ViewForm(treeComposite, SWT.FLAT | SWT.BORDER);
		ToolBar toolBar = new ToolBar(viewForm, SWT.FLAT);
		toolBar.setBackground(treeComposite.getBackground());
		ToolBarManager toolBarManager = new ToolBarManager(toolBar);
		viewForm.setTopRight(toolBar);
		GridDataFactory.fillDefaults().grab(true, true).span(2,1).applyTo(viewForm);
		Composite viewFormContents = new Composite(viewForm, SWT.FLAT);
		GridLayoutFactory.fillDefaults().numColumns(2).applyTo(viewFormContents);
		viewForm.setContent(viewFormContents);
		
		Action expandAllAction = new Action("Expand All") {
			@Override
			public void run() {
				propTableViewer.expandAll();
			};
		};
		expandAllAction.setImageDescriptor(ImageDescriptorRegistryUtils.getImageDescriptor(ImageDescriptorConstants.expandAllImage));
		Action collapseAllAction = new Action("Collapse All") {
			@Override
			public void run() {
				propTableViewer.collapseAll();
			};
		};*/
		
		propTableViewer = createTableViewer(treeComposite);
		/*collapseAllAction.setImageDescriptor(ImageDescriptorRegistryUtils.getImageDescriptor(ImageDescriptorConstants.collapseAllImage));
		List<Action> toolBarActions = getToolBarActions();
		if (toolBarActions != null) {
			for (Action action : toolBarActions) {
				toolBarManager.add(action);
			}
			toolBarManager.add(new Separator());
		}
		
		toolBarManager.add(expandAllAction);
		toolBarManager.add(collapseAllAction);
		toolBarManager.update(true);*/

		tableLayout = new TableLayout();
		tableLayout.addColumnData(new ColumnWeightData(50, true));
		tableLayout.addColumnData(new ColumnWeightData(50, true));
		propTableViewer.getTree().setLayout(tableLayout);

		TreeViewerColumn col1 = new TreeViewerColumn(propTableViewer, SWT.NONE);
		col1.getColumn().setText("Name");
		col1.getColumn().setWidth(100);

		TreeViewerColumn col2 = new TreeViewerColumn(propTableViewer, SWT.NONE);
		col2.getColumn().setText(getValueColumnName());
		col2.getColumn().setWidth(200);
		col2.setEditingSupport(getEditingSupport(null));
		
		propTableViewer.setContentProvider(getContentProvider());
		propTableViewer.setLabelProvider(new WidgetPropertiesTableLabelProvider());
		
		
        
		//UIUtils.hookExpandActions(propTableViewer, getActions());
		UIUtils.addDoubleClickExpandListener(propTableViewer);
		
		/*propTableViewer.addHelpListener(new HelpListener() {
			@Override
			public void helpRequested(HelpEvent e) {
				
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						ISelection selection = propTableViewer.getSelection();
						String contextId = getHelpContextId();
						if (selection instanceof StructuredSelection) {
							Object firstElement = ((StructuredSelection) selection).getFirstElement();
							if (firstElement instanceof WidgetPropertyDescriptor) {
								IPropertyDescriptor descriptor = ((WidgetPropertyDescriptor) firstElement).getPropertyDescriptor();
							    contextId = descriptor.getHelpContextIds().toString();
							} 
						} 
						ContextSensitiveHelpUtil.displayHelp(contextId);
					}
				});
			}
		});*/
	}

	protected String getHelpContextId() {
		return ContextSensitiveHelpConstants.EDIT_FORM_PROPERTIES;
	}
	
	protected String getValueColumnName() {
		return "Value";
	}
	
	protected void updateAdditionalColumns() {
		
	}
	
	protected String getAdditionalColumnText(Object element, int index) {
		return null;
	}

	protected List<Action> getActions() {
		Action applyAllAction = new Action("Apply All") {
			@Override
			public void run() {
				appyToAll();
			}

			@Override
			public boolean isEnabled() {
				Object element = ((IStructuredSelection) propTableViewer.getSelection()).getFirstElement();
				return element instanceof WidgetPropertyDescriptor;
			}

		};
		return Arrays.asList(applyAllAction);
	}
	
	protected List<Action> getToolBarActions() {
		return null;
	}

	public TreeViewer createTableViewer(Composite treeComposite) {
		int style = SWT.FULL_SELECTION | SWT.BORDER | SWT.HIDE_SELECTION | SWT.SINGLE;
		TreeViewer propTableViewer = new TreeViewer(treeComposite, style);
			/*@Override
			public void refresh() {
				super.refresh();
				if (popupMode && widgetFilterText.getText().equals(widgetFilter)) {
					expandAll();
				}
			}*/
		//};
		GridDataFactory.fillDefaults().grab(true, true).span(2, 1).applyTo(propTableViewer.getTree());
		propTableViewer.getTree().setHeaderVisible(true);
		propTableViewer.getTree().setLinesVisible(true);
		return propTableViewer;
	}
	
	private void appyToAll() {
		propTableViewer.expandAll();
		WidgetPropertyDescriptor source = (WidgetPropertyDescriptor) ((IStructuredSelection) propTableViewer
				.getSelection()).getFirstElement();
		Object sourceValue = getPropertyValue(source);
		Queue<TreeItem> treeItems = new LinkedList<TreeItem>(Arrays.asList(propTableViewer.getTree().getItems()));
		while (treeItems.peek() != null) {
			TreeItem treeItem = treeItems.poll();
			Object data = treeItem.getData();
			if (data instanceof WidgetPropertyDescriptor && data != source) {
				WidgetPropertyDescriptor target = (WidgetPropertyDescriptor) data;
				boolean isSimpleProp = sourceValue instanceof String || sourceValue instanceof Integer || sourceValue instanceof Boolean;
				KPage sourcePage = source.getWidget().getRoot();
				KPage targetPage = target.getWidget().getRoot();
				if (isSimpleProp || (targetPage.platform == sourcePage.platform && targetPage.getFormCategory() == sourcePage.getFormCategory())) {
					if (target.getPropertyDescriptor().getId() == source.getPropertyDescriptor().getId()) {
						checkAndSetPropertyValue(sourceValue, target);
					}
				}
			}
			treeItems.addAll(Arrays.asList(treeItem.getItems()));
		}
		propTableViewer.refresh();
	}

	private ModifyListener refreshListener = new ModifyListener() {
		
		@Override
		public void modifyText(ModifyEvent e) {
			propTableViewer.refresh();
		}
	};

	public ITreeContentProvider getContentProvider() {
		return new ITreeContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}

			@Override
			public void dispose() {
			}

			@Override
			public boolean hasChildren(Object element) {
				return getChildren(element) != null && getChildren(element).length > 0;
			}

			@Override
			public Object getParent(Object element) {
				if (element instanceof IPropertyDescriptor) {
					return ((IPropertyDescriptor) element).getId();
				}
				return null;
			}

			@Override
			public Object[] getElements(Object inputElement) {
				List<Object> widgets = new ArrayList<Object>();
				if(inputElement instanceof TemplateData){
					TemplateData data = (TemplateData) inputElement;
					return data.getWidgetDataList().toArray();
				} else if(inputElement instanceof GridTemplateWidgetData){
					GridTemplateWidgetData gTemplateWidgetData = (GridTemplateWidgetData) inputElement;
					gTemplateWidgetData.getWidgetProperties().toArray();
				}/* else if(inputElement instanceof WidgetPropertyInfo){
					WidgetPropertyInfo widgetPropertyInfo = (WidgetPropertyInfo) inputElement;
					gTemplateWidgetData.getWidgetProperties().toArray();
				}*/
				/*if (inputElement instanceof List) {
					for (KPage kPage : (List<KPage>) inputElement) {
						widgets.addAll(kPage.getForm().getChildren());
					}
				}*/
				return widgets.toArray();
			}

			private WidgetPropertyDescriptor getMatchingDescriptor(KPage targetPage, WidgetPropertyDescriptor sourceDescriptor) {
				KWidget sourceWidget = sourceDescriptor.getWidget();
				KWidget targetWidget = KPage.getCompatibleWidget(targetPage, sourceWidget);
				if (targetWidget != null) {
					// ensure that property categories are initialized
					getPropertyCategories(targetWidget);
					Map<KWidget, Map<String, WidgetPropertyDescriptor>> widgetIdMap = pagePropertyIdsMap.get(targetPage);
					Map<String, WidgetPropertyDescriptor> propertyMap = widgetIdMap.get(targetWidget);
					return propertyMap.get(sourceDescriptor.getPropertyDescriptor().getId());
				}
				return null;
			}
			
			@SuppressWarnings("unchecked")
			@Override
			public Object[] getChildren(Object parentElement) {
				List<Object> children = new ArrayList<Object>();
				if (parentElement instanceof KWidget) {
					if (parentElement instanceof KContainer) {
						children.addAll(((KContainer) parentElement).getChildren());
					}
					children.addAll(getPropertyCategories((KWidget) parentElement));
				} else if (parentElement instanceof WidgetPropertyCategory) {
					return ((WidgetPropertyCategory) parentElement).getPropertyDescriptors().toArray();
				}
				return children.toArray();
			}
		};
	}

	protected EditingSupport getEditingSupport(final IMobileChannelCellEditorProvider cellEditorProvider) {
		return new EditingSupport(propTableViewer) {

			@Override
			protected void setValue(Object element, Object value) {
				if (element instanceof WidgetPropertyDescriptor && !isReadOnlyMode()) {
					WidgetPropertyDescriptor widgetPropertyDescriptor = (WidgetPropertyDescriptor) element;
					checkAndSetPropertyValue(value, widgetPropertyDescriptor);
					propTableViewer.refresh();
				}
			}

			@Override
			protected Object getValue(Object element) {
				if (cellEditorProvider != null) {
					return cellEditorProvider.getValue(element);
				} 
				WidgetPropertyDescriptor widgetPropertyDescriptor = (WidgetPropertyDescriptor) element;
				return getPropertyValue(widgetPropertyDescriptor);
			}

			@Override
			protected CellEditor getCellEditor(Object element) {
				if (cellEditorProvider != null) {
					return cellEditorProvider.getCellEditor(element);
				} else if (element instanceof WidgetPropertyDescriptor) {
					IPropertyDescriptor descriptor = ((WidgetPropertyDescriptor) element).getPropertyDescriptor();
					return descriptor.createPropertyEditor(propTableViewer.getTree());
				}
				return null;
			}

			@Override
			protected boolean canEdit(Object element) {
				return getCellEditor(element) != null;
			}
		};
	}
	
	protected boolean isReadOnlyMode() {
		return false;
	}
	
	protected static interface IMobileChannelCellEditorProvider {
		CellEditor getCellEditor(Object element);

		Object getValue(Object element);
	}
	
	public static Object getPropertyValue(WidgetPropertyDescriptor widgetPropertyDescriptor) {
		KWidget kWidget = widgetPropertyDescriptor.getWidget();
		IPropertyDescriptor descriptor = widgetPropertyDescriptor.getPropertyDescriptor();
		Object propertyValue = kWidget.getEditablePropertyValue(descriptor.getId());
		return propertyValue;
	}
	
	private void checkAndSetPropertyValue(Object value, WidgetPropertyDescriptor widgetPropertyDescriptor) {
		if (value == null || KWidget.areEqual(value, getPropertyValue(widgetPropertyDescriptor))) {
			return;
		}
		setPropertyValue(value, widgetPropertyDescriptor);
		// updates the modified data
		KWidget kWidget = widgetPropertyDescriptor.getWidget();
		IPropertyDescriptor descriptor = widgetPropertyDescriptor.getPropertyDescriptor();
		kWidget.setPropertyValue(descriptor.getId(), value);
		widgetPropertyDescriptor.getWidgetPropertyCategory().setModified(true);
		KPage page = kWidget.getRoot();
		Map<KWidget, List<IPropertyDescriptor>> modifiedWidgetsMap = modifiedData.get(page);
		if (modifiedWidgetsMap == null) {
			modifiedWidgetsMap = new HashMap<KWidget, List<IPropertyDescriptor>>();
			modifiedData.put(page, modifiedWidgetsMap);
			if (formModifyListener != null) {
				formModifyListener.formModified();
			}
		}
		List<IPropertyDescriptor> modifiedDescs = modifiedWidgetsMap.get(kWidget);
		if (modifiedDescs == null) {
			modifiedDescs = new ArrayList<IPropertyDescriptor>();
			modifiedWidgetsMap.put(kWidget, modifiedDescs);
		}
		modifiedDescs.add(descriptor);
	}

	protected void setPropertyValue(Object value, WidgetPropertyDescriptor widgetPropertyDescriptor) {
		KWidget kWidget = widgetPropertyDescriptor.getWidget();
		IPropertyDescriptor descriptor = widgetPropertyDescriptor.getPropertyDescriptor();
		kWidget.setPropertyValue(descriptor.getId(), value);
	}

	/*private ViewerFilter viewerFilter = new ViewerFilter() {
		@Override
		public boolean select(Viewer viewer, Object parentElement, Object element) {
			return isTopDownMatch(element);
		}

		private boolean isTopDownMatch(Object element) {
			
			if (element instanceof WidgetPropertyCategory || isMatch(element)) {
				Object[] children = ((ITreeContentProvider) propTableViewer.getContentProvider()).getChildren(element);
				if (children != null) {
					for (Object child : children) {
						if (isTopDownMatch(child)) {
							return true;
						}
					}
				}

				if (element instanceof WidgetPropertyDescriptor) {
					WidgetPropertyDescriptor propertyDescriptor = (WidgetPropertyDescriptor) element;
					return isMatch(propertyDescriptor.getWidget()) && isMatch(propertyDescriptor.getWidgetPropertyCategory());
				}

				return false;
			}

			return false;
		}

		private boolean isMatch(Object element) {
			SearchPattern searchPattern = new SearchPattern();
			if (completeMatchButton.getSelection()) {
				searchPattern = new SearchPattern(SearchPattern.RULE_EXACT_MATCH);
			}
			String activeFilter = propertyFilterText.getText().toLowerCase();
			if (element instanceof KWidget) {
				activeFilter = widgetFilterText.getText();
				String id = ((KWidget) element).getID();
				searchPattern.setPattern(activeFilter);
				if ((completeMatchButton.getSelection() && activeFilter.isEmpty()) || searchPattern.matches(id)) {
					return true;
				}
			} else if (element instanceof WidgetPropertyCategory) {
				activeFilter = categoryFilterText.getText();
			}

			String text = ((ILabelProvider) propTableViewer.getLabelProvider()).getText(element);
			searchPattern.setPattern(activeFilter);
			return text != null && ((completeMatchButton.getSelection() && activeFilter.isEmpty()) || searchPattern.matches(text));
		}
	};*/
	
	
	private String widgetFilter;

	private class WidgetPropertiesTableLabelProvider extends TableLabelProvider implements IFontProvider, IColorProvider {

		@Override
		public String getColumnText(Object element, int columnIndex) {
			
			switch(columnIndex){
			case 0:
				if(element instanceof GridTemplateWidgetData){
					GridTemplateWidgetData gWidgetData = (GridTemplateWidgetData) element;
					return gWidgetData.getWidgetName();
				}
				if(element instanceof WidgetPropertyInfo){
					WidgetPropertyInfo wPropertyInfo = (WidgetPropertyInfo) element;
					return wPropertyInfo.getPropertyName();
				}
			}
			/*updateCheckedState(element);
			if (element instanceof WidgetPropertyCategory)
				switch (columnIndex) {
				case 0:
					return ((WidgetPropertyCategory) element).getCategory();
				default:
					return getAdditionalColumnText(element, columnIndex);
				}
			else if (element instanceof WidgetPropertyDescriptor) {
				WidgetPropertyDescriptor widgetPropertyDescriptor = (WidgetPropertyDescriptor) element;
				IPropertyDescriptor descriptor = widgetPropertyDescriptor.getPropertyDescriptor();
				switch (columnIndex) {
				case 0:
					return descriptor.getDisplayName();
				case 1:
					return getDisplayValue(widgetPropertyDescriptor);
				default:
					return getAdditionalColumnText(element, columnIndex);
				}
			} else if (element instanceof KWidget) {
				KWidget kWidget = (KWidget) element;
				switch (columnIndex) {
				case 0:
					String formCategory = "";
					if (kWidget.getRoot().getFormCategory() == KPage.TABLET_CAT) {
						formCategory = " " + LabelUtils.TABLET_GROUP;
					} else if (kWidget.getRoot().getFormCategory() == KPage.DESKTOPKIOSK_CAT) {
						formCategory = " " + LabelUtils.DESKTOP_GROUP;
					}
					return kWidget.getDisplayName() + formCategory;
				case 1:
					if (element instanceof KForm) {
						return KUtils.getFormName(kWidget.getRoot());
					}
					Object input = propTableViewer.getInput();
					if (input instanceof List) {
						if (((List<?>) input).size() > 1) {
							return kWidget.getID() + " (" + KUtils.getFormName(kWidget.getRoot()) + ")";
						}
					}
					return kWidget.getID();
				default:
					return getAdditionalColumnText(element, columnIndex);
				}
			}*/
			return super.getColumnText(element, columnIndex);
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			if (element instanceof WidgetPropertyDescriptor) {
				WidgetPropertyDescriptor widgetPropertyDescriptor = (WidgetPropertyDescriptor) element;
				KWidget kWidget = widgetPropertyDescriptor.getWidget();
				IPropertyDescriptor descriptor = widgetPropertyDescriptor.getPropertyDescriptor();
				switch (columnIndex) {
				case 1:
					if (descriptor.getLabelProvider() != null) {
						return descriptor.getLabelProvider().getImage(kWidget.getPropertyValue(descriptor.getId()));
					}
				default:
					break;
				}
			} 
			if (element instanceof KWidget) {
				String displayName = ((KWidget) element).getDisplayName();
				switch (columnIndex) {
				case 0:
					return  ImageDescriptorRegistryUtils.getImage(displayName);
				}
			} else if (element instanceof WidgetPropertyCategory) {
				switch (columnIndex) {
				case 0:
					return  PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_FOLDER);
				}
			}
			return null;
		}

		@Override
		public Font getFont(Object element) {
			return TemplateWidgetPropertiesTable.this.getFont(element);
		}


		@Override
		public Color getForeground(Object element) {
			return getColor(element);
		}

		@Override
		public Color getBackground(Object element) {
			return getBackgroundColor(element);
		}
		
		@Override
		public String getText(Object element) {
			return getColumnText(element, 0);
		}
	}
	
	public static String getDisplayValue(WidgetPropertyDescriptor widgetPropertyDescriptor) {
		Object propValue = getPropertyValue(widgetPropertyDescriptor);
		IPropertyDescriptor descriptor = widgetPropertyDescriptor.getPropertyDescriptor();
		if (descriptor.getLabelProvider() != null) {
			return descriptor.getLabelProvider().getText(propValue);
		}
		return propValue != null ? propValue.toString() : "";
	}

	protected Color getColor(Object element) {
		if (element instanceof WidgetPropertyDescriptor) {
			WidgetPropertyDescriptor widgetDesc = (WidgetPropertyDescriptor) element;
			if (isPropertyModified(widgetDesc)) {
				return Display.getDefault().getSystemColor(SWT.COLOR_BLUE);
			}
		}
		return null;
	}
	
	
	protected Color getBackgroundColor(Object element) {
		return null;
	}
	
	protected Font getFont(Object element) {
		boolean isModified = false;
		if (element instanceof KWidget) {
			KPage root = ((KWidget) element).getRoot();
			Map<KWidget, List<IPropertyDescriptor>> widgetMap = modifiedData.get(root);
			isModified = widgetMap != null && widgetMap.get(element) != null;
		} else if (element instanceof WidgetPropertyCategory) {
			isModified = ((WidgetPropertyCategory) element).isModified();
		} else if (element instanceof WidgetPropertyDescriptor) {
			WidgetPropertyDescriptor widgetDesc = (WidgetPropertyDescriptor) element;
			isModified = isPropertyModified(widgetDesc);
		}
		return isModified ? JFaceResources.getFontRegistry().getBold(JFaceResources.DEFAULT_FONT) : null;
	}
	
	private boolean isPropertyModified(WidgetPropertyDescriptor widgetDesc) {
		boolean isModified = false;
		KWidget kWidget = widgetDesc.getWidget();
		IPropertyDescriptor propertyDescriptor = widgetDesc.getPropertyDescriptor();
		Map<KWidget, List<IPropertyDescriptor>> modifiedWidgets = modifiedData.get(kWidget.getRoot());
		if (modifiedWidgets != null) {
			List<IPropertyDescriptor> modifiedDescs = modifiedWidgets.get(kWidget);
			if (modifiedDescs != null && modifiedDescs.contains(propertyDescriptor)) {
				isModified = true;
			}
		}
		return isModified;
	}
	
	public TreeViewer getViewer() {
		return propTableViewer;
	}

	protected void updateCheckedState(Object element) {
		
	}

	public static List<KWidget> getWidgets(KPage kPage) {
		List<KWidget> widgets = new ArrayList<KWidget>();
		if (kPage != null) {
			List<?> children = kPage.getChildren();
			for (Object object : children) {
				fillWidgets(widgets, (KContainer) object);
			}
		}
		return widgets;
	}

	private static void fillWidgets(List<KWidget> widgets, KContainer container) {
		widgets.add(container);
		List<?> children = container.getChildren();
		for (Object child : children) {
			if (child instanceof KContainer) {
				fillWidgets(widgets, (KContainer) child);
			} else if (child instanceof KComponent) {
				widgets.add((KWidget) child);
			}
		}
	}

	/**
	 * Filters a set of propertyDescriptors to return only the ones which are required 
	 * 
	 * @param widget
	 * @param propertyDescriptors
	 * @return
	 */
	 protected List<IPropertyDescriptor> filterPropertyDescriptors(KWidget widget, List<IPropertyDescriptor> propertyDescriptors) {
		List<IPropertyDescriptor> filteredDescriptors = new ArrayList<IPropertyDescriptor>();
		for (IPropertyDescriptor propertyDescriptor : propertyDescriptors) {
			if (isVisible(widget, propertyDescriptor)) {
				filteredDescriptors.add(propertyDescriptor);
			}
		}
		return filteredDescriptors;
	}

	protected boolean isVisible(KWidget widget, IPropertyDescriptor propertyDescriptor) {
		List<String> templateDescIds  = widget.getPortableGeneralProperties();
		return templateDescIds.contains(propertyDescriptor.getId());
	}

	/**
	 * Given a widget and category, this method returns the propertyDescriptors of the widget which belong to that category and which have been filtered by {@link TemplateWidgetPropertiesTable#filterPropertyDescriptors(KWidget, List)}
	 * 
	 * @param kWidget
	 * @return
	 */
	private List<WidgetPropertyCategory> getPropertyCategories(KWidget kWidget) {
		List<WidgetPropertyCategory> widgetPropertyCategories = null;
		KPage page = kWidget.getRoot();
		Map<KWidget, List<WidgetPropertyCategory>> widgetPropertiesMap = pagePropertiesMap.get(page);
		if (widgetPropertiesMap == null) {
			widgetPropertiesMap = new HashMap<KWidget, List<WidgetPropertyCategory>>();
			pagePropertiesMap.put(page, widgetPropertiesMap);
		}
		widgetPropertyCategories = widgetPropertiesMap.get(kWidget);
		if (widgetPropertyCategories == null) {
			List<IPropertyDescriptor> propertyDescriptors = filterPropertyDescriptors(kWidget, Arrays.asList(kWidget.getPropertyDescriptors()));
			widgetPropertyCategories = new ArrayList<WidgetPropertyCategory>(propertyDescriptors.size());
			widgetPropertiesMap.put(kWidget, widgetPropertyCategories);

			Map<String, WidgetPropertyCategory> map = new HashMap<String, WidgetPropertyCategory>();
			for (IPropertyDescriptor propertyDescriptor : propertyDescriptors) {
				String category = propertyDescriptor.getCategory();
				WidgetPropertyCategory widgetPropertyCategory = map.get(category);
				if (widgetPropertyCategory == null) {
					widgetPropertyCategory = new WidgetPropertyCategory(category, kWidget);
					map.put(category, widgetPropertyCategory);
					widgetPropertyCategories.add(widgetPropertyCategory);
				}
				widgetPropertyCategory.getPropertyDescriptors().add(new WidgetPropertyDescriptor(propertyDescriptor, kWidget, widgetPropertyCategory));
			}
		}

		return widgetPropertyCategories;
	}

	public void setInput(Object input) {
		expandedElementsMap.put(propTableViewer.getInput(), propTableViewer.getExpandedElements());
		/*if (widgetFilter != null) {
			widgetFilterText.setText(widgetFilter);
		}*/
		propTableViewer.setInput(input);
		propTableViewer.refresh();
		Object[] expandedElms = expandedElementsMap.get(input);
		if (expandedElms != null) {
			propTableViewer.setExpandedElements(expandedElms);
		}
	}
	

	public static class WidgetPropertyDescriptor {
		private IPropertyDescriptor propertyDescriptor;
		private KWidget widget;
		private WidgetPropertyCategory widgetPropertyCategory;

		public WidgetPropertyDescriptor(IPropertyDescriptor propertyDescriptor, KWidget widget, WidgetPropertyCategory widgetPropertyCategory) {
			this.propertyDescriptor = propertyDescriptor;
			this.widget = widget;
			this.widgetPropertyCategory = widgetPropertyCategory;
		}

		public IPropertyDescriptor getPropertyDescriptor() {
			return propertyDescriptor;
		}

		public KWidget getWidget() {
			return widget;
		}

		public WidgetPropertyCategory getWidgetPropertyCategory() {
			return widgetPropertyCategory;
		}

	}

	public static class WidgetPropertyCategory {
		private String category;
		private List<WidgetPropertyDescriptor> propertyDescriptors = new ArrayList<WidgetPropertyDescriptor>();
		private boolean isModified;
		private KWidget widget;

		public WidgetPropertyCategory(String category, KWidget widget) {
			this.category = category;
			this.widget = widget;
		}

		public String getCategory() {
			return category;
		}

		public List<WidgetPropertyDescriptor> getPropertyDescriptors() {
			return propertyDescriptors;
		}

		public void setModified(boolean isModified) {
			this.isModified = isModified;
		}

		public boolean isModified() {
			return isModified;
		}
		
		public KWidget getWidget() {
			return widget;
		}

	}

	public Map<KPage, Map<KWidget, List<IPropertyDescriptor>>> getModifiedData() {
		return modifiedData;
	}
	
	public Map<KPage, IFile> getFormFileMap() {
		return formFileMap;
	}
	
	public void setFormModifyListener(IFormModifyListener formModifyListener) {
		this.formModifyListener = formModifyListener;
	}
	
	public static interface IFormModifyListener {
		void formModified();
	}
	
	public void refresh() {
		propTableViewer.refresh();
	}
	
	public static Set<String> getUnmodifiableEntries() {
		return unmodifiableEntries;
	}
	
	public void setWidgetFilter(String widgetFilter) {
		this.widgetFilter = widgetFilter;
	}
	
	public void addSelectionChangedListener(ISelectionChangedListener selectionChangedListener) {
		propTableViewer.addSelectionChangedListener(selectionChangedListener);
	}
	
}
