package com.pat.tool.keditor.dialogs;

import static com.pat.tool.keditor.constants.MobileChannelConstants.DEFAULT_CHANNEL_ID;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.pat.tool.keditor.constants.MobileChannelConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.model.KContainer;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.model.KWidget;
import com.pat.tool.keditor.portability.TemplateWidgetPropertiesTable;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.TemplatesCache;
import com.pat.tool.keditor.utils.model.datagrid.TemplateData;
import com.pat.tool.keditor.utils.model.datagrid.TemplateDataUtils;

/**
 * This dialog will be populated with template and its all widget with 
 * desired properties. 
 * 
 *  For e.g. 
 *   input abc.kl and desired property list for each widget.
 *      Kpage
 *      |-KForm
 *         |-KHBOX
 *            |-KLabel
 * 
 *  Say for e.g KPage is not required here, so no properties
 *  So need to form an input in following format for this class so those property only visible in the
 *  filtered item. 
 *  Map<KWidget, List<String>>
 *   
 * @author Sharath
 *
 */
public class TemplatePropertiesDialog extends TitleAreaDialog {

	private KPage page;
	private String templateName = KUtils.EMPTY_STRING;
	private TemplateData templateData;
	private IProject project;
	private int platform = DEFAULT_CHANNEL_ID;
	/**
	 * -1 is given because currently grid templates are not present in any subfolder of grids folder
	 * like mobile,tablet or desktop
	 * @see TemplatesCache.getGrid
	 */
	private int formCategory = KPage.DESKTOPKIOSK_CAT;
	private final int formType;
	private TemplateWidgetPropertiesTable templateWidgetPropertiesTable;
	
	public TemplatePropertiesDialog(Shell parentShell,String templateName,IProject project,TemplateData templateData, 
			int  formType, int formCategory, int platform) {
		super(parentShell);
		this.templateName = templateName;
		this.project = project;
		this.templateData = templateData;
		this.formType = formType;
		this.formCategory = formCategory;
		this.platform = platform;
	}
	
		
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Edit Properties");
		newShell.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.prop_image));
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Control createDialogArea = super.createDialogArea(parent);
		Composite composite = new Composite((Composite) createDialogArea, SWT.NONE);
		composite.setLayout(new GridLayout());
		
		templateWidgetPropertiesTable = new TemplateWidgetPropertiesTable(composite, SWT.NONE, true);
		GridDataFactory.fillDefaults().grab(true, true).hint(600, 450).applyTo(templateWidgetPropertiesTable);
		page = getAndInitializeTemplatePage();
		//Assert.isNotNull(page, "The grid template " + templateName + "does not exist.");
		return parent;
	}
	
	protected Control createContents(Composite parent) {
		Control createContents = super.createContents(parent);
		if(page != null) {
			if(page.getChildren() == null || page.getChildren().size() == 0) {
				setErrorMessage("No widgets present in template to define data");
				getButton(IDialogConstants.OK_ID).setEnabled(false);
			} else {
				templateWidgetPropertiesTable.setInput(page);//page
				setMessage("Edit Template Properties");
			}
		} else {
			setErrorMessage("No Template is defined");
			getButton(IDialogConstants.OK_ID).setEnabled(false);
		}
		setTitle("Template Properties");
		return createContents;
	}
	
	private KPage getAndInitializeTemplatePage() {
		boolean getKPageFromScratch = false;
		String templateNameWithoutExt = KUtils.getFormNameWithoutExtension(templateName); 
		String gridTemplateNameWithoutExt = KUtils.getFormNameWithoutExtension(templateData.getGridTemplateName());
		if(templateData == null /*|| templateData.isDummyData()*/ || !gridTemplateNameWithoutExt.equals(templateNameWithoutExt)
				|| templateNameWithoutExt.isEmpty() || templateNameWithoutExt.equals(KUtils.DEFAULT_NONE)) {
			getKPageFromScratch = true;
		}
		if(getKPageFromScratch) {
			KPage templatePage = TemplateDataUtils.getTemplatePage(project.getName(), templateNameWithoutExt, platform, formCategory, formType);
			if(templatePage == null && platform != MobileChannelConstants.DEFAULT_CHANNEL_ID) {
				templatePage = TemplateDataUtils.getTemplatePage(project.getName(), templateNameWithoutExt, MobileChannelConstants.DEFAULT_CHANNEL_ID, formCategory, formType);
			}
			return templatePage;
		} else {
			TemplateDataUtils.syncDataGridTemplateWithGrid(templateData, project.getName(), templateNameWithoutExt,
					platform, formCategory, formType);
			return TemplateDataUtils.getKPageFromDataGridTemplate(templateData,project.getName(),platform,formCategory);
		}
	}


	public void populateWidgetsListForContainer(List<KWidget> widgetsList,Object	object) {
		if(object instanceof KContainer) {
			Iterator itr = ((KContainer)object).getChildren().iterator();
			while (itr.hasNext()) {
				Object child1 = (Object) itr.next();
				populateWidgetsListForContainer(widgetsList, child1);
			}
		} else if(object instanceof KWidget) {
			widgetsList.add((KWidget)object);
		}
	}
	
	@Override
	protected void okPressed() {
		if(page != null) {
			templateData = TemplateDataUtils.initTemplateDataFromKPage(page, formType);
		}
		super.okPressed();
	}


	public Object getDummyObject() {
		return templateData;
	}


	public TemplateData getTemplateData() {
		return templateData;
	}


	public void setTemplateData(TemplateData templateData) {
		this.templateData = templateData;
	}


	public void setPlatform(int platform) {
		this.platform = platform;
	}


	public int getPlatform() {
		return platform;
	}


	public void setFormCategory(int formCategory) {
		this.formCategory = formCategory;
	}


	public int getFormCategory() {
		return formCategory;
	}
}
