package com.pat.tool.keditor.dialogs;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

import com.pat.tool.keditor.mapping.ui.dialogs.GridTemplateSelectionDialog;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.utils.ColumnData;
import com.pat.tool.keditor.utils.KUtils;

/**
 * This is a Column Header Editing Support.
 * Same one we would like to use it Column Value as well.
 *  
 * For Text Header Type it will return {@link TextCellEditor}
 * otherwise it will {@link DialogCellEditor}
 * 
 * @author Rakesh
 * 3 Feb,2012
 */

public class ColumnHeaderEditingSupport extends EditingSupport {
	
	private int columnIndex;
	private TextCellEditor textCellEditor;
	private TableDataDialog tableDataDialog;
	
	public ColumnHeaderEditingSupport(TableDataDialog tableDataDialog, ColumnViewer viewer, int columnIndex) {
		super(viewer);
		this.tableDataDialog = tableDataDialog;
		this.columnIndex = columnIndex;
		textCellEditor = new TextCellEditor((Composite)viewer.getControl());
	}
	
	@Override
	protected CellEditor getCellEditor(final Object element) {
		if(element instanceof ColumnData) {
			ColumnData colData = (ColumnData) element;
			int headerType = colData.colHeaderType;
			if(columnIndex == TableDataDialog.COLUMN_HEADER_INDEX){
				switch(headerType){
				case ColumnData.TEXT_HEADER_TYPE:
					return textCellEditor;
				case ColumnData.TEMPLATE_HEADER_TYPE:
					return new DialogCellEditor((Composite) getViewer().getControl()) {
						
						@Override
						protected Object openDialogBox(Control cellEditorWindow) {
							GridTemplateSelectionDialog dialog = new GridTemplateSelectionDialog(tableDataDialog.project, KPage.GRID_MODE);
							int option = dialog.open();
							if(option == Window.OK){
								Object firstResult = dialog.getFirstResult();
								return firstResult;
							}
							return null;
						}
					};
				}
			} else if(columnIndex == TableDataDialog.COLUMN_INDEX){
				int colType = colData.colType;
				switch(colType){
				case ColumnData.TEXT_ROW_TYPE:
					return textCellEditor;
				case ColumnData.TEMPLATE_ROW_TYPE:
					return new DialogCellEditor((Composite) getViewer().getControl()) {
						
						@Override
						protected Object openDialogBox(Control cellEditorWindow) {
							GridTemplateSelectionDialog dialog = new GridTemplateSelectionDialog(tableDataDialog.project, KPage.GRID_MODE);
							int option = dialog.open();
							if(option == Window.OK){
								Object firstResult = dialog.getFirstResult();
								return firstResult;
							}
							return null;
						}
					};
				}
			} /*else if(columnIndex == TableDataDialog.COLUMN_HEADER_DATA_INDEX){
				String templateName = colData.colHeader;
				templateDataCellEditor.setTemplateName(templateName);
				return templateDataCellEditor;
			}*/
		}
		return null;
	}

	@Override
	protected boolean canEdit(Object element) {
		if(!(element instanceof ColumnData)){
			return true;
		}
		ColumnData cData = (ColumnData) element;
		switch(columnIndex){
		case TableDataDialog.COLUMN_INDEX:
			if(cData.colType != ColumnData.TEMPLATE_ROW_TYPE){
				return false;
			}
			break;
		/*case TableDataDialog.COLUMN_HEADER_DATA_INDEX:
			if(cData.colHeaderType != 1){
				return false;
			}
			String templateName = cData.colHeader;
			if(templateName.isEmpty()) {
				tableDataDialog.setMessage("Please define a header template for " + "'" + cData.getId() + "'",IMessageProvider.INFORMATION);
				return false;
			} else {
				IFolder folder = project.getFolder(NavigationViewConstants.FOLDER_TEMPLATES+File.separator+NavigationViewConstants.FOLDER_GRIDS);
				IFile templateFile = folder.getFile(templateName);
				if(!templateFile.exists()) {
					tableDataDialog.setMessage("The header template" + "'" + templateName + "'" + " does not exist",IMessageProvider.ERROR);
					return false;
				}
			}*/
		}	
		return true;
	}

	@Override
	protected Object getValue(Object element) {
		if(element instanceof ColumnData) {
			ColumnData colData = (ColumnData) element;
			if(columnIndex == TableDataDialog.COLUMN_HEADER_INDEX)
				return colData.getColHeader();
			else if(columnIndex == TableDataDialog.COLUMN_INDEX){
				return colData.getRowTemplate();
			}/* else if(columnIndex == TableDataDialog.COLUMN_HEADER_DATA_INDEX){
				Object colHeaderData = colData.getColHeaderData();
				if(colHeaderData instanceof DataGridColumnTemplateData) {
					return colData.getColHeaderData();
				} else {
					return new DataGridColumnTemplateData();
				}
			}*/
		}
		return "";
	}

	@Override
	protected void setValue(Object element, Object value) {
		if(element instanceof ColumnData && value instanceof String) {
			ColumnData colData = (ColumnData) element;
			if(columnIndex == TableDataDialog.COLUMN_HEADER_INDEX)
				colData.colHeader = KUtils.getFormNameWithoutExtension((String) value);
			else if(columnIndex == TableDataDialog.COLUMN_INDEX){
				colData.setRowTemplate(KUtils.getFormNameWithoutExtension((String) value));
			}
			/*if(value instanceof IFile){
				IFile file = (IFile) value;
				if(columnIndex == TableDataDialog.COLUMN_HEADER_INDEX){
					colData.colHeader = KUtils.getFormNameWithoutExtension(file.getName());
				}
				if(columnIndex == TableDataDialog.COLUMN_INDEX){
					colData.rowTemplate= file.getName();
				}
			} else if(value instanceof DataGridColumnTemplateData){
				DataGridColumnTemplateData data = (DataGridColumnTemplateData)value;
				data.setColumnId(colData.getId());
				colData.setColHeaderData(value);
			}else {
				colData.colHeader = (String)value;
			}*/
			tableDataDialog.validatePage(true);
			tableDataDialog.updateRowsData();
			getViewer().refresh(colData);
		}
	}
}
