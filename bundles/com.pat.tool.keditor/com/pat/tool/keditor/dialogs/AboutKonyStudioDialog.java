package com.pat.tool.keditor.dialogs;

import java.io.File;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.kony.ant.task.OS;
import com.kony.studio.StudioConstants;
import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.utils.KUtils;
/**
 * This is a About KonyStudio dialog, providing information about the KonyStudio. 
 * 
 * @author Lakshman K
 * Since 7.0
 *
 */
public class AboutKonyStudioDialog extends TrayDialog {
	
	
	private static String formattedText;
	
	
	Button viewCredits;
	Composite composite;
	
	static  {
		updateText();
	}
	
	private static void updateText() {
		
		String imagePath = KUtils.getPluginLocation()+"src"+File.separator+"com"+
							File.separator+"pat"+File.separator+"tool"+File.separator+"keditor"+
							File.separator+"icons"+File.separator+"aboutkonystudio.png";
		imagePath = OS.getOSSpecificPath(imagePath);
		imagePath = imagePath.substring(1, imagePath.length());
		String viewCreditsURL = "http://www.kony.com/oslicenses#vis";
		
		formattedText = " <html> <head> <style type=\'text/css\'>"
				+ "body { "
				+ "overflow: hidden;"
				+ "background: url("+imagePath+") no-repeat;"
				+ "}"
				+ "input.view-credits {"
				+ "cursor: pointer;"
				+ "background: #1e1e1e;"
				+ "color: white;"
				+ "border: none;"
				+ "border-radius: 5px;"
				+ "font-family: Arial;"
				+ "font-size: 12px;"
				+ "padding: 7px 15px!important;"
				+ "position: absolute;"
				+ "bottom: 20px;"
				+ "right: 20px; "
				+ "}"
				+ "</style>"
				+ "</head> <body>"
				+ "<input type='button' value='View Credits' class=\'view-credits\'"
				+ "onclick=\"javascript:window.open(\'"+viewCreditsURL+"\')\"/>"
				+ "</body></html> ";
	}
	
	public AboutKonyStudioDialog(Shell shell) {
		super(shell);
		setShellStyle((shell.getStyle() | SWT.SHEET) & (~SWT.RESIZE));
	}
	/**
	 * This API is used to append font to the corresponding items
	 * @param field
	 * @return <font style='font-family:Arial;font-size:12px;font-weight:normal'>Test</font>
	 */
	private static String applyFontType(String field) {
		String fontTypeStart="<font style='font-family:Arial;font-size:12px;font-weight:normal'>",fontTypeEnd="</font>";		
		return fontTypeStart+field+fontTypeEnd;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("About "+ StudioConstants.PRODUCT_NAME+"");
	}

	@Override
	protected int getShellStyle() {
		return super.getShellStyle();
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = (Composite) super.createDialogArea(parent);	
		composite = new Composite(c, SWT.BORDER);
		composite.setBackground(ColorConstants.white);
//		composite.setSize(540, 337);
		GridLayout grid = new GridLayout(1, false);
		grid.horizontalSpacing = 0;
		grid.verticalSpacing = 0;
		grid.marginWidth = 0;
		grid.marginHeight = 0;
		composite.setLayout(grid);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, false, false);
		composite.setLayoutData(gridData);
		createBrowserComposite(composite);
		setHelpAvailable(false);
		
		return c;
	}
	
	private void createBrowserComposite(Composite browsercomposite) {
		updateText();// required when license has been changed throug manage option
		Browser browser = new Browser(composite, SWT.NONE | SWT.READ_ONLY );
		//new PluginDetails(browser, "openPluginDetailsDialog"); 
        GridData gridData2 = new GridData(SWT.FILL,SWT.FILL,true,true);
        gridData2.widthHint = 538;
        gridData2.heightHint = 337;
        browser.setLayoutData(gridData2);
        browser.setText(formattedText);
	}
	
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		
		GridLayout gridlayout = (GridLayout) parent.getLayout();
		gridlayout.marginHeight = 0;
	}
	
	@Override
	protected boolean isResizable() {
		return false;
	}
	
	
	@Override
	protected void okPressed(){
		super.okPressed();
	}
	@Override
	protected void cancelPressed() {
		super.cancelPressed();
	}
	@Override
	protected void handleShellCloseEvent() {
		super.handleShellCloseEvent();
	}
}
