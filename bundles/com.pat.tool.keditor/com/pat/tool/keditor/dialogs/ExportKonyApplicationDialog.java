package com.pat.tool.keditor.dialogs;

import java.io.File;
import java.text.MessageFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IProject;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.utils.UIUtils;

/**
 * 
 * @author Piyush Devani
 */
public class ExportKonyApplicationDialog extends TitleAreaDialog {

	private static final String HELP_CONTEXT_ID = "exportApplicationsViz_dialog_id";
	private static final String ZIP_EXISTS_KEY = "{0} already exists and it will be overwritten"; //$NON-NLS-1$
	private static final String ZIP_NAME_INVALID_KEY = "{0} should start with alphabet and can contain alphanumeric number characters and _"; //$NON-NLS-1$
	private static final String ZIP_NAME_EMPTY = "Please specify {0}"; //$NON-NLS-1$
	private static final String ZIP_FOLDER_LOCN_NOT_EXIST_KEY = "{0} does not exist"; //$NON-NLS-1$
	private static final String ZIP_FOLDER_LOCN_EMPTY_KEY = "Please specify {0}"; //$NON-NLS-1$
	private static final String EXPORT_KONY_APPLICATION = "Export Kony Application"; //$NON-NLS-1$
	private static final String ZIP_FILE_DESTINATION = "Zip file destination "; //$NON-NLS-1$
	private static final String ZIP_FILE_NAME = "Zip file name"; //$NON-NLS-1$
	private static final String DIALOG_MESSAGE = EXPORT_KONY_APPLICATION + " in a zip file"; //$NON-NLS-1$
	private static final String BROWSE_BUTTON_TOOLTIP = "Browse folder location"; //$NON-NLS-1$
	private static final String EXPORT = "Export";
	
	private Text browseText;
	private Text zipFileNameText;
	private IProject project;
	private ZipInfo zipInfo;
	private Button importServicesCheck;
	
	private static final Pattern ZIP_FILE_NAME_REGEX = Pattern
	.compile("[a-zA-Z][a-zA-Z0-9_]*"); //$NON-NLS-1$

	public ExportKonyApplicationDialog(Shell parentShell,IProject project) {
		super(parentShell);
		this.project = project;
		setHelpAvailable(true);

	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText(EXPORT_KONY_APPLICATION);
	}
	
	@Override
	protected int getShellStyle() {
		return super.getShellStyle() | SWT.RESIZE;
	}
	
	@Override
	protected Control createContents(Composite parent) {
		Control contents = super.createContents(parent);
		setTitle(EXPORT_KONY_APPLICATION);
		setMessage(DIALOG_MESSAGE);
		validatePage();
		return contents;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite createDialogArea = (Composite) super.createDialogArea(parent);
		createGroups(createDialogArea);

		if(!KEditorPlugin.isCloudMode()){
			UIUtils.createNote(createDialogArea, "Note: User is not connected to KonyFabric. Services from Kony Fabric workspace will not be exported.", 5);
		}
		PlatformUI.getWorkbench().getHelpSystem().setHelp(createDialogArea, HELP_CONTEXT_ID);
		return createDialogArea;
	}

	private void createGroups(Composite parent) {
		Composite browseComposite = new Composite(parent, SWT.NONE);
		browseComposite.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		browseComposite.setLayout(new GridLayout(4, false));
		
		Label selectResourceLabel = new Label(browseComposite, SWT.NONE);
		selectResourceLabel.setText(ZIP_FILE_DESTINATION);
		
		browseText = new Text(browseComposite, SWT.BORDER /*| SWT.READ_ONLY*/);
		browseText.setLayoutData(new GridData(SWT.FILL,SWT.BEGINNING,true,false));
		
		String exportLocation = KEditorPlugin.getExportProjLocation();
		if(exportLocation == null) {
			exportLocation = "";
		}
		if(!new File(exportLocation).exists()) {
			exportLocation = System.getProperty("user.home");
		}
		browseText.setText(exportLocation);
		
		browseText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				validatePage();
			}
		});
		
		GridData buttonData = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false);
		buttonData.heightHint = 22;
		
		Button browseBtn = new Button(browseComposite, SWT.NONE);
		browseBtn.setText("..."); //$NON-NLS-1$
		browseBtn.setLayoutData(buttonData);
		browseBtn.setToolTipText(BROWSE_BUTTON_TOOLTIP); //$NON-NLS-1$
		
		browseBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(getShell());
				String open = dialog.open();
				if(open != null) {
					browseText.setText(open);
				}
			}
		});
		
		Button clearBtn = new Button(browseComposite, SWT.NONE);
		clearBtn.setLayoutData(buttonData);
		
		clearBtn.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.deleteImage));
		clearBtn.setToolTipText("Clear"); //$NON-NLS-1$
		clearBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				browseText.setText(""); //$NON-NLS-1$
			}
		});
		
		Label zipFileNameLabel = new Label(browseComposite, SWT.NONE);
		zipFileNameLabel.setText(ZIP_FILE_NAME);
		
		zipFileNameText = new Text(browseComposite, SWT.BORDER);
		GridData layoutData = new GridData(GridData.FILL_HORIZONTAL);
		layoutData.horizontalSpan = 3;
		zipFileNameText.setLayoutData(layoutData);
		zipFileNameText.setText(project.getName() + EXPORT);
		zipFileNameText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				validatePage();
			}
		});
		
		Composite importServicesCheckGroup = new Composite(parent, SWT.NONE);
		GridLayout glayout = new GridLayout();
		glayout.numColumns = 1;
		glayout.makeColumnsEqualWidth = false;
		importServicesCheckGroup.setLayout(glayout);
		importServicesCheckGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		importServicesCheckGroup.layout();
		
		importServicesCheck = new Button(importServicesCheckGroup, SWT.CHECK);
		importServicesCheck.setText("Export services from Kony Fabric workspace as part of project.");
		GridData data = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 3, 4);
		importServicesCheck.setLayoutData(data);;
		
		if(!KEditorPlugin.isCloudMode())
			importServicesCheckGroup.setVisible(false);
	}
	
	protected void validatePage() {
		int msgType = IMessageProvider.ERROR;
		String msg = null;
		String folderLocn = browseText.getText().trim();
		if(folderLocn.length() == 0) {
			msg = MessageFormat.format(ZIP_FOLDER_LOCN_EMPTY_KEY, ZIP_FILE_DESTINATION);
		} else {
			File file = new File(folderLocn);
			if(!file.exists()) {
				msg = MessageFormat.format(ZIP_FOLDER_LOCN_NOT_EXIST_KEY, ZIP_FILE_DESTINATION);
			} else {
				String zipFileName = zipFileNameText.getText().trim();
				if(zipFileName.length() == 0) {
					msg = MessageFormat.format(ZIP_NAME_EMPTY, ZIP_FILE_NAME);
				}
				Matcher matcher = ZIP_FILE_NAME_REGEX.matcher(zipFileName);
				if(!matcher.matches()) {
					msg = MessageFormat.format(ZIP_NAME_INVALID_KEY, ZIP_FILE_NAME);
				}
				String zipFileFullName = zipFileName + ".zip"; //$NON-NLS-1$
				File zipFile = new File(file, zipFileFullName);
				if(zipFile.exists()) {
					msg = MessageFormat.format(ZIP_EXISTS_KEY, zipFile);
					msgType = IMessageProvider.WARNING;
				}
			}
		}
		if(msg == null) {
			msg = DIALOG_MESSAGE;
			msgType = IMessageProvider.NONE;
		}
		setMessage(msg, msgType);
		getButton(OK).setEnabled(msgType != IMessageProvider.ERROR);
			
	}
	
	@Override
	protected void okPressed() {
		zipInfo = new ZipInfo();
		zipInfo.setZipFileDestination(browseText.getText().trim());
		zipInfo.setZipFileName(zipFileNameText.getText().trim());
		zipInfo.setIncludeServices(importServicesCheck.getSelection());
		KEditorPlugin.saveExportProjLocation(browseText.getText().trim());
		super.okPressed();
	}
	
	public static class ZipInfo {
		private String zipFileDestination;
		private String zipFileName;
		private boolean includeServices;
		
		public boolean isIncludeServices() {
			return includeServices;
		}
		public void setIncludeServices(boolean includeServices) {
			this.includeServices = includeServices;
		}
		public String getZipFileDestination() {
			return zipFileDestination;
		}
		public String getZipFileName() {
			return zipFileName;
		}
		public void setZipFileDestination(String zipFileDestination) {
			this.zipFileDestination = zipFileDestination;
		}
		public void setZipFileName(String zipFileName) {
			this.zipFileName = zipFileName;
		}
		@Override
		public String toString() {
			return "ZipInfo [zipFileDestination=" + zipFileDestination //$NON-NLS-1$
					+ ", zipFileName=" + zipFileName + "]"; //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	public ZipInfo getZipInfo() {
		return zipInfo;
	}

}
