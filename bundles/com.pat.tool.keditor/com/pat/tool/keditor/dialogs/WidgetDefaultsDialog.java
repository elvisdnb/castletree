package com.pat.tool.keditor.dialogs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.forms.widgets.Section;

import com.ibm.msg.client.wmq.v6.network.FWHelper;
import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.actions.WidgetDefaultsAction;
import com.pat.tool.keditor.console.ConsoleDisplayManager;
import com.pat.tool.keditor.editors.help.ContextSensitiveHelpConstants;
import com.pat.tool.keditor.editors.help.ContextSensitiveHelpUtil;
import com.pat.tool.keditor.model.IPropertyNameConstants;
import com.pat.tool.keditor.model.IWidget;
import com.pat.tool.keditor.model.WidgetUtil.Widget;
import com.pat.tool.keditor.model.psp.PropertyDescUtils;
import com.pat.tool.keditor.resources.WidgetDefaultsUtils;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.WidgetDefaultsConstants;
import com.pat.tool.keditor.utils.windowsViews.WinViewsConstants;

/**
 * @author Ram Anvesh
 * @since  3.2
 * @date Aug 2, 2011
 */
public class WidgetDefaultsDialog extends TitleAreaDialog {
	
	public static final String DEFAULTS_FILE_NAME = "defaults.properties";
	public static final String DEFAULTS_FOLDER_NAME = WidgetDefaultsUtils.DEFAULTS_FOLDER_NAME;
	
	private static IWidget[] applicableWidgets = {Widget.FORM};
	private TreeViewer widgetTreeViewer;
	private Composite propertiesComp;
	private Properties defaultsProperties;
	private final IProject project;
	private StackLayout stackLayout = new StackLayout();
	private Properties unmodifiedDefaultsProperties;

	public WidgetDefaultsDialog(Shell parentShell, IProject project) {
		super(parentShell);
		this.project = project;
		
		unmodifiedDefaultsProperties = initDefaultsProperties(project);
		defaultsProperties = initDefaultsProperties(project);
		setShellStyle(getShellStyle() | SWT.RESIZE);
	}
	
	public static Properties initDefaultsProperties(IProject project) {
		Properties defaultsProperties = new Properties();
		File defaultsFile = getDefaultsFile(project);
		if(defaultsFile.exists()){
			try {
				FileInputStream fs = new FileInputStream(defaultsFile);
				defaultsProperties.load(fs);
				fs.close();
			} catch (Exception e) {
			}
		} else {
			//Fill with default values
			defaultsProperties.put(WidgetDefaultsConstants.FORM_WINDOWS_IN_TRANSIT, WinViewsConstants.SLIDE_TRANSIT);
			defaultsProperties.put(WidgetDefaultsConstants.FORM_WINDOWS_OUT_TRANSIT, WinViewsConstants.SLIDE_TRANSIT);
			defaultsProperties.put(WidgetDefaultsConstants.FORM_WINDOWS_TRANSIT_MODE, WinViewsConstants.PARALLEL_TRANSIT_MODE);
			defaultsProperties.put(WidgetDefaultsConstants.FORM_WINDOWS_ANIMATE_HEAD_FOOT, WinViewsConstants.FALSE);
		}
		return defaultsProperties;
	}

	public static File getDefaultsFile(IProject project) {
		return new File(KUtils.getProjectLocation(project) + File.separator + DEFAULTS_FOLDER_NAME + File.separator + DEFAULTS_FILE_NAME);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite createDialogArea = (Composite) super.createDialogArea(parent);
		createMainComposite(createDialogArea);
		
		setTitle(WidgetDefaultsAction.DISPLAY_NAME);
		setMessage("Set the default properties to be applied to widgets if not specified at form level.");
    	ContextSensitiveHelpUtil.hookHelp(createDialogArea, ContextSensitiveHelpConstants.WIDGET_DEFAULTS);

		
		return createDialogArea;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Default Properties");
	}

	private void createMainComposite(Composite parent) {
		SashForm sash =  new SashForm(parent, SWT.HORIZONTAL);
		GridData layoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		sash.setLayoutData(layoutData);
		layoutData.widthHint = 400;
		layoutData.heightHint = 300;
		
		createWidgetTypeTree(sash);
		widgetTreeViewer.getTree().select(widgetTreeViewer.getTree().getItem(0));
		ITreeSelection selection = (ITreeSelection) widgetTreeViewer.getSelection();
		
		Object firstElement = null;
		if(selection.size() == 1)
			firstElement = selection.getFirstElement();
		if(!(firstElement instanceof IWidget))
			firstElement = null;
		createPropertyEntrySection(sash, (IWidget) firstElement);
		
		
		sash.setWeights(new int[]{30,70});
	}

	private void createWidgetTypeTree(final Composite parent) {
		widgetTreeViewer = new TreeViewer(parent);
		
		widgetTreeViewer.setContentProvider(new ITreeContentProvider() {
			
			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}
			
			@Override
			public void dispose() {
			}
			
			@Override
			public boolean hasChildren(Object element) {
				return false;
			}
			
			@Override
			public Object getParent(Object element) {
				return null;
			}
			
			@Override
			public Object[] getElements(Object inputElement) {
				if(inputElement instanceof Object[]){
					return (Object[]) inputElement;
				}
				return null;
			}
			
			@Override
			public Object[] getChildren(Object parentElement) {
				return null;
			}
		});
		
		widgetTreeViewer.setLabelProvider(new LabelProvider(){
			@Override
			public String getText(Object element) {
				if(element instanceof IWidget){
					return ((IWidget) element).getName();
				}
				return super.getText(element);
			}
			
			@Override
			public Image getImage(Object element) {
				if(element instanceof IWidget){
					return ((IWidget) element).getIconImage();
				}
				return super.getImage(element);
			}
		});
		
		widgetTreeViewer.setInput(applicableWidgets);
		
		widgetTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				ITreeSelection selection = (ITreeSelection) event.getSelection();
				Object firstElement = selection.getFirstElement();
				if(selection.size() == 1  && firstElement instanceof IWidget){
					createPropertyEntrySection(parent, (IWidget) firstElement);
				}
			}
		});
	}

	private void createPropertyEntrySection(Composite parent, IWidget widget) {
		if(propertiesComp == null){
			propertiesComp= new Composite(parent, SWT.NONE);
			propertiesComp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
			propertiesComp.setLayout(stackLayout);
		}
	
		Control childComp = null;
		
		if(widget == null){
			childComp = new Composite(propertiesComp, SWT.NONE);
			return;
		}
		if(widget instanceof Widget){
			switch ((Widget)widget) {
			case FORM:
				childComp = createFormPropertiesSection(propertiesComp);
				break;
			default:
				break;
			}
		}
		
		stackLayout.topControl = childComp;
		propertiesComp.layout();
		
	}

	private Control createFormPropertiesSection(Composite parent) {
		for(Control c : parent.getChildren()){
			c.dispose();
		}
		Composite comp = new Composite(parent, SWT.NONE);
		comp.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		comp.setLayout(new GridLayout(1, false));
		
		Section windowsSection = new Section(comp, Section.TITLE_BAR | Section.TWISTIE | Section.EXPANDED);
		windowsSection.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		windowsSection.setText(PropertyDescUtils.CAT_WIN_MOBILE);
		windowsSection.setTitleBarBackground(ColorConstants.lightGray);
		windowsSection.marginHeight = 0;
		windowsSection.marginWidth = 0;
		
		Composite windowsComp = new Composite(windowsSection, SWT.BORDER);
		windowsComp.setLayout(new GridLayout(2, false));
		windowsComp.setBackground(ColorConstants.white);

		windowsSection.setClient(windowsComp);
		
		Label inTransit= new Label(windowsComp, SWT.NONE);
		inTransit.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		inTransit.setText(IPropertyNameConstants.PROP_NAME_IN_TRANSITION);
		
		final Combo inTransitCombo= new Combo(windowsComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		inTransitCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		inTransitCombo.setItems(removeDefault(WinViewsConstants.IN_TRANSIT_ITEMS_DISPLAY, true));
		inTransitCombo.select(0); //Default value
		inTransitCombo.select(KUtils.getIndex(removeDefault(WinViewsConstants.IN_TRANSIT_ITEMS, false), 
				defaultsProperties.get(WidgetDefaultsConstants.FORM_WINDOWS_IN_TRANSIT)));
		inTransitCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				defaultsProperties.put(WidgetDefaultsConstants.FORM_WINDOWS_IN_TRANSIT,
						removeDefault(WinViewsConstants.IN_TRANSIT_ITEMS, false)[inTransitCombo.getSelectionIndex()]);
			}
		});
		
		
		Label outTransitLabel= new Label(windowsComp, SWT.NONE);
		outTransitLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		outTransitLabel.setText(IPropertyNameConstants.PROP_NAME_OUT_TRANSITION);
		
		final Combo outTransitCombo= new Combo(windowsComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		outTransitCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		outTransitCombo.setItems(removeDefault(WinViewsConstants.OUT_TRANSIT_ITEMS_DISPLAY, true));
		outTransitCombo.select(0); //Default value
		outTransitCombo.select(KUtils.getIndex(removeDefault(WinViewsConstants.OUT_TRANSIT_ITEMS, false), 
				defaultsProperties.get(WidgetDefaultsConstants.FORM_WINDOWS_OUT_TRANSIT)));
		outTransitCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				defaultsProperties.put(WidgetDefaultsConstants.FORM_WINDOWS_OUT_TRANSIT, 
						removeDefault(WinViewsConstants.OUT_TRANSIT_ITEMS, false)[outTransitCombo.getSelectionIndex()]);
			}
		});
		
		Label transitModeLabel= new Label(windowsComp, SWT.NONE);
		transitModeLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		transitModeLabel.setText("In/Out Transition mode");
		
		final Combo transitModeCombo= new Combo(windowsComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		transitModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		transitModeCombo.setItems(removeDefault(WinViewsConstants.IN_TRANSIT_MODE_ITEMS_DISPLAY, true));
		transitModeCombo.select(0);
		transitModeCombo.select(KUtils.getIndex(removeDefault(WinViewsConstants.IN_TRANSIT_MODE_ITEMS, false), 
				defaultsProperties.get(WidgetDefaultsConstants.FORM_WINDOWS_TRANSIT_MODE)));
		transitModeCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				defaultsProperties.put(WidgetDefaultsConstants.FORM_WINDOWS_TRANSIT_MODE, 
						removeDefault(WinViewsConstants.IN_TRANSIT_MODE_ITEMS, false)[transitModeCombo.getSelectionIndex()]);
			}
		});
		
		Label animHeaderFooterLabel= new Label(windowsComp, SWT.NONE);
		animHeaderFooterLabel.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, false, false));
		animHeaderFooterLabel.setText(IPropertyNameConstants.PROP_NAME_ANIMATE_HEADERS_FOOTERS);
		
		final Combo animHeaderFooterCombo= new Combo(windowsComp, SWT.DROP_DOWN | SWT.READ_ONLY);
		animHeaderFooterCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		animHeaderFooterCombo.setItems(removeDefault(WinViewsConstants.ANIMATE_HEADERS_FOOTERS_OPTIONS_DISPLAY, true));
		animHeaderFooterCombo.select(0);
		animHeaderFooterCombo.select(KUtils.getIndex(removeDefault(WinViewsConstants.ANIMATE_HEADERS_FOOTERS_OPTIONS, false), 
				defaultsProperties.get(WidgetDefaultsConstants.FORM_WINDOWS_ANIMATE_HEAD_FOOT)));
		animHeaderFooterCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				defaultsProperties.put(WidgetDefaultsConstants.FORM_WINDOWS_ANIMATE_HEAD_FOOT, 
						removeDefault(WinViewsConstants.ANIMATE_HEADERS_FOOTERS_OPTIONS, false)[animHeaderFooterCombo.getSelectionIndex()]);
			}
		});
		
		
		return comp;
	}
	
	@Override
	protected void okPressed() {
		if(!defaultsProperties.equals(unmodifiedDefaultsProperties)){
			writeDefaultsData(defaultsProperties,project);
			close();
		}
		return;
	}

	public static void writeDefaultsData(Properties defaultsProperties,IProject project) {
		try {
			String defaultsFolder = KUtils.getProjectLocation(project) + File.separator + DEFAULTS_FOLDER_NAME;
			File folder = new File(defaultsFolder);
			folder.mkdirs();
			File file = new File(defaultsFolder + File.separator + DEFAULTS_FILE_NAME);
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			try {
				defaultsProperties.store(fos, "");
			} catch (Exception ex) {
				KEditorPlugin.logError(ex);
				ConsoleDisplayManager.getDefault().printException(ex, ConsoleDisplayManager.MSG_ERROR);
			} finally {
				fos.close();
			}
		} catch (Exception e) {
			KEditorPlugin.logError(e);
			ConsoleDisplayManager.getDefault().printException(e, ConsoleDisplayManager.MSG_ERROR);
		}
	}
	
	public static final String[] removeDefault(String[] array, boolean isDisplayValue){
		String removeable = null;
		if(isDisplayValue)
			removeable = WinViewsConstants.DEFAULT_DISPLAY;
		else
			removeable = WinViewsConstants.DEFAULT;
		List<String> asList = new ArrayList<String>(Arrays.asList(array));
		asList.remove(removeable);
		return asList.toArray(new String[]{});
	}
}
