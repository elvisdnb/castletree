package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class EmulatorFileOptionDialog extends TitleAreaDialog {
	
	private List<String> options = new ArrayList<String>();
	private String enabledOption;
	private Map<String, Button> buttonMap = new HashMap<String, Button>();
//	private ButtonSelectionAdapter adapter;
	
	public EmulatorFileOptionDialog(Shell shell,List<String> options) {
		super(shell);
		this.options = options;
		enabledOption = options.get(0);
	}
	
	protected Control createContents(Composite parent) {
		Control contents = super.createContents(parent);
		setTitle("Choose a file");
		setMessage("The chosen file will be copied to emulator");
		setHelpAvailable(false);
		return contents;
	}
	
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite) super.createDialogArea(parent);
		createGroups(composite);
		getShell().pack();
		return composite;
	}
	
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Emulator option");
	}
	
	protected Control createButtonBar(Composite parent) {
    	Composite composite = (Composite) super.createButtonBar(parent);
    	getShell().pack();
        return composite;
	}
	
	private void createGroups(Composite composite) {
		for(String itr:options) {
			createButton(composite,SWT.RADIO,itr);
		}
		Button btnToEnable = buttonMap.get(enabledOption);
		btnToEnable.setSelection(true);
		
	}
	
	
	
	private Button createButton(Composite parent, int type,String key) {
		Button button = new Button(parent, type);
		button.setText(key);
		button.setData(key);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalIndent = 50;
		data.widthHint = 300;
		data.verticalIndent = 10;
		button.setLayoutData(data);
		buttonMap.put(key, button);
		return button;
	}
	
	/*private class ButtonSelectionAdapter extends SelectionAdapter {
		public void widgetSelected(SelectionEvent e) {
			if(e.widget instanceof Button && buttonMap.containsValue(e.widget)) {
				Button btn = (Button)e.widget;
				boolean sel = btn.getSelection();
				if(sel) {
					for(Button itr:buttonMap.values()) {
						itr.setSelection(false);
					}
				}
				btn.setSelection(true);
				enabledOption = (String) btn.getData();
			}
		}
	}*/

	public String getEnabledOption() {
		return enabledOption;
	}

}
