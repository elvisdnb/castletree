package com.pat.tool.keditor.dialogs;

import static com.pat.tool.keditor.constants.NavigationViewConstants.FOLDER_DESKTOP;
import static com.pat.tool.keditor.constants.NavigationViewConstants.FOLDER_GRIDS;
import static com.pat.tool.keditor.constants.NavigationViewConstants.FOLDER_TEMPLATES;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.CellLabelProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ColumnViewerEditor;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationStrategy;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TableViewerEditor;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.pat.tool.keditor.KEditorPlugin;
import com.pat.tool.keditor.cellEditors.TemplateDataCellEditor;
import com.pat.tool.keditor.constants.NavigationViewConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorConstants;
import com.pat.tool.keditor.imageutils.ImageDescriptorRegistryUtils;
import com.pat.tool.keditor.model.KPage;
import com.pat.tool.keditor.model.KTable;
import com.pat.tool.keditor.model.WidgetUtil;
import com.pat.tool.keditor.propertyDescriptor.NScriptCellEditor;
import com.pat.tool.keditor.table.AddColumnDataAction;
import com.pat.tool.keditor.table.AddNewRowDynamicTableAction;
import com.pat.tool.keditor.table.CustomListeners;
import com.pat.tool.keditor.table.DeleteRow;
import com.pat.tool.keditor.table.DynamicSortListener;
import com.pat.tool.keditor.table.ImageLabelProvider;
import com.pat.tool.keditor.table.InsertAfterAction;
import com.pat.tool.keditor.table.InsertBeforeAction;
import com.pat.tool.keditor.table.MoveDownAction;
import com.pat.tool.keditor.table.MoveUpAction;
import com.pat.tool.keditor.table.MultipleImageEditingSupport;
import com.pat.tool.keditor.table.PotentialNewRowData;
import com.pat.tool.keditor.table.SortListener;
import com.pat.tool.keditor.table.TableUtils;
import com.pat.tool.keditor.table.TableWithButtons;
import com.pat.tool.keditor.utils.ChoiceData;
import com.pat.tool.keditor.utils.ColumnData;
import com.pat.tool.keditor.utils.Columns;
import com.pat.tool.keditor.utils.DisplayUtils;
import com.pat.tool.keditor.utils.EditableTableItem;
import com.pat.tool.keditor.utils.EventRoot;
import com.pat.tool.keditor.utils.KUtils;
import com.pat.tool.keditor.utils.TabFolderUtils;
import com.pat.tool.keditor.utils.TableData;
import com.pat.tool.keditor.utils.ValidationUtil;
import com.pat.tool.keditor.utils.model.ListOfObject;
import com.pat.tool.keditor.utils.model.datagrid.TemplateData;
import com.pat.tool.keditor.widgets.IMobileChannel;
import com.pat.tool.keditor.widgets.MobileChannels;

/**
 * @author Ram Anvesh
 * @since 3.1
 */
public class TableDataDialog extends TitleAreaDialog {
	
	// for setting width to columns
	public static final ColumnData SAMPLE_COLUMN_OBJECT = new ColumnData();
	
	public static final String COLUMNS_INFO_KEY = "columninfo";
	
	public static final int COLUMN_ID_INDEX = 0;
	public static final int COLUMN_HEADER_TYPE_INDEX = 1;
	public static final int COLUMN_HEADER_INDEX = 2;
	public static final int COLUMN_HEADER_DATA_INDEX = 3;
	public static final int COLUMN_TYPE_INDEX = 4;
	public static final int COLUMN_INDEX = 5;
	public static final int COLUMN_WIDTH_INDEX = 6;
	public static final int COLUMN_SORT_INDEX = 7;
	public static final int COLUMN_ALIGNMENT_INDEX = 8;
	public static final int COLUMN_ON_CLICK_INDEX = 9;
	
	
	private static final String COLUMN_ID = "*ID";
	private static final String COLUMN_HEADER_TYPE = "Header Type";
	private static final String COLUMN_HEADER = "Header";
	private static final String COLUMN_TYPE = "Column Type";
	private static final String COLUMN = "Column";
	private static final String COLUMN_HEADER_DATA = "Header Data";
	private static final String COLUMN_ALIGNMENT = "Alignment";
	private static final String COLUMN_SORT = "Sort";
	private static final String COLUMN_WIDTH = "*Width(%)";
	private static final String COLUMN_ON_CLICK = "OnClick";

	//constants
	public static final String ID_PROPERTY = "colID";
	public static final String COLHEADTYPE_PROPERTY="colHeadType";
	public static final String COLHEAD_PROPERTY = "colHead";
	public static final String COLTYPE_PROPERTY = "colType";
	public static final String COL_PROPERTY = "colData";
	public static final String COLWIDTH_PROPERTY = "colWidth";
	public static final String SHOWONLY_PROPERTY = "colShowOnly";
	public static final String COL_VISIBLE = "colVisible";
	public static final String SORT_PROPERTY = "colSort";
	public static final String ONCLICK_PROPERTY = "onClick";
	public static final String ALIGN_PROPERTY = "colAlign";
	public static final String TABLE_MASTERDATA_GRID_EDIT = "tablemasterdata.edit_grid";

	private final List<String> formList;
	private final List<String> functionList;
	private final KPage root;
	private final KTable kTable;
	private final TableData tableData;
	private TableData modifiedTableData;
	private TableViewer columnsTableViewer;
	private String eventType;
	private StackLayout rowsStackLayout;
	private Composite rowsNoneGrp;
	private TableViewer rowsTableViewer;
	private final int category;
	private final Map<IMobileChannel, Boolean> applicablePlatforms;
	private ArrayList<String> newcolnames;
	private Composite rowsTableComp;
	private Composite rowsComp;
	private CTabItem rowsTab;
	final IProject project;
	private CTabItem colTab;
	private NScriptCellEditor nScriptCellEditor;
	private boolean enableGrids = false;
	private List<String> grids = new ArrayList<String>();

	private IMobileChannel mobileChannel;
	
	
	static {
		
	}
	
	public TableDataDialog(Shell parentShell, List<String> formList, List<String> functionList, KPage root, KTable table,
			TableData tableData, String eventType, int category, Map<IMobileChannel, Boolean> applicablePlatforms, IProject project) {
		super(parentShell);
		this.formList = formList;
		this.functionList = functionList;
		this.root = root;
		mobileChannel = MobileChannels.getMobileChannel(root.platform);
		this.kTable = table;
		this.tableData = tableData;
		this.eventType = eventType;
		this.category = category;
		this.applicablePlatforms = applicablePlatforms;
		this.project = project;
		enableGrids = WidgetUtil.isTemplateSupportInsideDataGridMasterData(table);
		if(enableGrids) {
			grids = KUtils.getTemplateNames(project.getName(), FOLDER_TEMPLATES  + File.separator + FOLDER_GRIDS+ File.separator + FOLDER_DESKTOP,"" /*KUtils.getFormCategoryName(root.getFormCategory())*/);
		}
		modifiedTableData = getModelDataForDialog();/*this.tableData.getCopy(true);*/
		setShellStyle(getShellStyle() | SWT.RESIZE);
	}
	
	private TableData getModelDataForDialog() {
		TableData modelData = tableData.getCopy(true);
		TableData.removeImageFormat(modelData);
		ChoiceData rows = modelData.getRows();
		List<ListOfObject> newTableRowData = rows.getNewTableRowData();
		ArrayList<ArrayList> nestedListData = ListOfObject.convertToListOfObjectLists(newTableRowData);
		rows.setTablerowData(nestedListData);
		return modelData;
	}
	
	@Override
	protected Control createContents(Composite parent) {
		Control createContents = super.createContents(parent);
		setTitle("Edit Master data for Data Grid");
		setMessage("Click on the column name to sort",IMessageProvider.INFORMATION);
		validatePage(false);
		return createContents;
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Data Grid: Master Data");
		newShell.setSize(800, 500);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite createDialogArea = (Composite) super.createDialogArea(parent);
		final CTabFolder tabFolder = TabFolderUtils.createCustomTabFolder(createDialogArea);
		tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		tabFolder.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(tabFolder.getSelection()== rowsTab){
					updateRowsData();

					//SWTException is thrown when cell editor is in focus
					//and tab is changed
					//TODO: Ram Anvesh : Debug this
				} else if(tabFolder.getSelection()== colTab){
					updateRowsData();
				}
				updateRowsStackLayout();
			}
		});
		
		createColumnsTab(tabFolder);
		createRowsTab(tabFolder);
		return createDialogArea;
	}
	
	private void createColumnsTab(CTabFolder tabFolder) {
		colTab = new CTabItem(tabFolder, SWT.NONE);
		colTab.setText("Columns");
		
		Composite columnsComp = new Composite(tabFolder, SWT.NONE);
		GridLayout layout2 = new GridLayout(2,false);
		layout2.marginHeight = 0;
		GridData data = new GridData(SWT.FILL,SWT.FILL,true,true);
		columnsComp.setLayoutData(data);
		columnsComp.setLayout(layout2);
		
		colTab.setControl(columnsComp);
		
		TableWithButtons tableWithBtns = new TableWithButtons(columnsComp,SWT.BORDER | SWT.FULL_SELECTION);
		tableWithBtns.createControl();
		columnsTableViewer = tableWithBtns.getViewer();//new TableViewer(table);
		tableWithBtns.getActionsManager().add(new AddColumnDataAction(columnsTableViewer,"Add"));
		tableWithBtns.getActionsManager().add(new DeleteRow(columnsTableViewer,"Delete"));
		tableWithBtns.getActionsManager().add(new MoveUpAction(columnsTableViewer,"MoveUp"));
		tableWithBtns.getActionsManager().add(new MoveDownAction(columnsTableViewer,"MoveDown"));
		tableWithBtns.getActionsManager().add(new InsertBeforeAction(columnsTableViewer,"Insert Before") );
		tableWithBtns.getActionsManager().add(new InsertAfterAction(columnsTableViewer,"Insert After") );
		
		TableLayout tablelayout = new TableLayout();
//		tablelayout.addColumnData(new ColumnWeightData(10, true));
//		tablelayout.addColumnData(new ColumnWeightData(18,  true));
//		tablelayout.addColumnData(new ColumnWeightData(15, true));
//		tablelayout.addColumnData(new ColumnWeightData(15, true));
//		tablelayout.addColumnData(new ColumnWeightData(15,  true));
//		tablelayout.addColumnData(new ColumnWeightData(10,  true));
//		tablelayout.addColumnData(new ColumnWeightData(15,  true));
//		tablelayout.addColumnData(new ColumnWeightData(10, true));
//		tablelayout.addColumnData(new ColumnWeightData(15, true));
//		tablelayout.addColumnData(new ColumnWeightData(13, true));
		
		Table table = columnsTableViewer.getTable();
		GridData gridData = new GridData(GridData.FILL_BOTH);
		table.setLayoutData(gridData);
		table.setLayout(tablelayout);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		
		createColumnViewers(table);

		tableWithBtns.getActionsManager().update(true);
		
		Composite controlComposite = new Composite(columnsComp, SWT.NULL);
		GridLayout layout4 = new GridLayout();
		layout4.numColumns = 5;
		layout4.marginHeight =0;
		layout4.marginWidth=0;
		controlComposite.setLayout(layout4);
		controlComposite.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		Label label = new Label(controlComposite, SWT.NONE);
		label.setText("(*) fields are mandatory");
		label.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLUE));
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalAlignment = SWT.BEGINNING;
		label.setLayoutData(data);
		
		columnsTableViewer.setContentProvider(new ArrayContentProvider());
				
		TableViewerEditor.create(columnsTableViewer,new ColumnViewerEditorActivationStrategy(columnsTableViewer),ColumnViewerEditor.TABBING_HORIZONTAL|ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR|ColumnViewerEditor.TABBING_VERTICAL);
		IBaseLabelProvider labelProvider = columnsTableViewer.getLabelProvider();
		CellLabelProvider labelProvider2 = columnsTableViewer.getLabelProvider(0);
		CellLabelProvider labelProvider3 = columnsTableViewer.getLabelProvider(1);
		columnsTableViewer.setInput(modifiedTableData.getColumns().getColList());
		DisplayUtils.setWidthToColumnsInTableViewer(columnsTableViewer, 50, SAMPLE_COLUMN_OBJECT);
	}

	private void setWidthToColumnViewer() {
		ITableLabelProvider viewerLabelProvider = (ITableLabelProvider) columnsTableViewer.getLabelProvider();
		TableColumn[] columns = columnsTableViewer.getTable().getColumns();
		for (int i = 0; i < columns.length; i++) {
			ColumnLabelProvider labelProvider = (ColumnLabelProvider) columnsTableViewer.getLabelProvider(i);
			if(labelProvider != null) {
				columns[i].setWidth(getColumnWidth(labelProvider,columns[i].getText()));
			} else if(viewerLabelProvider != null){
				columns[i].setWidth(getColumnWidth(viewerLabelProvider,columns[i].getText()));
			}
		}
	}
	

	private int getColumnWidth(ITableLabelProvider viewerLabelProvider,
			String text) {
		// TODO Auto-generated method stub
		return 0;
	}

	private int getColumnWidth(ColumnLabelProvider labelProvider, String text) {
		// TODO Auto-generated method stub
		return 0;
	}

	private void createColumnViewers(Table table) {
		//column headers
		TableColumn col_ID = new TableColumn(table, SWT.LEFT);
		col_ID.setText(COLUMN_ID);
		col_ID.setWidth(FigureUtilities.getTextWidth(COLUMN_ID, col_ID.getParent().getFont())+50);
		col_ID.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));
		col_ID.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn colViewer = new TableViewerColumn(columnsTableViewer, col_ID);
		colViewer.setEditingSupport(new ColumnTextEditingSupport(this,columnsTableViewer,COLUMN_ID_INDEX,enableGrids));
		colViewer.setLabelProvider(new TableLabelProvider(COLUMN_ID_INDEX));
		
		TableColumn header_Type = new TableColumn(table, SWT.LEFT);
		header_Type.setText(COLUMN_HEADER_TYPE);
		header_Type.setWidth(FigureUtilities.getTextWidth(COLUMN_HEADER_TYPE, header_Type.getParent().getFont())+50);
		header_Type.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));
		header_Type.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn headerTypeViewer = new TableViewerColumn(columnsTableViewer, header_Type);
		headerTypeViewer.setEditingSupport(new ColumnTextEditingSupport(this,columnsTableViewer,COLUMN_HEADER_TYPE_INDEX,enableGrids));
		headerTypeViewer.setLabelProvider(new TableLabelProvider(COLUMN_HEADER_TYPE_INDEX));
		
		TableColumn col_header = new TableColumn(table, SWT.LEFT);
		col_header.setText(COLUMN_HEADER);
		col_header.setWidth(FigureUtilities.getTextWidth(COLUMN_HEADER, col_header.getParent().getFont())+50);
		col_header.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));
		col_header.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn headerViewer = new TableViewerColumn(columnsTableViewer, col_header);
		headerViewer.setEditingSupport(new ColumnHeaderEditingSupport(this,columnsTableViewer,COLUMN_HEADER_INDEX));
		headerViewer.setLabelProvider(new TableLabelProvider(COLUMN_HEADER_INDEX));
		
		TableColumn col_header_data = new TableColumn(table, SWT.LEFT);
		col_header_data.setText(COLUMN_HEADER_DATA);
		col_header_data.setWidth(FigureUtilities.getTextWidth(COLUMN_HEADER_DATA, col_header_data.getParent().getFont())+50);
		TableViewerColumn headerDataViewer = new TableViewerColumn(columnsTableViewer, col_header_data);
		headerDataViewer.setEditingSupport(new ColumnTemplateEditingSupport(this,columnsTableViewer,COLUMN_HEADER_DATA_INDEX,project,root.platform,category));
		headerDataViewer.setLabelProvider(new TableLabelProvider(COLUMN_HEADER_DATA_INDEX));
		
		TableColumn col_type = new TableColumn(table, SWT.LEFT);
		col_type.setText(COLUMN_TYPE);
		col_type.setWidth(FigureUtilities.getTextWidth(COLUMN_TYPE, col_type.getParent().getFont())+50);
		col_type.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));		
		col_type.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn colTypeViewer = new TableViewerColumn(columnsTableViewer, col_type);
		colTypeViewer.setEditingSupport(new ColumnTextEditingSupport(this,columnsTableViewer, COLUMN_TYPE_INDEX,enableGrids));
		colTypeViewer.setLabelProvider(new TableLabelProvider(COLUMN_TYPE_INDEX));
				
		TableColumn col_data = new TableColumn(table, SWT.LEFT);
		col_data.setText(COLUMN);
		col_data.setWidth(FigureUtilities.getTextWidth(COLUMN, col_data.getParent().getFont())+50);
		col_data.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));		
		col_type.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn colDataViewer = new TableViewerColumn(columnsTableViewer, col_data);
		colDataViewer.setEditingSupport(new ColumnHeaderEditingSupport(this,columnsTableViewer,COLUMN_INDEX));
		colDataViewer.setLabelProvider(new TableLabelProvider(COLUMN_INDEX));

		
		TableColumn col_width = new TableColumn(table, SWT.LEFT);
		col_width.setText(COLUMN_WIDTH);
		col_width.setWidth(FigureUtilities.getTextWidth(COLUMN_WIDTH, col_width.getParent().getFont())+50);
		col_width.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));
		col_width.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn col_widthViewer = new TableViewerColumn(columnsTableViewer, col_width);
		col_widthViewer.setEditingSupport(new ColumnTextEditingSupport(this,columnsTableViewer,COLUMN_WIDTH_INDEX,enableGrids));
		col_widthViewer.setLabelProvider(new TableLabelProvider(COLUMN_WIDTH_INDEX));
		
		
		TableColumn col_sort = new TableColumn(table, SWT.LEFT);
		col_sort.setText(COLUMN_SORT);
		col_sort.setWidth(FigureUtilities.getTextWidth(COLUMN_SORT, col_sort.getParent().getFont())+50);
		col_sort.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));
		col_sort.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn col_sortViewer = new TableViewerColumn(columnsTableViewer,col_sort);
		col_sortViewer.setEditingSupport(new SortColumnEditingSupport(this,columnsTableViewer,COLUMN_SORT_INDEX));
		col_sortViewer.setLabelProvider(new TableLabelProvider(COLUMN_SORT_INDEX));
		
		
		TableColumn col_alignment = new TableColumn(table, SWT.LEFT);
		col_alignment.setText(COLUMN_ALIGNMENT);
		col_alignment.setWidth(FigureUtilities.getTextWidth(COLUMN_ALIGNMENT, col_alignment.getParent().getFont())+50);
		col_alignment.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));
		col_alignment.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn col_alignmentViewer = new TableViewerColumn(columnsTableViewer, col_alignment);
		col_alignmentViewer.setEditingSupport(new ColumnAlignmentEditingSupport(this,columnsTableViewer,COLUMN_ALIGNMENT_INDEX));
		col_alignmentViewer.setLabelProvider(new TableLabelProvider(COLUMN_ALIGNMENT_INDEX));
		
		TableColumn colOnClick = new TableColumn(table, SWT.LEFT);
		colOnClick.setText(COLUMN_ON_CLICK);
		colOnClick.setWidth(FigureUtilities.getTextWidth(COLUMN_ON_CLICK, colOnClick.getParent().getFont())+50);
		colOnClick.setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));
		colOnClick.addListener(SWT.Selection,new SortListener(columnsTableViewer));
		TableViewerColumn colOnClickViewer = new TableViewerColumn(columnsTableViewer, colOnClick);
		colOnClickViewer.setEditingSupport(new OnClickEditingSupport(this,columnsTableViewer,COLUMN_ON_CLICK_INDEX));
		colOnClickViewer.setLabelProvider(new TableLabelProvider(COLUMN_ON_CLICK_INDEX));
		
	}

	private void createRowsTab(CTabFolder tabFolder) {
		rowsTab = new CTabItem(tabFolder, SWT.NONE);
		rowsTab.setText("Rows");
		
		rowsComp = new Composite(tabFolder, SWT.NULL);
		rowsComp.setLayoutData(new GridData(GridData.FILL_BOTH));
		rowsStackLayout = new StackLayout();
 		rowsComp.setLayout(rowsStackLayout);
 		
 		rowsTab.setControl(rowsComp);
 		
 		updateRowsStackLayout();
 		
		rowsNoneGrp = new Composite(rowsComp, SWT.NULL);
		GridLayout layout = new GridLayout();
		rowsNoneGrp.setLayout(layout);
		rowsNoneGrp.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		
		Label errorLabel = new Label(rowsNoneGrp, SWT.NONE);
		errorLabel.setText("No columns have been defined.");
		errorLabel.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));

		rowsTableComp = new Composite(rowsComp, SWT.NONE);
		layout = new GridLayout(2,false);
		GridData data = new GridData(GridData.FILL_BOTH);
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = true;
		rowsTableComp.setLayoutData(data);
		rowsTableComp.setLayout(layout);
		
		TableWithButtons tableWithBtns = new TableWithButtons(rowsTableComp, SWT.BORDER | SWT.FULL_SELECTION);
		tableWithBtns.createControl();
		TableViewer v = tableWithBtns.getViewer();
		tableWithBtns.getActionsManager().add(new AddNewRowDynamicTableAction(v,"Add",TableUtils.DATAGRID){
			protected Object createNewRow(String firstCellVal) {
				Object createNewRow = super.createNewRow(firstCellVal);
				modifiedTableData.setRows(getUpdatedChoiceData(false));
				return createNewRow;
			}
		});
		tableWithBtns.getActionsManager().add(new DeleteRow(v,"Delete"){
			@Override
			protected void deleteRows(IStructuredSelection selection) {
				super.deleteRows(selection);
				modifiedTableData.setRows(getUpdatedChoiceData(false));
			}
		});
		tableWithBtns.getActionsManager().add(new MoveUpAction(v,"MoveUp"){
			@Override
			protected void moveRowUp(Object rowData) {
				super.moveRowUp(rowData);
				modifiedTableData.setRows(getUpdatedChoiceData(false));
			}
		});
		tableWithBtns.getActionsManager().add(new MoveDownAction(v,"MoveDown"){
			@Override
			protected void moveRowDown(Object object) {
				super.moveRowDown(object);
				modifiedTableData.setRows(getUpdatedChoiceData(false));
			}	
		});
		tableWithBtns.getActionsManager().add(new InsertBeforeAction(v,"Insert Before",TableUtils.DATAGRID){
			@Override
			protected void insertBefore(int idx) {
				super.insertBefore(idx);
				modifiedTableData.setRows(getUpdatedChoiceData(false));
			}
		});
		tableWithBtns.getActionsManager().add(new InsertAfterAction(v,"Insert After",TableUtils.DATAGRID){
			@Override
			public void insertAfter(int idx) {
				super.insertAfter(idx);
				modifiedTableData.setRows(getUpdatedChoiceData(false));
			}
		});
		
		rowsTableViewer = tableWithBtns.getViewer();
		rowsTableViewer.getTable().setLinesVisible(true);
		rowsTableViewer.getTable().setHeaderVisible(true);
		rowsTableViewer.setData(COLUMNS_INFO_KEY, columnsTableViewer);
		data = new GridData(GridData.FILL_BOTH);
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = true;
		rowsTableViewer.getTable().setLayoutData(data);

		rowsTableViewer.setContentProvider(new IStructuredContentProvider() {
			public Object[] getElements(Object input) {
				if (input instanceof ArrayList) {
					ArrayList<ArrayList> list = (ArrayList<ArrayList>)input;
					return list.toArray(new ArrayList[list.size()]);
				}
				return null;
			}
			
			public void dispose() {}
			
			public void inputChanged (Viewer viewer, Object obj, Object obj1) {	}
		});
		
		updateRowsData();
		
		tableWithBtns.getActionsManager().update(true);
	}

	void updateRowsData() {
		//remove all old columns
		if(rowsTableViewer != null){
			TableColumn[] tableColumns = rowsTableViewer.getTable().getColumns();
			for(TableColumn col : tableColumns){
				col.dispose();
			}
		}
		ArrayList<ColumnData> columns = modifiedTableData.getColumns().getColList();
		ArrayList<ArrayList> rows = modifiedTableData.getRows().getTablerowData();
		ArrayList<Object> headers = new ArrayList<Object>();
		if(rows != null && rows.size()>0) {
			headers = rows.get(0);
		}
		ArrayList<ArrayList> rowList = new ArrayList<ArrayList>();
		if(rows.size() > 1) {
			rowList.addAll(rows);
			rowList.remove(0);
		}
		newcolnames = new ArrayList<String>();
		for(int i=0; i<columns.size();i++) {
			ColumnData coldata = columns.get(i);
			String colname = coldata.colHeader;
			String colId = coldata.id;
			int type = coldata.colType;
			newcolnames.add(colId);
			TableViewerColumn column = new TableViewerColumn(rowsTableViewer, SWT.NONE);
			column.getColumn().setAlignment(SWT.LEFT);
			column.getColumn().setWidth(FigureUtilities.getTextWidth(colname, column.getColumn().getParent().getFont()) + 50);
			column.getColumn().setText(colname);
			column.getColumn().setImage(ImageDescriptorRegistryUtils.getImage(ImageDescriptorConstants.sortableColumn));
			column.getColumn().setMoveable(false);					
			if(type == ColumnData.TEXT_ROW_TYPE) {				//Text
				column.setEditingSupport(new TextEditingSupport(rowsTableViewer, i){
					@Override
					protected void setValue(Object element, Object value) {
						super.setValue(element, value);
						//Do this so that model is changed as soon as the viewer is changed, as the model and viewer-input is not same object
						//See the table building logic for more details
						modifiedTableData.setRows(getUpdatedChoiceData(false));
					}
				});
				column.setLabelProvider(new TextLabelProvider(i));
				column.getColumn().addListener(SWT.Selection,new DynamicSortListener(rowsTableViewer));
			} else if(type == ColumnData.IMAGE_ROW_TYPE) {				//Image
				column.setEditingSupport(new MultipleImageEditingSupport(rowsTableViewer, i, project.getName(), mobileChannel, TableUtils.DATAGRID, category) {
					@Override
					protected void setValue(Object element, Object value) {
						super.setValue(element, value);
						//Do this so that model is changed as soon as the viewer is changed, as the model and viewer-input is not same object
						//See the table building logic for more details
						modifiedTableData.setRows(getUpdatedChoiceData(false));
						validatePage(false);
					}
				});
				column.setLabelProvider(new ImageLabelProvider(i));		
				column.getColumn().addListener(SWT.Selection,new DynamicSortListener(rowsTableViewer));
			} else if(type == ColumnData.TEMPLATE_ROW_TYPE) {				//GridTemplate
				column.setEditingSupport(new TemplateEditingSupport(rowsTableViewer, i));
				column.setLabelProvider(new TextLabelProvider(i));		
				column.getColumn().addListener(SWT.Selection,new DynamicSortListener(rowsTableViewer));
			}
		}

		String col;
		int baseidx;
		ArrayList<Object> newRow = null;
		ArrayList<ArrayList> newRowList = new ArrayList<ArrayList> ();
		ArrayList<Object> t_row;
		for(int idx = 0; idx < newcolnames.size(); idx++) {
			col = newcolnames.get(idx);
			if (headers.contains(col)) {
				baseidx = headers.indexOf(col);							

				for(int i=0; i < rowList.size();i++) {
					t_row = rowList.get(i);

					if (newRowList.size() >= (i +1)) {
						newRow = newRowList.get(i);
					} else {
						newRow = new ArrayList<Object>();
						newRowList.add(newRow);
					}
					Object element = t_row.get(baseidx);
					addRowObject(newRow, idx, element,columns.get(idx));
				}

			} else {
				for(int i=0; i<rowList.size(); i++) {
					t_row = rowList.get(i);
					
					if (newRowList.size() >= (i +1)) {
						newRow = newRowList.get(i);
					} else {
						newRow = new ArrayList<Object>();
						newRowList.add(newRow);
					}
					addRowObject(newRow, idx, "",columns.get(idx));//TODO check for the type of the column and add default values accordingly.
				}
			}	
		}
		for(ArrayList obj : newRowList){
			obj.add(obj.size(), new EditableTableItem());
		}
		TableViewerEditor.create(rowsTableViewer,new ColumnViewerEditorActivationStrategy(rowsTableViewer),
				ColumnViewerEditor.TABBING_HORIZONTAL|ColumnViewerEditor.TABBING_MOVE_TO_ROW_NEIGHBOR|ColumnViewerEditor.TABBING_VERTICAL);
		rowsTableViewer.setInput(newRowList);
		//modifiedTableData.setRows(getUpdatedChoiceData(false));
	}

	private void addRowObject(ArrayList<Object> newRow, int idx, Object element, ColumnData columnData) {
		if(columnData != null) {
			if(columnData.getColType() == ColumnData.TEMPLATE_ROW_TYPE) {
				if(element instanceof TemplateData) {
					TemplateData rowTemplateData = (TemplateData)element;
					if(!rowTemplateData.getGridTemplateName().equals(columnData.getRowTemplate())) {
						element = new TemplateData();
					}
				} else {
					element = new TemplateData();
				}
			} else {
				if(!(element instanceof String)) {
					element = KUtils.EMPTY_STRING;
				}
			}
			newRow.add(idx, element);
			return;
 		}
		newRow.add(idx, KUtils.EMPTY_STRING);
	}

	private void updateRowsStackLayout() {
		if (modifiedTableData.getColumns().getColumnsCount() == 0) {
			rowsStackLayout.topControl = rowsNoneGrp;
		} else {
			rowsStackLayout.topControl = rowsTableComp;
		}
		rowsComp.layout();
	}
	

	public TableData getModifiedData() {
		return modifiedTableData;
	}
	
	@Override
	protected void okPressed() {
		//This is done so that the order in the rows is same as the order of the columns.
		//This should be so for the figure to render properly in the form and for xml generation.
		updateRowsData();
		modifiedTableData.setColumns(getUpdatedColumns());
		modifiedTableData.setRows(getUpdatedChoiceData(true));
		
		if (modifiedTableData.equals(tableData)) {
			modifiedTableData = tableData;
		}
		super.okPressed();
	}
	
	/**
	 * 
	 * @param updateRows Whether the rows table should be rebuilt
	 * @return true if page is valid
	 */
	boolean validatePage(boolean updateRows){
		setErrorMessage(null);
		
		boolean flag = true;
		if(!validateColumns())
			flag = false;
		if(flag && !validateRows(updateRows)){
			flag = false;
		}
		setPageComplete(flag);
		return flag;
	}

	/**
	 * Avoid calling this method. Call {@link TableDataDialog#validatePage(boolean)} instead
	 * 
	 * @return true if column data is valid
	 */
	private boolean validateColumns() {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		ArrayList<ColumnData> colList  = (ArrayList)columnsTableViewer.getInput();
		boolean flag = true;
		if (colList.size() == 0) {
			setErrorMessage("Columns are mandatory for datagrid");
			flag = false;
		} else {
			for(ColumnData data : colList)
			{
				if(data.getId() == null || data.getId().trim().equals(KUtils.EMPTY_STRING)){
					setErrorMessage("Columns marked as * cannot be empty");
					return false;
				} else if(!KUtils.isValidVariableName(data.getId())){
					setErrorMessage(data.getId() + " is not a valid string");
					return false;
				} else if(!isGridColumnTemplateExists(data.getColHeaderType(),data.getColHeader())) {
					setErrorMessage("The header template " + "'" + data.getColHeader() + "'" + " does not exist");
					return false;
				} else if(!isGridRowTemplateExists(data.getColType(),data.getRowTemplate())) {
					setErrorMessage("The row template " + "'" + data.getRowTemplate() + "'" + " does not exist");
					return false;
				}
				for(ColumnData data2 : colList){
					if(data != data2){
						if(data.getId().equals(data2.getId())){
							setErrorMessage("Duplicate Id \'" + data.getId() + "\'");
							return false;
						}
					}
				}
			}
		}
		return flag;
	}
	

	/**
	 * Avoid calling this method. Call {@link TableDataDialog#validatePage(boolean)} instead
	 * 
	 * @return true if row data is valid
	 */
	private boolean validateRows(boolean updateRows){
		updateRowsData();
		boolean flag = true;
		List<ArrayList> tablerowData = modifiedTableData.getRows().getTablerowData();
		List<ColumnData> colList = modifiedTableData.getColumns().getColList();
		List<Integer> imageCols = new ArrayList<Integer>();
		List<Integer> gridCols = new ArrayList<Integer>();
		for(int i = 0 ; i < colList.size(); i++){
			ColumnData column = colList.get(i);
			try {
				if ((enableGrids ? KUtils.COL_TYPES_FOR_DATAGRID_DESKTOPWEB
						: KUtils.COL_TYPES)[column.getColType()]
						.equals(KUtils.IMAGE_TYPE)) {
					imageCols.add(i);
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				KEditorPlugin.logError(e.getMessage(), e);
			}
			try {
				if ((enableGrids ? KUtils.COL_TYPES_FOR_DATAGRID_DESKTOPWEB
						: KUtils.COL_TYPES)[column.getColType()]
						.equals(KUtils.GRID_TYPE)) {
					gridCols.add(i);
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				KEditorPlugin.logError(e.getMessage(), e);
			}
		}
		for(int i = 1; i<tablerowData.size(); i++){
			List arrayList = tablerowData.get(i);
			for(Integer colIndex : imageCols){
				if(arrayList != null && colIndex < arrayList.size()){
					Object imageName = arrayList.get(colIndex);
					if(imageName instanceof String){
						String img = (String) imageName;
						flag = img.trim().isEmpty() || ValidationUtil.isValidImageName(img) ;
						if(!flag){
							setErrorMessage("The image '" + imageName + "'  in column '" + colList.get(colIndex).colHeader + "' is invalid");
							break;
						}
					}
				}
			}
			/*for(Integer colIndex : gridCols){
				if(arrayList != null && colIndex < arrayList.size()){
					Object gridname = arrayList.get(colIndex);
					if(gridname instanceof String){
						flag = grids.contains(gridname);
						if(!flag){
							setErrorMessage("The grid '" + gridname + "'  in column '" + colList.get(colIndex).colHeader + "' is invalid");
							break;
						}
					}
				}
			}*/
		}
		return flag;
	}
	
	public void setPageComplete(boolean b){
		getButton(Window.OK).setEnabled(b);
	}
	
	private class TextEditingSupport extends EditingSupport {
		
		private TextCellEditor cellEditor;
		private int index = 0;

		public TextEditingSupport(TableViewer viewer, int idx) {
			super(viewer);
			cellEditor = new TextCellEditor(viewer.getTable());
			index = idx;
			/*cellEditor.getControl().addTraverseListener(
					new CustomListener(viewer, cellEditor, idx));
			cellEditor.getControl().addKeyListener(
					new CustomListener(viewer, cellEditor, idx));*/
		}

		protected boolean canEdit(Object element) {
			if(element == PotentialNewRowData.INSTANCE) {
				if(index != 0)
					return false;
			}
			return true;
		}

		protected CellEditor getCellEditor(Object element) {
			return cellEditor;
		}

		protected Object getValue(Object element) {
			if (!(element instanceof PotentialNewRowData || element instanceof EditableTableItem)) {
				Object value = ((ArrayList) element).get(index);
				if(value != null && value instanceof String) {
					return value;
				}
			}
			return "";
		}

		protected void setValue(Object element, Object value) {
			if (!(element instanceof PotentialNewRowData)) {
				((ArrayList) element).set(index, value);
				getViewer().update(element, null);
			} else {
				Object input = rowsTableViewer.getInput();
				if(input instanceof ArrayList<?>){
					ArrayList<Object>  inputData = (ArrayList<Object>) input;
					ArrayList newData = TableUtils.addGridData(rowsTableViewer);
					newData.set(index, value);
					inputData.add(newData);
					rowsTableViewer.setInput(inputData);
					rowsTableViewer.getTable().select(inputData.size()-1);
				}
			}
		}
	}
	
	/**
	 * This is a common editing support for all types of Row.
	 * 
	 * @author Rakesh
	 * 
	 */
	private class TemplateEditingSupport extends EditingSupport {
		
		private int columnIndex = 0;
		private TemplateDataCellEditor editor ;

		public TemplateEditingSupport(TableViewer viewer, int idx) {
			super(viewer);
			columnIndex = idx;
			editor = new TemplateDataCellEditor((Composite)viewer.getControl(), project,KPage.GRID_MODE,category, root.platform);
		}

		protected boolean canEdit(Object element) {
			if(element == PotentialNewRowData.INSTANCE) {
				if(columnIndex == 0) {
					return true;
				}
				return false;
			}
			String rowTemplate = getRowTemplate();
			if(rowTemplate.isEmpty() || rowTemplate.equals(KUtils.DEFAULT_NONE)) {
				setMessage("Please define row template for " + "'" + getColumnId() + "'",IMessageProvider.INFORMATION);
				return false;
			} else {
				if(!isGridTemplateExists(rowTemplate)) {
					setMessage("The row template " + "'" + rowTemplate + "'" + " of " + "'" + getColumnId() + "'" + " does not exist",IMessageProvider.ERROR);
					return false;
				}
			}
			return true;
		}

		protected CellEditor getCellEditor(final Object element) {
			Columns columns = modifiedTableData.getColumns();
			ColumnData columnData = columns.getColList().get(columnIndex);
			String rowTemplate = KUtils.EMPTY_STRING;
			if(columnData != null) {
				if(columnData.getRowTemplate() != null)
					rowTemplate = columnData.getRowTemplate();
			}
			editor.setTemplateName(rowTemplate);
			return editor;
		}


		protected Object getValue(Object element) {
			if (!(element instanceof PotentialNewRowData || element instanceof EditableTableItem)) {
				Object value = ((ArrayList) element).get(columnIndex);
				if(value instanceof TemplateData) {
					return value;
				}
			}
			return new TemplateData();
		}

		protected void setValue(Object element, Object value) {
			if (!(element instanceof PotentialNewRowData)) {
				((ArrayList) element).set(columnIndex, value);
				rowsTableViewer.update(element, null);
			} else if(element instanceof PotentialNewRowData) {
				ArrayList newData = TableUtils.addGridData(rowsTableViewer);
				newData.set(columnIndex, value);
				Object input = getViewer().getInput();
				if(input instanceof ArrayList<?>){
					ArrayList<Object>  inputData = (ArrayList<Object>) input;
					inputData.add(newData);
					rowsTableViewer.setInput(inputData);
					rowsTableViewer.getTable().select(inputData.size()-1);
				}
			}
			// Do this so that model is changed as soon as the viewer is
			// changed, as the model and viewer-input is not same object
			// See the table building logic for more details
			modifiedTableData.setRows(getUpdatedChoiceData(false));
//			validatePage(false);
		}
		
		private String getRowTemplate() {
			Columns columns = modifiedTableData.getColumns();
			ColumnData columnData = columns.getColList().get(columnIndex);
			String rowTemplate = KUtils.EMPTY_STRING;
			if(columnData != null) {
				if(columnData.getRowTemplate() != null)
					rowTemplate = columnData.getRowTemplate();
			}
			return rowTemplate;
		}
		
		private String getColumnId() {
			Columns columns = modifiedTableData.getColumns();
			ColumnData columnData = columns.getColList().get(columnIndex);
			return columnData.getId();
		}
	}
	
	private class CustomListener extends CustomListeners{
		private TableViewer viewer;
		protected CustomListener(TableViewer viewer, CellEditor cellEditor,
				int idx) {
			super(viewer, cellEditor, idx);
			this.viewer = viewer;
		}

		@Override
		protected Object createnewRow(String cellVal) {
			ArrayList<ArrayList> inputList  = (ArrayList<ArrayList>) viewer.getInput();
	    	int colCnt = viewer.getTable().getColumnCount();
	    	ArrayList row = new ArrayList();
	    	for (int i = 0; i < colCnt; i++) {
	    		row.add("");
	    	}
	    	row.add(new EditableTableItem());
	    	if(inputList == null) {
	    		inputList = new ArrayList<ArrayList> ();
	    	}
	    	inputList.add(row);
	    	viewer.setInput(inputList);
	    	viewer.getTable().select(inputList.size()-1);
			return row;	
		}
	}
	
	private class TextLabelProvider extends ColumnLabelProvider {
		private int index = 0;
		public TextLabelProvider(int idx ) {
			super();
			index = idx;
		}
		public String getText(Object element) {
			String result="";
			if ((element == PotentialNewRowData.INSTANCE) && (index == 0)) {
	            result = "..."; //$NON-NLS-1$
	        } 
			else if (element instanceof ArrayList) {
				Object ele = ((ArrayList)element).get(index);
				if(ele instanceof TemplateData) {
					return ((TemplateData)ele).toDefinedString();
				}
				result = ele.toString();
			}
			return result;
		}
		
	}
	
	private ChoiceData getUpdatedChoiceData(boolean syncWithDataStructure) {
		ChoiceData newChData = new ChoiceData(KUtils.TABLE_ROW_TYPE);
		ArrayList<ArrayList> newChoices = new ArrayList<ArrayList>();
		ArrayList<ArrayList> choicesList  = (ArrayList)rowsTableViewer.getInput();
		if(choicesList != null) {
			for (Iterator<ArrayList> itr = choicesList.iterator(); itr.hasNext(); ) {
				ArrayList<?> data = itr.next();
				//Each row has an EditableTableItem at as the last item. Remove this when the dialog is returning the final data
				if(syncWithDataStructure)
					data.remove(data.size()-1);
				newChoices.add(data);
			}
		}
		newChoices.add(0,newcolnames);
		if(syncWithDataStructure) {
			newChData.setNewTableRowData(ListOfObject.convertToListOfObjectList(newChoices));
		} else {
			newChData.setTablerowData(newChoices);
		}
		return newChData;
	}
	
	private Columns getUpdatedColumns() {
		Columns newColumns = new Columns();
		ArrayList<ColumnData> newColumnList = new ArrayList<ColumnData>();
		ArrayList<ColumnData> colList  = (ArrayList)columnsTableViewer.getInput();
		for (Iterator<ColumnData> itr = colList.iterator(); itr.hasNext(); ) {
			ColumnData data = itr.next();
			newColumnList.add(data.getCopy(true));
		}
		newColumns.setColList(newColumnList);
		return newColumns;
	}
	
	void renameColIdsInRowData(String oldHeader, String newHeader){
		ArrayList<ArrayList> tablerowData = modifiedTableData.getRows().getTablerowData();
		if(tablerowData.size()>0){
			List<String> ids = tablerowData.get(0);
			for (int i = 0; i < ids.size(); i++) {
				String string = ids.get(i);
				if(oldHeader.equals(string)){
					ids.set(i, newHeader);
					break;
				}
			}
		}
	}
	
	/**
	 * @author Rakesh
	 *
	 */
	private class TableLabelProvider extends ColumnLabelProvider {
		
		private int column;
		
		private TableLabelProvider(int column){
			this.column = column;
		}
		
		@Override
		public String getText(Object element) {
			return getColumnText(element,column);
		}
		
		public String getColumnText(Object element, int columnIndex) {
			if (element == PotentialNewRowData.INSTANCE) {
	            if(columnIndex == 0)
	            	return "...";
	            return "";
	        } 
			ColumnData data = (ColumnData)element;
			switch(columnIndex) {
				case COLUMN_ID_INDEX:
					return data.id;
				case COLUMN_HEADER_TYPE_INDEX:
					if(enableGrids) {
						return KUtils.COL_HEADER_TYPES_FOR_DATAGRID_DESKTOPWEB[data.colHeaderType];
					} else {
						return KUtils.COL_HEADER_TYPES_FOR_DATAGRID[data.colHeaderType];
					}
				case COLUMN_HEADER_INDEX:
					return data.colHeader;
				case COLUMN_HEADER_DATA_INDEX:
					String columnHeaderDataString = KUtils.EMPTY_STRING;
					if(!(data.colHeader.isEmpty() || data.colHeader.equals(KUtils.DEFAULT_NONE))) {
						if(data.getColHeaderType() == ColumnData.TEMPLATE_HEADER_TYPE) {
							Object colHeaderData = data.getColHeaderData();
							if(colHeaderData instanceof TemplateData) {
								return ((TemplateData)colHeaderData).toDefinedString();
							}
						}
					}
					return columnHeaderDataString;
				case COLUMN_TYPE_INDEX:
					try {
						if(enableGrids) {
							return KUtils.COL_TYPES_FOR_DATAGRID_DESKTOPWEB[data.colType];
						} else {
							return KUtils.COL_TYPES_FOR_DATAGRID[data.colType];
						}
					} catch (ArrayIndexOutOfBoundsException e) {
						return KUtils.COL_TYPES_FOR_DATAGRID[0];
					}
				case COLUMN_INDEX:
					String column = KUtils.EMPTY_STRING;
					if(data.getColType() == ColumnData.TEMPLATE_ROW_TYPE) {
						column = data.getRowTemplate();
					}
					return column;
				case COLUMN_WIDTH_INDEX:
					return String.valueOf(data.colWidth);						
				case COLUMN_SORT_INDEX:
					return String.valueOf(data.getSort());
				case COLUMN_ALIGNMENT_INDEX:
					return KUtils.CONTENT_ALIGNS[data.getAlign()];
				case COLUMN_ON_CLICK_INDEX:
					return data.getEvent_onClick().toString();
				case 10:
					return String.valueOf(data.showOnly);
				case 11:
					return String.valueOf(data.visible);
				default:
					return "Invalid column: " + columnIndex;
			}
		}
	}
	
	/**
	 * @author Rakesh
	 */
	private class OnClickEditingSupport extends EditingSupport {
		
		private NScriptCellEditor nScriptCellEditor;
		private TableDataDialog tableDataDialog;
		
		public OnClickEditingSupport(TableDataDialog tableDataDialog, ColumnViewer viewer, int columnIndex){
			super(viewer);
			Composite parent = (Composite) viewer.getControl();
			nScriptCellEditor =  new NScriptCellEditor(parent,((ArrayList)formList),((ArrayList)functionList),root,eventType,kTable, root.getProjectName()); 
			this.tableDataDialog = tableDataDialog;
		}
		
		public OnClickEditingSupport(ColumnViewer viewer) {
			super(viewer);
		}

		@Override
		protected CellEditor getCellEditor(Object element) {
			return nScriptCellEditor;
		}

		@Override
		protected boolean canEdit(Object element) {
			return true;
		}

		@Override
		protected Object getValue(Object element) {
			if(element instanceof ColumnData){
				ColumnData cData = (ColumnData) element;
				return cData.getEvent_onClick();
			}
			return null;
		}

		@Override
		protected void setValue(Object element, Object value) {
			if(element instanceof ColumnData){
				ColumnData cData = (ColumnData) element;
				cData.setEvent_onClick((EventRoot)value);
				validatePage(true);
				getViewer().refresh(cData);
			}
		}
	}
	
	private boolean isGridTemplateExists(String templateName) {
		if(enableGrids) {
			IFolder folder = project.getFolder(NavigationViewConstants.FOLDER_TEMPLATES+File.separator+FOLDER_GRIDS + File.separator + FOLDER_DESKTOP);
			IFile templateFile = folder.getFile(KUtils.getFormNameWithExtension(templateName));
			if(!templateFile.exists()) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isGridColumnTemplateExists(int colHeaderType,String colHeader) {
		if(colHeaderType == ColumnData.TEMPLATE_HEADER_TYPE) {
			if(!(colHeader.isEmpty() || colHeader.equals(KUtils.DEFAULT_NONE)))
				return isGridTemplateExists(colHeader);
		}
		return true;
	}
	
	private boolean isGridRowTemplateExists(int colType, String rowTemplate) {
		if(colType == ColumnData.TEMPLATE_ROW_TYPE) {
			if(!(rowTemplate.isEmpty() || rowTemplate.equals(KUtils.DEFAULT_NONE)))
				return isGridTemplateExists(rowTemplate);
		}
		return true;
	}
}
