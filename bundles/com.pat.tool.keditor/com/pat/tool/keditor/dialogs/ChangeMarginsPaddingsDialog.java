package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.pat.tool.keditor.editors.help.ContextSensitiveHelpConstants;
import com.pat.tool.keditor.editors.help.ContextSensitiveHelpUtil;
import com.pat.tool.keditor.model.IWidget;
import com.pat.tool.keditor.model.WidgetUtil.Widget;
import com.pat.tool.keditor.platformselection.PlatformSelectionComposite;
import com.pat.tool.keditor.propertyDescriptor.MarginUnit;
import com.pat.tool.keditor.utils.RendererHashMapData;
import org.eclipse.swt.custom.ScrolledComposite;

public class ChangeMarginsPaddingsDialog extends TitleAreaDialog{

	private Text[] srcValues = new Text[4];
	private Text[] targetValues = new Text[4];
	private Button selectWidget;
	private Button marginButton;
	private Button paddingButton;
	private Button ignoreSrcButton;
	private PlatformSelectionComposite platSelComp;
	
	private RendererHashMapData renderData = new RendererHashMapData(false);
	private ArrayList<IWidget> widgetsList = new ArrayList<IWidget>();

	private MarginUnit srcMarginUnit;
	private MarginUnit targetMarginUnit;
	private int propertyName;
	private boolean ignoresource;

	public ChangeMarginsPaddingsDialog(Shell parentShell) {
		super(parentShell);
		setShellStyle(getShellStyle() | SWT.RESIZE); 
		setHelpAvailable(false);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		setTitle("Replace Margins/Paddings in project");
		getShell().setText("Replace Margins/Paddings");
		
		Composite control = (Composite) super.createDialogArea(parent);
		
		final ScrolledComposite scrolledSplashComposite = new ScrolledComposite(control, SWT.V_SCROLL | SWT.BORDER);
		scrolledSplashComposite.setLayoutData(new GridData(GridData.FILL_BOTH));
		
		Composite comp = new Composite(scrolledSplashComposite, SWT.NONE);
		comp.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true));
		
		GridLayout layout = new GridLayout(2,false);
		comp.setLayout(layout);
		
		Composite topComp = new Composite(comp, SWT.NONE);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		topComp.setLayoutData(data);
		layout = new GridLayout(5,false);
		topComp.setLayout(layout);
		
		Label dum = new Label(topComp, SWT.NONE);
		
		Label top = new Label(topComp, SWT.NONE);
		top.setText("Top");
		
		Label right = new Label(topComp, SWT.NONE);
		right.setText("Right");
		
		Label bottom = new Label(topComp, SWT.NONE);
		bottom.setText("Bottom");
		
		Label left = new Label(topComp, SWT.NONE);
		left.setText("Left");
		
		Label srcLabel = new Label(topComp, SWT.NONE);
		srcLabel.setText("Find value ");
		
		for(int i=0; i<4; i++) {
			srcValues[i] = new Text(topComp, SWT.BORDER);
			data = new GridData(GridData.FILL_HORIZONTAL);
			srcValues[i].setLayoutData(data);
			srcValues[i].setText("0");
			srcValues[i].addListener(SWT.Modify, NameModifyListener);
		}
		
		Label targetLabel = new Label(topComp, SWT.NONE);
		targetLabel.setText("Replace value ");
		
		for(int i=0; i<4; i++) {
			targetValues[i] = new Text(topComp, SWT.BORDER);
			data = new GridData(GridData.FILL_HORIZONTAL);
			targetValues[i].setLayoutData(data);
			targetValues[i].setText("0");
			targetValues[i].addListener(SWT.Modify, NameModifyListener);
		}
		
		Label sp = new Label(topComp, SWT.NONE);
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 5;
		sp.setLayoutData(data);
		
		ignoreSrcButton = new Button(topComp, SWT.CHECK);
		ignoreSrcButton.setText("Ignore source values(This will replace, irrespective of source values)");
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 5;
		ignoreSrcButton.setLayoutData(data);
		ignoreSrcButton.addSelectionListener(new CustomSelectionAdapter());
		
		Group catComp = new Group(comp, SWT.NONE);
		catComp.setText("Criteria");
		GridData data1 = new GridData(GridData.FILL_HORIZONTAL);
		data1.horizontalSpan = 2;
		catComp.setLayoutData(data1);
		layout = new GridLayout(3,false);
		catComp.setLayout(layout);
		
		Label spacer = new Label(catComp, SWT.NONE);
		spacer.setText("Property:            ");
		
		marginButton = new Button(catComp, SWT.CHECK);
		marginButton.setText("Margins      ");
		data1 = new GridData();
		data1.horizontalAlignment = SWT.RIGHT;
		marginButton.setSelection(true);
		
		paddingButton = new Button(catComp, SWT.CHECK);
		paddingButton.setText("Paddings");
		data1 = new GridData();
		data1.horizontalAlignment = SWT.LEFT;
		paddingButton.setLayoutData(data1);
		
		Label widgetType = new Label(catComp, SWT.NONE);
		widgetType.setText("Widget");
		
		selectWidget = new Button(catComp, SWT.PUSH);
		selectWidget.setText("Select widgets");
		data = new GridData(GridData.FILL_HORIZONTAL);
		data.horizontalSpan = 2;
		selectWidget.setLayoutData(data);
		selectWidget.addSelectionListener(new CustomSelectionAdapter());
		selectWidget.setData(new ArrayList<IWidget>());
		
		RendererHashMapData rendererData = new RendererHashMapData(false);
		
		platSelComp = new PlatformSelectionComposite(catComp, SWT.NONE, rendererData);
		data = new GridData(SWT.FILL,SWT.FILL,true,true,3,1);
		platSelComp.setLayoutData(data);
		platSelComp.init();
		platSelComp.addValidationEventListener(new Listener() {
			@Override
			public void handleEvent(Event event) {
				validateAndUpdateDialog();
			}
			
		});
		
		updateComp();
		 
		scrolledSplashComposite.setContent(comp);
		scrolledSplashComposite.setExpandVertical(true);
		scrolledSplashComposite.setExpandHorizontal(true);
		scrolledSplashComposite.setMinSize(comp.computeSize(SWT.DEFAULT, SWT.DEFAULT));
 
		
    	ContextSensitiveHelpUtil.hookHelp(control, ContextSensitiveHelpConstants.COPY_REPLACE_MARGINS_PADDINGS);
		return control;
	}
	
	private Listener NameModifyListener = new Listener() {
		public void handleEvent(Event e) {
			try {
				Integer.parseInt(((Text)(e.widget)).getText().trim());
			} catch(Exception ex) {
				((Text)(e.widget)).setText("0");
			}
			validateAndUpdateDialog();
		}
	};
	
	private class CustomSelectionAdapter extends SelectionAdapter {
        public void widgetSelected(SelectionEvent event) {
        	if(event.widget == ignoreSrcButton) {
        		boolean selection = ignoreSrcButton.getSelection();
        		for(int i=0; i<4; i++) {
        			if(selection) {
        				srcValues[i].setText("0");
        			}
        			srcValues[i].setEnabled(!selection);
        		}
        	} else if(event.widget == selectWidget) {
        		WidgetSelectionDialog wsel = new WidgetSelectionDialog(Display.getCurrent().getActiveShell());
        		wsel.setInitialSelwidgetList((ArrayList<IWidget>) selectWidget.getData());
        		int result = wsel.open();
        		if(result != Window.CANCEL) {
        			widgetsList = wsel.getWidgetList();
        			selectWidget.setData(widgetsList);
        		}
        	}
        	validateAndUpdateDialog();
        }
	};
	
	@Override
	protected void okPressed() {
		if(!validateAndUpdateDialog()) {
			return;
		}
		super.okPressed();
	}

	public boolean validateAndUpdateDialog() {
		String errorMessage = null;
		if(widgetsList.isEmpty()) {
			errorMessage = "At least one widget should be selected";
		}
		
		renderData = platSelComp.getUpdatedRendererHashMapData();
		if(renderData.equals(new RendererHashMapData(false))) {
			errorMessage = "At least one platform should be selected";
		}
		
		if(!(marginButton.getSelection() || paddingButton.getSelection())) {
			errorMessage = "Margins/Paddings should be selected";
		}
		
		ignoresource = ignoreSrcButton.getSelection();
		
		if(marginButton.getSelection() && paddingButton.getSelection()) {
			propertyName = 2;
		} else if(marginButton.getSelection()) {
			propertyName = 0;
		} else {
			propertyName = 1;
		}
		int top = 0;
		int right = 0;
		int bottom = 0;
		int left = 0;
		try {
			top = Integer.parseInt(srcValues[0].getText().trim());
			right = Integer.parseInt(srcValues[1].getText().trim());
			bottom = Integer.parseInt(srcValues[2].getText().trim());
			left = Integer.parseInt(srcValues[3].getText().trim());
		} catch(NumberFormatException ex) {
			top = 0;
			right = 0;
			bottom = 0;
			left = 0;
		}
		srcMarginUnit = new MarginUnit(top, right,bottom,left);
		try {
			top = Integer.parseInt(targetValues[0].getText().trim());
			right = Integer.parseInt(targetValues[1].getText().trim());
			bottom = Integer.parseInt(targetValues[2].getText().trim());
			left = Integer.parseInt(targetValues[3].getText().trim());
		} catch(NumberFormatException ex) {
			top = 0;
			right = 0;
			bottom = 0;
			left = 0;
		}
		targetMarginUnit = new MarginUnit(top, right,bottom,left);
		
		if(!ignoresource && srcMarginUnit.equals(targetMarginUnit)) {
			errorMessage = "Source and target values are same";
		}
		setErrorMessage(errorMessage);
		boolean valid = errorMessage == null;
		getButton(IDialogConstants.OK_ID).setEnabled(valid);
		return valid;
	}

	/**
	 * Updates the Platform Selection composite
	 */
	private void updateComp() {
		platSelComp.setSelection(new RendererHashMapData(false));
		platSelComp.updateButtonsEnabled(new RendererHashMapData(true));
		
		setMessage("Replace Margins/Paddings of selected platforms");
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				validateAndUpdateDialog();
			}
		});
	}

	public RendererHashMapData getRenderData() {
		return renderData;
	}
	
	public ArrayList<IWidget> getWidgetsList() {
		return widgetsList;
	}
	
	public MarginUnit getSrcMarginUnit() {
		return srcMarginUnit;
	}

	public MarginUnit getTargetMarginUnit() {
		return targetMarginUnit;
	}

	public int getPropertyName() {
		return propertyName;
	}

	public boolean isIgnoresource() {
		return ignoresource;
	}
}
