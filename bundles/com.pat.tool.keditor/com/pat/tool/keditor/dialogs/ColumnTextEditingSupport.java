package com.pat.tool.keditor.dialogs;

import java.util.ArrayList;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import com.pat.tool.keditor.table.PotentialNewRowData;
import com.pat.tool.keditor.utils.ColumnData;
import com.pat.tool.keditor.utils.KUtils;

/**
 * This is for all column who has TextCellEditor and ComboboxCellEditor.
 * 
 * @author Rakesh
 * 3 Feb,2012
 */
public class ColumnTextEditingSupport extends EditingSupport {

	private int columnIndex = -1;
	private TextCellEditor textCellEditor;
	private ComboBoxCellEditor columnHeaderTypeCellEditor,colTypeCellEditor;
	private TableDataDialog tableDataDialog;
	//Following values for header_type
	public static final String[] COL_HEADER_TYPES_DESKTOP_WEB = {KUtils.TEXT_TYPE, KUtils.GRID_TYPE};
	public static final String[] COL_HEADER_TYPES = {KUtils.TEXT_TYPE};
	//Following values for column_type
	public static final String[] COL_TYPES_DESKTOP_WEB = {KUtils.TEXT_TYPE,KUtils.IMAGE_TYPE,KUtils.GRID_TYPE};
	public static final String[] COL_TYPES = {KUtils.TEXT_TYPE,KUtils.IMAGE_TYPE};
	
	/**
	 * 
	 * @param tableDataDialog
	 * @param viewer
	 * @param columnIndex
	 * @param enableGrids
	 */
	public ColumnTextEditingSupport(TableDataDialog tableDataDialog, ColumnViewer viewer, int columnIndex,boolean enableGrids) {
		super(viewer);
		Composite parent = (Composite) viewer.getControl();
		textCellEditor = new TextCellEditor(parent,SWT.NONE);
		columnHeaderTypeCellEditor = new ComboBoxCellEditor(parent, enableGrids?KUtils.COL_HEADER_TYPES_FOR_DATAGRID_DESKTOPWEB:KUtils.COL_HEADER_TYPES_FOR_DATAGRID,SWT.READ_ONLY);
		colTypeCellEditor = new ComboBoxCellEditor(parent, enableGrids?KUtils.COL_TYPES_FOR_DATAGRID_DESKTOPWEB:KUtils.COL_TYPES_FOR_DATAGRID,SWT.READ_ONLY);
		this.columnIndex = columnIndex;
		this.tableDataDialog = tableDataDialog;
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		switch(columnIndex){
		case TableDataDialog.COLUMN_HEADER_TYPE_INDEX:
			return columnHeaderTypeCellEditor;
		case TableDataDialog.COLUMN_TYPE_INDEX:
			return colTypeCellEditor;
		}
		return textCellEditor;
	}

	@Override
	protected boolean canEdit(Object element) {
		if(element instanceof ColumnData){
			ColumnData cData = (ColumnData) element;
			switch(columnIndex){
			case TableDataDialog.COLUMN_INDEX:
				if(cData.colType != ColumnData.TEMPLATE_ROW_TYPE){
					return false;
				}
			}
		} else if(element instanceof PotentialNewRowData) {
			if(columnIndex != TableDataDialog.COLUMN_ID_INDEX) 
				return false;
		}
		return true;
	}

	@Override
	protected Object getValue(Object element) {
		if (element instanceof ColumnData) {
			ColumnData data = (ColumnData) element;
			switch (columnIndex) {
			case TableDataDialog.COLUMN_ID_INDEX:
				return data.id;
			case TableDataDialog.COLUMN_HEADER_TYPE_INDEX:
				return data.colHeaderType;
			case TableDataDialog.COLUMN_HEADER_INDEX:
				return data.colHeader;
			case TableDataDialog.COLUMN_TYPE_INDEX:
				return data.colType;
			case TableDataDialog.COLUMN_INDEX:
				return data.getRowTemplate();
			case TableDataDialog.COLUMN_WIDTH_INDEX:
				return String.valueOf(data.colWidth);
			/*case 9:
				return String.valueOf(data.showOnly);
			case 10:
				return String.valueOf(data.visible);*/
			default:
				return "Invalid column: " + columnIndex;
			}
		}
		return "";
	}

	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof ColumnData) {
			ColumnData data = (ColumnData) element;
			String str = value.toString();
			switch (columnIndex) {
			case TableDataDialog.COLUMN_ID_INDEX:
				String oldId = data.id;
				data.id = str;
				String newId = data.id;
				Object colHeaderData = data.getColHeaderData();
				tableDataDialog.validatePage(true);
				tableDataDialog.renameColIdsInRowData(oldId, newId);
				break;
			case TableDataDialog.COLUMN_HEADER_TYPE_INDEX:
				data.colHeaderType = Integer.parseInt(str);
				tableDataDialog.validatePage(true);
				break;
			case TableDataDialog.COLUMN_HEADER_INDEX:
				data.colHeader = str;
				tableDataDialog.validatePage(true);
				break;
			case TableDataDialog.COLUMN_TYPE_INDEX:
				data.colType = Integer.parseInt(str);
				tableDataDialog.validatePage(true);
				break;
			case TableDataDialog.COLUMN_INDEX:
				tableDataDialog.validatePage(true);
				break;
			case TableDataDialog.COLUMN_WIDTH_INDEX:
				data.colWidth = Integer.parseInt(str);
				tableDataDialog.validatePage(true);
				break;
			case 9:
				// return String.valueOf(data.showOnly);
			case 10:
				// return String.valueOf(data.visible);
			default:
				// return "Invalid column: " + columnIndex; }
			}
			getViewer().refresh(data);
		} else if(element  == PotentialNewRowData.INSTANCE) {
			if(columnIndex == TableDataDialog.COLUMN_ID_INDEX) {
				ColumnData data = new ColumnData();
				ArrayList<ColumnData> choicesList  = (ArrayList)getViewer().getInput();
				data.id = (String) value;
				choicesList.add(data);
				getViewer().setInput(choicesList);
				((TableViewer)getViewer()).getTable().select(choicesList.size()-1);
				tableDataDialog.validatePage(true);
			}
		} 
	}

}
