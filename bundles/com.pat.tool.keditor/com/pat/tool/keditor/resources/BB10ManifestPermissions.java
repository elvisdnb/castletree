package com.pat.tool.keditor.resources;

import java.util.Collections;
import java.util.HashSet;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import com.pat.tool.keditor.KEditorPlugin;

public class BB10ManifestPermissions {

	private static final String BUNDLE_NAME = "com.pat.tool.keditor.resources.bb10_properties"; //$NON-NLS-1$
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	private static final int DEFAULT_VALUE_INDEX = 0;
	private static final int DOCUMENTAION_INDEX = 1;
	private static final String BB10_PERMISSIONS_KEY = "bb10_manifest_permissions";
	
	private BB10ManifestPermissions() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	/**
	 * 
	 * @param permissionKey
	 * @return Default value of the permission key
	 */
	public static String getDefaultValue(String permissionKey){
		try {
			String value = getString(permissionKey);
			String[] split = value.split(",");
			return split[DEFAULT_VALUE_INDEX];
		} catch (Exception e){
			KEditorPlugin.logError(e.getMessage(), e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param permissionKey
	 * @return Documentation of the permission key
	 */
	public static String getDocumentation(String permissionKey){
		try {
			String value = getString(permissionKey);
			String[] split = value.split(",");
			return split[DOCUMENTAION_INDEX];
		} catch (Exception e){
			KEditorPlugin.logError(e.getMessage(), e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param permissionKey
	 * @return List of BB10 permission keys
	 */
	public static Set<String> getBB10PermissionKeys(){
		Set<String> set = new HashSet<String>();
		try {
			String value = getString(BB10_PERMISSIONS_KEY);
			String[] split = value.split(",");
			Collections.addAll(set, split);
			return set;
		} catch (Exception e){
			KEditorPlugin.logError(e.getMessage(), e);
			return set;
		}
	}
}
