package com.pat.tool.keditor.resources;

import java.util.Collections;
import java.util.HashSet;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import com.pat.tool.keditor.KEditorPlugin;

public class Wp81Capabilities {

	 
	private static final String BUNDLE_NAME = "com.pat.tool.keditor.resources.wp81s_manifest_capabilities"; //$NON-NLS-1$
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	private static final int DEFAULT_VALUE_INDEX = 0;
	private static final int DOCUMENTAION_INDEX = 1;
	private static final String WP81S_CAPABILITIES_KEY = "wp81s_manifest_capabilities";
	
	private Wp81Capabilities() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	/**
	 * 
	 * @param permissionKey
	 * @return Default value of the capabilities key
	 */
	public static String getDefaultValue(String permissionKey){
		try {
			String value = getString(permissionKey);
			String[] split = value.split(",");
			return split[DEFAULT_VALUE_INDEX];
		} catch (Exception e){
			KEditorPlugin.logError(e.getMessage(), e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param capabilitiesKey
	 * @return Documentation of the capabilities key
	 */
	public static String getDocumentation(String capabilitiesKey){
		try {
			String value = getString(capabilitiesKey);
			String[] split = value.split(",");
			return split[DOCUMENTAION_INDEX];
		} catch (Exception e){
			KEditorPlugin.logError(e.getMessage(), e);
			return null;
		}
	}
	
	/**
	 * 
	 * @param capabilitiesKey
	 * @return List of WINPHONE8.1 capabilities keys
	 */
	public static Set<String> getWP81CapabilitiKeys(){
		Set<String> set = new HashSet<String>();
		try {
			String value = getString(WP81S_CAPABILITIES_KEY);
			String[] split = value.split(",");
			Collections.addAll(set, split);
			return set;
		} catch (Exception e){
			KEditorPlugin.logError(e.getMessage(), e);
			return set;
		}
	}

}
