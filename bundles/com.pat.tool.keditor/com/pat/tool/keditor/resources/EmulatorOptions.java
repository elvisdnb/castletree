package com.pat.tool.keditor.resources;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class EmulatorOptions {
	
	private static final String BUNDLE_NAME = "com.pat.tool.keditor.resources.emulator_options";
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	public static final String ANDROID_MOBILE_OPTS_KEY = "android_mobile_options";
	public static final String NATIVE_APK_FILE_KEY = "Native_APK_file";
	public static final String WRAPPER_APK_FILE_KEY = "Wrapper_APK_file";
	public static final String MIXED_APK_FILE_KEY = "Mixed_APK_file";
	
	
	private static String[] android_mobile_opts;
	private static String native_apk_file;
	private static String wrapper_apk_file;
	private static String mixed_apk_file;
	
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	private static void initializeVersions(){
		String string = getString(ANDROID_MOBILE_OPTS_KEY);
		android_mobile_opts = string.split(",");
		native_apk_file = getString(NATIVE_APK_FILE_KEY);
		wrapper_apk_file = getString(WRAPPER_APK_FILE_KEY);
		mixed_apk_file = getString(MIXED_APK_FILE_KEY);
	}
	
	public static String[] getAndroid_mobile_options() {
		if(android_mobile_opts == null)
			initializeVersions();
		return android_mobile_opts;
	}
	
	public static String getNative_apk_file() {
		if(native_apk_file == null)
			initializeVersions();
		return native_apk_file;
	}
	
	public static String getWrapper_apk_file() {
		if(wrapper_apk_file == null)
			initializeVersions();
		return wrapper_apk_file;
	}
	
	public static String getMixed_apk_file() {
		if(mixed_apk_file == null)
			initializeVersions();
		return mixed_apk_file;
	}
	
}
