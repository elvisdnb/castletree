﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Framework;
using System.Collections;

namespace KNI
{
    public class FFI
    {
        public static void LoadApis()
        {
            //Add list entry here
            //Do not remove above commented line since that is the index point to place the new code
        }

        //Add new method here
        //Do not remove above commented line since that is the index point to place the new code



        #region Helper methods
        private static List<object> LuaTable2Vector(LuaTable luaTable)
        {
            List<object> luaTableArray = new List<object>();

            List<object>.Enumerator arrayEnumFirst = luaTable.getArrayEnumerator();
            arrayEnumFirst.MoveNext();
            while (arrayEnumFirst.MoveNext()) {
                if (arrayEnumFirst.Current is LuaTable) {
                    luaTableArray.Add(LuaTable2Vector(arrayEnumFirst.Current as LuaTable));
                } else {
                    luaTableArray.Add(arrayEnumFirst.Current);
                }
            }

            return luaTableArray;
        }

        private static Dictionary<object, object> LuaTable2HashTable(LuaTable luaTable)
        {
            Dictionary<object, object> htLuaTable = new Dictionary<object, object>();

            IDictionaryEnumerator hashEnumerator = luaTable.getHashEnumerator();
            while (hashEnumerator.MoveNext()) {
                if (hashEnumerator.Value is LuaTable) {
                    htLuaTable.Add(hashEnumerator.Key, LuaTable2HashTable(hashEnumerator.Value as LuaTable));
                } else {
                    htLuaTable.Add(hashEnumerator.Key, hashEnumerator.Value);
                }
            }

            return htLuaTable;
        }

        private static LuaTable Vector2LuaTable(List<object> luaTableArray)
        {
            LuaTable luaTable = new LuaTable(luaTableArray.Count, 0);
            foreach (object item in luaTableArray) {
                if (item is IList) {
                    List<object> luaTableSubArray = item as List<object>;
                    if (null != luaTableSubArray) {
                        luaTable.Add(Vector2LuaTable(luaTableSubArray));
                    }
                } else if (item is IDictionary) {
                    Dictionary<object, object> htSubLuaTable = item as Dictionary<object, object>;
                    if (null != htSubLuaTable) {
                        luaTable.Add(HashTable2LuaTable(htSubLuaTable));
                    }
                } else {
                    luaTable.Add(item);
                }
            }

            return luaTable;
        }

        private static LuaTable HashTable2LuaTable(Dictionary<object, object> htLuaTable)
        {
            LuaTable luaTable = new LuaTable(0, htLuaTable.Count);
            foreach (object item in htLuaTable.Keys) {
                if (htLuaTable[item] is IList) {
                    List<object> luaTableArray = htLuaTable[item] as List<object>;
                    if (null != luaTableArray) {
                        luaTable.setTable(item, Vector2LuaTable(luaTableArray));
                    }
                } else if (htLuaTable[item] is IDictionary) {
                    Dictionary<object, object> htSubLuaTable = htLuaTable[item] as Dictionary<object, object>;
                    if (null != htSubLuaTable) {
                        luaTable.setTable(item, HashTable2LuaTable(htSubLuaTable));
                    }
                } else {
                    luaTable.setTable(item, htLuaTable[item]);
                }
            }

            return luaTable;
        }
        #endregion Helper methods
    }
}
