/*
 * A template file of Firefox 4 excluding a code for ECMAScript
 * 
 * @author shinsuke
 */

// Class
/**
 * Window class.
 * 
 * @class Window
 * @returns {Window}  
 */
function Window() {
	// properties
	/**
	 * @property {MozURLProperty} URL
	 */
	this.URL = {};
	/**
	 * @property {OfflineResourceList} applicationCache
	 */
	this.applicationCache = {};
	/**
	 * @property {Boolean} closed
	 */
	this.closed = true;
	/**
	 * @property {Window} content
	 */
	this.content = {};
	/**
	 * @property {XULControllers} controllers
	 */
	this.controllers = {};
	/**
	 * @property {Crypto} crypto
	 */
	this.crypto = {};
	/**
	 * @property {String} defaultStatus
	 */
	this.defaultStatus = "";
	/**
	 * @property {HTMLDocument} document
	 */
	this.document = {};
	/**
	 * @property {Object} frameElement
	 */
	this.frameElement = {};
	/**
	 * @property {Window} frames
	 */
	this.frames = {};
	/**
	 * @property {Boolean} fullScreen
	 */
	this.fullScreen = true;
	/**
	 * @property {StorageList} globalStorage
	 */
	this.globalStorage = {};
	/**
	 * @property {History} history
	 */
	this.history = {};
	/**
	 * @property {Number} innerHeight
	 */
	this.innerHeight = 1;
	/**
	 * @property {Number} innerWidth
	 */
	this.innerWidth = 1;
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	/**
	 * @property {Storage} localStorage
	 */
	this.localStorage = {};
	/**
	 * @property {Object} location
	 */
	this.location = {};
	/**
	 * @property {BarProp} locationbar
	 */
	this.locationbar = {};
	/**
	 * @property {BarProp} menubar
	 */
	this.menubar = {};
	/**
	 * @property {Number} mozAnimationStartTime
	 */
	this.mozAnimationStartTime = 1;
	/**
	 * @property {Number} mozInnerScreenX
	 */
	this.mozInnerScreenX = 1;
	/**
	 * @property {Number} mozInnerScreenY
	 */
	this.mozInnerScreenY = 1;
	/**
	 * @property {Number} mozPaintCount
	 */
	this.mozPaintCount = 1;
	/**
	 * @property {IDBFactory} moz_indexedDB
	 */
	this.moz_indexedDB = {};
	/**
	 * @property {String} name
	 */
	this.name = "";
	/**
	 * @property {Navigator} navigator
	 */
	this.navigator = {};
	/**
	 * @property {Object} opener
	 */
	this.opener = {};
	/**
	 * @property {Number} outerHeight
	 */
	this.outerHeight = 1;
	/**
	 * @property {Number} outerWidth
	 */
	this.outerWidth = 1;
	/**
	 * @property {Number} pageXOffset
	 */
	this.pageXOffset = 1;
	/**
	 * @property {Number} pageYOffset
	 */
	this.pageYOffset = 1;
	/**
	 * @property {Window} parent
	 */
	this.parent = {};
	/**
	 * @property {BarProp} personalbar
	 */
	this.personalbar = {};
	/**
	 * @property {Object} pkcs11
	 */
	this.pkcs11 = {};
	/**
	 * @property {Screen} screen
	 */
	this.screen = {};
	/**
	 * @property {Number} screenX
	 */
	this.screenX = 1;
	/**
	 * @property {Number} screenY
	 */
	this.screenY = 1;
	/**
	 * @property {Number} scrollMaxX
	 */
	this.scrollMaxX = 1;
	/**
	 * @property {Number} scrollMaxY
	 */
	this.scrollMaxY = 1;
	/**
	 * @property {Number} scrollX
	 */
	this.scrollX = 1;
	/**
	 * @property {Number} scrollY
	 */
	this.scrollY = 1;
	/**
	 * @property {BarProp} scrollbars
	 */
	this.scrollbars = {};
	/**
	 * @property {Window} self
	 */
	this.self = {};
	/**
	 * @property {Window} sessionStorage
	 */
	this.sessionStorage = {};
	/**
	 * @property {String} status
	 */
	this.status = "";
	/**
	 * @property {BarProp} statusbar
	 */
	this.statusbar = {};
	/**
	 * @property {BarProp} toolbar
	 */
	this.toolbar = {};
	/**
	 * @property {Window} top
	 */
	this.top = {};
	/**
	 * @property {Window} window
	 */
	this.window = {};
	// methods
	Window.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	Window.prototype.alert = function(arg1){}
	Window.prototype.atob = function(arg1){}
	Window.prototype.back = function(){}
	Window.prototype.blur = function(){}
	Window.prototype.btoa = function(arg1){}
	Window.prototype.captureEvents = function(arg1){}
	Window.prototype.clearInterval = function(){}
	Window.prototype.clearTimeout = function(){}
	Window.prototype.close = function(){}
	Window.prototype.confirm = function(arg1){}
	Window.prototype.disableExternalCapture = function(){}
	Window.prototype.dispatchEvent = function(arg1){}
	Window.prototype.dump = function(arg1){}
	Window.prototype.enableExternalCapture = function(){}
	Window.prototype.find = function(arg1, arg2, arg3, arg4, arg5, arg6, arg7){}
	Window.prototype.focus = function(){}
	Window.prototype.forward = function(){}
	Window.prototype.getComputedStyle = function(arg1, arg2){}
	Window.prototype.getSelection = function(){}
	Window.prototype.home = function(){}
	Window.prototype.moveBy = function(arg1, arg2){}
	Window.prototype.moveTo = function(arg1, arg2){}
	Window.prototype.mozRequestAnimationFrame = function(arg1){}
	Window.prototype.open = function(arg1, arg2, arg3){}
	Window.prototype.openDialog = function(arg1, arg2, arg3){}
	Window.prototype.postMessage = function(arg1, arg2){}
	Window.prototype.print = function(){}
	Window.prototype.prompt = function(arg1, arg2){}
	Window.prototype.releaseEvents = function(arg1){}
	Window.prototype.removeEventListener = function(arg1, arg2, arg3){}
	Window.prototype.resizeBy = function(arg1, arg2){}
	Window.prototype.resizeTo = function(arg1, arg2){}
	Window.prototype.routeEvent = function(arg1){}
	Window.prototype.scroll = function(arg1, arg2){}
	Window.prototype.scrollBy = function(arg1, arg2){}
	Window.prototype.scrollByLines = function(arg1){}
	Window.prototype.scrollByPages = function(arg1){}
	Window.prototype.scrollTo = function(arg1, arg2){}
	Window.prototype.setInterval = function(){}
	Window.prototype.setResizable = function(arg1){}
	Window.prototype.setTimeout = function(){}
	Window.prototype.showModalDialog = function(arg1, arg2, arg3){}
	Window.prototype.sizeToContent = function(){}
	Window.prototype.stop = function(){}
	Window.prototype.updateCommands = function(arg1){}
}

/**
 * MozURLProperty class.
 * 
 * @class MozURLProperty
 * @returns {MozURLProperty}  
 */
function MozURLProperty() {
	// properties
	// methods
	MozURLProperty.prototype.createObjectURL = function(arg1){}
	MozURLProperty.prototype.revokeObjectURL = function(arg1){}
}

/**
 * OfflineResourceList class.
 * 
 * @class OfflineResourceList
 * @returns {OfflineResourceList}  
 */
function OfflineResourceList() {
	// properties
	/**
	 * @property {Number} CHECKING
	 */
	this.CHECKING = 1;
	/**
	 * @property {Number} DOWNLOADING
	 */
	this.DOWNLOADING = 1;
	/**
	 * @property {Number} IDLE
	 */
	this.IDLE = 1;
	/**
	 * @property {Number} OBSOLETE
	 */
	this.OBSOLETE = 1;
	/**
	 * @property {Number} UNCACHED
	 */
	this.UNCACHED = 1;
	/**
	 * @property {Number} UPDATEREADY
	 */
	this.UPDATEREADY = 1;
	/**
	 * @property {DOMStringList} mozItems
	 */
	this.mozItems = {};
	/**
	 * @property {Number} mozLength
	 */
	this.mozLength = 1;
	/**
	 * @property {Object} oncached
	 */
	this.oncached = {};
	/**
	 * @property {Object} onchecking
	 */
	this.onchecking = {};
	/**
	 * @property {Object} ondownloading
	 */
	this.ondownloading = {};
	/**
	 * @property {Object} onerror
	 */
	this.onerror = {};
	/**
	 * @property {Object} onnoupdate
	 */
	this.onnoupdate = {};
	/**
	 * @property {Object} onobsolete
	 */
	this.onobsolete = {};
	/**
	 * @property {Object} onprogress
	 */
	this.onprogress = {};
	/**
	 * @property {Object} onupdateready
	 */
	this.onupdateready = {};
	/**
	 * @property {Number} status
	 */
	this.status = 1;
	// methods
	OfflineResourceList.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	OfflineResourceList.prototype.dispatchEvent = function(arg1){}
	OfflineResourceList.prototype.mozAdd = function(arg1){}
	OfflineResourceList.prototype.mozHasItem = function(arg1){}
	OfflineResourceList.prototype.mozItem = function(arg1){}
	OfflineResourceList.prototype.mozRemove = function(arg1){}
	OfflineResourceList.prototype.removeEventListener = function(arg1, arg2, arg3){}
	OfflineResourceList.prototype.swapCache = function(){}
	OfflineResourceList.prototype.update = function(){}
}

/**
 * DOMStringList class.
 * 
 * @class DOMStringList
 * @returns {DOMStringList}  
 */
function DOMStringList() {
	// properties
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	// methods
	DOMStringList.prototype.contains = function(arg1){}
	DOMStringList.prototype.item = function(arg1){}
}

/**
 * XULControllers class.
 * 
 * @class XULControllers
 * @returns {XULControllers}  
 */
function XULControllers() {
	// properties
	// methods
	XULControllers.prototype.QueryInterface = function(arg1){}
	XULControllers.prototype.appendController = function(arg1){}
	XULControllers.prototype.getControllerAt = function(arg1){}
	XULControllers.prototype.getControllerById = function(arg1){}
	XULControllers.prototype.getControllerCount = function(){}
	XULControllers.prototype.getControllerForCommand = function(arg1){}
	XULControllers.prototype.getControllerId = function(arg1){}
	XULControllers.prototype.insertControllerAt = function(arg1, arg2){}
	XULControllers.prototype.removeController = function(arg1){}
	XULControllers.prototype.removeControllerAt = function(arg1){}
}

/**
 * Crypto class.
 * 
 * @class Crypto
 * @returns {Crypto}  
 */
function Crypto() {
	// properties
	/**
	 * @property {Boolean} enableSmartCardEvents
	 */
	this.enableSmartCardEvents = true;
	/**
	 * @property {Number} version
	 */
	this.version = 1;
	// methods
	Crypto.prototype.disableRightClick = function(){}
	Crypto.prototype.generateCRMFRequest = function(){}
	Crypto.prototype.importUserCertificates = function(arg1, arg2, arg3){}
	Crypto.prototype.logout = function(){}
	Crypto.prototype.popChallengeResponse = function(arg1){}
	Crypto.prototype.random = function(arg1){}
	Crypto.prototype.signText = function(arg1, arg2){}
}

/**
 * HTMLDocument class.
 * 
 * @class HTMLDocument
 * @returns {HTMLDocument}  
 */
function HTMLDocument() {
	// properties
	/**
	 * @property {Number} ATTRIBUTE_NODE
	 */
	this.ATTRIBUTE_NODE = 1;
	/**
	 * @property {Number} CDATA_SECTION_NODE
	 */
	this.CDATA_SECTION_NODE = 1;
	/**
	 * @property {Number} COMMENT_NODE
	 */
	this.COMMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_FRAGMENT_NODE
	 */
	this.DOCUMENT_FRAGMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_NODE
	 */
	this.DOCUMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINED_BY
	 */
	this.DOCUMENT_POSITION_CONTAINED_BY = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINS
	 */
	this.DOCUMENT_POSITION_CONTAINS = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_DISCONNECTED
	 */
	this.DOCUMENT_POSITION_DISCONNECTED = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_FOLLOWING
	 */
	this.DOCUMENT_POSITION_FOLLOWING = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC
	 */
	this.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_PRECEDING
	 */
	this.DOCUMENT_POSITION_PRECEDING = 1;
	/**
	 * @property {Number} DOCUMENT_TYPE_NODE
	 */
	this.DOCUMENT_TYPE_NODE = 1;
	/**
	 * @property {Number} ELEMENT_NODE
	 */
	this.ELEMENT_NODE = 1;
	/**
	 * @property {Number} ENTITY_NODE
	 */
	this.ENTITY_NODE = 1;
	/**
	 * @property {Number} ENTITY_REFERENCE_NODE
	 */
	this.ENTITY_REFERENCE_NODE = 1;
	/**
	 * @property {Number} NOTATION_NODE
	 */
	this.NOTATION_NODE = 1;
	/**
	 * @property {Number} PROCESSING_INSTRUCTION_NODE
	 */
	this.PROCESSING_INSTRUCTION_NODE = 1;
	/**
	 * @property {Number} TEXT_NODE
	 */
	this.TEXT_NODE = 1;
	/**
	 * @property {String} URL
	 */
	this.URL = "";
	/**
	 * @property {HTMLBodyElement} activeElement
	 */
	this.activeElement = {};
	/**
	 * @property {String} alinkColor
	 */
	this.alinkColor = "";
	/**
	 * @property {HTMLCollection} anchors
	 */
	this.anchors = {};
	/**
	 * @property {HTMLCollection} applets
	 */
	this.applets = {};
	/**
	 * @property {Object} attributes
	 */
	this.attributes = {};
	/**
	 * @property {String} baseURI
	 */
	this.baseURI = "";
	/**
	 * @property {String} bgColor
	 */
	this.bgColor = "";
	/**
	 * @property {HTMLBodyElement} body
	 */
	this.body = {};
	/**
	 * @property {String} characterSet
	 */
	this.characterSet = "";
	/**
	 * @property {NodeList} childNodes
	 */
	this.childNodes = {};
	/**
	 * @property {String} compatMode
	 */
	this.compatMode = "";
	/**
	 * @property {String} contentType
	 */
	this.contentType = "";
	/**
	 * @property {String} cookie
	 */
	this.cookie = "";
	/**
	 * @property {HTMLScriptElement} currentScript
	 */
	this.currentScript = {};
	/**
	 * @property {Window} defaultView
	 */
	this.defaultView = {};
	/**
	 * @property {String} designMode
	 */
	this.designMode = "";
	/**
	 * @property {String} dir
	 */
	this.dir = "";
	/**
	 * @property {Object} doctype
	 */
	this.doctype = {};
	/**
	 * @property {HTMLHtmlElement} documentElement
	 */
	this.documentElement = {};
	/**
	 * @property {String} documentURI
	 */
	this.documentURI = "";
	/**
	 * @property {String} domConfig
	 */
	this.domConfig = "";
	/**
	 * @property {String} domain
	 */
	this.domain = "";
	/**
	 * @property {HTMLCollection} embeds
	 */
	this.embeds = {};
	/**
	 * @property {String} fgColor
	 */
	this.fgColor = "";
	/**
	 * @property {HTMLHtmlElement} firstChild
	 */
	this.firstChild = {};
	/**
	 * @property {HTMLCollection} forms
	 */
	this.forms = {};
	/**
	 * @property {HTMLHeadElement} head
	 */
	this.head = {};
	/**
	 * @property {Number} height
	 */
	this.height = 1;
	/**
	 * @property {HTMLCollection} images
	 */
	this.images = {};
	/**
	 * @property {DOMImplementation} implementation
	 */
	this.implementation = {};
	/**
	 * @property {String} inputEncoding
	 */
	this.inputEncoding = "";
	/**
	 * @property {HTMLHtmlElement} lastChild
	 */
	this.lastChild = {};
	/**
	 * @property {Number} lastModified
	 */
	this.lastModified = 1;
	/**
	 * @property {Object} lastStyleSheetSet
	 */
	this.lastStyleSheetSet = {};
	/**
	 * @property {String} linkColor
	 */
	this.linkColor = "";
	/**
	 * @property {HTMLCollection} links
	 */
	this.links = {};
	/**
	 * @property {Object} localName
	 */
	this.localName = {};
	/**
	 * @property {Object} location
	 */
	this.location = {};
	/**
	 * @property {Object} namespaceURI
	 */
	this.namespaceURI = {};
	/**
	 * @property {Object} nextSibling
	 */
	this.nextSibling = {};
	/**
	 * @property {String} nodeName
	 */
	this.nodeName = "";
	/**
	 * @property {Number} nodeType
	 */
	this.nodeType = 1;
	/**
	 * @property {Object} nodeValue
	 */
	this.nodeValue = {};
	/**
	 * @property {Object} ownerDocument
	 */
	this.ownerDocument = {};
	/**
	 * @property {Object} parentNode
	 */
	this.parentNode = {};
	/**
	 * @property {HTMLCollection} plugins
	 */
	this.plugins = {};
	/**
	 * @property {String} preferredStyleSheetSet
	 */
	this.preferredStyleSheetSet = "";
	/**
	 * @property {Object} prefix
	 */
	this.prefix = {};
	/**
	 * @property {Object} previousSibling
	 */
	this.previousSibling = {};
	/**
	 * @property {String} readyState
	 */
	this.readyState = "";
	/**
	 * @property {String} referrer
	 */
	this.referrer = "";
	/**
	 * @property {String} selectedStyleSheetSet
	 */
	this.selectedStyleSheetSet = "";
	/**
	 * @property {Boolean} strictErrorChecking
	 */
	this.strictErrorChecking = true;
	/**
	 * @property {DOMStringList} styleSheetSets
	 */
	this.styleSheetSets = {};
	/**
	 * @property {StyleSheetList} styleSheets
	 */
	this.styleSheets = {};
	/**
	 * @property {Object} textContent
	 */
	this.textContent = {};
	/**
	 * @property {String} title
	 */
	this.title = "";
	/**
	 * @property {String} vlinkColor
	 */
	this.vlinkColor = "";
	/**
	 * @property {Number} width
	 */
	this.width = 1;
	/**
	 * @property {Object} xmlEncoding
	 */
	this.xmlEncoding = {};
	/**
	 * @property {Boolean} xmlStandalone
	 */
	this.xmlStandalone = true;
	/**
	 * @property {Object} xmlVersion
	 */
	this.xmlVersion = {};
	// methods
	HTMLDocument.prototype.addBinding = function(arg1, arg2){}
	HTMLDocument.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	HTMLDocument.prototype.adoptNode = function(arg1){}
	HTMLDocument.prototype.appendChild = function(arg1){}
	HTMLDocument.prototype.captureEvents = function(arg1){}
	HTMLDocument.prototype.clear = function(){}
	HTMLDocument.prototype.cloneNode = function(arg1){}
	HTMLDocument.prototype.close = function(){}
	HTMLDocument.prototype.compareDocumentPosition = function(arg1){}
	HTMLDocument.prototype.createAttribute = function(arg1){}
	HTMLDocument.prototype.createAttributeNS = function(arg1, arg2){}
	HTMLDocument.prototype.createCDATASection = function(arg1){}
	HTMLDocument.prototype.createComment = function(arg1){}
	HTMLDocument.prototype.createDocumentFragment = function(){}
	HTMLDocument.prototype.createElement = function(arg1){}
	HTMLDocument.prototype.createElementNS = function(arg1, arg2){}
	HTMLDocument.prototype.createEntityReference = function(arg1){}
	HTMLDocument.prototype.createEvent = function(arg1){}
	HTMLDocument.prototype.createExpression = function(arg1, arg2){}
	HTMLDocument.prototype.createNSResolver = function(arg1){}
	HTMLDocument.prototype.createNodeIterator = function(arg1, arg2, arg3, arg4){}
	HTMLDocument.prototype.createProcessingInstruction = function(arg1, arg2){}
	HTMLDocument.prototype.createRange = function(){}
	HTMLDocument.prototype.createTextNode = function(arg1){}
	HTMLDocument.prototype.createTreeWalker = function(arg1, arg2, arg3, arg4){}
	HTMLDocument.prototype.dispatchEvent = function(arg1){}
	HTMLDocument.prototype.elementFromPoint = function(arg1, arg2){}
	HTMLDocument.prototype.enableStyleSheetsForSet = function(arg1){}
	HTMLDocument.prototype.evaluate = function(arg1, arg2, arg3, arg4, arg5){}
	HTMLDocument.prototype.execCommand = function(arg1, arg2, arg3){}
	HTMLDocument.prototype.execCommandShowHelp = function(arg1){}
	HTMLDocument.prototype.getAnonymousElementByAttribute = function(arg1, arg2, arg3){}
	HTMLDocument.prototype.getAnonymousNodes = function(arg1){}
	HTMLDocument.prototype.getBindingParent = function(arg1){}
	HTMLDocument.prototype.getElementById = function(arg1){}
	HTMLDocument.prototype.getElementsByClassName = function(arg1){}
	HTMLDocument.prototype.getElementsByName = function(arg1){}
	HTMLDocument.prototype.getElementsByTagName = function(arg1){}
	HTMLDocument.prototype.getElementsByTagNameNS = function(arg1, arg2){}
	HTMLDocument.prototype.getFeature = function(arg1, arg2){}
	HTMLDocument.prototype.getSelection = function(){}
	HTMLDocument.prototype.getUserData = function(arg1){}
	HTMLDocument.prototype.hasAttributes = function(){}
	HTMLDocument.prototype.hasChildNodes = function(){}
	HTMLDocument.prototype.hasFocus = function(){}
	HTMLDocument.prototype.importNode = function(arg1, arg2){}
	HTMLDocument.prototype.insertBefore = function(arg1, arg2){}
	HTMLDocument.prototype.isDefaultNamespace = function(arg1){}
	HTMLDocument.prototype.isEqualNode = function(arg1){}
	HTMLDocument.prototype.isSameNode = function(arg1){}
	HTMLDocument.prototype.isSupported = function(arg1, arg2){}
	HTMLDocument.prototype.loadBindingDocument = function(arg1){}
	HTMLDocument.prototype.lookupNamespaceURI = function(arg1){}
	HTMLDocument.prototype.lookupPrefix = function(arg1){}
	HTMLDocument.prototype.mozSetImageElement = function(arg1, arg2){}
	HTMLDocument.prototype.normalize = function(){}
	HTMLDocument.prototype.normalizeDocument = function(){}
	HTMLDocument.prototype.open = function(){}
	HTMLDocument.prototype.queryCommandEnabled = function(arg1){}
	HTMLDocument.prototype.queryCommandIndeterm = function(arg1){}
	HTMLDocument.prototype.queryCommandState = function(arg1){}
	HTMLDocument.prototype.queryCommandSupported = function(arg1){}
	HTMLDocument.prototype.queryCommandText = function(arg1){}
	HTMLDocument.prototype.queryCommandValue = function(arg1){}
	HTMLDocument.prototype.querySelector = function(arg1){}
	HTMLDocument.prototype.querySelectorAll = function(arg1){}
	HTMLDocument.prototype.releaseCapture = function(){}
	HTMLDocument.prototype.releaseEvents = function(arg1){}
	HTMLDocument.prototype.removeBinding = function(arg1, arg2){}
	HTMLDocument.prototype.removeChild = function(arg1){}
	HTMLDocument.prototype.removeEventListener = function(arg1, arg2, arg3){}
	HTMLDocument.prototype.renameNode = function(arg1, arg2, arg3){}
	HTMLDocument.prototype.replaceChild = function(arg1, arg2){}
	HTMLDocument.prototype.routeEvent = function(arg1){}
	HTMLDocument.prototype.setUserData = function(arg1, arg2, arg3){}
	HTMLDocument.prototype.write = function(arg1){}
	HTMLDocument.prototype.writeln = function(arg1){}
}

/**
 * HTMLBodyElement class.
 * 
 * @class HTMLBodyElement
 * @returns {HTMLBodyElement}  
 */
function HTMLBodyElement() {
	// properties
	/**
	 * @property {Number} ATTRIBUTE_NODE
	 */
	this.ATTRIBUTE_NODE = 1;
	/**
	 * @property {Number} CDATA_SECTION_NODE
	 */
	this.CDATA_SECTION_NODE = 1;
	/**
	 * @property {Number} COMMENT_NODE
	 */
	this.COMMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_FRAGMENT_NODE
	 */
	this.DOCUMENT_FRAGMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_NODE
	 */
	this.DOCUMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINED_BY
	 */
	this.DOCUMENT_POSITION_CONTAINED_BY = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINS
	 */
	this.DOCUMENT_POSITION_CONTAINS = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_DISCONNECTED
	 */
	this.DOCUMENT_POSITION_DISCONNECTED = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_FOLLOWING
	 */
	this.DOCUMENT_POSITION_FOLLOWING = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC
	 */
	this.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_PRECEDING
	 */
	this.DOCUMENT_POSITION_PRECEDING = 1;
	/**
	 * @property {Number} DOCUMENT_TYPE_NODE
	 */
	this.DOCUMENT_TYPE_NODE = 1;
	/**
	 * @property {Number} ELEMENT_NODE
	 */
	this.ELEMENT_NODE = 1;
	/**
	 * @property {Number} ENTITY_NODE
	 */
	this.ENTITY_NODE = 1;
	/**
	 * @property {Number} ENTITY_REFERENCE_NODE
	 */
	this.ENTITY_REFERENCE_NODE = 1;
	/**
	 * @property {Number} NOTATION_NODE
	 */
	this.NOTATION_NODE = 1;
	/**
	 * @property {Number} PROCESSING_INSTRUCTION_NODE
	 */
	this.PROCESSING_INSTRUCTION_NODE = 1;
	/**
	 * @property {Number} TEXT_NODE
	 */
	this.TEXT_NODE = 1;
	/**
	 * @property {String} aLink
	 */
	this.aLink = "";
	/**
	 * @property {NamedNodeMap} attributes
	 */
	this.attributes = {};
	/**
	 * @property {String} background
	 */
	this.background = "";
	/**
	 * @property {String} baseURI
	 */
	this.baseURI = "";
	/**
	 * @property {String} bgColor
	 */
	this.bgColor = "";
	/**
	 * @property {Number} childElementCount
	 */
	this.childElementCount = 1;
	/**
	 * @property {NodeList} childNodes
	 */
	this.childNodes = {};
	/**
	 * @property {HTMLCollection} children
	 */
	this.children = {};
	/**
	 * @property {Object} classList
	 */
	this.classList = {};
	/**
	 * @property {String} className
	 */
	this.className = "";
	/**
	 * @property {Number} clientHeight
	 */
	this.clientHeight = 1;
	/**
	 * @property {Number} clientLeft
	 */
	this.clientLeft = 1;
	/**
	 * @property {Number} clientTop
	 */
	this.clientTop = 1;
	/**
	 * @property {Number} clientWidth
	 */
	this.clientWidth = 1;
	/**
	 * @property {String} contentEditable
	 */
	this.contentEditable = "";
	/**
	 * @property {String} dir
	 */
	this.dir = "";
	/**
	 * @property {Boolean} draggable
	 */
	this.draggable = true;
	/**
	 * @property {Text} firstChild
	 */
	this.firstChild = {};
	/**
	 * @property {HTMLScriptElement} firstElementChild
	 */
	this.firstElementChild = {};
	/**
	 * @property {Boolean} hidden
	 */
	this.hidden = true;
	/**
	 * @property {String} id
	 */
	this.id = "";
	/**
	 * @property {String} innerHTML
	 */
	this.innerHTML = "";
	/**
	 * @property {Boolean} isContentEditable
	 */
	this.isContentEditable = true;
	/**
	 * @property {String} lang
	 */
	this.lang = "";
	/**
	 * @property {HTMLPreElement} lastChild
	 */
	this.lastChild = {};
	/**
	 * @property {HTMLPreElement} lastElementChild
	 */
	this.lastElementChild = {};
	/**
	 * @property {String} link
	 */
	this.link = "";
	/**
	 * @property {String} localName
	 */
	this.localName = "";
	/**
	 * @property {String} namespaceURI
	 */
	this.namespaceURI = "";
	/**
	 * @property {Object} nextElementSibling
	 */
	this.nextElementSibling = {};
	/**
	 * @property {Object} nextSibling
	 */
	this.nextSibling = {};
	/**
	 * @property {String} nodeName
	 */
	this.nodeName = "";
	/**
	 * @property {Number} nodeType
	 */
	this.nodeType = 1;
	/**
	 * @property {Object} nodeValue
	 */
	this.nodeValue = {};
	/**
	 * @property {Number} offsetHeight
	 */
	this.offsetHeight = 1;
	/**
	 * @property {Number} offsetLeft
	 */
	this.offsetLeft = 1;
	/**
	 * @property {Object} offsetParent
	 */
	this.offsetParent = {};
	/**
	 * @property {Number} offsetTop
	 */
	this.offsetTop = 1;
	/**
	 * @property {Number} offsetWidth
	 */
	this.offsetWidth = 1;
	/**
	 * @property {HTMLDocument} ownerDocument
	 */
	this.ownerDocument = {};
	/**
	 * @property {HTMLHtmlElement} parentNode
	 */
	this.parentNode = {};
	/**
	 * @property {Object} prefix
	 */
	this.prefix = {};
	/**
	 * @property {HTMLHeadElement} previousElementSibling
	 */
	this.previousElementSibling = {};
	/**
	 * @property {Text} previousSibling
	 */
	this.previousSibling = {};
	/**
	 * @property {Number} scrollHeight
	 */
	this.scrollHeight = 1;
	/**
	 * @property {Number} scrollLeft
	 */
	this.scrollLeft = 1;
	/**
	 * @property {Number} scrollTop
	 */
	this.scrollTop = 1;
	/**
	 * @property {Number} scrollWidth
	 */
	this.scrollWidth = 1;
	/**
	 * @property {Boolean} spellcheck
	 */
	this.spellcheck = true;
	/**
	 * @property {CSSStyleDeclaration} style
	 */
	this.style = {};
	/**
	 * @property {Number} tabIndex
	 */
	this.tabIndex = 1;
	/**
	 * @property {String} tagName
	 */
	this.tagName = "";
	/**
	 * @property {String} text
	 */
	this.text = "";
	/**
	 * @property {String} textContent
	 */
	this.textContent = "";
	/**
	 * @property {String} title
	 */
	this.title = "";
	/**
	 * @property {String} vLink
	 */
	this.vLink = "";
	// methods
	HTMLBodyElement.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	HTMLBodyElement.prototype.appendChild = function(arg1){}
	HTMLBodyElement.prototype.blur = function(){}
	HTMLBodyElement.prototype.cloneNode = function(arg1){}
	HTMLBodyElement.prototype.compareDocumentPosition = function(arg1){}
	HTMLBodyElement.prototype.dispatchEvent = function(arg1){}
	HTMLBodyElement.prototype.focus = function(){}
	HTMLBodyElement.prototype.getAttribute = function(arg1){}
	HTMLBodyElement.prototype.getAttributeNS = function(arg1, arg2){}
	HTMLBodyElement.prototype.getAttributeNode = function(arg1){}
	HTMLBodyElement.prototype.getAttributeNodeNS = function(arg1, arg2){}
	HTMLBodyElement.prototype.getBoundingClientRect = function(){}
	HTMLBodyElement.prototype.getClientRects = function(){}
	HTMLBodyElement.prototype.getElementsByClassName = function(arg1){}
	HTMLBodyElement.prototype.getElementsByTagName = function(arg1){}
	HTMLBodyElement.prototype.getElementsByTagNameNS = function(arg1, arg2){}
	HTMLBodyElement.prototype.getFeature = function(arg1, arg2){}
	HTMLBodyElement.prototype.getUserData = function(arg1){}
	HTMLBodyElement.prototype.hasAttribute = function(arg1){}
	HTMLBodyElement.prototype.hasAttributeNS = function(arg1, arg2){}
	HTMLBodyElement.prototype.hasAttributes = function(){}
	HTMLBodyElement.prototype.hasChildNodes = function(){}
	HTMLBodyElement.prototype.insertBefore = function(arg1, arg2){}
	HTMLBodyElement.prototype.isDefaultNamespace = function(arg1){}
	HTMLBodyElement.prototype.isEqualNode = function(arg1){}
	HTMLBodyElement.prototype.isSameNode = function(arg1){}
	HTMLBodyElement.prototype.isSupported = function(arg1, arg2){}
	HTMLBodyElement.prototype.lookupNamespaceURI = function(arg1){}
	HTMLBodyElement.prototype.lookupPrefix = function(arg1){}
	HTMLBodyElement.prototype.mozMatchesSelector = function(arg1){}
	HTMLBodyElement.prototype.normalize = function(){}
	HTMLBodyElement.prototype.querySelector = function(arg1){}
	HTMLBodyElement.prototype.querySelectorAll = function(arg1){}
	HTMLBodyElement.prototype.releaseCapture = function(){}
	HTMLBodyElement.prototype.removeAttribute = function(arg1){}
	HTMLBodyElement.prototype.removeAttributeNS = function(arg1, arg2){}
	HTMLBodyElement.prototype.removeAttributeNode = function(arg1){}
	HTMLBodyElement.prototype.removeChild = function(arg1){}
	HTMLBodyElement.prototype.removeEventListener = function(arg1, arg2, arg3){}
	HTMLBodyElement.prototype.replaceChild = function(arg1, arg2){}
	HTMLBodyElement.prototype.scrollIntoView = function(arg1){}
	HTMLBodyElement.prototype.setAttribute = function(arg1, arg2){}
	HTMLBodyElement.prototype.setAttributeNS = function(arg1, arg2, arg3){}
	HTMLBodyElement.prototype.setAttributeNode = function(arg1){}
	HTMLBodyElement.prototype.setAttributeNodeNS = function(arg1){}
	HTMLBodyElement.prototype.setCapture = function(arg1){}
	HTMLBodyElement.prototype.setUserData = function(arg1, arg2, arg3){}
}

/**
 * NamedNodeMap class.
 * 
 * @class NamedNodeMap
 * @returns {NamedNodeMap}  
 */
function NamedNodeMap() {
	// properties
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	// methods
	NamedNodeMap.prototype.getNamedItem = function(arg1){}
	NamedNodeMap.prototype.getNamedItemNS = function(arg1, arg2){}
	NamedNodeMap.prototype.item = function(arg1){}
	NamedNodeMap.prototype.removeNamedItem = function(arg1){}
	NamedNodeMap.prototype.removeNamedItemNS = function(arg1, arg2){}
	NamedNodeMap.prototype.setNamedItem = function(arg1){}
	NamedNodeMap.prototype.setNamedItemNS = function(arg1){}
}

/**
 * Text class.
 * 
 * @class Text
 * @returns {Text}  
 */
function Text() {
	// properties
	/**
	 * @property {Number} ATTRIBUTE_NODE
	 */
	this.ATTRIBUTE_NODE = 1;
	/**
	 * @property {Number} CDATA_SECTION_NODE
	 */
	this.CDATA_SECTION_NODE = 1;
	/**
	 * @property {Number} COMMENT_NODE
	 */
	this.COMMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_FRAGMENT_NODE
	 */
	this.DOCUMENT_FRAGMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_NODE
	 */
	this.DOCUMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINED_BY
	 */
	this.DOCUMENT_POSITION_CONTAINED_BY = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINS
	 */
	this.DOCUMENT_POSITION_CONTAINS = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_DISCONNECTED
	 */
	this.DOCUMENT_POSITION_DISCONNECTED = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_FOLLOWING
	 */
	this.DOCUMENT_POSITION_FOLLOWING = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC
	 */
	this.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_PRECEDING
	 */
	this.DOCUMENT_POSITION_PRECEDING = 1;
	/**
	 * @property {Number} DOCUMENT_TYPE_NODE
	 */
	this.DOCUMENT_TYPE_NODE = 1;
	/**
	 * @property {Number} ELEMENT_NODE
	 */
	this.ELEMENT_NODE = 1;
	/**
	 * @property {Number} ENTITY_NODE
	 */
	this.ENTITY_NODE = 1;
	/**
	 * @property {Number} ENTITY_REFERENCE_NODE
	 */
	this.ENTITY_REFERENCE_NODE = 1;
	/**
	 * @property {Number} NOTATION_NODE
	 */
	this.NOTATION_NODE = 1;
	/**
	 * @property {Number} PROCESSING_INSTRUCTION_NODE
	 */
	this.PROCESSING_INSTRUCTION_NODE = 1;
	/**
	 * @property {Number} TEXT_NODE
	 */
	this.TEXT_NODE = 1;
	/**
	 * @property {Object} attributes
	 */
	this.attributes = {};
	/**
	 * @property {String} baseURI
	 */
	this.baseURI = "";
	/**
	 * @property {NodeList} childNodes
	 */
	this.childNodes = {};
	/**
	 * @property {String} data
	 */
	this.data = "";
	/**
	 * @property {Object} firstChild
	 */
	this.firstChild = {};
	/**
	 * @property {Boolean} isElementContentWhitespace
	 */
	this.isElementContentWhitespace = true;
	/**
	 * @property {Object} lastChild
	 */
	this.lastChild = {};
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	/**
	 * @property {Object} localName
	 */
	this.localName = {};
	/**
	 * @property {Object} namespaceURI
	 */
	this.namespaceURI = {};
	/**
	 * @property {HTMLScriptElement} nextSibling
	 */
	this.nextSibling = {};
	/**
	 * @property {String} nodeName
	 */
	this.nodeName = "";
	/**
	 * @property {Number} nodeType
	 */
	this.nodeType = 1;
	/**
	 * @property {String} nodeValue
	 */
	this.nodeValue = "";
	/**
	 * @property {HTMLDocument} ownerDocument
	 */
	this.ownerDocument = {};
	/**
	 * @property {HTMLBodyElement} parentNode
	 */
	this.parentNode = {};
	/**
	 * @property {Object} prefix
	 */
	this.prefix = {};
	/**
	 * @property {Object} previousSibling
	 */
	this.previousSibling = {};
	/**
	 * @property {String} textContent
	 */
	this.textContent = "";
	/**
	 * @property {String} wholeText
	 */
	this.wholeText = "";
	// methods
	Text.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	Text.prototype.appendChild = function(arg1){}
	Text.prototype.appendData = function(arg1){}
	Text.prototype.cloneNode = function(arg1){}
	Text.prototype.compareDocumentPosition = function(arg1){}
	Text.prototype.deleteData = function(arg1, arg2){}
	Text.prototype.dispatchEvent = function(arg1){}
	Text.prototype.getFeature = function(arg1, arg2){}
	Text.prototype.getUserData = function(arg1){}
	Text.prototype.hasAttributes = function(){}
	Text.prototype.hasChildNodes = function(){}
	Text.prototype.insertBefore = function(arg1, arg2){}
	Text.prototype.insertData = function(arg1, arg2){}
	Text.prototype.isDefaultNamespace = function(arg1){}
	Text.prototype.isEqualNode = function(arg1){}
	Text.prototype.isSameNode = function(arg1){}
	Text.prototype.isSupported = function(arg1, arg2){}
	Text.prototype.lookupNamespaceURI = function(arg1){}
	Text.prototype.lookupPrefix = function(arg1){}
	Text.prototype.normalize = function(){}
	Text.prototype.removeChild = function(arg1){}
	Text.prototype.removeEventListener = function(arg1, arg2, arg3){}
	Text.prototype.replaceChild = function(arg1, arg2){}
	Text.prototype.replaceData = function(arg1, arg2, arg3){}
	Text.prototype.replaceWholeText = function(arg1){}
	Text.prototype.setUserData = function(arg1, arg2, arg3){}
	Text.prototype.splitText = function(arg1){}
	Text.prototype.substringData = function(arg1, arg2){}
}

/**
 * HTMLPreElement class.
 * 
 * @class HTMLPreElement
 * @returns {HTMLPreElement}  
 */
function HTMLPreElement() {
	// properties
	/**
	 * @property {Number} ATTRIBUTE_NODE
	 */
	this.ATTRIBUTE_NODE = 1;
	/**
	 * @property {Number} CDATA_SECTION_NODE
	 */
	this.CDATA_SECTION_NODE = 1;
	/**
	 * @property {Number} COMMENT_NODE
	 */
	this.COMMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_FRAGMENT_NODE
	 */
	this.DOCUMENT_FRAGMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_NODE
	 */
	this.DOCUMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINED_BY
	 */
	this.DOCUMENT_POSITION_CONTAINED_BY = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINS
	 */
	this.DOCUMENT_POSITION_CONTAINS = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_DISCONNECTED
	 */
	this.DOCUMENT_POSITION_DISCONNECTED = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_FOLLOWING
	 */
	this.DOCUMENT_POSITION_FOLLOWING = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC
	 */
	this.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_PRECEDING
	 */
	this.DOCUMENT_POSITION_PRECEDING = 1;
	/**
	 * @property {Number} DOCUMENT_TYPE_NODE
	 */
	this.DOCUMENT_TYPE_NODE = 1;
	/**
	 * @property {Number} ELEMENT_NODE
	 */
	this.ELEMENT_NODE = 1;
	/**
	 * @property {Number} ENTITY_NODE
	 */
	this.ENTITY_NODE = 1;
	/**
	 * @property {Number} ENTITY_REFERENCE_NODE
	 */
	this.ENTITY_REFERENCE_NODE = 1;
	/**
	 * @property {Number} NOTATION_NODE
	 */
	this.NOTATION_NODE = 1;
	/**
	 * @property {Number} PROCESSING_INSTRUCTION_NODE
	 */
	this.PROCESSING_INSTRUCTION_NODE = 1;
	/**
	 * @property {Number} TEXT_NODE
	 */
	this.TEXT_NODE = 1;
	/**
	 * @property {NamedNodeMap} attributes
	 */
	this.attributes = {};
	/**
	 * @property {String} baseURI
	 */
	this.baseURI = "";
	/**
	 * @property {Number} childElementCount
	 */
	this.childElementCount = 1;
	/**
	 * @property {NodeList} childNodes
	 */
	this.childNodes = {};
	/**
	 * @property {HTMLCollection} children
	 */
	this.children = {};
	/**
	 * @property {Object} classList
	 */
	this.classList = {};
	/**
	 * @property {String} className
	 */
	this.className = "";
	/**
	 * @property {Number} clientHeight
	 */
	this.clientHeight = 1;
	/**
	 * @property {Number} clientLeft
	 */
	this.clientLeft = 1;
	/**
	 * @property {Number} clientTop
	 */
	this.clientTop = 1;
	/**
	 * @property {Number} clientWidth
	 */
	this.clientWidth = 1;
	/**
	 * @property {String} contentEditable
	 */
	this.contentEditable = "";
	/**
	 * @property {String} dir
	 */
	this.dir = "";
	/**
	 * @property {Boolean} draggable
	 */
	this.draggable = true;
	/**
	 * @property {Text} firstChild
	 */
	this.firstChild = {};
	/**
	 * @property {Object} firstElementChild
	 */
	this.firstElementChild = {};
	/**
	 * @property {Boolean} hidden
	 */
	this.hidden = true;
	/**
	 * @property {String} id
	 */
	this.id = "";
	/**
	 * @property {String} innerHTML
	 */
	this.innerHTML = "";
	/**
	 * @property {Boolean} isContentEditable
	 */
	this.isContentEditable = true;
	/**
	 * @property {String} lang
	 */
	this.lang = "";
	/**
	 * @property {Text} lastChild
	 */
	this.lastChild = {};
	/**
	 * @property {Object} lastElementChild
	 */
	this.lastElementChild = {};
	/**
	 * @property {String} localName
	 */
	this.localName = "";
	/**
	 * @property {String} namespaceURI
	 */
	this.namespaceURI = "";
	/**
	 * @property {HTMLPreElement} nextElementSibling
	 */
	this.nextElementSibling = {};
	/**
	 * @property {HTMLPreElement} nextSibling
	 */
	this.nextSibling = {};
	/**
	 * @property {String} nodeName
	 */
	this.nodeName = "";
	/**
	 * @property {Number} nodeType
	 */
	this.nodeType = 1;
	/**
	 * @property {Object} nodeValue
	 */
	this.nodeValue = {};
	/**
	 * @property {Number} offsetHeight
	 */
	this.offsetHeight = 1;
	/**
	 * @property {Number} offsetLeft
	 */
	this.offsetLeft = 1;
	/**
	 * @property {HTMLBodyElement} offsetParent
	 */
	this.offsetParent = {};
	/**
	 * @property {Number} offsetTop
	 */
	this.offsetTop = 1;
	/**
	 * @property {Number} offsetWidth
	 */
	this.offsetWidth = 1;
	/**
	 * @property {HTMLDocument} ownerDocument
	 */
	this.ownerDocument = {};
	/**
	 * @property {HTMLBodyElement} parentNode
	 */
	this.parentNode = {};
	/**
	 * @property {Object} prefix
	 */
	this.prefix = {};
	/**
	 * @property {HTMLPreElement} previousElementSibling
	 */
	this.previousElementSibling = {};
	/**
	 * @property {HTMLPreElement} previousSibling
	 */
	this.previousSibling = {};
	/**
	 * @property {Number} scrollHeight
	 */
	this.scrollHeight = 1;
	/**
	 * @property {Number} scrollLeft
	 */
	this.scrollLeft = 1;
	/**
	 * @property {Number} scrollTop
	 */
	this.scrollTop = 1;
	/**
	 * @property {Number} scrollWidth
	 */
	this.scrollWidth = 1;
	/**
	 * @property {Boolean} spellcheck
	 */
	this.spellcheck = true;
	/**
	 * @property {CSSStyleDeclaration} style
	 */
	this.style = {};
	/**
	 * @property {Number} tabIndex
	 */
	this.tabIndex = 1;
	/**
	 * @property {String} tagName
	 */
	this.tagName = "";
	/**
	 * @property {String} textContent
	 */
	this.textContent = "";
	/**
	 * @property {String} title
	 */
	this.title = "";
	/**
	 * @property {Number} width
	 */
	this.width = 1;
	// methods
	HTMLPreElement.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	HTMLPreElement.prototype.appendChild = function(arg1){}
	HTMLPreElement.prototype.blur = function(){}
	HTMLPreElement.prototype.cloneNode = function(arg1){}
	HTMLPreElement.prototype.compareDocumentPosition = function(arg1){}
	HTMLPreElement.prototype.dispatchEvent = function(arg1){}
	HTMLPreElement.prototype.focus = function(){}
	HTMLPreElement.prototype.getAttribute = function(arg1){}
	HTMLPreElement.prototype.getAttributeNS = function(arg1, arg2){}
	HTMLPreElement.prototype.getAttributeNode = function(arg1){}
	HTMLPreElement.prototype.getAttributeNodeNS = function(arg1, arg2){}
	HTMLPreElement.prototype.getBoundingClientRect = function(){}
	HTMLPreElement.prototype.getClientRects = function(){}
	HTMLPreElement.prototype.getElementsByClassName = function(arg1){}
	HTMLPreElement.prototype.getElementsByTagName = function(arg1){}
	HTMLPreElement.prototype.getElementsByTagNameNS = function(arg1, arg2){}
	HTMLPreElement.prototype.getFeature = function(arg1, arg2){}
	HTMLPreElement.prototype.getUserData = function(arg1){}
	HTMLPreElement.prototype.hasAttribute = function(arg1){}
	HTMLPreElement.prototype.hasAttributeNS = function(arg1, arg2){}
	HTMLPreElement.prototype.hasAttributes = function(){}
	HTMLPreElement.prototype.hasChildNodes = function(){}
	HTMLPreElement.prototype.insertBefore = function(arg1, arg2){}
	HTMLPreElement.prototype.isDefaultNamespace = function(arg1){}
	HTMLPreElement.prototype.isEqualNode = function(arg1){}
	HTMLPreElement.prototype.isSameNode = function(arg1){}
	HTMLPreElement.prototype.isSupported = function(arg1, arg2){}
	HTMLPreElement.prototype.lookupNamespaceURI = function(arg1){}
	HTMLPreElement.prototype.lookupPrefix = function(arg1){}
	HTMLPreElement.prototype.mozMatchesSelector = function(arg1){}
	HTMLPreElement.prototype.normalize = function(){}
	HTMLPreElement.prototype.querySelector = function(arg1){}
	HTMLPreElement.prototype.querySelectorAll = function(arg1){}
	HTMLPreElement.prototype.releaseCapture = function(){}
	HTMLPreElement.prototype.removeAttribute = function(arg1){}
	HTMLPreElement.prototype.removeAttributeNS = function(arg1, arg2){}
	HTMLPreElement.prototype.removeAttributeNode = function(arg1){}
	HTMLPreElement.prototype.removeChild = function(arg1){}
	HTMLPreElement.prototype.removeEventListener = function(arg1, arg2, arg3){}
	HTMLPreElement.prototype.replaceChild = function(arg1, arg2){}
	HTMLPreElement.prototype.scrollIntoView = function(arg1){}
	HTMLPreElement.prototype.setAttribute = function(arg1, arg2){}
	HTMLPreElement.prototype.setAttributeNS = function(arg1, arg2, arg3){}
	HTMLPreElement.prototype.setAttributeNode = function(arg1){}
	HTMLPreElement.prototype.setAttributeNodeNS = function(arg1){}
	HTMLPreElement.prototype.setCapture = function(arg1){}
	HTMLPreElement.prototype.setUserData = function(arg1, arg2, arg3){}
}

/**
 * CSSStyleDeclaration class.
 * 
 * @class CSSStyleDeclaration
 * @returns {CSSStyleDeclaration}  
 */
function CSSStyleDeclaration() {
	// properties
	/**
	 * @property {String} MozAppearance
	 */
	this.MozAppearance = "";
	/**
	 * @property {String} MozBackgroundInlinePolicy
	 */
	this.MozBackgroundInlinePolicy = "";
	/**
	 * @property {String} MozBinding
	 */
	this.MozBinding = "";
	/**
	 * @property {String} MozBorderBottomColors
	 */
	this.MozBorderBottomColors = "";
	/**
	 * @property {String} MozBorderEnd
	 */
	this.MozBorderEnd = "";
	/**
	 * @property {String} MozBorderEndColor
	 */
	this.MozBorderEndColor = "";
	/**
	 * @property {String} MozBorderEndStyle
	 */
	this.MozBorderEndStyle = "";
	/**
	 * @property {String} MozBorderEndWidth
	 */
	this.MozBorderEndWidth = "";
	/**
	 * @property {String} MozBorderImage
	 */
	this.MozBorderImage = "";
	/**
	 * @property {String} MozBorderLeftColors
	 */
	this.MozBorderLeftColors = "";
	/**
	 * @property {String} MozBorderRightColors
	 */
	this.MozBorderRightColors = "";
	/**
	 * @property {String} MozBorderStart
	 */
	this.MozBorderStart = "";
	/**
	 * @property {String} MozBorderStartColor
	 */
	this.MozBorderStartColor = "";
	/**
	 * @property {String} MozBorderStartStyle
	 */
	this.MozBorderStartStyle = "";
	/**
	 * @property {String} MozBorderStartWidth
	 */
	this.MozBorderStartWidth = "";
	/**
	 * @property {String} MozBorderTopColors
	 */
	this.MozBorderTopColors = "";
	/**
	 * @property {String} MozBoxAlign
	 */
	this.MozBoxAlign = "";
	/**
	 * @property {String} MozBoxDirection
	 */
	this.MozBoxDirection = "";
	/**
	 * @property {String} MozBoxFlex
	 */
	this.MozBoxFlex = "";
	/**
	 * @property {String} MozBoxOrdinalGroup
	 */
	this.MozBoxOrdinalGroup = "";
	/**
	 * @property {String} MozBoxOrient
	 */
	this.MozBoxOrient = "";
	/**
	 * @property {String} MozBoxPack
	 */
	this.MozBoxPack = "";
	/**
	 * @property {String} MozBoxSizing
	 */
	this.MozBoxSizing = "";
	/**
	 * @property {String} MozColumnCount
	 */
	this.MozColumnCount = "";
	/**
	 * @property {String} MozColumnGap
	 */
	this.MozColumnGap = "";
	/**
	 * @property {String} MozColumnRule
	 */
	this.MozColumnRule = "";
	/**
	 * @property {String} MozColumnRuleColor
	 */
	this.MozColumnRuleColor = "";
	/**
	 * @property {String} MozColumnRuleStyle
	 */
	this.MozColumnRuleStyle = "";
	/**
	 * @property {String} MozColumnRuleWidth
	 */
	this.MozColumnRuleWidth = "";
	/**
	 * @property {String} MozColumnWidth
	 */
	this.MozColumnWidth = "";
	/**
	 * @property {String} MozFloatEdge
	 */
	this.MozFloatEdge = "";
	/**
	 * @property {String} MozFontFeatureSettings
	 */
	this.MozFontFeatureSettings = "";
	/**
	 * @property {String} MozFontLanguageOverride
	 */
	this.MozFontLanguageOverride = "";
	/**
	 * @property {String} MozForceBrokenImageIcon
	 */
	this.MozForceBrokenImageIcon = "";
	/**
	 * @property {String} MozImageRegion
	 */
	this.MozImageRegion = "";
	/**
	 * @property {String} MozMarginEnd
	 */
	this.MozMarginEnd = "";
	/**
	 * @property {String} MozMarginStart
	 */
	this.MozMarginStart = "";
	/**
	 * @property {String} MozOpacity
	 */
	this.MozOpacity = "";
	/**
	 * @property {String} MozOutline
	 */
	this.MozOutline = "";
	/**
	 * @property {String} MozOutlineColor
	 */
	this.MozOutlineColor = "";
	/**
	 * @property {String} MozOutlineOffset
	 */
	this.MozOutlineOffset = "";
	/**
	 * @property {String} MozOutlineRadius
	 */
	this.MozOutlineRadius = "";
	/**
	 * @property {String} MozOutlineRadiusBottomleft
	 */
	this.MozOutlineRadiusBottomleft = "";
	/**
	 * @property {String} MozOutlineRadiusBottomright
	 */
	this.MozOutlineRadiusBottomright = "";
	/**
	 * @property {String} MozOutlineRadiusTopleft
	 */
	this.MozOutlineRadiusTopleft = "";
	/**
	 * @property {String} MozOutlineRadiusTopright
	 */
	this.MozOutlineRadiusTopright = "";
	/**
	 * @property {String} MozOutlineStyle
	 */
	this.MozOutlineStyle = "";
	/**
	 * @property {String} MozOutlineWidth
	 */
	this.MozOutlineWidth = "";
	/**
	 * @property {String} MozPaddingEnd
	 */
	this.MozPaddingEnd = "";
	/**
	 * @property {String} MozPaddingStart
	 */
	this.MozPaddingStart = "";
	/**
	 * @property {String} MozStackSizing
	 */
	this.MozStackSizing = "";
	/**
	 * @property {String} MozTabSize
	 */
	this.MozTabSize = "";
	/**
	 * @property {String} MozTransform
	 */
	this.MozTransform = "";
	/**
	 * @property {String} MozTransformOrigin
	 */
	this.MozTransformOrigin = "";
	/**
	 * @property {String} MozTransition
	 */
	this.MozTransition = "";
	/**
	 * @property {String} MozTransitionDelay
	 */
	this.MozTransitionDelay = "";
	/**
	 * @property {String} MozTransitionDuration
	 */
	this.MozTransitionDuration = "";
	/**
	 * @property {String} MozTransitionProperty
	 */
	this.MozTransitionProperty = "";
	/**
	 * @property {String} MozTransitionTimingFunction
	 */
	this.MozTransitionTimingFunction = "";
	/**
	 * @property {String} MozUserFocus
	 */
	this.MozUserFocus = "";
	/**
	 * @property {String} MozUserInput
	 */
	this.MozUserInput = "";
	/**
	 * @property {String} MozUserModify
	 */
	this.MozUserModify = "";
	/**
	 * @property {String} MozUserSelect
	 */
	this.MozUserSelect = "";
	/**
	 * @property {String} MozWindowShadow
	 */
	this.MozWindowShadow = "";
	/**
	 * @property {String} azimuth
	 */
	this.azimuth = "";
	/**
	 * @property {String} background
	 */
	this.background = "";
	/**
	 * @property {String} backgroundAttachment
	 */
	this.backgroundAttachment = "";
	/**
	 * @property {String} backgroundClip
	 */
	this.backgroundClip = "";
	/**
	 * @property {String} backgroundColor
	 */
	this.backgroundColor = "";
	/**
	 * @property {String} backgroundImage
	 */
	this.backgroundImage = "";
	/**
	 * @property {String} backgroundOrigin
	 */
	this.backgroundOrigin = "";
	/**
	 * @property {String} backgroundPosition
	 */
	this.backgroundPosition = "";
	/**
	 * @property {String} backgroundRepeat
	 */
	this.backgroundRepeat = "";
	/**
	 * @property {String} backgroundSize
	 */
	this.backgroundSize = "";
	/**
	 * @property {String} border
	 */
	this.border = "";
	/**
	 * @property {String} borderBottom
	 */
	this.borderBottom = "";
	/**
	 * @property {String} borderBottomColor
	 */
	this.borderBottomColor = "";
	/**
	 * @property {String} borderBottomLeftRadius
	 */
	this.borderBottomLeftRadius = "";
	/**
	 * @property {String} borderBottomRightRadius
	 */
	this.borderBottomRightRadius = "";
	/**
	 * @property {String} borderBottomStyle
	 */
	this.borderBottomStyle = "";
	/**
	 * @property {String} borderBottomWidth
	 */
	this.borderBottomWidth = "";
	/**
	 * @property {String} borderCollapse
	 */
	this.borderCollapse = "";
	/**
	 * @property {String} borderColor
	 */
	this.borderColor = "";
	/**
	 * @property {String} borderLeft
	 */
	this.borderLeft = "";
	/**
	 * @property {String} borderLeftColor
	 */
	this.borderLeftColor = "";
	/**
	 * @property {String} borderLeftStyle
	 */
	this.borderLeftStyle = "";
	/**
	 * @property {String} borderLeftWidth
	 */
	this.borderLeftWidth = "";
	/**
	 * @property {String} borderRadius
	 */
	this.borderRadius = "";
	/**
	 * @property {String} borderRight
	 */
	this.borderRight = "";
	/**
	 * @property {String} borderRightColor
	 */
	this.borderRightColor = "";
	/**
	 * @property {String} borderRightStyle
	 */
	this.borderRightStyle = "";
	/**
	 * @property {String} borderRightWidth
	 */
	this.borderRightWidth = "";
	/**
	 * @property {String} borderSpacing
	 */
	this.borderSpacing = "";
	/**
	 * @property {String} borderStyle
	 */
	this.borderStyle = "";
	/**
	 * @property {String} borderTop
	 */
	this.borderTop = "";
	/**
	 * @property {String} borderTopColor
	 */
	this.borderTopColor = "";
	/**
	 * @property {String} borderTopLeftRadius
	 */
	this.borderTopLeftRadius = "";
	/**
	 * @property {String} borderTopRightRadius
	 */
	this.borderTopRightRadius = "";
	/**
	 * @property {String} borderTopStyle
	 */
	this.borderTopStyle = "";
	/**
	 * @property {String} borderTopWidth
	 */
	this.borderTopWidth = "";
	/**
	 * @property {String} borderWidth
	 */
	this.borderWidth = "";
	/**
	 * @property {String} bottom
	 */
	this.bottom = "";
	/**
	 * @property {String} boxShadow
	 */
	this.boxShadow = "";
	/**
	 * @property {String} captionSide
	 */
	this.captionSide = "";
	/**
	 * @property {String} clear
	 */
	this.clear = "";
	/**
	 * @property {String} clip
	 */
	this.clip = "";
	/**
	 * @property {String} clipPath
	 */
	this.clipPath = "";
	/**
	 * @property {String} clipRule
	 */
	this.clipRule = "";
	/**
	 * @property {String} color
	 */
	this.color = "";
	/**
	 * @property {String} colorInterpolation
	 */
	this.colorInterpolation = "";
	/**
	 * @property {String} colorInterpolationFilters
	 */
	this.colorInterpolationFilters = "";
	/**
	 * @property {String} content
	 */
	this.content = "";
	/**
	 * @property {String} counterIncrement
	 */
	this.counterIncrement = "";
	/**
	 * @property {String} counterReset
	 */
	this.counterReset = "";
	/**
	 * @property {String} cssFloat
	 */
	this.cssFloat = "";
	/**
	 * @property {String} cssText
	 */
	this.cssText = "";
	/**
	 * @property {String} cue
	 */
	this.cue = "";
	/**
	 * @property {String} cueAfter
	 */
	this.cueAfter = "";
	/**
	 * @property {String} cueBefore
	 */
	this.cueBefore = "";
	/**
	 * @property {String} cursor
	 */
	this.cursor = "";
	/**
	 * @property {String} direction
	 */
	this.direction = "";
	/**
	 * @property {String} display
	 */
	this.display = "";
	/**
	 * @property {String} dominantBaseline
	 */
	this.dominantBaseline = "";
	/**
	 * @property {String} elevation
	 */
	this.elevation = "";
	/**
	 * @property {String} emptyCells
	 */
	this.emptyCells = "";
	/**
	 * @property {String} fill
	 */
	this.fill = "";
	/**
	 * @property {String} fillOpacity
	 */
	this.fillOpacity = "";
	/**
	 * @property {String} fillRule
	 */
	this.fillRule = "";
	/**
	 * @property {String} filter
	 */
	this.filter = "";
	/**
	 * @property {String} floodColor
	 */
	this.floodColor = "";
	/**
	 * @property {String} floodOpacity
	 */
	this.floodOpacity = "";
	/**
	 * @property {String} font
	 */
	this.font = "";
	/**
	 * @property {String} fontFamily
	 */
	this.fontFamily = "";
	/**
	 * @property {String} fontSize
	 */
	this.fontSize = "";
	/**
	 * @property {String} fontSizeAdjust
	 */
	this.fontSizeAdjust = "";
	/**
	 * @property {String} fontStretch
	 */
	this.fontStretch = "";
	/**
	 * @property {String} fontStyle
	 */
	this.fontStyle = "";
	/**
	 * @property {String} fontVariant
	 */
	this.fontVariant = "";
	/**
	 * @property {String} fontWeight
	 */
	this.fontWeight = "";
	/**
	 * @property {String} height
	 */
	this.height = "";
	/**
	 * @property {String} imageRendering
	 */
	this.imageRendering = "";
	/**
	 * @property {String} imeMode
	 */
	this.imeMode = "";
	/**
	 * @property {String} left
	 */
	this.left = "";
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	/**
	 * @property {String} letterSpacing
	 */
	this.letterSpacing = "";
	/**
	 * @property {String} lightingColor
	 */
	this.lightingColor = "";
	/**
	 * @property {String} lineHeight
	 */
	this.lineHeight = "";
	/**
	 * @property {String} listStyle
	 */
	this.listStyle = "";
	/**
	 * @property {String} listStyleImage
	 */
	this.listStyleImage = "";
	/**
	 * @property {String} listStylePosition
	 */
	this.listStylePosition = "";
	/**
	 * @property {String} listStyleType
	 */
	this.listStyleType = "";
	/**
	 * @property {String} margin
	 */
	this.margin = "";
	/**
	 * @property {String} marginBottom
	 */
	this.marginBottom = "";
	/**
	 * @property {String} marginLeft
	 */
	this.marginLeft = "";
	/**
	 * @property {String} marginRight
	 */
	this.marginRight = "";
	/**
	 * @property {String} marginTop
	 */
	this.marginTop = "";
	/**
	 * @property {String} marker
	 */
	this.marker = "";
	/**
	 * @property {String} markerEnd
	 */
	this.markerEnd = "";
	/**
	 * @property {String} markerMid
	 */
	this.markerMid = "";
	/**
	 * @property {String} markerOffset
	 */
	this.markerOffset = "";
	/**
	 * @property {String} markerStart
	 */
	this.markerStart = "";
	/**
	 * @property {String} marks
	 */
	this.marks = "";
	/**
	 * @property {String} mask
	 */
	this.mask = "";
	/**
	 * @property {String} maxHeight
	 */
	this.maxHeight = "";
	/**
	 * @property {String} maxWidth
	 */
	this.maxWidth = "";
	/**
	 * @property {String} minHeight
	 */
	this.minHeight = "";
	/**
	 * @property {String} minWidth
	 */
	this.minWidth = "";
	/**
	 * @property {String} opacity
	 */
	this.opacity = "";
	/**
	 * @property {String} orphans
	 */
	this.orphans = "";
	/**
	 * @property {String} outline
	 */
	this.outline = "";
	/**
	 * @property {String} outlineColor
	 */
	this.outlineColor = "";
	/**
	 * @property {String} outlineOffset
	 */
	this.outlineOffset = "";
	/**
	 * @property {String} outlineStyle
	 */
	this.outlineStyle = "";
	/**
	 * @property {String} outlineWidth
	 */
	this.outlineWidth = "";
	/**
	 * @property {String} overflow
	 */
	this.overflow = "";
	/**
	 * @property {String} overflowX
	 */
	this.overflowX = "";
	/**
	 * @property {String} overflowY
	 */
	this.overflowY = "";
	/**
	 * @property {String} padding
	 */
	this.padding = "";
	/**
	 * @property {String} paddingBottom
	 */
	this.paddingBottom = "";
	/**
	 * @property {String} paddingLeft
	 */
	this.paddingLeft = "";
	/**
	 * @property {String} paddingRight
	 */
	this.paddingRight = "";
	/**
	 * @property {String} paddingTop
	 */
	this.paddingTop = "";
	/**
	 * @property {String} page
	 */
	this.page = "";
	/**
	 * @property {String} pageBreakAfter
	 */
	this.pageBreakAfter = "";
	/**
	 * @property {String} pageBreakBefore
	 */
	this.pageBreakBefore = "";
	/**
	 * @property {String} pageBreakInside
	 */
	this.pageBreakInside = "";
	/**
	 * @property {Object} parentRule
	 */
	this.parentRule = {};
	/**
	 * @property {String} pause
	 */
	this.pause = "";
	/**
	 * @property {String} pauseAfter
	 */
	this.pauseAfter = "";
	/**
	 * @property {String} pauseBefore
	 */
	this.pauseBefore = "";
	/**
	 * @property {String} pitch
	 */
	this.pitch = "";
	/**
	 * @property {String} pitchRange
	 */
	this.pitchRange = "";
	/**
	 * @property {String} pointerEvents
	 */
	this.pointerEvents = "";
	/**
	 * @property {String} position
	 */
	this.position = "";
	/**
	 * @property {String} quotes
	 */
	this.quotes = "";
	/**
	 * @property {String} resize
	 */
	this.resize = "";
	/**
	 * @property {String} richness
	 */
	this.richness = "";
	/**
	 * @property {String} right
	 */
	this.right = "";
	/**
	 * @property {String} shapeRendering
	 */
	this.shapeRendering = "";
	/**
	 * @property {String} size
	 */
	this.size = "";
	/**
	 * @property {String} speak
	 */
	this.speak = "";
	/**
	 * @property {String} speakHeader
	 */
	this.speakHeader = "";
	/**
	 * @property {String} speakNumeral
	 */
	this.speakNumeral = "";
	/**
	 * @property {String} speakPunctuation
	 */
	this.speakPunctuation = "";
	/**
	 * @property {String} speechRate
	 */
	this.speechRate = "";
	/**
	 * @property {String} stopColor
	 */
	this.stopColor = "";
	/**
	 * @property {String} stopOpacity
	 */
	this.stopOpacity = "";
	/**
	 * @property {String} stress
	 */
	this.stress = "";
	/**
	 * @property {String} stroke
	 */
	this.stroke = "";
	/**
	 * @property {String} strokeDasharray
	 */
	this.strokeDasharray = "";
	/**
	 * @property {String} strokeDashoffset
	 */
	this.strokeDashoffset = "";
	/**
	 * @property {String} strokeLinecap
	 */
	this.strokeLinecap = "";
	/**
	 * @property {String} strokeLinejoin
	 */
	this.strokeLinejoin = "";
	/**
	 * @property {String} strokeMiterlimit
	 */
	this.strokeMiterlimit = "";
	/**
	 * @property {String} strokeOpacity
	 */
	this.strokeOpacity = "";
	/**
	 * @property {String} strokeWidth
	 */
	this.strokeWidth = "";
	/**
	 * @property {String} tableLayout
	 */
	this.tableLayout = "";
	/**
	 * @property {String} textAlign
	 */
	this.textAlign = "";
	/**
	 * @property {String} textAnchor
	 */
	this.textAnchor = "";
	/**
	 * @property {String} textDecoration
	 */
	this.textDecoration = "";
	/**
	 * @property {String} textIndent
	 */
	this.textIndent = "";
	/**
	 * @property {String} textRendering
	 */
	this.textRendering = "";
	/**
	 * @property {String} textShadow
	 */
	this.textShadow = "";
	/**
	 * @property {String} textTransform
	 */
	this.textTransform = "";
	/**
	 * @property {String} top
	 */
	this.top = "";
	/**
	 * @property {String} unicodeBidi
	 */
	this.unicodeBidi = "";
	/**
	 * @property {String} verticalAlign
	 */
	this.verticalAlign = "";
	/**
	 * @property {String} visibility
	 */
	this.visibility = "";
	/**
	 * @property {String} voiceFamily
	 */
	this.voiceFamily = "";
	/**
	 * @property {String} volume
	 */
	this.volume = "";
	/**
	 * @property {String} whiteSpace
	 */
	this.whiteSpace = "";
	/**
	 * @property {String} widows
	 */
	this.widows = "";
	/**
	 * @property {String} width
	 */
	this.width = "";
	/**
	 * @property {String} wordSpacing
	 */
	this.wordSpacing = "";
	/**
	 * @property {String} wordWrap
	 */
	this.wordWrap = "";
	/**
	 * @property {String} zIndex
	 */
	this.zIndex = "";
	// methods
	CSSStyleDeclaration.prototype.getPropertyCSSValue = function(arg1){}
	CSSStyleDeclaration.prototype.getPropertyPriority = function(arg1){}
	CSSStyleDeclaration.prototype.getPropertyValue = function(arg1){}
	CSSStyleDeclaration.prototype.item = function(arg1){}
	CSSStyleDeclaration.prototype.removeProperty = function(arg1){}
	CSSStyleDeclaration.prototype.setProperty = function(arg1, arg2, arg3){}
}

/**
 * HTMLCollection class.
 * 
 * @class HTMLCollection
 * @returns {HTMLCollection}  
 */
function HTMLCollection() {
	// properties
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	// methods
	HTMLCollection.prototype.item = function(arg1){}
	HTMLCollection.prototype.namedItem = function(arg1){}
}

/**
 * NodeList class.
 * 
 * @class NodeList
 * @returns {NodeList}  
 */
function NodeList() {
	// properties
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	// methods
	NodeList.prototype.item = function(arg1){}
}

/**
 * HTMLScriptElement class.
 * 
 * @class HTMLScriptElement
 * @returns {HTMLScriptElement}  
 */
function HTMLScriptElement() {
	// properties
	/**
	 * @property {Number} ATTRIBUTE_NODE
	 */
	this.ATTRIBUTE_NODE = 1;
	/**
	 * @property {Number} CDATA_SECTION_NODE
	 */
	this.CDATA_SECTION_NODE = 1;
	/**
	 * @property {Number} COMMENT_NODE
	 */
	this.COMMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_FRAGMENT_NODE
	 */
	this.DOCUMENT_FRAGMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_NODE
	 */
	this.DOCUMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINED_BY
	 */
	this.DOCUMENT_POSITION_CONTAINED_BY = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINS
	 */
	this.DOCUMENT_POSITION_CONTAINS = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_DISCONNECTED
	 */
	this.DOCUMENT_POSITION_DISCONNECTED = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_FOLLOWING
	 */
	this.DOCUMENT_POSITION_FOLLOWING = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC
	 */
	this.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_PRECEDING
	 */
	this.DOCUMENT_POSITION_PRECEDING = 1;
	/**
	 * @property {Number} DOCUMENT_TYPE_NODE
	 */
	this.DOCUMENT_TYPE_NODE = 1;
	/**
	 * @property {Number} ELEMENT_NODE
	 */
	this.ELEMENT_NODE = 1;
	/**
	 * @property {Number} ENTITY_NODE
	 */
	this.ENTITY_NODE = 1;
	/**
	 * @property {Number} ENTITY_REFERENCE_NODE
	 */
	this.ENTITY_REFERENCE_NODE = 1;
	/**
	 * @property {Number} NOTATION_NODE
	 */
	this.NOTATION_NODE = 1;
	/**
	 * @property {Number} PROCESSING_INSTRUCTION_NODE
	 */
	this.PROCESSING_INSTRUCTION_NODE = 1;
	/**
	 * @property {Number} TEXT_NODE
	 */
	this.TEXT_NODE = 1;
	/**
	 * @property {Boolean} async
	 */
	this.async = true;
	/**
	 * @property {NamedNodeMap} attributes
	 */
	this.attributes = {};
	/**
	 * @property {String} baseURI
	 */
	this.baseURI = "";
	/**
	 * @property {String} charset
	 */
	this.charset = "";
	/**
	 * @property {Number} childElementCount
	 */
	this.childElementCount = 1;
	/**
	 * @property {NodeList} childNodes
	 */
	this.childNodes = {};
	/**
	 * @property {HTMLCollection} children
	 */
	this.children = {};
	/**
	 * @property {Object} classList
	 */
	this.classList = {};
	/**
	 * @property {String} className
	 */
	this.className = "";
	/**
	 * @property {Number} clientHeight
	 */
	this.clientHeight = 1;
	/**
	 * @property {Number} clientLeft
	 */
	this.clientLeft = 1;
	/**
	 * @property {Number} clientTop
	 */
	this.clientTop = 1;
	/**
	 * @property {Number} clientWidth
	 */
	this.clientWidth = 1;
	/**
	 * @property {String} contentEditable
	 */
	this.contentEditable = "";
	/**
	 * @property {Boolean} defer
	 */
	this.defer = true;
	/**
	 * @property {String} dir
	 */
	this.dir = "";
	/**
	 * @property {Boolean} draggable
	 */
	this.draggable = true;
	/**
	 * @property {String} event
	 */
	this.event = "";
	/**
	 * @property {Text} firstChild
	 */
	this.firstChild = {};
	/**
	 * @property {Object} firstElementChild
	 */
	this.firstElementChild = {};
	/**
	 * @property {Boolean} hidden
	 */
	this.hidden = true;
	/**
	 * @property {String} htmlFor
	 */
	this.htmlFor = "";
	/**
	 * @property {String} id
	 */
	this.id = "";
	/**
	 * @property {String} innerHTML
	 */
	this.innerHTML = "";
	/**
	 * @property {Boolean} isContentEditable
	 */
	this.isContentEditable = true;
	/**
	 * @property {String} lang
	 */
	this.lang = "";
	/**
	 * @property {Text} lastChild
	 */
	this.lastChild = {};
	/**
	 * @property {Object} lastElementChild
	 */
	this.lastElementChild = {};
	/**
	 * @property {String} localName
	 */
	this.localName = "";
	/**
	 * @property {String} namespaceURI
	 */
	this.namespaceURI = "";
	/**
	 * @property {HTMLPreElement} nextElementSibling
	 */
	this.nextElementSibling = {};
	/**
	 * @property {HTMLPreElement} nextSibling
	 */
	this.nextSibling = {};
	/**
	 * @property {String} nodeName
	 */
	this.nodeName = "";
	/**
	 * @property {Number} nodeType
	 */
	this.nodeType = 1;
	/**
	 * @property {Object} nodeValue
	 */
	this.nodeValue = {};
	/**
	 * @property {Number} offsetHeight
	 */
	this.offsetHeight = 1;
	/**
	 * @property {Number} offsetLeft
	 */
	this.offsetLeft = 1;
	/**
	 * @property {Object} offsetParent
	 */
	this.offsetParent = {};
	/**
	 * @property {Number} offsetTop
	 */
	this.offsetTop = 1;
	/**
	 * @property {Number} offsetWidth
	 */
	this.offsetWidth = 1;
	/**
	 * @property {HTMLDocument} ownerDocument
	 */
	this.ownerDocument = {};
	/**
	 * @property {HTMLBodyElement} parentNode
	 */
	this.parentNode = {};
	/**
	 * @property {Object} prefix
	 */
	this.prefix = {};
	/**
	 * @property {Object} previousElementSibling
	 */
	this.previousElementSibling = {};
	/**
	 * @property {Text} previousSibling
	 */
	this.previousSibling = {};
	/**
	 * @property {Number} scrollHeight
	 */
	this.scrollHeight = 1;
	/**
	 * @property {Number} scrollLeft
	 */
	this.scrollLeft = 1;
	/**
	 * @property {Number} scrollTop
	 */
	this.scrollTop = 1;
	/**
	 * @property {Number} scrollWidth
	 */
	this.scrollWidth = 1;
	/**
	 * @property {Boolean} spellcheck
	 */
	this.spellcheck = true;
	/**
	 * @property {String} src
	 */
	this.src = "";
	/**
	 * @property {CSSStyleDeclaration} style
	 */
	this.style = {};
	/**
	 * @property {Number} tabIndex
	 */
	this.tabIndex = 1;
	/**
	 * @property {String} tagName
	 */
	this.tagName = "";
	/**
	 * @property {String} text
	 */
	this.text = "";
	/**
	 * @property {String} textContent
	 */
	this.textContent = "";
	/**
	 * @property {String} title
	 */
	this.title = "";
	/**
	 * @property {String} type
	 */
	this.type = "";
	// methods
	HTMLScriptElement.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	HTMLScriptElement.prototype.appendChild = function(arg1){}
	HTMLScriptElement.prototype.blur = function(){}
	HTMLScriptElement.prototype.cloneNode = function(arg1){}
	HTMLScriptElement.prototype.compareDocumentPosition = function(arg1){}
	HTMLScriptElement.prototype.dispatchEvent = function(arg1){}
	HTMLScriptElement.prototype.focus = function(){}
	HTMLScriptElement.prototype.getAttribute = function(arg1){}
	HTMLScriptElement.prototype.getAttributeNS = function(arg1, arg2){}
	HTMLScriptElement.prototype.getAttributeNode = function(arg1){}
	HTMLScriptElement.prototype.getAttributeNodeNS = function(arg1, arg2){}
	HTMLScriptElement.prototype.getBoundingClientRect = function(){}
	HTMLScriptElement.prototype.getClientRects = function(){}
	HTMLScriptElement.prototype.getElementsByClassName = function(arg1){}
	HTMLScriptElement.prototype.getElementsByTagName = function(arg1){}
	HTMLScriptElement.prototype.getElementsByTagNameNS = function(arg1, arg2){}
	HTMLScriptElement.prototype.getFeature = function(arg1, arg2){}
	HTMLScriptElement.prototype.getUserData = function(arg1){}
	HTMLScriptElement.prototype.hasAttribute = function(arg1){}
	HTMLScriptElement.prototype.hasAttributeNS = function(arg1, arg2){}
	HTMLScriptElement.prototype.hasAttributes = function(){}
	HTMLScriptElement.prototype.hasChildNodes = function(){}
	HTMLScriptElement.prototype.insertBefore = function(arg1, arg2){}
	HTMLScriptElement.prototype.isDefaultNamespace = function(arg1){}
	HTMLScriptElement.prototype.isEqualNode = function(arg1){}
	HTMLScriptElement.prototype.isSameNode = function(arg1){}
	HTMLScriptElement.prototype.isSupported = function(arg1, arg2){}
	HTMLScriptElement.prototype.lookupNamespaceURI = function(arg1){}
	HTMLScriptElement.prototype.lookupPrefix = function(arg1){}
	HTMLScriptElement.prototype.mozMatchesSelector = function(arg1){}
	HTMLScriptElement.prototype.normalize = function(){}
	HTMLScriptElement.prototype.querySelector = function(arg1){}
	HTMLScriptElement.prototype.querySelectorAll = function(arg1){}
	HTMLScriptElement.prototype.releaseCapture = function(){}
	HTMLScriptElement.prototype.removeAttribute = function(arg1){}
	HTMLScriptElement.prototype.removeAttributeNS = function(arg1, arg2){}
	HTMLScriptElement.prototype.removeAttributeNode = function(arg1){}
	HTMLScriptElement.prototype.removeChild = function(arg1){}
	HTMLScriptElement.prototype.removeEventListener = function(arg1, arg2, arg3){}
	HTMLScriptElement.prototype.replaceChild = function(arg1, arg2){}
	HTMLScriptElement.prototype.scrollIntoView = function(arg1){}
	HTMLScriptElement.prototype.setAttribute = function(arg1, arg2){}
	HTMLScriptElement.prototype.setAttributeNS = function(arg1, arg2, arg3){}
	HTMLScriptElement.prototype.setAttributeNode = function(arg1){}
	HTMLScriptElement.prototype.setAttributeNodeNS = function(arg1){}
	HTMLScriptElement.prototype.setCapture = function(arg1){}
	HTMLScriptElement.prototype.setUserData = function(arg1, arg2, arg3){}
}

/**
 * HTMLHtmlElement class.
 * 
 * @class HTMLHtmlElement
 * @returns {HTMLHtmlElement}  
 */
function HTMLHtmlElement() {
	// properties
	/**
	 * @property {Number} ATTRIBUTE_NODE
	 */
	this.ATTRIBUTE_NODE = 1;
	/**
	 * @property {Number} CDATA_SECTION_NODE
	 */
	this.CDATA_SECTION_NODE = 1;
	/**
	 * @property {Number} COMMENT_NODE
	 */
	this.COMMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_FRAGMENT_NODE
	 */
	this.DOCUMENT_FRAGMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_NODE
	 */
	this.DOCUMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINED_BY
	 */
	this.DOCUMENT_POSITION_CONTAINED_BY = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINS
	 */
	this.DOCUMENT_POSITION_CONTAINS = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_DISCONNECTED
	 */
	this.DOCUMENT_POSITION_DISCONNECTED = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_FOLLOWING
	 */
	this.DOCUMENT_POSITION_FOLLOWING = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC
	 */
	this.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_PRECEDING
	 */
	this.DOCUMENT_POSITION_PRECEDING = 1;
	/**
	 * @property {Number} DOCUMENT_TYPE_NODE
	 */
	this.DOCUMENT_TYPE_NODE = 1;
	/**
	 * @property {Number} ELEMENT_NODE
	 */
	this.ELEMENT_NODE = 1;
	/**
	 * @property {Number} ENTITY_NODE
	 */
	this.ENTITY_NODE = 1;
	/**
	 * @property {Number} ENTITY_REFERENCE_NODE
	 */
	this.ENTITY_REFERENCE_NODE = 1;
	/**
	 * @property {Number} NOTATION_NODE
	 */
	this.NOTATION_NODE = 1;
	/**
	 * @property {Number} PROCESSING_INSTRUCTION_NODE
	 */
	this.PROCESSING_INSTRUCTION_NODE = 1;
	/**
	 * @property {Number} TEXT_NODE
	 */
	this.TEXT_NODE = 1;
	/**
	 * @property {NamedNodeMap} attributes
	 */
	this.attributes = {};
	/**
	 * @property {String} baseURI
	 */
	this.baseURI = "";
	/**
	 * @property {Number} childElementCount
	 */
	this.childElementCount = 1;
	/**
	 * @property {NodeList} childNodes
	 */
	this.childNodes = {};
	/**
	 * @property {HTMLCollection} children
	 */
	this.children = {};
	/**
	 * @property {Object} classList
	 */
	this.classList = {};
	/**
	 * @property {String} className
	 */
	this.className = "";
	/**
	 * @property {Number} clientHeight
	 */
	this.clientHeight = 1;
	/**
	 * @property {Number} clientLeft
	 */
	this.clientLeft = 1;
	/**
	 * @property {Number} clientTop
	 */
	this.clientTop = 1;
	/**
	 * @property {Number} clientWidth
	 */
	this.clientWidth = 1;
	/**
	 * @property {String} contentEditable
	 */
	this.contentEditable = "";
	/**
	 * @property {String} dir
	 */
	this.dir = "";
	/**
	 * @property {Boolean} draggable
	 */
	this.draggable = true;
	/**
	 * @property {HTMLHeadElement} firstChild
	 */
	this.firstChild = {};
	/**
	 * @property {HTMLHeadElement} firstElementChild
	 */
	this.firstElementChild = {};
	/**
	 * @property {Boolean} hidden
	 */
	this.hidden = true;
	/**
	 * @property {String} id
	 */
	this.id = "";
	/**
	 * @property {String} innerHTML
	 */
	this.innerHTML = "";
	/**
	 * @property {Boolean} isContentEditable
	 */
	this.isContentEditable = true;
	/**
	 * @property {String} lang
	 */
	this.lang = "";
	/**
	 * @property {HTMLBodyElement} lastChild
	 */
	this.lastChild = {};
	/**
	 * @property {HTMLBodyElement} lastElementChild
	 */
	this.lastElementChild = {};
	/**
	 * @property {String} localName
	 */
	this.localName = "";
	/**
	 * @property {String} namespaceURI
	 */
	this.namespaceURI = "";
	/**
	 * @property {Object} nextElementSibling
	 */
	this.nextElementSibling = {};
	/**
	 * @property {Object} nextSibling
	 */
	this.nextSibling = {};
	/**
	 * @property {String} nodeName
	 */
	this.nodeName = "";
	/**
	 * @property {Number} nodeType
	 */
	this.nodeType = 1;
	/**
	 * @property {Object} nodeValue
	 */
	this.nodeValue = {};
	/**
	 * @property {Number} offsetHeight
	 */
	this.offsetHeight = 1;
	/**
	 * @property {Number} offsetLeft
	 */
	this.offsetLeft = 1;
	/**
	 * @property {Object} offsetParent
	 */
	this.offsetParent = {};
	/**
	 * @property {Number} offsetTop
	 */
	this.offsetTop = 1;
	/**
	 * @property {Number} offsetWidth
	 */
	this.offsetWidth = 1;
	/**
	 * @property {HTMLDocument} ownerDocument
	 */
	this.ownerDocument = {};
	/**
	 * @property {HTMLDocument} parentNode
	 */
	this.parentNode = {};
	/**
	 * @property {Object} prefix
	 */
	this.prefix = {};
	/**
	 * @property {Object} previousElementSibling
	 */
	this.previousElementSibling = {};
	/**
	 * @property {Object} previousSibling
	 */
	this.previousSibling = {};
	/**
	 * @property {Number} scrollHeight
	 */
	this.scrollHeight = 1;
	/**
	 * @property {Number} scrollLeft
	 */
	this.scrollLeft = 1;
	/**
	 * @property {Number} scrollTop
	 */
	this.scrollTop = 1;
	/**
	 * @property {Number} scrollWidth
	 */
	this.scrollWidth = 1;
	/**
	 * @property {Boolean} spellcheck
	 */
	this.spellcheck = true;
	/**
	 * @property {CSSStyleDeclaration} style
	 */
	this.style = {};
	/**
	 * @property {Number} tabIndex
	 */
	this.tabIndex = 1;
	/**
	 * @property {String} tagName
	 */
	this.tagName = "";
	/**
	 * @property {String} textContent
	 */
	this.textContent = "";
	/**
	 * @property {String} title
	 */
	this.title = "";
	/**
	 * @property {String} version
	 */
	this.version = "";
	// methods
	HTMLHtmlElement.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	HTMLHtmlElement.prototype.appendChild = function(arg1){}
	HTMLHtmlElement.prototype.blur = function(){}
	HTMLHtmlElement.prototype.cloneNode = function(arg1){}
	HTMLHtmlElement.prototype.compareDocumentPosition = function(arg1){}
	HTMLHtmlElement.prototype.dispatchEvent = function(arg1){}
	HTMLHtmlElement.prototype.focus = function(){}
	HTMLHtmlElement.prototype.getAttribute = function(arg1){}
	HTMLHtmlElement.prototype.getAttributeNS = function(arg1, arg2){}
	HTMLHtmlElement.prototype.getAttributeNode = function(arg1){}
	HTMLHtmlElement.prototype.getAttributeNodeNS = function(arg1, arg2){}
	HTMLHtmlElement.prototype.getBoundingClientRect = function(){}
	HTMLHtmlElement.prototype.getClientRects = function(){}
	HTMLHtmlElement.prototype.getElementsByClassName = function(arg1){}
	HTMLHtmlElement.prototype.getElementsByTagName = function(arg1){}
	HTMLHtmlElement.prototype.getElementsByTagNameNS = function(arg1, arg2){}
	HTMLHtmlElement.prototype.getFeature = function(arg1, arg2){}
	HTMLHtmlElement.prototype.getUserData = function(arg1){}
	HTMLHtmlElement.prototype.hasAttribute = function(arg1){}
	HTMLHtmlElement.prototype.hasAttributeNS = function(arg1, arg2){}
	HTMLHtmlElement.prototype.hasAttributes = function(){}
	HTMLHtmlElement.prototype.hasChildNodes = function(){}
	HTMLHtmlElement.prototype.insertBefore = function(arg1, arg2){}
	HTMLHtmlElement.prototype.isDefaultNamespace = function(arg1){}
	HTMLHtmlElement.prototype.isEqualNode = function(arg1){}
	HTMLHtmlElement.prototype.isSameNode = function(arg1){}
	HTMLHtmlElement.prototype.isSupported = function(arg1, arg2){}
	HTMLHtmlElement.prototype.lookupNamespaceURI = function(arg1){}
	HTMLHtmlElement.prototype.lookupPrefix = function(arg1){}
	HTMLHtmlElement.prototype.mozMatchesSelector = function(arg1){}
	HTMLHtmlElement.prototype.normalize = function(){}
	HTMLHtmlElement.prototype.querySelector = function(arg1){}
	HTMLHtmlElement.prototype.querySelectorAll = function(arg1){}
	HTMLHtmlElement.prototype.releaseCapture = function(){}
	HTMLHtmlElement.prototype.removeAttribute = function(arg1){}
	HTMLHtmlElement.prototype.removeAttributeNS = function(arg1, arg2){}
	HTMLHtmlElement.prototype.removeAttributeNode = function(arg1){}
	HTMLHtmlElement.prototype.removeChild = function(arg1){}
	HTMLHtmlElement.prototype.removeEventListener = function(arg1, arg2, arg3){}
	HTMLHtmlElement.prototype.replaceChild = function(arg1, arg2){}
	HTMLHtmlElement.prototype.scrollIntoView = function(arg1){}
	HTMLHtmlElement.prototype.setAttribute = function(arg1, arg2){}
	HTMLHtmlElement.prototype.setAttributeNS = function(arg1, arg2, arg3){}
	HTMLHtmlElement.prototype.setAttributeNode = function(arg1){}
	HTMLHtmlElement.prototype.setAttributeNodeNS = function(arg1){}
	HTMLHtmlElement.prototype.setCapture = function(arg1){}
	HTMLHtmlElement.prototype.setUserData = function(arg1, arg2, arg3){}
}

/**
 * HTMLHeadElement class.
 * 
 * @class HTMLHeadElement
 * @returns {HTMLHeadElement}  
 */
function HTMLHeadElement() {
	// properties
	/**
	 * @property {Number} ATTRIBUTE_NODE
	 */
	this.ATTRIBUTE_NODE = 1;
	/**
	 * @property {Number} CDATA_SECTION_NODE
	 */
	this.CDATA_SECTION_NODE = 1;
	/**
	 * @property {Number} COMMENT_NODE
	 */
	this.COMMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_FRAGMENT_NODE
	 */
	this.DOCUMENT_FRAGMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_NODE
	 */
	this.DOCUMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINED_BY
	 */
	this.DOCUMENT_POSITION_CONTAINED_BY = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINS
	 */
	this.DOCUMENT_POSITION_CONTAINS = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_DISCONNECTED
	 */
	this.DOCUMENT_POSITION_DISCONNECTED = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_FOLLOWING
	 */
	this.DOCUMENT_POSITION_FOLLOWING = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC
	 */
	this.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_PRECEDING
	 */
	this.DOCUMENT_POSITION_PRECEDING = 1;
	/**
	 * @property {Number} DOCUMENT_TYPE_NODE
	 */
	this.DOCUMENT_TYPE_NODE = 1;
	/**
	 * @property {Number} ELEMENT_NODE
	 */
	this.ELEMENT_NODE = 1;
	/**
	 * @property {Number} ENTITY_NODE
	 */
	this.ENTITY_NODE = 1;
	/**
	 * @property {Number} ENTITY_REFERENCE_NODE
	 */
	this.ENTITY_REFERENCE_NODE = 1;
	/**
	 * @property {Number} NOTATION_NODE
	 */
	this.NOTATION_NODE = 1;
	/**
	 * @property {Number} PROCESSING_INSTRUCTION_NODE
	 */
	this.PROCESSING_INSTRUCTION_NODE = 1;
	/**
	 * @property {Number} TEXT_NODE
	 */
	this.TEXT_NODE = 1;
	/**
	 * @property {NamedNodeMap} attributes
	 */
	this.attributes = {};
	/**
	 * @property {String} baseURI
	 */
	this.baseURI = "";
	/**
	 * @property {Number} childElementCount
	 */
	this.childElementCount = 1;
	/**
	 * @property {NodeList} childNodes
	 */
	this.childNodes = {};
	/**
	 * @property {HTMLCollection} children
	 */
	this.children = {};
	/**
	 * @property {Object} classList
	 */
	this.classList = {};
	/**
	 * @property {String} className
	 */
	this.className = "";
	/**
	 * @property {Number} clientHeight
	 */
	this.clientHeight = 1;
	/**
	 * @property {Number} clientLeft
	 */
	this.clientLeft = 1;
	/**
	 * @property {Number} clientTop
	 */
	this.clientTop = 1;
	/**
	 * @property {Number} clientWidth
	 */
	this.clientWidth = 1;
	/**
	 * @property {String} contentEditable
	 */
	this.contentEditable = "";
	/**
	 * @property {String} dir
	 */
	this.dir = "";
	/**
	 * @property {Boolean} draggable
	 */
	this.draggable = true;
	/**
	 * @property {Text} firstChild
	 */
	this.firstChild = {};
	/**
	 * @property {HTMLTitleElement} firstElementChild
	 */
	this.firstElementChild = {};
	/**
	 * @property {Boolean} hidden
	 */
	this.hidden = true;
	/**
	 * @property {String} id
	 */
	this.id = "";
	/**
	 * @property {String} innerHTML
	 */
	this.innerHTML = "";
	/**
	 * @property {Boolean} isContentEditable
	 */
	this.isContentEditable = true;
	/**
	 * @property {String} lang
	 */
	this.lang = "";
	/**
	 * @property {Text} lastChild
	 */
	this.lastChild = {};
	/**
	 * @property {HTMLTitleElement} lastElementChild
	 */
	this.lastElementChild = {};
	/**
	 * @property {String} localName
	 */
	this.localName = "";
	/**
	 * @property {String} namespaceURI
	 */
	this.namespaceURI = "";
	/**
	 * @property {HTMLBodyElement} nextElementSibling
	 */
	this.nextElementSibling = {};
	/**
	 * @property {Text} nextSibling
	 */
	this.nextSibling = {};
	/**
	 * @property {String} nodeName
	 */
	this.nodeName = "";
	/**
	 * @property {Number} nodeType
	 */
	this.nodeType = 1;
	/**
	 * @property {Object} nodeValue
	 */
	this.nodeValue = {};
	/**
	 * @property {Number} offsetHeight
	 */
	this.offsetHeight = 1;
	/**
	 * @property {Number} offsetLeft
	 */
	this.offsetLeft = 1;
	/**
	 * @property {Object} offsetParent
	 */
	this.offsetParent = {};
	/**
	 * @property {Number} offsetTop
	 */
	this.offsetTop = 1;
	/**
	 * @property {Number} offsetWidth
	 */
	this.offsetWidth = 1;
	/**
	 * @property {HTMLDocument} ownerDocument
	 */
	this.ownerDocument = {};
	/**
	 * @property {HTMLHtmlElement} parentNode
	 */
	this.parentNode = {};
	/**
	 * @property {Object} prefix
	 */
	this.prefix = {};
	/**
	 * @property {Object} previousElementSibling
	 */
	this.previousElementSibling = {};
	/**
	 * @property {Object} previousSibling
	 */
	this.previousSibling = {};
	/**
	 * @property {Number} scrollHeight
	 */
	this.scrollHeight = 1;
	/**
	 * @property {Number} scrollLeft
	 */
	this.scrollLeft = 1;
	/**
	 * @property {Number} scrollTop
	 */
	this.scrollTop = 1;
	/**
	 * @property {Number} scrollWidth
	 */
	this.scrollWidth = 1;
	/**
	 * @property {Boolean} spellcheck
	 */
	this.spellcheck = true;
	/**
	 * @property {CSSStyleDeclaration} style
	 */
	this.style = {};
	/**
	 * @property {Number} tabIndex
	 */
	this.tabIndex = 1;
	/**
	 * @property {String} tagName
	 */
	this.tagName = "";
	/**
	 * @property {String} textContent
	 */
	this.textContent = "";
	/**
	 * @property {String} title
	 */
	this.title = "";
	// methods
	HTMLHeadElement.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	HTMLHeadElement.prototype.appendChild = function(arg1){}
	HTMLHeadElement.prototype.blur = function(){}
	HTMLHeadElement.prototype.cloneNode = function(arg1){}
	HTMLHeadElement.prototype.compareDocumentPosition = function(arg1){}
	HTMLHeadElement.prototype.dispatchEvent = function(arg1){}
	HTMLHeadElement.prototype.focus = function(){}
	HTMLHeadElement.prototype.getAttribute = function(arg1){}
	HTMLHeadElement.prototype.getAttributeNS = function(arg1, arg2){}
	HTMLHeadElement.prototype.getAttributeNode = function(arg1){}
	HTMLHeadElement.prototype.getAttributeNodeNS = function(arg1, arg2){}
	HTMLHeadElement.prototype.getBoundingClientRect = function(){}
	HTMLHeadElement.prototype.getClientRects = function(){}
	HTMLHeadElement.prototype.getElementsByClassName = function(arg1){}
	HTMLHeadElement.prototype.getElementsByTagName = function(arg1){}
	HTMLHeadElement.prototype.getElementsByTagNameNS = function(arg1, arg2){}
	HTMLHeadElement.prototype.getFeature = function(arg1, arg2){}
	HTMLHeadElement.prototype.getUserData = function(arg1){}
	HTMLHeadElement.prototype.hasAttribute = function(arg1){}
	HTMLHeadElement.prototype.hasAttributeNS = function(arg1, arg2){}
	HTMLHeadElement.prototype.hasAttributes = function(){}
	HTMLHeadElement.prototype.hasChildNodes = function(){}
	HTMLHeadElement.prototype.insertBefore = function(arg1, arg2){}
	HTMLHeadElement.prototype.isDefaultNamespace = function(arg1){}
	HTMLHeadElement.prototype.isEqualNode = function(arg1){}
	HTMLHeadElement.prototype.isSameNode = function(arg1){}
	HTMLHeadElement.prototype.isSupported = function(arg1, arg2){}
	HTMLHeadElement.prototype.lookupNamespaceURI = function(arg1){}
	HTMLHeadElement.prototype.lookupPrefix = function(arg1){}
	HTMLHeadElement.prototype.mozMatchesSelector = function(arg1){}
	HTMLHeadElement.prototype.normalize = function(){}
	HTMLHeadElement.prototype.querySelector = function(arg1){}
	HTMLHeadElement.prototype.querySelectorAll = function(arg1){}
	HTMLHeadElement.prototype.releaseCapture = function(){}
	HTMLHeadElement.prototype.removeAttribute = function(arg1){}
	HTMLHeadElement.prototype.removeAttributeNS = function(arg1, arg2){}
	HTMLHeadElement.prototype.removeAttributeNode = function(arg1){}
	HTMLHeadElement.prototype.removeChild = function(arg1){}
	HTMLHeadElement.prototype.removeEventListener = function(arg1, arg2, arg3){}
	HTMLHeadElement.prototype.replaceChild = function(arg1, arg2){}
	HTMLHeadElement.prototype.scrollIntoView = function(arg1){}
	HTMLHeadElement.prototype.setAttribute = function(arg1, arg2){}
	HTMLHeadElement.prototype.setAttributeNS = function(arg1, arg2, arg3){}
	HTMLHeadElement.prototype.setAttributeNode = function(arg1){}
	HTMLHeadElement.prototype.setAttributeNodeNS = function(arg1){}
	HTMLHeadElement.prototype.setCapture = function(arg1){}
	HTMLHeadElement.prototype.setUserData = function(arg1, arg2, arg3){}
}

/**
 * HTMLTitleElement class.
 * 
 * @class HTMLTitleElement
 * @returns {HTMLTitleElement}  
 */
function HTMLTitleElement() {
	// properties
	/**
	 * @property {Number} ATTRIBUTE_NODE
	 */
	this.ATTRIBUTE_NODE = 1;
	/**
	 * @property {Number} CDATA_SECTION_NODE
	 */
	this.CDATA_SECTION_NODE = 1;
	/**
	 * @property {Number} COMMENT_NODE
	 */
	this.COMMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_FRAGMENT_NODE
	 */
	this.DOCUMENT_FRAGMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_NODE
	 */
	this.DOCUMENT_NODE = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINED_BY
	 */
	this.DOCUMENT_POSITION_CONTAINED_BY = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_CONTAINS
	 */
	this.DOCUMENT_POSITION_CONTAINS = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_DISCONNECTED
	 */
	this.DOCUMENT_POSITION_DISCONNECTED = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_FOLLOWING
	 */
	this.DOCUMENT_POSITION_FOLLOWING = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC
	 */
	this.DOCUMENT_POSITION_IMPLEMENTATION_SPECIFIC = 1;
	/**
	 * @property {Number} DOCUMENT_POSITION_PRECEDING
	 */
	this.DOCUMENT_POSITION_PRECEDING = 1;
	/**
	 * @property {Number} DOCUMENT_TYPE_NODE
	 */
	this.DOCUMENT_TYPE_NODE = 1;
	/**
	 * @property {Number} ELEMENT_NODE
	 */
	this.ELEMENT_NODE = 1;
	/**
	 * @property {Number} ENTITY_NODE
	 */
	this.ENTITY_NODE = 1;
	/**
	 * @property {Number} ENTITY_REFERENCE_NODE
	 */
	this.ENTITY_REFERENCE_NODE = 1;
	/**
	 * @property {Number} NOTATION_NODE
	 */
	this.NOTATION_NODE = 1;
	/**
	 * @property {Number} PROCESSING_INSTRUCTION_NODE
	 */
	this.PROCESSING_INSTRUCTION_NODE = 1;
	/**
	 * @property {Number} TEXT_NODE
	 */
	this.TEXT_NODE = 1;
	/**
	 * @property {NamedNodeMap} attributes
	 */
	this.attributes = {};
	/**
	 * @property {String} baseURI
	 */
	this.baseURI = "";
	/**
	 * @property {Number} childElementCount
	 */
	this.childElementCount = 1;
	/**
	 * @property {NodeList} childNodes
	 */
	this.childNodes = {};
	/**
	 * @property {HTMLCollection} children
	 */
	this.children = {};
	/**
	 * @property {Object} classList
	 */
	this.classList = {};
	/**
	 * @property {String} className
	 */
	this.className = "";
	/**
	 * @property {Number} clientHeight
	 */
	this.clientHeight = 1;
	/**
	 * @property {Number} clientLeft
	 */
	this.clientLeft = 1;
	/**
	 * @property {Number} clientTop
	 */
	this.clientTop = 1;
	/**
	 * @property {Number} clientWidth
	 */
	this.clientWidth = 1;
	/**
	 * @property {String} contentEditable
	 */
	this.contentEditable = "";
	/**
	 * @property {String} dir
	 */
	this.dir = "";
	/**
	 * @property {Boolean} draggable
	 */
	this.draggable = true;
	/**
	 * @property {Text} firstChild
	 */
	this.firstChild = {};
	/**
	 * @property {Object} firstElementChild
	 */
	this.firstElementChild = {};
	/**
	 * @property {Boolean} hidden
	 */
	this.hidden = true;
	/**
	 * @property {String} id
	 */
	this.id = "";
	/**
	 * @property {String} innerHTML
	 */
	this.innerHTML = "";
	/**
	 * @property {Boolean} isContentEditable
	 */
	this.isContentEditable = true;
	/**
	 * @property {String} lang
	 */
	this.lang = "";
	/**
	 * @property {Text} lastChild
	 */
	this.lastChild = {};
	/**
	 * @property {Object} lastElementChild
	 */
	this.lastElementChild = {};
	/**
	 * @property {String} localName
	 */
	this.localName = "";
	/**
	 * @property {String} namespaceURI
	 */
	this.namespaceURI = "";
	/**
	 * @property {Object} nextElementSibling
	 */
	this.nextElementSibling = {};
	/**
	 * @property {Text} nextSibling
	 */
	this.nextSibling = {};
	/**
	 * @property {String} nodeName
	 */
	this.nodeName = "";
	/**
	 * @property {Number} nodeType
	 */
	this.nodeType = 1;
	/**
	 * @property {Object} nodeValue
	 */
	this.nodeValue = {};
	/**
	 * @property {Number} offsetHeight
	 */
	this.offsetHeight = 1;
	/**
	 * @property {Number} offsetLeft
	 */
	this.offsetLeft = 1;
	/**
	 * @property {Object} offsetParent
	 */
	this.offsetParent = {};
	/**
	 * @property {Number} offsetTop
	 */
	this.offsetTop = 1;
	/**
	 * @property {Number} offsetWidth
	 */
	this.offsetWidth = 1;
	/**
	 * @property {HTMLDocument} ownerDocument
	 */
	this.ownerDocument = {};
	/**
	 * @property {HTMLHeadElement} parentNode
	 */
	this.parentNode = {};
	/**
	 * @property {Object} prefix
	 */
	this.prefix = {};
	/**
	 * @property {Object} previousElementSibling
	 */
	this.previousElementSibling = {};
	/**
	 * @property {Text} previousSibling
	 */
	this.previousSibling = {};
	/**
	 * @property {Number} scrollHeight
	 */
	this.scrollHeight = 1;
	/**
	 * @property {Number} scrollLeft
	 */
	this.scrollLeft = 1;
	/**
	 * @property {Number} scrollTop
	 */
	this.scrollTop = 1;
	/**
	 * @property {Number} scrollWidth
	 */
	this.scrollWidth = 1;
	/**
	 * @property {Boolean} spellcheck
	 */
	this.spellcheck = true;
	/**
	 * @property {CSSStyleDeclaration} style
	 */
	this.style = {};
	/**
	 * @property {Number} tabIndex
	 */
	this.tabIndex = 1;
	/**
	 * @property {String} tagName
	 */
	this.tagName = "";
	/**
	 * @property {String} text
	 */
	this.text = "";
	/**
	 * @property {String} textContent
	 */
	this.textContent = "";
	/**
	 * @property {String} title
	 */
	this.title = "";
	// methods
	HTMLTitleElement.prototype.addEventListener = function(arg1, arg2, arg3, arg4){}
	HTMLTitleElement.prototype.appendChild = function(arg1){}
	HTMLTitleElement.prototype.blur = function(){}
	HTMLTitleElement.prototype.cloneNode = function(arg1){}
	HTMLTitleElement.prototype.compareDocumentPosition = function(arg1){}
	HTMLTitleElement.prototype.dispatchEvent = function(arg1){}
	HTMLTitleElement.prototype.focus = function(){}
	HTMLTitleElement.prototype.getAttribute = function(arg1){}
	HTMLTitleElement.prototype.getAttributeNS = function(arg1, arg2){}
	HTMLTitleElement.prototype.getAttributeNode = function(arg1){}
	HTMLTitleElement.prototype.getAttributeNodeNS = function(arg1, arg2){}
	HTMLTitleElement.prototype.getBoundingClientRect = function(){}
	HTMLTitleElement.prototype.getClientRects = function(){}
	HTMLTitleElement.prototype.getElementsByClassName = function(arg1){}
	HTMLTitleElement.prototype.getElementsByTagName = function(arg1){}
	HTMLTitleElement.prototype.getElementsByTagNameNS = function(arg1, arg2){}
	HTMLTitleElement.prototype.getFeature = function(arg1, arg2){}
	HTMLTitleElement.prototype.getUserData = function(arg1){}
	HTMLTitleElement.prototype.hasAttribute = function(arg1){}
	HTMLTitleElement.prototype.hasAttributeNS = function(arg1, arg2){}
	HTMLTitleElement.prototype.hasAttributes = function(){}
	HTMLTitleElement.prototype.hasChildNodes = function(){}
	HTMLTitleElement.prototype.insertBefore = function(arg1, arg2){}
	HTMLTitleElement.prototype.isDefaultNamespace = function(arg1){}
	HTMLTitleElement.prototype.isEqualNode = function(arg1){}
	HTMLTitleElement.prototype.isSameNode = function(arg1){}
	HTMLTitleElement.prototype.isSupported = function(arg1, arg2){}
	HTMLTitleElement.prototype.lookupNamespaceURI = function(arg1){}
	HTMLTitleElement.prototype.lookupPrefix = function(arg1){}
	HTMLTitleElement.prototype.mozMatchesSelector = function(arg1){}
	HTMLTitleElement.prototype.normalize = function(){}
	HTMLTitleElement.prototype.querySelector = function(arg1){}
	HTMLTitleElement.prototype.querySelectorAll = function(arg1){}
	HTMLTitleElement.prototype.releaseCapture = function(){}
	HTMLTitleElement.prototype.removeAttribute = function(arg1){}
	HTMLTitleElement.prototype.removeAttributeNS = function(arg1, arg2){}
	HTMLTitleElement.prototype.removeAttributeNode = function(arg1){}
	HTMLTitleElement.prototype.removeChild = function(arg1){}
	HTMLTitleElement.prototype.removeEventListener = function(arg1, arg2, arg3){}
	HTMLTitleElement.prototype.replaceChild = function(arg1, arg2){}
	HTMLTitleElement.prototype.scrollIntoView = function(arg1){}
	HTMLTitleElement.prototype.setAttribute = function(arg1, arg2){}
	HTMLTitleElement.prototype.setAttributeNS = function(arg1, arg2, arg3){}
	HTMLTitleElement.prototype.setAttributeNode = function(arg1){}
	HTMLTitleElement.prototype.setAttributeNodeNS = function(arg1){}
	HTMLTitleElement.prototype.setCapture = function(arg1){}
	HTMLTitleElement.prototype.setUserData = function(arg1, arg2, arg3){}
}

/**
 * DOMImplementation class.
 * 
 * @class DOMImplementation
 * @returns {DOMImplementation}  
 */
function DOMImplementation() {
	// properties
	// methods
	DOMImplementation.prototype.createDocument = function(arg1, arg2, arg3){}
	DOMImplementation.prototype.createDocumentType = function(arg1, arg2, arg3){}
	DOMImplementation.prototype.createHTMLDocument = function(arg1){}
	DOMImplementation.prototype.hasFeature = function(arg1, arg2){}
}

/**
 * StyleSheetList class.
 * 
 * @class StyleSheetList
 * @returns {StyleSheetList}  
 */
function StyleSheetList() {
	// properties
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	// methods
	StyleSheetList.prototype.item = function(arg1){}
}

/**
 * StorageList class.
 * 
 * @class StorageList
 * @returns {StorageList}  
 */
function StorageList() {
	// properties
	// methods
	StorageList.prototype.namedItem = function(arg1){}
}

/**
 * History class.
 * 
 * @class History
 * @returns {History}  
 */
function History() {
	// properties
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	/**
	 * @property {Number} next
	 */
	this.next = 1;
	/**
	 * @property {Number} previous
	 */
	this.previous = 1;
	// methods
	History.prototype.back = function(){}
	History.prototype.current = function(){}
	History.prototype.forward = function(){}
	History.prototype.go = function(arg1){}
	History.prototype.item = function(arg1){}
	History.prototype.pushState = function(arg1, arg2, arg3){}
	History.prototype.replaceState = function(arg1, arg2, arg3){}
}

/**
 * Storage class.
 * 
 * @class Storage
 * @returns {Storage}  
 */
function Storage() {
	// properties
	// methods
}

/**
 * BarProp class.
 * 
 * @class BarProp
 * @returns {BarProp}  
 */
function BarProp() {
	// properties
	/**
	 * @property {Boolean} visible
	 */
	this.visible = true;
	// methods
}

/**
 * IDBFactory class.
 * 
 * @class IDBFactory
 * @returns {IDBFactory}  
 */
function IDBFactory() {
	// properties
	// methods
	IDBFactory.prototype.open = function(arg1){}
}

/**
 * Navigator class.
 * 
 * @class Navigator
 * @returns {Navigator}  
 */
function Navigator() {
	// properties
	/**
	 * @property {String} appCodeName
	 */
	this.appCodeName = "";
	/**
	 * @property {String} appName
	 */
	this.appName = "";
	/**
	 * @property {Number} appVersion
	 */
	this.appVersion = 1;
	/**
	 * @property {Number} buildID
	 */
	this.buildID = 1;
	/**
	 * @property {Boolean} cookieEnabled
	 */
	this.cookieEnabled = true;
	/**
	 * @property {GeoGeolocation} geolocation
	 */
	this.geolocation = {};
	/**
	 * @property {String} language
	 */
	this.language = "";
	/**
	 * @property {MimeTypeArray} mimeTypes
	 */
	this.mimeTypes = {};
	/**
	 * @property {DesktopNotificationCenter} mozNotification
	 */
	this.mozNotification = {};
	/**
	 * @property {Boolean} onLine
	 */
	this.onLine = true;
	/**
	 * @property {String} oscpu
	 */
	this.oscpu = "";
	/**
	 * @property {String} platform
	 */
	this.platform = "";
	/**
	 * @property {PluginArray} plugins
	 */
	this.plugins = {};
	/**
	 * @property {String} product
	 */
	this.product = "";
	/**
	 * @property {Number} productSub
	 */
	this.productSub = 1;
	/**
	 * @property {String} securityPolicy
	 */
	this.securityPolicy = "";
	/**
	 * @property {String} userAgent
	 */
	this.userAgent = "";
	/**
	 * @property {String} vendor
	 */
	this.vendor = "";
	/**
	 * @property {String} vendorSub
	 */
	this.vendorSub = "";
	// methods
	Navigator.prototype.javaEnabled = function(){}
	Navigator.prototype.mozIsLocallyAvailable = function(arg1, arg2){}
	Navigator.prototype.registerContentHandler = function(arg1, arg2, arg3){}
	Navigator.prototype.registerProtocolHandler = function(arg1, arg2, arg3){}
	Navigator.prototype.taintEnabled = function(){}
}

/**
 * GeoGeolocation class.
 * 
 * @class GeoGeolocation
 * @returns {GeoGeolocation}  
 */
function GeoGeolocation() {
	// properties
	// methods
	GeoGeolocation.prototype.clearWatch = function(arg1){}
	GeoGeolocation.prototype.getCurrentPosition = function(arg1, arg2, arg3){}
	GeoGeolocation.prototype.watchPosition = function(arg1, arg2, arg3){}
}

/**
 * MimeTypeArray class.
 * 
 * @class MimeTypeArray
 * @returns {MimeTypeArray}  
 */
function MimeTypeArray() {
	// properties
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	// methods
	MimeTypeArray.prototype.item = function(arg1){}
	MimeTypeArray.prototype.namedItem = function(arg1){}
}

/**
 * DesktopNotificationCenter class.
 * 
 * @class DesktopNotificationCenter
 * @returns {DesktopNotificationCenter}  
 */
function DesktopNotificationCenter() {
	// properties
	// methods
	DesktopNotificationCenter.prototype.createNotification = function(arg1, arg2, arg3){}
}

/**
 * PluginArray class.
 * 
 * @class PluginArray
 * @returns {PluginArray}  
 */
function PluginArray() {
	// properties
	/**
	 * @property {Number} length
	 */
	this.length = 1;
	// methods
	PluginArray.prototype.item = function(arg1){}
	PluginArray.prototype.namedItem = function(arg1){}
	PluginArray.prototype.refresh = function(arg1){}
}

/**
 * Screen class.
 * 
 * @class Screen
 * @returns {Screen}  
 */
function Screen() {
	// properties
	/**
	 * @property {Number} availHeight
	 */
	this.availHeight = 1;
	/**
	 * @property {Number} availLeft
	 */
	this.availLeft = 1;
	/**
	 * @property {Number} availTop
	 */
	this.availTop = 1;
	/**
	 * @property {Number} availWidth
	 */
	this.availWidth = 1;
	/**
	 * @property {Number} colorDepth
	 */
	this.colorDepth = 1;
	/**
	 * @property {Number} height
	 */
	this.height = 1;
	/**
	 * @property {Number} left
	 */
	this.left = 1;
	/**
	 * @property {Number} pixelDepth
	 */
	this.pixelDepth = 1;
	/**
	 * @property {Number} top
	 */
	this.top = 1;
	/**
	 * @property {Number} width
	 */
	this.width = 1;
	// methods
}

var window = new Window();
global = window;