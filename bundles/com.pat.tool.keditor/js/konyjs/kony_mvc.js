kony.mvc = kony.mvc || {};
kony.utils = kony.utils || {};

//Dictionary<viewID,controller>
kony.mvc.formView2ControllerMap = {};

// Utility Functions
function accessorDescriptor(field, fun) {
    var desc = { enumerable: true, configurable: true };
    desc[field] = fun;
    return desc;
}

function defineGetter(obj, prop, get) {
    if (Object.defineProperty)
        return Object.defineProperty(obj, prop, accessorDescriptor("get", get));
    if (Object.prototype.__defineGetter__)
        return obj.__defineGetter__(prop, get);

    throw new Error("browser does not support getters");
}

function defineSetter(obj, prop, set) {
    if (Object.defineProperty)
        return Object.defineProperty(obj, prop, accessorDescriptor("set", set));
    if (Object.prototype.__defineSetter__)
        return obj.__defineSetter__(prop, set);

    throw new Error("browser does not support setters");
}

//Base controller. This has view access to it.
kony.mvc.BaseController = function ()
{
    var viewModel;
    defineGetter(this, "view", function () {
        viewModel = viewModel || this.__initializeView(this)
        return viewModel;
    });
    defineSetter(this, "view", function (val) {
        viewModel = val;
    });
}

inheritsFrom = function (child, parent)
{
    child.prototype = Object.create(parent.prototype);
};

//Form Controller
kony.mvc.FormController = function (viewId1) {
    this.__initializeView = function (objController) {
        var retForm = null;
        formCreateFunc = require(objController.viewId);
        var formConfig = formCreateFunc(objController);
        if (Object.prototype.toString.call(formConfig) === '[object Array]') {
            retForm = new kony.ui.Form2(formConfig[0], formConfig[1], formConfig[2]);
        }
        else {
            retForm = new kony.ui.Form2(formConfig);
        }
        kony.mvc.formView2ControllerMap[objController.viewId] = objController;
        return retForm;
    }
    this.show = function (context) {
        if (null != this.view) {
            this.view.show(context);
        }
    }

    this.viewId = viewId1;
    kony.mvc.BaseController.call(this);
};
inheritsFrom(kony.mvc.FormController, kony.mvc.BaseController);


//Template controller
kony.mvc.TemplateController = function (viewId1, newID) {
    this.__initializeView = function (objController) {
        var retForm = null;
        require([objController.viewId], function (formCreateFunc) {
            retForm = formCreateFunc(objController);
        });

        if (newID != null) {
            kony.mvc.formView2ControllerMap[newID] = objController;
        }
        else {
            kony.mvc.formView2ControllerMap[retForm.id] = objController;
        }
        return retForm;
    }

    this.viewId = viewId1;
    kony.mvc.BaseController.call(this);    

    //Notify parent form/template with an action. callback is name of the function to be executed on parent's context
    this.executeOnParent = function (callback, args) {
        this.view.executeOnParent(callback, args);
    }
};
inheritsFrom(kony.mvc.TemplateController, kony.mvc.BaseController);
//****************************************************************



//Loads template views as files dynamically and instantiates those templates.
//Typically these will return flex container objects.
kony.utils.LoadJSFile = function (fileName) {
    var retForm = null;
    controllerConfig = require(fileName);
    {
        retForm = controllerConfig;
    }
    return retForm;
}

//global function which is called by framework engine with just function name. (i.e. without function pointer)
//viewName - template name. functionName - function name as string.
//eventobject - original source of event. subargs - additional context information
konyExecuteInJSContext = function (templateView, functionName, subargs) {
    if (templateView.id in kony.mvc.formView2ControllerMap) {
        var viewName = templateView.id;
        //Execute the function on controller
        var tmpController = kony.mvc.formView2ControllerMap[viewName];
        if (null != tmpController) {
            if (typeof functionName === 'string' || functionName instanceof String) {
                tmpController[functionName].apply(tmpController, subargs);
            }
            else {
                functionName.apply(tmpController, subargs);
            }
        }
    }
    else {
        var eventobject = null;
        if (subargs.length > 0) {
            eventobject = subargs[0];
            subargs.shift();
        }
        //Execute in global context
        if (null != eventobject) {
            functionName.apply(eventobject, subargs);
        }
        else {
            functionName(eventobject, subargs);
        }
    }
}

konyCallFunctionOnClonedController = function (templateView, functionName, subargs) {
    var viewName = templateView.id;
    if (viewName in kony.mvc.formView2ControllerMap) {
        //Execute the function on controller
        var tmpController = kony.mvc.formView2ControllerMap[viewName];
        if (null != tmpController) {
            tmpController.view = templateView;
            if (typeof functionName === 'string' || functionName instanceof String) {
                tmpController[functionName].apply(tmpController, subargs);
            }
            else {
                functionName.apply(tmpController, subargs);
            }
        }
    }
    else {
        var eventobject = null;
        if (subargs.length > 0) {
            eventobject = subargs[0];
            subargs.shift();
        }
        //Execute in global context
        if (null != eventobject) {
            functionName.apply(eventobject, subargs);
        }
        else {
            functionName(eventobject, subargs);
        }
    }
}

//Return the controller for given friendly name. Creates a controller, if it is not created already.
kony.mvc.GetController = function (formFriendlyName, isForm) {
    var tmpController = null;
    var formID = formFriendlyName;

    var tmpFormName = kony.mvc.registry.get(formID);
    if (null != tmpFormName) {
        formID = tmpFormName;
    }
    else {
        kony.mvc.registry.add(formID, formID);
    }
    if (null != formID) {
        if (formID in kony.mvc.formView2ControllerMap) {
            tmpController = kony.mvc.formView2ControllerMap[formID];
        }
        else {
            var tmpControllerName = kony.mvc.registry.getControllerName(formFriendlyName);
            if (null == tmpControllerName) {
                tmpControllerName = formID + "Controller";
            }
            var config = kony.utils.LoadJSFile(tmpControllerName);
            if (isForm) {
                tmpController = new kony.mvc.FormController(formID);
            }
            else {
                tmpController = new kony.mvc.TemplateController(formID);
            }
            //var proto = tmpController.prototype;

            for (var key in config) {
                if (key != 'prototype' && key != 'viewId' && config.hasOwnProperty(key)) {
                    tmpController[key] = config[key];
                }
            }
            var x = tmpController.view;
        }
    }
    return tmpController;
}

kony.mvc.CreateMasterController = function (masterName, newMasterID, args) {
    var tmpController = null;
    var formID = masterName;

    var tmpFormName = kony.mvc.registry.get(formID);
    if (null != tmpFormName) {
        formID = tmpFormName;
    }
    else {
        kony.mvc.registry.add(formID, formID);
    }
    if (null != formID) {
        var tmpControllerName = kony.mvc.registry.getControllerName(masterName);
        if (null == tmpControllerName) {
            tmpControllerName = formID + "Controller";
        }
        var config = kony.utils.LoadJSFile(tmpControllerName);
        tmpController = new kony.mvc.TemplateController(formID, newMasterID);

        for (var key in config) {
            if (key != 'prototype' && key != 'viewId' && key != 'constructor' && config.hasOwnProperty(key)) {
                tmpController[key] = config[key];
            }
        }
        if (config.hasOwnProperty("constructor")) {
            config["constructor"].apply(tmpController, args);
        }
        var x = tmpController.view;
    }
    return tmpController;
}

//below method is called by framework when it needs to initialize subview templates dynamically
function konyInitializeController(friendlyName) {
    var tmpController = kony.mvc.GetController(friendlyName, false);
    return tmpController.view;
}

//below method is called by framework when it needs to initialize user widgets dynamically
function konyInitializeUserWidgetController(friendlyName, newMasterID, args) {
    var tmpController = kony.mvc.CreateMasterController(friendlyName, newMasterID, args);
    return tmpController.view;
}

//below method is called by framework when it needs to initialize form templates dynamically. Ex: overlayconfig etc
function konyInitializeFormController(friendlyName) {
    var tmpController = kony.mvc.GetController(friendlyName, true);
    return tmpController.view;
}

//Navigation call responsible for form navigation among forms with context.
kony.mvc.Navigation = function (formname) {
    this.formFriendlyName = formname;

    this.navigate = function (context) {
        var tmpController = kony.mvc.GetController(this.formFriendlyName, true);
        if (null != tmpController) {
            tmpController.show(context);
        } else {
            kony.print("########## No controller is found to navigate #####")
        }
    };
};
//****************************************************************

///***************Master Manager************************
setMasterConfig = function (masterObject, masterName) {
    var configFileName = masterName + "Config";
    var config = {};
    var oneMasterConfig = require(configFileName);
    if (Object.keys(oneMasterConfig)[0] == masterName) {
        var master = oneMasterConfig[masterName];
        if (null != master["properties"]) {
            var propertiesOnMaster = master["properties"];
            setProperties(masterObject, propertiesOnMaster);
        }

        if (null != master["apis"]) {
            var propertiesOnMaster = master["apis"];
            setFunctions(masterObject, propertiesOnMaster);
        }

        if (null != master["events"]) {
            var propertiesOnMaster = master["events"];
            setEvents(masterObject, propertiesOnMaster);
        }
        return config;
    }
}

setProperties = function (masterObject, properties) {
    if (null == properties || null == masterObject)
        return;

    if (masterObject.id in kony.mvc.formView2ControllerMap) {
        var tmpController = kony.mvc.formView2ControllerMap[masterObject.id];
        if (null != tmpController) {
            for (i = 0; i < properties.length; i++) {
                var propName = properties[i];
                defineGetter(masterObject, propName, (function (propertyName) {
                    return function () {
                        var xx = tmpController.view.bottom;
                        return tmpController[propName];
                    }
                })(propName));
                defineSetter(masterObject, propName, (function (propertyName) {
                    return function (val) {
                        var xx = tmpController.view.bottom;
                        tmpController[propertyName] = val;
                    }
                })(propName));
            }
        }
    }
}

setFunctions = function (masterObject, properties) {
    if (null == properties || null == masterObject)
        return;

    if (masterObject.id in kony.mvc.formView2ControllerMap) {
        var tmpController = kony.mvc.formView2ControllerMap[masterObject.id];
        if (null != tmpController) {
            for (i = 0; i < properties.length; i++) {
                var propName = properties[i];
                var xx = tmpController.view.bottom;
                masterObject[propName] = tmpController[propName].bind(tmpController);
            }
        }
    }
}

setEvents = function (masterObject, properties) {
    if (null == properties || null == masterObject)
        return;

    if (masterObject.id in kony.mvc.formView2ControllerMap) {
        var tmpController = kony.mvc.formView2ControllerMap[masterObject.id];
        if (null != tmpController) {
            for (i = 0; i < properties.length; i++) {
                var propName = properties[i];
                defineGetter(masterObject, propName, (function (propertyName) {
                    return function () {
                        var xx = tmpController.view.bottom;
                        return tmpController[propName];
                    }
                })(propName));
                defineSetter(masterObject, propName, (function (propertyName) {
                    return function (val) {
                        var xx = tmpController.view.bottom;
                        tmpController[propertyName] = val;
                    }
                })(propName));
            }
        }
    }
}
///*****************************************************
//*********** Registry APIs ******************************
if (kony.mvc.registry == undefined) kony.mvc.registry = {};
registryMap = {};
kony.mvc.registry.add = function (friendlyName, formid, formCtrllrName) {
    if (friendlyName in registryMap) {
        kony.print("########## A form with friendly name " + friendlyName + " is already exists in registry.");
    }
    else {
        var formProps = {};
        formProps["name"] = formid;
        formProps["controllerName"] = formCtrllrName;
        registryMap[friendlyName] = formProps;
    }
};

kony.mvc.registry.remove = function (friendlyName) {
    if (friendlyName in registryMap) {
        delete registryMap[friendlyName];
    }
    else {
        kony.print("########## No form with friendly name " + friendlyName + " is found in registry");
    }
};

kony.mvc.registry.get = function (friendlyName) {
    if (friendlyName in registryMap) {
        var formProps = registryMap[friendlyName];
        if (null != formProps) {
            return formProps["name"];
        }
    }
    else {
        kony.print("########## No form with friendly name " + friendlyName + " is found in registry");
        return null;
    }
};

kony.mvc.registry.getControllerName = function (friendlyName) {
    if (friendlyName in registryMap) {
        var formProps = registryMap[friendlyName];
        if (null != formProps) {
            return formProps["controllerName"];
        }
    }
    else {
        kony.print("########## No form with friendly name " + friendlyName + " is found in registry");
        return null;
    }
};
//********************************************************