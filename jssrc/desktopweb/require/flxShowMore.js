define("flxShowMore", function() {
    return function(controller) {
        var flxShowMore = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxShowMore",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxShowMore.setDefaultUnit(kony.flex.DP);
        var flxLine = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "sknVerticalLine",
            "top": "0dp",
            "width": "3dp",
            "zIndex": 1
        }, {}, {});
        flxLine.setDefaultUnit(kony.flex.DP);
        flxLine.add();
        var flxRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox4",
            "top": "0dp",
            "width": "99%"
        }, {}, {});
        flxRow.setDefaultUnit(kony.flex.DP);
        var lblAddress = new kony.ui.RichText({
            "id": "lblAddress",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0be0eee345d9a4b",
            "text": "RichText",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow.add(lblAddress);
        var lblLine = new kony.ui.Label({
            "bottom": "2dp",
            "height": "1dp",
            "id": "lblLine",
            "isVisible": false,
            "left": "0dp",
            "skin": "CopyCopydefLabel4",
            "text": "label",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxShowMore.add(flxLine, flxRow, lblLine);
        return flxShowMore;
    }
})