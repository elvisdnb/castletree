define("userfrmDepositWithdrawalController", {
    // Arrangement ID    Account ID    Customer ID
    // AA203147R9BM    50006824    500008 - 3600
    // AA203142SY3T    50006848    500056 - 2400
    // AA20244D85DF    50004333    500046 - 10,000
    // http://ap-n1ffx1aerm97.temenos.cloud:8080
    // user@temenos.com/User123!
    //50008606 - AA21028T0WFJ - 500003
    //Type your controller code here 
    overrideContent: {},
    overrideNavFlow: "",
    noOfItemsPerPage: 6,
    gblSegPageCount: 1,
    accountsList: [],
    gblCurrency: "",
    //gblRedemptionFee: "0.00",
    //gblFullWithdrawalAmount: "0.00",
    gblCustomerId: "",
    gblArrangementId: "",
    gblDebitAccountNumber: "",
    //gblCustomerId:"500003",
    //gblArrangementId: "AA21028T0WFJ",
    //gblDebitAccountNumber: "50008606", 
    //-- // @ID - hardcode it for now - 50004888
    id: "",
    popupSegData: [],
    onNavigate: function(navObj) {
        kony.print("onNavigate Call >> navObj: " + JSON.stringify(navObj));
        this.initializeWithdrawal(navObj);
        this.bindEvents();
    },
    initializeWithdrawal: function(navObj) {
        try {
            this.showProgressIndicator();
            this.view.cusRadioWithdrawalType.options = [{
                "label": "Partial Withdrawal",
                "value": 1
            }, {
                "label": "Full Withdrawal",
                "value": 2
            }];
            this.gblDebitAccountNumber = navObj.lblAcNum;
            this.gblArrangementId = navObj.lblArrangementID;
            this.gblCurrency = navObj.lblCCY;
            this.gblCustomerId = navObj.customerId;
            this.setDepositHeaderDetails(navObj);
            this.view.cusTextFieldRedemptionFee.currencyCode = this.gblCurrency;
            this.view.cusTextFieldFullWithdralAmount.currencyCode = this.gblCurrency;
            this.view.cusTextFieldPrincipalAmnt.currencyCode = this.gblCurrency;
            this.view.cusTextFieldWithdrawalFee.currencyCode = this.gblCurrency;
            this.view.cusTextFieldOriginalInterest.currencyCode = this.gblCurrency;
            this.view.cusTextFieldNewInterest.currencyCode = this.gblCurrency;
            this.view.cusTextFieldAmount.currencyCode = this.gblCurrency;
            this.view.lblBilldetailsCurrency1.text = this.gblCurrency;
            this.view.lblBilldetailsWithdrawlFeeCurrency.text = this.gblCurrency;
            this.view.lblBillDetailsInterestAccruedCurrency.text = this.gblCurrency;
            this.view.lblBilldetailsTaxCurrency.text = this.gblCurrency;
            this.view.lblBilldetailsNetSettlementCurrency.text = this.gblCurrency;
            //this.view.cusTextFieldAmount.value = "0.00";
            // this.view.tempAmount.txtFloatText.text = "0.00";
            //this.view.tempWithdrawalFee.txtFloatText.text = "0.00";
            this.view.cusRadioWithdrawalType.value = 1;
            this.view.flxPartialWithdrawal.setVisibility(true);
            this.view.tempModeOfSettlement.txtFloatText.isReadOnly = true;
            this.view.ErrorAllert.setVisibility(false);
            this.assignDateToCalender();
            this.getDeposits();
            this.getPayInAccountDetails();
        } catch (err) {
            kony.print("Exception in initializeWithdrawal: " + err);
            this.closeProgressIndicator();
        }
    },
    setDepositHeaderDetails: function(newObj) {
        try {
            //TODO : take from segment data a navigation object
            this.view.FundDetails.lblAccountNumberResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAcNum);
            this.view.FundDetails.lblLoanTypeResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAcType); //"2345678";
            this.view.FundDetails.lblCurrencyValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblCCY);
            this.view.FundDetails.lblAmountValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblClearBal);
            this.view.FundDetails.lblStartDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblLockedAc);
            this.view.FundDetails.lblMaturityDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAvlBal);
        } catch (err) {
            kony.print("Error in setDepositHeaderDetails::: " + err);
            this.closeProgressIndicator();
        }
    },
    bindEvents: function() {
        try {
            var scope = this;
            this.view.postShow = this.postShowForm;
            this.view.cusTextFieldAmount.onChangeEvent = this.onChangeAmount;
            this.view.OverrideSegmentPopup.btnCancel.onClick = this.closeOverridePopup;
            this.view.OverrideSegmentPopup.imgClose.onTouchEnd = this.closeOverridePopup;
            this.view.OverrideSegmentPopup.btnProceed.onClick = this.onClickProceedOverride;
            this.view.cusButtonConfirm.onclickEvent = this.onClickOfSubmit;
            this.view.flxViewBillDetailsLink.onTouchStart = this.fetchBillDetails.bind(this);
            this.view.cusButtonCancel.onclickEvent = this.onClickCancel;
            this.view.ErrorAllert.imgClose.onTouchEnd = function() {
                scope.view.ErrorAllert.isVisible = false;
                scope.view.flxHeaderMenu.isVisible = true;
            };
            ///this.view.cusButtonOk.onclickEvent = this.SearchList;
            this.view.cusRadioWithdrawalType.onRadioChange = this.getSelectWithdrawalType;
            this.view.tempSettlementAccount.downArrowLbl.onTouchStart = this.showDropdownAccountList;
            this.view.AccountList2.segAccountList.onRowClick = this.getSegRowInfoDropdown;
            this.view.popupPayinPayout.segPayinPayout.onRowClick = this.getSegRowInfo;
            this.view.popupPayinPayout.flxPopUpClose.onTouchStart = this.advancedSearchClose;
            this.view.flxSettlementAccountSearch.onTouchStart = this.advancedSearchAccountsShow;
            this.view.flxBillDetailsIconClose.onTouchEnd = function() {
                scope.view.flxViewBillDetailsPopup.setVisibility(false);
            };
            this.view.cusButtonBillDetailsOkay.onclickEvent = this.onClickOfBillDetailsOkay;
            this.view.cusTextFieldAmount.validateRequired = "required";
        } catch (err) {
            kony.print("Exception in bindEvents: " + err);
            this.closeProgressIndicator();
        }
    },
    postShowForm: function() {
        try {
            this.view.flxViewBillDetailsPopup.setVisibility(false);
            this.view.flxSearchAccount.setVisibility(false);
            this.view.flxButtons.top = "500dp";
            this.view.flxViewBillDetailsLink.setEnabled(true);
            this.view.flxViewBillDetailsLink.skin = "";
            this.view.FundDetails.lblAccountNumberResponse.text = this.gblDebitAccountNumber;
            // this.view.tempAmount.flxFloatLableGrp.centerY="23%";
            this.view.MainTabs.flxContainerdeposits.skin = "sknSelected";
            this.view.cusTextFieldWithdrawalFee.value = this.formatAmountForCurrency("0.00");
            this.view.MainTabs.btnAccounts1.skin = "sknTabUnselected";
            this.view.MainTabs.btnAccounts2.skin = "sknTabSelected";
            this.view.MainTabs.btnAccounts3.skin = "sknTabUnselected";
            this.view.MainTabs.flxContainerAccount.skin = "sknNotselected";
            this.view.MainTabs.flxContainerdeposits.skin = "sknSelected";
            this.view.MainTabs.flxontainerLoans.skin = "sknNotselected";
            //this.view.tempAmount.flxFloatLableGrp.lblFloatLabel.skin ="sknFloatLblPostInput";
            //this.view.tempWithdrawalFee.flxFloatLableGrp.centerY="23%";
            //this.view.tempWithdrawalFee.flxFloatLableGrp.lblFloatLabel.skin="sknFloatLblPostInput";
            this.view.tempModeOfSettlement.flxFloatLableGrp.centerY = "23%";
            this.view.tempModeOfSettlement.flxFloatLableGrp.lblFloatLabel.skin = "sknFloatLblPostInput";
            this.view.tempSettlementAccount.flxFloatLableGrp.centerY = "23%";
            this.view.tempSettlementAccount.flxFloatLableGrp.lblFloatLabel.skin = "sknFloatLblPostInput";
            this.view.cusTextFieldPrincipalAmnt.disabled = true;
            this.view.cusTextFieldOriginalInterest.disabled = true;
            this.view.cusTextFieldNewInterest.disabled = true;
            this.view.cusTextFieldRedemptionFee.disabled = true;
            this.view.cusTextFieldFullWithdralAmount.disabled = true;
            this.view.cusTextBoxModeOfSettlement.disabled = true;
            this.disableViewBillDetailsLink();
            this.view.ErrorAllert.setVisibility(false);
            //is.view.flxFullWithdrawal.setVisibility(false);
            //this.view.tempModeOfSettlement.disabled=true;
            //this.view.tempSettlementAccount.txtFloatText.isReadOnly = true;
            //this.view.cusBillDetailsIconClose.isVisible = true;
            //this.view.cusButtonBillDetailsOkay.isVisible = true;
        } catch (err) {
            kony.print("Exception in postShowForm: " + err);
            this.closeProgressIndicator();
        }
    },
    onChangeAmount: function() {
        try {
            var amount = this.view.cusTextFieldAmount.value;
            kony.print("amount: " + amount);
            amount = this.removeAmountFormat(amount);
            if (Number(amount) > 0) {
                this.enableViewBillDetailsLink();
            } else {
                this.disableViewBillDetailsLink();
            }
        } catch (err) {
            kony.print("Exception in onChangeAmount: " + err);
            this.closeProgressIndicator();
        }
    },
    isFieldEmpty: function(str) {
        try {
            kony.print("Input str: " + str);
            if (str && str != "0.00" && str != "0" && str != "00" && str != "000" && str != "0,00") {
                return false;
            } else {
                return true;
            }
        } catch (err) {
            kony.print("Exception in isFieldEmpty: " + err);
            this.closeProgressIndicator();
        }
    },
    getDeposits: function(callback) {
        try {
            var serviceName = "FundsWithdrawal";
            // Get an instance of SDK
            var client = kony.sdk.getCurrentInstance();
            var integrationSvc = client.getIntegrationService(serviceName);
            var operationName = "getDeposits";
            var params = {};
            var headerParams = {
                "companyId": "NL0020001",
                "arrangementId": this.gblArrangementId //"AA20237F23ZS",
            };
            kony.print("headerParams: " + JSON.stringify(headerParams));
            var scope = this;
            integrationSvc.invokeOperation(operationName, headerParams, params, function(response) {
                try {
                    kony.print("getDepositsSuccessCalBack Success Response: " + JSON.stringify(response));
                    var objOutput = {};
                    if (response.opstatus === "0" || response.opstatus === 0) {
                        if (response.body) {
                            objOutput.depBalance = response.body[0].depBalance ? response.body[0].depBalance : "0.00";
                            scope.getOriginalInterestAtMaturity(objOutput);
                        }
                    }
                } catch (err) {
                    kony.print("Exception in getDepositsSuccessCalBack: " + err);
                    scope.closeProgressIndicator();
                }
            }, function(error) {
                try {
                    kony.print("getDepositsFailureCalBack Error Response::: " + JSON.stringify(error));
                    scope.closeProgressIndicator();
                    if (error.error.errorDetails && error.error.errorDetails) {
                        var errorDetails = error.error.errorDetails;
                        if (errorDetails[0].message) scope.view.ErrorAllert.lblMessage.text = errorDetails[0].code + " " + errorDetails[0].message;
                        else scope.view.ErrorAllert.lblMessage.text = error.errmsg;
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    } else if (error.opstatus && error.opstatus == "1011") {
                        scope.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity."
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    } else {
                        if (error.errmsg) scope.view.ErrorAllert.lblMessage.text = error.errmsg;
                        else scope.view.ErrorAllert.lblMessage.text = "An error occurred while making the request.";
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    }
                } catch (err) {
                    kony.print("Exception in getDepositsFailureCalBack: " + err);
                    scope.closeProgressIndicator();
                }
            });
        } catch (err) {
            kony.print("Exception in getDeposits: " + err);
            this.closeProgressIndicator();
        }
    },
    getOriginalInterestAtMaturity: function(objOutput) {
        try {
            var serviceName = "FundsWithdrawal";
            // Get an instance of SDK
            var client = kony.sdk.getCurrentInstance();
            var integrationSvc = client.getIntegrationService(serviceName);
            var operationName = "getcombinedSchedules";
            var params = {
                "arrangementid": this.gblArrangementId
            };
            var headerParams = {
                "companyId": "NL0020001"
            };
            kony.print("headerParams: " + JSON.stringify(headerParams));
            kony.print("params: " + JSON.stringify(params));
            //this.showProgressIndicator();
            var scope = this;
            integrationSvc.invokeOperation(operationName, headerParams, params, function(response) {
                try {
                    kony.print("getOriginalInterestAtMaturity Success Response: " + JSON.stringify(response));
                    scope.closeProgressIndicator();
                    kony.print("objOutput::: " + JSON.stringify(objOutput));
                    var balance = scope.formatAmountForCurrency(objOutput.depBalance);
                    scope.view.cusTextFieldPrincipalAmnt.value = balance;
                    scope.view.cusTextFieldFullWithdralAmount.value = balance;
                    scope.view.FundDetails.lblAmountValueResponse.text = balance;
                    //scope.gblRedemptionFee = scope.formatAmountForCurrency("0.00");
                    //scope.gblFullWithdrawalAmount = balance;
                    if (response.header.status === "success") {
                        if (response.body && response.body.length > 0) {
                            var interestsArray = response.body;
                            var totalInterest = 0;
                            for (var i = 0; i < interestsArray.length; i++) {
                                if (interestsArray[i].totalInterest) {
                                    var interest = interestsArray[i].totalInterest;
                                    interest = interest.replace(/[\s,]+/g, '').trim();
                                    totalInterest = Number(totalInterest) + Number(interest);
                                }
                            }
                            var interestRounded = totalInterest.toFixed(2);
                            //objOutput.originalInterest = scope.formatAmount(interestRounded);
                            scope.view.cusTextFieldOriginalInterest.value = scope.formatAmountForCurrency(interestRounded);
                            scope.view.cusTextFieldNewInterest.value = scope.formatAmountForCurrency(interestRounded);
                        }
                    } else {
                        scope.view.ErrorAllert.lblMessage.text = "An error occurred while making the request.";
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.closeProgressIndicator();
                    }
                    scope.view.flxFundWithdrawalWrapper.forceLayout();
                } catch (err) {
                    kony.print("Exception in getOriginalInterestAtMaturitySuccessCallBack: " + err);
                    scope.closeProgressIndicator();
                }
            }, function(error) {
                try {
                    kony.print("getOriginalInterestAtMaturity Error Response::: " + JSON.stringify(error));
                    scope.closeProgressIndicator();
                    if (error.error.errorDetails && error.error.errorDetails) {
                        var errorDetails = error.error.errorDetails;
                        if (errorDetails[0].message) scope.view.ErrorAllert.lblMessage.text = errorDetails[0].code + " " + errorDetails[0].message;
                        else scope.view.ErrorAllert.lblMessage.text = error.errmsg;
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    } else if (error.opstatus && error.opstatus == "1011") {
                        scope.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity."
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    } else {
                        if (error.errmsg) scope.view.ErrorAllert.lblMessage.text = error.errmsg;
                        else scope.view.ErrorAllert.lblMessage.text = "An error occurred while making the request.";
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    }
                } catch (err) {
                    kony.print("Exception in getOriginalInterestAtMaturityFailureCalBack: " + err);
                    scope.closeProgressIndicator();
                }
            });
        } catch (err) {
            this.closeProgressIndicator();
            kony.print("Exception in getOriginalInterestAtMaturity: " + err);
        }
    },
    getPayInAccountDetails: function() {
        try {
            var serviceName = "FundsWithdrawal";
            // Get an instance of SDK
            var client = kony.sdk.getCurrentInstance();
            var integrationSvc = client.getIntegrationService(serviceName);
            var operationName = "getPayInAccountDetails";
            var params = {
                "arrangementID": this.gblArrangementId
            }; //AA203147R9BM
            var headerParams = {
                "companyId": "NL0020001"
            };
            var scope = this;
            integrationSvc.invokeOperation(operationName, headerParams, params, function(response) {
                try {
                    //scope.getPayInAccountDetailsSuccessCalBack(response);
                    if (response.opstatus === "0" || response.opstatus === 0) {
                        if (response.body) {
                            scope.view.tempSettlementAccount.txtFloatText.text = response.body[0].payinAccount ? response.body[0].payinAccount : "";
                        }
                    }
                } catch (err) {
                    kony.print("Exception in getPayInAccountDetails SuccessCalBack: " + err);
                    scope.closeProgressIndicator();
                }
            }, function(error) {
                try {
                    kony.print("getPayInAccountDetails FailureCalBack Error Response::: " + JSON.stringify(error));
                    scope.closeProgressIndicator();
                    if (error.error.errorDetails && error.error.errorDetails) {
                        var errorDetails = error.error.errorDetails;
                        if (errorDetails[0].message) scope.view.ErrorAllert.lblMessage.text = errorDetails[0].code + " " + errorDetails[0].message;
                        else scope.view.ErrorAllert.lblMessage.text = error.errmsg;
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    } else if (error.opstatus && error.opstatus == "1011") {
                        scope.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity."
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    } else {
                        if (error.errmsg) scope.view.ErrorAllert.lblMessage.text = error.errmsg;
                        else scope.view.ErrorAllert.lblMessage.text = "An error occurred while making the request.";
                        scope.view.ErrorAllert.setVisibility(true);
                        scope.view.ErrorAllert.setFocus(true);
                    }
                } catch (err) {
                    kony.print("Exception in getPayInAccountDetails FailureCalBack: " + err);
                    scope.closeProgressIndicator();
                }
            });
        } catch (err) {
            kony.print("Exception in getPayInAccountDetails: " + err);
            this.closeProgressIndicator();
        }
    },
    getWithdrawalAccounts: function(headerParams, bodyParams) {
        try {
            var serviceName = "FundsWithdrawal";
            // Get an instance of SDK
            var client = kony.sdk.getCurrentInstance();
            var integrationSvc = client.getIntegrationService(serviceName);
            var operationName = "getAccounts";
            //"accountId": "50003027"
            var options = {
                "httpRequestOptions": {
                    "timeoutIntervalForRequest": 60,
                    "timeoutIntervalForResource": 600
                }
            };
            kony.print("Header params::: " + JSON.stringify(headerParams));
            kony.print("Input params::: " + JSON.stringify(bodyParams));
            this.showProgressIndicator();
            integrationSvc.invokeOperation(operationName, headerParams, bodyParams, this.getWithdrawalAccountsSuccessCalBack, this.getWithdrawalAccountsErrorCallBack, options);
        } catch (err) {
            kony.print("Exception in getWithdrawalAccounts: " + err);
            this.closeProgressIndicator();
        }
    },
    getWithdrawalAccountsSuccessCalBack: function(response) {
        try {
            kony.print("getWithdrawalAccountsSuccessCalBack Success Response: " + JSON.stringify(response));
            this.dataPerPageNumber = this.view.txtDataPerPageNumber.text;
            this.gblSegPageCount = 1;
            var segData = [],
                searchData = [],
                accountsAll = [];
            for (var i = 0; i < response.body.length; i++) {
                var usableBalance = response.body[i].workingBalance;
                kony.print("usableBalance::: " + JSON.stringify(usableBalance));
                var rowData = {
                    "lblAccID": response.body[i].accountId,
                    "lblCustomerID": response.body[i].customerId,
                    "lblProduct": response.body[i].productName,
                    "lblCCY": response.body[i].currencyId,
                    "lblUsableBalance": usableBalance + ""
                };
                accountsAll.push(rowData);
            }
            var accInput = this.view.tempSettlementAccount.txtFloatText.text;
            if (!this.isFieldEmpty(accInput)) {
                for (var j = 0; j < accountsAll.length; j++) {
                    if (kony.string.startsWith(accountsAll[j].lblAccID, accInput, true)) {
                        this.accountsList.push(accountsAll[j]);
                    }
                }
            } else this.accountsList = accountsAll;
            var recordsLength = this.accountsList.length > this.noOfItemsPerPage ? this.noOfItemsPerPage : this.accountsList.length;
            kony.print("recordsLength::: " + recordsLength);
            for (let k = 0; k < recordsLength; k++) {
                segData.push(this.accountsList[k]);
            }
            if (accountsAll.length > this.noOfItemsPerPage) {
                this.view.flxPagination.setVisibility(true);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnFocus";
            } else if (accountsAll.length <= this.noOfItemsPerPage) {
                this.view.flxPagination.setVisibility(false);
            }
            this.view.flxSearchAccount.setVisibility(false);
            this.view.segAccountsList.removeAll();
            this.closeProgressIndicator();
            var totalpage = parseInt((this.accountsList.length / this.noOfItemsPerPage));
            var remainingItems = parseInt(this.accountsList.length) % this.noOfItemsPerPage;
            totalpage = remainingItems > 0 ? totalpage + 1 : totalpage;
            this.view.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + totalpage;
            this.view.segAccountsList.setData(segData);
            this.view.flxDropDownAccountListOutsideWrapper.setVisibility(true);
        } catch (err) {
            kony.print("Exception in getWithdrawalAccountsSuccessCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    showPaginationSegmentData: function(btnName) {
        try {
            kony.print("onPreviousNxtDelivery---> ");
            var tmpData = [];
            var segVal = this.accountsList;
            var segValLength = segVal.length;
            var totalpage = parseInt((this.accountsList.length / this.noOfItemsPerPage));
            var remainingItems = parseInt(this.accountsList.length) % this.noOfItemsPerPage;
            totalpage = remainingItems > 0 ? totalpage + 1 : totalpage;
            this.view.flxPagination.isVisible = true;
            if (totalpage > 1) {
                this.view.btnNext.isVisible = true;
                this.view.lblSearchPage.isVisible = true;
                this.view.btnPrev.isVisible = true;
            } else {
                this.view.btnNext.isVisible = false;
                this.view.lblSearchPage.isVisible = false;
                this.view.btnPrev.isVisible = false;
            }
            kony.print("totalpage:: " + totalpage);
            kony.print("remainingItems:: " + remainingItems);
            if (btnName == "btnPrev" && this.gblSegPageCount > 1) this.gblSegPageCount--;
            else if (btnName == "btnNext" && this.gblSegPageCount < totalpage) this.gblSegPageCount++;
            else //gblEmpSegPageCount=1;
                this.view.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + totalpage;
            kony.print("gblSegPageCount::--------------->  " + this.gblSegPageCount);
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < totalpage) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnFocus"; // show Next Btn
                kony.print("gblSegPageCount case 1:: " + this.gblSegPageCount);
            } else if (this.gblSegPageCount == 1 && totalpage > 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnFocus"; // show Next Btn
                kony.print("gblSegPageCount case 2:: " + this.gblSegPageCount);
            } else if (this.gblSegPageCount == 1 && totalpage == 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.lblSearchPage.text = "";
            } else if (this.gblSegPageCount == totalpage && totalpage > 1) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnDisable";
            } else {
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.lblSearchPage.text = "";
            }
            var i = (this.gblSegPageCount - 1) * this.noOfItemsPerPage;
            var total = this.gblSegPageCount * this.noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < segValLength) {
                    tmpData.push(segVal[j]);
                }
            }
            this.view.segAccountsList.removeAll();
            this.view.segAccountsList.setData(tmpData);
            this.view.forceLayout();
            //return tmpData;
        } catch (err) {
            kony.print("Exception in Withdrawal showPaginationSegmentData: " + err);
        }
    },
    selectSegAccount: function() {
        try {
            var rowItems = this.view.segAccountsList.selectedRowItems;
            this.view.tempSettlementAccount.txtFloatText.text = rowItems[0].lblAccID;
            if (this.view.tempSettlementAccount.txtFloatText.text) {
                this.view.tempSettlementAccount.flxFloatLableGrp.centerY = "23%";
                this.view.tempSettlementAccount.lblFloatLabel.skin = "sknFloatLblPostInput";
                this.view.flxDropDownAccountListOutsideWrapper.setVisibility(false);
            } else {
                this.view.tempSettlementAccount.flxFloatLableGrp.centerY = "50%";
            }
            this.view.flxDropDownAccountListOutsideWrapper.setVisibility(false);
        } catch (err) {
            kony.print("Exception in Withdrawal selectSegAccount: " + err);
        }
    },
    getWithdrawalAccountsErrorCallBack: function(error) {
        try {
            kony.print("getWithdrawalAccountsErrorCallBack Error Response::: " + JSON.stringify(error));
            this.closeProgressIndicator();
            if (error.error.errorDetails && error.error.errorDetails) {
                var errorDetails = error.error.errorDetails;
                if (errorDetails[0].message) this.view.ErrorAllert.lblMessage.text = errorDetails[0].code + " " + errorDetails[0].message;
                else this.view.ErrorAllert.lblMessage.text = error.errmsg;
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            } else if (error.opstatus && error.opstatus == "1011") {
                this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity."
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            } else {
                if (error.errmsg) this.view.ErrorAllert.lblMessage.text = error.errmsg;
                else this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request.";
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            }
        } catch (err) {
            kony.print("Exception in getWithdrawalAccountsErrorCallBack: " + err);
            this.closeProgressIndicator();
        }
    },
    clearUIErrors: function() {
        this.view.lblErrorMessge.isVisible = false;
        // this.view.tempAmount.flxBottomLine.setVisibility(true);
        this.view.flxAmountPartialWithdrawal.skin = "slFbox";
        //this.view.tempAmount.clearErrormodeSkin();
    },
    fetchBillDetails: function() {
        try {
            var validation = true;
            if (this.view.cusRadioWithdrawalType.value === 1 || this.view.cusRadioWithdrawalType.value === "1") {
                validation = !this.isFieldEmpty(this.view.cusTextFieldAmount.value);
            }
            if (validation) {
                this.clearUIErrors();
                this.overrideContent = {};
                this.createAaSimulation();
            }
            //Link has to be disabled instead of showing error emssage
            /**else{
              this.view.flxAmountPartialWithdrawal.skin="sknFlxWhiteBgRedBorder";
              this.view.lblErrorMessge.isVisible = true;
            } */
        } catch (err) {
            kony.print("Exception in fetchBillDetails: " + err);
        }
    },
    onClickOfSubmit: function() {
        try {
            var withdrawType = "partial";
            if (this.view.cusRadioWithdrawalType.value === 2 || this.view.cusRadioWithdrawalType.value === "2") {
                withdrawType = "full";
            }
            if (withdrawType === "partial") {
                if (this.isFieldEmpty(this.view.cusTextFieldAmount.value)) {
                    //this.view.tempAmount.clearErrormodeSkin();
                    // this.view.tempAmount.flxBottomLine.setVisibility(false);
                    this.view.flxAmountPartialWithdrawal.skin = "sknFlxWhiteBgRedBorder";
                    this.view.lblErrorMessge.isVisible = true;
                } else {
                    this.clearUIErrors();
                    this.overrideContent = {};
                    this.createArrangementActivity();
                }
            } else {
                this.clearUIErrors();
                this.overrideContent = {};
                this.createArrangementActivity();
            }
        } catch (err) {
            kony.print("Exception in onClickOfSubmit: " + err);
        }
    },
    createAaSimulation: function() {
        try {
            var serviceName = "FundsWithdrawal";
            // Get an instance of SDK
            var client = kony.sdk.getCurrentInstance();
            var integrationSvc = client.getIntegrationService(serviceName);
            var operationName = "createAaSimulation";
            var withdrawType = "partial";
            var effectiveDate = this.view.cusDatePickerWithdrawalDate.value;
            if (this.view.cusRadioWithdrawalType.value === 2 || this.view.cusRadioWithdrawalType.value === "2") {
                withdrawType = "full";
            }
            var activityID = "DEPOSITS-REDEEM-ARRANGEMENT"; //full withdrawal
            var amount = "";
            if (withdrawType === "partial") {
                activityID = "DEPOSITS-WITHDRAW-PO.EARLY.WITHDRAWAL";
                amount = this.view.cusTextFieldAmount.value;
            }
            amount = this.removeAmountFormat(amount);
            //if(amount) amount = amount.replace(/[\s,]+/g,'').trim();
            kony.print(">>> gblCurrency :: " + this.gblCurrency);
            var params = {
                "closurenote": "string",
                "arrangementId": this.gblArrangementId, //"AA20237948HZ", 
                "activityId": activityID,
                "effectivedate": effectiveDate,
                "currencyId": this.gblCurrency,
                "automaticrun": "SIMULATE",
                "transactionAmount": amount,
                "closurereason": "ACCOUNT.TRANSFERRED",
                "overrideDetails": this.overrideContent
            };
            var headerParams = {
                "companyId": "NL0020001"
            };
            kony.print("createAaSimulation Input params::: " + JSON.stringify(params));
            this.showProgressIndicator();
            integrationSvc.invokeOperation(operationName, headerParams, params, this.createAaSimulationSuccessCalBack, this.createAaSimulationErrorCalBack);
        } catch (err) {
            kony.print("Exception in createAaSimulation: " + err);
            this.closeProgressIndicator();
        }
    },
    createAaSimulationSuccessCalBack: function(response) {
        try {
            kony.print("createAaSimulationSuccessCalBack Success Response: " + JSON.stringify(response));
            var scope = this;
            kony.timer.cancel("simulationtimer");
            var simulationId, headerId;
            var withdrawType = "partial";
            if (this.view.cusRadioWithdrawalType.value === 2 || this.view.cusRadioWithdrawalType.value === "2") {
                withdrawType = "full";
                simulationId = response.body.simulationRunReference;
            } else {
                headerId = response.header.id;
            }
            //Stop execution for 4 seconds after the simulation
            kony.timer.schedule("simulationtimer", function() {
                if (withdrawType === "partial") {
                    if (headerId) scope.getWithdrawalStatementDetails(headerId);
                    else kony.print("Could not process Withdrawal Statemetns due to no Simulation Reference ID");
                } else if (withdrawType === "full") {
                    if (simulationId) scope.getAaRedeemStmtByPropertyAd(simulationId);
                    else kony.print("Could not process Redeem Statements to no Simulation Reference ID");
                }
            }, 15, false);
        } catch (err) {
            kony.print("Exception in createAaSimulationSuccessCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    createAaSimulationErrorCalBack: function(error) {
        try {
            kony.print("createAaSimulationErrorCalBack Error Response::: " + JSON.stringify(error));
            this.closeProgressIndicator();
            if (error.override) {
                if (error.override.overrideDetails.length > 0) {
                    //override
                    this.overrideContent = error.override;
                    var overrideDetails = error.override.overrideDetails;
                    var segData = [];
                    for (var i = 0; i < overrideDetails.length; i++) {
                        var rowData = {
                            "lblSerial": "*",
                            "lblInfo": overrideDetails[i].description
                        };
                        segData.push(rowData);
                    }
                    this.view.OverrideSegmentPopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.overrideNavFlow = "aa_simulation";
                    this.view.OverrideSegmentPopup.lblPaymentInfo.text = "Override Confirmation";
                    this.view.OverrideSegmentPopup.segOverride.setData(segData);
                    this.view.OverrideSegmentPopup.isVisible = true;
                    this.view.forceLayout();
                } else {
                    if (error.errmsg) this.view.ErrorAllert.lblMessage.text = error.errmsg;
                    else this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request.";
                    this.view.ErrorAllert.setVisibility(true);
                    this.view.ErrorAllert.setFocus(true);
                }
            } else if (error.error.errorDetails && error.error.errorDetails) {
                var errorDetails = error.error.errorDetails;
                if (errorDetails[0].message) this.view.ErrorAllert.lblMessage.text = errorDetails[0].code + " " + errorDetails[0].message;
                else this.view.ErrorAllert.lblMessage.text = error.errmsg;
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            } else if (error.opstatus && error.opstatus == "1011") {
                this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity."
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            } else {
                if (error.errmsg) this.view.ErrorAllert.lblMessage.text = error.errmsg;
                else this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request.";
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            }
        } catch (err) {
            kony.print("Exception in createAaSimulationErrorCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    getWithdrawalStatementDetails: function(simulationId) {
        try {
            kony.print("simulationId::: " + simulationId);
            var serviceName = "FundsWithdrawal";
            // Get an instance of SDK
            var client = kony.sdk.getCurrentInstance();
            var integrationSvc = client.getIntegrationService(serviceName);
            var operationName = "getWithdrawalStatementDetails";
            var params = {
                "simref": simulationId
            };
            var headerParams = {
                "companyId": "NL0020001"
            };
            var options = {
                "httpRequestOptions": {
                    "timeoutIntervalForRequest": 60,
                    "timeoutIntervalForResource": 600
                }
            };
            //this.showProgressIndicator();
            integrationSvc.invokeOperation(operationName, headerParams, params, this.getWithdrawalStatementDetailsSuccessCalBack, this.getWithdrawalStatementDetailsErrorCalBack, options);
        } catch (err) {
            kony.print("Exception in getWithdrawalStatementDetails: " + err);
            this.closeProgressIndicator();
        }
    },
    getWithdrawalStatementDetailsSuccessCalBack: function(response) {
        try {
            kony.print("getWithdrawalStatementDetailsSuccessCalBack Success Response: " + JSON.stringify(response));
            this.closeProgressIndicator();
            //data mapping
            if (response.body && response.body.length > 0) {
                this.view.lblBilldetailsPrincipalAmount.text = this.formatAmountForCurrency(response.body[0].withdrawalAmt);
                var withdrawalFee = "0.00";
                if (response.body[0].withdrawalFee) withdrawalFee = response.body[0].withdrawalFee;
                withdrawalFee = this.formatAmountForCurrency(withdrawalFee);
                this.view.lblHiddenValue1.text = withdrawalFee;
                this.view.lblBilldetailsWithdrawlFeeText.text = "Withdrawal Fee";
                this.view.lblBilldetailsWithdrawlFee.text = "(" + withdrawalFee + ")";
                //var withdrawalAmt = (response.body[0].withdrawalAmt).replace(/[\s,]+/g,'').trim();
                var withdrawalAmt = response.body[0].withdrawalAmt;
                var totalAmount = Number(this.removeAmountFormat(withdrawalAmt)) - Number(this.removeAmountFormat(withdrawalFee));
                var amountRounded = totalAmount.toFixed(2);
                this.view.lblBilldetailsNetSettlementAmount.text = this.formatAmountForCurrency(amountRounded);
                //this.view.lblHiddenValue1.text = response.body[0].interestAtMaturity;
                this.view.lblHiddenValue2.text = this.formatAmountForCurrency(response.body[0].newInterestAtMaturity);
                this.view.cusIconPrintBillDetails.setVisibility(true);
                this.view.flxBillDetailsInterestAccrued.setVisibility(false);
                this.view.flxBilldetailsTax.setVisibility(false);
                this.view.flxViewBillDetailsPopup.setVisibility(true);
            }
        } catch (err) {
            kony.print("Exception in getWithdrawalStatementDetailsSuccessCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    getWithdrawalStatementDetailsErrorCalBack: function(error) {
        try {
            kony.print("getWithdrawalStatementDetailsErrorCalBack Error Response::: " + JSON.stringify(error));
            this.closeProgressIndicator();
        } catch (err) {
            kony.print("Exception in getWithdrawalStatementDetailsErrorCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    getAaRedeemStmtByPropertyAd: function(simulationId) {
        try {
            kony.print("getAaRedeemStmtByPropertyAd # simulationId::: " + simulationId);
            var serviceName = "FundsWithdrawal";
            // Get an instance of SDK
            var client = kony.sdk.getCurrentInstance();
            var integrationSvc = client.getIntegrationService(serviceName);
            var operationName = "getAaRedeemStmtByPropertyAd";
            var params = {
                "arrId": this.gblArrangementId, //"AA20237948HZ",
                "simref": simulationId
            };
            var headerParams = {
                "companyId": "NL0020001"
            };
            var options = {
                "httpRequestOptions": {
                    "timeoutIntervalForRequest": 60,
                    "timeoutIntervalForResource": 600
                }
            };
            kony.print("Input Params: " + JSON.stringify(params));
            //this.showProgressIndicator();
            integrationSvc.invokeOperation(operationName, headerParams, params, this.getAaRedeemStmtByPropertyAdSuccessCalBack, this.getAaRedeemStmtByPropertyAdErrorCalBack, options);
        } catch (err) {
            kony.print("Exception in getAaRedeemStmtByPropertyAd: " + err);
            this.closeProgressIndicator();
        }
    },
    getAaRedeemStmtByPropertyAdSuccessCalBack: function(response) {
        try {
            kony.print("getAaRedeemStmtByPropertyAdSuccessCalBack Success Response::: " + JSON.stringify(response));
            this.closeProgressIndicator();
            if (response.body && response.body.length > 0) {
                var len = response.body.length;
                var responseBody = response.body;
                var principalAmount, redemptionFee, interestAccrued, taxAmount;
                principalAmount = redemptionFee = interestAccrued = taxAmount = this.formatAmountForCurrency("0.00");
                for (var i = 0; i < len; i++) {
                    var desc = responseBody[i].description;
                    if (desc === "Early Redemption Fee") redemptionFee = responseBody[i].amtDescription;
                    if (desc === "Account") principalAmount = responseBody[i].amtDescription;
                    if (desc === "Deposit Interest") interestAccrued = responseBody[i].amtDescription;
                    if (desc === "Tax") taxAmount = responseBody[i].amtDescription;
                }
                this.view.lblBilldetailsPrincipalAmount.text = this.formatAmountForCurrency(principalAmount);
                this.view.lblHiddenValue1.txt = this.formatAmountForCurrency(principalAmount);
                this.view.lblBilldetailsWithdrawlFeeText.text = "Early Redemption Fee";
                if (redemptionFee.includes("-")) redemptionFee = redemptionFee.replace("-", "");
                this.view.lblBilldetailsWithdrawlFee.text = "(" + this.formatAmountForCurrency(redemptionFee) + ")"; //Early Redemption fee
                this.view.lblHiddenValue2.text = this.formatAmountForCurrency(redemptionFee);
                this.view.lblBillDetailsInterestAccruedAmount.text = this.formatAmountForCurrency(interestAccrued);
                if (taxAmount.includes("-")) taxAmount = taxAmount.replace("-", "");
                this.view.lblBilldetailsTaxAmount.text = "(" + this.formatAmountForCurrency(taxAmount) + ")";
                var totalAmount = Number(this.removeAmountFormat(principalAmount)) - Number(this.removeAmountFormat(redemptionFee)) + Number(this.removeAmountFormat(interestAccrued)) - Number(this.removeAmountFormat(taxAmount));
                var amountRounded = totalAmount.toFixed(2);
                this.view.lblBilldetailsNetSettlementAmount.text = this.formatAmountForCurrency(amountRounded);
                this.view.cusIconPrintBillDetails.setVisibility(false);
                this.view.flxBillDetailsInterestAccrued.setVisibility(true);
                this.view.flxBilldetailsTax.setVisibility(true);
                this.view.flxViewBillDetailsPopup.setVisibility(true);
            }
        } catch (err) {
            kony.print("Exception in getAaRedeemStmtByPropertyAdSuccessCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    onClickOfBillDetailsOkay: function() {
        try {
            if (this.view.cusRadioWithdrawalType.value === 2 || this.view.cusRadioWithdrawalType.value === "2") { //Full
                this.view.cusTextFieldRedemptionFee.value = this.view.lblHiddenValue2.text;
                //this.view.cusTextFieldFullWithdralAmount.value = this.view.lblHiddenValue1.text;
                //this.gblRedemptionFee = this.view.cusTextFieldRedemptionFee.value;
                this.view.cusTextFieldFullWithdralAmount.value = this.view.lblBilldetailsNetSettlementAmount.text;
                //this.gblFullWithdrawalAmount = this.view.cusTextFieldFullWithdralAmount.value;
            } else { //Partial
                //var originalPrincipal = this.view.cusTextFieldPrincipalAmnt.value;
                var originalPrincipal = this.view.FundDetails.lblAmountValueResponse.text;
                var enteredAmount = this.view.cusTextFieldAmount.value;
                this.view.cusTextFieldWithdrawalFee.value = this.view.lblHiddenValue1.text;
                //this.view.cusTextFieldOriginalInterest.value = this.view.lblHiddenValue1.text;
                this.view.cusTextFieldNewInterest.value = this.view.lblHiddenValue2.text;
                if (originalPrincipal) originalPrincipal = this.removeAmountFormat(originalPrincipal); //(originalPrincipal).replace(/[\s,]+/g,'').trim();
                if (enteredAmount) enteredAmount = this.removeAmountFormat(enteredAmount); //(enteredAmount).replace(/[\s,]+/g,'').trim();
                var principalAfterWithdrawal = Number(originalPrincipal) - Number(enteredAmount);
                this.view.cusTextFieldPrincipalAmnt.value = this.formatAmountForCurrency(principalAfterWithdrawal.toFixed(2));
            }
            this.view.flxViewBillDetailsPopup.setVisibility(false);
        } catch (err) {
            kony.print("Exception in onClickOfBillDetailsOkay: " + err);
        }
    },
    getAaRedeemStmtByPropertyAdErrorCalBack: function(error) {
        try {
            kony.print("getAaRedeemStmtByPropertyAdErrorCalBack Error Response::: " + JSON.stringify(error));
            this.closeProgressIndicator();
        } catch (err) {
            kony.print("Exception in getAaRedeemStmtByPropertyAdErrorCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    createArrangementActivity: function() {
        try {
            var creditAccountID = this.view.tempSettlementAccount.txtFloatText.text;
            var orderInitiationType = "PARWITH";
            var amnt = "0.00";
            if (this.view.cusRadioWithdrawalType.value === 2 || this.view.cusRadioWithdrawalType.value === "2") { //Full
                amnt = this.view.cusTextFieldFullWithdralAmount.value;
                //withdrawalDate = this.view.cusDatePickerFullWithdrawalDate.value;
                orderInitiationType = "DEPPAYOUT";
            } else {
                // amnt = this.view.tempAmount.txtFloatText.text;
                amnt = this.view.cusTextFieldAmount.value;
            }
            var withdrawalDate = this.view.cusDatePickerWithdrawalDate.value;
            withdrawalDate = withdrawalDate.replace(/ /g, "-");
            kony.print("withdrawalDate: " + withdrawalDate);
            //if(amnt) amnt = amnt.replace(/[\s,]+/g,'').trim();
            if (!this.isFieldEmpty(amnt)) amnt = this.removeAmountFormat(amnt);
            var headerParams = {
                "companyId": "NL0020001"
            };
            var params = {
                "paymentOrderProductId": "ACOTHER",
                "creditvaluedate": withdrawalDate,
                "creditAccountId": creditAccountID,
                "debitAccountId": this.gblDebitAccountNumber,
                "amount": amnt,
                "orderInitiationType": orderInitiationType,
                "overrideDetails": this.overrideContent
            };
            var serviceName = "FundsWithdrawal";
            var client = kony.sdk.getCurrentInstance(); // Get an instance of SDK
            var integrationSvc = client.getIntegrationService(serviceName);
            var operationName = "createArrangementActivity";
            var options = {
                "httpRequestOptions": {
                    "timeoutIntervalForRequest": 60,
                    "timeoutIntervalForResource": 600
                }
            };
            kony.print("headerParams # " + JSON.stringify(headerParams));
            kony.print("InputParams # " + JSON.stringify(params));
            this.showProgressIndicator();
            integrationSvc.invokeOperation(operationName, headerParams, params, this.createArrangementActivitySuccessCalBack, this.createArrangementActivityErrorCalBack, options);
        } catch (err) {
            kony.print("Exception in createArrangementActivity: " + err);
            this.closeProgressIndicator();
        }
    },
    createArrangementActivitySuccessCalBack: function(response) {
        try {
            this.closeProgressIndicator();
            kony.print("createArrangementActivitySuccessCalBack # Success Response: " + JSON.stringify(response));
            var withdrawType = "partial";
            if (this.view.cusRadioWithdrawalType.value === 2 || this.view.cusRadioWithdrawalType.value === "2") { //Full
                withdrawType = "full";
            }
            var refId = response.header["id"];
            var navToFundDeposit = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObj = {};
            navObj.customerId = this.gblCustomerId;
            navObj.showMessageInfo = true;
            navObj.isShowDeposits = "true";
            navObj.messageInfo = "Deposit withdrawn successfully! Ref :" + refId;
            navToFundDeposit.navigate(navObj);
        } catch (err) {
            kony.print("Exception in createArrangementActivitySuccessCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    createArrangementActivityErrorCalBack: function(response) {
        try {
            kony.print("createArrangementActivityErrorCalBack Error Response::: " + JSON.stringify(response));
            this.closeProgressIndicator();
            if (response.override && response.override.overrideDetails.length > 0) {
                //override
                this.overrideContent = response.override;
                var overrideDetails = response.override.overrideDetails;
                var segData = [];
                for (var i = 0; i < overrideDetails.length; i++) {
                    var rowData = {
                        "lblSerial": "*",
                        "lblInfo": overrideDetails[i].description
                    };
                    segData.push(rowData);
                }
                this.view.OverrideSegmentPopup.segOverride.widgetDataMap = {
                    "lblSerial": "lblSerial",
                    "lblInfo": "lblInfo"
                };
                this.overrideNavFlow = "arrangement_activity";
                this.view.OverrideSegmentPopup.lblPaymentInfo.text = "Override Confirmation";
                this.view.OverrideSegmentPopup.segOverride.setData(segData);
                this.view.OverrideSegmentPopup.isVisible = true;
                this.view.forceLayout();
            } else if (response.error.errorDetails && response.error.errorDetails) {
                var errorDetails = response.error.errorDetails;
                if (errorDetails[0].message) this.view.ErrorAllert.lblMessage.text = errorDetails[0].code + " " + errorDetails[0].message;
                else this.view.ErrorAllert.lblMessage.text = response.errmsg;
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            } else if (response.opstatus && response.opstatus == "1011") {
                this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity."
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            } else {
                if (response.errmsg) this.view.ErrorAllert.lblMessage.text = response.errmsg;
                else this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request.";
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            }
        } catch (err) {
            kony.print("Exception in createArrangementActivityErrorCalBack: " + err);
            this.closeProgressIndicator();
        }
    },
    onClickProceedOverride: function() {
        try {
            kony.print("onClickProceedOverride ### overrideNavFlow::: " + this.overrideNavFlow);
            if (this.overrideNavFlow === "arrangement_activity") {
                this.createArrangementActivity();
            } else if (this.overrideNavFlow === "aa_simulation") {
                this.createAaSimulation();
            }
            this.closeOverridePopup();
        } catch (err) {
            kony.print("Exception in onClickProceedOverride: " + err);
            this.closeProgressIndicator();
        }
    },
    closeOverridePopup: function() {
        this.view.OverrideSegmentPopup.isVisible = false;
    },
    getSelectWithdrawalType: function(param) {
        try {
            kony.print("getSelectWithdrawalType # param: " + JSON.stringify(param));
            var scope = this;
            //this.view.cusDatePickerWithdrawalDate.value = this.getCurrentDate();
            //this.view.cusDatePickerWithdrawalDate.displayFormat = "dd MMM yyyy";
            //this.view.cusDatePickerWithdrawalDate.min = this.getCurrentDate();
            if (param === 1) { //Partial Withdrawal
                this.view.flxFullWithdrawal.setVisibility(false);
                this.view.flxPartialWithdrawal.setVisibility(true);
                this.view.cusTextFieldAmount.value = this.formatAmountForCurrency("0.00");
                this.view.cusTextFieldWithdrawalFee.value = this.formatAmountForCurrency("0.00");
                this.view.cusTextFieldPrincipalAmnt.value = this.view.FundDetails.lblAmountValueResponse.text;
                this.view.cusTextFieldNewInterest.value = this.view.cusTextFieldOriginalInterest.value;
                this.disableViewBillDetailsLink();
                this.view.flxLine2.top = "380dp";
                this.view.flxCommonFields.top = "408dp";
                this.view.lblErrorMessge.top = "480dp";
                this.view.flxButtons.top = "540dp";
                this.view.flxFundWithdrawalWrapper.height = "600dp";
            } else if (param === 2) { //Full Withdrawal
                this.view.flxPartialWithdrawal.setVisibility(false);
                this.clearUIErrors();
                this.view.flxFullWithdrawal.setVisibility(true);
                this.view.cusTextFieldRedemptionFee.value = this.formatAmountForCurrency("0.00"); //this.gblRedemptionFee;
                this.view.cusTextFieldFullWithdralAmount.value = this.view.FundDetails.lblAmountValueResponse.text; //this.gblFullWithdrawalAmount;
                this.enableViewBillDetailsLink();
                this.view.flxLine2.top = "300dp";
                this.view.flxCommonFields.top = "328dp";
                this.view.lblErrorMessge.top = "400dp";
                this.view.flxButtons.top = "460dp";
                this.view.flxDownArrowList.top = "377dp";
                this.view.flxDownArrowList.height = "196dp";
                this.view.AccountList2.segAccountList.height = "140dp";
                this.view.flxFundWithdrawalWrapper.height = "520dp";
                //this.view.flxViewBillDetailsLink.onTouchEnd = function() {
                //  scope.overrideContent = {};
                //  scope.createAaSimulation();
                //};
            }
            this.view.flxFundWithdrawalWrapper.forceLayout();
        } catch (err) {
            kony.print("Exception in getSelectWithdrawalType: " + err);
        }
    },
    assignDateToCalender: function() {
        try {
            var serviceName = "FundsDeposit";
            var operationName = "getDate";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgeDate, this.errorCBgetDate);
        } catch (err) {
            kony.print("Error in Withdrawal assignDateToCalender::: " + err);
        }
    },
    successCBgeDate: function(res) {
        try {
            var today = res.body[0].currentWorkingDate;
            //var today = new Date(); 
            var dd = today.slice(8, 10);
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            shortMonth = today.slice(5, 7);
            switch (shortMonth) {
                case "01":
                    shortMonth = "Jan";
                    break;
                case "02":
                    shortMonth = "Feb";
                    break;
                case "03":
                    shortMonth = "Mar";
                    break;
                case "04":
                    shortMonth = "Apr";
                    break;
                case "05":
                    shortMonth = "May";
                    break;
                case "06":
                    shortMonth = "Jun";
                    break;
                case "07":
                    shortMonth = "Jul";
                    break;
                case "08":
                    shortMonth = "Aug";
                    break;
                case "09":
                    shortMonth = "Sep";
                    break;
                case "10":
                    shortMonth = "Oct";
                    break;
                case "11":
                    shortMonth = "Nov";
                    break;
                case "12":
                    shortMonth = "Dec";
                    break;
                default:
                    shortMonth = "";
            }
            var yyyy = today.slice(0, 4);
            if (dd < 10) {
                dd = '0' + dd;
            }
            var todayConverted = dd + " " + shortMonth + " " + yyyy;
            this.view.cusDatePickerWithdrawalDate.value = todayConverted;
            this.view.cusDatePickerWithdrawalDate.displayFormat = "dd MMM yyyy";
            this.view.cusDatePickerWithdrawalDate.min = todayConverted;
        } catch (err) {
            kony.print("Error in Withdrawal Controller successCBgeDate::: " + err);
        }
    },
    errorCBgetDate: function(error) {
        try {
            this.closeProgressIndicator();
            this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity."
            this.view.ErrorAllert.setVisibility(true);
            this.view.ErrorAllert.setFocus(true);
            kony.print("error response::: " + JSON.stringify(error));
        } catch (err) {
            kony.print("Error in Withdrawal Controller errorCBgetDate::: " + err);
        }
    },
    getCurrentDate: function() {
        try {
            var today = new Date();
            var dd = today.getDate();
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var todayConverted = dd + " " + shortMonth + " " + yyyy;
            return todayConverted;
        } catch (err) {
            kony.print("Exception in getCurrentDate: " + err);
        }
    },
    onClickCancel: function() {
        try {
            var navToHome = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObjet = {};
            navObjet.customerId = this.gblCustomerId;
            //navObjet.isShowLoans = "false";
            navObjet.showMessageInfo = false;
            navObjet.isShowDeposits = "true";
            navToHome.navigate(navObjet);
        } catch (err) {
            kony.print("Exception in onClickCancel: " + err);
        }
    },
    formatAmountForCurrency: function(amount) {
        try {
            if (amount === 0 || amount === "0" || amount === "0.00" || amount === 0.00 || amount === "0,00") {
                if (this.gblCurrency === "EUR" || this.gblCurrency === "eur") amount = "0,00";
                else amount = "0.00";
            } else {
                amount = amount.replace(/[\s,]+/g, ''); //remove commas
                amount = Application.numberFormater.convertForDispaly(amount, this.gblCurrency);
            }
            return amount;
        } catch (err) {
            kony.print("Exception in formatAmountForCurrency: " + err);
        }
    },
    formatAmount: function(x) {
        try {
            if (!x) return "0.00";
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            if (parts.length === 1) {
                parts[1] = "00";
            } else if (parts.length === 2) {
                if (parts[1].length === 1) parts[1] = parts[1] + "0";
            }
            return parts.join(".");
        } catch (err) {
            kony.print("Exception in formatAmount: " + err);
        }
    },
    /*EUR
    input format: 10,000.50 or 10.000,50
    output formt: 10000.50

    USD
    input format: 10,000.50
    output formt: 10000.50
    */
    removeAmountFormat: function(amnt) {
        try {
            if (amnt === "0.00" || amnt === 0.00 || amnt === "0,00") return "0.00";
            var amount = "";
            if (amnt) {
                if (amnt.charAt(amnt.length - 3) === ",") {
                    amount = amnt.replace(/\./g, ""); //remove all dots
                    amount = amount.replace(/[\s,]+/g, '.').trim(); //replace comma wih dot
                } else {
                    amount = amnt.replace(/[\s,]+/g, ""); //remove all commas
                }
            }
            kony.print("Amount after removing all formats: " + amount);
            return amount;
        } catch (err) {
            kony.print("Exception in removeAmountFormat: " + err);
        }
    },
    /**
    removeAmountFormat: function(x) {
      try{
        x = x.replace(/[\s,]+/g,'')
      }catch(err){
        kony.print("Exception in removeAmountFormat: "+err);
      }
    }, */
    enableViewBillDetailsLink: function() {
        this.view.flxViewBillDetailsLink.setEnabled(true);
        this.view.cusIconRefreshViewDetails.color = "temenos-primary";
        this.view.lblViewDetailsLink.skin = "CopydefLabel0d0ad7f88c31e43";
    },
    disableViewBillDetailsLink: function() {
        this.view.flxViewBillDetailsLink.setEnabled(false);
        this.view.cusIconRefreshViewDetails.color = "grey-light";
        this.view.lblViewDetailsLink.skin = "skinLabelLinkDisableFont333333";
    },
    showDropdownAccountList: function() {
        try {
            this.view.AccountList2.segAccountList.widgetDataMap = gblCutacctWithdrawalWidgetDate;
            this.view.AccountList2.segAccountList.setData(gblCutacctWithdrawalArray);
            this.view.flxDownArrowList.isVisible = true;
            this.view.lblErrorMessge.isVisible = false;
            //this.view.flxFundWithdrawalWrapper.height = "650dp";
            this.view.flxDownArrowList.forceLayout();
        } catch (err) {
            kony.print("Error in FundDeposit Controller showDropDownAccountList :::" + err);
        }
    },
    getSegRowInfoDropdown: function() {
        var getAcctId = this.view.AccountList2.segAccountList.selectedRowItems[0].lblAccountId;
        //     var curId=this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].currency;
        //     this._creditCurrency = curId;
        this.view.flxSearchAccount.setVisibility(false);
        //this.view.tbxPayOutAccount.text=getAcctId; 
        this.view.tempSettlementAccount.txtFloatText.text = getAcctId;
        this.view.tempSettlementAccount.animateComponent();
        //     this.view.popupPayinPayout.txtSearchBox.text=""
        this.view.flxDownArrowList.setVisibility(false);
        this.view.AccountList2.segAccountList.removeAll();
        this.view.AccountList2.segAccountList.setData(this.popupSegData);
    },
    getSegRowInfo: function() {
        var getAcctId = this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].lblAccountId;
        //     var curId=this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].currency;
        //     this._creditCurrency = curId;
        this.view.flxSearchAccount.setVisibility(false);
        //this.view.tbxPayOutAccount.text=getAcctId; 
        this.view.tempSettlementAccount.txtFloatText.text = getAcctId;
        this.view.tempSettlementAccount.animateComponent();
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        //this.view.flxFundWithdrawalWrapper.height = "600dp";
        // this.view.custPayIn.options = JSON.stringify(accountArray);
    },
    advancedSearchClose: function() {
        this.view.flxSearchAccount.setVisibility(false);
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.popupSegData = gblAllaccountarray;
        this.view.popupPayinPayout.segPayinPayout.setData(this.popupSegData);
    },
    advancedSearchAccountsShow: function() {
        try {
            this.view.flxSearchAccount.isVisible = true;
            this.view.flxDownArrowList.isVisible = false;
            this.view.lblErrorMessge.isVisible = false;
            this.view.popupPayinPayout.segPayinPayout.removeAll();
            this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate;
            this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        } catch (err) {
            kony.print("Error in FundDeposit Controller advancedSearchAccountsShow :::" + err);
        }
    },
    amountOnEndEditing: function() {
        // var amount = this.view.tempAmount.txtFloatText.text;
        kony.print("amountOnEndEditing >>>>>>>>>>>");
        var amount = this.view.cusTextFieldAmount.value;
        //scope.view.tempAmount.compTxtOnEndEditing();
        var formattedAmount = this.addDecimalPlaces(amount);
        this.view.cusTextFieldAmount.value = formattedAmount;
        var scope = this;
        if (this.isFieldEmpty(amount)) {
            this.view.flxViewBillDetailsLink.setEnabled(false);
            this.view.flxViewBillDetailsLink.skin = "slFbox";
        } else {
            this.view.flxViewBillDetailsLink.setEnabled(true);
            this.view.flxViewBillDetailsLink.skin = "slFbox";
        }
    },
    validateAmount: function(str) {
        try {
            var strAfterDecimalPoint3 = "";
            var strBeforeDecimalValue3 = " ";
            strAfterDecimalPoint3 = str.slice(str.indexOf(".") + 1, str.length);
            strBeforeDecimalValue3 = str.slice(0, str.indexOf("."));
            if (str.includes(".00")) {
                strAfterDecimalPoint3 = "00";
            }
            if (strAfterDecimalPoint3.includes(".")) {
                strAfterDecimalPoint3 = strAfterDecimalPoint3.replace(".", "");
            }
            if (strBeforeDecimalValue3.includes(".")) {
                strBeforeDecimalValue3 = strBeforeDecimalValue3.replace(".", "");
            }
            if (strAfterDecimalPoint3.includes("T")) {
                strAfterDecimalPoint3 = strAfterDecimalPoint3.replace("T", "");
            }
            if (strBeforeDecimalValue3.includes("T")) {
                strBeforeDecimalValue3 = strBeforeDecimalValue3.replace("T", "");
            }
            if (strAfterDecimalPoint3.includes("M")) {
                strAfterDecimalPoint3 = strAfterDecimalPoint3.replace("M", "");
            }
            if (strBeforeDecimalValue3.includes("M")) {
                strBeforeDecimalValue3 = strBeforeDecimalValue3.replace("M", "");
            }
            strBeforeDecimalValue3 = this.getKomaFormat(strBeforeDecimalValue3.replace(/,/g, ""));
            if (strAfterDecimalPoint3.length > 2) {
                strAfterDecimalPoint3 = strAfterDecimalPoint3.slice(0, 2);
            }
            var formattedAmount = strBeforeDecimalValue3 + "." + strAfterDecimalPoint3;
            return formattedAmount;
        } catch (err) {
            kony.print("Error in DepositWithdrawal Controller validateAmount ::::" + err);
        }
    },
    changeAmountFormat: function(str1) {
        try {
            var formattedAmount = "0.00";
            var str2 = str1.replace(/,/g, "");
            var str = !str2.includes(".00") ? str2 : str2.substring(0, str2.length - 3);
            //     str=str2;
            var lastChar = str.substring(str.length - 1);
            var woLastChar = str.substring(0, str.length - 1);
            var length = str1.length;
            if (isNaN(str) && woLastChar !== "" && !isNaN(woLastChar) && !str2.includes(".") && (str !== "0T" && str !== "0M")) {
                if (lastChar === "T") {
                    woLastChar += "000";
                    return this.getKomaFormat(woLastChar) + ".00";
                } else if (lastChar === "M") {
                    woLastChar += "000000";
                    return this.getKomaFormat(woLastChar) + ".00";
                } else {
                    //do not add the non numeric input
                    //check for addl component properties
                    formattedAmount = woLastChar;
                    //this.view.tempAmount.txtFloatText.text = woLastChar;
                }
            } else {
                if (isNaN(str)) {
                    //non a numeric input
                    if (str === ".") {
                        woLastChar = str;
                    }
                    var strAfterDecimalPoint1;
                    var strBeforeDecimalValue1;
                    strAfterDecimalPoint1 = woLastChar.slice(woLastChar.indexOf("."), woLastChar.length);
                    strBeforeDecimalValue1 = woLastChar.slice(0, woLastChar.indexOf("."));
                    if (strAfterDecimalPoint1.length > 2) {
                        strAfterDecimalPoint1 = woLastChar.slice(woLastChar.indexOf("."), (woLastChar.indexOf(".")) + 3);
                    }
                    strBeforeDecimalValue1 = this.getKomaFormat(strBeforeDecimalValue1);
                    formattedAmount = strBeforeDecimalValue1 + strAfterDecimalPoint1;
                    //this.view.tempAmount.txtFloatText.text = strBeforeDecimalValue1+strAfterDecimalPoint1;
                } else {
                    if (!str.includes(".")) {
                        return this.getKomaFormat(str);
                    } else {
                        var strAfterDecimalPoint;
                        var strBeforeDecimalValue;
                        strAfterDecimalPoint = str.slice(str.indexOf("."), str.length);
                        strBeforeDecimalValue = str.slice(0, str.indexOf("."));
                        if (strAfterDecimalPoint.length > 2) {
                            strAfterDecimalPoint = str.slice(str.indexOf("."), (str.indexOf(".")) + 3);
                            strBeforeDecimalValue = this.getKomaFormat(strBeforeDecimalValue);
                            formattedAmount = strBeforeDecimalValue + strAfterDecimalPoint;
                            //this.view.tempAmount.txtFloatText.text=strBeforeDecimalValue+strAfterDecimalPoint;
                        }
                    }
                }
            }
            return formattedAmount;
        } catch (err) {
            kony.print("Error in DepositWithdrawal Controller ChangeAmountFormat ::::" + err);
        }
    },
    addDecimalPlaces: function(value) {
        try {
            var strAfterDecimalPoint;
            var strBeforeDecimalValue;
            var formattedValue = "";
            strAfterDecimalPoint = value.slice(value.indexOf("."), value.length);
            strBeforeDecimalValue = value.slice(0, value.indexOf("."));
            if (value !== "" && value !== undefined && value !== " ") {
                //this.view.tempAmount.txtFloatText.text= value.includes(".")?value:value+".00";
                formattedValue = value.includes(".") ? value : value + ".00";
                // if(!value.includes(".")){this.view.tempAmount.txtFloatText.text=value+".00";}
                if (value.includes(".") && strAfterDecimalPoint.length === 3) {
                    if (strBeforeDecimalValue.length === 0) {
                        strBeforeDecimalValue = "0";
                    }
                    formattedValue = strBeforeDecimalValue + strAfterDecimalPoint;
                    //this.view.tempAmount.txtFloatText.text=strBeforeDecimalValue+strAfterDecimalPoint;
                }
                if (value.includes(".") && strAfterDecimalPoint.length === 2) {
                    if (strBeforeDecimalValue.length === 0) {
                        strBeforeDecimalValue = "0";
                    }
                    //this.view.tempAmount.txtFloatText.text=strBeforeDecimalValue+strAfterDecimalPoint+"0";
                    formattedValue = strBeforeDecimalValue + strAfterDecimalPoint + "0";
                }
                if (value.includes(".") && strAfterDecimalPoint.length === 1) {
                    if (strBeforeDecimalValue.length === 0) {
                        strBeforeDecimalValue = "0";
                    }
                    formattedValue = strBeforeDecimalValue + strAfterDecimalPoint + "00";
                    //this.view.tempAmount.txtFloatText.text=strBeforeDecimalValue+strAfterDecimalPoint+"00";
                }
            }
            return formattedValue;
        } catch (err) {
            kony.print("Error in DepositWithdrawal Controller addDecimalPlaces::::" + err);
        }
    },
    getKomaFormat: function(str) {
        try {
            var tempArr = str.split("");
            var arrLen = tempArr.length - 3;
            while (arrLen > 0) {
                tempArr.splice(arrLen, 0, ",");
                arrLen -= 3;
            }
            return tempArr.join("");
        } catch (err) {
            kony.print("Error in DepositWithdrawal Controller getKomaFormat::::" + err);
        }
    },
    advancedSearch: function() {
        try {
            this.view.flxSearchAccount.setVisibility(true);
            var customerId = this.view.txtCustomer.text;
            var AccountId = this.view.txtAccounts.text;
            var CurrencyId = this.view.txtCurrency.text;
            var serviceName = "FundsDeposit";
            var operationName = "getAccounts";
            if (CurrencyId === null || CurrencyId === "") {
                CurrencyId = undefined;
            }
            if (AccountId === null || AccountId === "") {
                AccountId = undefined;
            }
            if (customerId === null || customerId === "") {
                customerId = undefined;
            }
            var headers = {
                "companyId": "NL0020001",
                "accountId": AccountId,
                "currencyId": CurrencyId,
                "customerId": customerId
            };
            var inputParams = {
                "accountId": AccountId,
                "currencyId": CurrencyId,
                "customerId": customerId
            };
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBSearch, this.errorCBSearch);
        } catch (err) {
            kony.print("Exception in fund Withdrawal Account advancedSearch:: " + err);
        }
    },
    closeAccountsListPopup: function() {
        try {
            this.gblSegPageCount = 1;
            this.view.segAccountsList.removeAll();
            kony.print("Segment Data: " + JSON.stringify(this.view.segAccountsList.data));
            this.view.flxSearchAccount.isVisible = false;
            this.view.flxDropDownAccountListOutsideWrapper.isVisible = false;
            //this.view.forceLayout();
        } catch (err) {
            kony.print("Exception in Fund Withdrawal Controller closeAccountsListPopup::" + err);
        }
    },
    searchListClose: function() {
        this.view.flxSearchDetails.setVisibility(false);
    },
    SearchListAccounts: function() {},
    showProgressIndicator: function() {
        this.view.ProgressIndicator.isVisible = true;
    },
    closeProgressIndicator: function() {
        this.view.ProgressIndicator.isVisible = false;
    }
});
define("frmDepositWithdrawalControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("frmDepositWithdrawalController", ["userfrmDepositWithdrawalController", "frmDepositWithdrawalControllerActions"], function() {
    var controller = require("userfrmDepositWithdrawalController");
    var controllerActions = ["frmDepositWithdrawalControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
