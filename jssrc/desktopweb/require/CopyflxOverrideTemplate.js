define("CopyflxOverrideTemplate", function() {
    return function(controller) {
        var CopyflxOverrideTemplate = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "CopyflxOverrideTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        CopyflxOverrideTemplate.setDefaultUnit(kony.flex.DP);
        var lblSerial = new kony.ui.Label({
            "id": "lblSerial",
            "isVisible": true,
            "left": "2%",
            "skin": "CopysknLbl3",
            "text": "1",
            "top": "0dp",
            "width": "3%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 1, 0, 1],
            "paddingInPixel": false
        }, {});
        var lblInfo = new kony.ui.Label({
            "id": "lblInfo",
            "isVisible": true,
            "left": "10%",
            "skin": "CopysknLbl3",
            "text": "Label",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 1, 0, 1],
            "paddingInPixel": false
        }, {});
        CopyflxOverrideTemplate.add(lblSerial, lblInfo);
        return CopyflxOverrideTemplate;
    }
})