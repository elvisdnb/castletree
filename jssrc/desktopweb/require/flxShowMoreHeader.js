define("flxShowMoreHeader", function() {
    return function(controller) {
        var flxShowMoreHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxShowMoreHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_f4fb312f30db4fb985a099723a15d7af,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxShowMoreHeader.setDefaultUnit(kony.flex.DP);
        var lblShelves = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblShelves",
            "isVisible": true,
            "left": "1%",
            "skin": "CopyCopydefLabel5",
            "text": "Kony ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgLocation = new kony.ui.Image2({
            "centerY": "50%",
            "height": "25dp",
            "id": "imgLocation",
            "isVisible": true,
            "right": "1dp",
            "skin": "slImage",
            "src": "blue_downarrow_2.png",
            "width": "25dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTitle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTitle",
            "isVisible": true,
            "left": "20%",
            "skin": "CopyCopydefLabel0bcabaec60c7842",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        flxShowMoreHeader.add(lblShelves, imgLocation, lblTitle);
        return flxShowMoreHeader;
    }
})