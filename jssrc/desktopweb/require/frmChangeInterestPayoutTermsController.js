define("userfrmChangeInterestPayoutTermsController", {
    _arrangementId: "",
    _depositObj: {},
    //   gblarrangementId : "AA2007914PF4",
    onNavigate: function(navObj) {
        this._depositObj = {};
        kony.print("onNavigate Call");
        this.bindEvents();
        this.setDepositHeaderDetails(navObj);
        if (navObj && navObj.lblArrangementID) {
            this._arrangementId = navObj.lblArrangementID;
        }
        this.initializeChangeInterest();
    },
    initializeChangeInterest: function() {
        try {
            this.view.cusRadioChangeInterestType.options = [{
                "label": "Yes",
                "value": 1
            }, {
                "label": "No",
                "value": 2
            }, ];
            this.view.cusRadioChangeInterestType.value = 1;
            this.view.ErrorAllert.setVisibility(false);
        } catch (err) {
            kony.print("Exception in initializeChangeInterest: " + err);
        }
    },
    bindEvents: function() {
        this.view.postShow = this.postShow;
        this.view.cusRadioChangeInterestType.onRadioChange = this.getSelectChangeInterestType;
        this.view.flxCancel.onTouchStart = this.onClickCancel;
        this.view.flxSubmit.onTouchStart = this.onClickSubmitData;
        this.view.flxBack.onTouchStart = this.navToSCV;
    },
    navToSCV: function() {
        var navToHome = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._depositObj.customerId;
        navObjet.showMessageInfo = false;
        navObjet.isShowDeposits = "true";
        navToHome.navigate(navObjet);
    },
    postShow: function() {
        try {
            this.view.MainTabs.btnAccounts2.skin = "sknTabSelected";
            this.view.MainTabs.btnAccounts3.skin = "sknTabUnselected";
            this.view.MainTabs.flxContainerdeposits.skin = "sknSelected";
            this.view.MainTabs.flxontainerLoans.skin = "sknNotselectedTransparent";
            //       this.view.cusNavigateRail.setVisibility(true);
            this.view.cusInterestPayoutFrequency.isVisible = false;
            // this.view.cusInterestPayoutFrequency.value = "";
            this.view.lblErrorMsg.isVisible = false;
            this.view.ErrorAllert.isVisible = false;
            this.getSelectChangeInterestType();
            this.paymentScheduleResuest();
            this.getPaymentMethod();
        } catch (err) {
            kony.print("Exception in postShow: " + err);
        }
    },
    getSelectChangeInterestType: function(param) {
        try {
            kony.print("getSelectChangeInterestType # param: " + JSON.stringify(param));
            if (param === 1) { //yes
                this.view.cusInterestPayoutFrequency.isVisible = false;
                // this.view.cusInterestPayoutFrequency.value =  "";
                this.view.cusPaymentMethod.left = "43%";
            } else if (param === 2) { //no
                this.view.cusInterestPayoutFrequency.isVisible = true;
                this.view.cusInterestPayoutFrequency.left = "43%";
                this.view.cusPaymentMethod.left = "69%";
            }
            this.view.flxChangeInterest.forceLayout();
        } catch (err) {
            kony.print("Exception in getSelectChangeInterestType: " + err);
        }
    },
    paymentScheduleResuest: function() {
        try {
            var serviceName = "ChangeInterestPayouts";
            var operationName = "paymentScheduleRequest";
            var headers = {
                "companyId": "NL0020001",
                "page_size": "500"
            };
            var inputParams = {
                "paymentScheduleId": this._arrangementId + "..."
            };
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.SCBpaymentScheduleResuest, this.ECBpaymentScheduleResuest);
        } catch (err) {
            kony.print("Error in frmChangeIntrestPayout paymentScheduleResuest:::" + err);
        }
    },
    SCBpaymentScheduleResuest: function(res) {
        try {
            var id;
            var startDate;
            var paymentMethod;
            var paymentFrequency;
            var lenghth = res.body.length;
            //       var data = res.body;
            //       alert(JSON.stringify(res)+"in SCB");
            let max = 0;
            var maxRecord;
            for (i = 0; i < lenghth; i++) {
                if (res.body[i].paymentType !== "INTEREST") {
                    continue;
                }
                if (res.body[i].id) {
                    let temparr = res.body[i].id.split("-");
                    let date = temparr[2];
                    let suffixData = date.split(".");
                    let datee = suffixData[0];
                    let dataa = suffixData[1];
                    // if(date>max){
                    // max = date;
                    let finalDate = datee + dataa;
                    if (+finalDate > max) {
                        max = finalDate;
                        maxRecord = res.body[i];
                    }
                    // alert("in if"+res.body[i].id);
                }
            }
            //       alert("Dates::::" + JSON.stringify(maxRecord));
            //       alert("Max::::" +JSON.stringify(max));
            if (maxRecord !== undefined) {
                if (maxRecord.startDate !== undefined && maxRecord.startDate == "R_MATURITY") {
                    this.view.cusRadioChangeInterestType.value = 1;
                } else {
                    this.view.cusRadioChangeInterestType.value = 2;
                    this.view.cusInterestPayoutFrequency.value = maxRecord.paymentFrequency;
                }
                var method = maxRecord.paymentMethod;
                if (method == "PAY" || method == "CAPITALISE") {
                    var firstLetter = method.charAt(0);
                    var remainingLetter = (method.slice(1)).toLowerCase();
                    this.view.cusPaymentMethod.value = firstLetter + remainingLetter;
                }
            } else {
                this.view.cusPaymentMethod.value = "";
            }
            this.view.ProgressIndicator.isVisible = false;
            this.view.forceLayout();
            //       alert(paymentFrequency+"  paymentFrequency
        } catch (err) {
            kony.print("Error in frmChangeIntrestPayout SCBpaymentScheduleResuest:::" + err);
        }
    },
    ECBpaymentScheduleResuest: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            this.view.ErrorAllert.isVisible = true;
            //       alert("error response:::"+JSON.stringify(res));
        } catch (err) {
            kony.print("Error in FundDeposit Controller errorCBgetDate::::" + err);
        }
    },
    getPaymentMethod: function() {
        try {
            //       alert("in get payment");
            // this.view.ProgressIndicator.isVisible = false;
            var data = [{
                "label": "Pay",
            }, {
                "label": "Capitalise",
            }];
            this.view.cusPaymentMethod.options = JSON.stringify(data);
            //       this.view.cusPaymentMethod.value="Pay";
            this.view.ErrorAllert.setVisibility(false);
        } catch (err) {
            kony.print("Error in getPaymentMethod" + err);
        }
    },
    onClickCancel: function() {
        try {
            var navToHome = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObjet = {};
            navObjet.customerId = this._depositObj.customerId;
            //       navObjet.customerId = "500003";
            navObjet.showMessageInfo = false;
            navObjet.isShowDeposits = "true";
            navToHome.navigate(navObjet);
        } catch (err) {
            kony.print("Error in onClickCancel" + err);
        }
    },
    setDepositHeaderDetails: function(navObj) {
        try {
            var newObj = navObj;
            this._depositObj.currencyId = navObj.lblCCY;
            this._depositObj.arrangementId = navObj.lblArrangementID;
            this._depositObj.customerId = navObj.customerId;
            this._depositObj.produtId = navObj.loanProduct;
            //TODO : take from segment data a navigation object
            this.view.loanDetails.lblAccountNumberResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAcNum);
            this.view.loanDetails.lblLoanTypeResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAcType); //"2345678";
            this.view.loanDetails.lblCurrencyValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblCCY);
            this.view.loanDetails.lblAmountValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblClearBal);
            this.view.loanDetails.lblStartDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblLockedAc);
            this.view.loanDetails.lblMaturityDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAvlBal);
        } catch (err) {
            kony.print("Error in setDepositHeaderDetails:::" + err);
        }
    },
    onClickSubmitData: function() {
        try {
            var changeInterestType = this.view.cusRadioChangeInterestType.value;
            var paymentMethod = this.view.cusPaymentMethod.value;
            var frequency = this.view.cusInterestPayoutFrequency.value;
            var payoutFrequency = "";
            if (changeInterestType == 1) {
                if (!paymentMethod) {
                    this.view.lblErrorMsg.isVisible = true;
                    return;
                }
                payoutFrequency = "";
            } else {
                if (!paymentMethod || !frequency) {
                    this.view.lblErrorMsg.isVisible = true;
                    return;
                }
                payoutFrequency = frequency;
            }
            var serviceName = "ChangeInterestPayouts";
            var operationName = "updateActivityRequest";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {
                "arrangementId": this._arrangementId,
                "paymentMethod": paymentMethod.toUpperCase(),
                "activityId": "DEPOSITS-CHANGE-SCHEDULE",
                "startDate": changeInterestType == 1 ? "R_MATURITY" : "",
                "paymentFrequency": payoutFrequency
            };
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.sucessCBUpdateActivityRequest, this.FailureCBUpdateActivityRequest);
        } catch (err) {
            kony.print("Error in onClickSubmitData:::" + err);
        }
    },
    sucessCBUpdateActivityRequest: function(res) {
        try {
            kony.print("Success CallBack :::" + JSON.stringify(res));
            this.view.ProgressIndicator.isVisible = false;
            if (res === "" || res === null || res === undefined) {
                kony.print("Response is null or undefined");
            } else {
                var refId = res.header["aaaId"];
                var navToFundDeposit = new kony.mvc.Navigation("frmCustomerAccountsDetails");
                var navObjet = {};
                navObjet.customerId = this._depositObj.customerId;
                navObjet.showMessageInfo = true;
                navObjet.isShowDeposits = "true";
                navObjet.messageInfo = "Interest Pay-out Term amended successfully  Transaction Ref.No:" + refId;
                navToFundDeposit.navigate(navObjet);
            }
        } catch (err) {
            kony.print("Error in sucessCBUpdateActivityRequest:::" + err);
        }
    },
    FailureCBUpdateActivityRequest: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print(" Failure resss::: " + JSON.stringify(res));
            this.view.ErrorAllert.lblMessage.text = res.errmsg;
            this.view.ErrorAllert.isVisible = true;
        } catch (err) {
            kony.print("Error in sucessCBUpdateActivityRequest:::" + err);
        }
    },
});
define("frmChangeInterestPayoutTermsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("frmChangeInterestPayoutTermsController", ["userfrmChangeInterestPayoutTermsController", "frmChangeInterestPayoutTermsControllerActions"], function() {
    var controller = require("userfrmChangeInterestPayoutTermsController");
    var controllerActions = ["frmChangeInterestPayoutTermsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
