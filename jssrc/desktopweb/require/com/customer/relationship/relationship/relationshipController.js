define("com/customer/relationship/relationship/userrelationshipController", function() {
    return {};
});
define("com/customer/relationship/relationship/relationshipControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/customer/relationship/relationship/relationshipController", ["com/customer/relationship/relationship/userrelationshipController", "com/customer/relationship/relationship/relationshipControllerActions"], function() {
    var controller = require("com/customer/relationship/relationship/userrelationshipController");
    var actions = require("com/customer/relationship/relationship/relationshipControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
