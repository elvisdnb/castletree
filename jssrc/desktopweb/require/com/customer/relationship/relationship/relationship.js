define(function() {
    return function(controller) {
        var relationship = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "relationship",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "relationship"), extendConfig({}, controller.args[1], "relationship"), extendConfig({}, controller.args[2], "relationship"));
        relationship.setDefaultUnit(kony.flex.DP);
        var segRelationship = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }, {
                "relatedPartyNameVal": "No record"
            }],
            "groupCells": false,
            "id": "segRelationship",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa64",
            "separatorRequired": true,
            "separatorThickness": 10,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "CopyCopyflxRelationship": "CopyCopyflxRelationship",
                "flxHover": "flxHover",
                "flxTmpInner": "flxTmpInner",
                "relatedPartyNameVal": "relatedPartyNameVal"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segRelationship"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segRelationship"), extendConfig({}, controller.args[2], "segRelationship"));
        relationship.add(segRelationship);
        return relationship;
    }
})