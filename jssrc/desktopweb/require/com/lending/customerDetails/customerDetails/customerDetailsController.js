define("com/lending/customerDetails/customerDetails/usercustomerDetailsController", function() {
    return {
        initGettersSetters: function() {
            this.setCustomerValuesFromCache();
        },
        restCustomerValues: function() {
            var newCustomerObj = {};
            gblCustomerObj = {};
            this.setCustomerValues();
        },
        setCustomerValues: function() {
            this.view.lblCustomerName.text = "Eric VanDe Laar";
            this.view.lblCustomerNumberResponse.text = "101146";
            this.view.lblEmailResponse.text = "ericvdl@gmail.com";
            this.view.lblPhoneResponse.text = "+3197010281305";
            this.view.lblProfileInfo.text = "Profile";
            this.view.lblProfileResponse.text = "Customer rating - A";
            this.view.lblRelationResponse.text = "Dominic Laar - Child";
            this.view.lblLocationResponse.text = "Rotterdam, Netherland";
            this.view.lblDOBResponse.text = "04 Oct, 1975";
            this.view.lblAccountOfficerResponse.text = "Patrick Owen";
            this.view.lblMobileBankingResponse.text = "-";
            this.view.lblInternetBankingResponse.text = "-";
            //       gblCustomerObj =  customerObj;
        },
        setCustomerValuesFromCache: function() {
            this.view.lblCustomerName.text = "Eric VanDe Laar";
            this.view.lblCustomerNumberResponse.text = "101146";
            this.view.lblEmailResponse.text = "ericvdl@gmail.com";
            this.view.lblPhoneResponse.text = "+3197010281305";
            this.view.lblProfileInfo.text = "Profile";
            this.view.lblProfileResponse.text = "Customer rating - A";
            this.view.lblRelationResponse.text = "Dominic Laar - Child";
            this.view.lblLocationResponse.text = "Rotterdam, Netherland";
            this.view.lblDOBResponse.text = "04 Oct, 1975";
            this.view.lblAccountOfficerResponse.text = "Patrick Owen";
            this.view.lblMobileBankingResponse.text = "-";
            this.view.lblInternetBankingResponse.text = "-";
        }
    };
});
define("com/lending/customerDetails/customerDetails/customerDetailsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_b90cf040ffb24068b41a04d6b9460e67: function AS_FlexContainer_b90cf040ffb24068b41a04d6b9460e67(eventobject) {
        var self = this;
        return self.setCustomerValuesFromCache.call(this);
    }
});
define("com/lending/customerDetails/customerDetails/customerDetailsController", ["com/lending/customerDetails/customerDetails/usercustomerDetailsController", "com/lending/customerDetails/customerDetails/customerDetailsControllerActions"], function() {
    var controller = require("com/lending/customerDetails/customerDetails/usercustomerDetailsController");
    var actions = require("com/lending/customerDetails/customerDetails/customerDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
