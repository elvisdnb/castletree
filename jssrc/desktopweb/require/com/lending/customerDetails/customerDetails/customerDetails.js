define(function() {
    return function(controller) {
        var customerDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "157dp",
            "id": "customerDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "18dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_b90cf040ffb24068b41a04d6b9460e67(eventobject);
            },
            "right": "32dp",
            "skin": "CopysknFlxBorderCCE1",
            "top": "72dp",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, controller.args[0], "customerDetails"), extendConfig({}, controller.args[1], "customerDetails"), extendConfig({}, controller.args[2], "customerDetails"));
        customerDetails.setDefaultUnit(kony.flex.DP);
        var imgCustomerProfilePic = new kony.ui.Image2(extendConfig({
            "height": "108dp",
            "id": "imgCustomerProfilePic",
            "isVisible": true,
            "left": "34dp",
            "skin": "slImage",
            "src": "pficon_1_1.png",
            "top": "24dp",
            "width": "108dp",
            "zIndex": 1
        }, controller.args[0], "imgCustomerProfilePic"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCustomerProfilePic"), extendConfig({}, controller.args[2], "imgCustomerProfilePic"));
        var lblCustomerName = new kony.ui.Label(extendConfig({
            "height": "29dp",
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "159dp",
            "maxWidth": "300dp",
            "skin": "CopysknLblBG2",
            "text": "Eric VanDe Laar",
            "top": "31dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomerName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerName"), extendConfig({}, controller.args[2], "lblCustomerName"));
        var lblCustomerNumberResponse = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblCustomerNumberResponse",
            "isVisible": true,
            "left": "163dp",
            "skin": "CopysknLblBG3",
            "text": "101146",
            "top": "62dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomerNumberResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerNumberResponse"), extendConfig({}, controller.args[2], "lblCustomerNumberResponse"));
        var lblRelationInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblRelationInfo",
            "isVisible": true,
            "left": "459dp",
            "maxWidth": "160dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Relation",
            "top": "32dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRelationInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRelationInfo"), extendConfig({}, controller.args[2], "lblRelationInfo"));
        var lblDOBInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblDOBInfo",
            "isVisible": true,
            "left": "159dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Date of birth",
            "top": "91dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDOBInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDOBInfo"), extendConfig({}, controller.args[2], "lblDOBInfo"));
        var lblEmailInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblEmailInfo",
            "isVisible": true,
            "left": "281dp",
            "maxWidth": "160dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Email",
            "top": "91dp",
            "width": "31dp",
            "zIndex": 1
        }, controller.args[0], "lblEmailInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailInfo"), extendConfig({}, controller.args[2], "lblEmailInfo"));
        var lblPhoneInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblPhoneInfo",
            "isVisible": true,
            "left": "459dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Phone",
            "top": "91dp",
            "width": "33dp",
            "zIndex": 1
        }, controller.args[0], "lblPhoneInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPhoneInfo"), extendConfig({}, controller.args[2], "lblPhoneInfo"));
        var lblLocationInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblLocationInfo",
            "isVisible": true,
            "left": "640dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Location",
            "top": "91dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLocationInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLocationInfo"), extendConfig({}, controller.args[2], "lblLocationInfo"));
        var lblProfileInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblProfileInfo",
            "isVisible": true,
            "left": "640dp",
            "maxWidth": "160dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Profile",
            "top": "32dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProfileInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProfileInfo"), extendConfig({}, controller.args[2], "lblProfileInfo"));
        var lblAccountOfficerInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblAccountOfficerInfo",
            "isVisible": true,
            "left": "837dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Account Officer",
            "top": "32dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAccountOfficerInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountOfficerInfo"), extendConfig({}, controller.args[2], "lblAccountOfficerInfo"));
        var lblMobileBankingInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblMobileBankingInfo",
            "isVisible": true,
            "left": "975dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Mobile Banking",
            "top": "32dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "lblMobileBankingInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMobileBankingInfo"), extendConfig({}, controller.args[2], "lblMobileBankingInfo"));
        var lblInternetBankingInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblInternetBankingInfo",
            "isVisible": true,
            "left": "837dp",
            "skin": "CopysknLblRobotoReg2",
            "text": "Internet Banking",
            "top": "91dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblInternetBankingInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInternetBankingInfo"), extendConfig({}, controller.args[2], "lblInternetBankingInfo"));
        var lblRelationResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblRelationResponse",
            "isVisible": true,
            "left": "459dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "Dominic Laar - Child",
            "top": "48dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblRelationResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRelationResponse"), extendConfig({}, controller.args[2], "lblRelationResponse"));
        var lblDOBResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblDOBResponse",
            "isVisible": true,
            "left": "159dp",
            "maxWidth": "160dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "04 Oct, 1975",
            "top": "107dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDOBResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDOBResponse"), extendConfig({}, controller.args[2], "lblDOBResponse"));
        var lblEmailResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblEmailResponse",
            "isVisible": true,
            "left": "281dp",
            "maxWidth": "170dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "ericvdl@gmail.com",
            "top": "107dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmailResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailResponse"), extendConfig({}, controller.args[2], "lblEmailResponse"));
        var lblPhoneResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblPhoneResponse",
            "isVisible": true,
            "left": "459dp",
            "maxWidth": "160dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "+3197010281305",
            "top": "107dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPhoneResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPhoneResponse"), extendConfig({}, controller.args[2], "lblPhoneResponse"));
        var lblLocationResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblLocationResponse",
            "isVisible": true,
            "left": "640dp",
            "maxWidth": "190dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "Rotterdam, Netherlands",
            "top": "107dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLocationResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLocationResponse"), extendConfig({}, controller.args[2], "lblLocationResponse"));
        var lblProfileResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblProfileResponse",
            "isVisible": true,
            "left": "640dp",
            "maxWidth": "180dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "Customer rating - A",
            "top": "48dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProfileResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProfileResponse"), extendConfig({}, controller.args[2], "lblProfileResponse"));
        var lblAccountOfficerResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblAccountOfficerResponse",
            "isVisible": true,
            "left": "837dp",
            "maxWidth": "160dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "Patrick Owen",
            "top": "48dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAccountOfficerResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountOfficerResponse"), extendConfig({}, controller.args[2], "lblAccountOfficerResponse"));
        var lblMobileBankingResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblMobileBankingResponse",
            "isVisible": true,
            "left": "975dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "-",
            "top": "48dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblMobileBankingResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMobileBankingResponse"), extendConfig({}, controller.args[2], "lblMobileBankingResponse"));
        var lblInternetBankingResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblInternetBankingResponse",
            "isVisible": true,
            "left": "837dp",
            "skin": "CopysknLblRobotoReg3",
            "text": "-",
            "top": "107dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblInternetBankingResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInternetBankingResponse"), extendConfig({}, controller.args[2], "lblInternetBankingResponse"));
        customerDetails.add(imgCustomerProfilePic, lblCustomerName, lblCustomerNumberResponse, lblRelationInfo, lblDOBInfo, lblEmailInfo, lblPhoneInfo, lblLocationInfo, lblProfileInfo, lblAccountOfficerInfo, lblMobileBankingInfo, lblInternetBankingInfo, lblRelationResponse, lblDOBResponse, lblEmailResponse, lblPhoneResponse, lblLocationResponse, lblProfileResponse, lblAccountOfficerResponse, lblMobileBankingResponse, lblInternetBankingResponse);
        customerDetails.compInstData = {}
        return customerDetails;
    }
})