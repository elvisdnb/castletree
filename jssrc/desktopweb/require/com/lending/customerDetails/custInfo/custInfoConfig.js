define(function() {
    return {
        "properties": [],
        "apis": ["restCustomerValues", "setCustomerValues", "setCustomerValuesFromCache"],
        "events": []
    }
});