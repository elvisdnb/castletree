define(function() {
    return function(controller) {
        var custInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "157dp",
            "id": "custInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "18dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_g63224517cc14883803abeb289818668(eventobject);
            },
            "skin": "sknFlxBorderCCE3F7",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, controller.args[0], "custInfo"), extendConfig({}, controller.args[1], "custInfo"), extendConfig({}, controller.args[2], "custInfo"));
        custInfo.setDefaultUnit(kony.flex.DP);
        var imgCustomerProfilePic = new kony.ui.Image2(extendConfig({
            "height": "108dp",
            "id": "imgCustomerProfilePic",
            "isVisible": true,
            "left": "34dp",
            "skin": "slImage",
            "src": "pficon_1.png",
            "top": "24dp",
            "width": "108dp",
            "zIndex": 1
        }, controller.args[0], "imgCustomerProfilePic"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCustomerProfilePic"), extendConfig({}, controller.args[2], "imgCustomerProfilePic"));
        var lblCustomerName = new kony.ui.Label(extendConfig({
            "height": "29dp",
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "159dp",
            "maxWidth": "300dp",
            "skin": "sknLblBG4D4D4Dpx24",
            "text": "Eric VanDe Laar",
            "top": "31dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomerName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerName"), extendConfig({}, controller.args[2], "lblCustomerName"));
        var lblCustomerNumberResponse = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblCustomerNumberResponse",
            "isVisible": true,
            "left": "163dp",
            "skin": "sknLblBG757575px16",
            "text": "101146",
            "top": "62dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomerNumberResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerNumberResponse"), extendConfig({}, controller.args[2], "lblCustomerNumberResponse"));
        var lblRelationInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblRelationInfo",
            "isVisible": false,
            "left": "459dp",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Relation",
            "top": "32dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRelationInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRelationInfo"), extendConfig({}, controller.args[2], "lblRelationInfo"));
        var lblNationalityInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblNationalityInfo",
            "isVisible": true,
            "left": "27%",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Nationality",
            "top": "37dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblNationalityInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNationalityInfo"), extendConfig({}, controller.args[2], "lblNationalityInfo"));
        var lblDOBInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblDOBInfo",
            "isVisible": true,
            "left": "159dp",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Date of birth",
            "top": "95dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDOBInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDOBInfo"), extendConfig({}, controller.args[2], "lblDOBInfo"));
        var lblEmailInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblEmailInfo",
            "isVisible": true,
            "left": "41%",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Email",
            "top": "37dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblEmailInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailInfo"), extendConfig({}, controller.args[2], "lblEmailInfo"));
        var lblPhoneInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblPhoneInfo",
            "isVisible": true,
            "left": "41%",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Phone",
            "top": "95dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblPhoneInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPhoneInfo"), extendConfig({}, controller.args[2], "lblPhoneInfo"));
        var lblEmployerInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblEmployerInfo",
            "isVisible": true,
            "left": "55%",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Employer's Name",
            "top": "95dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblEmployerInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmployerInfo"), extendConfig({}, controller.args[2], "lblEmployerInfo"));
        var lblLocationInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblLocationInfo",
            "isVisible": true,
            "left": "27%",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Location",
            "top": "95dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblLocationInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLocationInfo"), extendConfig({}, controller.args[2], "lblLocationInfo"));
        var lblProfileInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblProfileInfo",
            "isVisible": true,
            "left": "70%",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Profile",
            "top": "95dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblProfileInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProfileInfo"), extendConfig({}, controller.args[2], "lblProfileInfo"));
        var lblAccountOfficerInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblAccountOfficerInfo",
            "isVisible": true,
            "left": "70%",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Account Officer",
            "top": "37dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountOfficerInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountOfficerInfo"), extendConfig({}, controller.args[2], "lblAccountOfficerInfo"));
        var lblEmplymentStatus = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblEmplymentStatus",
            "isVisible": true,
            "left": "55%",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Employment Status",
            "top": "37dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblEmplymentStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmplymentStatus"), extendConfig({}, controller.args[2], "lblEmplymentStatus"));
        var lblMobileBankingInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblMobileBankingInfo",
            "isVisible": true,
            "left": "83%",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Mobile Banking",
            "top": "32dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "lblMobileBankingInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMobileBankingInfo"), extendConfig({}, controller.args[2], "lblMobileBankingInfo"));
        var lblInternetBankingInfo = new kony.ui.Label(extendConfig({
            "height": "17dp",
            "id": "lblInternetBankingInfo",
            "isVisible": true,
            "left": "83%",
            "skin": "sknLblRobotoReg11pxLightGrey",
            "text": "Internet Banking",
            "top": "91dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblInternetBankingInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInternetBankingInfo"), extendConfig({}, controller.args[2], "lblInternetBankingInfo"));
        var lblRelationResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblRelationResponse",
            "isVisible": false,
            "left": "459dp",
            "skin": "sknLblRobotoReg14px",
            "text": "Dominic Laar - Child",
            "top": "48dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblRelationResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRelationResponse"), extendConfig({}, controller.args[2], "lblRelationResponse"));
        var lblNationalityResponse = new kony.ui.Label(extendConfig({
            "id": "lblNationalityResponse",
            "isVisible": true,
            "left": "27%",
            "skin": "sknLblRobotoReg14px",
            "text": "Netherlands",
            "top": "58dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblNationalityResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNationalityResponse"), extendConfig({}, controller.args[2], "lblNationalityResponse"));
        var lblDOBResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblDOBResponse",
            "isVisible": true,
            "left": "159dp",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg14px",
            "text": "04 Oct, 1975",
            "top": "116dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDOBResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDOBResponse"), extendConfig({}, controller.args[2], "lblDOBResponse"));
        var lblEmailResponse = new kony.ui.Label(extendConfig({
            "id": "lblEmailResponse",
            "isVisible": true,
            "left": "41%",
            "maxWidth": "170dp",
            "skin": "sknLblRobotoReg14px",
            "text": "ericvdl@gmail.com",
            "top": "58dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblEmailResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailResponse"), extendConfig({}, controller.args[2], "lblEmailResponse"));
        var lblPhoneResponse = new kony.ui.Label(extendConfig({
            "id": "lblPhoneResponse",
            "isVisible": true,
            "left": "41%",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg14px",
            "text": "+3197010281305",
            "top": "116dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblPhoneResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPhoneResponse"), extendConfig({}, controller.args[2], "lblPhoneResponse"));
        var lblEmployerResponse = new kony.ui.Label(extendConfig({
            "id": "lblEmployerResponse",
            "isVisible": true,
            "left": "55%",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg14px",
            "text": "Sint-Laurenscollege Sint-Laurenscollege",
            "top": "116dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblEmployerResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmployerResponse"), extendConfig({}, controller.args[2], "lblEmployerResponse"));
        var lblLocationResponse = new kony.ui.Label(extendConfig({
            "id": "lblLocationResponse",
            "isVisible": true,
            "left": "27%",
            "maxWidth": "190dp",
            "skin": "sknLblRobotoReg14px",
            "text": "Rotterdam, Netherlands",
            "top": "116dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblLocationResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLocationResponse"), extendConfig({}, controller.args[2], "lblLocationResponse"));
        var lblProfileResponse = new kony.ui.Label(extendConfig({
            "id": "lblProfileResponse",
            "isVisible": true,
            "left": "70%",
            "maxWidth": "180dp",
            "skin": "sknLblRobotoReg14px",
            "text": "Customer rating - A",
            "top": "116dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblProfileResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProfileResponse"), extendConfig({}, controller.args[2], "lblProfileResponse"));
        var lblAccountOfficerResponse = new kony.ui.Label(extendConfig({
            "id": "lblAccountOfficerResponse",
            "isVisible": true,
            "left": "70%",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg14px",
            "text": "Patrick Owen",
            "top": "58dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountOfficerResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountOfficerResponse"), extendConfig({}, controller.args[2], "lblAccountOfficerResponse"));
        var lblEmplomentStatusResponse = new kony.ui.Label(extendConfig({
            "id": "lblEmplomentStatusResponse",
            "isVisible": true,
            "left": "55%",
            "maxWidth": "160dp",
            "skin": "sknLblRobotoReg14px",
            "text": "Hired",
            "top": "58dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblEmplomentStatusResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmplomentStatusResponse"), extendConfig({}, controller.args[2], "lblEmplomentStatusResponse"));
        var lblMobileBankingResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblMobileBankingResponse",
            "isVisible": true,
            "left": "83%",
            "skin": "sknLblRobotoReg14px",
            "text": "-",
            "top": "48dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblMobileBankingResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMobileBankingResponse"), extendConfig({}, controller.args[2], "lblMobileBankingResponse"));
        var lblInternetBankingResponse = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblInternetBankingResponse",
            "isVisible": true,
            "left": "83%",
            "skin": "sknLblRobotoReg14px",
            "text": "-",
            "top": "107dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblInternetBankingResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInternetBankingResponse"), extendConfig({}, controller.args[2], "lblInternetBankingResponse"));
        custInfo.add(imgCustomerProfilePic, lblCustomerName, lblCustomerNumberResponse, lblRelationInfo, lblNationalityInfo, lblDOBInfo, lblEmailInfo, lblPhoneInfo, lblEmployerInfo, lblLocationInfo, lblProfileInfo, lblAccountOfficerInfo, lblEmplymentStatus, lblMobileBankingInfo, lblInternetBankingInfo, lblRelationResponse, lblNationalityResponse, lblDOBResponse, lblEmailResponse, lblPhoneResponse, lblEmployerResponse, lblLocationResponse, lblProfileResponse, lblAccountOfficerResponse, lblEmplomentStatusResponse, lblMobileBankingResponse, lblInternetBankingResponse);
        custInfo.compInstData = {}
        return custInfo;
    }
})