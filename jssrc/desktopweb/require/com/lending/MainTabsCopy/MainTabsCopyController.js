define("com/lending/MainTabsCopy/userMainTabsCopyController", function() {
    return {};
});
define("com/lending/MainTabsCopy/MainTabsCopyControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/lending/MainTabsCopy/MainTabsCopyController", ["com/lending/MainTabsCopy/userMainTabsCopyController", "com/lending/MainTabsCopy/MainTabsCopyControllerActions"], function() {
    var controller = require("com/lending/MainTabsCopy/userMainTabsCopyController");
    var actions = require("com/lending/MainTabsCopy/MainTabsCopyControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
