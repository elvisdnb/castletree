define(function() {
    return function(controller) {
        var MainTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "64dp",
            "id": "MainTabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxContainerBGF2F7FD",
            "top": "0dp",
            "width": "98%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, controller.args[0], "MainTabs"), extendConfig({}, controller.args[1], "MainTabs"), extendConfig({}, controller.args[2], "MainTabs"));
        MainTabs.setDefaultUnit(kony.flex.DP);
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "34dp",
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var btnAccounts1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnAccounts1",
            "isVisible": true,
            "left": "27dp",
            "onClick": controller.AS_Button_fb872159826b4baf9bc1a6cccf31cc32,
            "skin": "sknTabUnselected",
            "text": "Accounts",
            "top": "0px",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "btnAccounts1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAccounts1"), extendConfig({}, controller.args[2], "btnAccounts1"));
        var btnAccounts2 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnAccounts2",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknTabUnselected",
            "text": "Deposits",
            "top": "0px",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "btnAccounts2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAccounts2"), extendConfig({}, controller.args[2], "btnAccounts2"));
        var btnAccounts3 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnAccounts3",
            "isVisible": true,
            "left": "20dp",
            "onClick": controller.AS_Button_b312ef910fe44c3da0ee24ff7d0c734d,
            "skin": "sknTabUnselected",
            "text": "Loans",
            "top": "0px",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "btnAccounts3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAccounts3"), extendConfig({}, controller.args[2], "btnAccounts3"));
        var flxSelectedLine = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "height": "220dp",
            "id": "flxSelectedLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "22dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedLine"), extendConfig({}, controller.args[1], "flxSelectedLine"), extendConfig({}, controller.args[2], "flxSelectedLine"));
        flxSelectedLine.setDefaultUnit(kony.flex.DP);
        flxSelectedLine.add();
        flxButtons.add(btnAccounts1, btnAccounts2, btnAccounts3, flxSelectedLine);
        var flxContainerAccount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "2dp",
            "id": "flxContainerAccount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "27dp",
            "isModalContainer": false,
            "skin": "sknNotselectedTransparent",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxContainerAccount"), extendConfig({}, controller.args[1], "flxContainerAccount"), extendConfig({}, controller.args[2], "flxContainerAccount"));
        flxContainerAccount.setDefaultUnit(kony.flex.DP);
        flxContainerAccount.add();
        var flxContainerdeposits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "2dp",
            "id": "flxContainerdeposits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "127dp",
            "isModalContainer": false,
            "skin": "sknNotselected",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxContainerdeposits"), extendConfig({}, controller.args[1], "flxContainerdeposits"), extendConfig({}, controller.args[2], "flxContainerdeposits"));
        flxContainerdeposits.setDefaultUnit(kony.flex.DP);
        flxContainerdeposits.add();
        var flxontainerLoans = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "2dp",
            "id": "flxontainerLoans",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "227dp",
            "isModalContainer": false,
            "skin": "sknNotselectedTransparent",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxontainerLoans"), extendConfig({}, controller.args[1], "flxontainerLoans"), extendConfig({}, controller.args[2], "flxontainerLoans"));
        flxontainerLoans.setDefaultUnit(kony.flex.DP);
        flxontainerLoans.add();
        var flxSeparatorLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparatorLine1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "18dp",
            "isModalContainer": false,
            "skin": "sknSeparatorBlue",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeparatorLine1"), extendConfig({}, controller.args[1], "flxSeparatorLine1"), extendConfig({}, controller.args[2], "flxSeparatorLine1"));
        flxSeparatorLine1.setDefaultUnit(kony.flex.DP);
        flxSeparatorLine1.add();
        MainTabs.add(flxButtons, flxContainerAccount, flxContainerdeposits, flxontainerLoans, flxSeparatorLine1);
        MainTabs.compInstData = {}
        return MainTabs;
    }
})