define("com/lending/MainTabs/userMainTabsController", function() {
    return {};
});
define("com/lending/MainTabs/MainTabsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/lending/MainTabs/MainTabsController", ["com/lending/MainTabs/userMainTabsController", "com/lending/MainTabs/MainTabsControllerActions"], function() {
    var controller = require("com/lending/MainTabs/userMainTabsController");
    var actions = require("com/lending/MainTabs/MainTabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
