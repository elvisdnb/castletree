define(function() {
    return function(controller) {
        var floatReadOnlylText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": false,
            "isMaster": true,
            "height": "56dp",
            "id": "floatReadOnlylText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "flxReadOnlyTxtBg",
            "top": "0dp",
            "width": "100%",
            "zIndex": 4,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "floatReadOnlylText"), extendConfig({}, controller.args[1], "floatReadOnlylText"), extendConfig({}, controller.args[2], "floatReadOnlylText"));
        floatReadOnlylText.setDefaultUnit(kony.flex.DP);
        var flxBgColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxBgColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "temenosGreyLighest",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBgColor"), extendConfig({}, controller.args[1], "flxBgColor"), extendConfig({}, controller.args[2], "flxBgColor"));
        flxBgColor.setDefaultUnit(kony.flex.DP);
        var lblFloatLabel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFloatLabel",
            "isVisible": true,
            "left": 0,
            "skin": "sknReadOnlyFloatLblPre",
            "text": "First Name",
            "textStyle": {},
            "width": "100%",
            "zIndex": 2,
            "blur": {
                "enabled": true,
                "value": 0
            }
        }, controller.args[0], "lblFloatLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [5, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFloatLabel"), extendConfig({}, controller.args[2], "lblFloatLabel"));
        var flxBottomLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "2dp",
            "id": "flxBottomLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknGreyLine",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxBottomLine"), extendConfig({}, controller.args[1], "flxBottomLine"), extendConfig({}, controller.args[2], "flxBottomLine"));
        flxBottomLine.setDefaultUnit(kony.flex.DP);
        flxBottomLine.add();
        var txtFloatText = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "sknFloatTextPost",
            "height": "100%",
            "id": "txtFloatText",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "sknFloatTextPostReadOnly",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtFloatText"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
            "padding": [5, 5, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtFloatText"), extendConfig({
            "autoCorrect": false,
            "hoverSkin": "sknFloatTextPost",
            "placeholderSkin": "sknFloatTextPost"
        }, controller.args[2], "txtFloatText"));
        flxBgColor.add(lblFloatLabel, flxBottomLine, txtFloatText);
        floatReadOnlylText.add(flxBgColor);
        floatReadOnlylText.compInstData = {}
        return floatReadOnlylText;
    }
})