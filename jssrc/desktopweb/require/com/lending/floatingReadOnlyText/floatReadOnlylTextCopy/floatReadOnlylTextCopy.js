define(function() {
    return function(controller) {
        var floatReadOnlylTextCopy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": false,
            "isMaster": true,
            "height": "60dp",
            "id": "floatReadOnlylTextCopy",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopysknFlxWhiteBg",
            "top": "0dp",
            "width": "100%",
            "zIndex": 4,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "floatReadOnlylTextCopy"), extendConfig({}, controller.args[1], "floatReadOnlylTextCopy"), extendConfig({}, controller.args[2], "floatReadOnlylTextCopy"));
        floatReadOnlylTextCopy.setDefaultUnit(kony.flex.DP);
        var lblFloatLabel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFloatLabel",
            "isVisible": true,
            "left": 0,
            "skin": "CopysknReadOnlyFloatLblPre",
            "text": "First Name",
            "textStyle": {},
            "width": "100%",
            "zIndex": 1,
            "blur": {
                "enabled": true,
                "value": 0
            }
        }, controller.args[0], "lblFloatLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [5, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFloatLabel"), extendConfig({}, controller.args[2], "lblFloatLabel"));
        var txtFloatText = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "CopysknFloatTextPost1",
            "height": "100%",
            "id": "txtFloatText",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "CopysknFloatTextPost1",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "txtFloatText"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
            "padding": [5, 5, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtFloatText"), extendConfig({
            "autoCorrect": false,
            "hoverSkin": "CopysknFloatTextPost1",
            "placeholderSkin": "CopysknFloatTextPost1"
        }, controller.args[2], "txtFloatText"));
        floatReadOnlylTextCopy.add(lblFloatLabel, txtFloatText);
        floatReadOnlylTextCopy.compInstData = {}
        return floatReadOnlylTextCopy;
    }
})