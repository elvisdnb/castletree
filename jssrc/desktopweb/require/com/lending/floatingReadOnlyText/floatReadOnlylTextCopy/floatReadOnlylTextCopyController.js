define("com/lending/floatingReadOnlyText/floatReadOnlylTextCopy/userfloatReadOnlylTextCopyController", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {
            //this.view.floatLabel.shadowDepth = 4;
        },
        defaultSkin: function() {
            this.view.lblFloatLabel.centerY = "50%";
            this.view.lblFloatLabel.skin = "sknReadOnlyFloatLblPre";
        },
        animateComponent: function() {
            if (this.view.lblFloatLabel.centerY !== "23%") {
                //   this.view.lblFloatLabel.isVisible=true;
                //this.view.txtFloatText.opacity="100%";
                this.view.lblFloatLabel.skin = "sknReadOnlyFloatLblPost";
                //this.view.txtFloatText.placeholder="";
                this.view.lblFloatLabel.animate(kony.ui.createAnimation({
                    "100": {
                        centerY: "23%",
                        "stepConfig": {
                            "timingFunction": kony.anim.EASE
                        }
                    }
                }), {
                    "delay": 0,
                    "iterationCount": 1,
                    "duration": 0.2,
                    "fillMode": kony.anim.FILL_MODE_FORWARDS
                }, {
                    "animationEnd": function() {
                        kony.print("****Animation End****");
                    }
                });
            }
        },
        reverseAnimateComponent: function() {
            try {
                var txt = this.view.txtFloatText.text;
                if (txt.length === 0) {
                    this.reverseAnimateComponentUtil();
                }
            } catch (e) {
                this.reverseAnimateComponentUtil();
            }
        },
        reverseAnimateComponentUtil: function() {
            var txtLabel = this.view.txtFloatText;
            if (txtLabel !== null) {
                this.view.txtFloatText.text = "";
                var txt = txtLabel.text;
                if (kony.string.equalsIgnoreCase(txt, "")) {
                    this.view.lblFloatLabel.animate(kony.ui.createAnimation({
                        "100": {
                            centerY: "50%",
                            "stepConfig": {
                                "timingFunction": kony.anim.EASE
                            }
                        }
                    }), {
                        "delay": 0,
                        "iterationCount": 1,
                        "duration": 0.2,
                        "fillMode": kony.anim.FILL_MODE_FORWARDS
                    }, {
                        "animationEnd": function() {
                            kony.print("****Animation End****");
                        }
                    });
                    this.view.lblFloatLabel.skin = "sknReadOnlyFloatLblPre";
                }
            }
        },
        setReadOnlyText: function(inputText) {
            this.animateComponent();
            this.view.txtFloatText.text = inputText;
        },
        resetReadOnlyText: function() {
            this.view.txtFloatText.text = "";
            this.reverseAnimateComponent();
        }
    };
});
define("com/lending/floatingReadOnlyText/floatReadOnlylTextCopy/floatReadOnlylTextCopyControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/lending/floatingReadOnlyText/floatReadOnlylTextCopy/floatReadOnlylTextCopyController", ["com/lending/floatingReadOnlyText/floatReadOnlylTextCopy/userfloatReadOnlylTextCopyController", "com/lending/floatingReadOnlyText/floatReadOnlylTextCopy/floatReadOnlylTextCopyControllerActions"], function() {
    var controller = require("com/lending/floatingReadOnlyText/floatReadOnlylTextCopy/userfloatReadOnlylTextCopyController");
    var actions = require("com/lending/floatingReadOnlyText/floatReadOnlylTextCopy/floatReadOnlylTextCopyControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
