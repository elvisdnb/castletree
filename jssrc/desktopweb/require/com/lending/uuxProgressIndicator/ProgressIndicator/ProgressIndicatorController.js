define("com/lending/uuxProgressIndicator/ProgressIndicator/userProgressIndicatorController", function() {
    return {};
});
define("com/lending/uuxProgressIndicator/ProgressIndicator/ProgressIndicatorControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/uuxProgressIndicator/ProgressIndicator/ProgressIndicatorController", ["com/lending/uuxProgressIndicator/ProgressIndicator/userProgressIndicatorController", "com/lending/uuxProgressIndicator/ProgressIndicator/ProgressIndicatorControllerActions"], function() {
    var controller = require("com/lending/uuxProgressIndicator/ProgressIndicator/userProgressIndicatorController");
    var actions = require("com/lending/uuxProgressIndicator/ProgressIndicator/ProgressIndicatorControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "isVisible1", function(val) {
            this.view.ProgressIndicator.isVisible = val;
        });
        defineGetter(this, "isVisible1", function() {
            return this.view.ProgressIndicator.isVisible;
        });
        defineSetter(this, "progresstext", function(val) {
            this.view.lblProgressInfo.text = val;
        });
        defineGetter(this, "progresstext", function() {
            return this.view.lblProgressInfo.text;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    controller.setVisibility1 = function() {
        var wModel = this.view.ProgressIndicator;
        return wModel.setVisibility.apply(wModel, arguments);
    };
    return controller;
});
