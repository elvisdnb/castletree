define(function() {
    return {
        "properties": [{
            "name": "isVisible1",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "progresstext",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": ["setVisibility1"],
        "events": []
    }
});