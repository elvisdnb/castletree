define(function() {
    return function(controller) {
        var ProgressIndicator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "ProgressIndicator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknlblBlockedSeduled",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1400]
        }, controller.args[0], "ProgressIndicator"), extendConfig({}, controller.args[1], "ProgressIndicator"), extendConfig({}, controller.args[2], "ProgressIndicator"));
        ProgressIndicator.setDefaultUnit(kony.flex.DP);
        var flxInnerProgressIndicatorWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "100dp",
            "id": "flxInnerProgressIndicatorWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxWhiteBg",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxInnerProgressIndicatorWrapper"), extendConfig({}, controller.args[1], "flxInnerProgressIndicatorWrapper"), extendConfig({}, controller.args[2], "flxInnerProgressIndicatorWrapper"));
        flxInnerProgressIndicatorWrapper.setDefaultUnit(kony.flex.DP);
        var lblProgressInfo = new kony.ui.Label(extendConfig({
            "height": "29dp",
            "id": "lblProgressInfo",
            "isVisible": true,
            "left": "10%",
            "skin": "sknLbl16px005096BG",
            "text": "Progress",
            "top": "20dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblProgressInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProgressInfo"), extendConfig({}, controller.args[2], "lblProgressInfo"));
        var cusProgressIndicator = new kony.ui.CustomWidget(extendConfig({
            "id": "cusProgressIndicator",
            "isVisible": true,
            "left": "10%",
            "right": "43dp",
            "top": "60dp",
            "height": "4dp",
            "zIndex": 1,
            "clipBounds": true
        }, controller.args[0], "cusProgressIndicator"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "cusProgressIndicator"), extendConfig({
            "widgetName": "uuxProgressIndicator",
            "indeterminate": true
        }, controller.args[2], "cusProgressIndicator"));
        flxInnerProgressIndicatorWrapper.add(lblProgressInfo, cusProgressIndicator);
        ProgressIndicator.add(flxInnerProgressIndicatorWrapper);
        ProgressIndicator.compInstData = {}
        return ProgressIndicator;
    }
})