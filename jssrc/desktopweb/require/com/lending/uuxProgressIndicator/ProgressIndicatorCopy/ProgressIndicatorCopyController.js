define("com/lending/uuxProgressIndicator/ProgressIndicatorCopy/userProgressIndicatorCopyController", function() {
    return {};
});
define("com/lending/uuxProgressIndicator/ProgressIndicatorCopy/ProgressIndicatorCopyControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/uuxProgressIndicator/ProgressIndicatorCopy/ProgressIndicatorCopyController", ["com/lending/uuxProgressIndicator/ProgressIndicatorCopy/userProgressIndicatorCopyController", "com/lending/uuxProgressIndicator/ProgressIndicatorCopy/ProgressIndicatorCopyControllerActions"], function() {
    var controller = require("com/lending/uuxProgressIndicator/ProgressIndicatorCopy/userProgressIndicatorCopyController");
    var actions = require("com/lending/uuxProgressIndicator/ProgressIndicatorCopy/ProgressIndicatorCopyControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
