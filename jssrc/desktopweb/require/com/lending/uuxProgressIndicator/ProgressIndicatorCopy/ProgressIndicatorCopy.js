define(function() {
    return function(controller) {
        var ProgressIndicatorCopy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "968dp",
            "id": "ProgressIndicatorCopy",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopysknlblBlockedSeduled",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1400]
        }, controller.args[0], "ProgressIndicatorCopy"), extendConfig({}, controller.args[1], "ProgressIndicatorCopy"), extendConfig({}, controller.args[2], "ProgressIndicatorCopy"));
        ProgressIndicatorCopy.setDefaultUnit(kony.flex.DP);
        var flxInnerProgressIndicatorWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "100dp",
            "id": "flxInnerProgressIndicatorWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopysknFlxWhiteBg",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxInnerProgressIndicatorWrapper"), extendConfig({}, controller.args[1], "flxInnerProgressIndicatorWrapper"), extendConfig({}, controller.args[2], "flxInnerProgressIndicatorWrapper"));
        flxInnerProgressIndicatorWrapper.setDefaultUnit(kony.flex.DP);
        var lblProgressInfo = new kony.ui.Label(extendConfig({
            "height": "29dp",
            "id": "lblProgressInfo",
            "isVisible": true,
            "left": "10%",
            "skin": "CopysknLbl2",
            "text": "Progress",
            "top": "20dp",
            "width": "123dp",
            "zIndex": 1
        }, controller.args[0], "lblProgressInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProgressInfo"), extendConfig({}, controller.args[2], "lblProgressInfo"));
        var cusProgressIndicator = new kony.ui.CustomWidget(extendConfig({
            "id": "cusProgressIndicator",
            "isVisible": true,
            "left": "10%",
            "right": "43dp",
            "top": "60dp",
            "height": "4dp",
            "zIndex": 1,
            "clipBounds": true
        }, controller.args[0], "cusProgressIndicator"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "cusProgressIndicator"), extendConfig({
            "widgetName": "uuxProgressIndicator",
            "indeterminate": true
        }, controller.args[2], "cusProgressIndicator"));
        flxInnerProgressIndicatorWrapper.add(lblProgressInfo, cusProgressIndicator);
        ProgressIndicatorCopy.add(flxInnerProgressIndicatorWrapper);
        ProgressIndicatorCopy.compInstData = {}
        return ProgressIndicatorCopy;
    }
})