define("com/lending/revisedPaymentSchedule/RevisedPaymentSchedule/userRevisedPaymentScheduleController", function() {
    return {};
});
define("com/lending/revisedPaymentSchedule/RevisedPaymentSchedule/RevisedPaymentScheduleControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/revisedPaymentSchedule/RevisedPaymentSchedule/RevisedPaymentScheduleController", ["com/lending/revisedPaymentSchedule/RevisedPaymentSchedule/userRevisedPaymentScheduleController", "com/lending/revisedPaymentSchedule/RevisedPaymentSchedule/RevisedPaymentScheduleControllerActions"], function() {
    var controller = require("com/lending/revisedPaymentSchedule/RevisedPaymentSchedule/userRevisedPaymentScheduleController");
    var actions = require("com/lending/revisedPaymentSchedule/RevisedPaymentSchedule/RevisedPaymentScheduleControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
