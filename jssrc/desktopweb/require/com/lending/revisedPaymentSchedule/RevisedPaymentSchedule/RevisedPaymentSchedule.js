define(function() {
    return function(controller) {
        var RevisedPaymentSchedule = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "RevisedPaymentSchedule",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknlblBlockedSeduled",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, controller.args[0], "RevisedPaymentSchedule"), extendConfig({}, controller.args[1], "RevisedPaymentSchedule"), extendConfig({}, controller.args[2], "RevisedPaymentSchedule"));
        RevisedPaymentSchedule.setDefaultUnit(kony.flex.DP);
        var flxRevisedPaymentSchedule = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "100%",
            "id": "flxRevisedPaymentSchedule",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0h870f661235f4e",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRevisedPaymentSchedule"), extendConfig({}, controller.args[1], "flxRevisedPaymentSchedule"), extendConfig({}, controller.args[2], "flxRevisedPaymentSchedule"));
        flxRevisedPaymentSchedule.setDefaultUnit(kony.flex.DP);
        var flxRevisedPayment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "400dp",
            "id": "flxRevisedPayment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopyslFbox0g3fb5b22c13341",
            "width": "835dp",
            "zIndex": 1
        }, controller.args[0], "flxRevisedPayment"), extendConfig({}, controller.args[1], "flxRevisedPayment"), extendConfig({}, controller.args[2], "flxRevisedPayment"));
        flxRevisedPayment.setDefaultUnit(kony.flex.DP);
        var flxRevisedPmtHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxRevisedPmtHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRevisedPmtHeader"), extendConfig({}, controller.args[1], "flxRevisedPmtHeader"), extendConfig({}, controller.args[2], "flxRevisedPmtHeader"));
        flxRevisedPmtHeader.setDefaultUnit(kony.flex.DP);
        var lblRevisedPayment = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "29dp",
            "id": "lblRevisedPayment",
            "isVisible": true,
            "left": "25dp",
            "skin": "CopydefLabel0h891708545fc44",
            "text": "Revised Payment Schedule",
            "top": "2dp",
            "width": "300dp",
            "zIndex": 1
        }, controller.args[0], "lblRevisedPayment"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRevisedPayment"), extendConfig({}, controller.args[2], "lblRevisedPayment"));
        var imgPrint = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgPrint",
            "isVisible": false,
            "right": "124dp",
            "skin": "slImage",
            "src": "ico_print.png",
            "top": "2dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgPrint"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPrint"), extendConfig({}, controller.args[2], "imgPrint"));
        var imgClose1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgClose1",
            "isVisible": false,
            "right": "64dp",
            "skin": "CopyslImage0i72989521fd64f",
            "src": "ico_close.png",
            "top": "2dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgClose1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose1"), extendConfig({}, controller.args[2], "imgClose1"));
        var imgClose = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "imgClose",
            "isVisible": true,
            "right": "64dp",
            "skin": "sknBtnImgClose",
            "text": "L",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "imgClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose"), extendConfig({}, controller.args[2], "imgClose"));
        flxRevisedPmtHeader.add(lblRevisedPayment, imgPrint, imgClose1, imgClose);
        var flxRevisedPaymentDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "280dp",
            "id": "flxRevisedPaymentDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "44dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "86dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRevisedPaymentDetails"), extendConfig({}, controller.args[1], "flxRevisedPaymentDetails"), extendConfig({}, controller.args[2], "flxRevisedPaymentDetails"));
        flxRevisedPaymentDetails.setDefaultUnit(kony.flex.DP);
        var lblPaymentDate = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblPaymentDate",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Payment Date",
            "top": "0dp",
            "width": "91dp",
            "zIndex": 1
        }, controller.args[0], "lblPaymentDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPaymentDate"), extendConfig({}, controller.args[2], "lblPaymentDate"));
        var lblAmount = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblAmount",
            "isVisible": true,
            "left": "130dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Amount",
            "top": "0dp",
            "width": "51dp",
            "zIndex": 1
        }, controller.args[0], "lblAmount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmount"), extendConfig({}, controller.args[2], "lblAmount"));
        var lblPrincipal = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblPrincipal",
            "isVisible": true,
            "left": "255dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Principal",
            "top": "0dp",
            "width": "58dp",
            "zIndex": 1
        }, controller.args[0], "lblPrincipal"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrincipal"), extendConfig({}, controller.args[2], "lblPrincipal"));
        var lblInterest = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblInterest",
            "isVisible": true,
            "left": "385dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Interest",
            "top": "0dp",
            "width": "51dp",
            "zIndex": 1
        }, controller.args[0], "lblInterest"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInterest"), extendConfig({}, controller.args[2], "lblInterest"));
        var lblTax = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblTax",
            "isVisible": true,
            "left": "475dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Tax",
            "top": "0dp",
            "width": "51dp",
            "zIndex": 1
        }, controller.args[0], "lblTax"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTax"), extendConfig({}, controller.args[2], "lblTax"));
        var lblTotoutstanding = new kony.ui.Label(extendConfig({
            "height": "21dp",
            "id": "lblTotoutstanding",
            "isVisible": true,
            "left": "600dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Total Outstanding",
            "top": "0dp",
            "width": "116dp",
            "zIndex": 1
        }, controller.args[0], "lblTotoutstanding"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTotoutstanding"), extendConfig({}, controller.args[2], "lblTotoutstanding"));
        var segRevised = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "lblAmount": "0.00",
                "lblInterest": "0.00%",
                "lblPaymentDate": "19 Dec 2020",
                "lblPrincipal": "0.00",
                "lblTax": "0.00",
                "lbltotaloutstanding": "0.00"
            }, {
                "lblAmount": "0.00",
                "lblInterest": "0.00%",
                "lblPaymentDate": "19 Dec 2020",
                "lblPrincipal": "0.00",
                "lblTax": "0.00",
                "lbltotaloutstanding": "0.00"
            }, {
                "lblAmount": "0.00",
                "lblInterest": "0.00%",
                "lblPaymentDate": "19 Dec 2020",
                "lblPrincipal": "0.00",
                "lblTax": "0.00",
                "lbltotaloutstanding": "0.00"
            }, {
                "lblAmount": "0.00",
                "lblInterest": "0.00%",
                "lblPaymentDate": "19 Dec 2020",
                "lblPrincipal": "0.00",
                "lblTax": "0.00",
                "lbltotaloutstanding": "0.00"
            }, {
                "lblAmount": "0.00",
                "lblInterest": "0.00%",
                "lblPaymentDate": "19 Dec 2020",
                "lblPrincipal": "0.00",
                "lblTax": "0.00",
                "lbltotaloutstanding": "0.00"
            }, {
                "lblAmount": "0.00",
                "lblInterest": "0.00%",
                "lblPaymentDate": "19 Dec 2020",
                "lblPrincipal": "0.00",
                "lblTax": "0.00",
                "lbltotaloutstanding": "0.00"
            }],
            "groupCells": false,
            "height": "210dp",
            "id": "segRevised",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "Flex0i494021d9e244d",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": true,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": "30dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "Flex0i494021d9e244d": "Flex0i494021d9e244d",
                "flxScheduleheader": "flxScheduleheader",
                "lblAmount": "lblAmount",
                "lblInterest": "lblInterest",
                "lblPaymentDate": "lblPaymentDate",
                "lblPrincipal": "lblPrincipal",
                "lblTax": "lblTax",
                "lbltotaloutstanding": "lbltotaloutstanding"
            },
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "segRevised"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segRevised"), extendConfig({}, controller.args[2], "segRevised"));
        flxRevisedPaymentDetails.add(lblPaymentDate, lblAmount, lblPrincipal, lblInterest, lblTax, lblTotoutstanding, segRevised);
        var flxPagination = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "53%",
            "clipBounds": true,
            "height": "50dp",
            "id": "flxPagination",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "320dp",
            "width": "96%",
            "zIndex": 1
        }, controller.args[0], "flxPagination"), extendConfig({}, controller.args[1], "flxPagination"), extendConfig({}, controller.args[2], "flxPagination"));
        flxPagination.setDefaultUnit(kony.flex.DP);
        var btnNext = new kony.ui.Button(extendConfig({
            "height": "30dp",
            "id": "btnNext",
            "isVisible": true,
            "right": "45dp",
            "skin": "sknBtnFocus",
            "text": "N",
            "top": "10dp",
            "width": 30,
            "zIndex": 1
        }, controller.args[0], "btnNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnNext"), extendConfig({}, controller.args[2], "btnNext"));
        var btnPrev = new kony.ui.Button(extendConfig({
            "height": "30dp",
            "id": "btnPrev",
            "isVisible": true,
            "right": "8dp",
            "skin": "sknBtnDisable",
            "text": "O",
            "top": "10dp",
            "width": 30,
            "zIndex": 1
        }, controller.args[0], "btnPrev"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPrev"), extendConfig({}, controller.args[2], "btnPrev"));
        var lblSearchPage = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "lblSearchPage",
            "isVisible": true,
            "right": "32dp",
            "skin": "CopydefLabel0b044b7d672a44a",
            "text": "1-6 of 18",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchPage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchPage"), extendConfig({}, controller.args[2], "lblSearchPage"));
        var lblRowPerPageNumber = new kony.ui.Label(extendConfig({
            "id": "lblRowPerPageNumber",
            "isVisible": false,
            "left": "354dp",
            "skin": "CopydefLabel0bfa17407e4df49",
            "text": "Rows Per Page",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRowPerPageNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRowPerPageNumber"), extendConfig({}, controller.args[2], "lblRowPerPageNumber"));
        flxPagination.add(btnNext, btnPrev, lblSearchPage, lblRowPerPageNumber);
        flxRevisedPayment.add(flxRevisedPmtHeader, flxRevisedPaymentDetails, flxPagination);
        flxRevisedPaymentSchedule.add(flxRevisedPayment);
        RevisedPaymentSchedule.add(flxRevisedPaymentSchedule);
        RevisedPaymentSchedule.compInstData = {}
        return RevisedPaymentSchedule;
    }
})