define(function() {
    return function(controller) {
        var ErrorAllert = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "72dp",
            "id": "ErrorAllert",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "18dp",
            "isModalContainer": false,
            "right": 32,
            "skin": "CopyslFbox0i274505141e54c",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "ErrorAllert"), extendConfig({}, controller.args[1], "ErrorAllert"), extendConfig({}, controller.args[2], "ErrorAllert"));
        ErrorAllert.setDefaultUnit(kony.flex.DP);
        var FlexContainer0dbccc9f9939044 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "72dp",
            "id": "FlexContainer0dbccc9f9939044",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "4dp",
            "zIndex": 1
        }, controller.args[0], "FlexContainer0dbccc9f9939044"), extendConfig({}, controller.args[1], "FlexContainer0dbccc9f9939044"), extendConfig({}, controller.args[2], "FlexContainer0dbccc9f9939044"));
        FlexContainer0dbccc9f9939044.setDefaultUnit(kony.flex.DP);
        FlexContainer0dbccc9f9939044.add();
        var flxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0ee2bb947586247",
            "top": "15dp",
            "width": "53dp",
            "zIndex": 1
        }, controller.args[0], "flxError"), extendConfig({}, controller.args[1], "flxError"), extendConfig({}, controller.args[2], "flxError"));
        flxError.setDefaultUnit(kony.flex.DP);
        var FlexContainer0c96d2d6573ff47 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "FlexContainer0c96d2d6573ff47",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0a1e681babae747",
            "top": "0dp",
            "width": "4dp",
            "zIndex": 1
        }, controller.args[0], "FlexContainer0c96d2d6573ff47"), extendConfig({}, controller.args[1], "FlexContainer0c96d2d6573ff47"), extendConfig({}, controller.args[2], "FlexContainer0c96d2d6573ff47"));
        FlexContainer0c96d2d6573ff47.setDefaultUnit(kony.flex.DP);
        FlexContainer0c96d2d6573ff47.add();
        var lblError = new kony.ui.Label(extendConfig({
            "height": "18dp",
            "id": "lblError",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopydefLabel0ief89495d3c543",
            "text": "Alert",
            "top": "2dp",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "lblError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError"), extendConfig({}, controller.args[2], "lblError"));
        flxError.add(FlexContainer0c96d2d6573ff47, lblError);
        var imgCloseBackup = new kony.ui.Image2(extendConfig({
            "height": "14dp",
            "id": "imgCloseBackup",
            "isVisible": false,
            "left": "1100dp",
            "skin": "CopyslImage0a38c8a6971fb49",
            "src": "ico_close.png",
            "top": "15dp",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgCloseBackup"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCloseBackup"), extendConfig({}, controller.args[2], "imgCloseBackup"));
        var lblMessage = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "lblMessage",
            "isVisible": true,
            "left": "180dp",
            "skin": "CopydefLabel0dddb32e87e7e4d",
            "text": "Subtitle text goes here",
            "top": "14dp",
            "width": "786dp",
            "zIndex": 1
        }, controller.args[0], "lblMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMessage"), extendConfig({}, controller.args[2], "lblMessage"));
        var imgClose1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "14dp",
            "id": "imgClose1",
            "isVisible": false,
            "right": "40dp",
            "skin": "sknTransparentClose",
            "text": "L",
            "top": "15dp",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgClose1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose1"), extendConfig({}, controller.args[2], "imgClose1"));
        var imgClose2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "imgClose2",
            "isVisible": false,
            "right": "40dp",
            "text": "L",
            "top": "15dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgClose2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose2"), extendConfig({}, controller.args[2], "imgClose2"));
        var Label0c0024c57dfb34f = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "Label0c0024c57dfb34f",
            "isVisible": false,
            "left": "50dp",
            "text": "Error Notification",
            "top": "13dp",
            "width": "127dp",
            "zIndex": 1
        }, controller.args[0], "Label0c0024c57dfb34f"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "Label0c0024c57dfb34f"), extendConfig({}, controller.args[2], "Label0c0024c57dfb34f"));
        var lblImag = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "24dp",
            "id": "lblImag",
            "isVisible": false,
            "left": "9dp",
            "skin": "CopydefLabel0e20f0d7c83ae40",
            "text": "W",
            "top": "11dp",
            "width": "24dp",
            "zIndex": 1
        }, controller.args[0], "lblImag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImag"), extendConfig({}, controller.args[2], "lblImag"));
        var lblErrorNotification = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblErrorNotification",
            "isVisible": true,
            "left": "50dp",
            "skin": "CopydefLabel0h7164d1a66954d",
            "text": "Error Notification",
            "top": "13dp",
            "width": "127dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorNotification"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorNotification"), extendConfig({}, controller.args[2], "lblErrorNotification"));
        var imgClose = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "defBtnFocus",
            "height": "20dp",
            "id": "imgClose",
            "isVisible": true,
            "right": "40dp",
            "skin": "CopydefBtnNormal0h6f71e9952dd41",
            "text": "L",
            "top": "15dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose"), extendConfig({}, controller.args[2], "imgClose"));
        var btnImage = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "24dp",
            "id": "btnImage",
            "isVisible": true,
            "left": "9dp",
            "right": "64dp",
            "skin": "CopysknBtnImgClose0b16845fdda1d47",
            "text": "W",
            "top": "11dp",
            "width": "24dp",
            "zIndex": 1
        }, controller.args[0], "btnImage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnImage"), extendConfig({}, controller.args[2], "btnImage"));
        ErrorAllert.add(FlexContainer0dbccc9f9939044, flxError, imgCloseBackup, lblMessage, imgClose1, imgClose2, Label0c0024c57dfb34f, lblImag, lblErrorNotification, imgClose, btnImage);
        ErrorAllert.compInstData = {}
        return ErrorAllert;
    }
})