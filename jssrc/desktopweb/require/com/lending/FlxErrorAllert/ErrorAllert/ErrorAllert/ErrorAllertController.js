define("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllert/userErrorAllertController", function() {
    return {};
});
define("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllert/ErrorAllertControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllert/ErrorAllertController", ["com/lending/FlxErrorAllert/ErrorAllert/ErrorAllert/userErrorAllertController", "com/lending/FlxErrorAllert/ErrorAllert/ErrorAllert/ErrorAllertControllerActions"], function() {
    var controller = require("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllert/userErrorAllertController");
    var actions = require("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllert/ErrorAllertControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
