define("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllertCopy/userErrorAllertCopyController", function() {
    return {};
});
define("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllertCopy/ErrorAllertCopyControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllertCopy/ErrorAllertCopyController", ["com/lending/FlxErrorAllert/ErrorAllert/ErrorAllertCopy/userErrorAllertCopyController", "com/lending/FlxErrorAllert/ErrorAllert/ErrorAllertCopy/ErrorAllertCopyControllerActions"], function() {
    var controller = require("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllertCopy/userErrorAllertCopyController");
    var actions = require("com/lending/FlxErrorAllert/ErrorAllert/ErrorAllertCopy/ErrorAllertCopyControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
