define(function() {
    return function(controller) {
        var ErrorAllertCopy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "72dp",
            "id": "ErrorAllertCopy",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "18dp",
            "isModalContainer": false,
            "right": 32,
            "skin": "CopyCopyslFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "ErrorAllertCopy"), extendConfig({}, controller.args[1], "ErrorAllertCopy"), extendConfig({}, controller.args[2], "ErrorAllertCopy"));
        ErrorAllertCopy.setDefaultUnit(kony.flex.DP);
        var flxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxError",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox0c1bf8540b1534c",
            "top": "15dp",
            "width": "53dp",
            "zIndex": 1
        }, controller.args[0], "flxError"), extendConfig({}, controller.args[1], "flxError"), extendConfig({}, controller.args[2], "flxError"));
        flxError.setDefaultUnit(kony.flex.DP);
        var FlexContainer0c96d2d6573ff47 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "FlexContainer0c96d2d6573ff47",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox1",
            "top": "0dp",
            "width": "4dp",
            "zIndex": 1
        }, controller.args[0], "FlexContainer0c96d2d6573ff47"), extendConfig({}, controller.args[1], "FlexContainer0c96d2d6573ff47"), extendConfig({}, controller.args[2], "FlexContainer0c96d2d6573ff47"));
        FlexContainer0c96d2d6573ff47.setDefaultUnit(kony.flex.DP);
        FlexContainer0c96d2d6573ff47.add();
        var lblError = new kony.ui.Label(extendConfig({
            "height": "18dp",
            "id": "lblError",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopyCopydefLabel",
            "text": "Alert",
            "top": "2dp",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "lblError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError"), extendConfig({}, controller.args[2], "lblError"));
        flxError.add(FlexContainer0c96d2d6573ff47, lblError);
        var imgClose = new kony.ui.Image2(extendConfig({
            "height": "14dp",
            "id": "imgClose",
            "isVisible": true,
            "left": "1100dp",
            "skin": "CopyCopyslImage",
            "src": "ico_close_1.png",
            "top": "15dp",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgClose"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose"), extendConfig({}, controller.args[2], "imgClose"));
        var lblMessage = new kony.ui.Label(extendConfig({
            "height": "30dp",
            "id": "lblMessage",
            "isVisible": true,
            "left": "120dp",
            "skin": "CopyCopydefLabel1",
            "text": "Subtitle text goes here",
            "top": "14dp",
            "width": "786dp",
            "zIndex": 1
        }, controller.args[0], "lblMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMessage"), extendConfig({}, controller.args[2], "lblMessage"));
        ErrorAllertCopy.add(flxError, imgClose, lblMessage);
        ErrorAllertCopy.compInstData = {}
        return ErrorAllertCopy;
    }
})