define("com/lending/fundDetails/FundDetails/userFundDetailsController", function() {
    return {};
});
define("com/lending/fundDetails/FundDetails/FundDetailsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/fundDetails/FundDetails/FundDetailsController", ["com/lending/fundDetails/FundDetails/userFundDetailsController", "com/lending/fundDetails/FundDetails/FundDetailsControllerActions"], function() {
    var controller = require("com/lending/fundDetails/FundDetails/userFundDetailsController");
    var actions = require("com/lending/fundDetails/FundDetails/FundDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
