define(function() {
    return function(controller) {
        var FundDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "128dp",
            "id": "FundDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "FundDetails"), extendConfig({}, controller.args[1], "FundDetails"), extendConfig({}, controller.args[2], "FundDetails"));
        FundDetails.setDefaultUnit(kony.flex.DP);
        var flxFundDetailsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "64dp",
            "id": "flxFundDetailsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopysknFlxContainerBGF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFundDetailsHeader"), extendConfig({}, controller.args[1], "flxFundDetailsHeader"), extendConfig({}, controller.args[2], "flxFundDetailsHeader"));
        flxFundDetailsHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountNumberInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountNumberInfo",
            "isVisible": true,
            "left": "27dp",
            "skin": "CopysknLbl",
            "text": "Account Number",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountNumberInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountNumberInfo"), extendConfig({}, controller.args[2], "lblAccountNumberInfo"));
        var lblAccountTypeInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountTypeInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopysknLbl",
            "text": "Account Type",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountTypeInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountTypeInfo"), extendConfig({}, controller.args[2], "lblAccountTypeInfo"));
        var lblCurrencyValueInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblCurrencyValueInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopysknLbl",
            "text": "CCY",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencyValueInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyValueInfo"), extendConfig({}, controller.args[2], "lblCurrencyValueInfo"));
        var lblAmountValueInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAmountValueInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopysknLbl",
            "text": "Balance",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAmountValueInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountValueInfo"), extendConfig({}, controller.args[2], "lblAmountValueInfo"));
        var lblStartDateInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblStartDateInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopysknLbl",
            "text": "Start Date",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblStartDateInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateInfo"), extendConfig({}, controller.args[2], "lblStartDateInfo"));
        var lblMaturityDateInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblMaturityDateInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopysknLbl",
            "text": "Maturity Date",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblMaturityDateInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaturityDateInfo"), extendConfig({}, controller.args[2], "lblMaturityDateInfo"));
        var lblAccountServiceInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountServiceInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopysknLbl",
            "text": "Account Services",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountServiceInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountServiceInfo"), extendConfig({}, controller.args[2], "lblAccountServiceInfo"));
        flxFundDetailsHeader.add(lblAccountNumberInfo, lblAccountTypeInfo, lblCurrencyValueInfo, lblAmountValueInfo, lblStartDateInfo, lblMaturityDateInfo, lblAccountServiceInfo);
        var flxDepositsDetailsvalue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "64dp",
            "id": "flxDepositsDetailsvalue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "32dp",
            "skin": "sknFlxWhiteBgBoreder",
            "top": "68dp",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxDepositsDetailsvalue"), extendConfig({}, controller.args[1], "flxDepositsDetailsvalue"), extendConfig({}, controller.args[2], "flxDepositsDetailsvalue"));
        flxDepositsDetailsvalue.setDefaultUnit(kony.flex.DP);
        var lblAccountNumberResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountNumberResponse",
            "isVisible": true,
            "left": "27dp",
            "skin": "sknLblFontColorWhite",
            "text": "50004098",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountNumberResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountNumberResponse"), extendConfig({}, controller.args[2], "lblAccountNumberResponse"));
        var lblAccountTypeResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAccountTypeResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "Long Term Deposit",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountTypeResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountTypeResponse"), extendConfig({}, controller.args[2], "lblAccountTypeResponse"));
        var lblCurrencyValueResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCurrencyValueResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "USD",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencyValueResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyValueResponse"), extendConfig({}, controller.args[2], "lblCurrencyValueResponse"));
        var lblAmountValueResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAmountValueResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "83,550.50",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAmountValueResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountValueResponse"), extendConfig({}, controller.args[2], "lblAmountValueResponse"));
        var lblStartDateResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblStartDateResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "19/10/2020",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblStartDateResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateResponse"), extendConfig({}, controller.args[2], "lblStartDateResponse"));
        var lblMaturityDateResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblMaturityDateResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "19/10/2021",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblMaturityDateResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaturityDateResponse"), extendConfig({}, controller.args[2], "lblMaturityDateResponse"));
        var flxListBoxSelectedValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxListBoxSelectedValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 20,
            "minWidth": "160dp",
            "isModalContainer": false,
            "skin": "sknFlxBG2772C3Border",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "flxListBoxSelectedValue"), extendConfig({}, controller.args[1], "flxListBoxSelectedValue"), extendConfig({}, controller.args[2], "flxListBoxSelectedValue"));
        flxListBoxSelectedValue.setDefaultUnit(kony.flex.DP);
        var lblListBoxSelectedValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblListBoxSelectedValue",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblBGFFFFFF",
            "text": "Fund Deposits",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblListBoxSelectedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblListBoxSelectedValue"), extendConfig({}, controller.args[2], "lblListBoxSelectedValue"));
        var imgListBoxSelectedDropDown = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgListBoxSelectedDropDown",
            "isVisible": false,
            "left": "8dp",
            "skin": "slImage",
            "src": "drop_down_arrow_1.png",
            "width": "18dp",
            "zIndex": 1
        }, controller.args[0], "imgListBoxSelectedDropDown"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgListBoxSelectedDropDown"), extendConfig({}, controller.args[2], "imgListBoxSelectedDropDown"));
        var flxontainerLoans = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "2dp",
            "id": "flxontainerLoans",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopysknFlxBG",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxontainerLoans"), extendConfig({}, controller.args[1], "flxontainerLoans"), extendConfig({}, controller.args[2], "flxontainerLoans"));
        flxontainerLoans.setDefaultUnit(kony.flex.DP);
        flxontainerLoans.add();
        flxListBoxSelectedValue.add(lblListBoxSelectedValue, imgListBoxSelectedDropDown, flxontainerLoans);
        flxDepositsDetailsvalue.add(lblAccountNumberResponse, lblAccountTypeResponse, lblCurrencyValueResponse, lblAmountValueResponse, lblStartDateResponse, lblMaturityDateResponse, flxListBoxSelectedValue);
        FundDetails.add(flxFundDetailsHeader, flxDepositsDetailsvalue);
        FundDetails.compInstData = {}
        return FundDetails;
    }
})