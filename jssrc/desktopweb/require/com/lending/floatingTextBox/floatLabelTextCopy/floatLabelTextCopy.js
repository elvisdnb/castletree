define(function() {
    return function(controller) {
        var floatLabelTextCopy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": false,
            "isMaster": true,
            "height": "56dp",
            "id": "floatLabelTextCopy",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyflxTemenosLighest",
            "top": "0dp",
            "width": "100%",
            "zIndex": 3,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "floatLabelTextCopy"), extendConfig({}, controller.args[1], "floatLabelTextCopy"), extendConfig({}, controller.args[2], "floatLabelTextCopy"));
        floatLabelTextCopy.setDefaultUnit(kony.flex.DP);
        var flxBgColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxBgColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyflxTemenosLighest",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBgColor"), extendConfig({}, controller.args[1], "flxBgColor"), extendConfig({}, controller.args[2], "flxBgColor"));
        flxBgColor.setDefaultUnit(kony.flex.DP);
        var txtFloatText = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "CopysknFloatTextPost",
            "height": "100%",
            "id": "txtFloatText",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "CopysknFloatTextPost",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "txtFloatText"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
            "padding": [5, 5, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtFloatText"), extendConfig({
            "autoCorrect": false,
            "hoverSkin": "CopysknFloatTextPost",
            "placeholderSkin": "CopysknFloatTextPost"
        }, controller.args[2], "txtFloatText"));
        var flxBottomLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxBottomLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopysknFlxblueLine",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxBottomLine"), extendConfig({}, controller.args[1], "flxBottomLine"), extendConfig({}, controller.args[2], "flxBottomLine"));
        flxBottomLine.setDefaultUnit(kony.flex.DP);
        flxBottomLine.add();
        var flxFloatLableGrp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxFloatLableGrp",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopytemenosLighestBlue",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFloatLableGrp"), extendConfig({}, controller.args[1], "flxFloatLableGrp"), extendConfig({}, controller.args[2], "flxFloatLableGrp"));
        flxFloatLableGrp.setDefaultUnit(kony.flex.DP);
        var rtAsterix = new kony.ui.RichText(extendConfig({
            "centerY": "50%",
            "id": "rtAsterix",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "defRichTextLink",
            "right": "2dp",
            "skin": "CopysknrtFloatLabel",
            "text": "<sup><red>*</red></sup>",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtAsterix"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [5, 1, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtAsterix"), extendConfig({}, controller.args[2], "rtAsterix"));
        var lblFloatLabel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFloatLabel",
            "isVisible": true,
            "skin": "CopysknFloatLblPreInput",
            "text": "First Name",
            "textStyle": {},
            "width": "100%",
            "zIndex": 1,
            "blur": {
                "enabled": true,
                "value": 0
            }
        }, controller.args[0], "lblFloatLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFloatLabel"), extendConfig({}, controller.args[2], "lblFloatLabel"));
        flxFloatLableGrp.add(rtAsterix, lblFloatLabel);
        var downArrowLbl = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "downArrowLbl",
            "isVisible": true,
            "right": "5%",
            "skin": "Copycastleicon",
            "text": "c",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 3
        }, controller.args[0], "downArrowLbl"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "downArrowLbl"), extendConfig({}, controller.args[2], "downArrowLbl"));
        flxBgColor.add(txtFloatText, flxBottomLine, flxFloatLableGrp, downArrowLbl);
        floatLabelTextCopy.add(flxBgColor);
        floatLabelTextCopy.compInstData = {}
        return floatLabelTextCopy;
    }
})