define("com/lending/loanDetails/loanDetails/userloanDetailsController", function() {
    return {};
});
define("com/lending/loanDetails/loanDetails/loanDetailsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/loanDetails/loanDetails/loanDetailsController", ["com/lending/loanDetails/loanDetails/userloanDetailsController", "com/lending/loanDetails/loanDetails/loanDetailsControllerActions"], function() {
    var controller = require("com/lending/loanDetails/loanDetails/userloanDetailsController");
    var actions = require("com/lending/loanDetails/loanDetails/loanDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
