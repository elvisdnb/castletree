define(function() {
    return function(controller) {
        var loanDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "129dp",
            "id": "loanDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "loanDetails"), extendConfig({}, controller.args[1], "loanDetails"), extendConfig({}, controller.args[2], "loanDetails"));
        loanDetails.setDefaultUnit(kony.flex.DP);
        var flxLoanDetailsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "64dp",
            "id": "flxLoanDetailsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0j9097742fdc64c",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLoanDetailsHeader"), extendConfig({}, controller.args[1], "flxLoanDetailsHeader"), extendConfig({}, controller.args[2], "flxLoanDetailsHeader"));
        flxLoanDetailsHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountNumberInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountNumberInfo",
            "isVisible": true,
            "left": "27dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Account Number",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountNumberInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountNumberInfo"), extendConfig({}, controller.args[2], "lblAccountNumberInfo"));
        var lblLoanTypeInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblLoanTypeInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Loan Type",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblLoanTypeInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTypeInfo"), extendConfig({}, controller.args[2], "lblLoanTypeInfo"));
        var lblCurrencyValueInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblCurrencyValueInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "CCY",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencyValueInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyValueInfo"), extendConfig({}, controller.args[2], "lblCurrencyValueInfo"));
        var lblAmountValueInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAmountValueInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Balance",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAmountValueInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountValueInfo"), extendConfig({}, controller.args[2], "lblAmountValueInfo"));
        var lblStartDateInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblStartDateInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Start Date",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblStartDateInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateInfo"), extendConfig({}, controller.args[2], "lblStartDateInfo"));
        var lblMaturityDateInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblMaturityDateInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Maturity Date",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblMaturityDateInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaturityDateInfo"), extendConfig({}, controller.args[2], "lblMaturityDateInfo"));
        var lblAccountServiceInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountServiceInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Service Action",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountServiceInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountServiceInfo"), extendConfig({}, controller.args[2], "lblAccountServiceInfo"));
        flxLoanDetailsHeader.add(lblAccountNumberInfo, lblLoanTypeInfo, lblCurrencyValueInfo, lblAmountValueInfo, lblStartDateInfo, lblMaturityDateInfo, lblAccountServiceInfo);
        var flxseparatorLinedetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxseparatorLinedetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSeparatorGreye5e5e5",
            "top": "64dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxseparatorLinedetails"), extendConfig({}, controller.args[1], "flxseparatorLinedetails"), extendConfig({}, controller.args[2], "flxseparatorLinedetails"));
        flxseparatorLinedetails.setDefaultUnit(kony.flex.DP);
        flxseparatorLinedetails.add();
        var flxLoanDetailsvalue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxLoanDetailsvalue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "1dp",
            "isModalContainer": false,
            "right": "32dp",
            "skin": "sknFlxWhiteBgBoreder",
            "top": "65dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLoanDetailsvalue"), extendConfig({}, controller.args[1], "flxLoanDetailsvalue"), extendConfig({}, controller.args[2], "flxLoanDetailsvalue"));
        flxLoanDetailsvalue.setDefaultUnit(kony.flex.DP);
        var lblAccountNumberResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountNumberResponse",
            "isVisible": true,
            "left": "27dp",
            "skin": "sknLblFontColorWhite",
            "text": "Account Number",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountNumberResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountNumberResponse"), extendConfig({}, controller.args[2], "lblAccountNumberResponse"));
        var lblLoanTypeResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblLoanTypeResponse",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLblFontColorWhite",
            "text": "Standard Personal Loan",
            "width": "170dp",
            "zIndex": 1
        }, controller.args[0], "lblLoanTypeResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTypeResponse"), extendConfig({}, controller.args[2], "lblLoanTypeResponse"));
        var lblCurrencyValueResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCurrencyValueResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "CCY",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencyValueResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyValueResponse"), extendConfig({}, controller.args[2], "lblCurrencyValueResponse"));
        var lblAmountValueResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAmountValueResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "Balance",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAmountValueResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountValueResponse"), extendConfig({}, controller.args[2], "lblAmountValueResponse"));
        var lblStartDateResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblStartDateResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "Start Date",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblStartDateResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateResponse"), extendConfig({}, controller.args[2], "lblStartDateResponse"));
        var lblMaturityDateResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblMaturityDateResponse",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblFontColorWhite",
            "text": "Maturity Date",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblMaturityDateResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaturityDateResponse"), extendConfig({}, controller.args[2], "lblMaturityDateResponse"));
        var flxListBoxSelectedValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "47%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxListBoxSelectedValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "minWidth": "160dp",
            "isModalContainer": false,
            "skin": "sknFlxBG2772C3Border",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "flxListBoxSelectedValue"), extendConfig({}, controller.args[1], "flxListBoxSelectedValue"), extendConfig({}, controller.args[2], "flxListBoxSelectedValue"));
        flxListBoxSelectedValue.setDefaultUnit(kony.flex.DP);
        var lblListBoxSelectedValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblListBoxSelectedValue",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblBGFFFFFF",
            "text": "Change Interest",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblListBoxSelectedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblListBoxSelectedValue"), extendConfig({}, controller.args[2], "lblListBoxSelectedValue"));
        var imgListBoxSelectedDropDown = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgListBoxSelectedDropDown",
            "isVisible": false,
            "left": "8dp",
            "skin": "slImage",
            "src": "drop_down_arrow.png",
            "width": "18dp",
            "zIndex": 1
        }, controller.args[0], "imgListBoxSelectedDropDown"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgListBoxSelectedDropDown"), extendConfig({}, controller.args[2], "imgListBoxSelectedDropDown"));
        var FlexContainer0ebe191bc1cf946 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "2dp",
            "id": "FlexContainer0ebe191bc1cf946",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "-120dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0da1528041d7845",
            "top": "35dp",
            "width": "220dp",
            "zIndex": 1
        }, controller.args[0], "FlexContainer0ebe191bc1cf946"), extendConfig({}, controller.args[1], "FlexContainer0ebe191bc1cf946"), extendConfig({}, controller.args[2], "FlexContainer0ebe191bc1cf946"));
        FlexContainer0ebe191bc1cf946.setDefaultUnit(kony.flex.DP);
        FlexContainer0ebe191bc1cf946.add();
        flxListBoxSelectedValue.add(lblListBoxSelectedValue, imgListBoxSelectedDropDown, FlexContainer0ebe191bc1cf946);
        flxLoanDetailsvalue.add(lblAccountNumberResponse, lblLoanTypeResponse, lblCurrencyValueResponse, lblAmountValueResponse, lblStartDateResponse, lblMaturityDateResponse, flxListBoxSelectedValue);
        loanDetails.add(flxLoanDetailsHeader, flxseparatorLinedetails, flxLoanDetailsvalue);
        loanDetails.compInstData = {}
        return loanDetails;
    }
})