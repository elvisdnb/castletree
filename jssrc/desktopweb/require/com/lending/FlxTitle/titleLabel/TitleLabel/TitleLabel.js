define(function() {
    return function(controller) {
        var TitleLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "70dp",
            "id": "TitleLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "minHeight": "56dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "TitleLabel"), extendConfig({}, controller.args[1], "TitleLabel"), extendConfig({}, controller.args[2], "TitleLabel"));
        TitleLabel.setDefaultUnit(kony.flex.DP);
        var lblTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblTitle",
            "isVisible": true,
            "left": "15%",
            "skin": "sknLblRobotoMed16pxBlack",
            "text": "Enter Title",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTitle"), extendConfig({}, controller.args[2], "lblTitle"));
        var flxTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxTitle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0c62f59ec8f4c46",
            "top": "95%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTitle"), extendConfig({}, controller.args[1], "flxTitle"), extendConfig({}, controller.args[2], "flxTitle"));
        flxTitle.setDefaultUnit(kony.flex.DP);
        flxTitle.add();
        TitleLabel.add(lblTitle, flxTitle);
        TitleLabel.compInstData = {}
        return TitleLabel;
    }
})