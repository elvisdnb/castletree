define("com/lending/FlxTitle/titleLabel/TitleLabel/userTitleLabelController", function() {
    return {};
});
define("com/lending/FlxTitle/titleLabel/TitleLabel/TitleLabelControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/FlxTitle/titleLabel/TitleLabel/TitleLabelController", ["com/lending/FlxTitle/titleLabel/TitleLabel/userTitleLabelController", "com/lending/FlxTitle/titleLabel/TitleLabel/TitleLabelControllerActions"], function() {
    var controller = require("com/lending/FlxTitle/titleLabel/TitleLabel/userTitleLabelController");
    var actions = require("com/lending/FlxTitle/titleLabel/TitleLabel/TitleLabelControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
