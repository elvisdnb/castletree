define("com/lending/BlockUnblockDetails/Deposits/userDepositsController", function() {
    return {};
});
define("com/lending/BlockUnblockDetails/Deposits/DepositsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/BlockUnblockDetails/Deposits/DepositsController", ["com/lending/BlockUnblockDetails/Deposits/userDepositsController", "com/lending/BlockUnblockDetails/Deposits/DepositsControllerActions"], function() {
    var controller = require("com/lending/BlockUnblockDetails/Deposits/userDepositsController");
    var actions = require("com/lending/BlockUnblockDetails/Deposits/DepositsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
