define("com/lending/contractRequestDescription/ContractRequestDescription/userContractRequestDescriptionController", function() {
    return {
        initGettersSetters: function() {
            this.view.flxInnerCollapseWrapper.isVisible = false;
            this.view.lblArrow.text = 'M';
            /*defineGetter(this, 'selectedKey', () => {
                return this._selectedKey;
            });*/
            /*defineSetter(this, 'selectedKey', value => {
                this._selectedKey = value;
            });*/
        },
        collapseResponseContainer: function() {
            this.view.flxInnerCollapseWrapper.isVisible = false;
            this.view.lblDescriptionNotesHeaderResponse.isVisible = true;
        },
        expandREsponseContainer: function() {
            this.view.flxInnerCollapseWrapper.isVisible = true;
            this.view.lblDescriptionNotesHeaderResponse.isVisible = false;
        },
        populateDataToResponseContainer: function(desc, additionalNote, contractType, contractChannel, contractDate, status) {
            this.view.cusTextAreaDescriptionResponse.setEnabled(false);
            this.view.cusTextAreaDescriptionResponse.text = Application.validation.isNullUndefinedObj(desc);
            this.view.cusTextAreaAdditionalResponse.text = Application.validation.isNullUndefinedObj(additionalNote);
            this.view.lblAppointmentResponse.text = Application.validation.isNullUndefinedObj(contractType);
            this.view.lblContractChannelResponse.text = Application.validation.isNullUndefinedObj(contractChannel);
            this.view.lblContractDateResponse.text = Application.validation.isNullUndefinedObj(contractDate);
            kony.print("status" + status);
            if (Application.validation.isNullUndefinedObj(status) !== "") {
                //this.view.cusDropDownStatus.selectedKey = status;
            }
        },
        getStatusSelectKey: function() {
            return this.view.cusDropDownStatus.selectedKey;
        },
        setStatusSelectedKey: function(key) {
            if (!Application.validation.isNullUndefinedObj(key) !== "") {
                this.view.cusDropDownStatus.selectedKey = key;
            }
        },
    };
});
define("com/lending/contractRequestDescription/ContractRequestDescription/ContractRequestDescriptionControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/lending/contractRequestDescription/ContractRequestDescription/ContractRequestDescriptionController", ["com/lending/contractRequestDescription/ContractRequestDescription/userContractRequestDescriptionController", "com/lending/contractRequestDescription/ContractRequestDescription/ContractRequestDescriptionControllerActions"], function() {
    var controller = require("com/lending/contractRequestDescription/ContractRequestDescription/userContractRequestDescriptionController");
    var actions = require("com/lending/contractRequestDescription/ContractRequestDescription/ContractRequestDescriptionControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "arrowChangeText", function(val) {
            this.view.lblArrow.text = val;
        });
        defineGetter(this, "arrowChangeText", function() {
            return this.view.lblArrow.text;
        });
        defineSetter(this, "ContractTypeResponse", function(val) {
            this.view.lblAppointmentResponse.text = val;
        });
        defineGetter(this, "ContractTypeResponse", function() {
            return this.view.lblAppointmentResponse.text;
        });
        defineSetter(this, "contractChannelResponse", function(val) {
            this.view.lblContractChannelResponse.text = val;
        });
        defineGetter(this, "contractChannelResponse", function() {
            return this.view.lblContractChannelResponse.text;
        });
        defineSetter(this, "contractDateResponse", function(val) {
            this.view.lblContractDateResponse.text = val;
        });
        defineGetter(this, "contractDateResponse", function() {
            return this.view.lblContractDateResponse.text;
        });
        defineSetter(this, "descriptionText", function(val) {
            this.view.cusTextAreaDescriptionResponse.text = val;
        });
        defineGetter(this, "descriptionText", function() {
            return this.view.cusTextAreaDescriptionResponse.text;
        });
        defineSetter(this, "additionalNotesValue", function(val) {
            this.view.cusTextAreaAdditionalResponse.text = val;
        });
        defineGetter(this, "additionalNotesValue", function() {
            return this.view.cusTextAreaAdditionalResponse.text;
        });
        defineSetter(this, "buttonText", function(val) {
            this.view.btnSubmit.text = val;
        });
        defineGetter(this, "buttonText", function() {
            return this.view.btnSubmit.text;
        });
        defineSetter(this, "statusMasterData", function(val) {
            this.view.cusDropDownStatus.masterData = val;
        });
        defineGetter(this, "statusMasterData", function() {
            return this.view.cusDropDownStatus.masterData;
        });
        defineSetter(this, "selectedKey1", function(val) {
            this.view.cusDropDownStatus.selectedKey = val;
        });
        defineGetter(this, "selectedKey1", function() {
            return this.view.cusDropDownStatus.selectedKey;
        });
        defineSetter(this, "selectedKeys", function(val) {
            this.view.cusDropDownStatus.selectedKeys = val;
        });
        defineGetter(this, "selectedKeys", function() {
            return this.view.cusDropDownStatus.selectedKeys;
        });
        defineSetter(this, "descriptionHeaderResponse", function(val) {
            this.view.lblDescriptionNotesHeaderResponse.text = val;
        });
        defineGetter(this, "descriptionHeaderResponse", function() {
            return this.view.lblDescriptionNotesHeaderResponse.text;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    controller.AS_arrowOnTouchEnd_a96cdd41788846b1a7e7c17606b9af93 = function() {
        if (this.arrowOnTouchEnd) {
            this.arrowOnTouchEnd.apply(this, arguments);
        }
    }
    controller.AS_onClickSubmit_j387c199c0314df8b0ddda33f5550ab0 = function() {
        if (this.onClickSubmit) {
            this.onClickSubmit.apply(this, arguments);
        }
    }
    return controller;
});
