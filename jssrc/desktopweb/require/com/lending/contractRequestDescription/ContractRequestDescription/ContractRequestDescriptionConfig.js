define(function() {
    return {
        "properties": [{
            "name": "arrowChangeText",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "ContractTypeResponse",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "contractChannelResponse",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "contractDateResponse",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "descriptionText",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "additionalNotesValue",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "buttonText",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "statusMasterData",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "selectedKey1",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "selectedKeys",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "descriptionHeaderResponse",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": ["collapseResponseContainer", "expandREsponseContainer", "populateDataToResponseContainer", "getStatusSelectKey", "setStatusSelectedKey"],
        "events": ["onClickSubmit", "arrowOnTouchEnd"]
    }
});