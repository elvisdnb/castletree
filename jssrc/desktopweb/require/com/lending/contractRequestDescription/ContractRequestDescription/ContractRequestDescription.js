define(function() {
    return function(controller) {
        var ContractRequestDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "ContractRequestDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBGffffffBorderCCE3F7",
            "top": "0dp",
            "width": "100%",
            "zIndex": kony.flex.ZINDEX_AUTO,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1180, 1280, 1366]
        }, controller.args[0], "ContractRequestDescription"), extendConfig({}, controller.args[1], "ContractRequestDescription"), extendConfig({}, controller.args[2], "ContractRequestDescription"));
        ContractRequestDescription.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "32dp",
            "isModalContainer": false,
            "right": "32dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var lblRequestDescriptionHeaderInfo = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblRequestDescriptionHeaderInfo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl16px005096BGRotoMedium",
            "text": "Request Description",
            "top": "23dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblRequestDescriptionHeaderInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRequestDescriptionHeaderInfo"), extendConfig({}, controller.args[2], "lblRequestDescriptionHeaderInfo"));
        var lblArrow = new kony.ui.Label(extendConfig({
            "height": "24dp",
            "id": "lblArrow",
            "isVisible": true,
            "onTouchEnd": controller.AS_arrowOnTouchEnd_a96cdd41788846b1a7e7c17606b9af93,
            "right": "0dp",
            "skin": "sknLblFontIconArrow0075DB",
            "text": "M",
            "top": "22dp",
            "width": "24dp",
            "zIndex": 1
        }, controller.args[0], "lblArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblArrow"), extendConfig({}, controller.args[2], "lblArrow"));
        var flxLineSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxLineSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "32dp",
            "skin": "sknFlxBorderCCE3F7",
            "top": "67dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLineSeparator1"), extendConfig({}, controller.args[1], "flxLineSeparator1"), extendConfig({}, controller.args[2], "flxLineSeparator1"));
        flxLineSeparator1.setDefaultUnit(kony.flex.DP);
        flxLineSeparator1.add();
        var lblDescriptionNotesHeaderResponse = new kony.ui.Label(extendConfig({
            "id": "lblDescriptionNotesHeaderResponse",
            "isVisible": true,
            "right": "50dp",
            "skin": "sknFont14px757575",
            "text": "  Payment holyday for three months with recalculation of payment for ...",
            "top": "23dp",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionNotesHeaderResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionNotesHeaderResponse"), extendConfig({}, controller.args[2], "lblDescriptionNotesHeaderResponse"));
        flxHeader.add(lblRequestDescriptionHeaderInfo, lblArrow, flxLineSeparator1, lblDescriptionNotesHeaderResponse);
        var flxInnerCollapseWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxInnerCollapseWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "32dp",
            "isModalContainer": false,
            "right": "32dp",
            "skin": "slFbox",
            "top": "69dp",
            "zIndex": kony.flex.ZINDEX_AUTO
        }, controller.args[0], "flxInnerCollapseWrapper"), extendConfig({}, controller.args[1], "flxInnerCollapseWrapper"), extendConfig({}, controller.args[2], "flxInnerCollapseWrapper"));
        flxInnerCollapseWrapper.setDefaultUnit(kony.flex.DP);
        var flxHeadingLabelWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxHeadingLabelWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": kony.flex.ZINDEX_AUTO
        }, controller.args[0], "flxHeadingLabelWrapper"), extendConfig({}, controller.args[1], "flxHeadingLabelWrapper"), extendConfig({}, controller.args[2], "flxHeadingLabelWrapper"));
        flxHeadingLabelWrapper.setDefaultUnit(kony.flex.DP);
        var lblDescriptionInfo = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblDescriptionInfo",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLblBg434343px14RobotoMedium",
            "text": "Description",
            "top": "10dp",
            "width": "83dp",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionInfo"), extendConfig({}, controller.args[2], "lblDescriptionInfo"));
        var lblAdditionalNotesInfo = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblAdditionalNotesInfo",
            "isVisible": true,
            "left": "204dp",
            "skin": "sknLblBg434343px14RobotoMedium",
            "text": "Additional Notes",
            "top": "10dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblAdditionalNotesInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalNotesInfo"), extendConfig({}, controller.args[2], "lblAdditionalNotesInfo"));
        var lblContractTypeInfo = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblContractTypeInfo",
            "isVisible": true,
            "left": "161dp",
            "skin": "sknLblBg434343px14RobotoMedium",
            "text": "Contact Type",
            "top": "10dp",
            "width": "93dp",
            "zIndex": 1
        }, controller.args[0], "lblContractTypeInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractTypeInfo"), extendConfig({}, controller.args[2], "lblContractTypeInfo"));
        var lblContractChannelInfo = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblContractChannelInfo",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblBg434343px14RobotoMedium",
            "text": "Contact Channel",
            "top": "10dp",
            "width": "113dp",
            "zIndex": 1
        }, controller.args[0], "lblContractChannelInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractChannelInfo"), extendConfig({}, controller.args[2], "lblContractChannelInfo"));
        var lblContractDateInfo = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblContractDateInfo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLblBg434343px14RobotoMedium",
            "text": "Contact Date",
            "top": "10dp",
            "width": "93dp",
            "zIndex": 1
        }, controller.args[0], "lblContractDateInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractDateInfo"), extendConfig({}, controller.args[2], "lblContractDateInfo"));
        var lblStatusInfo = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblStatusInfo",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblBg434343px14RobotoMedium",
            "text": "Status",
            "top": "10dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblStatusInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatusInfo"), extendConfig({}, controller.args[2], "lblStatusInfo"));
        flxHeadingLabelWrapper.add(lblDescriptionInfo, lblAdditionalNotesInfo, lblContractTypeInfo, lblContractChannelInfo, lblContractDateInfo, lblStatusInfo);
        var flxLineSeparato2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "1dp",
            "id": "flxLineSeparato2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxLineBG707070",
            "top": "1dp",
            "width": "100%",
            "zIndex": kony.flex.ZINDEX_AUTO
        }, controller.args[0], "flxLineSeparato2"), extendConfig({}, controller.args[1], "flxLineSeparato2"), extendConfig({}, controller.args[2], "flxLineSeparato2"));
        flxLineSeparato2.setDefaultUnit(kony.flex.DP);
        flxLineSeparato2.add();
        var flxResponseWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "106dp",
            "id": "flxResponseWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "zIndex": kony.flex.ZINDEX_AUTO
        }, controller.args[0], "flxResponseWrapper"), extendConfig({}, controller.args[1], "flxResponseWrapper"), extendConfig({}, controller.args[2], "flxResponseWrapper"));
        flxResponseWrapper.setDefaultUnit(kony.flex.DP);
        var cusTextAreaDescriptionResponse = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "height": "70dp",
            "id": "cusTextAreaDescriptionResponse",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "3dp",
            "numberOfVisibleLines": 3,
            "skin": "sknTxtAreaFont575757px14RotoRegular",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "18dp",
            "width": "250dp",
            "zIndex": 1
        }, controller.args[0], "cusTextAreaDescriptionResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "cusTextAreaDescriptionResponse"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "cusTextAreaDescriptionResponse"));
        var cusTextAreaAdditionalResponse = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "height": "70dp",
            "id": "cusTextAreaAdditionalResponse",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "36dp",
            "numberOfVisibleLines": 3,
            "skin": "sknTxtArea575757px14RotoRegularBGF2F7FD",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "18dp",
            "width": "250dp",
            "zIndex": 1
        }, controller.args[0], "cusTextAreaAdditionalResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "cusTextAreaAdditionalResponse"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "cusTextAreaAdditionalResponse"));
        var lblAppointmentResponse = new kony.ui.Label(extendConfig({
            "height": "70dp",
            "id": "lblAppointmentResponse",
            "isVisible": true,
            "left": "32dp",
            "skin": "sknLblFont14PX575757RotoRegular",
            "text": "Appointment",
            "top": "18dp",
            "width": "85dp",
            "zIndex": 1
        }, controller.args[0], "lblAppointmentResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAppointmentResponse"), extendConfig({}, controller.args[2], "lblAppointmentResponse"));
        var lblContractChannelResponse = new kony.ui.Label(extendConfig({
            "height": "70dp",
            "id": "lblContractChannelResponse",
            "isVisible": true,
            "left": "38dp",
            "skin": "sknLblFont14PX575757RotoRegular",
            "text": "Appointment",
            "top": "18dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblContractChannelResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractChannelResponse"), extendConfig({}, controller.args[2], "lblContractChannelResponse"));
        var lblContractDateResponse = new kony.ui.Label(extendConfig({
            "height": "70dp",
            "id": "lblContractDateResponse",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblFont14PX575757RotoRegular",
            "text": "Appointment",
            "top": "18dp",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "lblContractDateResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractDateResponse"), extendConfig({}, controller.args[2], "lblContractDateResponse"));
        var cusDropDownStatus = new kony.ui.ListBox(extendConfig({
            "height": "40dp",
            "id": "cusDropDownStatus",
            "isVisible": true,
            "left": "24dp",
            "masterData": [
                ["NEW", "New"],
                ["PENDING", "Pending"],
                ["CANCELLED", "Cancelled"]
            ],
            "skin": "sknDropDown14pxUUX",
            "top": "32dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "cusDropDownStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "cusDropDownStatus"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "cusDropDownStatus"));
        var btnSubmit = new kony.ui.Button(extendConfig({
            "height": "32dp",
            "id": "btnSubmit",
            "isVisible": true,
            "left": "24dp",
            "onClick": controller.AS_onClickSubmit_j387c199c0314df8b0ddda33f5550ab0,
            "skin": "sknBtnFontWhite12pxBG6f61d0",
            "text": "Submit",
            "top": "36dp",
            "width": "104dp",
            "zIndex": 1
        }, controller.args[0], "btnSubmit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSubmit"), extendConfig({}, controller.args[2], "btnSubmit"));
        flxResponseWrapper.add(cusTextAreaDescriptionResponse, cusTextAreaAdditionalResponse, lblAppointmentResponse, lblContractChannelResponse, lblContractDateResponse, cusDropDownStatus, btnSubmit);
        var flxLineSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxLineSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxLineBG707070",
            "top": "1dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLineSeparator3"), extendConfig({}, controller.args[1], "flxLineSeparator3"), extendConfig({}, controller.args[2], "flxLineSeparator3"));
        flxLineSeparator3.setDefaultUnit(kony.flex.DP);
        flxLineSeparator3.add();
        var flxDummy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "26dp",
            "id": "flxDummy",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDummy"), extendConfig({}, controller.args[1], "flxDummy"), extendConfig({}, controller.args[2], "flxDummy"));
        flxDummy.setDefaultUnit(kony.flex.DP);
        flxDummy.add();
        flxInnerCollapseWrapper.add(flxHeadingLabelWrapper, flxLineSeparato2, flxResponseWrapper, flxLineSeparator3, flxDummy);
        ContractRequestDescription.add(flxHeader, flxInnerCollapseWrapper);
        ContractRequestDescription.breakpointResetData = {};
        ContractRequestDescription.breakpointData = {
            maxBreakpointWidth: 1366,
            "1180": {
                "lblDescriptionInfo": {
                    "width": {
                        "type": "string",
                        "value": "210dp"
                    },
                    "segmentProps": []
                },
                "lblAdditionalNotesInfo": {
                    "left": {
                        "type": "string",
                        "value": "36dp"
                    },
                    "width": {
                        "type": "string",
                        "value": "210dp"
                    },
                    "segmentProps": []
                },
                "lblContractTypeInfo": {
                    "left": {
                        "type": "string",
                        "value": "32dp"
                    },
                    "width": {
                        "type": "string",
                        "value": "85dp"
                    },
                    "segmentProps": []
                },
                "lblContractChannelInfo": {
                    "left": {
                        "type": "string",
                        "value": "38dp"
                    },
                    "width": {
                        "type": "string",
                        "value": "120dp"
                    },
                    "segmentProps": []
                },
                "lblContractDateInfo": {
                    "left": {
                        "type": "string",
                        "value": "5dp"
                    },
                    "width": {
                        "type": "string",
                        "value": "100dp"
                    },
                    "segmentProps": []
                },
                "lblStatusInfo": {
                    "left": {
                        "type": "string",
                        "value": "24dp"
                    },
                    "segmentProps": []
                },
                "cusTextAreaDescriptionResponse": {
                    "width": {
                        "type": "string",
                        "value": "210dp"
                    },
                    "segmentProps": []
                },
                "cusTextAreaAdditionalResponse": {
                    "width": {
                        "type": "string",
                        "value": "210dp"
                    },
                    "segmentProps": []
                }
            },
            "1280": {
                "ContractRequestDescription": {
                    "zIndex": 2,
                    "segmentProps": [],
                    "isSrcCompTopFlex": true
                },
                "flxInnerCollapseWrapper": {
                    "zIndex": 1,
                    "segmentProps": []
                },
                "flxHeadingLabelWrapper": {
                    "zIndex": 1,
                    "segmentProps": []
                },
                "flxLineSeparato2": {
                    "zIndex": 1,
                    "segmentProps": []
                },
                "flxResponseWrapper": {
                    "zIndex": 1,
                    "segmentProps": []
                }
            }
        }
        ContractRequestDescription.compInstData = {}
        return ContractRequestDescription;
    }
})