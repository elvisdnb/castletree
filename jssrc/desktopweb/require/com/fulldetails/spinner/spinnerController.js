define("com/fulldetails/spinner/userspinnerController", function() {
    return {};
});
define("com/fulldetails/spinner/spinnerControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/fulldetails/spinner/spinnerController", ["com/fulldetails/spinner/userspinnerController", "com/fulldetails/spinner/spinnerControllerActions"], function() {
    var controller = require("com/fulldetails/spinner/userspinnerController");
    var actions = require("com/fulldetails/spinner/spinnerControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
