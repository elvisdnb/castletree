define(function() {
    return function(controller) {
        var spinner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "spinner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0ff6a8d8239db42",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "spinner"), extendConfig({}, controller.args[1], "spinner"), extendConfig({}, controller.args[2], "spinner"));
        spinner.setDefaultUnit(kony.flex.DP);
        var imgSpinnerFulldetails = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "30dp",
            "id": "imgSpinnerFulldetails",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "loadingblue.gif",
            "top": "0",
            "width": "30dp"
        }, controller.args[0], "imgSpinnerFulldetails"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSpinnerFulldetails"), extendConfig({}, controller.args[2], "imgSpinnerFulldetails"));
        spinner.add(imgSpinnerFulldetails);
        return spinner;
    }
})