define(function() {
    return function(controller) {
        var uuxButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "uuxButton",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "uuxButton"), extendConfig({}, controller.args[1], "uuxButton"), extendConfig({}, controller.args[2], "uuxButton"));
        uuxButton.setDefaultUnit(kony.flex.DP);
        var UUXButtonWrapper = new kony.ui.CustomWidget(extendConfig({
            "id": "UUXButtonWrapper",
            "isVisible": true,
            "left": "0",
            "top": "0",
            "width": "100%",
            "height": "100%",
            "clipBounds": false
        }, controller.args[0], "UUXButtonWrapper"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "UUXButtonWrapper"), extendConfig({
            "widgetName": "uuxButton",
            "compact": false,
            "contained": false,
            "cta": true,
            "disabled": false,
            "icon": "",
            "labelText": "",
            "outlined": false,
            "size": {
                "optionsList": ["small", "medium", "large", "X-large"],
                "selectedValue": "large"
            },
            "trailingIcon": false,
            "typeButton": {
                "optionsList": ["button", "submit", "reset"],
                "selectedValue": "button"
            },
            "onclickEvent": controller.AS_onclickEvent_f6604aa7c2c84700b78149c7a41a54d5
        }, controller.args[2], "UUXButtonWrapper"));
        uuxButton.add(UUXButtonWrapper);
        uuxButton.compInstData = {}
        return uuxButton;
    }
})