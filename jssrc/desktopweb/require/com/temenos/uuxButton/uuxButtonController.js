define("com/temenos/uuxButton/useruuxButtonController", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {},
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {}
    };
});
define("com/temenos/uuxButton/uuxButtonControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/temenos/uuxButton/uuxButtonController", ["com/temenos/uuxButton/useruuxButtonController", "com/temenos/uuxButton/uuxButtonControllerActions"], function() {
    var controller = require("com/temenos/uuxButton/useruuxButtonController");
    var actions = require("com/temenos/uuxButton/uuxButtonControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "cta", function(val) {
            this.view.UUXButtonWrapper.cta = val;
        });
        defineGetter(this, "cta", function() {
            return this.view.UUXButtonWrapper.cta;
        });
        defineSetter(this, "labelText", function(val) {
            this.view.UUXButtonWrapper.labelText = val;
        });
        defineGetter(this, "labelText", function() {
            return this.view.UUXButtonWrapper.labelText;
        });
        defineSetter(this, "icon", function(val) {
            this.view.UUXButtonWrapper.icon = val;
        });
        defineGetter(this, "icon", function() {
            return this.view.UUXButtonWrapper.icon;
        });
        defineSetter(this, "typeButton", function(val) {
            this.view.UUXButtonWrapper.typeButton = val;
        });
        defineGetter(this, "typeButton", function() {
            return this.view.UUXButtonWrapper.typeButton;
        });
        defineSetter(this, "size", function(val) {
            this.view.UUXButtonWrapper.size = val;
        });
        defineGetter(this, "size", function() {
            return this.view.UUXButtonWrapper.size;
        });
        defineSetter(this, "outlined", function(val) {
            this.view.UUXButtonWrapper.outlined = val;
        });
        defineGetter(this, "outlined", function() {
            return this.view.UUXButtonWrapper.outlined;
        });
        defineSetter(this, "disabled", function(val) {
            this.view.UUXButtonWrapper.disabled = val;
        });
        defineGetter(this, "disabled", function() {
            return this.view.UUXButtonWrapper.disabled;
        });
        defineSetter(this, "trailingIcon", function(val) {
            this.view.UUXButtonWrapper.trailingIcon = val;
        });
        defineGetter(this, "trailingIcon", function() {
            return this.view.UUXButtonWrapper.trailingIcon;
        });
        defineSetter(this, "compact", function(val) {
            this.view.UUXButtonWrapper.compact = val;
        });
        defineGetter(this, "compact", function() {
            return this.view.UUXButtonWrapper.compact;
        });
        defineSetter(this, "contained", function(val) {
            this.view.UUXButtonWrapper.contained = val;
        });
        defineGetter(this, "contained", function() {
            return this.view.UUXButtonWrapper.contained;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    controller.AS_onclickEvent_f6604aa7c2c84700b78149c7a41a54d5 = function() {
        if (this.onclickEvent) {
            this.onclickEvent.apply(this, arguments);
        }
    }
    return controller;
});
