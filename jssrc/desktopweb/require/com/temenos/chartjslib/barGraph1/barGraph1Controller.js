define("com/temenos/chartjslib/barGraph1/userbarGraph1Controller", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {},
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {},
        updateGraph: function() {
            kony.print("update graph function called on component, will this trigger even to tpw");
            //Please set valid  properties before calling this property update
            //e.g. Do not hide graph on initial rendering
            // var mydatasets = [{"label":"lbl1","backgroundColor":"#FF9800","data":[10,11,9]},{"label":"lbl2","backgroundColor":"#2C8631","data":[8,4,5]}]
            // var xlabels = ["a", "b", "c"]
            this.view.TPWbargraph1.updateBars = true;
        }
    };
});
define("com/temenos/chartjslib/barGraph1/barGraph1ControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/temenos/chartjslib/barGraph1/barGraph1Controller", ["com/temenos/chartjslib/barGraph1/userbarGraph1Controller", "com/temenos/chartjslib/barGraph1/barGraph1ControllerActions"], function() {
    var controller = require("com/temenos/chartjslib/barGraph1/userbarGraph1Controller");
    var actions = require("com/temenos/chartjslib/barGraph1/barGraph1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "canvasWidth", function(val) {
            this.view.TPWbargraph1.canvasWidth = val;
        });
        defineGetter(this, "canvasWidth", function() {
            return this.view.TPWbargraph1.canvasWidth;
        });
        defineSetter(this, "canvasHeight", function(val) {
            this.view.TPWbargraph1.canvasHeight = val;
        });
        defineGetter(this, "canvasHeight", function() {
            return this.view.TPWbargraph1.canvasHeight;
        });
        defineSetter(this, "datasetsArray", function(val) {
            this.view.TPWbargraph1.datasetsArray = val;
        });
        defineGetter(this, "datasetsArray", function() {
            return this.view.TPWbargraph1.datasetsArray;
        });
        defineSetter(this, "xlabelsArray", function(val) {
            this.view.TPWbargraph1.xlabelsArray = val;
        });
        defineGetter(this, "xlabelsArray", function() {
            return this.view.TPWbargraph1.xlabelsArray;
        });
        defineSetter(this, "lineAreaFill", function(val) {
            this.view.TPWbargraph1.lineAreaFill = val;
        });
        defineGetter(this, "lineAreaFill", function() {
            return this.view.TPWbargraph1.lineAreaFill;
        });
        defineSetter(this, "yBeginsAtZero", function(val) {
            this.view.TPWbargraph1.yBeginsAtZero = val;
        });
        defineGetter(this, "yBeginsAtZero", function() {
            return this.view.TPWbargraph1.yBeginsAtZero;
        });
        defineSetter(this, "xlabelString", function(val) {
            this.view.TPWbargraph1.xlabelString = val;
        });
        defineGetter(this, "xlabelString", function() {
            return this.view.TPWbargraph1.xlabelString;
        });
        defineSetter(this, "ylabelString", function(val) {
            this.view.TPWbargraph1.ylabelString = val;
        });
        defineGetter(this, "ylabelString", function() {
            return this.view.TPWbargraph1.ylabelString;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});
