define(function() {
    return function(controller) {
        var barGraph1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "barGraph1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "barGraph1"), extendConfig({}, controller.args[1], "barGraph1"), extendConfig({}, controller.args[2], "barGraph1"));
        barGraph1.setDefaultUnit(kony.flex.DP);
        var TPWbargraph1 = new kony.ui.CustomWidget(extendConfig({
            "id": "TPWbargraph1",
            "isVisible": true,
            "left": "0",
            "top": "0",
            "width": "100%",
            "height": "100%",
            "clipBounds": false
        }, controller.args[0], "TPWbargraph1"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "TPWbargraph1"), extendConfig({
            "widgetName": "barGraph1",
            "canvasHeight": "800",
            "canvasWidth": "600",
            "chartObj": "",
            "datasetsArray": "[]",
            "lineAreaFill": false,
            "typevalue": "",
            "updateBars": false,
            "xlabelString": "",
            "xlabelsArray": "[]",
            "yBeginsAtZero": true,
            "ylabelString": ""
        }, controller.args[2], "TPWbargraph1"));
        barGraph1.add(TPWbargraph1);
        barGraph1.compInstData = {}
        return barGraph1;
    }
})