define(function() {
    return {
        "properties": [{
            "name": "canvasWidth",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "ySuggestedMin",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "canvasHeight",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "xlabelsArray",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "datasetsArray",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "lineAreaFill",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "yBeginsAtZero",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "yStepSize",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "ySuggestedMax",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": ["updateGraph"],
        "events": []
    }
});