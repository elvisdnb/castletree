define(function() {
    return function(controller) {
        var barGraph2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "barGraph2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "barGraph2"), extendConfig({}, controller.args[1], "barGraph2"), extendConfig({}, controller.args[2], "barGraph2"));
        barGraph2.setDefaultUnit(kony.flex.DP);
        var TPWBarGraph2 = new kony.ui.CustomWidget(extendConfig({
            "id": "TPWBarGraph2",
            "isVisible": true,
            "left": "0dp",
            "top": "0dp",
            "width": "100%",
            "height": "100%",
            "zIndex": 1,
            "clipBounds": false
        }, controller.args[0], "TPWBarGraph2"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "TPWBarGraph2"), extendConfig({
            "widgetName": "barGraph2",
            "canvasHeight": "300",
            "canvasWidth": "400",
            "chartObj": "",
            "datasetsArray": "[{\"label\":\"Personal Loans\",\"backgroundColor\":\"rgba(40, 2, 207, 0.1)\",\"pointBackgroundColor\" : \"#FF9800\",\"pointBorderColor\": \"#ffffff\",\"data\":[10,11,9,8,7,6,10]},{\"label\":\"Mortgage Loans\",\"backgroundColor\":\"rgba(96, 82, 201, 0.1)\",\"pointBackgroundColor\" : \"#2C8631\",\"pointBorderColor\": \"#ffffff\",\"data\":[8,4,4,5,9,6,4]}]",
            "lineAreaFill": false,
            "updateBars": false,
            "xlabelsArray": " [\"Day 1\", \"Day 2\", \"Day 3\", \"Day 4\", \"Day 5\", \"Day 6\", \"Day 7\"]",
            "yBeginsAtZero": false,
            "yStepSize": "",
            "ySuggestedMin": ""
        }, controller.args[2], "TPWBarGraph2"));
        barGraph2.add(TPWBarGraph2);
        barGraph2.compInstData = {}
        return barGraph2;
    }
})