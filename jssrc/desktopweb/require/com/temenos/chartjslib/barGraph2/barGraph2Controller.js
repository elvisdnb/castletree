define("com/temenos/chartjslib/barGraph2/userbarGraph2Controller", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {},
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {},
        updateGraph: function() {
            kony.print("update graph function called on component, will this trigger even to tpw");
            //Please set valid  properties before calling this property update
            //e.g. Do not hide graph on initial rendering
            // var mydatasets = [{"label":"lbl1","backgroundColor":"#FF9800","data":[10,11,9]},{"label":"lbl2","backgroundColor":"#2C8631","data":[8,4,5]}]
            // var xlabels = ["a", "b", "c"]
            this.view.TPWBarGraph2.updateBars = true;
        }
    };
});
define("com/temenos/chartjslib/barGraph2/barGraph2ControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/temenos/chartjslib/barGraph2/barGraph2Controller", ["com/temenos/chartjslib/barGraph2/userbarGraph2Controller", "com/temenos/chartjslib/barGraph2/barGraph2ControllerActions"], function() {
    var controller = require("com/temenos/chartjslib/barGraph2/userbarGraph2Controller");
    var actions = require("com/temenos/chartjslib/barGraph2/barGraph2ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "canvasWidth", function(val) {
            this.view.TPWBarGraph2.canvasWidth = val;
        });
        defineGetter(this, "canvasWidth", function() {
            return this.view.TPWBarGraph2.canvasWidth;
        });
        defineSetter(this, "ySuggestedMin", function(val) {
            this.view.TPWBarGraph2.ySuggestedMin = val;
        });
        defineGetter(this, "ySuggestedMin", function() {
            return this.view.TPWBarGraph2.ySuggestedMin;
        });
        defineSetter(this, "canvasHeight", function(val) {
            this.view.TPWBarGraph2.canvasHeight = val;
        });
        defineGetter(this, "canvasHeight", function() {
            return this.view.TPWBarGraph2.canvasHeight;
        });
        defineSetter(this, "xlabelsArray", function(val) {
            this.view.TPWBarGraph2.xlabelsArray = val;
        });
        defineGetter(this, "xlabelsArray", function() {
            return this.view.TPWBarGraph2.xlabelsArray;
        });
        defineSetter(this, "datasetsArray", function(val) {
            this.view.TPWBarGraph2.datasetsArray = val;
        });
        defineGetter(this, "datasetsArray", function() {
            return this.view.TPWBarGraph2.datasetsArray;
        });
        defineSetter(this, "lineAreaFill", function(val) {
            this.view.TPWBarGraph2.lineAreaFill = val;
        });
        defineGetter(this, "lineAreaFill", function() {
            return this.view.TPWBarGraph2.lineAreaFill;
        });
        defineSetter(this, "yBeginsAtZero", function(val) {
            this.view.TPWBarGraph2.yBeginsAtZero = val;
        });
        defineGetter(this, "yBeginsAtZero", function() {
            return this.view.TPWBarGraph2.yBeginsAtZero;
        });
        defineSetter(this, "yStepSize", function(val) {
            this.view.TPWBarGraph2.yStepSize = val;
        });
        defineGetter(this, "yStepSize", function() {
            return this.view.TPWBarGraph2.yStepSize;
        });
        defineSetter(this, "ySuggestedMax", function(val) {
            this.view.TPWBarGraph2.ySuggestedMax = val;
        });
        defineGetter(this, "ySuggestedMax", function() {
            return this.view.TPWBarGraph2.ySuggestedMax;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});
