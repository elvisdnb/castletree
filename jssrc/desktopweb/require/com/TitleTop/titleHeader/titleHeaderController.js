define("com/TitleTop/titleHeader/usertitleHeaderController", function() {
    return {};
});
define("com/TitleTop/titleHeader/titleHeaderControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/TitleTop/titleHeader/titleHeaderController", ["com/TitleTop/titleHeader/usertitleHeaderController", "com/TitleTop/titleHeader/titleHeaderControllerActions"], function() {
    var controller = require("com/TitleTop/titleHeader/usertitleHeaderController");
    var actions = require("com/TitleTop/titleHeader/titleHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
