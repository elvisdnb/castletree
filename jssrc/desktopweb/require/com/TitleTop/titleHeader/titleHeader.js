define(function() {
    return function(controller) {
        var titleHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "125dp",
            "id": "titleHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "topNameBlock",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "titleHeader"), extendConfig({}, controller.args[1], "titleHeader"), extendConfig({}, controller.args[2], "titleHeader"));
        titleHeader.setDefaultUnit(kony.flex.DP);
        var flxTitleHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTitleHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "32dp",
            "width": "90%"
        }, controller.args[0], "flxTitleHeader"), extendConfig({}, controller.args[1], "flxTitleHeader"), extendConfig({}, controller.args[2], "flxTitleHeader"));
        flxTitleHeader.setDefaultUnit(kony.flex.DP);
        var imgPrevArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgPrevArrow",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "bread_arrow_1.png",
            "top": "0",
            "width": "8dp"
        }, controller.args[0], "imgPrevArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPrevArrow"), extendConfig({}, controller.args[2], "imgPrevArrow"));
        var lblDashboard = new kony.ui.Label(extendConfig({
            "id": "lblDashboard",
            "isVisible": true,
            "left": "3dp",
            "skin": "defLabel2",
            "text": "Dashboard /",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblDashboard"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDashboard"), extendConfig({}, controller.args[2], "lblDashboard"));
        var lblCustId = new kony.ui.Label(extendConfig({
            "id": "lblCustId",
            "isVisible": true,
            "left": 3,
            "skin": "defLabel11",
            "text": "140597-7199",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustId"), extendConfig({}, controller.args[2], "lblCustId"));
        flxTitleHeader.add(imgPrevArrow, lblDashboard, lblCustId);
        var lblCustName = new kony.ui.Label(extendConfig({
            "id": "lblCustName",
            "isVisible": true,
            "left": "0dp",
            "skin": "defLabel3",
            "text": "James May",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustName"), extendConfig({}, controller.args[2], "lblCustName"));
        titleHeader.add(flxTitleHeader, lblCustName);
        return titleHeader;
    }
})