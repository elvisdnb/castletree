define("com/search/usersearchController", function() {
    return {
        storeData: {},
        globalVariables: function() {
            rowHeight = 120;
            this.loadAddress();
            mData = this.view.segSearch.seg1.data;
            this.view.segSearch.isVisible = false;
        },
        prev_index: -1,
        breakpointChange: function() {
            var currBreakpoint = kony.application.getCurrentBreakpoint();
            if (currBreakpoint === 640) {
                var data = this.view.segSearch.seg1.data;
                // console.log("segment "+JSON.stringify(data));
                if (data === undefined) {
                    return;
                }
                data.map(function(e) {
                    e[0].template = "flxDownK";
                });
                this.view.segSearch.seg1.setData(data);
            } else {
                var data1 = this.view.segSearch.seg1.data;
                // console.log("segment2 "+JSON.stringify(data1));
                if (data1 === undefined) {
                    return;
                }
                data1.map(function(e) {
                    e[0].template = "flxDown";
                });
                this.view.segSearch.seg1.setData(data1);
            }
        },
        loadAddress: function() {
            this.view.segSearch.seg1.widgetDataMap = {
                lblAddress: "lblAddress",
                lblShelves: "lblShelves",
                lblTitle: "lblTitle",
                imgLocation: "imgLocation",
                flxDown: "flxDown",
                flxDownK: "flxDownK",
                flxRow: "flxRow",
                flxParent: "flxParent",
                flxProgress: "flxProgress",
                flxAnimate: "flxAnimate",
                flxLine: "flxLine"
            };
            //     data = [
            //       [{lblShelves:"Using Transact",imgLocation:"blue_downarrow.png",flxAnimate:{width:"40%"},template:"flxDown"},
            //        [{lblAddress:{text: "Dashboard", skin: "sknUnselected"},flxLine:{skin: "sknVerticalLine", isVisible: true},template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Loan Origination", skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Account Origination",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Payment Holiday",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Disbursement",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Change Interest",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Repayment",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Increase Commitment",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Loan Payoff",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Postings Restrictions",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Funds Blocking \\ Unblocing",skin: "sknUnselected"},flxLine:"",template:"flxParent",flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Add \\ Remove Owner",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Change Product",skin: "sknUnselected"},flxLine:"",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:{text: "Make a Payment",skin: "sknUnselected"},flxLine:"",template:"flxParent",flxRow:{height:"0dp"}},
            //         //{lblAddress:{text: "Change OD Limit",skin: "sknUnselected"},flxLine:"",template:"flxParent",flxRow:{height:"0dp"}}
            //        ]],
            //       [{lblShelves:"Managing Your Account",imgLocation:"blue_downarrow.png",flxAnimate:{width:"70%"},template:"flxDown"},
            //        [{lblAddress:{text: "Change OD Limit",skin: "sknUnselected"},flxLine:"",template:"flxParent",flxRow:{height:"0dp"}},
            //         {lblAddress:"80 6th St. Dallas 204-333-4444",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:"7566 Cactus Court Belton 204-333-4444",template:"flxParent", flxRow:{height:"0dp"}}
            //        ]],
            //       [{lblShelves:"Safety and security",imgLocation:"blue_downarrow.png",lblLocationCount:"3",imgPackage:"icons_box.png",lblPackages:"10",lblPercentage:"50%",imgLeft:"icons_arrow_01.png",flxProgress:{height:"4dp"},flxAnimate:{width:"50%"},template:"flxDown",lblTest:"lblTest"},
            //        [{lblAddress:"9560 Pumpkin Hill Ave. San Antonio 204-333-4444",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:"19909 120TH AVE NE STELLA EXPRESS 204-333-4444",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:"8624 Lake Forest Ave. Houston 204-333-4444",template:"flxParent", flxRow:{height:"0dp"}}
            //        ]],
            //       [{lblShelves:"Rules and policies",imgLocation:"blue_downarrow.png",flxAnimate:{width:"30%"},template:"flxDown"},
            //        [{lblAddress:"7939 Helen Rd. Odessa 204-333-4444",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:"7566 Cactus Court Belton 204-555-1212",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:"8429 West Manor Station Ave. Houston 204-555-1212",template:"flxParent", flxRow:{height:"0dp"}},
            //         {lblAddress:"19909 120TH AVE NE SOLO ENTERPRISES 204-555-1212",template:"flxParent", flxRow:{height:"0dp"}}
            //        ]],
            //     ];
            this.view.segSearch.seg1.setData(changeIntrest);
        },
        onClick: function() {
            var section = this.view.segSearch.seg1.selectedIndices[0][0];
            // console.log("section "+JSON.stringify(section));
            if (this.prev_index != -1) {
                var data = this.view.segSearch.seg1.data[this.prev_index];
                var count = data[1].length;
                var rowsToAnim = [];
                for (i = 0; i < count; i++) {
                    rowsToAnim.push({
                        rowIndex: i,
                        sectionIndex: this.prev_index
                    });
                }
                data[0].imgLocation = "blue_downarrow.png";
                this.view.segSearch.seg1.setSectionAt(data, this.prev_index);
                var animationDef1 = {
                    "100": {
                        "stepConfig": {
                            "timingFunction": kony.anim.EASE
                        },
                        "height": "0dp"
                    }
                };
                var animationConfig1 = {
                    "delay": 0,
                    "iterationCount": 1,
                    "fillMode": kony.anim.FILL_MODE_FORWARDS,
                    "duration": 0.1
                };
                var animationDefObject1 = kony.ui.createAnimation(animationDef1);
                this.view.segSearch.seg1.animateRows({
                    rows: rowsToAnim,
                    widgets: ["flxRow"],
                    animation: {
                        definition: animationDefObject1,
                        config: animationConfig1
                    }
                });
                console.log("rowsToAnim " + JSON.stringify(rowsToAnim));
                console.log("this.prev_index " + this.prev_index);
            }
            if (section == this.prev_index) {
                this.prev_index = -1;
                return;
            }
            var data1 = this.view.segSearch.seg1.data[section];
            const search = "Change OD Limit";
            data1[1].forEach(value => {
                if (value.lblAddress.text === search) {
                    value.lblAddress.skin = "sknSelectedItem";
                }
            });
            // console.log(JSON.stringify(data1[1]));
            data1[0].imgLocation = "blue_uparrow.png";
            this.view.segSearch.seg1.setSectionAt(data1, section);
            var count1 = data1[1].length;
            var rowsToAnim1 = [];
            for (i = 0; i < count1; i++) {
                rowsToAnim1.push({
                    rowIndex: i,
                    sectionIndex: section
                });
            }
            var animationDef = {
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "height": rowHeight + "dp" //kony.flex.USE_PREFERED_SIZE // rowHeight+"dp"
                }
            };
            var animationConfig = {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.1
            };
            var animationDefObject = kony.ui.createAnimation(animationDef);
            this.view.segSearch.seg1.animateRows({
                rows: rowsToAnim1,
                widgets: ["flxRow"],
                animation: {
                    definition: animationDefObject,
                    config: animationConfig
                }
            });
            this.prev_index = section;
            console.log("rowsToAnim1 " + JSON.stringify(rowsToAnim1));
        },
        onClickRow: function() {
            var row = this.view.segSearch.seg1.selectedRowIndex[1];
            var section = this.view.segSearch.seg1.selectedRowIndex[0];
            var data = this.view.segSearch.seg1.data[section][1][row];
            // this.storeData.lblAddress = data;
            //     var ntf = new kony.mvc.Navigation("frmMap");
            //     ntf.navigate(data);
        },
        getSearchResult: function(mData, searchValue) {
            var i, j;
            var length = mData.length;
            var sData = [];
            for (i = 0; i < length; i++) {
                // console.log(mData[i][1][0].lblAddress.text);
                //  console.log(mData[i][1][0].lblAddress);
                if (mData[i] !== null || mData[i] !== undefined) {
                    var txt1 = "";
                    var txt2 = "";
                    if (mData[i][1][0].lblAddress !== null && mData[i][1][0].lblAddress !== undefined) {
                        txt1 = mData[i][1][0].lblAddress;
                        txt1 = txt1.toLowerCase();
                    }
                    if (mData[i][0].lblShelves !== null && mData[i][0].lblShelves !== undefined) {
                        txt2 = mData[i][0].lblShelves;
                        txt2 = txt2.toLowerCase();
                    }
                    var pattern = searchValue.toLowerCase();
                    if (txt1.search(pattern.trim()) !== -1 || txt2.search(pattern.trim()) !== -1) {
                        sData.push(mData[i]);
                        var dataLength = sData.length - 1;
                    }
                }
            }
            return sData;
        },
        onTxtChange: function(searchValue) {
            //   alert(searchValue);
            var search = searchValue;
            if (search !== "") {
                this.view.segSearch.isVisible = true;
                var sData = this.getSearchResult(mData, search);
                console.log(sData);
                this.view.segSearch.seg1.setData(sData);
            } else {
                this.view.segSearch.isVisible = false;
            }
        }
    };
});
define("com/search/searchControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexScrollContainer_d08d523552c74724ba7e92a79b61ecad: function AS_FlexScrollContainer_d08d523552c74724ba7e92a79b61ecad(eventobject) {
        var self = this;
        return self.globalVariables.call(this);
    },
    AS_TextField_d18b94dc699844039f721d844eb4c87b: function AS_TextField_d18b94dc699844039f721d844eb4c87b(eventobject, changedtext) {
        var self = this;
        mData = changeIntrest.concat(changeIntrest1);
    },
    AS_TextField_e136b0b1c7a747068786f4820e45b8cf: function AS_TextField_e136b0b1c7a747068786f4820e45b8cf(eventobject) {
        var self = this;
        return self.onTxtChange.call(this);
    }
});
define("com/search/searchController", ["com/search/usersearchController", "com/search/searchControllerActions"], function() {
    var controller = require("com/search/usersearchController");
    var actions = require("com/search/searchControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
