define(function() {
    return function(controller) {
        var search = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "isMaster": true,
            "height": "100%",
            "horizontalScrollIndicator": true,
            "id": "search",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1200, 1920],
            "pagingEnabled": false,
            "postShow": controller.AS_FlexScrollContainer_d08d523552c74724ba7e92a79b61ecad,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "search"), extendConfig({}, controller.args[1], "search"), extendConfig({}, controller.args[2], "search"));
        search.setDefaultUnit(kony.flex.DP);
        var flxSeg = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "height": "100%",
            "id": "flxSeg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSeg"), extendConfig({}, controller.args[1], "flxSeg"), extendConfig({}, controller.args[2], "flxSeg"));
        flxSeg.setDefaultUnit(kony.flex.DP);
        var segSearch = new com.konymp.segSearch(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "segSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "seg1": {
                    "data": [
                        [{
                                "imgLocation": "icons_map_2.png",
                                "lblDown": "M",
                                "lblShelves": "Kony "
                            },
                            [{
                                "lblAddress": "RichText",
                                "lblAddress1": "19909 120TH AVE NE",
                                "lblLine": "Label"
                            }, {
                                "lblAddress": "RichText",
                                "lblAddress1": "19909 120TH AVE NE",
                                "lblLine": "Label"
                            }]
                        ],
                        [{
                                "imgLocation": "icons_map_2.png",
                                "lblDown": "M",
                                "lblShelves": "Kony "
                            },
                            [{
                                "lblAddress": "RichText",
                                "lblAddress1": "19909 120TH AVE NE",
                                "lblLine": "Label"
                            }, {
                                "lblAddress": "RichText",
                                "lblAddress1": "19909 120TH AVE NE",
                                "lblLine": "Label"
                            }]
                        ]
                    ]
                }
            }
        }, controller.args[0], "segSearch"), extendConfig({
            "overrides": {}
        }, controller.args[1], "segSearch"), extendConfig({
            "overrides": {}
        }, controller.args[2], "segSearch"));
        flxSeg.add(segSearch);
        var txtSearch = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtSearch",
            "isVisible": false,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "1158dp",
            "placeholder": "Placeholder",
            "secureTextEntry": false,
            "skin": "defTextBoxNormal",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "300dp",
            "zIndex": 1
        }, controller.args[0], "txtSearch"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearch"), extendConfig({
            "autoCorrect": false,
            "onBeginEditing": controller.AS_TextField_d18b94dc699844039f721d844eb4c87b,
            "onKeyUp": controller.AS_TextField_e136b0b1c7a747068786f4820e45b8cf,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtSearch"));
        search.add(flxSeg, txtSearch);
        search.breakpointResetData = {};
        search.breakpointData = {
            maxBreakpointWidth: 1920,
            "640": {
                "search": {
                    "segmentProps": [],
                    "isSrcCompTopFlex": true
                },
                "flxSeg": {
                    "height": {
                        "type": "string",
                        "value": "78%"
                    },
                    "top": {
                        "type": "string",
                        "value": "15%"
                    },
                    "segmentProps": []
                }
            },
            "1920": {
                "flxSeg": {
                    "height": {
                        "type": "string",
                        "value": "80%"
                    },
                    "top": {
                        "type": "string",
                        "value": "10%"
                    },
                    "width": {
                        "type": "string",
                        "value": "80%"
                    },
                    "segmentProps": []
                },
                "txtSearch": {
                    "focusSkin": "CopydefTextBoxNormal0g285aa6386c64c",
                    "padding": [1, 0, 0, 0],
                    "placeholderSkin": "CopydefTextBoxNormal0g285aa6386c64c",
                    "skin": "CopydefTextBoxNormal0g285aa6386c64c",
                    "segmentProps": []
                }
            }
        }
        search.compInstData = {
            "segSearch.seg1": {
                "data": [
                    [{
                            "imgLocation": "icons_map_2.png",
                            "lblDown": "M",
                            "lblShelves": "Kony "
                        },
                        [{
                            "lblAddress": "RichText",
                            "lblAddress1": "19909 120TH AVE NE",
                            "lblLine": "Label"
                        }, {
                            "lblAddress": "RichText",
                            "lblAddress1": "19909 120TH AVE NE",
                            "lblLine": "Label"
                        }]
                    ],
                    [{
                            "imgLocation": "icons_map_2.png",
                            "lblDown": "M",
                            "lblShelves": "Kony "
                        },
                        [{
                            "lblAddress": "RichText",
                            "lblAddress1": "19909 120TH AVE NE",
                            "lblLine": "Label"
                        }, {
                            "lblAddress": "RichText",
                            "lblAddress1": "19909 120TH AVE NE",
                            "lblLine": "Label"
                        }]
                    ]
                ]
            }
        }
        return search;
    }
})