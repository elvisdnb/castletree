define("com/konymp/helpCenter/userhelpCenterController", function() {
    return {};
});
define("com/konymp/helpCenter/helpCenterControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_a400d28c4fb044a8b46444a580abf4d0: function AS_Button_a400d28c4fb044a8b46444a580abf4d0(eventobject) {
        var self = this;
        this.view.isVisible = false;
    },
    AS_Button_g4d6d6516cfe49bb9b7fb433cae63686: function AS_Button_g4d6d6516cfe49bb9b7fb433cae63686(eventobject) {
        var self = this;
        this.view.isVisible = false;
    }
});
define("com/konymp/helpCenter/helpCenterController", ["com/konymp/helpCenter/userhelpCenterController", "com/konymp/helpCenter/helpCenterControllerActions"], function() {
    var controller = require("com/konymp/helpCenter/userhelpCenterController");
    var actions = require("com/konymp/helpCenter/helpCenterControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
