define(function() {
    return function(controller) {
        var helpCenter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "helpCenter",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0a65c3cacd3214e",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "helpCenter"), extendConfig({}, controller.args[1], "helpCenter"), extendConfig({}, controller.args[2], "helpCenter"));
        helpCenter.setDefaultUnit(kony.flex.DP);
        var flxMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": false,
            "height": "545dp",
            "id": "flxMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopyslFbox0ea3e988cb93148",
            "width": "900dp",
            "zIndex": 1
        }, controller.args[0], "flxMain"), extendConfig({}, controller.args[1], "flxMain"), extendConfig({}, controller.args[2], "flxMain"));
        flxMain.setDefaultUnit(kony.flex.DP);
        var flxTop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "9%",
            "id": "flxTop",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "100%"
        }, controller.args[0], "flxTop"), extendConfig({}, controller.args[1], "flxTop"), extendConfig({}, controller.args[2], "flxTop"));
        flxTop.setDefaultUnit(kony.flex.DP);
        var flxBreadcrum = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxBreadcrum",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "38%",
            "zIndex": 1
        }, controller.args[0], "flxBreadcrum"), extendConfig({}, controller.args[1], "flxBreadcrum"), extendConfig({}, controller.args[2], "flxBreadcrum"));
        flxBreadcrum.setDefaultUnit(kony.flex.DP);
        flxBreadcrum.add();
        var flxBck = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxBck",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%"
        }, controller.args[0], "flxBck"), extendConfig({}, controller.args[1], "flxBck"), extendConfig({}, controller.args[2], "flxBck"));
        flxBck.setDefaultUnit(kony.flex.DP);
        var flxBack = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxBack",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flxBack"), extendConfig({}, controller.args[1], "flxBack"), extendConfig({}, controller.args[2], "flxBack"));
        flxBack.setDefaultUnit(kony.flex.DP);
        var cusBackIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "cusBackIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyiconFontPurple0a2f41c3ab23d4d",
            "text": "O",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "cusBackIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "cusBackIcon"), extendConfig({}, controller.args[2], "cusBackIcon"));
        flxBack.add(cusBackIcon);
        var rtxSelected = new kony.ui.RichText(extendConfig({
            "id": "rtxSelected",
            "isVisible": true,
            "left": "28%",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0b5276fc8c28b4c",
            "text": "<p><font color=\"#000000\"><u>Explorer</u></font></p>\n",
            "top": "30%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxSelected"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxSelected"), extendConfig({}, controller.args[2], "rtxSelected"));
        var btnExplorer = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0fde86432622249",
            "height": "100%",
            "id": "btnExplorer",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_g4d6d6516cfe49bb9b7fb433cae63686,
            "skin": "CopydefBtnNormal0fde86432622249",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnExplorer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnExplorer"), extendConfig({}, controller.args[2], "btnExplorer"));
        flxBck.add(flxBack, rtxSelected, btnExplorer);
        var flxClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "75%",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "flxClose"), extendConfig({}, controller.args[1], "flxClose"), extendConfig({}, controller.args[2], "flxClose"));
        flxClose.setDefaultUnit(kony.flex.DP);
        var lblCLose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblCLose",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0ef35acea602344",
            "text": "L",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCLose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCLose"), extendConfig({}, controller.args[2], "lblCLose"));
        var Button0jc4a46cb137845 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0c429e5fcb8e04b",
            "height": "100%",
            "id": "Button0jc4a46cb137845",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_a400d28c4fb044a8b46444a580abf4d0,
            "skin": "CopydefBtnNormal0c429e5fcb8e04b",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "Button0jc4a46cb137845"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "Button0jc4a46cb137845"), extendConfig({}, controller.args[2], "Button0jc4a46cb137845"));
        flxClose.add(lblCLose, Button0jc4a46cb137845);
        var lblBreadBoardSplashValue1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBreadBoardSplashValue1",
            "isVisible": true,
            "left": "11%",
            "skin": "CopydefLabel0ba3c86f1daeb42",
            "text": "/",
            "top": "0dp",
            "width": "8dp",
            "zIndex": 1
        }, controller.args[0], "lblBreadBoardSplashValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBreadBoardSplashValue1"), extendConfig({}, controller.args[2], "lblBreadBoardSplashValue1"));
        var lblPath = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPath",
            "isVisible": true,
            "left": "13%",
            "skin": "CopydefLabel0ba3c86f1daeb42",
            "text": "Lending",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPath"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPath"), extendConfig({}, controller.args[2], "lblPath"));
        flxTop.add(flxBreadcrum, flxBck, flxClose, lblBreadBoardSplashValue1, lblPath);
        var flx1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80%",
            "id": "flx1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20%",
            "width": "100%"
        }, controller.args[0], "flx1"), extendConfig({}, controller.args[1], "flx1"), extendConfig({}, controller.args[2], "flx1"));
        flx1.setDefaultUnit(kony.flex.DP);
        var lblQues = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblQues",
            "isVisible": true,
            "skin": "CopydefLabel0cb3cb909948546",
            "text": "What would you like to explore in Lending?",
            "top": "8%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblQues"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblQues"), extendConfig({}, controller.args[2], "lblQues"));
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "9%",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35%",
            "width": "53%"
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({}, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lbl1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "lbl1",
            "isVisible": true,
            "left": "2%",
            "skin": "CopysknStatusNew1",
            "text": "Disburse",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl1"), extendConfig({}, controller.args[2], "lbl1"));
        var lbl2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "lbl2",
            "isVisible": true,
            "left": "5%",
            "skin": "CopysknStatusNew1",
            "text": "Increase Commitment",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl2"), extendConfig({}, controller.args[2], "lbl2"));
        var lbl3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "lbl3",
            "isVisible": true,
            "left": "5%",
            "skin": "CopysknStatusNew1",
            "text": "Payment Holiday",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl3"), extendConfig({}, controller.args[2], "lbl3"));
        flxOptions.add(lbl1, lbl2, lbl3);
        var flxOptions2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "9%",
            "id": "flxOptions2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "47%",
            "width": "53%"
        }, controller.args[0], "flxOptions2"), extendConfig({}, controller.args[1], "flxOptions2"), extendConfig({}, controller.args[2], "flxOptions2"));
        flxOptions2.setDefaultUnit(kony.flex.DP);
        var lbl4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "lbl4",
            "isVisible": true,
            "left": "2%",
            "skin": "CopysknStatusNew1",
            "text": "Repayment",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl4"), extendConfig({}, controller.args[2], "lbl4"));
        var lbl5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "lbl5",
            "isVisible": true,
            "left": "5%",
            "skin": "CopysknStatusNew1",
            "text": "Change Interest",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl5"), extendConfig({}, controller.args[2], "lbl5"));
        var lbl6 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "30dp",
            "id": "lbl6",
            "isVisible": true,
            "left": "5%",
            "skin": "CopysknStatusNew1",
            "text": "Payoff",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl6"), extendConfig({}, controller.args[2], "lbl6"));
        flxOptions2.add(lbl4, lbl5, lbl6);
        flx1.add(lblQues, flxOptions, flxOptions2);
        var flx2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85%",
            "id": "flx2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flx2"), extendConfig({}, controller.args[1], "flx2"), extendConfig({}, controller.args[2], "flx2"));
        flx2.setDefaultUnit(kony.flex.DP);
        var lblSelected = new kony.ui.Label(extendConfig({
            "id": "lblSelected",
            "isVisible": true,
            "left": "18%",
            "skin": "CopydefLabel0h0f3d1d7c2414b",
            "text": "Disbursement",
            "top": "10%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelected"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected"), extendConfig({}, controller.args[2], "lblSelected"));
        var lblDesc = new kony.ui.Label(extendConfig({
            "id": "lblDesc",
            "isVisible": true,
            "left": "18%",
            "skin": "CopydefLabel0e27da0af85bb42",
            "text": "After a decision has been made on a loan request, Temenos retail lending allows you to disburse loan amounts in full or partial payments",
            "top": "100dp",
            "width": "65%",
            "zIndex": 1
        }, controller.args[0], "lblDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesc"), extendConfig({}, controller.args[2], "lblDesc"));
        var flxVideo = new kony.ui.FlexContainer(extendConfig({
            "bottom": "80dp",
            "clipBounds": false,
            "height": "220dp",
            "id": "flxVideo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "18%",
            "isModalContainer": false,
            "skin": "CopyslFbox0c75499a9810f4b",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "flxVideo"), extendConfig({}, controller.args[1], "flxVideo"), extendConfig({}, controller.args[2], "flxVideo"));
        flxVideo.setDefaultUnit(kony.flex.DP);
        var Image0ca71114746d849 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "45dp",
            "id": "Image0ca71114746d849",
            "isVisible": true,
            "left": "11%",
            "skin": "slImage",
            "src": "imagedrag.png",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "Image0ca71114746d849"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "Image0ca71114746d849"), extendConfig({}, controller.args[2], "Image0ca71114746d849"));
        var lblThumbnailDesc = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblThumbnailDesc",
            "isVisible": true,
            "left": "22%",
            "skin": "CopydefLabel0hcd0e64feca04f",
            "text": "Check out the video on how to disburse?",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblThumbnailDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblThumbnailDesc"), extendConfig({}, controller.args[2], "lblThumbnailDesc"));
        flxVideo.add(Image0ca71114746d849, lblThumbnailDesc);
        flx2.add(lblSelected, lblDesc, flxVideo);
        var flx3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "91%",
            "id": "flx3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "9%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flx3"), extendConfig({}, controller.args[1], "flx3"), extendConfig({}, controller.args[2], "flx3"));
        flx3.setDefaultUnit(kony.flex.DP);
        var brw = new kony.ui.Browser(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "detectTelNumber": true,
            "enableZoom": false,
            "height": "100%",
            "htmlString": "<div style=\"position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;\">\n<iframe src=\"https://player.stornaway.io/embed/4c4499ba\" frameborder=\"0\" allowfullscreen style=\"width: 100%; height: 100%; position: absolute; top: 0; left: 0;\"></iframe>\n</div>",
            "id": "brw",
            "isVisible": true,
            "left": "0dp",
            "setAsContent": false,
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "brw"), extendConfig({}, controller.args[1], "brw"), extendConfig({}, controller.args[2], "brw"));
        flx3.add(brw);
        flxMain.add(flxTop, flx1, flx2, flx3);
        helpCenter.add(flxMain);
        helpCenter.compInstData = {}
        return helpCenter;
    }
})