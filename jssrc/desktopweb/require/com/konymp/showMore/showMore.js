define(function() {
    return function(controller) {
        var showMore = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "showMore",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0ea98703a386244",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "showMore"), extendConfig({}, controller.args[1], "showMore"), extendConfig({}, controller.args[2], "showMore"));
        showMore.setDefaultUnit(kony.flex.DP);
        var segShowMore = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [
                [{
                        "imgLocation": "blue_downarrow.png",
                        "lblDown": "J",
                        "lblShelves": "Kony ",
                        "lblTitle": "Label"
                    },
                    [{
                        "lblAddress": "Dashboard",
                        "lblLine": "label",
                        "rtxAddress": "RichText"
                    }]
                ]
            ],
            "groupCells": false,
            "height": "65%",
            "id": "segShowMore",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxParent",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "flxDown",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "FlexGroup0d7f2ca3295ba45": "FlexGroup0d7f2ca3295ba45",
                "flxDown": "flxDown",
                "flxLine": "flxLine",
                "flxParent": "flxParent",
                "flxRow": "flxRow",
                "imgLocation": "imgLocation",
                "lblAddress": "lblAddress",
                "lblDown": "lblDown",
                "lblLine": "lblLine",
                "lblShelves": "lblShelves",
                "lblTitle": "lblTitle",
                "rtxAddress": "rtxAddress"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segShowMore"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segShowMore"), extendConfig({}, controller.args[2], "segShowMore"));
        showMore.add(segShowMore);
        showMore.compInstData = {}
        return showMore;
    }
})