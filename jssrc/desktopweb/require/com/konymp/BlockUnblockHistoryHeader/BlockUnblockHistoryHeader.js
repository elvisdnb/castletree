define(function() {
    return function(controller) {
        var BlockUnblockHistoryHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "80dp",
            "id": "BlockUnblockHistoryHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxF5F2FDbgLightPurpleBackground",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, controller.args[0], "BlockUnblockHistoryHeader"), extendConfig({}, controller.args[1], "BlockUnblockHistoryHeader"), extendConfig({}, controller.args[2], "BlockUnblockHistoryHeader"));
        BlockUnblockHistoryHeader.setDefaultUnit(kony.flex.DP);
        var lblRefNoTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRefNoTitle",
            "isVisible": true,
            "left": "25dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Ref No",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRefNoTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRefNoTitle"), extendConfig({}, controller.args[2], "lblRefNoTitle"));
        var lblStartDateTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStartDateTitle",
            "isVisible": true,
            "left": "101dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Start Date",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStartDateTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateTitle"), extendConfig({}, controller.args[2], "lblStartDateTitle"));
        var lblToDateTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblToDateTitle",
            "isVisible": true,
            "left": "42dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "To Date",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblToDateTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToDateTitle"), extendConfig({}, controller.args[2], "lblToDateTitle"));
        var lblAmountTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAmountTitle",
            "isVisible": true,
            "left": "54dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Amount",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAmountTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountTitle"), extendConfig({}, controller.args[2], "lblAmountTitle"));
        var lblReasonTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblReasonTitle",
            "isVisible": true,
            "left": "63dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Reason",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblReasonTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblReasonTitle"), extendConfig({}, controller.args[2], "lblReasonTitle"));
        var lblEditTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblEditTitle",
            "isVisible": true,
            "left": "74dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Edit",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEditTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditTitle"), extendConfig({}, controller.args[2], "lblEditTitle"));
        var lblUnblockTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblUnblockTitle",
            "isVisible": true,
            "left": "29dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Unblock",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUnblockTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUnblockTitle"), extendConfig({}, controller.args[2], "lblUnblockTitle"));
        BlockUnblockHistoryHeader.add(lblRefNoTitle, lblStartDateTitle, lblToDateTitle, lblAmountTitle, lblReasonTitle, lblEditTitle, lblUnblockTitle);
        BlockUnblockHistoryHeader.compInstData = {}
        return BlockUnblockHistoryHeader;
    }
})