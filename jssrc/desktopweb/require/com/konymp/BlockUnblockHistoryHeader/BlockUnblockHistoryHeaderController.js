define("com/konymp/BlockUnblockHistoryHeader/userBlockUnblockHistoryHeaderController", function() {
    return {};
});
define("com/konymp/BlockUnblockHistoryHeader/BlockUnblockHistoryHeaderControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/BlockUnblockHistoryHeader/BlockUnblockHistoryHeaderController", ["com/konymp/BlockUnblockHistoryHeader/userBlockUnblockHistoryHeaderController", "com/konymp/BlockUnblockHistoryHeader/BlockUnblockHistoryHeaderControllerActions"], function() {
    var controller = require("com/konymp/BlockUnblockHistoryHeader/userBlockUnblockHistoryHeaderController");
    var actions = require("com/konymp/BlockUnblockHistoryHeader/BlockUnblockHistoryHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
