define(function() {
    return function(controller) {
        var help = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "help",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0f1d22019362442",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "help"), extendConfig({}, controller.args[1], "help"), extendConfig({}, controller.args[2], "help"));
        help.setDefaultUnit(kony.flex.DP);
        var flxMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": false,
            "height": "550dp",
            "id": "flxMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopyslFbox0d7bc1504c41e4b",
            "width": "900dp",
            "zIndex": 1
        }, controller.args[0], "flxMain"), extendConfig({}, controller.args[1], "flxMain"), extendConfig({}, controller.args[2], "flxMain"));
        flxMain.setDefaultUnit(kony.flex.DP);
        var FlexGroup0b337f90c7acb4e = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "32%",
            "id": "FlexGroup0b337f90c7acb4e",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2%",
            "width": "100%"
        }, controller.args[0], "FlexGroup0b337f90c7acb4e"), extendConfig({}, controller.args[1], "FlexGroup0b337f90c7acb4e"), extendConfig({}, controller.args[2], "FlexGroup0b337f90c7acb4e"));
        FlexGroup0b337f90c7acb4e.setDefaultUnit(kony.flex.DP);
        var flxTop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "75%",
            "id": "flxTop",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0ida8debfd3904f",
            "top": "3%",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "flxTop"), extendConfig({}, controller.args[1], "flxTop"), extendConfig({}, controller.args[2], "flxTop"));
        flxTop.setDefaultUnit(kony.flex.DP);
        var lblHi = new kony.ui.Label(extendConfig({
            "id": "lblHi",
            "isVisible": true,
            "right": "25.50%",
            "skin": "CopydefLabel0e3b00e014fc84d",
            "text": "Hi, how can we help?",
            "top": "24%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHi"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHi"), extendConfig({}, controller.args[2], "lblHi"));
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "22%",
            "clipBounds": true,
            "height": "26%",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "5%",
            "skin": "CopyslFbox0ba84d592371044",
            "width": "42%"
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var lstCategories = new kony.ui.ListBox(extendConfig({
            "focusSkin": "CopydefListBoxNormal0g0daf83bd3df48",
            "height": "100%",
            "id": "lstCategories",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "All Categories"],
                ["lb2", "Dictionary"],
                ["lb3", "FAQs"],
                ["Key323001737", "How to's"]
            ],
            "selectedKey": "lb1",
            "skin": "CopydefListBoxNormal0g0daf83bd3df48",
            "top": "0dp",
            "width": "34%",
            "zIndex": 1
        }, controller.args[0], "lstCategories"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstCategories"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstCategories"));
        var txtPartialText = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "CopysknTxtBoxNormal",
            "height": "40px",
            "id": "txtPartialText",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "35%",
            "placeholder": "Search by keywords, phrases",
            "secureTextEntry": false,
            "skin": "CopysknTxtBoxNormal",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "24dp",
            "width": "55%",
            "zIndex": 1
        }, controller.args[0], "txtPartialText"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtPartialText"), extendConfig({
            "autoCorrect": false,
            "onKeyUp": controller.AS_TextField_dbdfcb90acd04036b190f4b10a2b8783,
            "placeholderSkin": "CopyskntxtBoxPH"
        }, controller.args[2], "txtPartialText"));
        var lblSearchIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblSearchIcon",
            "isVisible": true,
            "right": "15dp",
            "skin": "CopyiconFontPurple",
            "text": "k",
            "top": "0",
            "width": "20dp"
        }, controller.args[0], "lblSearchIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchIcon"), extendConfig({}, controller.args[2], "lblSearchIcon"));
        var flxSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "80%",
            "id": "flxSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34.50%",
            "isModalContainer": false,
            "skin": "CopyslFbox0d1fc1b57d72a4b",
            "top": "0dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxSeparator"), extendConfig({}, controller.args[1], "flxSeparator"), extendConfig({}, controller.args[2], "flxSeparator"));
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        var clearSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "clearSearch",
            "isVisible": false,
            "right": "15dp",
            "skin": "CopydefLabel0b245adf35ddf43",
            "text": "L",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "clearSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "clearSearch"), extendConfig({}, controller.args[2], "clearSearch"));
        var clearTxtBxSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "clearTxtBxSearch",
            "isVisible": false,
            "right": "15dp",
            "skin": "CopydefLabel0fb6c63ef5cbb4a",
            "text": "L",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "clearTxtBxSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "clearTxtBxSearch"), extendConfig({}, controller.args[2], "clearTxtBxSearch"));
        var imgClearSearch = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgClearSearch",
            "isVisible": false,
            "right": "15dp",
            "skin": "slImage",
            "src": "cross_1_4.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgClearSearch"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClearSearch"), extendConfig({}, controller.args[2], "imgClearSearch"));
        var btnClearTxt = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0bbe222a8889b4d",
            "height": "20dp",
            "id": "btnClearTxt",
            "isVisible": false,
            "onClick": controller.AS_Button_bdd5065cfdef4fdfa7fbc7fc7a53bcbc,
            "right": "13dp",
            "skin": "CopydefBtnNormal0bbe222a8889b4d",
            "top": "15dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "btnClearTxt"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnClearTxt"), extendConfig({}, controller.args[2], "btnClearTxt"));
        flxSearch.add(lstCategories, txtPartialText, lblSearchIcon, flxSeparator, clearSearch, clearTxtBxSearch, imgClearSearch, btnClearTxt);
        var lblHelpBig = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "lblHelpBig",
            "isVisible": true,
            "left": "85dp",
            "skin": "CopydefLabel0h028d8bdb89e49",
            "text": "HELP",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHelpBig"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHelpBig"), extendConfig({}, controller.args[2], "lblHelpBig"));
        var flxHelpGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHelpGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "53%"
        }, controller.args[0], "flxHelpGroup"), extendConfig({}, controller.args[1], "flxHelpGroup"), extendConfig({}, controller.args[2], "flxHelpGroup"));
        flxHelpGroup.setDefaultUnit(kony.flex.DP);
        var lblHelp = new kony.ui.Label(extendConfig({
            "id": "lblHelp",
            "isVisible": true,
            "left": "203dp",
            "skin": "CopydefLabel0f0467e57c50a4c",
            "text": "HELP CENTER",
            "top": "25.50%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHelp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHelp"), extendConfig({}, controller.args[2], "lblHelp"));
        var flxBack = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24%",
            "clipBounds": true,
            "height": "18%",
            "id": "flxBack",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "203dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "55%",
            "zIndex": 1
        }, controller.args[0], "flxBack"), extendConfig({}, controller.args[1], "flxBack"), extendConfig({}, controller.args[2], "flxBack"));
        flxBack.setDefaultUnit(kony.flex.DP);
        var flxHelpHome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHelpHome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "11%"
        }, controller.args[0], "flxHelpHome"), extendConfig({}, controller.args[1], "flxHelpHome"), extendConfig({}, controller.args[2], "flxHelpHome"));
        flxHelpHome.setDefaultUnit(kony.flex.DP);
        var rtxSelected = new kony.ui.RichText(extendConfig({
            "centerY": "50%",
            "id": "rtxSelected",
            "isVisible": true,
            "left": "1dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0f50f9b4a5d174e",
            "text": "<p><font color=\"#ffffff\"><u>Help</u></font></p>\n",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxSelected"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxSelected"), extendConfig({}, controller.args[2], "rtxSelected"));
        var btnHomeHelp = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0g14882667ba340",
            "height": "100%",
            "id": "btnHomeHelp",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_fb2ffec1480e4a91ab402dbc345ce18a,
            "skin": "CopydefBtnNormal0g14882667ba340",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnHomeHelp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnHomeHelp"), extendConfig({}, controller.args[2], "btnHomeHelp"));
        flxHelpHome.add(rtxSelected, btnHomeHelp);
        var lblBreadBoardSplashValue = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblBreadBoardSplashValue",
            "isVisible": true,
            "left": "1%",
            "skin": "CopysknLbl0addadc73a94e45",
            "text": "/",
            "top": "0dp",
            "width": "6dp",
            "zIndex": 1
        }, controller.args[0], "lblBreadBoardSplashValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBreadBoardSplashValue"), extendConfig({}, controller.args[2], "lblBreadBoardSplashValue"));
        var flxLoanHome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxLoanHome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "29%"
        }, controller.args[0], "flxLoanHome"), extendConfig({}, controller.args[1], "flxLoanHome"), extendConfig({}, controller.args[2], "flxLoanHome"));
        flxLoanHome.setDefaultUnit(kony.flex.DP);
        var lblLoanServices = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0a214fb19867640",
            "height": "100%",
            "id": "lblLoanServices",
            "isVisible": true,
            "left": "1dp",
            "onClick": controller.AS_Button_e4d554bce5874df89a1425170d266b94,
            "skin": "CopydefBtnNormal0a214fb19867640",
            "text": "Loan Services",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanServices"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanServices"), extendConfig({}, controller.args[2], "lblLoanServices"));
        flxLoanHome.add(lblLoanServices);
        var lblBreadBoardSplashValue1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBreadBoardSplashValue1",
            "isVisible": true,
            "left": "2dp",
            "skin": "CopysknLbl0addadc73a94e45",
            "text": "/",
            "top": "0dp",
            "width": "6dp",
            "zIndex": 1
        }, controller.args[0], "lblBreadBoardSplashValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBreadBoardSplashValue1"), extendConfig({}, controller.args[2], "lblBreadBoardSplashValue1"));
        var lblPath = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPath",
            "isVisible": true,
            "left": "2dp",
            "skin": "CopydefLabel0b359685520464f",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPath"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPath"), extendConfig({}, controller.args[2], "lblPath"));
        flxBack.add(flxHelpHome, lblBreadBoardSplashValue, flxLoanHome, lblBreadBoardSplashValue1, lblPath);
        flxHelpGroup.add(lblHelp, flxBack);
        flxTop.add(lblHi, flxSearch, lblHelpBig, flxHelpGroup);
        var imgHelp = new kony.ui.Image2(extendConfig({
            "bottom": "0dp",
            "height": "160dp",
            "id": "imgHelp",
            "isVisible": true,
            "left": "35dp",
            "skin": "slImage",
            "src": "start2.png",
            "width": "190dp",
            "zIndex": 1
        }, controller.args[0], "imgHelp"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgHelp"), extendConfig({}, controller.args[2], "imgHelp"));
        var flxCloseHelp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18%",
            "id": "flxCloseHelp",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "15dp",
            "width": "4%",
            "zIndex": 1
        }, controller.args[0], "flxCloseHelp"), extendConfig({}, controller.args[1], "flxCloseHelp"), extendConfig({}, controller.args[2], "flxCloseHelp"));
        flxCloseHelp.setDefaultUnit(kony.flex.DP);
        var lblCLose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblCLose",
            "isVisible": true,
            "skin": "CopydefLabel0cf64ac2218ab49",
            "text": "L",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCLose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCLose"), extendConfig({}, controller.args[2], "lblCLose"));
        var btnCloseHelp = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0f4667b97f9be49",
            "height": "100%",
            "id": "btnCloseHelp",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_c7621bafc21e49edb500dc952504f498,
            "skin": "CopydefBtnNormal0f4667b97f9be49",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnCloseHelp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCloseHelp"), extendConfig({}, controller.args[2], "btnCloseHelp"));
        flxCloseHelp.add(lblCLose, btnCloseHelp);
        FlexGroup0b337f90c7acb4e.add(flxTop, imgHelp, flxCloseHelp);
        var flxServices = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "66%",
            "id": "flxServices",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "34%",
            "width": "100%"
        }, controller.args[0], "flxServices"), extendConfig({}, controller.args[1], "flxServices"), extendConfig({}, controller.args[2], "flxServices"));
        flxServices.setDefaultUnit(kony.flex.DP);
        var flxTypes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "150dp",
            "id": "flxTypes",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0%",
            "width": "100%"
        }, controller.args[0], "flxTypes"), extendConfig({}, controller.args[1], "flxTypes"), extendConfig({}, controller.args[2], "flxTypes"));
        flxTypes.setDefaultUnit(kony.flex.DP);
        var flxLoan = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "90dp",
            "id": "flxLoan",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12%",
            "isModalContainer": false,
            "skin": "sknFlxTypeUnselect",
            "top": "25%",
            "width": "185dp",
            "zIndex": 1
        }, controller.args[0], "flxLoan"), extendConfig({}, controller.args[1], "flxLoan"), extendConfig({}, controller.args[2], "flxLoan"));
        flxLoan.setDefaultUnit(kony.flex.DP);
        var lblLoan = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblLoan",
            "isVisible": true,
            "skin": "CopydefLabel0be32d5fe0b6b46",
            "text": "Loan Services",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblLoan"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoan"), extendConfig({}, controller.args[2], "lblLoan"));
        var btnLoan = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0i193d821e35c49",
            "height": "100%",
            "id": "btnLoan",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_a9960c3e36454247b50ad32401d3af17,
            "skin": "CopydefBtnNormal0i193d821e35c49",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnLoan"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLoan"), extendConfig({}, controller.args[2], "btnLoan"));
        flxLoan.add(lblLoan, btnLoan);
        var flxDeposits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "90dp",
            "id": "flxDeposits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_jab0b7944625463ab8d4906fc027d099,
            "skin": "sknFlxTypeUnselect",
            "top": "25%",
            "width": "185dp",
            "zIndex": 1
        }, controller.args[0], "flxDeposits"), extendConfig({}, controller.args[1], "flxDeposits"), extendConfig({}, controller.args[2], "flxDeposits"));
        flxDeposits.setDefaultUnit(kony.flex.DP);
        var lblDeposit = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDeposit",
            "isVisible": true,
            "skin": "CopydefLabel0be32d5fe0b6b46",
            "text": "Deposit Services",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDeposit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDeposit"), extendConfig({}, controller.args[2], "lblDeposit"));
        var btnDeposit = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0c6e7b3df99104c",
            "height": "100%",
            "id": "btnDeposit",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_d7b96f0d523e4992a759010fb2db87ba,
            "skin": "CopydefBtnNormal0c6e7b3df99104c",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnDeposit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnDeposit"), extendConfig({}, controller.args[2], "btnDeposit"));
        flxDeposits.add(lblDeposit, btnDeposit);
        var flxAccount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "90dp",
            "id": "flxAccount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_cb20c973e91b4730806650c7d0361ea1,
            "skin": "sknFlxTypeUnselect",
            "top": "25%",
            "width": "185dp",
            "zIndex": 1
        }, controller.args[0], "flxAccount"), extendConfig({}, controller.args[1], "flxAccount"), extendConfig({}, controller.args[2], "flxAccount"));
        flxAccount.setDefaultUnit(kony.flex.DP);
        var lblAccount = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblAccount",
            "isVisible": true,
            "skin": "CopydefLabel0be32d5fe0b6b46",
            "text": "Account Services",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAccount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccount"), extendConfig({}, controller.args[2], "lblAccount"));
        var btnAccount = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0c8814c5e570648",
            "height": "100%",
            "id": "btnAccount",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_g07438ebeb6143949f2e5d43b002caf3,
            "skin": "CopydefBtnNormal0c8814c5e570648",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnAccount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAccount"), extendConfig({}, controller.args[2], "btnAccount"));
        flxAccount.add(lblAccount, btnAccount);
        flxTypes.add(flxLoan, flxDeposits, flxAccount);
        var flxBottom = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65%",
            "id": "flxBottom",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBottom"), extendConfig({}, controller.args[1], "flxBottom"), extendConfig({}, controller.args[2], "flxBottom"));
        flxBottom.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "17%",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10%",
            "width": "88%",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var lblType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblType",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0dc0d46c370544c",
            "text": "Loan Services",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType"), extendConfig({}, controller.args[2], "lblType"));
        var FlexContainer0da2ffc32184b42 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "2dp",
            "id": "FlexContainer0da2ffc32184b42",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "26%",
            "skin": "CopyslFbox0d2e85f5b544f4f",
            "width": "57%",
            "zIndex": 1
        }, controller.args[0], "FlexContainer0da2ffc32184b42"), extendConfig({}, controller.args[1], "FlexContainer0da2ffc32184b42"), extendConfig({}, controller.args[2], "FlexContainer0da2ffc32184b42"));
        FlexContainer0da2ffc32184b42.setDefaultUnit(kony.flex.DP);
        FlexContainer0da2ffc32184b42.add();
        flxHeader.add(lblType, FlexContainer0da2ffc32184b42);
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18%",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "12%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "38%",
            "width": "70%"
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({}, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lbl1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "35dp",
            "id": "lbl1",
            "isVisible": false,
            "left": "0%",
            "skin": "CopysknStatusNew",
            "text": "Disburse",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl1"), extendConfig({}, controller.args[2], "lbl1"));
        var lbl2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "35dp",
            "id": "lbl2",
            "isVisible": false,
            "left": "2%",
            "skin": "CopysknStatusNew",
            "text": "Increase Commitment",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl2"), extendConfig({}, controller.args[2], "lbl2"));
        var lbl3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "35dp",
            "id": "lbl3",
            "isVisible": false,
            "left": "2%",
            "skin": "CopysknStatusNew",
            "text": "Payment Holiday",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl3"), extendConfig({}, controller.args[2], "lbl3"));
        var flx1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flx1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "CopysknFlxTypeUnselect0a864cae510434f",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flx1"), extendConfig({}, controller.args[1], "flx1"), extendConfig({}, controller.args[2], "flx1"));
        flx1.setDefaultUnit(kony.flex.DP);
        var btn1 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e3661809bb844b",
            "height": "100%",
            "id": "btn1",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_e4ea719cfadc4f008f35b898932b2808,
            "skin": "CopydefBtnNormal0e3661809bb844b",
            "text": "Disbursement",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btn1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [8, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btn1"), extendConfig({}, controller.args[2], "btn1"));
        flx1.add(btn1);
        var flx2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flx2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "CopysknFlxTypeUnselect0a864cae510434f",
            "top": "0dp",
            "width": "23%",
            "zIndex": 1
        }, controller.args[0], "flx2"), extendConfig({}, controller.args[1], "flx2"), extendConfig({}, controller.args[2], "flx2"));
        flx2.setDefaultUnit(kony.flex.DP);
        var btn2 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e3661809bb844b",
            "height": "100%",
            "id": "btn2",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_e5c26499c6f243f585ceeb0565124ad8,
            "skin": "CopydefBtnNormal0e3661809bb844b",
            "text": "Increase Commitment",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btn2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [8, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btn2"), extendConfig({}, controller.args[2], "btn2"));
        flx2.add(btn2);
        var flx3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flx3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "CopysknFlxTypeUnselect0a864cae510434f",
            "top": "0dp",
            "width": "18%",
            "zIndex": 1
        }, controller.args[0], "flx3"), extendConfig({}, controller.args[1], "flx3"), extendConfig({}, controller.args[2], "flx3"));
        flx3.setDefaultUnit(kony.flex.DP);
        var btn3 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e3661809bb844b",
            "height": "100%",
            "id": "btn3",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_f44e332a93af4fad874a55484222a065,
            "skin": "CopydefBtnNormal0e3661809bb844b",
            "text": "Payment Holiday",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btn3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [8, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btn3"), extendConfig({}, controller.args[2], "btn3"));
        flx3.add(btn3);
        var flx4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flx4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "CopysknFlxTypeUnselect0a864cae510434f",
            "top": "0dp",
            "width": "13%",
            "zIndex": 1
        }, controller.args[0], "flx4"), extendConfig({}, controller.args[1], "flx4"), extendConfig({}, controller.args[2], "flx4"));
        flx4.setDefaultUnit(kony.flex.DP);
        var btn4 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e3661809bb844b",
            "height": "100%",
            "id": "btn4",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_af8b0070cab044c2a847ff5bf8a5ffbc,
            "skin": "CopydefBtnNormal0e3661809bb844b",
            "text": "Repayment",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btn4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [8, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btn4"), extendConfig({}, controller.args[2], "btn4"));
        flx4.add(btn4);
        flxOptions.add(lbl1, lbl2, lbl3, flx1, flx2, flx3, flx4);
        var flxOptions2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18%",
            "id": "flxOptions2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "12%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "62%",
            "width": "80%"
        }, controller.args[0], "flxOptions2"), extendConfig({}, controller.args[1], "flxOptions2"), extendConfig({}, controller.args[2], "flxOptions2"));
        flxOptions2.setDefaultUnit(kony.flex.DP);
        var lbl4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "35dp",
            "id": "lbl4",
            "isVisible": false,
            "left": "0%",
            "skin": "CopysknStatusNew",
            "text": "Repayment",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl4"), extendConfig({}, controller.args[2], "lbl4"));
        var lbl5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "35dp",
            "id": "lbl5",
            "isVisible": false,
            "left": "2%",
            "skin": "CopysknStatusNew",
            "text": "Change Interest",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl5"), extendConfig({}, controller.args[2], "lbl5"));
        var lbl6 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "35dp",
            "id": "lbl6",
            "isVisible": false,
            "left": "2%",
            "skin": "CopysknStatusNew",
            "text": "Loan Payoff",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl6"), extendConfig({}, controller.args[2], "lbl6"));
        var lbl7 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "35dp",
            "id": "lbl7",
            "isVisible": false,
            "left": "2%",
            "skin": "CopysknStatusNew",
            "text": "Mortgage Loan Creation",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 1, 3, 1],
            "paddingInPixel": false
        }, controller.args[1], "lbl7"), extendConfig({}, controller.args[2], "lbl7"));
        var flx5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flx5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "CopysknFlxTypeUnselect0a864cae510434f",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flx5"), extendConfig({}, controller.args[1], "flx5"), extendConfig({}, controller.args[2], "flx5"));
        flx5.setDefaultUnit(kony.flex.DP);
        var btn5 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e3661809bb844b",
            "height": "100%",
            "id": "btn5",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_fd84a072e5794071b3d4481a0ffb4ef3,
            "skin": "CopydefBtnNormal0e3661809bb844b",
            "text": "Change Interest",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btn5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [8, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btn5"), extendConfig({}, controller.args[2], "btn5"));
        flx5.add(btn5);
        var flx6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flx6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "CopysknFlxTypeUnselect0a864cae510434f",
            "top": "0dp",
            "width": "12%",
            "zIndex": 1
        }, controller.args[0], "flx6"), extendConfig({}, controller.args[1], "flx6"), extendConfig({}, controller.args[2], "flx6"));
        flx6.setDefaultUnit(kony.flex.DP);
        var btn6 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e3661809bb844b",
            "height": "100%",
            "id": "btn6",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_d5e7355265e04a868c79b333c07ad831,
            "skin": "CopydefBtnNormal0e3661809bb844b",
            "text": "Loan Creation",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btn6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [8, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btn6"), extendConfig({}, controller.args[2], "btn6"));
        flx6.add(btn6);
        var flx7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flx7",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "CopysknFlxTypeUnselect0a864cae510434f",
            "top": "0dp",
            "width": "21%",
            "zIndex": 1
        }, controller.args[0], "flx7"), extendConfig({}, controller.args[1], "flx7"), extendConfig({}, controller.args[2], "flx7"));
        flx7.setDefaultUnit(kony.flex.DP);
        var btn7 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e3661809bb844b",
            "height": "100%",
            "id": "btn7",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_f366aa14dac14f8aac543153bab98f79,
            "skin": "CopydefBtnNormal0e3661809bb844b",
            "text": "Personal Loan Creation",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btn7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [8, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btn7"), extendConfig({}, controller.args[2], "btn7"));
        flx7.add(btn7);
        var flx8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flx8",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "CopysknFlxTypeUnselect0a864cae510434f",
            "top": "0dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flx8"), extendConfig({}, controller.args[1], "flx8"), extendConfig({}, controller.args[2], "flx8"));
        flx8.setDefaultUnit(kony.flex.DP);
        var btn8 = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e3661809bb844b",
            "height": "100%",
            "id": "btn8",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_j9534b4701d14de78630876d593c6806,
            "skin": "CopydefBtnNormal0e3661809bb844b",
            "text": "Mortgage Loan Creation",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btn8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [8, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btn8"), extendConfig({}, controller.args[2], "btn8"));
        flx8.add(btn8);
        flxOptions2.add(lbl4, lbl5, lbl6, lbl7, flx5, flx6, flx7, flx8);
        flxBottom.add(flxHeader, flxOptions, flxOptions2);
        flxServices.add(flxTypes, flxBottom);
        var flxServiceType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "66%",
            "id": "flxServiceType",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "34%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxServiceType"), extendConfig({}, controller.args[1], "flxServiceType"), extendConfig({}, controller.args[2], "flxServiceType"));
        flxServiceType.setDefaultUnit(kony.flex.DP);
        var segServiceTypes = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "lblServiceType": "Button"
            }],
            "groupCells": false,
            "height": "88%",
            "id": "segServiceTypes",
            "isVisible": true,
            "left": "3%",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "Copyseg0hf86d63f783543",
            "rowTemplate": "flxServiceType",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "9%",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxIndicator": "flxIndicator",
                "flxServiceType": "flxServiceType",
                "lblServiceType": "lblServiceType"
            },
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "segServiceTypes"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segServiceTypes"), extendConfig({}, controller.args[2], "segServiceTypes"));
        var flxDesc = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "98.50%",
            "horizontalScrollIndicator": true,
            "id": "flxDesc",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "28%",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "70%",
            "zIndex": 1
        }, controller.args[0], "flxDesc"), extendConfig({}, controller.args[1], "flxDesc"), extendConfig({}, controller.args[2], "flxDesc"));
        flxDesc.setDefaultUnit(kony.flex.DP);
        var flxDesc1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "26%",
            "id": "flxDesc1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "CopyslFbox0b55482982c8340",
            "top": "9%",
            "width": "96%",
            "zIndex": 1
        }, controller.args[0], "flxDesc1"), extendConfig({}, controller.args[1], "flxDesc1"), extendConfig({}, controller.args[2], "flxDesc1"));
        flxDesc1.setDefaultUnit(kony.flex.DP);
        var lblDesc1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDesc1",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopydefLabel0g410ea96985e4c",
            "text": "How to Change Interest Rate",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDesc1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesc1"), extendConfig({}, controller.args[2], "lblDesc1"));
        var flxThumbnail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "85%",
            "id": "flxThumbnail",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "CopyslFbox0c79c721c2aee4b",
            "top": "0dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flxThumbnail"), extendConfig({}, controller.args[1], "flxThumbnail"), extendConfig({}, controller.args[2], "flxThumbnail"));
        flxThumbnail.setDefaultUnit(kony.flex.DP);
        var PlayCircleDark = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "PlayCircleDark",
            "isVisible": true,
            "skin": "CopyslFontAwesomeIcon0e799814c2ef944",
            "text": "",
            "top": "0dp",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "PlayCircleDark"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "PlayCircleDark"), extendConfig({}, controller.args[2], "PlayCircleDark"));
        var btnVideo = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0g50b9ee526a644",
            "height": "100%",
            "id": "btnVideo",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_b5a11f28e4e94e3f87e7addb97961710,
            "skin": "CopydefBtnNormal0g50b9ee526a644",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnVideo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnVideo"), extendConfig({}, controller.args[2], "btnVideo"));
        flxThumbnail.add(PlayCircleDark, btnVideo);
        flxDesc1.add(lblDesc1, flxThumbnail);
        var flxDescHeader2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12%",
            "id": "flxDescHeader2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10%",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxDescHeader2"), extendConfig({}, controller.args[1], "flxDescHeader2"), extendConfig({}, controller.args[2], "flxDescHeader2"));
        flxDescHeader2.setDefaultUnit(kony.flex.DP);
        var lblDescHeader2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescHeader2",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopydefLabel0c0e6d1fb9d8146",
            "text": "Dictionary",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescHeader2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescHeader2"), extendConfig({}, controller.args[2], "lblDescHeader2"));
        var rtxShow2 = new kony.ui.RichText(extendConfig({
            "centerY": "50%",
            "id": "rtxShow2",
            "isVisible": true,
            "linkSkin": "defRichTextLink",
            "right": "15dp",
            "skin": "CopydefRichTextNormal0af274e5b181349",
            "text": "<p><font color=\"#808080\"><u>Show More</u></font></p>\n",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxShow2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxShow2"), extendConfig({}, controller.args[2], "rtxShow2"));
        var btnShowMore = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0f8e16b46b0bb40",
            "height": "100%",
            "id": "btnShowMore",
            "isVisible": true,
            "onClick": controller.AS_Button_eb7d3dbba998435e9040ecab21dbf2b6,
            "right": "0dp",
            "skin": "CopydefBtnNormal0f8e16b46b0bb40",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "btnShowMore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnShowMore"), extendConfig({}, controller.args[2], "btnShowMore"));
        flxDescHeader2.add(lblDescHeader2, rtxShow2, btnShowMore);
        var flxDesc2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22%",
            "id": "flxDesc2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5%",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "flxDesc2"), extendConfig({}, controller.args[1], "flxDesc2"), extendConfig({}, controller.args[2], "flxDesc2"));
        flxDesc2.setDefaultUnit(kony.flex.DP);
        var flxLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0%",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxLeft"), extendConfig({}, controller.args[1], "flxLeft"), extendConfig({}, controller.args[2], "flxLeft"));
        flxLeft.setDefaultUnit(kony.flex.DP);
        var flxSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator1"), extendConfig({}, controller.args[1], "flxSeparator1"), extendConfig({}, controller.args[2], "flxSeparator1"));
        flxSeparator1.setDefaultUnit(kony.flex.DP);
        flxSeparator1.add();
        var flxDescType1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "42%",
            "id": "flxDescType1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxDescType1"), extendConfig({}, controller.args[1], "flxDescType1"), extendConfig({}, controller.args[2], "flxDescType1"));
        flxDescType1.setDefaultUnit(kony.flex.DP);
        var lblDescType1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescType1",
            "isVisible": true,
            "left": "15dp",
            "skin": "CopydefLabel0fd66c37ab96f4e",
            "text": "Interest Rate Type",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescType1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescType1"), extendConfig({}, controller.args[2], "lblDescType1"));
        var plus1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "plus1",
            "isVisible": true,
            "right": "5dp",
            "skin": "CopyslFontAwesomeIcon0a856a9535a4248",
            "text": "G",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "plus1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "plus1"), extendConfig({}, controller.args[2], "plus1"));
        var btnPlus1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0c0ed3b431ae244",
            "height": "100%",
            "id": "btnPlus1",
            "isVisible": true,
            "onClick": controller.AS_Button_gd593ce330574612903e381494a26fe8,
            "right": "0dp",
            "skin": "CopydefBtnNormal0c0ed3b431ae244",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnPlus1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPlus1"), extendConfig({}, controller.args[2], "btnPlus1"));
        flxDescType1.add(lblDescType1, plus1, btnPlus1);
        var flxExpand1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60%",
            "id": "flxExpand1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxExpand1"), extendConfig({}, controller.args[1], "flxExpand1"), extendConfig({}, controller.args[2], "flxExpand1"));
        flxExpand1.setDefaultUnit(kony.flex.DP);
        var rtx1 = new kony.ui.RichText(extendConfig({
            "id": "rtx1",
            "isVisible": true,
            "left": "15dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0ia05dbea757f45",
            "text": "<p>&#9679;     Refers to the existing fixed interest rate of the loan<br>&#9679;     It can be edited and left blank if the loan is initially based on floating rate of interest <br>&#9679;     You can subsequently choose fixed option and fill in new fixed rate if the customer wishes to switch over<br></p>",
            "top": "10dp",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "rtx1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtx1"), extendConfig({}, controller.args[2], "rtx1"));
        var Label0e9ea2d0c21b54f = new kony.ui.Label(extendConfig({
            "id": "Label0e9ea2d0c21b54f",
            "isVisible": false,
            "left": "20dp",
            "skin": "CopydefLabel0ff3266f6383444",
            "text": "The default interest rate type agreed in the loan contract is displayed on the screenAn option to choose either Fixed or Floating rate is provided to alter the interest rate typeEntering the interest rate type is mandatory and cannot be left blank",
            "top": "0dp",
            "width": "75%",
            "zIndex": 1
        }, controller.args[0], "Label0e9ea2d0c21b54f"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "Label0e9ea2d0c21b54f"), extendConfig({}, controller.args[2], "Label0e9ea2d0c21b54f"));
        flxExpand1.add(rtx1, Label0e9ea2d0c21b54f);
        var flxSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator2"), extendConfig({}, controller.args[1], "flxSeparator2"), extendConfig({}, controller.args[2], "flxSeparator2"));
        flxSeparator2.setDefaultUnit(kony.flex.DP);
        flxSeparator2.add();
        var flxDescType2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "42%",
            "id": "flxDescType2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxDescType2"), extendConfig({}, controller.args[1], "flxDescType2"), extendConfig({}, controller.args[2], "flxDescType2"));
        flxDescType2.setDefaultUnit(kony.flex.DP);
        var plus2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "plus2",
            "isVisible": true,
            "right": "5dp",
            "skin": "CopyslFontAwesomeIcon0a856a9535a4248",
            "text": "G",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "plus2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "plus2"), extendConfig({}, controller.args[2], "plus2"));
        var lblDescType2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescType2",
            "isVisible": true,
            "left": "15dp",
            "skin": "CopydefLabel0fd66c37ab96f4e",
            "text": "Effective Date",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescType2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescType2"), extendConfig({}, controller.args[2], "lblDescType2"));
        var btnPlus2 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0c0ed3b431ae244",
            "height": "100%",
            "id": "btnPlus2",
            "isVisible": true,
            "onClick": controller.AS_Button_a61dda0d6ec3460eab26afb79ec07dd8,
            "right": "0dp",
            "skin": "CopydefBtnNormal0c0ed3b431ae244",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnPlus2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPlus2"), extendConfig({}, controller.args[2], "btnPlus2"));
        flxDescType2.add(plus2, lblDescType2, btnPlus2);
        var flxExpand2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30%",
            "id": "flxExpand2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxExpand2"), extendConfig({}, controller.args[1], "flxExpand2"), extendConfig({}, controller.args[2], "flxExpand2"));
        flxExpand2.setDefaultUnit(kony.flex.DP);
        var rtx2 = new kony.ui.RichText(extendConfig({
            "id": "rtx2",
            "isVisible": true,
            "left": "15dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0ia05dbea757f45",
            "text": "RichText",
            "top": "10dp",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "rtx2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtx2"), extendConfig({}, controller.args[2], "rtx2"));
        flxExpand2.add(rtx2);
        var flxSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator3"), extendConfig({}, controller.args[1], "flxSeparator3"), extendConfig({}, controller.args[2], "flxSeparator3"));
        flxSeparator3.setDefaultUnit(kony.flex.DP);
        flxSeparator3.add();
        flxLeft.add(flxSeparator1, flxDescType1, flxExpand1, flxSeparator2, flxDescType2, flxExpand2, flxSeparator3);
        var flxRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "51%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0%",
            "width": "49%",
            "zIndex": 1
        }, controller.args[0], "flxRight"), extendConfig({}, controller.args[1], "flxRight"), extendConfig({}, controller.args[2], "flxRight"));
        flxRight.setDefaultUnit(kony.flex.DP);
        var flxSeparator4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator4"), extendConfig({}, controller.args[1], "flxSeparator4"), extendConfig({}, controller.args[2], "flxSeparator4"));
        flxSeparator4.setDefaultUnit(kony.flex.DP);
        flxSeparator4.add();
        var flxDescType3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "42%",
            "id": "flxDescType3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxDescType3"), extendConfig({}, controller.args[1], "flxDescType3"), extendConfig({}, controller.args[2], "flxDescType3"));
        flxDescType3.setDefaultUnit(kony.flex.DP);
        var plus3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "plus3",
            "isVisible": true,
            "right": "0dp",
            "skin": "CopyslFontAwesomeIcon0a856a9535a4248",
            "text": "G",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "plus3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "plus3"), extendConfig({}, controller.args[2], "plus3"));
        var lblDescType3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescType3",
            "isVisible": true,
            "left": "15dp",
            "skin": "CopydefLabel0fd66c37ab96f4e",
            "text": "Tier Type",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescType3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescType3"), extendConfig({}, controller.args[2], "lblDescType3"));
        var btnPlus3 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0c0ed3b431ae244",
            "height": "100%",
            "id": "btnPlus3",
            "isVisible": true,
            "onClick": controller.AS_Button_e8ebbe79c01b4e9a85b59a3b86968214,
            "right": "0dp",
            "skin": "CopydefBtnNormal0c0ed3b431ae244",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnPlus3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPlus3"), extendConfig({}, controller.args[2], "btnPlus3"));
        flxDescType3.add(plus3, lblDescType3, btnPlus3);
        var flxExpand3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36%",
            "id": "flxExpand3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxExpand3"), extendConfig({}, controller.args[1], "flxExpand3"), extendConfig({}, controller.args[2], "flxExpand3"));
        flxExpand3.setDefaultUnit(kony.flex.DP);
        var rtx3 = new kony.ui.RichText(extendConfig({
            "id": "rtx3",
            "isVisible": true,
            "left": "15dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0ia05dbea757f45",
            "text": "RichText",
            "top": "10dp",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "rtx3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtx3"), extendConfig({}, controller.args[2], "rtx3"));
        flxExpand3.add(rtx3);
        var flxSeparator5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator5"), extendConfig({}, controller.args[1], "flxSeparator5"), extendConfig({}, controller.args[2], "flxSeparator5"));
        flxSeparator5.setDefaultUnit(kony.flex.DP);
        flxSeparator5.add();
        var flxDescType4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "42%",
            "id": "flxDescType4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxDescType4"), extendConfig({}, controller.args[1], "flxDescType4"), extendConfig({}, controller.args[2], "flxDescType4"));
        flxDescType4.setDefaultUnit(kony.flex.DP);
        var plus4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "plus4",
            "isVisible": true,
            "right": "0dp",
            "skin": "CopyslFontAwesomeIcon0a856a9535a4248",
            "text": "G",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "plus4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "plus4"), extendConfig({}, controller.args[2], "plus4"));
        var lblDescType4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescType4",
            "isVisible": true,
            "left": "15dp",
            "skin": "CopydefLabel0fd66c37ab96f4e",
            "text": "Fixed Rate",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescType4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescType4"), extendConfig({}, controller.args[2], "lblDescType4"));
        var btnPlus4 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0c0ed3b431ae244",
            "height": "100%",
            "id": "btnPlus4",
            "isVisible": true,
            "onClick": controller.AS_Button_b050c26801874b91aa86dd60d1de7653,
            "right": "0dp",
            "skin": "CopydefBtnNormal0c0ed3b431ae244",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnPlus4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPlus4"), extendConfig({}, controller.args[2], "btnPlus4"));
        flxDescType4.add(plus4, lblDescType4, btnPlus4);
        var flxExpand4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36%",
            "id": "flxExpand4",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_f2df1befbb2c462ea205c7061ea7e186,
            "skin": "slFbox",
            "top": "1%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxExpand4"), extendConfig({}, controller.args[1], "flxExpand4"), extendConfig({}, controller.args[2], "flxExpand4"));
        flxExpand4.setDefaultUnit(kony.flex.DP);
        var rtx4 = new kony.ui.RichText(extendConfig({
            "id": "rtx4",
            "isVisible": true,
            "left": "15dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0ia05dbea757f45",
            "text": "RichText",
            "top": "10dp",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "rtx4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtx4"), extendConfig({}, controller.args[2], "rtx4"));
        flxExpand4.add(rtx4);
        var flxSeparator6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator6"), extendConfig({}, controller.args[1], "flxSeparator6"), extendConfig({}, controller.args[2], "flxSeparator6"));
        flxSeparator6.setDefaultUnit(kony.flex.DP);
        flxSeparator6.add();
        flxRight.add(flxSeparator4, flxDescType3, flxExpand3, flxSeparator5, flxDescType4, flxExpand4, flxSeparator6);
        flxDesc2.add(flxLeft, flxRight);
        var flxDescHeader3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12%",
            "id": "flxDescHeader3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxDescHeader3"), extendConfig({}, controller.args[1], "flxDescHeader3"), extendConfig({}, controller.args[2], "flxDescHeader3"));
        flxDescHeader3.setDefaultUnit(kony.flex.DP);
        var lblDescHeader3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescHeader3",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopydefLabel0c0e6d1fb9d8146",
            "text": "Frequently Asked Questions",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescHeader3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescHeader3"), extendConfig({}, controller.args[2], "lblDescHeader3"));
        var rtxShow3 = new kony.ui.RichText(extendConfig({
            "centerY": "50%",
            "id": "rtxShow3",
            "isVisible": true,
            "linkSkin": "defRichTextLink",
            "right": "15dp",
            "skin": "CopydefRichTextNormal0af274e5b181349",
            "text": "<p><font color=\"#808080\"><u>Show More</u></font></p>\n",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxShow3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxShow3"), extendConfig({}, controller.args[2], "rtxShow3"));
        var btnMoreQues = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0f8e16b46b0bb40",
            "height": "100%",
            "id": "btnMoreQues",
            "isVisible": true,
            "right": "0dp",
            "skin": "CopydefBtnNormal0f8e16b46b0bb40",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "btnMoreQues"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnMoreQues"), extendConfig({}, controller.args[2], "btnMoreQues"));
        flxDescHeader3.add(lblDescHeader3, rtxShow3, btnMoreQues);
        var flxDesc3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "25%",
            "id": "flxDesc3",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxDesc3"), extendConfig({}, controller.args[1], "flxDesc3"), extendConfig({}, controller.args[2], "flxDesc3"));
        flxDesc3.setDefaultUnit(kony.flex.DP);
        var flxFAQ = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFAQ",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFAQ"), extendConfig({}, controller.args[1], "flxFAQ"), extendConfig({}, controller.args[2], "flxFAQ"));
        flxFAQ.setDefaultUnit(kony.flex.DP);
        var flxSeparator7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator7",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator7"), extendConfig({}, controller.args[1], "flxSeparator7"), extendConfig({}, controller.args[2], "flxSeparator7"));
        flxSeparator7.setDefaultUnit(kony.flex.DP);
        flxSeparator7.add();
        var flxQues1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "44%",
            "id": "flxQues1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxQues1"), extendConfig({}, controller.args[1], "flxQues1"), extendConfig({}, controller.args[2], "flxQues1"));
        flxQues1.setDefaultUnit(kony.flex.DP);
        var lblQues1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblQues1",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopydefLabel0fd66c37ab96f4e",
            "text": "Can we define the date from  which the interest rate change should be applicable?",
            "width": "82%",
            "zIndex": 1
        }, controller.args[0], "lblQues1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblQues1"), extendConfig({}, controller.args[2], "lblQues1"));
        var plus5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "plus5",
            "isVisible": true,
            "right": "25dp",
            "skin": "CopyslFontAwesomeIcon0a856a9535a4248",
            "text": "G",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "plus5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "plus5"), extendConfig({}, controller.args[2], "plus5"));
        flxQues1.add(lblQues1, plus5);
        var flxAns1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40%",
            "id": "flxAns1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "81%",
            "zIndex": 1
        }, controller.args[0], "flxAns1"), extendConfig({}, controller.args[1], "flxAns1"), extendConfig({}, controller.args[2], "flxAns1"));
        flxAns1.setDefaultUnit(kony.flex.DP);
        var rtxAns1 = new kony.ui.RichText(extendConfig({
            "id": "rtxAns1",
            "isVisible": true,
            "left": "20dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0c882223f1e7a41",
            "text": "Yes, the field ‘Effective Date’ can be used to define the date from which the rate change is to be applied.",
            "top": "5dp",
            "width": "95.50%",
            "zIndex": 1
        }, controller.args[0], "rtxAns1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAns1"), extendConfig({}, controller.args[2], "rtxAns1"));
        flxAns1.add(rtxAns1);
        var flxSeparator8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator8",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator8"), extendConfig({}, controller.args[1], "flxSeparator8"), extendConfig({}, controller.args[2], "flxSeparator8"));
        flxSeparator8.setDefaultUnit(kony.flex.DP);
        flxSeparator8.add();
        var flxQues2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "44%",
            "id": "flxQues2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxQues2"), extendConfig({}, controller.args[1], "flxQues2"), extendConfig({}, controller.args[2], "flxQues2"));
        flxQues2.setDefaultUnit(kony.flex.DP);
        var lblQues2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblQues2",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopydefLabel0fd66c37ab96f4e",
            "text": "Is it possible to change the type of interest for a loan i.e., fixed to floating and vice versa?",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblQues2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblQues2"), extendConfig({}, controller.args[2], "lblQues2"));
        var plus6 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "plus6",
            "isVisible": true,
            "right": "25dp",
            "skin": "CopyslFontAwesomeIcon0a856a9535a4248",
            "text": "G",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "plus6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "plus6"), extendConfig({}, controller.args[2], "plus6"));
        flxQues2.add(lblQues2, plus6);
        var flxAns2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40%",
            "id": "flxAns2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "81%",
            "zIndex": 1
        }, controller.args[0], "flxAns2"), extendConfig({}, controller.args[1], "flxAns2"), extendConfig({}, controller.args[2], "flxAns2"));
        flxAns2.setDefaultUnit(kony.flex.DP);
        var rtxAns2 = new kony.ui.RichText(extendConfig({
            "id": "rtxAns2",
            "isVisible": true,
            "left": "20dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0c882223f1e7a41",
            "text": "Yes, the interest on an existing loan can be changed from Fixed type to floating type and vice versa. When the type of interest is changed, the earlier values are overwritten, and the new values take effect from the effective date of interest change",
            "top": "5dp",
            "width": "95.50%",
            "zIndex": 1
        }, controller.args[0], "rtxAns2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAns2"), extendConfig({}, controller.args[2], "rtxAns2"));
        flxAns2.add(rtxAns2);
        var flxSeparator9 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator9",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0f3e7ae57148d4b",
            "top": "1dp",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator9"), extendConfig({}, controller.args[1], "flxSeparator9"), extendConfig({}, controller.args[2], "flxSeparator9"));
        flxSeparator9.setDefaultUnit(kony.flex.DP);
        flxSeparator9.add();
        flxFAQ.add(flxSeparator7, flxQues1, flxAns1, flxSeparator8, flxQues2, flxAns2, flxSeparator9);
        flxDesc3.add(flxFAQ);
        flxDesc.add(flxDesc1, flxDescHeader2, flxDesc2, flxDescHeader3, flxDesc3);
        var lblNoData = new kony.ui.Label(extendConfig({
            "id": "lblNoData",
            "isVisible": false,
            "left": "50%",
            "skin": "CopydefLabel0c793b033d85749",
            "text": "Content update in progress",
            "top": "38%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoData"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoData"), extendConfig({}, controller.args[2], "lblNoData"));
        flxServiceType.add(segServiceTypes, flxDesc, lblNoData);
        var flxSeg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "64%",
            "id": "flxSeg",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0ba94bfbf51a047",
            "top": "34%",
            "width": "96%"
        }, controller.args[0], "flxSeg"), extendConfig({}, controller.args[1], "flxSeg"), extendConfig({}, controller.args[2], "flxSeg"));
        flxSeg.setDefaultUnit(kony.flex.DP);
        var flxSearchResult = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15%",
            "id": "flxSearchResult",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearchResult"), extendConfig({}, controller.args[1], "flxSearchResult"), extendConfig({}, controller.args[2], "flxSearchResult"));
        flxSearchResult.setDefaultUnit(kony.flex.DP);
        var lblResult = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblResult",
            "isVisible": true,
            "left": "18dp",
            "skin": "CopydefLabel0d3bdda87cdd648",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResult"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResult"), extendConfig({}, controller.args[2], "lblResult"));
        var flxBck = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxBck",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%"
        }, controller.args[0], "flxBck"), extendConfig({}, controller.args[1], "flxBck"), extendConfig({}, controller.args[2], "flxBck"));
        flxBck.setDefaultUnit(kony.flex.DP);
        var lblBackArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBackArrow",
            "isVisible": true,
            "left": "18dp",
            "skin": "CopydefLabel0efb611e65c684f",
            "text": "O",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBackArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBackArrow"), extendConfig({}, controller.args[2], "lblBackArrow"));
        var lblBack = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBack",
            "isVisible": true,
            "left": "38dp",
            "skin": "CopydefLabel0b0777154d7234a",
            "text": "Back",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBack"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBack"), extendConfig({}, controller.args[2], "lblBack"));
        var btnBackShowMore = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0a73c6d785ad648",
            "height": "100%",
            "id": "btnBackShowMore",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_d59462caa3844de598357c8c69b3cb69,
            "skin": "CopydefBtnNormal0a73c6d785ad648",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnBackShowMore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": false,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBackShowMore"), extendConfig({}, controller.args[2], "btnBackShowMore"));
        flxBck.add(lblBackArrow, lblBack, btnBackShowMore);
        flxSearchResult.add(lblResult, flxBck);
        var segSearch = new kony.ui.FlexContainer(extendConfig({
            "centerX": "50%",
            "clipBounds": true,
            "height": "84%",
            "id": "segSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "16%",
            "width": "96%"
        }, controller.args[0], "segSearch"), extendConfig({}, controller.args[1], "segSearch"), extendConfig({}, controller.args[2], "segSearch"));
        segSearch.setDefaultUnit(kony.flex.DP);
        var seg1 = new kony.ui.SegmentedUI2(extendConfig({
            "data": [
                [{
                        "imgLocation": "icons_map_2.png",
                        "lblDown": "M",
                        "lblShelves": "Kony "
                    },
                    [{
                        "lblAddress": "RichText",
                        "lblAddress1": "19909 120TH AVE NE",
                        "lblLine": "Label"
                    }, {
                        "lblAddress": "RichText",
                        "lblAddress1": "19909 120TH AVE NE",
                        "lblLine": "Label"
                    }]
                ],
                [{
                        "imgLocation": "icons_map_2.png",
                        "lblDown": "M",
                        "lblShelves": "Kony "
                    },
                    [{
                        "lblAddress": "RichText",
                        "lblAddress1": "19909 120TH AVE NE",
                        "lblLine": "Label"
                    }, {
                        "lblAddress": "RichText",
                        "lblAddress1": "19909 120TH AVE NE",
                        "lblLine": "Label"
                    }]
                ]
            ],
            "groupCells": false,
            "height": "100%",
            "id": "seg1",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "CopyCopyseg1",
            "rowTemplate": "CopyflxParent",
            "sectionHeaderSkin": "CopysliPhoneSegmentHeader0b690653eba5a43",
            "sectionHeaderTemplate": "CopyflxDown",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "CopyflxDown": "CopyflxDown",
                "CopyflxParent": "CopyflxParent",
                "FlexContainer0c2ca7dc5c88140": "FlexContainer0c2ca7dc5c88140",
                "FlexGroup0d7f2ca3295ba45": "FlexGroup0d7f2ca3295ba45",
                "btnUpArrow": "btnUpArrow",
                "flxLine": "flxLine",
                "flxRow": "flxRow",
                "imgLocation": "imgLocation",
                "lblAddress": "lblAddress",
                "lblAddress1": "lblAddress1",
                "lblDown": "lblDown",
                "lblLine": "lblLine",
                "lblShelves": "lblShelves",
                "lblTitle": "lblTitle"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "seg1"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "seg1"), extendConfig({}, controller.args[2], "seg1"));
        segSearch.add(seg1);
        var flxQues = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "84%",
            "id": "flxQues",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "16%",
            "width": "96%",
            "zIndex": 1
        }, controller.args[0], "flxQues"), extendConfig({}, controller.args[1], "flxQues"), extendConfig({}, controller.args[2], "flxQues"));
        flxQues.setDefaultUnit(kony.flex.DP);
        var segQues = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [
                [{
                        "lblQues": "Label",
                        "lblUp": "J"
                    },
                    [{
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }]
                ],
                [{
                        "lblQues": "Label",
                        "lblUp": "J"
                    },
                    [{
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }, {
                        "rtxAnswer": "RichText"
                    }]
                ]
            ],
            "groupCells": false,
            "height": "100%",
            "id": "segQues",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "Copyseg0jcfc0f8aa75e4d",
            "rowTemplate": "flxAnswers",
            "sectionHeaderSkin": "CopysliPhoneSegmentHeader0b737e41801764f",
            "sectionHeaderTemplate": "flxMoreHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAnswers": "flxAnswers",
                "flxMoreHeader": "flxMoreHeader",
                "flxSpace": "flxSpace",
                "lblQues": "lblQues",
                "lblUp": "lblUp",
                "rtxAnswer": "rtxAnswer"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segQues"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segQues"), extendConfig({}, controller.args[2], "segQues"));
        flxQues.add(segQues);
        var flxBackAns = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15%",
            "id": "flxBackAns",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBackAns"), extendConfig({}, controller.args[1], "flxBackAns"), extendConfig({}, controller.args[2], "flxBackAns"));
        flxBackAns.setDefaultUnit(kony.flex.DP);
        var CopyflxBck0c8bd2742893248 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "CopyflxBck0c8bd2742893248",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_j1d35ca4ca3d4010a3897ab58d163b89,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%"
        }, controller.args[0], "CopyflxBck0c8bd2742893248"), extendConfig({}, controller.args[1], "CopyflxBck0c8bd2742893248"), extendConfig({}, controller.args[2], "CopyflxBck0c8bd2742893248"));
        CopyflxBck0c8bd2742893248.setDefaultUnit(kony.flex.DP);
        var CopylblBackArrow0a405d8ec3e4d46 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "CopylblBackArrow0a405d8ec3e4d46",
            "isVisible": true,
            "left": "18dp",
            "skin": "CopydefLabel0efb611e65c684f",
            "text": "O",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "CopylblBackArrow0a405d8ec3e4d46"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "CopylblBackArrow0a405d8ec3e4d46"), extendConfig({}, controller.args[2], "CopylblBackArrow0a405d8ec3e4d46"));
        var CopylblBack0j2de3a34459746 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "CopylblBack0j2de3a34459746",
            "isVisible": true,
            "left": "38dp",
            "skin": "CopydefLabel0b0777154d7234a",
            "text": "Back",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "CopylblBack0j2de3a34459746"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "CopylblBack0j2de3a34459746"), extendConfig({}, controller.args[2], "CopylblBack0j2de3a34459746"));
        CopyflxBck0c8bd2742893248.add(CopylblBackArrow0a405d8ec3e4d46, CopylblBack0j2de3a34459746);
        flxBackAns.add(CopyflxBck0c8bd2742893248);
        flxSeg.add(flxSearchResult, segSearch, flxQues, flxBackAns);
        flxMain.add(FlexGroup0b337f90c7acb4e, flxServices, flxServiceType, flxSeg);
        var flxPlayVideo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": false,
            "height": "545dp",
            "id": "flxPlayVideo",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopyslFbox0ea3e988cb93148",
            "width": "900dp",
            "zIndex": 1
        }, controller.args[0], "flxPlayVideo"), extendConfig({}, controller.args[1], "flxPlayVideo"), extendConfig({}, controller.args[2], "flxPlayVideo"));
        flxPlayVideo.setDefaultUnit(kony.flex.DP);
        var flxVideoHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "9%",
            "id": "flxVideoHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1%",
            "width": "100%"
        }, controller.args[0], "flxVideoHeader"), extendConfig({}, controller.args[1], "flxVideoHeader"), extendConfig({}, controller.args[2], "flxVideoHeader"));
        flxVideoHeader.setDefaultUnit(kony.flex.DP);
        var flxBreadcrum = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxBreadcrum",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "38%",
            "zIndex": 1
        }, controller.args[0], "flxBreadcrum"), extendConfig({}, controller.args[1], "flxBreadcrum"), extendConfig({}, controller.args[2], "flxBreadcrum"));
        flxBreadcrum.setDefaultUnit(kony.flex.DP);
        flxBreadcrum.add();
        var flxBckType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxBckType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%"
        }, controller.args[0], "flxBckType"), extendConfig({}, controller.args[1], "flxBckType"), extendConfig({}, controller.args[2], "flxBckType"));
        flxBckType.setDefaultUnit(kony.flex.DP);
        var flxBckArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxBckArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flxBckArrow"), extendConfig({}, controller.args[1], "flxBckArrow"), extendConfig({}, controller.args[2], "flxBckArrow"));
        flxBckArrow.setDefaultUnit(kony.flex.DP);
        var cusBackIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "cusBackIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyiconFontPurple0a2f41c3ab23d4d",
            "text": "O",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "cusBackIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "cusBackIcon"), extendConfig({}, controller.args[2], "cusBackIcon"));
        flxBckArrow.add(cusBackIcon);
        var rtxType = new kony.ui.RichText(extendConfig({
            "id": "rtxType",
            "isVisible": true,
            "left": "25%",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0b5276fc8c28b4c",
            "text": "<p><font color=\"#000000\"><u>Loan Services</u></font></p>\n",
            "top": "30%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxType"), extendConfig({}, controller.args[2], "rtxType"));
        var btnBckType = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0d611a34b26b040",
            "height": "100%",
            "id": "btnBckType",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_hb78e04627a942f8b3cf4aabac4b8772,
            "skin": "CopydefBtnNormal0d611a34b26b040",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnBckType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBckType"), extendConfig({}, controller.args[2], "btnBckType"));
        flxBckType.add(flxBckArrow, rtxType, btnBckType);
        var lblSlash = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSlash",
            "isVisible": true,
            "left": "16%",
            "skin": "CopydefLabel0ba3c86f1daeb42",
            "text": "/",
            "top": "0dp",
            "width": "8dp",
            "zIndex": 1
        }, controller.args[0], "lblSlash"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSlash"), extendConfig({}, controller.args[2], "lblSlash"));
        var lblVideoPath = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblVideoPath",
            "isVisible": true,
            "left": "18%",
            "skin": "CopydefLabel0ba3c86f1daeb42",
            "text": "Lending",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVideoPath"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVideoPath"), extendConfig({}, controller.args[2], "lblVideoPath"));
        var flxClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "75%",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "width": "45dp",
            "zIndex": 1
        }, controller.args[0], "flxClose"), extendConfig({}, controller.args[1], "flxClose"), extendConfig({}, controller.args[2], "flxClose"));
        flxClose.setDefaultUnit(kony.flex.DP);
        var lblCloseVideo = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblCloseVideo",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0ef35acea602344",
            "text": "L",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCloseVideo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCloseVideo"), extendConfig({}, controller.args[2], "lblCloseVideo"));
        var btnCloseVideo = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0d0ae0862bb034c",
            "height": "100%",
            "id": "btnCloseVideo",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_fe5de1f853c64277a5b6644bcff72b14,
            "skin": "CopydefBtnNormal0d0ae0862bb034c",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnCloseVideo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCloseVideo"), extendConfig({}, controller.args[2], "btnCloseVideo"));
        flxClose.add(lblCloseVideo, btnCloseVideo);
        flxVideoHeader.add(flxBreadcrum, flxBckType, lblSlash, lblVideoPath, flxClose);
        var flxBrw = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "91%",
            "id": "flxBrw",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "9%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBrw"), extendConfig({}, controller.args[1], "flxBrw"), extendConfig({}, controller.args[2], "flxBrw"));
        flxBrw.setDefaultUnit(kony.flex.DP);
        var brw = new kony.ui.Browser(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "detectTelNumber": true,
            "enableZoom": false,
            "height": "100%",
            "htmlString": "<div style=\"position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;\">\n<iframe src=\"https://player.stornaway.io/embed/4c4499ba\" frameborder=\"0\" allowfullscreen style=\"width: 100%; height: 100%; position: absolute; top: 0; left: 0;\"></iframe>\n</div>",
            "id": "brw",
            "isVisible": true,
            "left": "0dp",
            "setAsContent": false,
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "brw"), extendConfig({}, controller.args[1], "brw"), extendConfig({}, controller.args[2], "brw"));
        flxBrw.add(brw);
        flxPlayVideo.add(flxVideoHeader, flxBrw);
        help.add(flxMain, flxPlayVideo);
        help.breakpointResetData = {};
        help.breakpointData = {
            maxBreakpointWidth: 1366,
            "640": {
                "flxSeg": {
                    "segmentProps": []
                },
                "seg1": {
                    "height": {
                        "type": "string",
                        "value": "100%"
                    },
                    "top": {
                        "type": "string",
                        "value": "0dp"
                    },
                    "segmentProps": []
                }
            }
        }
        help.compInstData = {}
        return help;
    }
})