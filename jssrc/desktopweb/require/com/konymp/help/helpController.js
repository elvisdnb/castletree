define("com/konymp/help/userhelpController", function() {
    return {
        prev_index: -1,
        gblType: "",
        loanArr: ["Disbursement", "Increase Commitment", "Payment Holiday", "Repayment", "Change Interest", "Loan Payoff", "Personal Loan Creation", "Mortgage Loan Creation"],
        depositsArr: [],
        accountArr: [],
        selectedService: [],
        storeData: {},
        globalVariables: function() {
            rowHeight = 120;
            this.loadAddress();
            mData = this.view.seg1.data;
            this.view.flxServices.isVisible = true;
            this.view.flxSeg.isVisible = false;
            this.view.flxBack.isVisible = false;
            this.view.flxServiceType.isVisible = false;
        },
        breakpointChange: function() {
            var currBreakpoint = kony.application.getCurrentBreakpoint();
            if (currBreakpoint === 640) {
                var data = this.view.seg1.data;
                // console.log("segment "+JSON.stringify(data));
                if (data === undefined) {
                    return;
                }
                data.map(function(e) {
                    e[0].template = "flxDownK";
                });
                this.view.seg1.setData(data);
            } else {
                var data1 = this.view.seg1.data;
                // console.log("segment2 "+JSON.stringify(data1));
                if (data1 === undefined) {
                    return;
                }
                data1.map(function(e) {
                    e[0].template = "flxDown";
                });
                this.view.seg1.setData(data1);
            }
        },
        loadAddress: function() {
            this.view.seg1.widgetDataMap = {
                lblAddress: "lblAddress",
                lblShelves: "lblShelves",
                lblTitle: "lblTitle",
                imgLocation: "imgLocation",
                flxDown: "flxDown",
                flxDownK: "flxDownK",
                flxRow: "flxRow",
                flxParent: "flxParent",
                flxProgress: "flxProgress",
                flxAnimate: "flxAnimate",
                flxLine: "flxLine"
            };
            this.view.seg1.setData(changeIntrest);
        },
        onClick: function() {
            var section = this.view.seg1.selectedIndices[0];
            // console.log("section "+JSON.stringify(section));
            if (this.prev_index !== -1) {
                var data = this.view.seg1.data[this.prev_index];
                var count = data[1].length;
                var rowsToAnim = [];
                for (i = 0; i < count; i++) {
                    rowsToAnim.push({
                        rowIndex: i,
                        sectionIndex: this.prev_index
                    });
                }
                data[0].imgLocation = "blue_downarrow.png";
                this.view.seg1.setSectionAt(data, this.prev_index);
                var animationDef1 = {
                    "100": {
                        "stepConfig": {
                            "timingFunction": kony.anim.EASE
                        },
                        "height": "0dp"
                    }
                };
                var animationConfig1 = {
                    "delay": 0,
                    "iterationCount": 1,
                    "fillMode": kony.anim.FILL_MODE_FORWARDS,
                    "duration": 0.1
                };
                var animationDefObject1 = kony.ui.createAnimation(animationDef1);
                this.view.seg1.animateRows({
                    rows: rowsToAnim,
                    widgets: ["flxRow"],
                    animation: {
                        definition: animationDefObject1,
                        config: animationConfig1
                    }
                });
                console.log("rowsToAnim " + JSON.stringify(rowsToAnim));
                console.log("this.prev_index " + this.prev_index);
            }
            if (section == this.prev_index) {
                this.prev_index = -1;
                return;
            }
            var data1 = this.view.seg1.data[section];
            const search = "Change OD Limit";
            data1[1].forEach(value => {
                if (value.lblAddress.text === search) {
                    value.lblAddress.skin = "sknSelectedItem";
                }
            });
            // console.log(JSON.stringify(data1[1]));
            data1[0].imgLocation = "blue_uparrow.png";
            this.view.seg1.setSectionAt(data1, section);
            var count1 = data1[1].length;
            var rowsToAnim1 = [];
            for (i = 0; i < count1; i++) {
                rowsToAnim1.push({
                    rowIndex: i,
                    sectionIndex: section
                });
            }
            var animationDef = {
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "height": rowHeight + "dp" //kony.flex.USE_PREFERED_SIZE // rowHeight+"dp"
                }
            };
            var animationConfig = {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 0.1
            };
            var animationDefObject = kony.ui.createAnimation(animationDef);
            this.view.seg1.animateRows({
                rows: rowsToAnim1,
                widgets: ["flxRow"],
                animation: {
                    definition: animationDefObject,
                    config: animationConfig
                }
            });
            this.prev_index = section;
            console.log("rowsToAnim1 " + JSON.stringify(rowsToAnim1));
        },
        onClickRow: function(context) {
            var row = this.view.seg1.selectedRowIndex[1];
            var section = this.view.seg1.selectedRowIndex[0];
            var data = this.view.seg1.data[section][1][row];
        },
        rowClickNav: function() {
            this.view.txtPartialText.text = "";
            this.view.lblSearchIcon.isVisible = true;
            this.view.clearTxtBxSearch.isVisible = false;
            this.view.imgClearSearch.isVisible = false;
            this.view.btnClearTxt.isVisible = false;
            this.view.flxSeg.isVisible = false;
            var x = this.view.seg1.selectedRowItems[0];
        },
        getSearchResult: function(mData, searchValue) {
            var i, j;
            var length = mData.length;
            var sData = [];
            for (i = 0; i < length; i++) {
                if (mData[i] !== null || mData[i] !== undefined) {
                    var txt1 = "";
                    var txt2 = "";
                    if (mData[i][1][0].lblAddress !== null && mData[i][1][0].lblAddress !== undefined) {
                        txt1 = mData[i][1][0].lblAddress;
                        txt1 = txt1.toLowerCase();
                    }
                    if (mData[i][0].lblShelves !== null && mData[i][0].lblShelves !== undefined) {
                        txt2 = mData[i][0].lblShelves;
                        txt2 = txt2.toLowerCase();
                    }
                    var pattern = searchValue.toLowerCase();
                    if (txt1.search(pattern.trim()) !== -1 || txt2.search(pattern.trim()) !== -1) {
                        sData.push(mData[i]);
                        var dataLength = sData.length - 1;
                    }
                }
            }
            return sData;
        },
        searchData: function() {
            var tempChangeInterestData = showTableChangeInterest();
            var tempLoanData = showTableLoanPayOff();
            var tempPaymentData = showTablePaymentHoliday();
            mData = tempChangeInterestData.concat(tempLoanData, tempPaymentData);
            this.view.flxSeg.isVisible = true;
            this.onTxtChange(this.view.txtPartialText.text);
        },
        onTxtChange: function(searchValue) {
            var search = searchValue;
            if (search !== "") {
                this.view.clearTxtBxSearch.isVisible = true;
                this.view.imgClearSearch.isVisible = true;
                this.view.btnClearTxt.isVisible = true;
                this.view.flxBackAns.isVisible = false;
                this.view.flxQues.isVisible = false;
                this.view.lblSearchIcon.isVisible = false;
                this.view.flxSearchResult.isVisible = true;
                this.view.lblResult.isVisible = true;
                this.view.flxSeg.isVisible = true;
                this.view.segSearch.isVisible = true;
                this.view.showMore.isVisible = false;
                this.view.flxBck.isVisible = false;
                this.view.flxDesc.scrollToEnd();
                var sData = this.getSearchResult(mData, search);
                this.view.lblResult.text = "Showing (" + sData.length + ") search results for '" + search + "'";
                this.view.seg1.setData(sData);
            } else {
                if (this.view.flxServices.isVisible) {
                    this.view.flxServiceType.isVisible = false;
                } else {
                    this.view.flxServiceType.isVisible = true;
                }
                this.clearTxtBxSearchReset();
            }
        },
        clearTxtBxSearch: function() {
            this.view.txtPartialText.text = "";
            this.clearTxtBxSearchReset();
            this.view.lblResult.text = "";
            this.view.flxServiceType.isVisible = true;
            if (this.view.flxServices.isVisible) {
                this.view.flxServiceType.isVisible = false;
            } else {
                this.view.flxServiceType.isVisible = true;
            }
        },
        clearTxtBxSearchReset: function() {
            this.view.lblSearchIcon.isVisible = true;
            this.view.clearTxtBxSearch.isVisible = false;
            this.view.imgClearSearch.isVisible = false;
            this.view.btnClearTxt.isVisible = false;
            this.view.flxSeg.isVisible = false;
        },
        showMore: function() {
            if (this.gblType === "Change Interest") {
                var tempTable = showTableChangeInterest();
                this.showMoreReset();
                this.view.seg1.setData(tempTable);
            } else if (this.gblType === "Loan Payoff") {
                var tempLoanTable = showTableLoanPayOff();
                this.showMoreReset();
                this.view.seg1.setData(tempLoanTable);
            } else if (this.gblType === "Payment Holiday") {
                var tempPaymentTable = showTablePaymentHoliday();
                this.showMoreReset();
                this.view.seg1.setData(tempPaymentTable);
            } else {
                this.view.lblNoData.isVisible = true;
                this.view.flxDesc.isVisible = false;
            }
            this.collapse();
        },
        showMoreReset: function() {
            this.view.flxSearchResult.isVisible = true;
            this.view.lblNoData.isVisible = false;
            this.view.flxSeg.isVisible = true;
            this.view.flxBck.isVisible = true;
            this.view.segSearch.isVisible = true;
            this.view.lblResult.isVisible = false;
        },
        //btnShowMore - for more questions
        showMoreQuestions: function() {
            this.view.flxBackAns.isVisible = true;
            this.view.flxServiceType.isVisible = false;
            this.view.flxServices.isVisible = false;
            this.view.flxSeg.isVisible = true;
            this.view.segSearch.isVisible = false;
            this.view.flxSearchResult.isVisible = false;
            this.view.flxQues.isVisible = true;
            var changeInterestQues = [];
            var loanQues = [];
            var paymentHolidayQues = [];
            if (this.gblType === "Change Interest") {
                changeInterestQues = showTableQues();
                this.view.segQues.setData(changeInterestQues);
            } else if (this.gblType === "Loan Payoff") {
                loanQues = showTableLoanQues();
                this.view.segQues.setData(loanQues);
            } else if (this.gblType === "Payment Holiday") {
                paymentHolidayQues = showTablePaymentQues();
                this.view.segQues.setData(paymentHolidayQues);
            } else {
                return;
            }
        },
        onClickBckAnswers: function() {
            this.view.flxBackAns.isVisible = false;
            this.view.flxQues.isVisible = false;
            this.view.flxSeg.isVisible = false;
            if (this.gblType === "Change Interest") {
                this.onClickBckAnswersReset();
                this.helpServiceType("btnLoan");
                this.selectedServiceType("Change Interest");
            } else if (this.gblType === "Loan Payoff") {
                this.onClickBckAnswersReset();
                this.helpServiceType("btnLoan");
                this.selectedServiceType("Loan Payoff");
            } else if (this.gblType === "Payment Holiday") {
                this.onClickBckAnswersReset();
                this.helpServiceType("btnLoan");
                this.selectedServiceType("Payment Holiday");
            } else {
                return;
            }
        },
        sectionClicked: function(context) {
            var tempMasterData = [];
            var index = context.sectionIndex;
            var tempData = this.view.seg1.data;
            if (this.gblType === "Change Interest") {
                tempMasterData = showTableChangeInterest();
            } else if (this.gblType === "Payment Holiday") {
                tempMasterData = showTablePaymentHoliday();
            } else {
                tempMasterData = showTableLoanPayOff();
            }
            if (tempData[index][0].lblDown === "M") {
                tempData[index][0].lblDown = "J";
                tempData[index][1] = [];
            } else {
                tempData[index][0].lblDown = "M";
                tempData[index][1] = tempMasterData[index][1];
            }
            this.view.seg1.setData(tempData);
        },
        btnClick: function(context) {
            if (context === "Change Interest") {
                this.onClickBckAnswersReset();
                this.helpServiceType("btnLoan");
                this.selectedServiceType(context);
            } else if (context === "Loan Payoff") {
                this.onClickBckAnswersReset();
                this.helpServiceType("btnLoan");
                this.selectedServiceType(context);
            } else if (context === "Payment Holiday") {
                this.onClickBckAnswersReset();
                this.helpServiceType("btnLoan");
                this.selectedServiceType(context);
            } else {
                return;
            }
        },
        onClickBckAnswersReset: function() {
            this.view.txtPartialText.text = "";
            this.view.flxSeg.isVisible = false;
            this.view.clearTxtBxSearch.isVisible = false;
            this.view.imgClearSearch.isVisible = false;
            this.view.btnClearTxt.isVisible = false;
            this.view.lblSearchIcon.isVisible = true;
        },
        quesSectionClicked: function(context) {
            var tempMasterData = [];
            var index = context.sectionIndex;
            var tempData = this.view.segQues.data;
            if (this.gblType === "Change Interest") {
                tempMasterData = showTableQues();
            } else if (this.gblType === "Payment Holiday") {
                tempMasterData = showTableLoanQues();
            } else {
                tempMasterData = showTablePaymentQues();
            }
            if (tempData[index][0].lblUp === "M") {
                tempData[index][0].lblUp = "J";
                tempData[index][1] = [];
            } else {
                tempData[index][0].lblUp = "M";
                tempData[index][1] = tempMasterData[index][1];
            }
            this.view.segQues.setData(tempData);
        },
        onClickBckShowMore: function() {
            this.view.flxDesc.isVisible = true;
            this.view.flxSeg.isVisible = false;
        },
        helpHome: function() {
            this.view.lblBreadBoardSplashValue1.isVisible = false;
            this.helpHomeReset();
            this.onClickBtnCloseHelpReset();
        },
        loanServicesHome: function() {
            this.view.lblBreadBoardSplashValue1.isVisible = false;
            this.onClickBtnCloseHelpReset();
        },
        onClickBtnCloseHelp: function() {
            this.view.isVisible = false;
            this.view.lblBreadBoardSplashValue1.isVisible = false;
            this.helpHomeReset();
            this.onClickBtnCloseHelpReset();
        },
        helpHomeReset: function() {
            this.view.flxBottom.isVisible = false;
            this.view.flxLoan.skin = "sknFlxTypeUnselect";
            this.view.lblBreadBoardSplashValue.isVisible = false;
            this.view.lblLoanServices.isVisible = false;
        },
        onClickBtnCloseHelpReset: function() {
            this.view.txtPartialText.text = "";
            this.view.lblPath.isVisible = false;
            this.view.flxSeg.isVisible = false;
            this.view.flxServiceType.isVisible = false;
            this.view.clearSearch.isVisible = false;
            this.view.flxServices.isVisible = true;
            this.view.lblSearchIcon.isVisible = true;
        },
        helpServiceType: function(serviceArr) {
            if (serviceArr === "btnLoan") {
                this.view.flxLoan.skin = "sknFlxTypeSelect"; //sknFlxTypeSelect, sknFlxTypeUnselect
                this.view.flxDeposits.skin = "sknFlxTypeUnselect";
                this.view.flxAccount.skin = "sknFlxTypeUnselect";
                this.view.flxBack.isVisible = true;
                this.view.lblBreadBoardSplashValue.isVisible = true;
                this.view.lblLoanServices.isVisible = true;
                this.view.lblLoanServices.text = "Loan Services";
                this.view.lblBreadBoardSplashValue1.isVisible = false;
                this.view.lblPath.isVisible = false;
                this.view.lblType.isVisible = true;
                this.view.lblType.text = "Loan Services";
                this.view.flxBottom.isVisible = true;
                this.selectedService = this.loanArr;
                this.view.btn1.text = this.loanArr[0];
                this.view.btn2.text = this.loanArr[1];
                this.view.btn3.text = this.loanArr[2];
                this.view.btn4.text = this.loanArr[3];
                this.view.btn5.text = this.loanArr[4];
                this.view.btn6.text = this.loanArr[5];
                this.view.btn7.text = this.loanArr[6];
                this.view.btn8.text = this.loanArr[7];
            } else if (serviceArr === "btnDeposit") {
                this.selectedService = this.depositsArr;
                this.view.lblBreadBoardSplashValue.isVisible = false;
                this.view.flxDeposits.skin = "sknFlxTypeSelect";
                this.view.flxLoan.skin = "sknFlxTypeUnselect";
                this.view.flxAccount.skin = "sknFlxTypeUnselect";
                this.view.lblLoanServices.text = "";
                this.view.lblType.isVisible = false;
                this.view.lblLoanServices.isVisible = false;
                this.view.lblType.text = "Deposit Services";
                this.view.flxBottom.isVisible = false;
            } else {
                this.selectedService = this.accountArr;
                this.view.lblBreadBoardSplashValue.isVisible = false;
                this.view.flxAccount.skin = "sknFlxTypeSelect";
                this.view.flxLoan.skin = "sknFlxTypeUnselect";
                this.view.flxDeposits.skin = "sknFlxTypeUnselect";
                this.view.lblType.isVisible = false;
                this.view.lblLoanServices.isVisible = false;
                this.view.lblLoanServices.text = "";
                this.view.lblType.text = "Account Services";
                this.view.flxBottom.isVisible = false;
            }
        },
        selectedServiceType: function(context) {
            switch (context) {
                case this.selectedService[0]:
                    {
                        this.setSegData(this.selectedService[0]);
                        break;
                    }
                case this.selectedService[1]:
                    {
                        this.setSegData(this.selectedService[1]);
                        break;
                    }
                case this.selectedService[2]:
                    {
                        this.setSegData(this.selectedService[2]);
                        break;
                    }
                case this.selectedService[3]:
                    {
                        this.setSegData(this.selectedService[3]);
                        break;
                    }
                case this.selectedService[4]:
                    {
                        this.setSegData(this.selectedService[4]);
                        break;
                    }
                case this.selectedService[5]:
                    {
                        this.setSegData(this.selectedService[5]);
                        break;
                    }
                case this.selectedService[6]:
                    {
                        this.setSegData(this.selectedService[6]);
                        break;
                    }
                case this.selectedService[7]:
                    {
                        this.setSegData(this.selectedService[7]);
                        break;
                    }
                default:
                    {
                        //console.log('Empty action received.');
                        break;
                    }
            }
        },
        loadSegData: function() {
            this.view.flxDesc.isVisible = true;
            this.view.lblNoData.isVisible = false;
            var x = this.view.segServiceTypes.selectedRowItems[0];
            const rowText = x.lblServiceType.text;
            this.setSegData(rowText);
        },
        selectedRowTxt: function(context) {
            this.setSegData(context);
        },
        setSegData: function(type) {
            this.view.flxServices.isVisible = false;
            this.view.flxServiceType.isVisible = true;
            this.view.flxBack.isVisible = true;
            this.view.lblBreadBoardSplashValue.isVisible = true;
            this.view.lblBreadBoardSplashValue1.isVisible = true;
            this.view.lblPath.isVisible = true;
            this.view.lblPath.text = " " + type;
            this.view.segServiceTypes.widgetDataMap = {
                flxIndicator: "flxIndicator",
                lblServiceType: "lblServiceType"
            };
            //btnsknSelectedType, sknSelectedType, btnsknUnSelectedSkin, sknUnSelectedSkin, flxUnSelectedSkin, flxSelectedSkin
            let loanData = [{
                lblServiceType: {
                    text: "Disbursement",
                    skin: "btnsknUnSelectedSkin"
                },
                flxIndicator: {
                    skin: "flxUnSelectedSkin",
                    isVisible: true
                },
                template: "flxServiceType"
            }, {
                lblServiceType: {
                    text: "Increase Commitment",
                    skin: "btnsknUnSelectedSkin"
                },
                flxIndicator: {
                    skin: "flxUnSelectedSkin",
                    isVisible: true
                },
                template: "flxServiceType"
            }, {
                lblServiceType: {
                    text: "Payment Holiday",
                    skin: "btnsknUnSelectedSkin"
                },
                flxIndicator: {
                    skin: "flxUnSelectedSkin",
                    isVisible: true
                },
                template: "flxServiceType"
            }, {
                lblServiceType: {
                    text: "Repayment",
                    skin: "btnsknUnSelectedSkin"
                },
                flxIndicator: {
                    skin: "flxUnSelectedSkin",
                    isVisible: true
                },
                template: "flxServiceType"
            }, {
                lblServiceType: {
                    text: "Change Interest",
                    skin: "btnsknUnSelectedSkin"
                },
                flxIndicator: {
                    skin: "flxUnSelectedSkin",
                    isVisible: true
                },
                template: "flxServiceType"
            }, {
                lblServiceType: {
                    text: "Loan Payoff",
                    skin: "btnsknUnSelectedSkin"
                },
                flxIndicator: {
                    skin: "flxUnSelectedSkin",
                    isVisible: true
                },
                template: "flxServiceType"
            }, {
                lblServiceType: {
                    text: "Personal Loan Creation",
                    skin: "btnsknUnSelectedSkin"
                },
                flxIndicator: {
                    skin: "flxUnSelectedSkin",
                    isVisible: true
                },
                template: "flxServiceType"
            }, {
                lblServiceType: {
                    text: "Mortgage Loan Creation",
                    skin: "btnsknUnSelectedSkin"
                },
                flxIndicator: {
                    skin: "flxUnSelectedSkin",
                    isVisible: true
                },
                template: "flxServiceType"
            }, ];
            this.gblType = type;
            loanData.forEach((txt) => {
                if (txt.lblServiceType.text === type) {
                    txt.lblServiceType.skin = "sknSelectedType";
                    txt.flxIndicator.skin = "flxSelectedSkin";
                }
            });
            this.view.segServiceTypes.setData(loanData);
            if (type === "Change Interest") {
                this.view.flxDesc.isVisible = true;
                this.view.lblNoData.isVisible = false;
                //this.playSelectedVideo(type);
                var tempMasterData = showTableChangeInterest();
                const y = tempMasterData.map((txt) => {
                    return txt.map((lbl) => {
                        return lbl.lblShelves;
                    });
                });
                const z = tempMasterData.map(([, txt]) => {
                    return txt[0].lblAddress;
                });
                this.view.lblDesc1.text = "How to Change Interest Rate";
                this.view.lblDescType1.text = y[0][0];
                this.view.rtx1.text = "The default interest rate type agreed in the loan contract is displayed on the screen. An option to choose either Fixed or Floating rate is provided to alter the interest rate type."; //z[0];
                this.view.lblDescType2.text = y[1][0];
                this.view.rtx2.text = "The tier type for a loan contract can be on a level, banded or single. Tier type is only displayed and cannot be edited. It is automatically picked up from the loan contract"; //z[1];
                this.view.lblDescType3.text = y[2][0];
                this.view.rtx3.text = "The effective date from which the interest rate changes are likely to be reflected is to be entered in this field. A calendar icon is provided to help choose the date which will then be replacing the current date."; //z[2];
                this.view.lblDescType4.text = y[3][0];
                this.view.rtx4.text = "Refers to the existing fixed interest rate of the loan. It can be edited and left blank if the loan is initially based on floating rate of interest."; //z[3];
                var tempChangeInterestQues = [];
                tempChangeInterestQues = showTableQues();
                const m = tempChangeInterestQues.map((txt) => {
                    return txt[0].lblQues;
                });
                const n = tempChangeInterestQues.map((txt) => {
                    return txt[1][0].rtxAnswer;
                });
                this.view.lblQues1.text = m[0];
                this.view.lblQues2.text = m[1];
                this.view.rtxAns1.text = n[0];
                this.view.rtxAns2.text = n[1];
                this.collapse();
            } else if (type === "Loan Payoff") {
                this.view.flxDesc.isVisible = true;
                this.view.lblNoData.isVisible = false;
                var tempMasterLoanData = showTableLoanPayOff();
                const y = tempMasterLoanData.map((txt) => {
                    return txt.map((lbl) => {
                        return lbl.lblShelves;
                    });
                });
                const z = tempMasterLoanData.map(([, txt]) => {
                    return txt[0].lblAddress;
                });
                this.view.lblDesc1.text = "How to Loan Payoff";
                this.view.lblDescType1.text = y[5][0];
                this.view.rtx1.text = "The PAYING ACCOUNT to be defaulted from the loan contractWhere more than one Paying account is mapped to the contract, the account number in the 1st Multi-value shall be displayed by default."; //z[0];  
                this.view.lblDescType2.text = y[6][0];
                this.view.rtx2.text = "Should be defaulted from the effective date selection. Cannot be edited";
                this.view.lblDescType3.text = y[4][0];
                this.view.rtx3.text = "The payment mode should allows you to make the payment via the following options: Account to account transfer - Based on selection, the amount must be transferred from the customer’s savings/current account to the loan account."; //z[5];
                this.view.lblDescType4.text = y[0][0];
                this.view.rtx4.text = "By default, it refers to the current date. It is a compulsory field and can be edited. Clicking this will enable a Calendar icon from where the date can be chosen."; //z[7];
                var tempLoanQues = [];
                tempLoanQues = showTableLoanQues();
                const m = tempLoanQues.map((txt) => {
                    return txt[0].lblQues;
                });
                const n = tempLoanQues.map((txt) => {
                    return txt[1][0].rtxAnswer;
                });
                this.view.lblQues1.text = m[0];
                this.view.lblQues2.text = m[1];
                this.view.rtxAns1.text = n[0];
                this.view.rtxAns2.text = n[1];
                this.collapse();
            } else if (type === "Payment Holiday") {
                this.view.flxDesc.isVisible = true;
                this.view.lblNoData.isVisible = false;
                var tempMasterPaymentData = showTablePaymentHoliday();
                const y = tempMasterPaymentData.map((txt) => {
                    return txt.map((lbl) => {
                        return lbl.lblShelves;
                    });
                });
                const z = tempMasterPaymentData.map(([, txt]) => {
                    return txt[0].lblAddress;
                });
                this.view.lblDesc1.text = "How to apply Payment Holiday";
                this.view.lblDescType1.text = y[0][0];
                this.view.rtx1.text = "Refers to the date from which payment holiday is to be started. You will be able to select dates from the pop-up that displays upcoming 6 repayments from the repayment schedule of the loan."; //z[0];
                this.view.lblDescType2.text = y[1][0];
                this.view.rtx2.text = "Account number of the loan/ Arrangement ID of the loan. This is not displayed in the screen but used for background processing."; //z[1];
                this.view.lblDescType3.text = y[2][0];
                this.view.rtx3.text = "You will be able to fill the new payment amount. As you check subsequent dates, the new payment amount will override the existing payments automatically in every row. It is not compulsory for availing the service."; //z[2];
                this.view.lblDescType4.text = y[3][0];
                this.view.rtx4.text = "A compulsory selection field wherein you can select one from the 2 options provided in the drop-down list. Term and Payment are the 2 options provided."; //z[3];
                var tempPaymentQues = [];
                tempPaymentQues = showTablePaymentQues();
                const m = tempPaymentQues.map((txt) => {
                    return txt[0].lblQues;
                });
                const n = tempPaymentQues.map((txt) => {
                    return txt[1][0].rtxAnswer;
                });
                this.view.lblQues1.text = m[0];
                this.view.lblQues2.text = m[1];
                this.view.rtxAns1.text = n[0];
                this.view.rtxAns2.text = n[1];
                this.collapse();
            } else {
                this.view.flxDesc.isVisible = false;
                this.view.lblNoData.isVisible = true;
            }
        },
        enableVideo: function() {
            this.view.flxMain.isVisible = false;
            this.view.flxPlayVideo.isVisible = true;
            this.view.rtxType.text = "Loan Services";
            this.view.lblVideoPath.text = this.gblType;
            this.playSelectedVideo(this.gblType);
        },
        playSelectedVideo: function(videoType) {
            var videoLink = "";
            var htmlStr = '';
            if (videoType === "Change Interest Rate") {
                videoLink = "";
                htmlStr = '<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;"><iframe src="' + videoLink + '" frameborder="0" allowfullscreen style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></iframe></div>';
                this.view.brw.htmlString = htmlStr;
            } else if (videoType === "Loan Payoff") {
                videoLink = "";
                htmlStr = '<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;"><iframe src="' + videoLink + '" frameborder="0" allowfullscreen style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></iframe></div>';
                this.view.brw.htmlString = htmlStr;
            } else if (videoType === "Payment Holiday") {
                videoLink = "https://bsgpoc.konylabs.net/downloads/PaymentHoliday.mp4";
                htmlStr = '<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;"><iframe src="' + videoLink + '" frameborder="0" allowfullscreen style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></iframe></div>';
                this.view.brw.htmlString = htmlStr;
            } else {
                videoLink = "";
                htmlStr = '<div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden;"><iframe src="' + videoLink + '" frameborder="0" allowfullscreen style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></iframe></div>';
                this.view.brw.htmlString = htmlStr;
            }
        },
        onClickBckVideo: function() {
            this.view.flxMain.isVisible = true;
            this.view.flxPlayVideo.isVisible = false;
        },
        expandFunc: function() {
            if (this.view.plus1.text === "G") {
                this.view.plus1.text = "p";
                this.view.plus2.text = "G";
                this.view.plus4.text = "G";
                this.view.plus3.text = "G";
                this.view.flxExpand1.isVisible = true;
                this.view.flxExpand2.isVisible = false;
                this.view.flxExpand3.isVisible = false;
                this.view.flxExpand4.isVisible = false;
                this.view.flxDesc2.height = "60%";
                this.view.flxLeft.height = "100%";
                this.view.flxDescType1.height = "15%";
                this.view.flxDescType2.height = "15%";
                this.view.flxExpand1.height = "45%";
                this.view.flxRight.height = "40%";
                this.view.flxDescType3.height = "42%";
                this.view.flxDescType4.height = "42%";
            } else {
                this.collapse();
            }
        },
        expandFunc2: function() {
            if (this.view.plus2.text === "G") {
                this.view.plus2.text = "p";
                this.view.plus1.text = "G";
                this.view.plus4.text = "G";
                this.view.plus3.text = "G";
                this.view.flxExpand2.isVisible = true;
                this.view.flxExpand1.isVisible = false;
                this.view.flxExpand3.isVisible = false;
                this.view.flxExpand4.isVisible = false;
                this.view.flxDesc2.height = "60%";
                this.view.flxLeft.height = "100%";
                this.view.flxDescType1.height = "15%";
                this.view.flxDescType2.height = "15%";
                this.view.flxExpand2.height = "36%";
                this.view.flxRight.height = "40%";
                this.view.flxDescType3.height = "42%";
                this.view.flxDescType4.height = "42%";
            } else {
                this.collapse();
            }
        },
        expandFunc3: function() {
            if (this.view.plus3.text === "G") {
                this.view.plus3.text = "p";
                this.view.plus1.text = "G";
                this.view.plus2.text = "G";
                this.view.plus4.text = "G";
                this.view.flxExpand3.isVisible = true;
                this.view.flxExpand4.isVisible = false;
                this.view.flxExpand2.isVisible = false;
                this.view.flxExpand1.isVisible = false;
                this.view.flxDesc2.height = "60%";
                this.view.flxLeft.height = "40%";
                this.view.flxDescType1.height = "42%";
                this.view.flxDescType2.height = "42%";
                this.view.flxExpand3.height = "48%";
                this.view.flxRight.height = "100%";
                this.view.flxDescType3.height = "15%";
                this.view.flxDescType4.height = "15%";
            } else {
                this.collapse();
            }
        },
        expandFunc4: function() {
            if (this.view.plus4.text === "G") {
                this.view.plus4.text = "p";
                this.view.plus3.text = "G";
                this.view.plus1.text = "G";
                this.view.plus2.text = "G";
                this.view.flxExpand4.isVisible = true;
                this.view.flxExpand3.isVisible = false;
                this.view.flxExpand2.isVisible = false;
                this.view.flxExpand1.isVisible = false;
                this.view.flxDesc2.height = "60%";
                this.view.flxLeft.height = "40%";
                this.view.flxDescType1.height = "42%";
                this.view.flxDescType2.height = "42%";
                this.view.flxExpand4.height = "37%";
                this.view.flxRight.height = "100%";
                this.view.flxDescType3.height = "15%";
                this.view.flxDescType4.height = "15%";
            } else {
                this.collapse();
            }
        },
        collapse: function() {
            this.view.plus1.text = "G";
            this.view.plus2.text = "G";
            this.view.plus4.text = "G";
            this.view.plus3.text = "G";
            this.view.flxExpand1.isVisible = false;
            this.view.flxExpand2.isVisible = false;
            this.view.flxExpand3.isVisible = false;
            this.view.flxExpand4.isVisible = false;
            this.view.flxDesc2.height = "22%";
            this.view.flxLeft.height = "100%";
            this.view.flxDescType1.height = "42%";
            this.view.flxDescType2.height = "42%";
            this.view.flxRight.height = "100%";
            this.view.flxDescType3.height = "42%";
            this.view.flxDescType4.height = "42%";
        },
        ques1: function() {
            if (this.view.collapse.text === "G") {
                this.view.flxDesc.scrollToEnd();
                this.view.collapse.text = "p";
                this.view.collapse1.text = "G";
                this.view.flxDesc3.height = "50%";
                this.view.flxFAQ.height = "100%";
                this.view.flxQues1.height = "25%";
                this.view.flxAns1.isVisible = true;
                this.view.flxAns2.isVisible = false;
                this.view.flxAns1.height = "35%";
                this.view.flxQues2.height = "25%";
                this.collapse();
            } else {
                this.view.collapse.text = "G";
                this.view.collapse1.text = "G";
                this.view.flxAns1.isVisible = false;
                this.view.flxAns2.isVisible = false;
                this.view.flxDesc3.height = "25%";
                this.view.flxFAQ.height = "100%";
                this.view.flxQues1.height = "44%";
                this.view.flxQues2.height = "44%";
            }
        },
        ques2: function() {
            if (this.view.collapse1.text === "G") {
                this.view.flxDesc.scrollToEnd();
                this.view.collapse1.text = "p";
                this.view.collapse.text = "G";
                this.view.flxDesc3.height = "50%";
                this.view.flxFAQ.height = "100%";
                this.view.flxQues1.height = "25%";
                this.view.flxAns2.isVisible = true;
                this.view.flxAns1.isVisible = false;
                this.view.flxAns2.height = "35%";
                this.view.flxQues2.height = "25%";
                this.collapse();
            } else {
                this.view.collapse.text = "G";
                this.view.collapse1.text = "G";
                this.view.flxAns1.isVisible = false;
                this.view.flxAns2.isVisible = false;
                this.view.flxDesc3.height = "25%";
                this.view.flxFAQ.height = "100%";
                this.view.flxQues1.height = "44%";
                this.view.flxQues2.height = "44%";
            }
        },
    };
});
define("com/konymp/help/helpControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_j1d35ca4ca3d4010a3897ab58d163b89: function AS_FlexContainer_j1d35ca4ca3d4010a3897ab58d163b89(eventobject) {
        var self = this;
        return self.onClickBckAnswers.call(this);
    },
    AS_Button_e4ea719cfadc4f008f35b898932b2808: function AS_Button_e4ea719cfadc4f008f35b898932b2808(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    AS_Button_e5c26499c6f243f585ceeb0565124ad8: function AS_Button_e5c26499c6f243f585ceeb0565124ad8(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    AS_Button_f44e332a93af4fad874a55484222a065: function AS_Button_f44e332a93af4fad874a55484222a065(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    AS_Button_af8b0070cab044c2a847ff5bf8a5ffbc: function AS_Button_af8b0070cab044c2a847ff5bf8a5ffbc(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    AS_Button_fd84a072e5794071b3d4481a0ffb4ef3: function AS_Button_fd84a072e5794071b3d4481a0ffb4ef3(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    AS_Button_d5e7355265e04a868c79b333c07ad831: function AS_Button_d5e7355265e04a868c79b333c07ad831(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    AS_Button_f366aa14dac14f8aac543153bab98f79: function AS_Button_f366aa14dac14f8aac543153bab98f79(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    AS_Button_j9534b4701d14de78630876d593c6806: function AS_Button_j9534b4701d14de78630876d593c6806(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    AS_Button_g07438ebeb6143949f2e5d43b002caf3: function AS_Button_g07438ebeb6143949f2e5d43b002caf3(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    AS_Button_d59462caa3844de598357c8c69b3cb69: function AS_Button_d59462caa3844de598357c8c69b3cb69(eventobject) {
        var self = this;
        return self.onClickBckShowMore.call(this);
    },
    AS_Button_hb78e04627a942f8b3cf4aabac4b8772: function AS_Button_hb78e04627a942f8b3cf4aabac4b8772(eventobject) {
        var self = this;
        return self.onClickBckVideo.call(this);
    },
    AS_Button_bdd5065cfdef4fdfa7fbc7fc7a53bcbc: function AS_Button_bdd5065cfdef4fdfa7fbc7fc7a53bcbc(eventobject) {
        var self = this;
        return self.clearTxtBxSearch.call(this);
    },
    AS_Button_c7621bafc21e49edb500dc952504f498: function AS_Button_c7621bafc21e49edb500dc952504f498(eventobject) {
        var self = this;
        return self.onClickBtnCloseHelp.call(this);
    },
    AS_Button_fe5de1f853c64277a5b6644bcff72b14: function AS_Button_fe5de1f853c64277a5b6644bcff72b14(eventobject) {
        var self = this;
        return self.onClickBckVideo.call(this);
    },
    AS_Button_d7b96f0d523e4992a759010fb2db87ba: function AS_Button_d7b96f0d523e4992a759010fb2db87ba(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    AS_Button_fb2ffec1480e4a91ab402dbc345ce18a: function AS_Button_fb2ffec1480e4a91ab402dbc345ce18a(eventobject) {
        var self = this;
        return self.helpHome.call(this);
    },
    AS_Button_a9960c3e36454247b50ad32401d3af17: function AS_Button_a9960c3e36454247b50ad32401d3af17(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    AS_Button_gd593ce330574612903e381494a26fe8: function AS_Button_gd593ce330574612903e381494a26fe8(eventobject) {
        var self = this;
        return self.expandFunc.call(this);
    },
    AS_Button_a61dda0d6ec3460eab26afb79ec07dd8: function AS_Button_a61dda0d6ec3460eab26afb79ec07dd8(eventobject) {
        var self = this;
        return self.expandFunc2.call(this);
    },
    AS_Button_e8ebbe79c01b4e9a85b59a3b86968214: function AS_Button_e8ebbe79c01b4e9a85b59a3b86968214(eventobject) {
        var self = this;
        return self.expandFunc3.call(this);
    },
    AS_Button_b050c26801874b91aa86dd60d1de7653: function AS_Button_b050c26801874b91aa86dd60d1de7653(eventobject) {
        var self = this;
        return self.expandFunc4.call(this);
    },
    AS_Button_eb7d3dbba998435e9040ecab21dbf2b6: function AS_Button_eb7d3dbba998435e9040ecab21dbf2b6(eventobject) {
        var self = this;
        return self.showMore.call(this);
    },
    AS_Button_b5a11f28e4e94e3f87e7addb97961710: function AS_Button_b5a11f28e4e94e3f87e7addb97961710(eventobject) {
        var self = this;
        return self.enableVideo.call(this);
    },
    AS_FlexContainer_cb20c973e91b4730806650c7d0361ea1: function AS_FlexContainer_cb20c973e91b4730806650c7d0361ea1(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    AS_FlexContainer_jab0b7944625463ab8d4906fc027d099: function AS_FlexContainer_jab0b7944625463ab8d4906fc027d099(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    AS_FlexContainer_f2df1befbb2c462ea205c7061ea7e186: function AS_FlexContainer_f2df1befbb2c462ea205c7061ea7e186(eventobject) {
        var self = this;
        return self.expandFunc4.call(this);
    },
    AS_Button_e4d554bce5874df89a1425170d266b94: function AS_Button_e4d554bce5874df89a1425170d266b94(eventobject) {
        var self = this;
        return self.loanServicesHome.call(this);
    },
    AS_TextField_dbdfcb90acd04036b190f4b10a2b8783: function AS_TextField_dbdfcb90acd04036b190f4b10a2b8783(eventobject) {
        var self = this;
        self.searchData.call(this);
    }
});
define("com/konymp/help/helpController", ["com/konymp/help/userhelpController", "com/konymp/help/helpControllerActions"], function() {
    var controller = require("com/konymp/help/userhelpController");
    var actions = require("com/konymp/help/helpControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
