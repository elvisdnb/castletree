define(function() {
    return function(controller) {
        var popupPayinPayoutCopy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "isMaster": true,
            "id": "popupPayinPayoutCopy",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox0a1af6ccf824346",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "popupPayinPayoutCopy"), extendConfig({}, controller.args[1], "popupPayinPayoutCopy"), extendConfig({}, controller.args[2], "popupPayinPayoutCopy"));
        popupPayinPayoutCopy.setDefaultUnit(kony.flex.DP);
        var FlexContainer0d563d2dda18c41 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 10,
            "centerX": "50%",
            "clipBounds": true,
            "height": "50dp",
            "id": "FlexContainer0d563d2dda18c41",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "90%"
        }, controller.args[0], "FlexContainer0d563d2dda18c41"), extendConfig({}, controller.args[1], "FlexContainer0d563d2dda18c41"), extendConfig({}, controller.args[2], "FlexContainer0d563d2dda18c41"));
        FlexContainer0d563d2dda18c41.setDefaultUnit(kony.flex.DP);
        var lblsearchHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblsearchHeading",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyCopyCopydefLabel1",
            "text": "Search for Account",
            "top": "5dp",
            "width": "89%"
        }, controller.args[0], "lblsearchHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblsearchHeading"), extendConfig({}, controller.args[2], "lblsearchHeading"));
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "top": "5dp",
            "width": "40dp"
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var imgClose = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "24dp",
            "id": "imgClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "ico_close_2.png",
            "top": "0dp",
            "width": "24dp",
            "zIndex": 1
        }, controller.args[0], "imgClose"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose"), extendConfig({}, controller.args[2], "imgClose"));
        flxPopUpClose.add(imgClose);
        FlexContainer0d563d2dda18c41.add(lblsearchHeading, flxPopUpClose);
        var flxInnerSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 10,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxInnerSearch",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "CopyflxsearchOuter",
            "top": "0",
            "width": "90%"
        }, controller.args[0], "flxInnerSearch"), extendConfig({}, controller.args[1], "flxInnerSearch"), extendConfig({}, controller.args[2], "flxInnerSearch"));
        flxInnerSearch.setDefaultUnit(kony.flex.DP);
        var FlexContainer0d49b88e8783340 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "centerX": "50%",
            "clipBounds": true,
            "focusSkin": "CopyCopyslFbox2",
            "height": "40dp",
            "id": "FlexContainer0d49b88e8783340",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox2",
            "top": "20dp",
            "width": "90%"
        }, controller.args[0], "FlexContainer0d49b88e8783340"), extendConfig({}, controller.args[1], "FlexContainer0d49b88e8783340"), extendConfig({}, controller.args[2], "FlexContainer0d49b88e8783340"));
        FlexContainer0d49b88e8783340.setDefaultUnit(kony.flex.DP);
        var txtSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "bottom": "10dp",
            "centerY": "50%",
            "focusSkin": "defTextBoxFocus",
            "height": "35dp",
            "id": "txtSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": 0,
            "placeholder": "Search by Customer ID Account Number and Currency",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "CopyCopydefTextBoxNormal",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "20dp",
            "width": "100%",
            "blur": {
                "enabled": false,
                "value": 0
            },
            "isSensitiveText": false
        }, controller.args[0], "txtSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchBox"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtSearchBox"));
        var imgSrc = new kony.ui.Image2(extendConfig({
            "centerY": "50.00%",
            "height": "40px",
            "id": "imgSrc",
            "isVisible": true,
            "left": "90%",
            "right": "5%",
            "skin": "slImage",
            "src": "search_2.png",
            "width": "60px",
            "zIndex": 2
        }, controller.args[0], "imgSrc"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSrc"), extendConfig({}, controller.args[2], "imgSrc"));
        FlexContainer0d49b88e8783340.add(txtSearchBox, imgSrc);
        var FlexContainer0e3c37c0e40204d = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "FlexContainer0e3c37c0e40204d",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "CopyCopyCopyslFbox",
            "top": "0",
            "width": "90%"
        }, controller.args[0], "FlexContainer0e3c37c0e40204d"), extendConfig({}, controller.args[1], "FlexContainer0e3c37c0e40204d"), extendConfig({}, controller.args[2], "FlexContainer0e3c37c0e40204d"));
        FlexContainer0e3c37c0e40204d.setDefaultUnit(kony.flex.DP);
        var flxsegheaderPayout = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxsegheaderPayout",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyCopyslFbox2",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxsegheaderPayout"), extendConfig({}, controller.args[1], "flxsegheaderPayout"), extendConfig({}, controller.args[2], "flxsegheaderPayout"));
        flxsegheaderPayout.setDefaultUnit(kony.flex.DP);
        var lblAccountIdhead = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAccountIdhead",
            "isVisible": true,
            "left": "1%",
            "skin": "CopyCopyCopydefLabel",
            "text": "Account Number",
            "top": "0",
            "width": "24%"
        }, controller.args[0], "lblAccountIdhead"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountIdhead"), extendConfig({}, controller.args[2], "lblAccountIdhead"));
        var lblCurreny = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCurreny",
            "isVisible": true,
            "left": "0",
            "skin": "CopyCopyCopydefLabel",
            "text": "CCY",
            "top": "0",
            "width": "9.60%"
        }, controller.args[0], "lblCurreny"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurreny"), extendConfig({}, controller.args[2], "lblCurreny"));
        var lblProductName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblProductName",
            "isVisible": true,
            "left": "0",
            "skin": "CopyCopyCopydefLabel",
            "text": "Product",
            "top": "0",
            "width": "28%"
        }, controller.args[0], "lblProductName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductName"), extendConfig({}, controller.args[2], "lblProductName"));
        var lblCustomerIdhead = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCustomerIdhead",
            "isVisible": true,
            "left": "0",
            "skin": "CopyCopyCopydefLabel",
            "text": "Cutomer ID",
            "top": "0",
            "width": "15%"
        }, controller.args[0], "lblCustomerIdhead"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerIdhead"), extendConfig({}, controller.args[2], "lblCustomerIdhead"));
        var lblBalance = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBalance",
            "isVisible": true,
            "left": "0",
            "skin": "CopyCopyCopydefLabel",
            "text": "Usable Balance",
            "top": "0",
            "width": "20%"
        }, controller.args[0], "lblBalance"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBalance"), extendConfig({}, controller.args[2], "lblBalance"));
        flxsegheaderPayout.add(lblAccountIdhead, lblCurreny, lblProductName, lblCustomerIdhead, lblBalance);
        var FlexContainer0h7f6ad36bea740 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "2dp",
            "id": "FlexContainer0h7f6ad36bea740",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "CopyCopyCopyslFbox1",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "FlexContainer0h7f6ad36bea740"), extendConfig({}, controller.args[1], "FlexContainer0h7f6ad36bea740"), extendConfig({}, controller.args[2], "FlexContainer0h7f6ad36bea740"));
        FlexContainer0h7f6ad36bea740.setDefaultUnit(kony.flex.DP);
        FlexContainer0h7f6ad36bea740.add();
        var segPayinPayout = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 20,
            "centerX": "50%",
            "data": [{
                "lblAccountId": "235242",
                "lblCurrency": "gertejreoite",
                "lblCustomerId": "424242",
                "lblProductId": "4234242424",
                "lblWorkingBal": "42342424"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }, {
                "lblAccountId": "Label",
                "lblCurrency": "Label",
                "lblCustomerId": "Label",
                "lblProductId": "-",
                "lblWorkingBal": "-"
            }],
            "groupCells": false,
            "height": "240dp",
            "id": "segPayinPayout",
            "isVisible": true,
            "left": "0",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "CopyCopyCopyseg",
            "rowTemplate": "CopyflxRowPayinPayout1",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "00509654",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "CopyflxRowPayinPayout1": "CopyflxRowPayinPayout1",
                "lblAccountId": "lblAccountId",
                "lblCurrency": "lblCurrency",
                "lblCustomerId": "lblCustomerId",
                "lblProductId": "lblProductId",
                "lblWorkingBal": "lblWorkingBal"
            },
            "width": "100%"
        }, controller.args[0], "segPayinPayout"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segPayinPayout"), extendConfig({}, controller.args[2], "segPayinPayout"));
        FlexContainer0e3c37c0e40204d.add(flxsegheaderPayout, FlexContainer0h7f6ad36bea740, segPayinPayout);
        flxInnerSearch.add(FlexContainer0d49b88e8783340, FlexContainer0e3c37c0e40204d);
        popupPayinPayoutCopy.add(FlexContainer0d563d2dda18c41, flxInnerSearch);
        popupPayinPayoutCopy.compInstData = {}
        return popupPayinPayoutCopy;
    }
})