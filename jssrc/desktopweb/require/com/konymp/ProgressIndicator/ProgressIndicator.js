define(function() {
    return function(controller) {
        var ProgressIndicator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "ProgressIndicator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "ProgressIndicator"), extendConfig({}, controller.args[1], "ProgressIndicator"), extendConfig({}, controller.args[2], "ProgressIndicator"));
        ProgressIndicator.setDefaultUnit(kony.flex.DP);
        var FlexContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "FlexContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "CopyslFbox0jd3033936fc240",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "FlexContainer"), extendConfig({}, controller.args[1], "FlexContainer"), extendConfig({}, controller.args[2], "FlexContainer"));
        FlexContainer.setDefaultUnit(kony.flex.DP);
        var FlexContainerPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "127dp",
            "id": "FlexContainerPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 0,
            "skin": "CopyslFbox0bb03d28bed1249",
            "width": "444dp",
            "zIndex": 1
        }, controller.args[0], "FlexContainerPopUp"), extendConfig({}, controller.args[1], "FlexContainerPopUp"), extendConfig({}, controller.args[2], "FlexContainerPopUp"));
        FlexContainerPopUp.setDefaultUnit(kony.flex.DP);
        var ImgProgressIndicator = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "id": "ImgProgressIndicator",
            "isVisible": true,
            "left": "43dp",
            "right": "37dp",
            "skin": "CopyslImage0g67aa99637504d",
            "src": "progress_indicator.gif",
            "top": "70dp",
            "width": "400dp",
            "zIndex": 1
        }, controller.args[0], "ImgProgressIndicator"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "ImgProgressIndicator"), extendConfig({}, controller.args[2], "ImgProgressIndicator"));
        var LblProgressIndicator = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "LblProgressIndicator",
            "isVisible": true,
            "left": "167dp",
            "skin": "CopydefLabel0j49d82df567443",
            "text": "Progress Indicator..",
            "textStyle": {},
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "LblProgressIndicator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "LblProgressIndicator"), extendConfig({}, controller.args[2], "LblProgressIndicator"));
        var ImageClose = new kony.ui.Image2(extendConfig({
            "height": "10dp",
            "id": "ImageClose",
            "isVisible": true,
            "onTouchStart": controller.AS_Image_aef243a1ac1b4348b174203156b5d486,
            "right": "9dp",
            "skin": "slImage",
            "src": "cross_1_4.png",
            "top": "10dp",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "ImageClose"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "ImageClose"), extendConfig({}, controller.args[2], "ImageClose"));
        FlexContainerPopUp.add(ImgProgressIndicator, LblProgressIndicator, ImageClose);
        FlexContainer.add(FlexContainerPopUp);
        ProgressIndicator.add(FlexContainer);
        ProgressIndicator.compInstData = {}
        return ProgressIndicator;
    }
})