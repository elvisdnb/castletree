define("com/konymp/ProgressIndicator/userProgressIndicatorController", function() {
    return {};
});
define("com/konymp/ProgressIndicator/ProgressIndicatorControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Image_aef243a1ac1b4348b174203156b5d486: function AS_Image_aef243a1ac1b4348b174203156b5d486(eventobject, x, y) {
        var self = this;
        this.view.FlexContainer.isVisible = false;
        this.view.forceLayout();
    }
});
define("com/konymp/ProgressIndicator/ProgressIndicatorController", ["com/konymp/ProgressIndicator/userProgressIndicatorController", "com/konymp/ProgressIndicator/ProgressIndicatorControllerActions"], function() {
    var controller = require("com/konymp/ProgressIndicator/userProgressIndicatorController");
    var actions = require("com/konymp/ProgressIndicator/ProgressIndicatorControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
