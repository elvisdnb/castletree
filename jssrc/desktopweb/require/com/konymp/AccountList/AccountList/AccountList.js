define(function() {
    return function(controller) {
        var AccountList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "isMaster": true,
            "height": "240dp",
            "id": "AccountList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "postShow": controller.AS_FlexContainer_g3e2bcf671df48bb826003c9235acd22,
            "skin": "sknflxWhiteBGBorder0075db",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "AccountList"), extendConfig({}, controller.args[1], "AccountList"), extendConfig({}, controller.args[2], "AccountList"));
        AccountList.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBGF2F7FDBorder",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var lblAccNo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAccNo",
            "isVisible": true,
            "left": "1%",
            "skin": "CopydefLabel0gbe086184e6d4d",
            "text": "Account No",
            "top": "0",
            "width": "25.30%"
        }, controller.args[0], "lblAccNo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccNo"), extendConfig({}, controller.args[2], "lblAccNo"));
        var lblType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblType",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0gbe086184e6d4d",
            "text": "Account Type",
            "top": "0dp",
            "width": "35.30%"
        }, controller.args[0], "lblType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType"), extendConfig({}, controller.args[2], "lblType"));
        var lblBalance = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBalance",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0gbe086184e6d4d",
            "text": "Balance",
            "top": "0dp",
            "width": "38.30%"
        }, controller.args[0], "lblBalance"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBalance"), extendConfig({}, controller.args[2], "lblBalance"));
        flxHeader.add(lblAccNo, lblType, lblBalance);
        var segAccountList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }],
            "groupCells": false,
            "height": "195dp",
            "id": "segAccountList",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "Copyseg0fb7bc96dc2464b",
            "rowTemplate": "FlxAccountList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": true,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": "40dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "FlxAccountList": "FlxAccountList",
                "lblAccountId": "lblAccountId",
                "lblWorkingBal": "lblWorkingBal",
                "lbltype": "lbltype"
            },
            "width": "100%"
        }, controller.args[0], "segAccountList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAccountList"), extendConfig({}, controller.args[2], "segAccountList"));
        var lblNoAccounts = new kony.ui.Label(extendConfig({
            "height": "195dp",
            "id": "lblNoAccounts",
            "isVisible": false,
            "left": "0",
            "skin": "CopydefLabel0gbe086184e6d4d",
            "text": "No Accounts Found",
            "top": 40,
            "width": "100%"
        }, controller.args[0], "lblNoAccounts"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoAccounts"), extendConfig({}, controller.args[2], "lblNoAccounts"));
        AccountList.add(flxHeader, segAccountList, lblNoAccounts);
        AccountList.compInstData = {}
        return AccountList;
    }
})