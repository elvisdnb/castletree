define("com/konymp/getStarted/usergetStartedController", function() {
    return {
        closeHelp: function() {
            kony.store.setItem("demoFlag", "1");
            this.view.isVisible = false;
        },
        btnStart: function() {
            if (this.view.lblHi.text === "Hi There") {
                this.view.lblHi.text = "Explore";
                this.view.lbl2.text = "Take an interactive tour of all things lending from origination to servicing";
                this.view.btnStart.text = "Next";
                this.view.img1.src = "start1.png";
                this.view.CircleDark2.skin = "sknIndicatorSelected";
                this.view.CircleDark1.skin = "sknIndicatorUnselect";
                this.view.CircleDark3.skin = "sknIndicatorUnselect";
                this.view.flxInidcator1.isVisible = true;
                this.view.flxInidcator1.bottom = "175dp";
            } else if (this.view.lblHi.text === "Explore") {
                this.view.lblHi.text = "Help Center";
                this.view.lbl2.text = "Lost your way? Your go to place for all information";
                this.view.btnStart.text = "Get Started";
                this.view.btnStart.isVisible = false;
                this.view.btnStart1.isVisible = true;
                this.view.btnStart1.text = "Get Started";
                this.view.forceLayout();
                this.view.img1.src = "start0.png";
                this.view.flxInidcator1.isVisible = true;
                this.view.flxInidcator1.bottom = "125dp";
                this.view.CircleDark3.skin = "sknIndicatorSelected";
                this.view.CircleDark1.skin = "sknIndicatorUnselect";
                this.view.CircleDark2.skin = "sknIndicatorUnselect";
            } else {
                return;
            }
            //         if(this.view.btnStart.text === "Get Started"){
            //           this.view.isVisible = false;
            //         }else{
            //           this.view.isVisible = true;
            //         }
        }
    };
});
define("com/konymp/getStarted/getStartedControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_gb211ccc462d48ee942a2bf2659d5a28: function AS_Button_gb211ccc462d48ee942a2bf2659d5a28(eventobject) {
        var self = this;
        return self.closeHelp.call(this);
    },
    AS_Button_ca5647f3ac24432caf1db8b764acd1e3: function AS_Button_ca5647f3ac24432caf1db8b764acd1e3(eventobject) {
        var self = this;
        return self.btnStart.call(this);
    },
    AS_Button_g1a747a124684ca2b9c14fa374143a38: function AS_Button_g1a747a124684ca2b9c14fa374143a38(eventobject) {
        var self = this;
        kony.store.setItem("demoFlag", "1");
        this.view.isVisible = false;
    },
    AS_FlexContainer_ee0e875e848244fd927cde18fce984a0: function AS_FlexContainer_ee0e875e848244fd927cde18fce984a0(eventobject) {
        var self = this;
        kony.store.setItem("demoFlag", "1");
        this.view.isVisible = false;
    }
});
define("com/konymp/getStarted/getStartedController", ["com/konymp/getStarted/usergetStartedController", "com/konymp/getStarted/getStartedControllerActions"], function() {
    var controller = require("com/konymp/getStarted/usergetStartedController");
    var actions = require("com/konymp/getStarted/getStartedControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
