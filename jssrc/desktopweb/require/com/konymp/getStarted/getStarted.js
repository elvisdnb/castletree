define(function() {
    return function(controller) {
        var getStarted = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "isMaster": true,
            "height": "100%",
            "id": "getStarted",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ee0e875e848244fd927cde18fce984a0,
            "skin": "CopyslFbox0f4260580003b4d",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "getStarted"), extendConfig({}, controller.args[1], "getStarted"), extendConfig({}, controller.args[2], "getStarted"));
        getStarted.setDefaultUnit(kony.flex.DP);
        var flxMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "65%",
            "id": "flxMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "CopysknDashboardFlex0e5d36d45339e4e",
            "width": "22%",
            "zIndex": 20
        }, controller.args[0], "flxMain"), extendConfig({}, controller.args[1], "flxMain"), extendConfig({}, controller.args[2], "flxMain"));
        flxMain.setDefaultUnit(kony.flex.DP);
        var flxClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "7%",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "2%",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxClose"), extendConfig({}, controller.args[1], "flxClose"), extendConfig({}, controller.args[2], "flxClose"));
        flxClose.setDefaultUnit(kony.flex.DP);
        var lblClose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblClose",
            "isVisible": true,
            "skin": "CopydefLabel0h4ded597e2664d",
            "text": "L",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose"), extendConfig({}, controller.args[2], "lblClose"));
        var btnCloseGetStarted = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0e8e17a750b904c",
            "height": "100%",
            "id": "btnCloseGetStarted",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_Button_gb211ccc462d48ee942a2bf2659d5a28,
            "skin": "CopydefBtnNormal0e8e17a750b904c",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnCloseGetStarted"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCloseGetStarted"), extendConfig({}, controller.args[2], "btnCloseGetStarted"));
        flxClose.add(lblClose, btnCloseGetStarted);
        var img1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "height": "155dp",
            "id": "img1",
            "isVisible": true,
            "skin": "slImage",
            "src": "start3.png",
            "top": "11%",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "img1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "img1"), extendConfig({}, controller.args[2], "img1"));
        var lblHi = new kony.ui.Label(extendConfig({
            "id": "lblHi",
            "isVisible": true,
            "left": "38%",
            "skin": "CopydefLabel0gef564804eb542",
            "text": "Hi There",
            "top": "53%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHi"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHi"), extendConfig({}, controller.args[2], "lblHi"));
        var lbl2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lbl2",
            "isVisible": true,
            "skin": "CopydefLabel0b6962196437342",
            "text": "Welcome to Temenos Retail Lending Experience",
            "top": "63%",
            "width": "84%",
            "zIndex": 1
        }, controller.args[0], "lbl2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl2"), extendConfig({}, controller.args[2], "lbl2"));
        var flxIndicators = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50.00%",
            "clipBounds": true,
            "height": "5%",
            "id": "flxIndicators",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "78%",
            "width": "23%",
            "zIndex": 1
        }, controller.args[0], "flxIndicators"), extendConfig({}, controller.args[1], "flxIndicators"), extendConfig({}, controller.args[2], "flxIndicators"));
        flxIndicators.setDefaultUnit(kony.flex.DP);
        var CircleDark1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "CircleDark1",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIndicatorSelected",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "CircleDark1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "CircleDark1"), extendConfig({}, controller.args[2], "CircleDark1"));
        var CircleDark2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "CircleDark2",
            "isVisible": true,
            "skin": "sknIndicatorUnselect",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "CircleDark2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "CircleDark2"), extendConfig({}, controller.args[2], "CircleDark2"));
        var CircleDark3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "CircleDark3",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknIndicatorUnselect",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "CircleDark3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "CircleDark3"), extendConfig({}, controller.args[2], "CircleDark3"));
        flxIndicators.add(CircleDark1, CircleDark2, CircleDark3);
        var flxBtn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "2%",
            "clipBounds": true,
            "height": "12%",
            "id": "flxBtn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "flxBtn"), extendConfig({}, controller.args[1], "flxBtn"), extendConfig({}, controller.args[2], "flxBtn"));
        flxBtn.setDefaultUnit(kony.flex.DP);
        var btnStart = new kony.ui.Button(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0ed300015681d46",
            "height": "34dp",
            "id": "btnStart",
            "isVisible": true,
            "onClick": controller.AS_Button_ca5647f3ac24432caf1db8b764acd1e3,
            "skin": "CopydefBtnNormal0ed300015681d46",
            "text": "Next",
            "width": "63%",
            "zIndex": 1
        }, controller.args[0], "btnStart"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnStart"), extendConfig({}, controller.args[2], "btnStart"));
        var btnStart1 = new kony.ui.Button(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0ed300015681d46",
            "height": "34dp",
            "id": "btnStart1",
            "isVisible": false,
            "onClick": controller.AS_Button_g1a747a124684ca2b9c14fa374143a38,
            "skin": "CopydefBtnNormal0ed300015681d46",
            "text": "Get Started",
            "width": "63%",
            "zIndex": 1
        }, controller.args[0], "btnStart1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnStart1"), extendConfig({}, controller.args[2], "btnStart1"));
        flxBtn.add(btnStart, btnStart1);
        flxMain.add(flxClose, img1, lblHi, lbl2, flxIndicators, flxBtn);
        var flxInidcator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "176dp",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxInidcator1",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "50dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0fe3a3f8ea9f047",
            "width": "50dp",
            "zIndex": 20
        }, controller.args[0], "flxInidcator1"), extendConfig({}, controller.args[1], "flxInidcator1"), extendConfig({}, controller.args[2], "flxInidcator1"));
        flxInidcator1.setDefaultUnit(kony.flex.DP);
        var line1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "2dp",
            "id": "line1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0f178c9eb553248",
            "width": "70%",
            "zIndex": 1
        }, controller.args[0], "line1"), extendConfig({}, controller.args[1], "line1"), extendConfig({}, controller.args[2], "line1"));
        line1.setDefaultUnit(kony.flex.DP);
        line1.add();
        var CircleLight = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "CircleLight",
            "isVisible": true,
            "right": "0dp",
            "skin": "CopyslFontAwesomeIcon0ac916273b65d41",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "CircleLight"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "CircleLight"), extendConfig({}, controller.args[2], "CircleLight"));
        flxInidcator1.add(line1, CircleLight);
        getStarted.add(flxMain, flxInidcator1);
        getStarted.compInstData = {}
        return getStarted;
    }
})