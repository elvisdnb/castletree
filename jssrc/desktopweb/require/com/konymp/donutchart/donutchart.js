define(function() {
    return function(controller) {
        var donutchart = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "donutchart",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxMainDonut",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "donutchart"), extendConfig({}, controller.args[1], "donutchart"), extendConfig({}, controller.args[2], "donutchart"));
        donutchart.setDefaultUnit(kony.flex.DP);
        var doughnutBrowser = new kony.ui.Browser(extendConfig({
            "centerX": "50%",
            "detectTelNumber": false,
            "enableNativeCommunication": true,
            "enableZoom": false,
            "height": "100%",
            "id": "doughnutBrowser",
            "isVisible": true,
            "left": "0dp",
            "setAsContent": false,
            "requestURLConfig": {
                "URL": "Chart_donut/donut.html",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "doughnutBrowser"), extendConfig({}, controller.args[1], "doughnutBrowser"), extendConfig({}, controller.args[2], "doughnutBrowser"));
        donutchart.add(doughnutBrowser);
        donutchart.compInstData = {}
        return donutchart;
    }
})