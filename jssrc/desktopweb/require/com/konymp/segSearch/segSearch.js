define(function() {
    return function(controller) {
        var segSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "segSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1200, 1920]
        }, controller.args[0], "segSearch"), extendConfig({}, controller.args[1], "segSearch"), extendConfig({}, controller.args[2], "segSearch"));
        segSearch.setDefaultUnit(kony.flex.DP);
        var seg1 = new kony.ui.SegmentedUI2(extendConfig({
            "data": [
                [{
                        "imgLocation": "icons_map_2.png",
                        "lblDown": "M",
                        "lblShelves": "Kony "
                    },
                    [{
                        "lblAddress": "RichText",
                        "lblAddress1": "19909 120TH AVE NE",
                        "lblLine": "Label"
                    }, {
                        "lblAddress": "RichText",
                        "lblAddress1": "19909 120TH AVE NE",
                        "lblLine": "Label"
                    }]
                ],
                [{
                        "imgLocation": "icons_map_2.png",
                        "lblDown": "M",
                        "lblShelves": "Kony "
                    },
                    [{
                        "lblAddress": "RichText",
                        "lblAddress1": "19909 120TH AVE NE",
                        "lblLine": "Label"
                    }, {
                        "lblAddress": "RichText",
                        "lblAddress1": "19909 120TH AVE NE",
                        "lblLine": "Label"
                    }]
                ]
            ],
            "groupCells": false,
            "height": "65%",
            "id": "seg1",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "CopyCopyseg1",
            "rowTemplate": "CopyflxParent",
            "sectionHeaderSkin": "CopysliPhoneSegmentHeader0b690653eba5a43",
            "sectionHeaderTemplate": "CopyflxDown",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "CopyflxDown": "CopyflxDown",
                "CopyflxParent": "CopyflxParent",
                "FlexContainer0c2ca7dc5c88140": "FlexContainer0c2ca7dc5c88140",
                "FlexGroup0d7f2ca3295ba45": "FlexGroup0d7f2ca3295ba45",
                "btnUpArrow": "btnUpArrow",
                "flxLine": "flxLine",
                "flxRow": "flxRow",
                "imgLocation": "imgLocation",
                "lblAddress": "lblAddress",
                "lblAddress1": "lblAddress1",
                "lblDown": "lblDown",
                "lblLine": "lblLine",
                "lblShelves": "lblShelves",
                "lblTitle": "lblTitle"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "seg1"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "seg1"), extendConfig({}, controller.args[2], "seg1"));
        segSearch.add(seg1);
        segSearch.breakpointResetData = {};
        segSearch.breakpointData = {
            maxBreakpointWidth: 1920,
            "640": {
                "seg1": {
                    "height": {
                        "type": "string",
                        "value": "100%"
                    },
                    "top": {
                        "type": "string",
                        "value": "0dp"
                    },
                    "segmentProps": []
                }
            },
            "1200": {
                "seg1": {
                    "height": {
                        "type": "string",
                        "value": "100%"
                    },
                    "top": {
                        "type": "string",
                        "value": "24%"
                    },
                    "segmentProps": []
                }
            },
            "1920": {
                "seg1": {
                    "height": {
                        "type": "string",
                        "value": "100%"
                    },
                    "top": {
                        "type": "string",
                        "value": "0dp"
                    },
                    "segmentProps": []
                }
            }
        }
        segSearch.compInstData = {}
        return segSearch;
    }
})