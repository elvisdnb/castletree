/**
 * Created by Team Kony.
 * Copyright (c) 2017 Kony Inc. All rights reserved.
 */
/**
 * @controller: Dual Line Chart UDW
 * @author: Tejaswini Tubati and Sumeet Bartha
 * @category: Reusable Component
 * @componentVersion: 1.0
 * @description: Generates dual line chart by taking the required parameters as input
 */
define("com/konymp/multiline/usermultilineController", function() {
    var konyLoggerModule = require('com/konymp/multiline/konyLogger');
    konymp = {};
    konymp.logger = new konyLoggerModule("Multi Line Chart Component");
    return {
        /**
         * @function constructor
         * @private
         * @params {Object} baseConfig, layoutConfig, pspConfig
         */
        constructor: function(baseConfig, layoutConfig, pspConfig) {
            var analytics = require("com/konymp/" + "multiline" + "/analytics");
            analytics.notifyAnalytics();
            konymp.logger.trace("----------Entering constructor---------", konymp.logger.FUNCTION_ENTRY);
            this._chartProperties = {
                _lowValue: "500",
                _highValue: "2500",
                _bgColor: "#314ebc",
                _enableChartAnimation: true,
                _xAxisTitle: "",
                _yAxisTitle: "",
                _enableGrid: true,
                _enableGridAnimation: false,
                _enableLegends: true,
                _legendFontSize: "95%",
                _legendFontColor: "#ffffff",
                _titleFontSize: "12",
                _titleFontColor: "#ffffff",
                _enableStaticPreview: true
            };
            this._chartTitle = "";
            this._chartData = [];
            this._lineDetails = [];
            chart_multiLine_defined_global = function(state) {
                if (state === 'ready') {
                    this.showGridChart();
                }
            }.bind(this);
            konymp.logger.trace("----------Exiting constructor ---------", konymp.logger.FUNCTION_EXIT);
        },
        /**
         * @function initGetterSetters
         * @private
         * @description: Logic for getters/setters of custom properties
         */
        initGettersSetters: function() {
            this.hexCodeFormat = /^(#)?([0-9a-fA-F]{3})([0-9a-fA-F]{3})?$/;
            this.legendFontSizeFormat = /[0-9]*[0-9]{2}(%)/;
            konymp.logger.trace("----------Entering initGettersSetters Function---------", konymp.logger.FUNCTION_ENTRY);
            defineSetter(this, "chartTitle", function(val) {
                konymp.logger.trace("----------Entering chartTitle Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartTitle = val;
                konymp.logger.trace("----------Exiting chartTitle Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "xAxisTitle", function(val) {
                konymp.logger.trace("----------Entering xAxisTitle Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartProperties._xAxisTitle = val;
                konymp.logger.trace("----------Exiting xAxisTitle Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "yAxisTitle", function(val) {
                konymp.logger.trace("----------Entering yAxisTitle Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartProperties._yAxisTitle = val;
                konymp.logger.trace("----------Exiting yAxisTitle Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "titleFontColor", function(val) {
                konymp.logger.trace("----------Entering titleFontColor Setter---------", konymp.logger.FUNCTION_ENTRY);
                try {
                    if (this.hexCodeFormat.test(val)) {
                        this._chartProperties._titleFontColor = val;
                    } else {
                        throw {
                            "Error": "InvalidTitleFontColorCode",
                            "message": "The color code must be in hex format. Eg.:#000000"
                        };
                    }
                } catch (exception) {
                    konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                    if (exception.Error === "InvalidTitleFontColorCode") {
                        throw (exception);
                    }
                }
                konymp.logger.trace("----------Exiting titleFontColor Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "titleFontSize", function(val) {
                konymp.logger.trace("----------Entering titleFontSize Setter---------", konymp.logger.FUNCTION_ENTRY);
                try {
                    if (!isNaN(parseInt(val))) {
                        this._chartProperties._titleFontSize = val;
                    } else {
                        throw {
                            "Error": "NotNumber",
                            "message": "Title font size value should be a number"
                        };
                    }
                } catch (exception) {
                    konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                    if (exception.Error === "NotNumber") {
                        throw (exception);
                    }
                }
                konymp.logger.trace("----------Exiting titleFontSize Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "enableLegends", function(val) {
                konymp.logger.trace("----------Entering enableLegends Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartProperties._enableLegends = val;
                konymp.logger.trace("----------Exiting enableLegends Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "legendFontSize", function(val) {
                konymp.logger.trace("----------Entering legendFontSize Setter---------", konymp.logger.FUNCTION_ENTRY);
                try {
                    if (this.legendFontSizeFormat.test(val)) {
                        this._chartProperties._legendFontSize = val;
                    } else {
                        throw {
                            "Error": "WrongFormat",
                            "message": "Legend font size value should be a number followed by %"
                        };
                    }
                } catch (exception) {
                    konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                    if (exception.Error === "WrongFormat") {
                        throw (exception);
                    }
                }
                konymp.logger.trace("----------Exiting legendFontSize Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "legendFontColor", function(val) {
                konymp.logger.trace("----------Entering legendFontColor Setter---------", konymp.logger.FUNCTION_ENTRY);
                try {
                    if (this.hexCodeFormat.test(val)) {
                        this._chartProperties._legendFontColor = val;
                    } else {
                        throw {
                            "Error": "InvalidLegendFontColorCode",
                            "message": "The color code must be in hex format. Eg.:#000000"
                        };
                    }
                } catch (exception) {
                    konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                    if (exception.Error === "InvalidLegendFontColorCode") {
                        throw (exception);
                    }
                }
                konymp.logger.trace("----------Exiting legendFontColor Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "bgColor", function(val) {
                konymp.logger.trace("----------Entering backgroundColor Setter---------", konymp.logger.FUNCTION_ENTRY);
                try {
                    if (this.hexCodeFormat.test(val)) {
                        this._chartProperties._bgColor = val;
                    } else {
                        throw {
                            "Error": "InvalidBackgoundColorCode",
                            "message": "The color code must be in hex format. Eg.:#000000"
                        };
                    }
                } catch (exception) {
                    konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                    if (exception.Error === "InvalidBackgoundColorCode") {
                        throw (exception);
                    }
                }
                konymp.logger.trace("----------Exiting backgroundColor Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "enableGrid", function(val) {
                konymp.logger.trace("----------Entering enableGrid Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartProperties._enableGrid = val;
                konymp.logger.trace("----------Exiting enableGrid Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "enableGridAnimation", function(val) {
                konymp.logger.trace("----------Entering enableGridAnimation Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartProperties._enableGridAnimation = val;
                konymp.logger.trace("----------Exiting enableGridAnimation Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "enableChartAnimation", function(val) {
                konymp.logger.trace("----------Entering enableChartAnimation Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartProperties._enableChartAnimation = val;
                konymp.logger.trace("----------Exiting enableChartAnimation Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "lowValue", function(val) {
                konymp.logger.trace("----------Entering lowValue Setter---------", konymp.logger.FUNCTION_ENTRY);
                try {
                    if (!isNaN(parseInt(val))) {
                        this._chartProperties._lowValue = val;
                    } else {
                        throw {
                            "Error": "NotNumber",
                            "message": "Low/High value should be a number"
                        };
                    }
                } catch (exception) {
                    konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                    if (exception.Error === "NotNumber") {
                        throw (exception);
                    }
                }
                konymp.logger.trace("----------Exiting lowValue Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "highValue", function(val) {
                konymp.logger.trace("----------Entering highValue Setter---------", konymp.logger.FUNCTION_ENTRY);
                try {
                    if (!isNaN(parseInt(val))) {
                        this._chartProperties._highValue = val;
                    } else {
                        throw {
                            "Error": "NotNumber",
                            "message": "Low/High value should be a number"
                        };
                    }
                } catch (exception) {
                    konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                    if (exception.Error === "NotNumber") {
                        throw (exception);
                    }
                }
                konymp.logger.trace("----------Exiting highValue Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "lineDetails", function(val) {
                konymp.logger.trace("----------Entering lineDetails Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._lineDetails = val.data;
                konymp.logger.trace("----------Exiting lineDetails Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "chartData", function(val) {
                konymp.logger.trace("----------Entering chartData Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartData = val.data;
                konymp.logger.trace("----------Exiting chartData Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            defineSetter(this, "enableStaticPreview", function(val) {
                konymp.logger.trace("----------Entering enableStaticPreview Setter---------", konymp.logger.FUNCTION_ENTRY);
                this._chartProperties._enableStaticPreview = val;
                konymp.logger.trace("----------Exiting enableStaticPreview Setter---------", konymp.logger.FUNCTION_EXIT);
            });
            this.view.lineChartBrowser.onPageFinished = this.showGridChart.bind(this);
            konymp.logger.trace("----------Exiting initGettersSetters Function---------", konymp.logger.FUNCTION_EXIT);
        },
        /**
         * @function createChart
         * @access exposed to user
         * @param {JSON} dataSet, lineDetails
         * @description: generates line chart by taking the data and the other params as input
         */
        createChart: function(dataSet, lineDetails) {
            konymp.logger.trace("----------Entering createChart Function---------", konymp.logger.FUNCTION_ENTRY);
            try {
                var labels, series = [],
                    colors = [],
                    ds1, ds2, ds3, ds4, ds5;
                if (dataSet !== null && dataSet !== "" && dataSet !== undefined && dataSet.length !== 0 && lineDetails !== null && lineDetails !== undefined && lineDetails !== "" && lineDetails.length !== 0) {
                    if (dataSet[0].label !== "") {
                        labels = dataSet.map(function(Obj) {
                            return Obj.label;
                        });
                    }
                    if (typeof dataSet[0].dataPoint1 !== "undefined" && dataSet[0].dataPoint1 !== "" && lineDetails[0].color !== "undefined" && lineDetails[0].legendName !== "undefined" && lineDetails[0].color !== "" && lineDetails[0].legendName !== "") {
                        ds1 = dataSet.map(function(Obj) {
                            return Obj.dataPoint1;
                        });
                        ds1 = {
                            name: lineDetails[0].legendName,
                            data: ds1
                        };
                        series.push(ds1);
                        colors.push(lineDetails[0].color);
                    }
                    if (typeof dataSet[0].dataPoint2 !== "undefined" && dataSet[0].dataPoint2 !== "" && lineDetails[1].color !== "undefined" && lineDetails[1].legendName !== "undefined" && lineDetails[1].color !== "" && lineDetails[1].legendName !== "") {
                        ds2 = dataSet.map(function(Obj) {
                            return Obj.dataPoint2;
                        });
                        ds2 = {
                            name: lineDetails[1].legendName,
                            data: ds2
                        };
                        series.push(ds2);
                        colors.push(lineDetails[1].color);
                    }
                    if (typeof dataSet[0].dataPoint3 !== "undefined" && dataSet[0].dataPoint3 !== "" && lineDetails[2].color !== "undefined" && lineDetails[2].legendName !== "undefined" && lineDetails[2].color !== "" && lineDetails[2].legendName !== "") {
                        ds3 = dataSet.map(function(Obj) {
                            return Obj.dataPoint3;
                        });
                        ds3 = {
                            name: lineDetails[2].legendName,
                            data: ds3
                        };
                        series.push(ds3);
                        colors.push(lineDetails[2].color);
                    }
                    if (typeof dataSet[0].dataPoint4 !== "undefined" && dataSet[0].dataPoint4 !== "" && lineDetails[3].color !== "undefined" && lineDetails[3].legendName !== "undefined" && lineDetails[3].color !== "" && lineDetails[3].legendName !== "") {
                        ds4 = dataSet.map(function(Obj) {
                            return Obj.dataPoint4;
                        });
                        ds4 = {
                            name: lineDetails[3].legendName,
                            data: ds4
                        };
                        series.push(ds4);
                        colors.push(lineDetails[3].color);
                    }
                    if (typeof dataSet[0].dataPoint5 !== "undefined" && dataSet[0].dataPoint5 !== "" && lineDetails[4].color !== "undefined" && lineDetails[4].legendName !== "undefined" && lineDetails[4].color !== "" && lineDetails[4].legendName !== "") {
                        ds5 = dataSet.map(function(Obj) {
                            return Obj.dataPoint5;
                        });
                        ds5 = {
                            name: lineDetails[4].legendName,
                            data: ds5
                        };
                        series.push(ds5);
                        colors.push(lineDetails[4].color);
                    }
                } else {
                    throw {
                        "Error": "noData",
                        "message": "Data is not passed at all/passed in an improper way"
                    };
                }
                if (this.validateAllParams(this._chartTitle, labels, series, this._chartProperties)) {
                    var x = this.view.lineChartBrowser.evaluateJavaScript('var chartObj = new konymp.charts.lineChart(); chartObj.drawLineChart(' + JSON.stringify(this._chartTitle) + ',' + JSON.stringify(labels) + ',' + JSON.stringify(series) + ',' + JSON.stringify(colors) + ',' + JSON.stringify(this._chartProperties) + ');');
                    this.view.forceLayout();
                    konymp.logger.trace("----------Exiting createChart Function---------", konymp.logger.FUNCTION_EXIT);
                    return true;
                }
            } catch (exception) {
                konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                if (exception.Error === "noData") {
                    throw (exception);
                }
            }
        },
        createChartRawData: function(labels, series, colors) {
            konymp.logger.trace("----------Entering createChart Function---------", konymp.logger.FUNCTION_ENTRY);
            try {
                if (this.validateAllParams(this._chartTitle, labels, series, this._chartProperties)) {
                    var x = this.view.lineChartBrowser.evaluateJavaScript('var chartObj = new konymp.charts.lineChart(); chartObj.drawLineChart(' + JSON.stringify(this._chartTitle) + ',' + JSON.stringify(labels) + ',' + JSON.stringify(series) + ',' + JSON.stringify(colors) + ',' + JSON.stringify(this._chartProperties) + ');');
                    this.view.forceLayout();
                    konymp.logger.trace("----------Exiting createChart Function---------", konymp.logger.FUNCTION_EXIT);
                    return true;
                }
            } catch (exception) {
                konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                if (exception.Error === "noData") {
                    throw (exception);
                }
            }
        },
        /**
         * @function validateData
         * @private
         * @param {String/Array/JSON} data
         * @param {String(datatype)} type
         * @description: validates the data param based on the corresponding type param
         */
        validateData: function(data, type) {
            konymp.logger.trace("----------Entering validateData Function---------", konymp.logger.FUNCTION_ENTRY);
            try {
                if (type === 'array') {
                    konymp.logger.trace("----------Exiting validateData Function---------", konymp.logger.FUNCTION_EXIT);
                    return Array.isArray(data);
                } else if (typeof data === type) {
                    konymp.logger.trace("----------Exiting validateData Function---------", konymp.logger.FUNCTION_EXIT);
                    return true;
                } else {
                    konymp.logger.trace("----------Exiting validateData Function---------", konymp.logger.FUNCTION_EXIT);
                    return false;
                }
            } catch (exception) {
                konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
            }
        },
        /**
         * @function validateAllParams
         * @private
         * @params {String} title, color, xAxisTitle, yAxisTitle
         * @params {JS Array} labels, series 
         * @description: invokes the validation of all params and returns a true only if all are validated
         */
        validateAllParams: function(title, labels, series, properties) {
            konymp.logger.trace("----------Entering validateAllParams Function---------", konymp.logger.FUNCTION_ENTRY);
            try {
                if (!this.validateData(title, 'string')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong dataType for title " + JSON.stringify(title)
                    };
                }
                if (!this.validateData(labels, 'array')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong datatype for labels " + JSON.stringify(labels)
                    };
                }
                if (!this.validateData(series, 'array')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong datatype for series " + JSON.stringify(series)
                    };
                }
                if (!this.validateData(properties._xAxisTitle, 'string')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong dataType for xAxisTitle " + JSON.stringify(properties._xAxisTitle)
                    };
                }
                if (!this.validateData(properties._yAxisTitle, 'string')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong dataType for yAxisTitle " + JSON.stringify(properties._yAxisTitle)
                    };
                }
                if (!this.validateData(properties._titleFontColor, 'string')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong dataType for titleFontColor " + JSON.stringify(properties._titleFontColor)
                    };
                }
                if (!this.validateData(properties._titleFontSize, 'string')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong dataType for titleFontSize " + JSON.stringify(properties._titleFontSize)
                    };
                }
                if (!this.validateData(properties._bgColor, 'string')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong dataType for backgroundColor " + JSON.stringify(properties._bgColor)
                    };
                }
                if (!this.validateData(properties._legendFontColor, 'string')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong dataType for legendFontColor " + JSON.stringify(properties._legendFontColor)
                    };
                }
                if (!this.validateData(properties._legendFontSize, 'string')) {
                    throw {
                        "Error": "Invalid Datatype",
                        "message": "Wrong dataType for legendFontSize " + JSON.stringify(properties._legendFontSize)
                    };
                }
            } catch (exception) {
                konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                if (exception.Error === "Invalid Datatype") {
                    throw (exception);
                }
            }
            konymp.logger.trace("----------Exiting validateAllParams Function---------", konymp.logger.FUNCTION_EXIT);
            return true;
        },
        /**
         * @function showGridChart 
         * @description creates the chart with the data in the data grid on browser load
         */
        showGridChart: function() {
            try {
                if (this._chartProperties._enableStaticPreview && this._chartData.length !== 0 && this._lineDetails.length !== 0) {
                    this.createChart(this._chartData, this._lineDetails);
                } else {
                    throw {
                        "Error": "NoData",
                        "message": "No data in data grid"
                    };
                }
            } catch (exception) {
                if (exception.Error === "NoData") {
                    konymp.logger.error(JSON.stringify(exception), konymp.logger.EXCEPTION);
                }
            }
        }
    };
});
define("com/konymp/multiline/multilineControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/konymp/multiline/multilineController", ["com/konymp/multiline/usermultilineController", "com/konymp/multiline/multilineControllerActions"], function() {
    var controller = require("com/konymp/multiline/usermultilineController");
    var actions = require("com/konymp/multiline/multilineControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});
