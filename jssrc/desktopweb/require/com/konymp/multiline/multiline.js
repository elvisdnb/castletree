define(function() {
    return function(controller) {
        var multiline = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "multiline",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "multiline"), extendConfig({}, controller.args[1], "multiline"), extendConfig({}, controller.args[2], "multiline"));
        multiline.setDefaultUnit(kony.flex.DP);
        var lineChartBrowser = new kony.ui.Browser(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "detectTelNumber": true,
            "enableNativeCommunication": true,
            "enableZoom": false,
            "height": "100%",
            "id": "lineChartBrowser",
            "isVisible": true,
            "left": 0,
            "setAsContent": false,
            "requestURLConfig": {
                "URL": "chartist_multiLine/multiLineChart.html",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lineChartBrowser"), extendConfig({}, controller.args[1], "lineChartBrowser"), extendConfig({}, controller.args[2], "lineChartBrowser"));
        multiline.add(lineChartBrowser);
        multiline.compInstData = {}
        return multiline;
    }
})