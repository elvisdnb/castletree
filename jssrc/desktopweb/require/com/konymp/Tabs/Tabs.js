define(function() {
    return function(controller) {
        var Tabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "64dp",
            "id": "Tabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "98%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "Tabs"), extendConfig({}, controller.args[1], "Tabs"), extendConfig({}, controller.args[2], "Tabs"));
        Tabs.setDefaultUnit(kony.flex.DP);
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "34dp",
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var btnAccounts1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnAccounts1",
            "isVisible": true,
            "left": "27dp",
            "onClick": controller.AS_Button_fb872159826b4baf9bc1a6cccf31cc32,
            "skin": "sknTabSelected",
            "text": "Accounts",
            "top": "0px",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "btnAccounts1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAccounts1"), extendConfig({}, controller.args[2], "btnAccounts1"));
        var btnAccounts2 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnAccounts2",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknTabUnselected",
            "text": "Loans",
            "top": "0px",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "btnAccounts2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAccounts2"), extendConfig({}, controller.args[2], "btnAccounts2"));
        var btnAccounts3 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnAccounts3",
            "isVisible": true,
            "left": "20dp",
            "onClick": controller.AS_Button_b312ef910fe44c3da0ee24ff7d0c734d,
            "skin": "sknTabUnselected",
            "text": "Deposits",
            "top": "0px",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "btnAccounts3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAccounts3"), extendConfig({}, controller.args[2], "btnAccounts3"));
        var btnAccounts4 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnAccounts4",
            "isVisible": true,
            "left": "20dp",
            "onClick": controller.AS_Button_b312ef910fe44c3da0ee24ff7d0c734d,
            "skin": "sknTabUnselected",
            "text": "Bundles",
            "top": "0px",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "btnAccounts4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAccounts4"), extendConfig({}, controller.args[2], "btnAccounts4"));
        var flxSelectedLine = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "height": "220dp",
            "id": "flxSelectedLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "22dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedLine"), extendConfig({}, controller.args[1], "flxSelectedLine"), extendConfig({}, controller.args[2], "flxSelectedLine"));
        flxSelectedLine.setDefaultUnit(kony.flex.DP);
        flxSelectedLine.add();
        flxButtons.add(btnAccounts1, btnAccounts2, btnAccounts3, btnAccounts4, flxSelectedLine);
        var flxContainerAccount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "2dp",
            "id": "flxContainerAccount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "27dp",
            "isModalContainer": false,
            "skin": "sknSelected",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxContainerAccount"), extendConfig({}, controller.args[1], "flxContainerAccount"), extendConfig({}, controller.args[2], "flxContainerAccount"));
        flxContainerAccount.setDefaultUnit(kony.flex.DP);
        flxContainerAccount.add();
        var flxContainerLoans = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "2dp",
            "id": "flxContainerLoans",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "127dp",
            "isModalContainer": false,
            "skin": "sknNotselectedTransparent",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxContainerLoans"), extendConfig({}, controller.args[1], "flxContainerLoans"), extendConfig({}, controller.args[2], "flxContainerLoans"));
        flxContainerLoans.setDefaultUnit(kony.flex.DP);
        flxContainerLoans.add();
        var flxContainerDeposits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "2dp",
            "id": "flxContainerDeposits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "227dp",
            "isModalContainer": false,
            "skin": "sknNotselected",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxContainerDeposits"), extendConfig({}, controller.args[1], "flxContainerDeposits"), extendConfig({}, controller.args[2], "flxContainerDeposits"));
        flxContainerDeposits.setDefaultUnit(kony.flex.DP);
        flxContainerDeposits.add();
        var flxContainerBundles = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "2dp",
            "id": "flxContainerBundles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "328dp",
            "isModalContainer": false,
            "skin": "sknNotselected",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxContainerBundles"), extendConfig({}, controller.args[1], "flxContainerBundles"), extendConfig({}, controller.args[2], "flxContainerBundles"));
        flxContainerBundles.setDefaultUnit(kony.flex.DP);
        flxContainerBundles.add();
        var flxSeparatorLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparatorLine1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "18dp",
            "isModalContainer": false,
            "skin": "sknSeparatorBlue",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeparatorLine1"), extendConfig({}, controller.args[1], "flxSeparatorLine1"), extendConfig({}, controller.args[2], "flxSeparatorLine1"));
        flxSeparatorLine1.setDefaultUnit(kony.flex.DP);
        flxSeparatorLine1.add();
        Tabs.add(flxButtons, flxContainerAccount, flxContainerLoans, flxContainerDeposits, flxContainerBundles, flxSeparatorLine1);
        Tabs.compInstData = {}
        return Tabs;
    }
})