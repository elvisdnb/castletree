define("com/konymp/Tabs/userTabsController", function() {
    return {};
});
define("com/konymp/Tabs/TabsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/konymp/Tabs/TabsController", ["com/konymp/Tabs/userTabsController", "com/konymp/Tabs/TabsControllerActions"], function() {
    var controller = require("com/konymp/Tabs/userTabsController");
    var actions = require("com/konymp/Tabs/TabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
