define("com/konymp/questions/userquestionsController", function() {
    return {};
});
define("com/konymp/questions/questionsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/questions/questionsController", ["com/konymp/questions/userquestionsController", "com/konymp/questions/questionsControllerActions"], function() {
    var controller = require("com/konymp/questions/userquestionsController");
    var actions = require("com/konymp/questions/questionsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
