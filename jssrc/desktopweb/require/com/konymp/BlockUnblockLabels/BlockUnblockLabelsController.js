define("com/konymp/BlockUnblockLabels/userBlockUnblockLabelsController", function() {
    return {};
});
define("com/konymp/BlockUnblockLabels/BlockUnblockLabelsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/BlockUnblockLabels/BlockUnblockLabelsController", ["com/konymp/BlockUnblockLabels/userBlockUnblockLabelsController", "com/konymp/BlockUnblockLabels/BlockUnblockLabelsControllerActions"], function() {
    var controller = require("com/konymp/BlockUnblockLabels/userBlockUnblockLabelsController");
    var actions = require("com/konymp/BlockUnblockLabels/BlockUnblockLabelsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
