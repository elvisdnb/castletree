define(function() {
    return function(controller) {
        var BlockUnblockLabels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "BlockUnblockLabels",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "skin": "sknFlxContainerBGF2F2FF",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, controller.args[0], "BlockUnblockLabels"), extendConfig({}, controller.args[1], "BlockUnblockLabels"), extendConfig({}, controller.args[2], "BlockUnblockLabels"));
        BlockUnblockLabels.setDefaultUnit(kony.flex.DP);
        var flxHeadeing = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "56dp",
            "id": "flxHeadeing",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeadeing"), extendConfig({}, controller.args[1], "flxHeadeing"), extendConfig({}, controller.args[2], "flxHeadeing"));
        flxHeadeing.setDefaultUnit(kony.flex.DP);
        var lblRefNoHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblRefNoHeading",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Ref No",
            "top": 18,
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblRefNoHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRefNoHeading"), extendConfig({}, controller.args[2], "lblRefNoHeading"));
        var lblStartDateHeading = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "height": "21dp",
            "id": "lblStartDateHeading",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Start Date",
            "top": 18,
            "width": "92dp",
            "zIndex": 1
        }, controller.args[0], "lblStartDateHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateHeading"), extendConfig({}, controller.args[2], "lblStartDateHeading"));
        var lblEndDateHeading = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "height": "21dp",
            "id": "lblEndDateHeading",
            "isVisible": true,
            "left": "8dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "End Date",
            "top": 18,
            "width": "92dp",
            "zIndex": 1
        }, controller.args[0], "lblEndDateHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEndDateHeading"), extendConfig({}, controller.args[2], "lblEndDateHeading"));
        var lblAmountHeading = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "height": "21dp",
            "id": "lblAmountHeading",
            "isVisible": true,
            "left": "8dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Amount",
            "top": 18,
            "width": "96dp",
            "zIndex": 1
        }, controller.args[0], "lblAmountHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountHeading"), extendConfig({}, controller.args[2], "lblAmountHeading"));
        var lblReasonHeading = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "height": "21dp",
            "id": "lblReasonHeading",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Reason",
            "top": 18,
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblReasonHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblReasonHeading"), extendConfig({}, controller.args[2], "lblReasonHeading"));
        var lblEditHeading = new kony.ui.Label(extendConfig({
            "centerY": "55%",
            "height": "21dp",
            "id": "lblEditHeading",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Edit",
            "top": 18,
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "lblEditHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditHeading"), extendConfig({}, controller.args[2], "lblEditHeading"));
        var lblUnblockHeading = new kony.ui.Label(extendConfig({
            "centerY": "55%",
            "height": "21dp",
            "id": "lblUnblockHeading",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "UnBlock",
            "top": 18,
            "width": "64dp",
            "zIndex": 1
        }, controller.args[0], "lblUnblockHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUnblockHeading"), extendConfig({}, controller.args[2], "lblUnblockHeading"));
        flxHeadeing.add(lblRefNoHeading, lblStartDateHeading, lblEndDateHeading, lblAmountHeading, lblReasonHeading, lblEditHeading, lblUnblockHeading);
        var flxBlockFundsList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxBlockFundsList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBlockFundsList"), extendConfig({}, controller.args[1], "flxBlockFundsList"), extendConfig({}, controller.args[2], "flxBlockFundsList"));
        flxBlockFundsList.setDefaultUnit(kony.flex.DP);
        var segBlockFundsList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "btnEdit": "",
                "btnUnblock": "",
                "lblAmount": "",
                "lblEndDate": "",
                "lblReason": "",
                "lblReferenceNo": "",
                "lblStartDate": ""
            }],
            "groupCells": false,
            "height": "100%",
            "id": "segBlockFundsList",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "Copyseg0b0addaabae4a40",
            "rowSkin": "sknSegTransparent",
            "rowTemplate": "flxRowBlockFunds",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa32",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": true,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "btnEdit": "btnEdit",
                "btnUnblock": "btnUnblock",
                "flxRowBlockFunds": "flxRowBlockFunds",
                "lblAmount": "lblAmount",
                "lblEndDate": "lblEndDate",
                "lblReason": "lblReason",
                "lblReferenceNo": "lblReferenceNo",
                "lblStartDate": "lblStartDate"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segBlockFundsList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segBlockFundsList"), extendConfig({}, controller.args[2], "segBlockFundsList"));
        var lblNoRecord = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "56dp",
            "id": "lblNoRecord",
            "isVisible": false,
            "skin": "sknLabelFont14BGE3D3FE",
            "text": "No Records Found",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblNoRecord"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoRecord"), extendConfig({}, controller.args[2], "lblNoRecord"));
        flxBlockFundsList.add(segBlockFundsList, lblNoRecord);
        BlockUnblockLabels.add(flxHeadeing, flxBlockFundsList);
        BlockUnblockLabels.compInstData = {}
        return BlockUnblockLabels;
    }
})