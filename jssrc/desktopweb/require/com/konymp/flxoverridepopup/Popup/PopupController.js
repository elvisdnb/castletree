define("com/konymp/flxoverridepopup/Popup/userPopupController", function() {
    return {};
});
define("com/konymp/flxoverridepopup/Popup/PopupControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/flxoverridepopup/Popup/PopupController", ["com/konymp/flxoverridepopup/Popup/userPopupController", "com/konymp/flxoverridepopup/Popup/PopupControllerActions"], function() {
    var controller = require("com/konymp/flxoverridepopup/Popup/userPopupController");
    var actions = require("com/konymp/flxoverridepopup/Popup/PopupControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
