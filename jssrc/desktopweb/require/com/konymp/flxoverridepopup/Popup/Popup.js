define(function() {
    return function(controller) {
        var Popup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "986dp",
            "id": "Popup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknlblBlockedSeduled",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "Popup"), extendConfig({}, controller.args[1], "Popup"), extendConfig({}, controller.args[2], "Popup"));
        Popup.setDefaultUnit(kony.flex.DP);
        var flxPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "494dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0g4e27fbd729446",
            "top": "318dp",
            "width": "455dp",
            "zIndex": 1
        }, controller.args[0], "flxPopup"), extendConfig({}, controller.args[1], "flxPopup"), extendConfig({}, controller.args[2], "flxPopup"));
        flxPopup.setDefaultUnit(kony.flex.DP);
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var lblPaymentInfo = new kony.ui.Label(extendConfig({
            "id": "lblPaymentInfo",
            "isVisible": true,
            "left": "5%",
            "skin": "CopydefLabel0cd19e22c059a4d",
            "text": "Payment Confirmation",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPaymentInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPaymentInfo"), extendConfig({}, controller.args[2], "lblPaymentInfo"));
        var imgClose = new kony.ui.Image2(extendConfig({
            "height": "24dp",
            "id": "imgClose",
            "isVisible": true,
            "right": "30dp",
            "skin": "slImage",
            "src": "ico_close.png",
            "top": "8dp",
            "width": "24dp",
            "zIndex": 1
        }, controller.args[0], "imgClose"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose"), extendConfig({}, controller.args[2], "imgClose"));
        flxPopupHeader.add(lblPaymentInfo, imgClose);
        var flxPopupMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "5%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "381dp",
            "zIndex": 1
        }, controller.args[0], "flxPopupMessage"), extendConfig({}, controller.args[1], "flxPopupMessage"), extendConfig({}, controller.args[2], "flxPopupMessage"));
        flxPopupMessage.setDefaultUnit(kony.flex.DP);
        var lblMessage = new kony.ui.Label(extendConfig({
            "id": "lblMessage",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblBG6C6C6Cpx16",
            "text": "An amount highr than due amount is being paid",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMessage"), extendConfig({}, controller.args[2], "lblMessage"));
        var lblProceed = new kony.ui.Label(extendConfig({
            "id": "lblProceed",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblBG6C6C6Cpx16",
            "text": "Do you want to proceed?",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProceed"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProceed"), extendConfig({}, controller.args[2], "lblProceed"));
        flxPopupMessage.add(lblMessage, lblProceed);
        var flxFooter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxFooter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBorderE9EDF3",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFooter"), extendConfig({}, controller.args[1], "flxFooter"), extendConfig({}, controller.args[2], "flxFooter"));
        flxFooter.setDefaultUnit(kony.flex.DP);
        var btnCancel = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnCancel",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknBtnBGffffffBorderCCE3F7",
            "text": "No",
            "top": "12dp",
            "width": "171dp",
            "zIndex": 1
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({}, controller.args[2], "btnCancel"));
        var btnProceed = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnProceed",
            "isVisible": true,
            "left": "24dp",
            "skin": "sknBtnBG0075DBRounded24",
            "text": "Yes",
            "top": "12dp",
            "width": "177dp",
            "zIndex": 1
        }, controller.args[0], "btnProceed"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnProceed"), extendConfig({}, controller.args[2], "btnProceed"));
        flxFooter.add(btnCancel, btnProceed);
        var flxDummy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10dp",
            "id": "flxDummy",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDummy"), extendConfig({}, controller.args[1], "flxDummy"), extendConfig({}, controller.args[2], "flxDummy"));
        flxDummy.setDefaultUnit(kony.flex.DP);
        flxDummy.add();
        flxPopup.add(flxPopupHeader, flxPopupMessage, flxFooter, flxDummy);
        Popup.add(flxPopup);
        Popup.compInstData = {}
        return Popup;
    }
})