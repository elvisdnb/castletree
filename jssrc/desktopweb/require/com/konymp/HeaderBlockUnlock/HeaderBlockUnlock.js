define(function() {
    return function(controller) {
        var HeaderBlockUnlock = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "129dp",
            "id": "HeaderBlockUnlock",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "HeaderBlockUnlock"), extendConfig({}, controller.args[1], "HeaderBlockUnlock"), extendConfig({}, controller.args[2], "HeaderBlockUnlock"));
        HeaderBlockUnlock.setDefaultUnit(kony.flex.DP);
        var flxAccountBlockUnblockDetailsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "64dp",
            "id": "flxAccountBlockUnblockDetailsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0j9097742fdc64c",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAccountBlockUnblockDetailsHeader"), extendConfig({}, controller.args[1], "flxAccountBlockUnblockDetailsHeader"), extendConfig({}, controller.args[2], "flxAccountBlockUnblockDetailsHeader"));
        flxAccountBlockUnblockDetailsHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountTypeInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountTypeInfo",
            "isVisible": true,
            "left": "27dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Account Type",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountTypeInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountTypeInfo"), extendConfig({}, controller.args[2], "lblAccountTypeInfo"));
        var lblAccountNumberInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountNumberInfo",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Account Number",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountNumberInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountNumberInfo"), extendConfig({}, controller.args[2], "lblAccountNumberInfo"));
        var lblCurrencyValueInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblCurrencyValueInfo",
            "isVisible": true,
            "left": "42dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "CCY",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencyValueInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyValueInfo"), extendConfig({}, controller.args[2], "lblCurrencyValueInfo"));
        var lblAmountValueInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAmountValueInfo",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Ledger/Cleared Balance",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAmountValueInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountValueInfo"), extendConfig({}, controller.args[2], "lblAmountValueInfo"));
        var lblAvailableLimitInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAvailableLimitInfo",
            "isVisible": true,
            "left": "100dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Available Limit",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAvailableLimitInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableLimitInfo"), extendConfig({}, controller.args[2], "lblAvailableLimitInfo"));
        var lblAccountServiceInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountServiceInfo",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknPaymentDate14pxBlack",
            "text": "Account Services",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountServiceInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountServiceInfo"), extendConfig({}, controller.args[2], "lblAccountServiceInfo"));
        flxAccountBlockUnblockDetailsHeader.add(lblAccountTypeInfo, lblAccountNumberInfo, lblCurrencyValueInfo, lblAmountValueInfo, lblAvailableLimitInfo, lblAccountServiceInfo);
        var flxseparatorLinedetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxseparatorLinedetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSeparatorGreye5e5e5",
            "top": "64dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxseparatorLinedetails"), extendConfig({}, controller.args[1], "flxseparatorLinedetails"), extendConfig({}, controller.args[2], "flxseparatorLinedetails"));
        flxseparatorLinedetails.setDefaultUnit(kony.flex.DP);
        flxseparatorLinedetails.add();
        var flxAccountBlockUnblockDetailsvalue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxAccountBlockUnblockDetailsvalue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "32dp",
            "skin": "sknFlxWhiteBgBoreder",
            "top": "65dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAccountBlockUnblockDetailsvalue"), extendConfig({}, controller.args[1], "flxAccountBlockUnblockDetailsvalue"), extendConfig({}, controller.args[2], "flxAccountBlockUnblockDetailsvalue"));
        flxAccountBlockUnblockDetailsvalue.setDefaultUnit(kony.flex.DP);
        var lblAccountTypeResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAccountTypeResponse",
            "isVisible": true,
            "left": "27dp",
            "skin": "sknLblFontColorWhite",
            "text": "Account Type",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountTypeResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountTypeResponse"), extendConfig({}, controller.args[2], "lblAccountTypeResponse"));
        var lblAccountNumberResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAccountNumberResponse",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblFontColorWhite",
            "text": "50205054",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAccountNumberResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountNumberResponse"), extendConfig({}, controller.args[2], "lblAccountNumberResponse"));
        var lblCurrencyValueResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCurrencyValueResponse",
            "isVisible": true,
            "left": "42dp",
            "skin": "sknLblFontColorWhite",
            "text": "CCY",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencyValueResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyValueResponse"), extendConfig({}, controller.args[2], "lblCurrencyValueResponse"));
        var lblAmountValueResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAmountValueResponse",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblFontColorWhite",
            "text": "2,80,034.00",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAmountValueResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountValueResponse"), extendConfig({}, controller.args[2], "lblAmountValueResponse"));
        var lblFundHeldResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblFundHeldResponse",
            "isVisible": true,
            "left": "-32dp",
            "skin": "payRed",
            "text": "Fund Held",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblFundHeldResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFundHeldResponse"), extendConfig({}, controller.args[2], "lblFundHeldResponse"));
        var lblAvailableLimitResponse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblAvailableLimitResponse",
            "isVisible": true,
            "left": "-30dp",
            "skin": "sknLblFontColorWhite",
            "text": "11,933.00",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "lblAvailableLimitResponse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableLimitResponse"), extendConfig({}, controller.args[2], "lblAvailableLimitResponse"));
        var flxListBoxSelectedValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "47%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxListBoxSelectedValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "23dp",
            "minWidth": "160dp",
            "isModalContainer": false,
            "skin": "sknFlxBG2772C3Border",
            "width": "163dp",
            "zIndex": 1
        }, controller.args[0], "flxListBoxSelectedValue"), extendConfig({}, controller.args[1], "flxListBoxSelectedValue"), extendConfig({}, controller.args[2], "flxListBoxSelectedValue"));
        flxListBoxSelectedValue.setDefaultUnit(kony.flex.DP);
        var lblListBoxSelectedValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblListBoxSelectedValue",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblBGFFFFFF",
            "text": "Block/UnBlock Funds",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblListBoxSelectedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblListBoxSelectedValue"), extendConfig({}, controller.args[2], "lblListBoxSelectedValue"));
        var imgListBoxSelectedDropDown = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgListBoxSelectedDropDown",
            "isVisible": false,
            "left": "8dp",
            "skin": "slImage",
            "src": "drop_down_arrow.png",
            "width": "18dp",
            "zIndex": 1
        }, controller.args[0], "imgListBoxSelectedDropDown"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgListBoxSelectedDropDown"), extendConfig({}, controller.args[2], "imgListBoxSelectedDropDown"));
        var flxButtomLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "2dp",
            "id": "flxButtomLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "-120dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0da1528041d7845",
            "top": "35dp",
            "width": "220dp",
            "zIndex": 1
        }, controller.args[0], "flxButtomLine"), extendConfig({}, controller.args[1], "flxButtomLine"), extendConfig({}, controller.args[2], "flxButtomLine"));
        flxButtomLine.setDefaultUnit(kony.flex.DP);
        flxButtomLine.add();
        flxListBoxSelectedValue.add(lblListBoxSelectedValue, imgListBoxSelectedDropDown, flxButtomLine);
        flxAccountBlockUnblockDetailsvalue.add(lblAccountTypeResponse, lblAccountNumberResponse, lblCurrencyValueResponse, lblAmountValueResponse, lblFundHeldResponse, lblAvailableLimitResponse, flxListBoxSelectedValue);
        HeaderBlockUnlock.add(flxAccountBlockUnblockDetailsHeader, flxseparatorLinedetails, flxAccountBlockUnblockDetailsvalue);
        HeaderBlockUnlock.compInstData = {}
        return HeaderBlockUnlock;
    }
})