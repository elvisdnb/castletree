define("com/konymp/HeaderBlockUnlock/userHeaderBlockUnlockController", function() {
    return {};
});
define("com/konymp/HeaderBlockUnlock/HeaderBlockUnlockControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/HeaderBlockUnlock/HeaderBlockUnlockController", ["com/konymp/HeaderBlockUnlock/userHeaderBlockUnlockController", "com/konymp/HeaderBlockUnlock/HeaderBlockUnlockControllerActions"], function() {
    var controller = require("com/konymp/HeaderBlockUnlock/userHeaderBlockUnlockController");
    var actions = require("com/konymp/HeaderBlockUnlock/HeaderBlockUnlockControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
