define(function() {
    return function(controller) {
        var AccountList2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "isMaster": true,
            "id": "AccountList2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "postShow": controller.AS_FlexContainer_d1a68f4179ae4c61ae40182f8a71101f,
            "skin": "sknflxWhiteBGBorder0075db",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10,
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "AccountList2"), extendConfig({}, controller.args[1], "AccountList2"), extendConfig({}, controller.args[2], "AccountList2"));
        AccountList2.setDefaultUnit(kony.flex.DP);
        var flxAccoutMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccoutMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxAccoutMain"), extendConfig({}, controller.args[1], "flxAccoutMain"), extendConfig({}, controller.args[2], "flxAccoutMain"));
        flxAccoutMain.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var lblAccNo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAccNo",
            "isVisible": true,
            "left": "1%",
            "skin": "CopydefLabel0gbe086184e6d4d",
            "text": "Account No",
            "top": "0",
            "width": "32.30%"
        }, controller.args[0], "lblAccNo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccNo"), extendConfig({}, controller.args[2], "lblAccNo"));
        var lblType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblType",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0gbe086184e6d4d",
            "text": "Account Type",
            "top": "0",
            "width": "33.30%"
        }, controller.args[0], "lblType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType"), extendConfig({}, controller.args[2], "lblType"));
        var lblBalance = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBalance",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0gbe086184e6d4d",
            "text": "Balance",
            "top": "0",
            "width": "33.30%"
        }, controller.args[0], "lblBalance"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBalance"), extendConfig({}, controller.args[2], "lblBalance"));
        flxHeader.add(lblAccNo, lblType, lblBalance);
        var segAccountList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }, {
                "lblAccountId": "-",
                "lblWorkingBal": "Final",
                "lbltype": "-"
            }],
            "groupCells": false,
            "height": "200dp",
            "id": "segAccountList",
            "isVisible": true,
            "left": "0",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "FlxAccountList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": true,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": 40,
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "FlxAccountList": "FlxAccountList",
                "lblAccountId": "lblAccountId",
                "lblWorkingBal": "lblWorkingBal",
                "lbltype": "lbltype"
            },
            "width": "100%"
        }, controller.args[0], "segAccountList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAccountList"), extendConfig({}, controller.args[2], "segAccountList"));
        var lblNoAccounts = new kony.ui.Label(extendConfig({
            "height": "195dp",
            "id": "lblNoAccounts",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0gbe086184e6d4d",
            "text": "No Accounts Found",
            "top": 40,
            "width": "100%"
        }, controller.args[0], "lblNoAccounts"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoAccounts"), extendConfig({}, controller.args[2], "lblNoAccounts"));
        flxAccoutMain.add(flxHeader, segAccountList, lblNoAccounts);
        AccountList2.add(flxAccoutMain);
        AccountList2.compInstData = {}
        return AccountList2;
    }
})