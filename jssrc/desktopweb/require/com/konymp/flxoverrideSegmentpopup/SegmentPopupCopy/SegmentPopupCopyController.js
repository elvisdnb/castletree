define("com/konymp/flxoverrideSegmentpopup/SegmentPopupCopy/userSegmentPopupCopyController", function() {
    return {};
});
define("com/konymp/flxoverrideSegmentpopup/SegmentPopupCopy/SegmentPopupCopyControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/flxoverrideSegmentpopup/SegmentPopupCopy/SegmentPopupCopyController", ["com/konymp/flxoverrideSegmentpopup/SegmentPopupCopy/userSegmentPopupCopyController", "com/konymp/flxoverrideSegmentpopup/SegmentPopupCopy/SegmentPopupCopyControllerActions"], function() {
    var controller = require("com/konymp/flxoverrideSegmentpopup/SegmentPopupCopy/userSegmentPopupCopyController");
    var actions = require("com/konymp/flxoverrideSegmentpopup/SegmentPopupCopy/SegmentPopupCopyControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
