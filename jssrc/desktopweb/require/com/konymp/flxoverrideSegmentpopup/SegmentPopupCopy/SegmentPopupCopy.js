define(function() {
    return function(controller) {
        var SegmentPopupCopy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "968dp",
            "id": "SegmentPopupCopy",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopysknlblBlockedSeduled",
            "top": "0dp",
            "width": "100%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "SegmentPopupCopy"), extendConfig({}, controller.args[1], "SegmentPopupCopy"), extendConfig({}, controller.args[2], "SegmentPopupCopy"));
        SegmentPopupCopy.setDefaultUnit(kony.flex.DP);
        var flxPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "310dp",
            "id": "flxPopup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "494dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox0j90c88e0f1f44a",
            "top": "318dp",
            "width": "456dp",
            "zIndex": 1
        }, controller.args[0], "flxPopup"), extendConfig({}, controller.args[1], "flxPopup"), extendConfig({}, controller.args[2], "flxPopup"));
        flxPopup.setDefaultUnit(kony.flex.DP);
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var lblPaymentInfo = new kony.ui.Label(extendConfig({
            "id": "lblPaymentInfo",
            "isVisible": true,
            "left": "32dp",
            "skin": "CopysknLbl1",
            "text": "Override Confirmation",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPaymentInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPaymentInfo"), extendConfig({}, controller.args[2], "lblPaymentInfo"));
        var imgClose = new kony.ui.Image2(extendConfig({
            "height": "24dp",
            "id": "imgClose",
            "isVisible": true,
            "right": "36dp",
            "skin": "slImage",
            "src": "ico_close_1.png",
            "top": "8dp",
            "width": "24dp",
            "zIndex": 1
        }, controller.args[0], "imgClose"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClose"), extendConfig({}, controller.args[2], "imgClose"));
        flxPopupHeader.add(lblPaymentInfo, imgClose);
        var flxPopupMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "180dp",
            "id": "flxPopupMessage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "32dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "381dp",
            "zIndex": 1
        }, controller.args[0], "flxPopupMessage"), extendConfig({}, controller.args[1], "flxPopupMessage"), extendConfig({}, controller.args[2], "flxPopupMessage"));
        flxPopupMessage.setDefaultUnit(kony.flex.DP);
        var segOverride = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }, {
                "lblInfo": "Label",
                "lblSerial": "1"
            }],
            "groupCells": false,
            "height": "160dp",
            "id": "segOverride",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowSkin": "seg2Normal",
            "rowTemplate": "CopyflxOverrideTemplate",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": true,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "CopyFlxAccountList": "CopyFlxAccountList",
                "CopyflxOverrideTemplate": "CopyflxOverrideTemplate",
                "lblInfo": "lblInfo",
                "lblSerial": "lblSerial"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segOverride"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segOverride"), extendConfig({}, controller.args[2], "segOverride"));
        flxPopupMessage.add(segOverride);
        var flxFooter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxFooter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopysknFlxBorderE",
            "top": "240dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFooter"), extendConfig({}, controller.args[1], "flxFooter"), extendConfig({}, controller.args[2], "flxFooter"));
        flxFooter.setDefaultUnit(kony.flex.DP);
        var btnCancel = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnCancel",
            "isVisible": true,
            "left": "40dp",
            "skin": "CopysknBtnBGffffffBorderCCE",
            "text": "No",
            "top": 12,
            "width": "171dp",
            "zIndex": 1
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({}, controller.args[2], "btnCancel"));
        var btnProceed = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnProceed",
            "isVisible": true,
            "left": "24dp",
            "skin": "CopysknBtnBG",
            "text": "Yes",
            "top": 12,
            "width": "177dp",
            "zIndex": 1
        }, controller.args[0], "btnProceed"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnProceed"), extendConfig({}, controller.args[2], "btnProceed"));
        flxFooter.add(btnCancel, btnProceed);
        flxPopup.add(flxPopupHeader, flxPopupMessage, flxFooter);
        SegmentPopupCopy.add(flxPopup);
        SegmentPopupCopy.compInstData = {}
        return SegmentPopupCopy;
    }
})