define("com/konymp/flxoverrideSegmentpopup/SegmentPopup/userSegmentPopupController", function() {
    return {};
});
define("com/konymp/flxoverrideSegmentpopup/SegmentPopup/SegmentPopupControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/konymp/flxoverrideSegmentpopup/SegmentPopup/SegmentPopupController", ["com/konymp/flxoverrideSegmentpopup/SegmentPopup/userSegmentPopupController", "com/konymp/flxoverrideSegmentpopup/SegmentPopup/SegmentPopupControllerActions"], function() {
    var controller = require("com/konymp/flxoverrideSegmentpopup/SegmentPopup/userSegmentPopupController");
    var actions = require("com/konymp/flxoverrideSegmentpopup/SegmentPopup/SegmentPopupControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
