define("com/documentImages/doc/userdocController", function() {
    return {};
});
define("com/documentImages/doc/docControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/documentImages/doc/docController", ["com/documentImages/doc/userdocController", "com/documentImages/doc/docControllerActions"], function() {
    var controller = require("com/documentImages/doc/userdocController");
    var actions = require("com/documentImages/doc/docControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
