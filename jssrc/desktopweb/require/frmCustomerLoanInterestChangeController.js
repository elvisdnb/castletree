define("userfrmCustomerLoanInterestChangeController", {
    arrangementId: "",
    changeLoanModeValue: [],
    currentCurrency: "",
    FloatingRateValue: [],
    currentLoanType: "",
    floatingData: [],
    fixedData: [],
    fixed_segData: [],
    floating_segData: [],
    _loanObj: {},
    currentOverride: {},
    //Type your controller code here 
    onNavigate: function(navObj) {
        this._loanObj = {};
        currentCurrency = navObj.lblCCY;
        currentLoanType = navObj.loanProduct;
        arrangementId = Application.validation.isNullUndefinedObj(navObj.lblArrangementId);
        this.getInterestRateBasedFloatingIndex();
        this.setLoanHeaderDetails(navObj);
        this.bindEvents();
    },
    bindEvents: function() {
        this.getCurrentDate(transactDate);
        this.view.postShow = this.postShowFunctionCall;
        this.view.lblBreadBoardOverviewvalue.onTouchEnd = this.navigateCustomerAccountDetails;
        this.view.lblBreadBoardHomeValue.onTouchEnd = this.navigateHome;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.onCloseErrorAlert;
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup.btnProceed.onClick = this.validateSegData;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.cusRadioButtonSelectionModeValue.onRadioChange = this.getSelectInterestType;
        this.view.cusButtonConfirm.onclickEvent = this.validateSegData; //this.onClickSubmitData; - Added validation for segment data
        this.view.cusButtonCancel.onclickEvent = this.navigateCustomerAccountDetails;
    },
    navigateHome: function() {
        var navToHome = new kony.mvc.Navigation("frmDashboard");
        var navObjet = {};
        navObjet.clientId = "";
        navToHome.navigate(navObjet);
    },
    closeSegmentOverrite: function() {
        this.currentOverride = {};
        this.view.SegmentPopup.isVisible = false;
    },
    navToHomefunc: function() {
        var navToHome = new kony.mvc.Navigation("frmDashboard");
        navToHome.navigate({});
    },
    navigateCustomerAccountDetails: function() {
        var navToCusLoans = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navObjet.isPopupShow = "false";
        navToCusLoans.navigate(navObjet);
    },
    postShowFunctionCall: function() {
        try {
            //kony.print("customerDetailsValue::"+JSON.stringify(customerDetailsValue));
            this.initializeCustomComponent();
            this.view.SegmentPopup.isVisible = false;
            this.view.ErrorAllert.isVisible = false;
            this.view.flxMessageInfo.isVisible = false;
            this.view.flxHeaderMenu.isVisible = true;
            //this.view.cusRadioButtonSelectionModeValue.onRadioChange = this.getSelectInterestType;
            //this.populatingCustomerDetails();
            //this.view.cusButtonConfirm.onclickEvent = this.validateSegData; //this.onClickSubmitData; - Added validation for segment data
            //this.view.cusButtonCancel.onclickEvent = this.navigateCustomerAccountDetails;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController postFunctionCall:::" + err);
        }
    },
    onCloseErrorAlert: function() {
        this.view.ErrorAllert.isVisible = false;
        this.view.flxHeaderMenu.isVisible = true;
        this.view.forceLayout();
    },
    setLoanHeaderDetails: function(navObj) {
        try {
            var newObj = navObj;
            this._loanObj.currencyId = navObj.lblCCY;
            this._loanObj.arrangementId = navObj.lblArrangementId;
            this._loanObj.customerId = navObj.customerId;
            this._loanObj.produtId = navObj.loanProduct;
            //TODO : take from segment data a navigation object
            this.view.loanDetails.lblAccountNumberResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHeaderAN);
            this.view.loanDetails.lblLoanTypeResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHeaderAccountType); //"2345678";
            this.view.loanDetails.lblCurrencyValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblCCY);
            this.view.loanDetails.lblAmountValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblClearedBalance);
            this.view.loanDetails.lblStartDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAvailableLimit);
            this.view.loanDetails.lblMaturityDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHdr);
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController setLoanHeaderDetails:::" + err);
        }
    },
    initializeCustomComponent: function() {
        try {
            this.view.cusBackIcon.color = "temenos-dark";
            this.view.cusBackIcon.size = "small";
            this.view.cusBackIcon.value = "chevron_left";
            this.view.cusButtonDelete.cta = false;
            this.view.cusButtonDelete.labelText = "Delete";
            this.view.cusButtonDelete.icon = "remove_circle_outline";
            this.view.cusButtonDelete.size = "large";
            this.view.cusButtonDelete.outlined = false;
            this.view.cusButtonDelete.disabled = false;
            this.view.cusButtonDelete.trailingIcon = false;
            this.view.cusButtonDelete.compact = false;
            // this.view.cusNavigationRail.menuItems = '[{ "icon": "search", "label": "search" },{ "icon": "menu", "label": "menu" },{ "icon": "restore", "label": "restore" }]';
            //this.view.cusNavigationRail.bottomBtnItems = '[{"label":"Settings"},{"label":"Last Updated"},{"label":"Logout"}]';
            this.view.flxFixedModeWrapper.isVisible = true;
            this.view.flxFloatingModeWrapper.isVisible = false;
            this.view.flxHeaderMenu.isVisible = true;
            this.view.flxMessageInfo.isVisible = false;
            this.view.forceLayout();
            this.populatingTierTypeDropDown();
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController initializeCustomComponent:::" + err);
        }
    },
    getSelectInterestType: function() {
        try {
            kony.print("alRadio Button:::" + this.view.cusRadioButtonSelectionModeValue.value);
            kony.print("changeLoanModeValue:::" + JSON.stringify(changeLoanModeValue));
            kony.print("fixed_segData:::" + JSON.stringify(fixed_segData));
            kony.print("floating_segData:::" + JSON.stringify(floating_segData));
            var options = this.view.cusRadioButtonSelectionModeValue.value;
            if (options === 1) {
                this.view.cusTierListResponse.left = "100dp";
                this.view.cusTierListResponse.top = "20dp";
                this.view.cusTierListResponse.width = "150dp";
                this.view.cusTierListResponse.height = "60dp";
                this.view.cusEffectiveDateResponse.left = "60dp";
                this.view.cusEffectiveDateResponse.top = "20dp";
                this.view.cusEffectiveDateResponse.width = "350dp";
                this.view.cusEffectiveDateResponse.height = "60dp";
                this.view.flxButtonsHzFlow.left = "5dp";
                if (fixed_segData.length > 0) {
                    this.loadingDataToFixedMode(fixed_segData);
                } else {
                    var json1 = {
                        "interestRate": "",
                        "interestProperty": "",
                        "marginOperand": "",
                        "interestMargin": "",
                        "interestType": "",
                        "interestRateType": "",
                        "baseInterestRate": ""
                    };
                    var arr1 = [];
                    arr1.push(json1);
                    this.loadingDataToFixedMode(arr1);
                }
            }
            if (options === 2) {
                this.view.cusTierListResponse.left = "55dp";
                this.view.cusTierListResponse.top = "20dp";
                this.view.cusTierListResponse.width = "160dp";
                this.view.cusTierListResponse.height = "60dp";
                this.view.cusEffectiveDateResponse.left = "170dp";
                this.view.cusEffectiveDateResponse.top = "20dp";
                this.view.cusEffectiveDateResponse.width = "262dp";
                this.view.cusEffectiveDateResponse.height = "60dp";
                this.view.flxButtonsHzFlow.left = "0dp";
                if (floating_segData.length > 0) {
                    this.loadingDataToFloatingMode(floating_segData);
                } else {
                    var json = {
                        "interestRate": "",
                        "interestProperty": "",
                        "marginOperand": "",
                        "interestMargin": "",
                        "interestType": "",
                        "interestRateType": "",
                        "floating": ""
                    };
                    var arr = [];
                    arr.push(json);
                    this.loadingDataToFloatingMode(arr);
                }
            }
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController getSelectInterestType:::" + err);
        }
    },
    onChangeFloatInterestEvent: function() {
        try {
            this.view.flxFixedModeWrapper.isVisible = false;
            this.view.flxFloatingModeWrapper.isVisible = true;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController onChangeInterestEvent:::" + err);
        }
    },
    onChangeFixedInterestEvent: function() {
        try {
            this.view.flxFixedModeWrapper.isVisible = true;
            this.view.flxFloatingModeWrapper.isVisible = false;
            var segData = this.view.segFixed.data;
            if (segData.length > 0) {}
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController onChangeFixedInterestEvent:::" + err);
        }
    },
    populatingTierTypeDropDown: function() {
        try {
            this.view.cusDropDownTierListResponse.options = '[{"label": "Select"},{"label": "Example Tier1"}, {"label": "Example Tier2"}, {"label": "Example Tier3"}]';
            this.view.cusDropDownTierListResponse.value = "Select";
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController populatingTierTypeDropDown:::" + err);
        }
    },
    callingLoanServiceDetails: function() {
        try {
            this.currentOverride = {};
            this.view.segFixed.removeAll();
            this.view.segFloating.removeAll();
            var serviceName = "LendingNew";
            var operationName = "arrangementInterestDetails";
            var inputParams = {
                "arrangementId": arrangementId
            };
            var headers = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.loanServiceDetailsSuccessCB, this.loanServiceDetailsFailureCB);
            //Application.loader.showLoader("Please Wait...");
            this.view.ProgressIndicator.isVisible = true;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController callingLoanServiceDetails:::" + err);
        }
    },
    loanServiceDetailsSuccessCB: function(res) {
        try {
            //Application.loader.dismissLoader();
            this.view.ProgressIndicator.isVisible = false;
            var responseData = res.body;
            var exactData_fixed = [];
            changeLoanModeValue = [];
            var exactData_floating = [];
            fixed_segData = [];
            floating_segData = [];
            var interestRateType;
            if (responseData.length > 0) {
                for (var a in responseData) {
                    //if(responseData[a].interestProperty === "Principal Interest") {
                    if (responseData[a].interestRateType === "Variable") {
                        interestRateType = Application.validation.isNullUndefinedObj(responseData[a].interestType);
                        var json = {
                            "interestRate": responseData[a].interestRate,
                            "interestProperty": responseData[a].interestProperty,
                            "marginOperand": responseData[a].marginOperand,
                            "interestMargin": responseData[a].interestMargin,
                            "interestType": responseData[a].interestType,
                            "interestRateType": responseData[a].interestRateType,
                            "floating": responseData[a].floating,
                            "uptoTierAmount": responseData[a].tierAmount
                        };
                        exactData_floating.push(json);
                    }
                    if (responseData[a].interestRateType === "Fixed") {
                        interestRateType = Application.validation.isNullUndefinedObj(responseData[a].interestType);
                        var json1 = {
                            "interestRate": responseData[a].interestRate,
                            "interestProperty": responseData[a].interestProperty,
                            "marginOperand": responseData[a].marginOperand,
                            "interestMargin": responseData[a].interestMargin,
                            "interestType": responseData[a].interestType,
                            "interestRateType": responseData[a].interestRateType,
                            "baseInterestRate": responseData[a].baseInterestRate,
                            "uptoTierAmount": responseData[a].tierAmount
                        };
                        exactData_fixed.push(json1);
                    }
                    //}
                }
                //kony.print("changeLoanModeValue:::"+JSON.stringify(changeLoanModeValue));
                kony.print("exactData_fixed:::" + JSON.stringify(exactData_fixed));
                kony.print("exactData_floating:::" + JSON.stringify(exactData_floating));
                fixed_segData = exactData_fixed;
                floating_segData = exactData_floating;
                kony.print("interestRateType:::" + JSON.stringify(interestRateType));
                this.view.cusTierListResponse.value = interestRateType;
                this.view.forceLayout();
                this.callingModeBasedServiceCall(fixed_segData, floating_segData);
            } else {
                kony.print("No Data to show");
            }
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController loanServiceDetailsSuccessCB:::" + err);
        }
    },
    loanServiceDetailsFailureCB: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            //Application.loader.dismissLoader();
            alert("Service Failure");
            kony.print("" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController loanServiceDetailsFailureCB:::" + err);
        }
    },
    loadingDataToFixedMode: function(responseData) {
        try {
            this.view.flxFixedModeWrapper.isVisible = false;
            this.view.flxFloatingModeWrapper.isVisible = false;
            var dataArray = [];
            var interestType = "";
            for (var a in responseData) {
                if (responseData[a].interestType === "" || responseData[a].interestType === undefined || responseData[a].interestType === null) {
                    kony.print("responseData[a].interestType:::" + responseData[a].interestType);
                } else {
                    interestType = Application.validation.isNullUndefinedObj(responseData[a].interestType);
                    kony.print("interestType:::" + interestType);
                }
                var currentSelected = this.checkOperandData(responseData[a].marginOperand);
                var interestRateData = Application.validation.isNullUndefinedObj(responseData[a].interestRate);
                if (interestRateData !== "") {
                    interestRateData = parseFloat(interestRateData);
                    interestRateData = interestRateData.toFixed(2);
                }
                var uptoTierAmount = Application.validation.isNullUndefinedObj(responseData[a].uptoTierAmount);
                if (uptoTierAmount === "0") {
                    uptoTierAmount = "";
                }
                var json_data = {
                    "interestMargin": {
                        text: Application.validation.isNullUndefinedObj(responseData[a].interestMargin),
                        onEndEditing: this.calculateEffectiveRateFixed,
                        onDone: this.calculateEffectiveRateFixed
                    },
                    "interestRate": interestRateData, //Application.validation.isNullUndefinedObj(responseData[a].interestRate),
                    "marginOperand": {
                        masterData: [
                            ["select", "Please Select"],
                            ["ADD", "Add"],
                            ["SUB", "Sub"]
                        ],
                        selectedKey: currentSelected,
                        onSelection: this.operandSelectionHandlerFixed
                    }, //responseData[a].marginOperand,
                    "interestType": Application.validation.isNullUndefinedObj(responseData[a].interestType),
                    "uptoTierAmount": Application.validation.isNullUndefinedObj(uptoTierAmount),
                    "fixedRate": {
                        text: Application.validation.isNullUndefinedObj(responseData[a].baseInterestRate),
                        onEndEditing: this.calculateEffectiveRateFixed,
                        onDone: this.calculateEffectiveRateFixed
                    },
                };
                dataArray.push(json_data);
            }
            var widgetData = {
                "lblFixedRate": "fixedRate",
                "lstOperand": "marginOperand",
                "lblMargin": "interestMargin",
                "lblEffectiveRate": "interestRate",
                "lblUptoAmount": "uptoTierAmount"
            };
            this.view.segFixed.rowTemplate = "flxSegLoanFixed";
            //this.view.cusTierListResponse.value = interestType;
            this.view.segFixed.widgetDataMap = widgetData;
            this.view.segFixed.setData(dataArray);
            this.view.flxFixedModeWrapper.isVisible = true;
            this.view.flxFixedModeWrapper.forceLayout();
            fixedData = dataArray;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController loadingDataToFixedMode:::" + err);
        }
    },
    loadingDynamicDataFloatingRate: function() {
        var dataArray = [
            ["select", "Please Select"]
        ];
        if (floatingInterestRateAPI.length > 0) {
            for (var a in floatingInterestRateAPI) {
                var data = [floatingInterestRateAPI[a].key, floatingInterestRateAPI[a].description];
                dataArray.push(data);
            }
        }
        kony.print("loadingDynamicDataFloatingRate::::" + JSON.stringify(dataArray));
        return dataArray;
    },
    loadingDataToFloatingMode: function(responseData) {
        try {
            var listdata = [];
            listdata = this.loadingDynamicDataFloatingRate();
            this.view.flxFixedModeWrapper.isVisible = false;
            this.view.flxFloatingModeWrapper.isVisible = false;
            //FloatingRateValue = [["select","Please Select"],["1","Loan Base Rate"],["3","Fixed Dep Rate"],["6","SL Rates"],["10","Curr Base Rate"],["11","Sav Base Rate"],["12","OD Base Rate"],["15","CDI"],["20","Refinance Rate"],["30","FDIC Rate"],["32","Pel Bonus Rate"]];
            var dataArray = [];
            for (var a in responseData) {
                var currentSelected = this.appendingFloatingRate(responseData[a].floating);
                var currentSelected1 = this.checkOperandData(responseData[a].marginOperand);
                var convertToFloat_currentSelected = parseFloat(currentSelected[1]);
                var correctData_InterestRate;
                if (convertToFloat_currentSelected === null || convertToFloat_currentSelected === undefined || convertToFloat_currentSelected === "" || isNaN(convertToFloat_currentSelected) === true) {
                    correctData_InterestRate = "";
                } else {
                    correctData_InterestRate = convertToFloat_currentSelected.toFixed(2);
                }
                kony.print("FloatingRateValue:::" + JSON.stringify(listdata));
                var interestRateData = Application.validation.isNullUndefinedObj(responseData[a].interestRate);
                if (interestRateData !== "") {
                    interestRateData = parseFloat(interestRateData);
                    interestRateData = interestRateData.toFixed(2);
                }
                var uptoTierAmount = Application.validation.isNullUndefinedObj(responseData[a].uptoTierAmount);
                if (uptoTierAmount === "0") {
                    uptoTierAmount = "";
                }
                var json_data = {
                    "interestMargin": {
                        text: Application.validation.isNullUndefinedObj(responseData[a].interestMargin),
                        onEndEditing: this.calculateEffectiveRateFloating,
                        onDone: this.calculateEffectiveRateFloating
                    },
                    "interestRate": interestRateData, //Application.validation.isNullUndefinedObj(responseData[a].interestRate),
                    "marginOperand": {
                        masterData: [
                            ["select", "Please Select"],
                            ["ADD", "Add"],
                            ["SUB", "Sub"]
                        ],
                        selectedKey: currentSelected1,
                        onSelection: this.operandSelectionHandlerFloating
                    },
                    "interestType": Application.validation.isNullUndefinedObj(responseData[a].interestType),
                    "uptoTierAmount": Application.validation.isNullUndefinedObj(uptoTierAmount),
                    "lstFloatingrate": {
                        "masterData": listdata,
                        selectedKey: currentSelected[0],
                        onSelection: this.floatingIndexSelectionCallback
                    },
                    "lblInterestRate": Application.validation.isNullUndefinedObj(correctData_InterestRate)
                };
                dataArray.push(json_data);
            }
            var widgetData = {
                "lstFloatingrate": "lstFloatingrate",
                "lblInterestRate": "lblInterestRate",
                "lstOperand": "marginOperand",
                "lblMargin": "interestMargin",
                "lblEffectiveRate": "interestRate",
                "lblUptoAmount": "uptoTierAmount"
            };
            this.view.segFloating.rowTemplate = "flxSegLoanFloating";
            this.view.segFloating.widgetDataMap = widgetData;
            this.view.segFloating.setData(dataArray);
            this.view.flxFloatingModeWrapper.isVisible = true;
            this.view.flxFloatingModeWrapper.forceLayout();
            floatingData = dataArray;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController loadingDataToFloatingMode:::" + err);
        }
    },
    operandSelectionHandlerFixed: function(widget, context) {
        try {
            var eventobj = widget.selectedKey;
            var selectedContext = context["rowIndex"];
            var segData = fixedData[selectedContext];
            var operandData = segData.marginOperand.selectedKey;
            kony.print("operandData--->" + operandData);
            var marginRate = segData.interestMargin;
            var currentRate = Application.validation.isNullUndefinedNumber(marginRate.text);
            var fixed = segData.fixedRate; //interestRate;//changed to fixedRate from interestRate
            var fixedRemoved = Application.validation.isNullUndefinedNumber(fixed.text);
            var effectiveRate = "";
            kony.print("parseFloat(fixedRemoved)::" + parseFloat(fixedRemoved));
            kony.print("parseFloat(currentRate)::" + parseFloat(currentRate));
            if (operandData === "ADD") {
                effectiveRate = parseFloat(fixedRemoved) + parseFloat(currentRate);
            } else if (operandData === "SUB") {
                effectiveRate = parseFloat(fixedRemoved) - parseFloat(currentRate);
            } else if (operandData === "select") { //added this block on 02/03/2021
                effectiveRate = parseFloat(fixedRemoved);
            }
            kony.print("effectiveRate changed::" + effectiveRate);
            var filter_effectiveRate = "";
            if (effectiveRate === null || effectiveRate === undefined || effectiveRate === "" || isNaN(effectiveRate) === true) {
                kony.print("effectiveRate changed_if_inside::" + effectiveRate);
                filter_effectiveRate = "";
            } else {
                filter_effectiveRate = effectiveRate.toFixed(2);
                kony.print("effectiveRate changed_else::" + effectiveRate);
            }
            var json_data = {
                "interestRate": "" + Application.validation.isNullUndefinedObj(filter_effectiveRate),
                "marginOperand": {
                    masterData: [
                        ["select", "Please Select"],
                        ["ADD", "Add"],
                        ["SUB", "Sub"]
                    ],
                    selectedKey: eventobj,
                    onSelection: this.operandSelectionHandlerFixed
                }, //responseData[a].marginOperand,
                "interestType": Application.validation.isNullUndefinedObj(segData.interestType),
                "uptoTierAmount": Application.validation.isNullUndefinedObj(segData.uptoTierAmount),
                "interestMargin": {
                    text: Application.validation.isNullUndefinedObj(segData.interestMargin.text),
                    onEndEditing: this.calculateEffectiveRateFixed,
                    onDone: this.calculateEffectiveRateFixed
                },
                "fixedRate": {
                    text: Application.validation.isNullUndefinedObj(segData.fixedRate.text),
                    onEndEditing: this.calculateEffectiveRateFixed,
                    onDone: this.calculateEffectiveRateFixed
                },
            };
            kony.print(json_data);
            var currentIndex = Application.validation.isNullUndefinedObj(selectedContext);
            this.view.segFixed.setDataAt(json_data, currentIndex);
            this.view.flxFixedModeWrapper.forceLayout();
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController operandSelectionHandlerFixed:::" + err);
        }
    },
    operandSelectionHandlerFloating: function(widget, context) {
        try {
            var eventobj = widget.selectedKey;
            var selectedContext = context["rowIndex"];
            var segData = floatingData[selectedContext];
            //additioanl code -start
            var operandData = segData.marginOperand.selectedKey;
            kony.print("operandData--->" + operandData);
            var marginRate = segData.interestMargin;
            var currentRate = Application.validation.isNullUndefinedNumber(marginRate.text);
            var fixed = segData.lblInterestRate;
            var fixedRemoved = Application.validation.isNullUndefinedNumber(fixed);
            var effectiveRate = "";
            kony.print("fixedRemoved::" + fixedRemoved);
            if (currentRate === null || currentRate === undefined || currentRate === "" || currentRate === 0 || isNaN(currentRate) === true) {
                effectiveRate = "";
            } else {
                if (fixedRemoved === null || fixedRemoved === undefined || fixedRemoved === "" || fixedRemoved === 0 || isNaN(fixedRemoved) === true) {
                    effectiveRate = "";
                } else {
                    if (operandData === "ADD") {
                        effectiveRate = parseFloat(fixedRemoved) + parseFloat(currentRate);
                    } else if (operandData === "SUB") {
                        effectiveRate = parseFloat(fixedRemoved) - parseFloat(currentRate);
                    } else if (operandData === "select") {
                        effectiveRate = parseFloat(fixedRemoved);
                    }
                }
            }
            var filter_effectiveRate;
            if (effectiveRate === null || effectiveRate === undefined || effectiveRate === "" || isNaN(effectiveRate) === true) {
                kony.print("effectiveRate changed_if_inside::" + effectiveRate);
                filter_effectiveRate = "";
            } else {
                filter_effectiveRate = effectiveRate.toFixed(2);
                kony.print("effectiveRate changed_else::" + effectiveRate);
            }
            //additioanl code -end
            var listdata = [];
            listdata = this.loadingDynamicDataFloatingRate();
            var json_data = {
                "lblInterestRate": Application.validation.isNullUndefinedObj(segData.lblInterestRate),
                "interestMargin": {
                    text: Application.validation.isNullUndefinedObj(segData.interestMargin.text),
                    onEndEditing: this.calculateEffectiveRateFloating,
                    onDone: this.calculateEffectiveRateFloating
                },
                "interestRate": Application.validation.isNullUndefinedObj(filter_effectiveRate),
                "marginOperand": {
                    masterData: [
                        ["select", "Please Select"],
                        ["ADD", "Add"],
                        ["SUB", "Sub"]
                    ],
                    selectedKey: eventobj,
                    onSelection: this.operandSelectionHandlerFloating
                },
                "interestType": Application.validation.isNullUndefinedObj(segData.interestType),
                "uptoTierAmount": Application.validation.isNullUndefinedObj(segData.uptoTierAmount),
                "lstFloatingrate": {
                    "masterData": listdata,
                    selectedKey: segData.lstFloatingrate.selectedKey,
                    onSelection: this.floatingIndexSelectionCallback
                },
            };
            kony.print("segDataAt::" + json_data);
            var currentIndex = Application.validation.isNullUndefinedObj(selectedContext);
            this.view.segFloating.setDataAt(json_data, currentIndex);
            this.view.flxFloatingModeWrapper.forceLayout();
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController operandSelectionHandlerFloating:::" + err);
        }
    },
    floatingIndexSelectionCallback: function(widget, context) {
        try {
            kony.print("#####ListBox on Selection Trigger####");
            var eventobj = widget.selectedKey;
            var listdata = this.loadingDynamicDataFloatingRate();
            var selectedContext = context["rowIndex"];
            var segData = floatingData[selectedContext];
            kony.print("segData_Floating----" + JSON.stringify(segData));
            var exactInterestRate = this.calculateInterestBasedOnFloatingIndex(eventobj, currentCurrency);
            var exactInterestRate_tofixed = parseFloat(exactInterestRate);
            kony.print("exactInterestRate::::" + exactInterestRate);
            kony.print("exactInterestRate_tofixed::::" + exactInterestRate_tofixed);
            var marginOperandData = segData.marginOperand.selectedKey;
            var final_InterestRate;
            if (exactInterestRate_tofixed === null || exactInterestRate_tofixed === undefined || exactInterestRate_tofixed === "" || isNaN(exactInterestRate_tofixed) === true) {
                final_InterestRate = "";
            } else {
                final_InterestRate = exactInterestRate_tofixed.toFixed(2);
            }
            var effective;
            if (final_InterestRate !== "") {
                var marginRate = segData.interestMargin;
                var currentRate = Application.validation.isNullUndefinedNumber(marginRate.text);
                if (marginOperandData === "select") {
                    effective = final_InterestRate
                } else if (marginOperandData === "ADD") {
                    effective = parseFloat(final_InterestRate) + parseFloat(currentRate);
                } else if (marginOperandData === "SUB") {
                    effective = parseFloat(final_InterestRate) - parseFloat(currentRate);
                }
                kony.print("effective::" + effective);
            } else {
                effective = "";
            }
            var json_data = {
                "lblInterestRate": Application.validation.isNullUndefinedObj(final_InterestRate),
                "interestMargin": {
                    text: Application.validation.isNullUndefinedObj(segData.interestMargin.text),
                    onEndEditing: this.calculateEffectiveRateFloating,
                    onDone: this.calculateEffectiveRateFloating
                },
                "interestRate": "" + effective, //(segData.interestRate),
                "marginOperand": {
                    masterData: [
                        ["select", "Please Select"],
                        ["ADD", "Add"],
                        ["SUB", "Sub"]
                    ],
                    selectedKey: segData.marginOperand.selectedKey,
                    onSelection: this.operandSelectionHandlerFloating
                },
                "interestType": Application.validation.isNullUndefinedObj(segData.interestType),
                "uptoTierAmount": Application.validation.isNullUndefinedObj(segData.uptoTierAmount),
                "lstFloatingrate": {
                    "masterData": listdata,
                    selectedKey: eventobj,
                    onSelection: this.floatingIndexSelectionCallback
                },
            };
            kony.print("SegDataAt::" + JSON.stringify(json_data));
            this.view.segFloating.setDataAt(json_data, selectedContext);
            this.view.flxFloatingModeWrapper.forceLayout();
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController floatingIndexSelectionCallback:::" + err);
        }
    },
    calculateEffectiveRateFixed: function(widgetRefTxtBx, segmentIndices) {
        try {
            //segmentIndices.rowIndex =0
            //segmentIndices.sectionIndex = 0
            // var selectedRowIndex = Application.validation.isNullUndefinedObj(this.view.segFixed.selectedRowIndex);
            // kony.print("selectedRowIndex::"+JSON.stringify(selectedRowIndex));
            // var selectData = this.view.segFixed.selectedRowItems;
            var selectData1 = this.view.segFixed.data[segmentIndices.rowIndex];
            kony.print("selectData::" + JSON.stringify(selectData1));
            //if(segmentIndices.rowIndex) {
            var operandData = selectData1.marginOperand.selectedKey;
            var marginRate = selectData1.interestMargin;
            var currentRate = Application.validation.isNullUndefinedNumber(marginRate.text);
            var fixed = selectData1.fixedRate; //interestRate;//changed to fixedRate from interestRate
            var fixedRemoved = Application.validation.isNullUndefinedNumber(fixed.text);
            var effectiveRate = "";
            kony.print("parseFloat(fixedRemoved)::" + parseFloat(fixedRemoved));
            kony.print("parseFloat(currentRate)::" + parseFloat(currentRate));
            if (operandData === "ADD") {
                effectiveRate = parseFloat(fixedRemoved) + parseFloat(currentRate);
            } else if (operandData === "SUB") {
                effectiveRate = parseFloat(fixedRemoved) - parseFloat(currentRate);
            } else if (operandData === "select") {
                effectiveRate = parseFloat(fixedRemoved);
            }
            kony.print("effectiveRate changed::" + effectiveRate);
            var filter_effectiveRate = "";
            if (effectiveRate === null || effectiveRate === undefined || effectiveRate === "" || isNaN(effectiveRate) === true) {
                kony.print("effectiveRate changed_if_inside::" + effectiveRate);
                filter_effectiveRate = "";
            } else {
                filter_effectiveRate = effectiveRate.toFixed(2);
                kony.print("effectiveRate changed_else::" + effectiveRate);
            }
            var dataArray = [];
            var json_data = {
                "interestRate": "" + Application.validation.isNullUndefinedObj(filter_effectiveRate),
                "marginOperand": {
                    masterData: [
                        ["select", "Please Select"],
                        ["ADD", "Add"],
                        ["SUB", "Sub"]
                    ],
                    selectedKey: selectData1.marginOperand.selectedKey,
                    onSelection: this.operandSelectionHandlerFixed
                }, //responseData[a].marginOperand,
                "interestType": Application.validation.isNullUndefinedObj(selectData1.interestType),
                "uptoTierAmount": Application.validation.isNullUndefinedObj(selectData1.uptoTierAmount),
                "interestMargin": {
                    text: Application.validation.isNullUndefinedObj(selectData1.interestMargin.text),
                    onEndEditing: this.calculateEffectiveRateFixed,
                    onDone: this.calculateEffectiveRateFixed
                },
                "fixedRate": {
                    text: Application.validation.isNullUndefinedObj(selectData1.fixedRate.text),
                    onEndEditing: this.calculateEffectiveRateFixed,
                    onDone: this.calculateEffectiveRateFixed
                },
            };
            dataArray.push(json_data);
            kony.print(json_data);
            //var currentIndex = Application.validation.isNullUndefinedObj(selectedRowIndex[0]);
            this.view.segFixed.setDataAt(json_data, segmentIndices.rowIndex);
            this.view.flxFixedModeWrapper.forceLayout();
            // }
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController calculateEffectiveRateFixed:::" + err);
        }
    },
    calculateEffectiveRateFloating: function(widgetRefTxtBx, segmentIndices) {
        try {
            //segmentIndices.rowIndex =0
            //segmentIndices.sectionIndex = 0
            var listdata = this.loadingDynamicDataFloatingRate();
            //kony.print("selectedRowIndex::"+JSON.stringify(selectedRowIndex));
            //var selectData = this.view.segFloating.selectedRowItems; 
            var selectData1 = this.view.segFloating.data[segmentIndices.rowIndex];
            kony.print("selectData::" + JSON.stringify(selectData1));
            // if(selectData.length > 0) {
            var operandData = selectData1.marginOperand.selectedKey;
            var marginRate = selectData1.interestMargin;
            var currentRate = Application.validation.isNullUndefinedNumber(marginRate.text);
            var fixed = selectData1.lblInterestRate;
            var fixedRemoved = Application.validation.isNullUndefinedNumber(fixed);
            var effectiveRate = "";
            kony.print("fixedRemoved::" + fixedRemoved);
            if (fixedRemoved === null || fixedRemoved === undefined || fixedRemoved === "" || fixedRemoved === 0 || isNaN(fixedRemoved) === true) {
                effectiveRate = "";
            } else {
                if (operandData === "ADD") {
                    effectiveRate = parseFloat(fixedRemoved) + parseFloat(currentRate);
                } else if (operandData === "SUB") {
                    effectiveRate = parseFloat(fixedRemoved) - parseFloat(currentRate);
                } else if (operandData === "select") {
                    effectiveRate = parseFloat(fixedRemoved);
                }
            }
            kony.print("effectiveRate ::" + effectiveRate);
            var filter_effectiveRate;
            if (effectiveRate === null || effectiveRate === undefined || effectiveRate === "" || isNaN(effectiveRate) === true) {
                kony.print("effectiveRate changed_if_inside::" + effectiveRate);
                filter_effectiveRate = "";
            } else {
                filter_effectiveRate = effectiveRate.toFixed(2);
                kony.print("effectiveRate changed_else::" + effectiveRate);
            }
            var dataArray = [];
            var json_data = {
                "interestRate": "" + Application.validation.isNullUndefinedObj(filter_effectiveRate),
                "marginOperand": {
                    masterData: [
                        ["select", "Please Select"],
                        ["ADD", "Add"],
                        ["SUB", "Sub"]
                    ],
                    selectedKey: selectData1.marginOperand.selectedKey,
                    onSelection: this.operandSelectionHandlerFloating
                }, //responseData[a].marginOperand,
                "interestType": Application.validation.isNullUndefinedObj(selectData1.interestType),
                "uptoTierAmount": Application.validation.isNullUndefinedObj(selectData1.uptoTierAmount),
                "interestMargin": {
                    text: Application.validation.isNullUndefinedObj(selectData1.interestMargin.text),
                    onEndEditing: this.calculateEffectiveRateFloating,
                    onDone: this.calculateEffectiveRateFloating
                },
                "lblInterestRate": Application.validation.isNullUndefinedObj(selectData1.lblInterestRate),
                "lstFloatingrate": {
                    "masterData": listdata,
                    selectedKey: selectData1.lstFloatingrate.selectedKey,
                    onSelection: this.floatingIndexSelectionCallback
                },
                //"fixedRate" : Application.validation.isNullUndefinedObj(selectData1.fixedRate)
            };
            dataArray.push(json_data);
            // var currentIndex = Application.validation.isNullUndefinedObj(selectedRowIndex[0]);
            kony.print("segDataAt::" + json_data);
            this.view.segFloating.setDataAt(json_data, segmentIndices.rowIndex);
            this.view.flxFloatingModeWrapper.forceLayout();
            //  }
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController calculateEffectiveRateFloating:::" + err);
        }
    },
    callingModeBasedServiceCall: function(fixed, floating) {
        try {
            var self = this;
            //this.view.cusTierListResponse.disabled = true;
            this.view.cusTierListResponse.labelText = "Tier Type";
            //this.view.cusTierListResponse.value = "";
            this.view.cusRadioButtonSelectionModeValue.options = [{
                "label": "Fixed",
                "value": 1
            }, {
                "label": "Floating",
                "value": 2
            }];
            if (floating.length > 0) {
                this.view.cusRadioButtonSelectionModeValue.value = 2;
                self.loadingDataToFloatingMode(floating);
                kony.print("onChange floating::" + JSON.stringify(floating));
                changeLoanModeValue = floating;
            }
            if (fixed.length > 0) {
                this.view.cusRadioButtonSelectionModeValue.value = 1;
                self.loadingDataToFixedMode(fixed);
                kony.print("onChange fixed::" + JSON.stringify(fixed));
                changeLoanModeValue = fixed;
            }
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController callingModeBasedServiceCall:::" + err);
        }
    },
    getInterestRateBasedFloatingIndex: function() {
        try {
            var currentCurrencyData = Application.validation.isNullUndefinedObj(currentCurrency);
            kony.print("#####currentCurrency#####" + currentCurrencyData);
            var serviceName = "LendingNew";
            var operationName = "getInterestConditions";
            var headers = {
                "currency": currentCurrencyData
            };
            var inputParams = {};
            //Application.loader.showLoader("Please Wait...");
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.interestRateSuccessCallBackfunc, this.interestRateFailureCallBackfunc);
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController getInterestRateBasedFloatingIndex:::" + err);
        }
    },
    interestRateSuccessCallBackfunc: function(res) {
        try {
            // Application.loader.dismissLoader();
            this.view.ProgressIndicator.isVisible = false;
            this.callingLoanServiceDetails(); // calling real time data after rate
            kony.print("response::" + JSON.stringify(res));
            floatingInterestRateAPI = [];
            var responseData = res.body;
            if (responseData.length > 0) {
                for (var a in responseData) {
                    var json = {
                        "rate": responseData[a].rate,
                        "key": responseData[a].key,
                        "currency": responseData[a].currency,
                        "description": responseData[a].description
                    };
                    floatingInterestRateAPI.push(json);
                }
            }
            kony.print("floatingInterestRateAPI:::" + JSON.stringify(floatingInterestRateAPI));
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController callingModeBasedServiceCall:::" + err);
        }
    },
    interestRateFailureCallBackfunc: function(res) {
        try {
            //Application.loader.dismissLoader();
            this.view.ProgressIndicator.isVisible = false;
            alert("Service Faliure");
            kony.print("Service Faliure:::::" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController callingModeBasedServiceCall:::" + err);
        }
    },
    appendingFloatingRate: function(res) {
        try {
            kony.print("res in appendingFloatingRate" + res);
            var resData = Application.validation.isNullUndefinedObj(res);
            var response = [];
            if (resData === undefined || resData === null || resData === "") {
                response[0] = "select";
                response[1] = "";
            } else {
                response[0] = resData;
                response[1] = this.calculateInterestBasedOnFloatingIndex(resData, currentCurrency);
            }
            kony.print("response in appendingFloatingRate" + JSON.stringify(response));
            return response;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController appendingFloatingRate:::" + err);
        }
    },
    checkOperandData: function(res) {
        try {
            var resData = Application.validation.isNullUndefinedObj(res);
            var response;
            if (resData === undefined || resData === null || resData === "") {
                response = "select";
            } else {
                response = resData;
            }
            return response;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController checkOperandData:::" + err);
        }
    },
    checkOperandDataSelectedKey: function(res) {
        try {
            var resData = Application.validation.isNullUndefinedObj(res);
            var response;
            if (resData === undefined || resData === null || resData === "" || resData === "select") {
                response = "";
            } else {
                response = resData;
            }
            return response;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController checkOperandData:::" + err);
        }
    },
    calculateInterestBasedOnFloatingIndex: function(res, currentCurrency) {
        try {
            kony.print("calculateInterestBasedOnFloatingIndex____res:::" + res + "***currentCurrency" + currentCurrency);
            var currentInterestRate = "";
            if (currentCurrency === "" || currentCurrency === null) {
                kony.print("No Currency Found");
            } else {
                for (var a in floatingInterestRateAPI) {
                    if (res === floatingInterestRateAPI[a].key && currentCurrency === floatingInterestRateAPI[a].currency) {
                        currentInterestRate = floatingInterestRateAPI[a].rate;
                    }
                }
            }
            kony.print("currentInterestRate in calculateInterestBasedOnFloatingIndex" + JSON.stringify(currentInterestRate));
            return currentInterestRate;
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController calculateInterestBasedOnFloatingIndex:::" + err);
        }
    },
    onClickSubmitData: function() {
        try {
            this.view.ErrorAllert.isVisible = false;
            this.view.SegmentPopup.isVisible = false;
            kony.print("###### Button Event Triggered ######");
            var options = this.view.cusRadioButtonSelectionModeValue.value;
            var currentData = this.view.cusEffectiveDateResponse.value;
            kony.print("currentData:::" + currentData);
            var systemDate = this.formatDate(currentData);
            kony.print("systemDate_current:::" + systemDate);
            var operationName = "postPersonalChangeInterest";
            var operationName1 = "postMortgageInterestChange";
            var serviceName = "LendingNew";
            var headers = {
                "companyId": "NL0020001",
                "Content-Type": "application/json"
            };
            kony.print("this.currentOverride----->" + JSON.stringify(this.currentOverride));
            var inputParams = {};
            kony.print("options:::" + options);
            if (options === 1) {
                var segData = this.view.segFixed.data;
                var dataArray = [];
                if (segData.length > 0) {
                    for (var a in segData) {
                        var operandData = this.checkOperandDataSelectedKey(segData[a].marginOperand.selectedKey);
                        if (operandData === "" || operandData === null || operandData === undefined) {
                            var json = {
                                "upTo": segData[a].uptoTierAmount,
                                "margin": "null",
                                "marginRate": "null", //segData[a].interestMargin.text,
                                "marginOperation": "null", //operandData,
                                "fixed": segData[a].fixedRate.text,
                                "effectiveRate": segData[a].interestRate,
                                "floating": ""
                            };
                        } else {
                            var json = {
                                "upTo": segData[a].uptoTierAmount,
                                "margin": "SINGLE",
                                "marginRate": segData[a].interestMargin.text,
                                "marginOperation": operandData,
                                "fixed": segData[a].fixedRate.text,
                                "effectiveRate": segData[a].interestRate,
                                "floating": ""
                            };
                        }
                        dataArray.push(json);
                    }
                    kony.print("dataArray:::" + JSON.stringify(dataArray));
                    kony.print("checking####" + currentLoanType.indexOf("personal"));
                    inputParams = {
                        "interest": dataArray,
                        "arrangmentId": arrangementId,
                        "arrangementEffectiveDate": systemDate, // Hided this part, as of now. [Due to Transact] 
                        "overrideDetails": this.currentOverride,
                    };
                    //Application.loader.showLoader("Please Wait...");
                    this.view.ProgressIndicator.isVisible = true;
                    if (currentLoanType === "PERSONAL.LOAN.STANDARD") {
                        kony.print("personal---->1");
                        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.submitSuccessCallbackFunc, this.submitFailureCallbackFunc);
                    } else if (currentLoanType === "MORTGAGE") {
                        kony.print("mortgage---->1");
                        MFHelper.Utils.commonMFServiceCall(serviceName, operationName1, headers, inputParams, this.submitSuccessCallbackFunc, this.submitFailureCallbackFunc);
                    } else {
                        alert("Loan Type Not allowed to increase commitment");
                        this.view.ProgressIndicator.isVisible = false;
                    }
                }
            }
            if (options === 2) {
                var segData1 = this.view.segFloating.data;
                var dataArray1 = [];
                if (segData1.length > 0) {
                    for (var b in segData1) {
                        var selectKeyData = segData1[b].lstFloatingrate.selectedKey;
                        if (selectKeyData === "select" || selectKeyData === null || selectKeyData === undefined) {
                            selectKeyData = "";
                        }
                        var operandData1 = this.checkOperandDataSelectedKey(segData1[b].marginOperand.selectedKey);
                        if (operandData1 === "" || operandData1 === null || operandData1 === undefined) {
                            var json1 = {
                                "upTo": segData1[b].uptoTierAmount,
                                "margin": "null",
                                "marginRate": "null", //segData1[b].interestMargin.text,
                                "marginOperation": "null", //operandData1,
                                "fixed": "",
                                "effectiveRate": segData1[b].interestRate,
                                "floating": Application.validation.isNullUndefinedObj(selectKeyData)
                            };
                        } else {
                            var json1 = {
                                "upTo": segData1[b].uptoTierAmount,
                                "margin": "SINGLE",
                                "marginRate": segData1[b].interestMargin.text,
                                "marginOperation": operandData1,
                                "fixed": "",
                                "effectiveRate": segData1[b].interestRate,
                                "floating": Application.validation.isNullUndefinedObj(selectKeyData)
                            };
                        }
                        dataArray1.push(json1);
                    }
                    kony.print("dataArray1:::" + JSON.stringify(dataArray1));
                    inputParams = {
                        "interest": dataArray1,
                        "arrangmentId": arrangementId,
                        "arrangementEffectiveDate": systemDate, // Hided this part, as of now. [Due to Transact] ,
                        "overrideDetails": this.currentOverride,
                    };
                    //Application.loader.showLoader("Please Wait...");
                    this.view.ProgressIndicator.isVisible = true;
                    if (currentLoanType === "PERSONAL.LOAN.STANDARD") {
                        kony.print("personal---->2");
                        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.submitSuccessCallbackFunc, this.submitFailureCallbackFunc);
                    } else if (currentLoanType === "MORTGAGE") {
                        kony.print("mortgage---->2");
                        MFHelper.Utils.commonMFServiceCall(serviceName, operationName1, headers, inputParams, this.submitSuccessCallbackFunc, this.submitFailureCallbackFunc);
                    } else {
                        alert("Loan Type Not allowed to increase commitment");
                        this.view.ProgressIndicator.isVisible = false;
                    }
                }
            }
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController populatingTierTypeDropDown:::" + err);
        }
    },
    submitSuccessCallbackFunc: function(res) {
        try {
            //Application.loader.dismissLoader();
            this.currentOverride = {};
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success CallBack :::" + JSON.stringify(res));
            if (res === "" || res === null || res === undefined) {
                kony.print("Response is null or undefined");
            } else {
                var resId = res.header;
                kony.print("RES___ID***" + JSON.stringify(resId));
                var data_id = Application.validation.isNullUndefinedObj(resId.aaaId);
                //this.view.lblMessageWithReferenceNumber.text = "Interest Change applied successfully!      Reference : Interest Change applied successfully!      Reference : "+data_id;
                var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
                var navObjet = {};
                navObjet.customerId = this._loanObj.customerId;
                navObjet.isShowLoans = "true";
                navObjet.isPopupShow = "true";
                navObjet.messageInfo = "Interest Change applied successfully!     Reference : " + data_id;
                navToChangeInterest1.navigate(navObjet);
            }
            //this.view.flxHeaderMenu.isVisible = false;
            //this.view.flxMessageInfo.isVisible = true;
            //this.callingLoanServiceDetails(); //Calling GET to update the value.
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController submitSuccessCallbackFunc:::" + err);
        }
    },
    submitFailureCallbackFunc: function(res) {
        try {
            //Application.loader.dismissLoader();
            //alert("Service failure");
            this.currentOverride = {};
            kony.print("Service Failure:::" + JSON.stringify(res));
            this.view.ProgressIndicator.isVisible = false;
            var errorData = Application.validation.isNullUndefinedObj(res.error);
            var genericError = Application.validation.isNullUndefinedObj(res.errmsg);
            if (errorData !== "") {
                this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message;
            } else if (genericError !== "") {
                /**if(genericError === "Backend request failed for service postMortgageInterestChange with HTTP status code 400.") 
                {**/
                this.view.ErrorAllert.lblMessage.text = "Unable to process, please try again";
                /**} else {
                  this.view.ErrorAllert.lblMessage.text = res.errmsg;
                }**/
            }
            this.view.ErrorAllert.isVisible = true;
            if (res.override !== "" && res.override !== undefined) {
                var overwriteDetails = [];
                var overWriteData = [];
                var segDataForOverrite = [];
                this.currentOverride = res.override.overrideDetails;
                overwriteDetails = res.override.overrideDetails;
                if (overwriteDetails.length > 0) {
                    for (var a in overwriteDetails) {
                        overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                        var json = {
                            "lblSerial": "*",
                            "lblInfo": overwriteDetails[a].description
                        }
                        segDataForOverrite.push(json);
                    }
                    this.view.SegmentPopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
                    this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
                    this.view.SegmentPopup.isVisible = true;
                    this.view.ErrorAllert.lblMessage.text = "";
                    this.view.ErrorAllert.isVisible = false;
                    kony.print("overWriteData::::" + JSON.stringify(overWriteData));
                    kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
                    this.view.forceLayout();
                }
            }
            //kony.print("Service Failure:::"+JSON.stringify(res.error.errorDetails));
            this.view.flxHeaderMenu.isVisible = false;
            this.view.flxMessageInfo.isVisible = false;
            this.view.scrollToBeginning();
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController submitFailureCallbackFunc:::" + err);
        }
    },
    getCurrentDate: function(transactDate) {
        try {
            var today = new Date(transactDate);
            var dd = today.getDate();
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var todayConverted = dd + " " + shortMonth + " " + yyyy;
            this.view.cusEffectiveDateResponse.displayFormat = "dd MMM yyyy";
            this.view.cusEffectiveDateResponse.value = todayConverted;
            this.view.cusEffectiveDateResponse.min = todayConverted;
            kony.print("todayConverted--->" + todayConverted);
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController getCurrentDate:::" + err);
        }
    },
    formatDate: function(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return year + "" + month + "" + day;
    },
    validateSegData: function() {
        try {
            var isValid = false;
            var currentData = this.view.cusEffectiveDateResponse.value;
            kony.print("currentData:::" + currentData);
            var systemDate = this.formatDate(currentData);
            kony.print("systemDate_current:::" + systemDate);
            var options = this.view.cusRadioButtonSelectionModeValue.value;
            kony.print("options:::" + options);
            if (options === 1) {
                var segData = this.view.segFixed.data;
                if (segData.length > 0) {
                    kony.print("segData[a].fixedRate.text:::" + JSON.stringify(segData));
                    for (var a in segData) {
                        if (segData[a].fixedRate.text === "" || segData[a].fixedRate.text === null || segData[a].fixedRate.text === undefined) {
                            kony.print("segData[a].fixedRate.text:::" + segData[a].fixedRate.text);
                            isValid = true;
                        } else {
                            isValid = false;
                        }
                    }
                }
            } else if (options === 2) {
                var segData1 = this.view.segFloating.data;
                if (segData1.length > 0) {
                    for (var b in segData1) {
                        if (segData1[b].lstFloatingrate.selectedKey === "" || segData1[b].lstFloatingrate.selectedKey === null || segData1[b].lstFloatingrate.selectedKey === undefined || segData1[b].lstFloatingrate.selectedKey === "select") {
                            kony.print("segData1[b].lstFloatingrate.selectedKey:::" + segData1[b].lstFloatingrate.selectedKey);
                            isValid = true;
                        } else {
                            isValid = false;
                        }
                    }
                }
            }
            if (isValid) {
                this.view.lblErrorMsg.isVisible = true;
                this.view.lblErrorMsg.text = "Please fill the mandatory field";
                this.view.flxMainContainer.forceLayout();
                //alert("Please fill the mandatory field");
            } else {
                this.view.lblErrorMsg.isVisible = false;
                this.onClickSubmitData();
            }
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController validateSegData:::" + err);
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    }
});
define("frmCustomerLoanInterestChangeControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_b6da1c9231af46438a5fb2acd9dacea7: function AS_UWI_b6da1c9231af46438a5fb2acd9dacea7(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_bbd783e212c6498b83b01a8438674b10: function AS_UWI_bbd783e212c6498b83b01a8438674b10(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmCustomerLoanInterestChangeController", ["userfrmCustomerLoanInterestChangeController", "frmCustomerLoanInterestChangeControllerActions"], function() {
    var controller = require("userfrmCustomerLoanInterestChangeController");
    var controllerActions = ["frmCustomerLoanInterestChangeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
