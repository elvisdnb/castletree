define("frmLoanCreation", function() {
    return function(controller) {
        function addWidgetsfrmLoanCreation() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "6.50%",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "0dp",
                "width": "90%",
                "zIndex": 9
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoansSuccessMessage = new kony.ui.Label({
                "height": "20dp",
                "id": "lblLoansSuccessMessage",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BG",
                "text": "New loan account 51237781231 created for John Doe",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblLoansSuccessMessage);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "pagingEnabled": false,
                "right": "24dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxContainerBGF0hc984c66b6a545",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var FlexContainer0d8781aeee9e447 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "968dp",
                "id": "FlexContainer0d8781aeee9e447",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "98%",
                "zIndex": 9
            }, {}, {});
            FlexContainer0d8781aeee9e447.setDefaultUnit(kony.flex.DP);
            var flxMainWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "50dp",
                "clipBounds": true,
                "id": "flxMainWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "300dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverview = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverview",
                "isVisible": true,
                "left": "120dp",
                "skin": "CopysknLbl0d3dccf1b0dea4f",
                "text": "Overview",
                "top": "26dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadboardHolidayPaymentSplash = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadboardHolidayPaymentSplash",
                "isVisible": true,
                "left": "180dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "26dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardLoanCreation = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardLoanCreation",
                "isVisible": true,
                "left": "200dp",
                "skin": "sknLblLoanServiceBlacl12px",
                "text": "Loan Creation",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverview, cusBackIcon, lblBreadboardHolidayPaymentSplash, lblBreadBoardLoanCreation);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "24dp",
                "skin": "sknFlxBorderCCE3F7",
                "top": "8dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxLoanCreationTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": false,
                "height": "450dp",
                "id": "flxLoanCreationTab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a0d1087168f844",
                "top": "5dp",
                "width": "98%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxLoanCreationTab.setDefaultUnit(kony.flex.DP);
            var flxTopWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxTopWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxTopWrapper.setDefaultUnit(kony.flex.DP);
            var flxNewLoanTitles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNewLoanTitles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNewLoanTitles.setDefaultUnit(kony.flex.DP);
            var flxLoansTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxLoansTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "28dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3dp",
                "width": "201dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxLoansTitle.setDefaultUnit(kony.flex.DP);
            var lblNewLoans = new kony.ui.Label({
                "height": "29dp",
                "id": "lblNewLoans",
                "isVisible": true,
                "left": "0",
                "skin": "primary24px",
                "text": "New Loan Creation",
                "top": "0",
                "width": "207dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerName = new kony.ui.Label({
                "id": "lblCustomerName",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0g1225c742c9844",
                "text": "for John Doe",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoansTitle.add(lblNewLoans, lblCustomerName);
            var flxLoanSteps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxLoanSteps",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "122dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "550dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxLoanSteps.setDefaultUnit(kony.flex.DP);
            var flxStep1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "146.15%",
                "id": "flxStep1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-25dp",
                "width": "140px"
            }, {}, {});
            flxStep1.setDefaultUnit(kony.flex.DP);
            var flxStepClick = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStepClick",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "skinBlue",
                "top": "0",
                "width": "40dp"
            }, {}, {});
            flxStepClick.setDefaultUnit(kony.flex.DP);
            var TPWFlag1 = new kony.ui.CustomWidget({
                "id": "TPWFlag1",
                "isVisible": true,
                "left": "0",
                "top": "0",
                "width": "24px",
                "height": "24px",
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-light",
                "iconName": "flag",
                "size": "small"
            });
            flxStepClick.add(TPWFlag1);
            var Label0f0949fa7b1ca45 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "Label0f0949fa7b1ca45",
                "isVisible": true,
                "left": "10dp",
                "skin": "primary14pxregular",
                "text": "Basic details",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStep1.add(flxStepClick, Label0f0949fa7b1ca45);
            var flxArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxArrow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "5dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "135dp"
            }, {}, {});
            flxArrow.setDefaultUnit(kony.flex.DP);
            var TPWArrow = new kony.ui.CustomWidget({
                "id": "TPWArrow",
                "isVisible": false,
                "left": "0",
                "top": "0",
                "width": "120px",
                "height": "50px",
                "centerX": "50.00%",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-primary",
                "iconName": "trending_flat",
                "size": "large"
            });
            var imgLoanArrow = new kony.ui.Image2({
                "height": "40dp",
                "id": "imgLoanArrow",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "loanarrow.png",
                "top": "0",
                "width": "130dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrow.add(TPWArrow, imgLoanArrow);
            var flxLoanSpecifies = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoanSpecifies",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": -12,
                "width": "150px"
            }, {}, {});
            flxLoanSpecifies.setDefaultUnit(kony.flex.DP);
            var flxStepClick2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStepClick2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "stepUnActive",
                "top": "0",
                "width": "40dp"
            }, {}, {});
            flxStepClick2.setDefaultUnit(kony.flex.DP);
            var TPWFlag2 = new kony.ui.CustomWidget({
                "id": "TPWFlag2",
                "isVisible": true,
                "left": "0",
                "top": "0",
                "width": "24px",
                "height": "24px",
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-primary",
                "iconName": "flag",
                "size": "small"
            });
            flxStepClick2.add(TPWFlag2);
            var lblLoanSpecifies = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblLoanSpecifies",
                "isVisible": true,
                "left": "10dp",
                "skin": "primary14pxregular",
                "text": "Loan specifics",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoanSpecifies.add(flxStepClick2, lblLoanSpecifies);
            flxLoanSteps.add(flxStep1, flxArrow, flxLoanSpecifies);
            flxNewLoanTitles.add(flxLoansTitle, flxLoanSteps);
            var flxBasicDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "centerX": "50%",
                "clipBounds": false,
                "height": "230dp",
                "id": "flxBasicDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "83dp",
                "width": "1100dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxBasicDetails.setDefaultUnit(kony.flex.DP);
            var flxTopRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTopRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 45,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "57%",
                "zIndex": 1
            }, {}, {});
            flxTopRow.setDefaultUnit(kony.flex.DP);
            var flxTopBenRows = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTopBenRows",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "maxHeight": "200dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxTopBenRows.setDefaultUnit(kony.flex.DP);
            var flxBenRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxBenRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxBenRow.setDefaultUnit(kony.flex.DP);
            var TPWBenName = new kony.ui.CustomWidget({
                "id": "TPWBenName",
                "isVisible": true,
                "left": "24dp",
                "top": "5px",
                "width": "295px",
                "height": "56px",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "search",
                "labelText": "Beneficiary Name",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": true,
                "showIconButtonTrailing": true,
                "showIconButtonTrailingWithValue": false,
                "value": "John Doe"
            });
            var TPWBenRole = new kony.ui.CustomWidget({
                "id": "TPWBenRole",
                "isVisible": true,
                "left": "32dp",
                "top": "5px",
                "width": "295dp",
                "height": "56px",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "search"
                },
                "iconButtonIcon": "",
                "labelText": "Role",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": "Beneficial Owner"
            });
            var segAddRow = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }, {
                    "lblBenLabel": "Label",
                    "lblBenLabel1": "Label",
                    "lblRole": "",
                    "lblRole1": ""
                }],
                "groupCells": false,
                "id": "segAddRow",
                "isVisible": false,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAddRowWrap",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ffffff00",
                "separatorRequired": true,
                "separatorThickness": 15,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddRowWrap": "flxAddRowWrap",
                    "flxBenName": "flxBenName",
                    "flxBenRole": "flxBenRole",
                    "flxImgClicks": "flxImgClicks",
                    "flxImgClicks1": "flxImgClicks1",
                    "lblBenLabel": "lblBenLabel",
                    "lblBenLabel1": "lblBenLabel1",
                    "lblRole": "lblRole",
                    "lblRole1": "lblRole1"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBenRow.add(TPWBenName, TPWBenRole, segAddRow);
            flxTopBenRows.add(flxBenRow);
            var flxAddRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddRow",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%"
            }, {}, {});
            flxAddRow.setDefaultUnit(kony.flex.DP);
            var btnRowAdd = new kony.ui.CustomWidget({
                "id": "btnRowAdd",
                "isVisible": true,
                "left": "0",
                "top": "0",
                "width": "150px",
                "height": "40px",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "add_circle",
                "labelText": "Add Row",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxAddRow.add(btnRowAdd);
            flxTopRow.add(flxTopBenRows, flxAddRow);
            var flxRightpart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxRightpart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "340dp",
                "zIndex": 1
            }, {}, {});
            flxRightpart.setDefaultUnit(kony.flex.DP);
            var custLoanType = new kony.ui.CustomWidget({
                "id": "custLoanType",
                "isVisible": true,
                "right": "3dp",
                "top": "55dp",
                "width": "303dp",
                "height": "55px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Loan Type",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": true,
                "showIconButtonTrailing": true,
                "showIconButtonTrailingWithValue": false,
                "value": " "
            });
            var custLoanType1 = new kony.ui.CustomWidget({
                "id": "custLoanType1",
                "isVisible": false,
                "right": "3dp",
                "top": "50dp",
                "width": "300px",
                "height": "65px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Loan Type",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custCurrency = new kony.ui.CustomWidget({
                "id": "custCurrency",
                "isVisible": true,
                "right": "3dp",
                "top": "122dp",
                "width": "303dp",
                "height": "55dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Currency",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            flxRightpart.add(custLoanType, custLoanType1, custCurrency);
            flxBasicDetails.add(flxTopRow, flxRightpart);
            var frmLoanSpecifiesTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "centerX": "50%",
                "clipBounds": false,
                "height": "230dp",
                "id": "frmLoanSpecifiesTab",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "103dp",
                "width": "1100dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            frmLoanSpecifiesTab.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var custLoanAmountUSD = new kony.ui.CustomWidget({
                "id": "custLoanAmountUSD",
                "isVisible": true,
                "left": "32dp",
                "top": "0dp",
                "width": "273dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Loan Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "suffix": "",
                "validateRequired": "required",
                "value": ""
            });
            var custLoanAmountEUR = new kony.ui.CustomWidget({
                "id": "custLoanAmountEUR",
                "isVisible": true,
                "left": "32dp",
                "top": "0dp",
                "width": "273dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Loan Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "suffix": "",
                "validateRequired": "required",
                "value": ""
            });
            var custLoanTerm = new kony.ui.CustomWidget({
                "id": "custLoanTerm",
                "isVisible": true,
                "left": "35%",
                "top": "0dp",
                "width": "273dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "LoanTerm",
                "maxLength": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            var custMaturityDate = new kony.ui.CustomWidget({
                "id": "custMaturityDate",
                "isVisible": false,
                "left": "626dp",
                "top": "0dp",
                "width": "273dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": false,
                "disabled": true,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Maturity date",
                "max": "",
                "min": "1900-01-01",
                "onDone": "",
                "showWeekNumber": false,
                "value": "-",
                "weekLabel": "Wk"
            });
            var custMatDate = new kony.ui.CustomWidget({
                "id": "custMatDate",
                "isVisible": true,
                "left": "67%",
                "top": "0dp",
                "width": "273dp",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Maturity Date",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": " "
            });
            var custPrincipalInterest = new kony.ui.CustomWidget({
                "id": "custPrincipalInterest",
                "isVisible": true,
                "left": "32dp",
                "top": "74dp",
                "width": "273px",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Principal Interest",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": " "
            });
            var custPenaltyInterest = new kony.ui.CustomWidget({
                "id": "custPenaltyInterest",
                "isVisible": true,
                "left": "35%",
                "top": "74dp",
                "width": "273dp",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Penalty Interest",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": " "
            });
            var custRepaymentFrequencyold = new kony.ui.CustomWidget({
                "id": "custRepaymentFrequencyold",
                "isVisible": false,
                "left": "72%",
                "top": "74dp",
                "width": "273dp",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Repayment Frequency",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            var custRepaymentFrequency = new kony.ui.CustomWidget({
                "id": "custRepaymentFrequency",
                "isVisible": true,
                "left": "67%",
                "top": "74dp",
                "width": "297dp",
                "height": "56dp",
                "zIndex": 2,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRecurrenceDatePicker",
                "dailyMax": "",
                "dense": true,
                "formattedValue": "",
                "lableText": "Repayment Frequency",
                "messagePrefix": "",
                "monthlyDayMax": "",
                "monthlyMax": "",
                "value": "",
                "weeklyMax": "",
                "yearlyDayMax": "",
                "yearlyMax": ""
            });
            var custLimitID = new kony.ui.CustomWidget({
                "id": "custLimitID",
                "isVisible": false,
                "left": "825px",
                "top": "70dp",
                "width": "255dp",
                "height": "50px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Limit ID",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var flxpayOutSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayOutSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "58%",
                "isModalContainer": false,
                "skin": "searchflex",
                "top": 148,
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxpayOutSearch.setDefaultUnit(kony.flex.DP);
            var iconPayoutsearch = new kony.ui.CustomWidget({
                "id": "iconPayoutsearch",
                "isVisible": false,
                "left": "0dp",
                "top": "0",
                "width": "40dp",
                "height": "40dp",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-light",
                "iconName": "search",
                "size": "medium"
            });
            var lblSearchIcon1 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblSearchIcon1",
                "isVisible": true,
                "left": "0%",
                "skin": "iconFontPurple20px",
                "text": "k",
                "top": "0",
                "width": "20dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxpayOutSearch.add(iconPayoutsearch, lblSearchIcon1);
            var flxpayInSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayInSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "285dp",
                "isModalContainer": false,
                "skin": "searchflex",
                "top": 148,
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxpayInSearch.setDefaultUnit(kony.flex.DP);
            var iconPayinsearch = new kony.ui.CustomWidget({
                "id": "iconPayinsearch",
                "isVisible": false,
                "left": "0",
                "top": "0",
                "width": "40px",
                "height": "40px",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-light",
                "iconName": "search",
                "size": "medium"
            });
            var lblSearchIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblSearchIcon",
                "isVisible": true,
                "left": 0,
                "skin": "iconFontPurple20px",
                "text": "k",
                "top": "0",
                "width": "20dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxpayInSearch.add(iconPayinsearch, lblSearchIcon);
            var tbxPayOutAccount1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "CopydefTextBoxNormal0e65805a6ac6e46",
                "height": "56px",
                "id": "tbxPayOutAccount1",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "31dp",
                "onTouchStart": controller.AS_TextField_j4b048bc619c4ce1b26bba5669ca874f,
                "placeholder": "PayOut",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0e65805a6ac6e46",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "188dp",
                "width": "233dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "CopydefTextBoxPlaceholder0jb990765c75e41"
            });
            var flxpayout1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayout1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "150dp",
                "width": "220dp",
                "zIndex": 1
            }, {}, {});
            flxpayout1.setDefaultUnit(kony.flex.DP);
            flxpayout1.add();
            var tbxPayInAccount1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "56dp",
                "id": "tbxPayInAccount1",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "329dp",
                "placeholder": "PayIn",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0c202ab592b5649",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "188dp",
                "width": "233dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var tbxPayOutAccount = new com.lending.floatingTextBox.floatLabelText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "tbxPayOutAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "31dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "flxTemenosLighest",
                "top": "148dp",
                "width": "253dp",
                "zIndex": 1,
                "overrides": {
                    "downArrowLbl": {
                        "isVisible": true,
                        "zIndex": 5
                    },
                    "floatLabelText": {
                        "centerX": "viz.val_cleared",
                        "height": "40dp",
                        "isVisible": true,
                        "left": "31dp",
                        "top": "148dp",
                        "width": "253dp",
                        "zIndex": 1
                    },
                    "lblFloatLabel": {
                        "left": "5%",
                        "text": "Pay-out Account "
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var tbxPayInAccount = new com.lending.floatingTextBox.floatLabelText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "tbxPayInAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "flxTemenosLighest",
                "top": "148dp",
                "width": "253dp",
                "zIndex": 1,
                "overrides": {
                    "downArrowLbl": {
                        "isVisible": true,
                        "zIndex": 5
                    },
                    "floatLabelText": {
                        "centerX": "viz.val_cleared",
                        "height": "40dp",
                        "isVisible": true,
                        "left": "35%",
                        "top": "148dp",
                        "width": "253dp",
                        "zIndex": 1
                    },
                    "lblFloatLabel": {
                        "left": "5%",
                        "text": "Pay-in Account "
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxPayinComp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "235dp",
                "id": "flxPayinComp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35%",
                "isModalContainer": false,
                "skin": "CopyslFbox0a1f15f55625746",
                "top": "190dp",
                "width": "400dp",
                "zIndex": 1
            }, {}, {});
            flxPayinComp.setDefaultUnit(kony.flex.DP);
            var AccountList = new com.konymp.AccountList.AccountList({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "235dp",
                "id": "AccountList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxWhiteBGBorder0075db",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "AccountList": {
                        "height": "235dp",
                        "isVisible": true,
                        "width": "100%"
                    },
                    "segAccountList": {
                        "height": "190dp",
                        "top": "40dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPayinComp.add(AccountList);
            var flxPayoutComp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "235dp",
                "id": "flxPayoutComp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fdd0bec932a740",
                "top": "190dp",
                "width": "400dp",
                "zIndex": 2
            }, {}, {});
            flxPayoutComp.setDefaultUnit(kony.flex.DP);
            var AccountList2 = new com.konymp.AccountList2.AccountList2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "235dp",
                "id": "AccountList2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxWhiteBGBorder0075db",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "AccountList2": {
                        "bottom": "viz.val_cleared",
                        "centerX": "50%",
                        "height": "235dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPayoutComp.add(AccountList2);
            flxRow1.add(custLoanAmountUSD, custLoanAmountEUR, custLoanTerm, custMaturityDate, custMatDate, custPrincipalInterest, custPenaltyInterest, custRepaymentFrequencyold, custRepaymentFrequency, custLimitID, flxpayOutSearch, flxpayInSearch, tbxPayOutAccount1, flxpayout1, tbxPayInAccount1, tbxPayOutAccount, tbxPayInAccount, flxPayinComp, flxPayoutComp);
            frmLoanSpecifiesTab.add(flxRow1);
            var lblErrorMsg = new kony.ui.Label({
                "height": "21dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "70dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "320dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "353dp",
                "width": "1100dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxCustButtons.setDefaultUnit(kony.flex.DP);
            var flxSeePayment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSeePayment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "34%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxSeePayment.setDefaultUnit(kony.flex.DP);
            var TPW0hb1a91d98e404c = new kony.ui.CustomWidget({
                "id": "TPW0hb1a91d98e404c",
                "isVisible": true,
                "left": "10dp",
                "top": "15dp",
                "width": "30px",
                "height": "25px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "autorenew",
                "size": "small"
            });
            var lblPaymentSchedule = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPaymentSchedule",
                "isVisible": true,
                "left": "0dp",
                "skin": "primary14pxregular",
                "text": "See payment schedule",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeePayment.add(TPW0hb1a91d98e404c, lblPaymentSchedule);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "46.36%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3dp",
                "width": "620dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var custCancel = new kony.ui.CustomWidget({
                "id": "custCancel",
                "isVisible": true,
                "right": "330dp",
                "top": "0dp",
                "width": "196dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custProceed = new kony.ui.CustomWidget({
                "id": "custProceed",
                "isVisible": true,
                "right": "110dp",
                "top": "0dp",
                "width": "196dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "onclickEvent": controller.AS_TPW_f937666cd8aa4daebf54a21779a11059,
                "labelText": "Proceed",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custSubmit = new kony.ui.CustomWidget({
                "id": "custSubmit",
                "isVisible": true,
                "right": "110dp",
                "top": "0dp",
                "width": "196dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Submit",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxButtons.add(custCancel, custProceed, custSubmit);
            flxCustButtons.add(flxSeePayment, flxButtons);
            flxTopWrapper.add(flxNewLoanTitles, flxBasicDetails, frmLoanSpecifiesTab, lblErrorMsg, flxCustButtons);
            var flxClickEnable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "530dp",
                "id": "flxClickEnable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-210%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 30,
                "width": "105%",
                "zIndex": 1
            }, {}, {});
            flxClickEnable.setDefaultUnit(kony.flex.DP);
            flxClickEnable.add();
            var FlexContainer0g9617a8127cf4c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "FlexContainer0g9617a8127cf4c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fda5754cd9f44b",
                "top": "90dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0g9617a8127cf4c.setDefaultUnit(kony.flex.DP);
            FlexContainer0g9617a8127cf4c.add();
            flxLoanCreationTab.add(flxTopWrapper, flxClickEnable, FlexContainer0g9617a8127cf4c);
            flxMainWrapper.add(flxHeaderMenu, custInfo, flxLoanCreationTab);
            var custPayout = new kony.ui.CustomWidget({
                "id": "custPayout",
                "isVisible": false,
                "left": "-600px",
                "top": "510dp",
                "width": "225dp",
                "height": "50px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Pay-out Account",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custPayIn = new kony.ui.CustomWidget({
                "id": "custPayIn",
                "isVisible": false,
                "left": "-600px",
                "top": "510dp",
                "width": "225dp",
                "height": "50px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Pay-in Account",
                "options": "",
                "readonly": false,
                "value": ""
            });
            FlexContainer0d8781aeee9e447.add(flxMainWrapper, custPayout, custPayIn);
            flxMainContainer.add(FlexContainer0d8781aeee9e447);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "6.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "92.50%",
                "zIndex": 1,
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "6.50%",
                        "width": "92.50%",
                        "zIndex": 1
                    },
                    "imgCloseBackup": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxpayInOutPOP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxpayInOutPOP",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0g815ca6a691d4d",
                "top": "0",
                "width": "100%",
                "zIndex": 9
            }, {}, {});
            flxpayInOutPOP.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c8da61cffdb244 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "500dp",
                "id": "FlexContainer0c8da61cffdb244",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0e5562f0a71eb42",
                "top": "0",
                "width": "60%"
            }, {}, {});
            FlexContainer0c8da61cffdb244.setDefaultUnit(kony.flex.DP);
            var popupPayinPayout = new com.konymp.popupPayinPayout.popupPayinPayout({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popupPayinPayout",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0e816618aedef47",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxInnerSearch": {
                        "height": "400dp"
                    },
                    "imgClose1": {
                        "src": "ico_close.png"
                    },
                    "imgSrc1": {
                        "src": "search.png"
                    },
                    "popupPayinPayout": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexContainer0c8da61cffdb244.add(popupPayinPayout);
            flxpayInOutPOP.add(FlexContainer0c8da61cffdb244);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0dac10a9aca3042"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0ec4cae47d8e148"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_a64fb2afbc5c436db0490ee3a7636646;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_f1e6b3c2205a4598a4711462563af3fc;
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "ProgressIndicator": {
                        "isVisible": false,
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var RevisedPaymentSchedule = new com.lending.revisedPaymentSchedule.RevisedPaymentSchedule({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "RevisedPaymentSchedule",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "RevisedPaymentSchedule": {
                        "isVisible": false
                    },
                    "imgClose1": {
                        "src": "ico_close.png"
                    },
                    "imgPrint": {
                        "isVisible": false,
                        "src": "ico_print.png"
                    },
                    "lblRevisedPayment": {
                        "text": "Payment Schedule"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "height": "100%",
                        "isVisible": false,
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "flxPopup": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "left": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    },
                    "segOverride": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var SegmentPopup1 = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "SegmentPopup1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "height": "100%",
                        "isVisible": false,
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "flxPopup": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "left": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    },
                    "segOverride": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "custInfo": {
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "tbxPayOutAccount.downArrowLbl": {
                    "zIndex": 5
                },
                "tbxPayOutAccount": {
                    "centerX": "",
                    "height": "40dp",
                    "left": "31dp",
                    "top": "148dp",
                    "width": "253dp",
                    "zIndex": 1
                },
                "tbxPayOutAccount.lblFloatLabel": {
                    "left": "5%",
                    "text": "Pay-out Account "
                },
                "tbxPayInAccount.downArrowLbl": {
                    "zIndex": 5
                },
                "tbxPayInAccount": {
                    "centerX": "",
                    "height": "40dp",
                    "left": "35%",
                    "top": "148dp",
                    "width": "253dp",
                    "zIndex": 1
                },
                "tbxPayInAccount.lblFloatLabel": {
                    "left": "5%",
                    "text": "Pay-in Account "
                },
                "AccountList": {
                    "height": "235dp",
                    "width": "100%"
                },
                "AccountList.segAccountList": {
                    "height": "190dp",
                    "top": "40dp"
                },
                "AccountList2": {
                    "bottom": "",
                    "centerX": "50%",
                    "height": "235dp"
                },
                "ErrorAllert": {
                    "left": "6.50%",
                    "width": "92.50%",
                    "zIndex": 1
                },
                "ErrorAllert.imgCloseBackup": {
                    "src": "ico_close.png"
                },
                "popupPayinPayout.flxInnerSearch": {
                    "height": "400dp"
                },
                "popupPayinPayout.imgClose1": {
                    "src": "ico_close.png"
                },
                "popupPayinPayout.imgSrc1": {
                    "src": "search.png"
                },
                "popupPayinPayout": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "height": "100%",
                    "left": "0dp",
                    "width": "100%"
                },
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0dac10a9aca3042",
                    "skin2": "CopydefBtnNormal0ec4cae47d8e148",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "RevisedPaymentSchedule.imgClose1": {
                    "src": "ico_close.png"
                },
                "RevisedPaymentSchedule.imgPrint": {
                    "src": "ico_print.png"
                },
                "RevisedPaymentSchedule.lblRevisedPayment": {
                    "text": "Payment Schedule"
                },
                "SegmentPopup": {
                    "height": "100%",
                    "right": "",
                    "width": "100%"
                },
                "SegmentPopup.flxPopup": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "left": "",
                    "top": ""
                },
                "SegmentPopup.imgClose": {
                    "src": "ico_close.png"
                },
                "SegmentPopup1": {
                    "height": "100%",
                    "right": "",
                    "width": "100%"
                },
                "SegmentPopup1.flxPopup": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "left": "",
                    "top": ""
                },
                "SegmentPopup1.imgClose": {
                    "src": "ico_close.png"
                }
            }
            this.add(flxMessageInfo, flxMainContainer, ErrorAllert, flxpayInOutPOP, uuxNavigationRail, ProgressIndicator, RevisedPaymentSchedule, SegmentPopup, SegmentPopup1, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmLoanCreation,
            "enabledForIdleTimeout": false,
            "id": "frmLoanCreation",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1280, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});