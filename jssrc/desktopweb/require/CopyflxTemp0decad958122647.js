define("CopyflxTemp0decad958122647", function() {
    return function(controller) {
        var CopyflxTemp0decad958122647 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "CopyflxTemp0decad958122647",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        CopyflxTemp0decad958122647.setDefaultUnit(kony.flex.DP);
        var lblGroup = new kony.ui.Label({
            "height": "100%",
            "id": "lblGroup",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLblGrey15Sbold",
            "text": "Client ID",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblExpires = new kony.ui.Label({
            "height": "100%",
            "id": "lblExpires",
            "isVisible": true,
            "left": "100px",
            "skin": "sknLblGrey15Sbold",
            "text": "Activity",
            "top": "0dp",
            "width": "150px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "height": "100%",
            "id": "lblDate",
            "isVisible": true,
            "left": "270px",
            "skin": "sknLblGrey15Sbold",
            "text": "Date/Time",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblChannel = new kony.ui.Label({
            "height": "100%",
            "id": "lblChannel",
            "isVisible": true,
            "left": "400px",
            "skin": "sknLblGrey15Sbold",
            "text": "Channel",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        CopyflxTemp0decad958122647.add(lblGroup, lblExpires, lblDate, lblChannel);
        return CopyflxTemp0decad958122647;
    }
})