define("userfrmRepaymentController", {
    //Type your controller code here 
    _loanObj: {},
    arrangementId: "",
    billDetailsArray: [],
    dataPerPageNumber: 0,
    formatedBillDetails: [],
    currentCurrency: "",
    currentLoanAccountId: "",
    currentOverride: {},
    accountDetailsArray: [],
    currentSelectBillDetails: [],
    onNavigate: function(navObj) {
        this._loanObj = {};
        this._loanObj.customerId = navObj.customerId;
        this.arrangementId = Application.validation.isNullUndefinedObj(navObj.lblArrangementId);
        this.currentCurrency = Application.validation.isNullUndefinedObj(navObj.lblCCY);
        this.currentLoanAccountId = Application.validation.isNullUndefinedObj(navObj.lblHeaderAN);
        this.bindEvents();
        this.getOutstandingBillBasedOnArrangementId();
        this.setLoanHeaderDetails(navObj);
        this.pauseNavigation();
        this.view.AccountList.segAccountList.removeAll();
        //this.view.floatLabelText.flxBottomLine.isVisible = false; //Visible off Border for Field
        //this.view.floatLabelText.resetReadOnlyText();
    },
    bindEvents: function() {
        this.view.cusTextPaymentAmount.onKeyUp = this.reformatOnValChange;
        this.view.postShow = this.postShowFunctionCall;
        this.view.flxViewBillDetails.onTouchEnd = this.showOutstandingBillData;
        this.view.flxHeaderSegmentOutstandingClose.onTouchEnd = this.closeOutstandingPopup;
        this.view.flxSubmit.onTouchEnd = this.validationOnSubmitData;
        this.view.flxCancel.onTouchEnd = this.navigateCustomerAccountDetails;
        this.view.lblBreadBoardOverviewvalue.onTouchEnd = this.navigateCustomerAccountDetails;
        this.view.lblBreadBoardHomeValue.onTouchEnd = this.navigateHome; // lblBreadBoardHomeValue
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup.btnProceed.onClick = this.postUpdateToRepaymentSystem;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.AccountList.isVisible = false;
        // this.view.imgSearch.onTouchEnd = this.openAccountDetailsPopup;
        this.view.flxDebitAccountInnerWrapper.onTouchEnd = this.openAccountDetailsPopup;
        //this.view.flxMainContainer.onTouchEnd = this.closeAccountSegment;
        this.view.AccountList.segAccountList.onRowClick = this.onRowClickSegDebitAccount;
        this.view.segOutstandingBill.onRowClick = this.onRowClickSegOverallBillDetails;
        this.view.flxNavigationBillDetailsHeader.onTouchEnd = this.goBackToBillOverView;
        this.view.flxBillDetailsClose.onTouchEnd = this.goBackToBillOverView;
        this.view.Popup.btnProceed.onClick = this.postUpdateToRepaymentSystem;
        this.view.Popup.btnCancel.onClick = this.closepopupWarningMessage;
        this.view.btnNext.onClick = this.showOutstandingBillData;
        this.view.btnPrev.onClick = this.showOutstandingBillData;
        kony.timer.schedule("timer4", this.accountDynamicLoad, 3, false);
    },
    navigateHome: function() {
        var navToHome = new kony.mvc.Navigation("frmDashboard");
        var navObjet = {};
        navObjet.clientId = "";
        navToHome.navigate(navObjet);
    },
    accountDynamicLoad: function() {
        //kony.print("accountvalue"+JSON.stringify(gblCutacctArray));
        if (gblCutacctArray.length === 0) {
            this.view.AccountList.lblNoAccounts.isVisible = true;
            this.view.AccountList.segAccountList.isVisible = false;
        } else {
            this.view.AccountList.segAccountList.widgetDataMap = gblCutacctWidgetDate;
            this.view.AccountList.segAccountList.setData(gblCutacctArray);
            this.view.AccountList.lblNoAccounts.isVisible = false;
            this.view.AccountList.segAccountList.isVisible = true;
        }
    },
    //TODO DV- discuss // Already own accounts are listed
    //search icon may be needed for all accounts search not here
    openAccountDetailsPopup: function() {
        try {
            kony.print("####function triggered openAccountDetailsPopup###");
            /*
            var segdataPopulated = [];
            if(this.accountDetailsArray.length > 0) {
              for(var a in this.accountDetailsArray) {
                var mathSignDetermine = Math.sign(this.accountDetailsArray[a].availableBalance);
                var avaliableAmount = {};
                if(mathSignDetermine === -1 || mathSignDetermine === 0) {
                  avaliableAmount = {text:""+this.accountDetailsArray[a].availableBalance,skin:"sknflxFontD32E2FPX12"};
                } else {
                  avaliableAmount = {text:""+this.accountDetailsArray[a].availableBalance,skin:"sknflxFont434343px12"};
                }
                var json = {
                  "lblAccountNumber" : this.accountDetailsArray[a].accountId,
                  "lblAvaliableBalance" : avaliableAmount,
                  "productName" : ""+this.accountDetailsArray[a].productName
                };
                segdataPopulated.push(json);
              }

            }


            this.view.AccountList.segAccountList.rowTemplate = "FlxAccountList";
            var widgetDataMap = {
              "lblAccountId" : "lblAccountNumber",
              "lblWorkingBal" : "lblAvaliableBalance",
              "lbltype" : "productName"
            };
            this.view.AccountList.segAccountList.widgetDataMap = widgetDataMap;
            this.view.AccountList.segAccountList.setData(segdataPopulated);
            */
            if (this.view.AccountList.isVisible) {
                this.view.AccountList.isVisible = false;
            } else {
                this.view.AccountList.isVisible = true;
            }
            //kony.print("####function triggered segdataPopulated###"+JSON.stringify(segdataPopulated));
            //this.view.flxRepaymentWrapper.forceLayout();
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController openAccountDetailsPopup:::" + err);
        }
    },
    closeAccountSegment: function() {
        kony.print("*****closeAccountSegment event fired********");
        this.view.AccountList.isVisible = false;
        //this.view.forceLayout();
    },
    closeSegmentOverrite: function() {
        this.currentOverride = {};
        this.view.SegmentPopup.isVisible = false;
        this.view.forceLayout();
    },
    navigateCustomerAccountDetails: function() {
        var navToCusLoans = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navObjet.isPopupShow = "false";
        navToCusLoans.navigate(navObjet);
    },
    setLoanHeaderDetails: function(navObj) {
        try {
            var newObj = navObj;
            this._loanObj.currencyId = navObj.lblCCY;
            this._loanObj.arrangementId = navObj.lblArrangementId;
            this._loanObj.customerId = navObj.customerId;
            this._loanObj.produtId = navObj.loanProduct;
            //TODO : take from segment data a navigation object
            this.view.loanDetails.lblAccountNumberResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHeaderAN);
            this.view.loanDetails.lblLoanTypeResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHeaderAccountType); //"2345678";
            this.view.loanDetails.lblCurrencyValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblCCY);
            this.view.loanDetails.lblAmountValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblClearedBalance);
            this.view.loanDetails.lblStartDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAvailableLimit);
            this.view.loanDetails.lblMaturityDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHdr);
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController setLoanHeaderDetails:::" + err);
        }
    },
    getCurrentDate: function() {
        try {
            var today = new Date();
            var dd = today.getDate();
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var todayConverted = dd + "-" + shortMonth + "-" + yyyy;
            return todayConverted;
            /**this.view.cusEffectiveDateResponse.displayFormat = "dd-MMM-yyyy";
            this.view.cusEffectiveDateResponse.value = todayConverted;
            this.view.cusEffectiveDateResponse.min = todayConverted;**/
        } catch (err) {
            kony.print("Error in CustomerLoanInterestChangeController getCurrentDate:::" + err);
        }
    },
    formatDate: function(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return year + "-" + month + "-" + day;
    },
    getCurrentTransactDate: function(transactDate) {
        try {
            var today = new Date(transactDate);
            var dd = today.getDate();
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var todayConverted = dd + " " + shortMonth + " " + yyyy;
            this.view.cusCalenderPaymentDate.value = todayConverted;
            this.view.cusCalenderPaymentDate.displayFormat = "dd MMM yyyy";
            this.view.cusCalenderPaymentDate.min = todayConverted;
        } catch (err) {
            kony.print("Error in CreateContractLog getCurrentDate:::" + err);
        }
    },
    postShowFunctionCall: function() {
        try {
            // initialize Customer Widget 
            this.view.cusBillIcon.color = "temenos-primary";
            this.view.cusBillIcon.size = "small";
            this.view.cusBillIcon.iconName = "receipt";
            this.view.SegmentPopup.isVisible = false;
            //this.view.cusTextPaymentDue.disabled = true;
            //this.view.cusTextPaymentDue.value = "";
            this.view.cusListBoxPaymentMode.options = '[{"label":"Account Transfer","value":"ACOTHER"}]';
            this.view.cusListBoxPaymentMode.labelText = "Payment Mode";
            this.view.cusListBoxPaymentMode.value = "ACOTHER";
            //this.view.cusListBoxPaymentMode.disabled = true;
            //var currentDate = this.getCurrentDate();
            this.getCurrentTransactDate(transactDate);
            this.view.cusIconClose.color = "temenos-primary";
            this.view.cusIconClose.size = "small";
            this.view.cusIconClose.iconName = "highlight_off";
            this.view.cusIconPrinter.color = "temenos-primary";
            this.view.cusIconPrinter.size = "small";
            this.view.cusIconPrinter.iconName = "print";
            //this.view.cusTextDebitAccount.iconButtonIcon = "search";
            //this.view.cusTextDebitAccount.showIconButtonTrailing = true;
            this.view.cusTextPaymentAmount.value = "";
            this.view.cusTextPaymentAmount.currencyCode = this.currentCurrency;
            this.view.SegmentPopup.isVisible = false;
            this.view.Popup.isVisible = false;
            this.view.flxGenericBillDetailsWrapper.isVisible = false;
            this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = false;
            this.view.cusTextDebitAccountNumber.validateRequired = 'required';
            this.view.forceLayout();
        } catch (err) {
            kony.print("Error in RepaymentController postShowFunctionCall::" + err);
        }
    },
    closeOutstandingPopup: function() {
        try {
            this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = false;
            this.view.segOutstandingBill.removeAll();
            this.view.forceLayout();
        } catch (err) {
            kony.print("Error in RepaymentController loadingCustomerWidgetData::" + err);
        }
    },
    getOutstandingBillBasedOnArrangementId: function() {
        try {
            this.currentOverride = {};
            this.view.ErrorAllert.isVisible = false;
            this.view.lblTotalBillCountInfo.text = "";
            this.view.cusTextDebitAccountNumber.value = "";
            var serviceName = "LendingNew";
            var operationName = "getOutstandingBills"; //"getOutstandingBillDetails";
            var headers = {};
            var inputParams = {
                "arrangementID": this.arrangementId
            };
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgetOutstandingBill, this.errorCBgetOutstandingBill);
        } catch (err) {
            kony.print("Error in RepaymentController getOutstandingBillBasedOnArrangementId::" + err);
        }
    },
    successCBgetOutstandingBill: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            this.billDetailsArray = [];
            this.dataPerPageNumber = 0;
            this.formatedBillDetails = [];
            this.view.segOutstandingBill.removeAll();
            this.dataPerPageNumber = this.view.txtDataPerPageNumber.text;
            kony.print("success response:::" + JSON.stringify(res));
            var responseBody = res.body;
            var outstandingBillAmountData = [];
            var responseBodyFormatArray = [];
            var outstandingTotalBill = 0;
            if (responseBody.length > 0) {
                for (var a in responseBody) {
                    var outstanding = Application.validation.isNullUndefinedObj(responseBody[a].outstandingAmount);
                    if (outstanding !== "") {
                        outstanding = outstanding.replace(/,/g, ''); //replace(",","");
                        outstandingBillAmountData.push(outstanding);
                    } else {
                        outstanding = 0;
                        outstandingBillAmountData.push(outstanding);
                    }
                    var outstandingNew = Application.validation.isNullUndefinedObj(responseBody[a].outstandingAmount);
                    if (outstandingNew !== "") {
                        outstandingNew = outstanding.replace(/,/g, ''); //replace(",","");
                    }
                    var json = {
                        "lblBillIdInfo": Application.validation.isNullUndefinedObj(responseBody[a].billId),
                        "lblDueDateInfo": Application.validation.isNullUndefinedObj(responseBody[a].billDate),
                        "lblTypeInfo": Application.validation.isNullUndefinedObj(responseBody[a].billType),
                        "lblBilledInfo": Application.numberFormater.convertForDispaly(Application.validation.isNullUndefinedObj(responseBody[a].billedAmount), this.currentCurrency), //Application.validation.isNullUndefinedObj(responseBody[a].billedAmount),
                        "lblOutstandingInfo": Application.numberFormater.convertForDispaly(Application.validation.isNullUndefinedObj(outstandingNew), this.currentCurrency),
                        "lblStatusInfo": Application.validation.isNullUndefinedObj(responseBody[a].billStatus),
                    };
                    responseBodyFormatArray.push(json);
                }
                outstandingTotalBill = outstandingBillAmountData.length;
                this.billDetailsArray = responseBody;
                this.formatedBillDetails = responseBodyFormatArray;
                kony.print("this.formatedBillDetails::::" + JSON.stringify(this.formatedBillDetails));
                kony.print("outstandingBillAmountData::::" + JSON.stringify(outstandingBillAmountData));
                kony.print("outstandingTotalBill::::" + JSON.stringify(outstandingTotalBill));
                this.view.lblTotalBillCountInfo.text = "" + outstandingTotalBill;
            }
            this.getPayInDefaultAccountDetails(); //Calling service to fetch the Default Pay-in Account details
            var totalBillAmount = this.calculateTotalOutstandBill(outstandingBillAmountData);
            if (Application.validation.isNullUndefinedObj(totalBillAmount) !== "") {
                //totalBillAmount = totalBillAmount.toFixed(2);
                /**if(this.currentCurrency === "EUR") {
                  totalBillAmount = totalBillAmount.replace(".",",");
                }**/
                totalBillAmount = Application.numberFormater.convertForDispaly(totalBillAmount, this.currentCurrency);
                this.view.cusTextPaymentDue.value = totalBillAmount;
            } else {
                this.view.cusTextPaymentDue.value = totalBillAmount;
            }
            this.view.cusTextPaymentDue.currencyCode = this.currentCurrency;
            this.resumeNavigation();
            //this.view.cusTextPaymentDue.decimals = 2;
            //this.view.cusTextPaymentDue.disabled = true;
            kony.print("totalBillAmount----->" + totalBillAmount);
        } catch (err) {
            kony.print("Error in RepaymentController successCBgetOutstandingBill::" + err);
        }
    },
    errorCBgetOutstandingBill: function(res) {
        try {
            this.view.segOutstandingBill.removeAll();
            this.formatedBillDetails = [];
            kony.print("error response:::" + JSON.stringify(res));
            this.view.ProgressIndicator.isVisible = false;
            // TODO - Calling the default account service when there is no payment due is avaliable 
            this.getPayInDefaultAccountDetails(); //Calling service to fetch the Default Pay-in Account details
            this.view.cusTextPaymentDue.value = "0.00";
            this.view.lblTotalBillCountInfo.text = "0";
            this.view.cusTextPaymentDue.currencyCode = this.currentCurrency;
            this.resumeNavigation();
            //this.view.ErrorAllert.lblMessage.text = res.error.message;
            //this.view.ErrorAllert.isVisible = true;
        } catch (err) {
            kony.print("Error in RepaymentController errorCBgetOutstandingBill::" + err);
        }
    },
    calculateTotalOutstandBill: function(data) {
        try {
            kony.print("outstandingBillDetail_array--->" + JSON.stringify(data));
            var sumData = 0;
            if (data.length > 0) {
                var sum = data.reduce(function(a, b) {
                    return parseFloat(a) + parseFloat(b);
                }, 0);
                kony.print("sum---->" + sum);
                sumData = sum.toFixed(2);
            }
            return sumData;
        } catch (err) {
            kony.print("Error in RepaymentController errorCBgetOutstandingBill::" + err);
        }
    },
    getPayInDefaultAccountDetails: function() {
        try {
            var serviceName = "LendingNew";
            var operationName = "getPayInAccountDetails";
            var headers = {};
            var inputParams = {
                "arrangementID": this.arrangementId
            };
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgetPayInDefaultAccountDetails, this.errorCBgetPayInDefaultAccountDetails);
        } catch (err) {
            kony.print("Error in RepaymentController getPayInDefaultAccountDetails::" + err);
        }
    },
    successCBgetPayInDefaultAccountDetails: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print("success response:::" + JSON.stringify(res));
            //this.getAccountBasedonCustomer(); // calling other Account Info
            var responseData = res.body;
            if (responseData.length > 0) {
                var accountDetails = responseData[0].payinAccount;
                kony.print("accountDetails:::" + JSON.stringify(accountDetails));
                //this.view.floatReadOnlylText.txtFloatText.text = accountDetails;
                this.view.cusTextDebitAccountNumber.value = accountDetails;
                //this.view.floatLabelText.animateComponent();
                //this.view.forceLayout();
            }
        } catch (err) {
            kony.print("Error in RepaymentController successCBgetPayInDefaultAccountDetails::" + err);
        }
    },
    errorCBgetPayInDefaultAccountDetails: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print("error response:::" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in RepaymentController errorCBgetPayInDefaultAccountDetails::" + err);
        }
    },
    showPaginationSegmentData: function(dataPerPage, arrayOfData, btnName) {
        kony.print("showPaginationSegmentData===>");
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.flxPaginationNetSettlementAmountInfo.isVisible = true;
                this.view.flxPaginationNetSettlementAmountResponse.isVisible = true;
                this.view.btnNext.isVisible = true;
                this.view.lblSearchPage.isVisible = true;
                this.view.btnPrev.isVisible = true;
                this.view.flxPagination.isVisible = true;
            } else {
                this.view.flxPaginationNetSettlementAmountInfo.isVisible = true;
                this.view.flxPaginationNetSettlementAmountResponse.isVisible = true;
                this.view.btnNext.isVisible = false;
                this.view.lblSearchPage.isVisible = false;
                this.view.btnPrev.isVisible = false;
                this.view.flxPagination.isVisible = true;
            }
            if (btnName == "btnPrev" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName == "btnNext" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;            
            } else if (this.gblSegPageCount == 1 && this.totalpage > 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;           
            } else if (this.gblSegPageCount == 1 && this.totalpage == 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.lblSearchPage.text = "";
            } else if (this.gblSegPageCount == this.totalpage && this.totalpage > 1) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnDisable";
                //this.view.lblSearchPage.isVisible = true;            
            } else {
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.lblSearchPage.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in RepaymentController next previous method " + err);
        }
    },
    showOutstandingBillData: function(buttonevent) {
        try {
            this.view.cusIconClose.color = "temenos-primary";
            this.view.cusIconClose.size = "small";
            this.view.cusIconClose.iconName = "highlight_off";
            this.view.cusIconPrinter.color = "temenos-primary";
            this.view.cusIconPrinter.size = "small";
            this.view.cusIconPrinter.iconName = "print";
            kony.print("showOutstandingBillData===>");
            if (this.formatedBillDetails.length > 0) {
                var segmentData = this.showPaginationSegmentData(this.dataPerPageNumber, this.formatedBillDetails, buttonevent.id);
                var widgetMap = {
                    "lblBillIdInfo": "lblBillIdInfo",
                    "lblDueDateInfo": "lblDueDateInfo",
                    "lblTypeInfo": "lblTypeInfo",
                    "lblBilledInfo": "lblBilledInfo",
                    "lblOutstandingInfo": "lblOutstandingInfo",
                    "lblStatusInfo": "lblStatusInfo",
                };
                this.view.segOutstandingBill.rowTemplate = "flxOutstandingBillDetails";
                this.view.segOutstandingBill.widgetDataMap = widgetMap;
                this.view.segOutstandingBill.setData(segmentData);
                kony.print("segDataSet ====>>" + JSON.stringify(segmentData));
                var paymentDueAmount = this.view.cusTextPaymentDue.value;
                this.view.flxPaginationNetSettlementAmountResponse.text = "" + paymentDueAmount;
                this.view.flxOutstandingRepaymentSegmentWrapper.isVisible = true;
                this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = true;
                this.view.flxNoOutstandingBillDetailsWrapper.isVisible = false;
                this.view.flxPagination.isVisible = true;
                this.view.flxSegmentHeaderInfo.isVisible = true;
                if (segmentData.length === 0) {
                    this.view.flxOutstandingRepaymentSegmentWrapper.isVisible = false;
                    this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = true;
                    this.view.flxPagination.isVisible = false;
                    this.view.flxSegmentHeaderInfo.isVisible = false;
                    this.view.flxNoOutstandingBillDetailsWrapper.isVisible = true;
                }
                //this.view.flxOutstandingRepaymentOutsideWrapper.forceLayout();
                //this.view.forceLayout();
            } else {
                this.view.flxOutstandingRepaymentSegmentWrapper.isVisible = false;
                this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = true;
                this.view.flxPagination.isVisible = false;
                this.view.flxSegmentHeaderInfo.isVisible = false;
                this.view.flxNoOutstandingBillDetailsWrapper.isVisible = true;
            }
        } catch (err) {
            kony.print("Error in RepaymentController showOutstandingBillData:::" + err);
        }
    },
    validationOnSubmitData: function() {
        try {
            this.view.lblErrorMsg.isVisible = false;
            var amount = this.view.cusTextPaymentAmount.value;
            var dueAmount = this.view.cusTextPaymentDue.value;
            var accountNumber = this.view.cusTextDebitAccountNumber.value;
            /**var isvalid = true;
            var message  = "";
            if(amount === "" || amount === undefined || amount === null || amount === 0) {
              isvalid = false;
            }
            if(accountNumber === "" || accountNumber === undefined || accountNumber === null || accountNumber === 0) {
              isvalid = false;
            }**/
            var isValidData = this.validateData();
            if (isValidData !== 1) {
                //if(isvalid) {
                if (amount > dueAmount) {
                    message = "An Amount higher than due is being paid";
                    this.view.Popup.lblMessage.text = message;
                    this.view.Popup.isVisible = true;
                }
                if (amount < dueAmount) {
                    message = "An Amount lower than due is being paid";
                    this.view.Popup.lblMessage.text = message;
                    this.view.Popup.isVisible = true;
                }
                if (dueAmount === 0 || dueAmount === null || dueAmount === "" || dueAmount === undefined) {
                    message = "An Amount higher than due is being paid";
                    this.view.Popup.lblMessage.text = message;
                    this.view.Popup.isVisible = true;
                }
                if (amount === dueAmount) {
                    this.view.Popup.lblMessage.text = "";
                    this.view.Popup.isVisible = false;
                    this.postUpdateToRepaymentSystem();
                }
            } else {
                //this.view.cusTextPaymentAmount.validationMessage = "Please Enter The Payment Amount";
                //this.view.cusTextPaymentAmount.validateOnInitialRender = "required";
                this.view.lblErrorMsg.isVisible = true;
                /** var isValidData = this.validateSubmit();
                 if(isValidData !== 1) {
                   alert("Submit");
                 } **/
            }
            this.view.forceLayout();
        } catch (err) {
            kony.print("Error in RepaymentController validationOnSubmitData:::" + err);
        }
    },
    closepopupWarningMessage: function() {
        this.view.Popup.lblMessage.text = "";
        this.view.Popup.isVisible = false;
    },
    postUpdateToRepaymentSystem: function() {
        try {
            //this.currentOverride = {};
            this.view.Popup.lblMessage.text = "";
            this.view.flxHeaderMenu.isVisible = true;
            this.view.Popup.isVisible = false;
            this.view.lblErrorMsg.isVisible = false;
            this.view.ErrorAllert.isVisible = false;
            var amount = this.view.cusTextPaymentAmount.value;
            //var currentDate = this.getCurrentDate();
            var currentDate = this.view.cusCalenderPaymentDate.value;
            var requestedDate = this.formatDate(currentDate);
            var calendarDate = this.view.cusCalenderPaymentDate.value;
            var requestedDebitDate = this.formatDate(calendarDate);
            var debitAccountDate = this.view.cusTextDebitAccountNumber.value;
            //var debitAccountDate = this.view.floatReadOnlylText.txtFloatText.text;
            var serviceName = "LendingNew";
            var operationName = "postLoanRepayment";
            kony.print("this.currentOverride----->" + JSON.stringify(this.currentOverride));
            var headers = {};
            /**if(this.currentOverride === "" || this.currentOverride === {} || this.currentOverride === null  || this.currentOverride === undefined) {
              var headers = {};
            } else {
              var headers = {
                "overrideDetails" : this.currentOverride
              };
            }**/
            var inputParams = {};
            inputParams.paymentOrderProductId = "ACOTHER";
            inputParams.debitAccountId = debitAccountDate;
            inputParams.debitCurrency = this.currentCurrency;
            inputParams.debitValueDate = requestedDebitDate;
            inputParams.executionDate = requestedDate;
            inputParams.creditAccountId = this.currentLoanAccountId;
            inputParams.paymentCurrencyId = this.currentCurrency;
            inputParams.amount = amount;
            inputParams.overrideDetails = this.currentOverride;
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.submitSuccessCallbackFunc, this.submitFailureCallbackFunc);
        } catch (err) {
            kony.print("Error in RepaymentController showOutstandingBillData:::" + err);
        }
    },
    submitSuccessCallbackFunc: function(res) {
        try {
            //Application.loader.dismissLoader();
            this.currentOverride = {};
            this.view.cusTextPaymentAmount.value = "";
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success CallBack :::" + JSON.stringify(res));
            if (res === "" || res === null || res === undefined) {
                kony.print("Response is null or undefined");
            } else {
                var resId = res.header;
                kony.print("RES___ID***" + JSON.stringify(resId));
                var data_id = Application.validation.isNullUndefinedObj(resId.id);
                //this.view.lblMessageWithReferenceNumber.text = "Interest Change applied successfully!      Reference : Interest Change applied successfully!      Reference : "+data_id;
                var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
                var navObjet = {};
                navObjet.customerId = this._loanObj.customerId;
                navObjet.isShowLoans = "true";
                navObjet.isPopupShow = "true";
                navObjet.messageInfo = "Loan repayment completed successfully!   Reference : " + data_id;
                navToChangeInterest1.navigate(navObjet);
            }
            //this.view.flxHeaderMenu.isVisible = false;
            //this.view.flxMessageInfo.isVisible = true;
            //this.callingLoanServiceDetails(); //Calling GET to update the value.
        } catch (err) {
            kony.print("Error in RepaymentController submitSuccessCallbackFunc:::" + err);
        }
    },
    submitFailureCallbackFunc: function(res) {
        try {
            this.currentOverride = {};
            //Application.loader.dismissLoader();
            this.view.ProgressIndicator.isVisible = false;
            //alert("Service failure");
            //kony.print("Inside ErrorDetails=====>"+JSON.stringify(res.error.errorDetails));
            if (res.override !== "" && res.override !== undefined) {
                var overwriteDetails = [];
                var overWriteData = [];
                var segDataForOverrite = [];
                this.currentOverride = res.override.overrideDetails;
                overwriteDetails = res.override.overrideDetails;
                if (overwriteDetails.length > 0) {
                    for (var a in overwriteDetails) {
                        overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                        var json = {
                            "lblSerial": "*",
                            "lblInfo": overwriteDetails[a].description
                        }
                        segDataForOverrite.push(json);
                    }
                    this.view.SegmentPopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.view.SegmentPopup.lblPaymentInfo.text = "override Confirmation";
                    this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
                    this.view.SegmentPopup.isVisible = true;
                    kony.print("overWriteData::::" + JSON.stringify(overWriteData));
                    kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
                    this.view.forceLayout();
                }
            }
            if (res.error.errorDetails !== []) { // !== "" || res.error.errorDetails !== null || res.error.errorDetails !== undefined || res.error.errorDetails !== []) {
                kony.print("Inside ErrorDetails=====>");
                this.view.ProgressIndicator.isVisible = false;
                if (res.error.errorDetails[0] === "" || res.error.errorDetails[0] === [] || res.error.errorDetails[0] === undefined || res.error.errorDetails[0] === null) {
                    kony.print("Inside ErrorDetails Empty=====>");
                } else {
                    this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message; //error.errorDetails[0].message
                    this.view.ErrorAllert.isVisible = true;
                    kony.print("Service Failure:::" + JSON.stringify(res.error.errorDetails));
                    this.view.flxHeaderMenu.isVisible = false;
                    this.view.flxMessageInfo.isVisible = false;
                    this.view.forceLayout();
                }
            }
        } catch (err) {
            kony.print("Error in RepaymentController submitFailureCallbackFunc:::" + err);
        }
    },
    //TODO - not needed - already globally loaded
    /*
  getAccountBasedonCustomer : function() {
    try {
      this.view.ProgressIndicator.isVisible = true;
      this.accountDetailsArray = [];
      var serviceName = "LendingNew";
      var operationName = "getAccounts";
      var headers = {
        "customerId" : this._loanObj.customerId,
        "companyId" : "NL0020001"
      };
      var inputParams = {};
      MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, 
                                         inputParams, 
                                         this.successCallBackfuncGetAccountBasedonCustomer,
                                         this.failureCallBackfuncGetAccountBasedonCustomer)
    } catch(err) {
      kony.print("Error in RepaymentController getAccountBasedonCustomer:::"+err);
    }
  },

  successCallBackfuncGetAccountBasedonCustomer : function(res) {
    try {
      this.view.ProgressIndicator.isVisible = false;
      kony.print("#### Success CB Response #####"+JSON.stringify(res.body));
      var responseData = res.body;
      if(responseData.length > 0){
        var formatedArray = [];
        for(var a in responseData) {
          var json = {
            "accountId" : responseData[a].accountId,
            "availableBalance" : responseData[a].availableBalance,
            "currencyId" : responseData[a].currencyId,
            "customerId" : responseData[a].customerId,
            "productName" : responseData[a].productName
          };
          formatedArray.push(json);
        }
        kony.print("@@@@formatedArray#####"+JSON.stringify(formatedArray));
        this.accountDetailsArray = formatedArray;
      }
    } catch(err) {
      kony.print("Error in RepaymentController successCallBackfuncGetAccountBasedonCustomer:::"+err);
    }
  },

  failureCallBackfuncGetAccountBasedonCustomer : function(res) {
    try {
      this.view.ProgressIndicator.isVisible = false;
      kony.print("#### Failure CB Response #####"+JSON.stringify(res));
    } catch(err) {
      kony.print("Error in RepaymentController failureCallBackfuncGetAccountBasedonCustomer:::"+err);
    }
  },

*/
    onRowClickSegOverallBillDetails: function() {
        try {
            this.currentSelectBillDetails = [];
            kony.print("####____onRowClickSegOverallBillDetails___####");
            var selectedRowData = this.view.segOutstandingBill.selectedRowItems[0];
            kony.print("####____onRowClickSegOverallBillDetails___####" + JSON.stringify(selectedRowData));
            this.currentSelectBillDetails = selectedRowData;
            var billid = selectedRowData.lblBillIdInfo;
            this.getBillDetails(billid);
        } catch (err) {
            kony.print("Error in RepaymentController onRowClickSegOverallBillDetails:::" + err);
        }
    },
    onRowClickSegDebitAccount: function() {
        try {
            kony.print("####____onRowClickSegDebitAccount___####");
            //TODO -better use the events, arguments
            var selectedRowData = this.view.AccountList.segAccountList.selectedRowItems[0];
            kony.print("####____onRowClickSegDebitAccount___####" + JSON.stringify(selectedRowData));
            this.view.cusTextDebitAccountNumber.value = selectedRowData.accountid;
            //this.view.floatLabelText.animateComponent();
            //this.view.floatReadOnlylText.txtFloatText.text = selectedRowData.lblAccountNumber;
            this.view.AccountList.setVisibility(false);
        } catch (err) {
            this.view.AccountList.setVisibility(false);
            kony.print("Error in RepaymentController onRowClickSegDebitAccount:::" + err);
        }
    },
    getBillDetails: function(billId) {
        try {
            this.view.ProgressIndicator.isVisible = true;
            var serviceName = "LendingNew";
            var operationName = "getBillDetailsBasedBillId";
            var headers = {
                "billId": billId,
            };
            var inputParams = {
                arrangementId: this.arrangementId
            };
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfuncGetBillDetails, this.failureCallBackfuncGetBillDetails);
        } catch (err) {
            kony.print("Error in RepaymentController getBillDetails:::" + err);
        }
    },
    failureCallBackfuncGetBillDetails: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        kony.print("Failure CB failureCallBackfuncGetBillDetails---->" + JSON.stringify(res));
    },
    successCallBackfuncGetBillDetails_old: function(res) {
        try {
            //var response = res.body;
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success CB successCallBackfuncGetBillDetails_old---->" + JSON.stringify(res.body));
            var principal_interest = 0.00;
            var amount = 0.00;
            var charges = 0.00;
            var Penalty_Interest = 0.00;
            var responseIteration = res.body[0].properties;
            if (responseIteration.length > 0) {
                //var responseIteration = response.properties;
                if (responseIteration.length > 0) {
                    for (var a in responseIteration) {
                        if (responseIteration[a].propertyName === "Principal Interest") {
                            principal_interest = responseIteration[a].interestAmount;
                        }
                        if (responseIteration[a].propertyName === "Penalty Interest") {
                            Penalty_Interest = responseIteration[a].interestAmount;
                        }
                        if (responseIteration[a].propertyName === "Account") {
                            amount = responseIteration[a].interestAmount;
                        }
                        if (responseIteration[a].propertyName === "Periodic Charges") {
                            charges = responseIteration[a].interestAmount;
                        }
                    }
                    kony.print("principal_interest:::" + principal_interest);
                    kony.print("Penalty_Interest:::" + Penalty_Interest);
                    kony.print("amount:::" + amount);
                    kony.print("charges:::" + charges);
                    var netAmount = parseFloat(principal_interest) + parseFloat(Penalty_Interest) + parseFloat(amount) + parseFloat(charges);
                    kony.print("netAmount:::" + netAmount);
                    this.view.lblCurrencyinfo.text = this.currentCurrency;
                    this.view.lblCurrencyinfo02.text = this.currentCurrency;
                    this.view.lblCurrencyinfo03.text = this.currentCurrency;
                    this.view.lblCurrencyinfo04.text = this.currentCurrency;
                    this.view.lblCurrencyinfo05.text = this.currentCurrency;
                    this.view.lblNetSettlementResponse.text = "" + netAmount;
                    this.view.lblChargeResponse.text = "" + charges;
                    this.view.lblPenaltyInterestResponse.text = "" + Penalty_Interest;
                    this.view.lblPrincipalInterestResponse.text = "" + principal_interest;
                    this.view.lblPrincipalResponse.text = "" + amount;
                    this.view.lblHeaderBillNumberResponse.text = this.currentSelectBillDetails.lblBillIdInfo;
                    this.view.flxGenericBillDetailsWrapper.isVisible = true;
                    this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = false;
                    this.view.flxGenericBillDetailsWrapper.forceLayout();
                }
            }
        } catch (err) {
            kony.print("Error in RepaymentController successCallBackfuncGetBillDetails_old:::" + err);
        }
    },
    successCallBackfuncGetBillDetails: function(res) {
        try {
            //var response = res.body;
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success CB successCallBackfuncGetBillDetails---->" + JSON.stringify(res.body));
            var responseIteration = res.body[0].properties;
            var mathArray = [];
            var segDataToPopulate = [];
            if (responseIteration.length > 0) {
                for (var a in responseIteration) {
                    mathArray.push(responseIteration[a].interestAmount);
                    var json = {
                        "lblTypeInfo": responseIteration[a].propertyName,
                        "lblCurrency": this.currentCurrency,
                        //"lblAmount" : ""+responseIteration[a].interestAmount
                        "lblAmount": Application.numberFormater.convertForDispaly(responseIteration[a].interestAmount, this.currentCurrency),
                    };
                    segDataToPopulate.push(json);
                }
                kony.print("####segDataToPopulate####" + JSON.stringify(segDataToPopulate));
                this.view.segParticularBillDetail.widgetDataMap = {
                    "lblTypeInfo": "lblTypeInfo",
                    "lblCurrency": "lblCurrency",
                    "lblAmount": "lblAmount"
                };
                this.view.segParticularBillDetail.rowTemplate = "flxSegParticularBillDetails";
                this.view.segParticularBillDetail.setData(segDataToPopulate);
                kony.print("####mathArray####" + JSON.stringify(mathArray));
                this.view.lblHeaderBillNumberResponse.text = this.currentSelectBillDetails.lblBillIdInfo;
                var netAmount = this.calculateTotalOutstandBill(mathArray);
                kony.print("####netAmount####" + JSON.stringify(netAmount));
                //this.view.lblNetSettlementResponse.text = ""+netAmount;
                this.view.lblNetSettlementResponse.text = Application.numberFormater.convertForDispaly(netAmount, this.currentCurrency)
                    //this.view.lblIssuesDateResponse.text = "";
                this.view.lblDueDateBillResponse.text = this.currentSelectBillDetails.lblDueDateInfo;
                this.view.lblAgeingResponse.text = this.currentSelectBillDetails.lblStatusInfo;
                this.view.flxGenericBillDetailsWrapper.isVisible = true;
                this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = false;
                this.view.lblCurrencyinfo05.text = this.currentCurrency;
                this.view.flxGenericBillDetailsWrapper.forceLayout();
            }
        } catch (err) {
            kony.print("Error in RepaymentController successCallBackfuncGetBillDetails:::" + err);
        }
    },
    goBackToBillOverView: function() {
        this.view.flxGenericBillDetailsWrapper.isVisible = false;
        this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = true;
    },
    reformatOnValChange: function(event) {
        //already existing value in tex box
        kony.print(event.keyCode);
        var str = this.view.cusTextPaymentAmount.value;
        kony.print(str);
        if (str !== "" && !isNaN(str)) {
            //already a number is in the box
            if (event.keyCode === 84) {
                this.view.cusTextPaymentAmount.value = str + "000";
            } else if (event.keyCode === 77) {
                this.view.cusTextPaymentAmount.value = str + "000000";
            } else {
                //do not do anything
            }
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    validateData: function() {
        var Requiredflag = 0;
        var FormId = document.getElementsByTagName("form")[0].getAttribute("id");
        var form = document.getElementById(FormId);
        var Requiredfilter = form.querySelectorAll('*[required]');
        for (var i = 0; i < Requiredfilter.length; i++) {
            if (Requiredfilter[i].disabled !== true && Requiredfilter[i].value === '') {
                var ElementId = Requiredfilter[i].id;
                var Element = document.getElementById(ElementId);
                Element.reportValidity();
                Requiredflag = 1;
            }
        }
        return Requiredflag;
    },
});
define("frmRepaymentControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_ic0b5f44bf3a4729b845e7850b39b467: function AS_UWI_ic0b5f44bf3a4729b845e7850b39b467(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_gf34c04ce94246f3854345b0e71f3975: function AS_UWI_gf34c04ce94246f3854345b0e71f3975(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmRepaymentController", ["userfrmRepaymentController", "frmRepaymentControllerActions"], function() {
    var controller = require("userfrmRepaymentController");
    var controllerActions = ["frmRepaymentControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
