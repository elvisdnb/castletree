define("userfrmCustomerCreationController", {
    //Type your controller code here 
    arrangementId: "",
    _clientId: "",
    _duplicateId: "",
    currentOverride: {},
    //Type your controller code here 
    onNavigate: function() {
        this.bindEvents();
        this.fetchCuntryDetails();
        this._duplicateId = "";
        this.view.custTitleDropDown.value = "";
        this.view.custFirstName.value = "";
        this.view.custLastName.value = "";
        this.view.custAlternateRefName.value = "";
        this.view.custGenderDropDown.value = "";
        this.view.custDateOfBirth.value = "";
        this.view.custNationalityDropDown.value = "";
        this.view.custResidenceDropDown.value = "";
        this.view.custEmail.value = "";
        this.view.custPhoneNumber.value = "";
        this.view.custAddress.value = "";
        this.view.custStreet.value = "";
        this.view.custTown.value = "";
        this.view.custCityTextField.value = "";
        this.view.custCountryDropDown.value = "";
        this.view.custPostalCode.value = "";
        this.view.custEmployersName.value = "";
        //this.view.ProgressIndicator.isVisible=false;
        this.view.ErrorAllert.isVisible = false;
        this.view.lblErrorMsg.isVisible = false;
        this.view.SegmentPopup.isVisible = false;
    },
    bindEvents: function() {
        this.view.postShow = this.postShowFunctionCall;
        _clientId = "";
        this.view.custConfirm.onclickEvent = this.onClickSubmitData;
        this.view.custCancel.onclickEvent = this.onClickCancel;
        //this.view.custPhoneNumber.valueChanged= this.validatePhoneNumber;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.closeErrorAllert;
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup.btnProceed.onClick = this.onClickSubmitData;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.custFirstName.valueChanged = this.uppercaseToFirtCharacter;
        this.view.custLastName.valueChanged = this.uppercaseToFirtCharacter;
        this.view.custFirstName.valueChanged = this.restictSpeicalChars;
        this.view.custLastName.valueChanged = this.restictSpeicalChars;
        this.view.custFirstName.valueChanged = this.updateAlternateRefName;
        this.view.custDateOfBirth.valueChanged = this.updateAlternateRefName;
        this.view.custLastName.valueChanged = this.updateAlternateRefName;
        this.view.flxBack.onTouchEnd = this.onClickCancel;
    },
    /*
   getCurrentTransactDate : function() {
    try {
      var today = this.view.custDateOfBirth.value;
      var dd = today.getDate();
      var shortMonth = today.toLocaleString('en-us', { month: 'short' });
      var yyyy = today.getFullYear();
      if(dd<10) 
      {
        dd='0'+dd;
      } 
      var todayConverted = dd+"-"+shortMonth+"-"+yyyy;
      this.view.custDateOfBirth.value = todayConverted;
      this.view.custDateOfBirth.displayFormat = "dd-MMM-yyyy";
      this.view.custDateOfBirth.min = todayConverted;
    } catch(err) {
      kony.print("Error in CreateContractLog getCurrentDate:::"+err);
    }
  },
*/
    postShowFunctionCall: function() {
        try {
            this.initializeCustomComponent();
            this.populatingTitleDropDown();
            //this.view.ProgressIndicator.isVisible=false;
        } catch (err) {
            alert("Error in CustomerLoanInterestChangeController postFunctionCall:::" + err);
        }
    },
    closeSegmentOverrite: function() {
        this.view.SegmentPopup.isVisible = false;
        this.view.forceLayout();
    },
    onClickCancel: function() {
        var navToHome = new kony.mvc.Navigation("frmDashboard");
        var navObjet = {};
        navObjet.clientId = "";
        navToHome.navigate(navObjet);
    },
    closeErrorAllert: function() {
        this.view.ErrorAllert.isVisible = false;
        this.view.flxHeaderMenu.isVisible = true;
    },
    initializeCustomComponent: function() {
        this.view.custEmail.iconButtonIcon = "email";
        this.view.custEmail.showIconButtonTrailing = true;
        this.view.custPhoneNumber.iconButtonIcon = "phone";
        this.view.custPhoneNumber.showIconButtonTrailing = "true";
        this.view.custViewCustomerButton.icon = "account_circle";
        this.view.custViewCustomerButton.size = "large";
        this.view.nationalityDropDownList.placeholder = "Nationality";
        //this.view.custAlternateRefName.disabled=true;
        this.view.custFirstName.maxLength = 35;
        this.view.custLastName.maxLength = 35;
        this.view.custEmail.maxLength = 50;
        this.view.custPhoneNumber.maxLength = 17;
        this.view.custAlternateRefName.maxLength = 10;
        this.view.custStreet.maxLength = 35;
        this.view.custAddress.maxLength = 35;
        this.view.custTown.maxLength = 35;
        this.view.custPostalCode.maxLength = 35;
        this.view.custEmployersName.maxLength = 35;
        this.view.custCityTextField.maxLength = 17;
        //this.view.custFirstName.borderColor="red";
    },
    uppercaseToFirtCharacter: function() {
        kony.print("enter in validation::::");
        var firstName = this.view.custFirstName.value; //35
        var lastName = this.view.custLastName.value; //35
        firstName = firstName.trim();
        lastName = lastName.trim();
        firstName = firstName.replace(/[^a-zA-Z 0-9]+/g, '');
        var firstChar = firstName.charAt(0);
        var remainingStr = firstName.slice(1);
        firstName = firstChar.toUpperCase() + remainingStr;
        this.view.custFirstName.value = firstName;
        lastName = lastName.replace(/[^a-zA-Z 0-9&/]+/g, '');
        var firstChar1 = lastName.charAt(0);
        var remainingStr1 = lastName.slice(1);
        lastName = firstChar1.toUpperCase() + remainingStr1;
        this.view.custLastName.value = lastName;
    },
    restictSpeicalChars: function() {
        var firstName = this.view.custFirstName.value;
        if (firstName !== "") {
            var specialCharRest = /^[a-zA-Z&/]+$/;
            var regExpValue = specialCharRest.test(firstName);
            kony.print("regValue::::" + regExpValue);
            if (regExpValue === false) {
                alert("speical characters not allowed except & and /");
            }
        }
    },
    updateDateOfBirth: function() {
        var dob = this.view.custDateOfBirth.valu;
        var d = new Date(dob);
        var YYYY = d.getFullYear();
        var YY = YYYY.toString().substring(2);
        var MM = ("0" + (d.getMonth() + 1)).slice(-2);
        return YY + MM + "01";
    },
    updateAlternateRefName: function() {
        kony.print("entered in updated value::::");
        var firstName = this.view.custFirstName.value;
        var lastName = this.view.custLastName.value;
        var dob = this.view.custDateOfBirth.value;
        /* if(firstName !== "" || lastName !==""){ 
          var specialCharRest = /^[a-zA-Z&/]+$/;
          var regExpValue = specialCharRest.test(firstName);
          kony.print("regValue::::"+regExpValue);
          if(regExpValue === false){
            //alert("speical characters not allowed except & and /");
            firstName=firstName.replace(/^[a-zA-Z&/]+$/,'');
            this.view.custFirstName.value=firstName;
          }   
        }*/
        firstName = firstName.trim();
        lastName = lastName.trim();
        if (firstName !== "") {
            firstName = firstName.replace(/[^a-zA-Z 0-9]+/g, '');
            firstName = firstName.charAt(0).toLocaleUpperCase();
            this.view.custAlternateRefName.value = firstName;
        }
        if (lastName !== "") {
            lastName = lastName.replace(/[^a-zA-Z 0-9]+/g, '');
            lastName = lastName.substr(0, 3).toLocaleUpperCase();
            this.view.custAlternateRefName.value = lastName + firstName;
        }
        if (dob !== "") {
            kony.print("in dobb....");
            var d = new Date(dob);
            var YYYY = d.getFullYear();
            var YY = YYYY.toString().substring(2);
            var MM = ("0" + (d.getMonth() + 1)).slice(-2);
            //var alterRefName=(lastName+firstName).toLocaleUpperCase();
            this.view.custAlternateRefName.value = lastName + firstName + YY + MM + "01";
        }
        /*  firstName=firstName.replace(/[&/]+/g,'');
    var firstChar = firstName.charAt(0);
    var remainingStr = this.view.custFirstName.value.slice(1);
    firstName = firstChar.toUpperCase() + remainingStr;
    this.view.custFirstName.value=firstName;
	kony.print("lastName:::::"+lastName);

    lastName=lastName.replace(/[&/]+/g,'');
    var firstChar1 = lastName.charAt(0);
    var remainingStr1 = this.view.custLastName.value.slice(1);
    lastName = firstChar1.toUpperCase() + remainingStr1;
    this.view.custLastName.value=lastName;*/
    },
    validatePhoneNumber: function() {
        var usrid = this.view.custPhoneNumber.value; //txtUserid-->ID of textbox
        kony.print("phone number" + usrid);
        var numeric = /^\d[0-9]{9}$/; //This contains A to Z , 0 to 9 and A to B
        if (usrid.match(numeric)) {
            return true;
        } else {
            // alert("Enter a Valid phone number ");
            return false;
        }
    },
    validateEmail: function() {
        var emailId = this.view.custEmail.value;
        if (emailId) {
            var emailRegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            var regExpValue = emailRegExp.test(emailId);
            if (regExpValue) {
                //this.validatePhoneNumber();
            } else {
                alert("Please enter valid email");
                return;
            }
        }
    },
    populatingTitleDropDown: function() {
        try {
            this.view.custTitleDropDown.options = '[{"label": "Please Select"},{"label": "DR"}, {"label": "MISS"}, {"label": "MR"},  {"label": "MRS"}, {"label": "MS"}]';
            this.view.custTitleDropDown.value = "Please Select";
            this.view.custGenderDropDown.options = '[{"label": "Please Select"},{"label": "Male"}, {"label": "Female"}, {"label": "Others"}]';
            this.view.custGenderDropDown.value = "Please Select";
            this.view.custEmploymentStatusDropDown.options = '[{"label": "Please Select","value":"Select"},{"label": "Hired", "value":"EMPLOYED"}, {"label": "Retired","value":"RETIRED"}, {"label": "Freelance","value":"SELF-EMPLOYED"}, {"label": "Educatee","value":"STUDENT"}, {"label": "Unemployed","value":"UNEMPLOYED"}, {"label": "Other","value":"OTHER"}]';
            this.view.custEmploymentStatusDropDown.value = "Select";
            this.view.custAccountOfficerDropDown.options = '[{"label": "Retail Banking Mgr","value":"2"}, {"label": "Retail Banking-3","value":"3"}, {"label": "Customer Service Agent","value":"26"}, {"label": "Retail Credit Officer","value":"28"}, {"label": "Retail Credit Manager","value":"29"}]';
            this.view.custAccountOfficerDropDown.value = "26";
            this.view.custSectorDropDown.options = '[{"label": "Individual","value":"1001"}, {"label": "Staff","value":"1002"}, {"label": "High Networth","value":"1003"}, {"label": "Director","value":"1004"}, {"label": "Principal Shareholder","value":"1005"}, {"label": "Executive Officer","value":"1006"}, {"label": "Director (Employee)","value":"1007"}, {"label": "Principal Shareholder (Employee)","value":"1008"}, {"label": "Individuals with entrepreneurial actvivity (Self-employed)","value":"1009"}, {"label": "Executive Officer (Employee)","value":"1010"}, {"label": "Others","value":"9000"}]';
            this.view.custSectorDropDown.value = "1001";
        } catch (err) {
            kony.print("Error in CustomerCreationController populatingTitleDropDown:::" + err);
        }
    },
    fetchCuntryDetails: function() {
        this.view.ProgressIndicator.isVisible = true;
        var serviceName = "LendingNew";
        var operationName = "getCountries";
        var headers = "";
        var inputParams = "";
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunction90res, this.failureCallBackfunction90res);
    },
    successCallBackfunction90res: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        var responseData = res.body;
        var arrayData = [{
            "label": "Please Select",
            "value": "Select"
        }];
        if (responseData.length > 0) {
            for (var j in responseData) {
                var jsonArray = {
                    "label": responseData[j].displayName,
                    "value": responseData[j].countryId
                };
                arrayData.push(jsonArray);
            }
        }
        kony.print("loading Dynamic countries ::::" + JSON.stringify(arrayData));
        this.view.custCountryDropDown.options = JSON.stringify(arrayData);
        // this.view.custCountryDropDown.value="Select";
        this.view.custResidenceDropDown.options = JSON.stringify(arrayData);
        //this.view.custResidenceDropDown.value="Select";
        this.view.custNationalityDropDown.options = JSON.stringify(arrayData);
        //this.view.custNationalityDropDown.value="Select";
        this.view.forceLayout();
    },
    failureCallBackfunction90res: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        kony.print("Error in fetching countries...." + JSON.stringify(res));
    },
    onClickSubmitData: function() {
        // this.validateEmail();
        // this.updateAlternateRefName();
        var firstName = this.view.custFirstName.value;
        var alterRefName = this.view.custAlternateRefName.value;
        var phoneNumber = this.view.custPhoneNumber.value;
        var sector = this.view.custSectorDropDown.value;
        var lastName = this.view.custLastName.value;
        var dob = this.view.custDateOfBirth.value;
        var isValidData = this.validateData();
        if (isValidData === 0) {
            if (firstName !== "" && alterRefName !== "" && phoneNumber !== "" && sector !== "" && lastName !== "" && dob !== "") {
                if (this._duplicateId === "customerMnemonic") {
                    var number = this.view.custAlternateRefName.value;
                    number = number.slice(-2);
                    number = parseInt(number);
                    alterRefName = alterRefName.slice(0, -2) + "0" + (number + 1);
                    this.view.custAlternateRefName.value = alterRefName;
                }
                this.view.lblErrorMsg.isVisible = false;
                this.view.SegmentPopup.isVisible = false;
                var title = this.view.custTitleDropDown.value;
                var gender = this.view.custGenderDropDown.value;
                var nationality = this.view.custNationalityDropDown.value;
                var residency = this.view.custResidenceDropDown.value;
                var country = this.view.custCountryDropDown.value;
                var employeeStatus = this.view.custEmploymentStatusDropDown.value;
                var accountOfficer = this.view.custAccountOfficerDropDown.value;
                if (title === "Please Select") {
                    title = "";
                }
                if (gender === "Please Select") {
                    gender = "";
                }
                if (nationality === "Select") {
                    nationality = "";
                }
                if (country === "Select") {
                    country = "";
                }
                if (employeeStatus === "Select") {
                    employeeStatus = "";
                }
                if (residency === "Select") {
                    residency = "";
                }
                var d = new Date(dob);
                var YYYY = d.getFullYear();
                var MM = ("0" + (d.getMonth() + 1)).slice(-2);
                var date = "" + d.getDate();
                date = date.length > 1 ? date : '0' + date;
                dob = YYYY + "-" + MM + "-" + date;
                this.view.ProgressIndicator.isVisible = true;
                var serviceName = "LendingNew";
                var operationName = "postCustomerCreation";
                var headers = "";
                var inputParams = {};
                inputParams.title = title;
                inputParams.customerName = firstName;
                inputParams.lastName = lastName;
                inputParams.customerNameAdditional = this.view.custLastName.value;
                inputParams.gender = gender.toUpperCase();
                inputParams.dateOfBirth = dob;
                inputParams.nationalityId = nationality;
                inputParams.residenceId = residency;
                inputParams.email = this.view.custEmail.value;
                inputParams.phoneNumber = phoneNumber;
                inputParams.street = this.view.custStreet.value;
                inputParams.address = this.view.custAddress.value;
                inputParams.addressCity = this.view.custCityTextField.value;
                inputParams.town = this.view.custTown.value;
                inputParams.country = country;
                inputParams.postCode = this.view.custPostalCode.value;
                inputParams.employStatus = employeeStatus;
                inputParams.employerName = this.view.custEmployersName.value;
                inputParams.accountOfficerId = accountOfficer;
                inputParams.sectorId = sector;
                inputParams.customerMnemonic = alterRefName;
                inputParams.displayName = firstName;
                inputParams.overrideDetails = this.currentOverride;
                MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunction, this.failureCallBackfunction);
            }
        } else {
            this.view.lblErrorMsg.isVisible = true;
        }
    },
    successCallBackfunction: function(res) {
        this.currentOverride = {};
        this.view.ProgressIndicator.isVisible = false;
        kony.print("json response::::" + JSON.stringify(res.header));
        var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.isShowLoans = "true";
        navObjet.customerId = res.header["id"];
        navObjet.clientId = res.header["id"];
        navToChangeInterest1.navigate(navObjet);
    },
    failureCallBackfunction: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        this.currentOverride = {};
        kony.print("Error response from post customer creation API::::: " + JSON.stringify(res));
        if (res.override !== "" && res.override !== null && res.override !== undefined) {
            var overwriteDetails = [];
            var overWriteData = [];
            var segDataForOverrite = [];
            this.currentOverride = res.override.overrideDetails;
            overwriteDetails = res.override.overrideDetails;
            if (overwriteDetails.length > 0) {
                for (var a in overwriteDetails) {
                    overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                    if (overwriteDetails[a].id === "DUP.CONTRACT") {
                        // this._duplicateId=overwriteDetails[a].id;
                        //kony.print("Id:::::"+this._duplicateId);
                    }
                    var json = {
                        "lblSerial": "*",
                        "lblInfo": overwriteDetails[a].description
                    };
                    segDataForOverrite.push(json);
                }
                this.view.SegmentPopup.segOverride.widgetDataMap = {
                    "lblSerial": "lblSerial",
                    "lblInfo": "lblInfo"
                };
                this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
                this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
                this.view.ErrorAllert.isVisible = false;
                this.view.SegmentPopup.isVisible = true;
                kony.print("overWriteData::::" + JSON.stringify(overWriteData));
                kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
                this.view.forceLayout();
            }
        }
        var response = res.error;
        if (response !== "" && response !== null && response !== undefined) {
            var errorDetails = [];
            var errorDetailsInSingle = "";
            errorDetails = res.error.errorDetails;
            kony.print("errorDetails::::::" + JSON.stringify(errorDetails));
            if (errorDetails.length > 0) {
                this.view.flxHeaderMenu.isVisible = false;
                this.view.ErrorAllert.isVisible = true;
                this.view.SegmentPopup.isVisible = false;
                for (var i in errorDetails) {
                    if (errorDetailsInSingle === null || errorDetailsInSingle === "" || errorDetailsInSingle === undefined) {
                        errorDetailsInSingle = errorDetails[i].message;
                        this._duplicateId = errorDetails[i].fieldName;
                    } else {
                        errorDetailsInSingle = errorDetailsInSingle + " , " + errorDetails[i].message;
                    }
                }
            }
            kony.print("rsponse::::::" + errorDetailsInSingle);
            this.view.ErrorAllert.lblMessage.text = errorDetailsInSingle;
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    validateData: function() {
        var Requiredflag = 0;
        var FormId = document.getElementsByTagName("form")[0].getAttribute("id");
        var form = document.getElementById(FormId);
        var Requiredfilter = form.querySelectorAll('*[required]');
        for (var i = 0; i < Requiredfilter.length; i++) {
            if (Requiredfilter[i].disabled !== true && Requiredfilter[i].value === '') {
                var ElementId = Requiredfilter[i].id;
                var Element = document.getElementById(ElementId);
                Element.reportValidity();
                Requiredflag = 1;
            }
        }
        return Requiredflag;
    },
});
define("frmCustomerCreationControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_c52782fa925f42ce84d1141a49d10d65: function AS_UWI_c52782fa925f42ce84d1141a49d10d65(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_fb1cefdc91dd404ab8b02f8c76e05629: function AS_UWI_fb1cefdc91dd404ab8b02f8c76e05629(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmCustomerCreationController", ["userfrmCustomerCreationController", "frmCustomerCreationControllerActions"], function() {
    var controller = require("userfrmCustomerCreationController");
    var controllerActions = ["frmCustomerCreationControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
