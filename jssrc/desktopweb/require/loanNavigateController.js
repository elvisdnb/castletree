define("userloanNavigateController", {
    onNavigate: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        this.view.postShow = this.postShowFunctionCall;
        this.view.DPWSelectLoan.valueChanged = this.getLoanValues;
        this.view.btnTestGraph.onClick = this.reloadGraph;
        this.view.btntestGraph2.onClick = this.btntestGraph2;
        this.view.btnGetValue.onClick = this.getUUXValue;
        this.view.UUXReccurencePicker.onValueSelected = this.freqValSelectionDone;
    },
    postShowFunctionCall: function() {
        this.view.DPWSelectLoan.options = '[{"label": "Select","value":"None"},{"label": "Personal Loan","value":"PERSONAL.LOAN.STANDARD"},{"label": "Mortgage Loan","value":"MORTGAGE"}]';
    },
    getLoanValues: function() {
        var selectedValues = this.view.DPWSelectLoan.value;
        var navigatetoloan = new kony.mvc.Navigation("frmLoanCreation");
        navigatetoloan.navigate(selectedValues);
    },
    getUUXValue: function() {
        var val = this.view.UUXReccurencePicker.value;
        kony.print("************val*********>" + val);
    },
    freqValSelectionDone: function() {
        var val = this.view.UUXReccurencePicker.formattedValue;
        kony.print("************val*********>" + val);
    },
    reloadGraph() {
        var mydatasets = [];
        var series1Data = {
            label: "Personal Loans",
            backgroundColor: "#FF9800",
            data: [10, 11, 9, 8, 7, 6, 10]
        };
        var series2Data = {
            label: "Mortgage Loans",
            backgroundColor: "#2C8631",
            data: [8, 4, 4, 5, 9, 6, 4]
        };
        mydatasets.push(series1Data);
        mydatasets.push(series2Data);
        var myxlabels = ["Day 1", "Day 2", "Day 3", "Day 4", "Day 5", "Day 6", "Day 7"];
        this.view.barGraph1.isVisible = true; //only after the widget is initialized
        this.view.barGraph1.datasetsArray = JSON.stringify(mydatasets);
        this.view.barGraph1.xlabelsArray = JSON.stringify(myxlabels);
        this.view.barGraph1.updateGraph();
    },
    btntestGraph2: function() {
        var mydatasets = [];
        var series1Data = {
            label: "Personal Loans",
            backgroundColor: "#FF9800",
            data: [10, 11, 9, 8]
        };
        var series2Data = {
            label: "Mortgage Loans",
            backgroundColor: "#2C8631",
            data: [8, 4, 4, 5]
        };
        mydatasets.push(series1Data);
        mydatasets.push(series2Data);
        var myxlabels = ["Day 1", "Day 2", "Day 3", "Day 4"];
        this.view.fullGraph1.isVisible = true; //only after the widget is initialized
        this.view.fullGraph1.datasetsArray = JSON.stringify(mydatasets);
        this.view.fullGraph1.xlabelsArray = JSON.stringify(myxlabels);
        this.view.fullGraph1.updateGraph();
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
            this.view.helpCenter.isVisible = true;
            this.view.help.isVisible = false;
            this.view.getStarted.isVisible = false;
        }
        /*
  reloadGraph(){
    //var labelsArr = [ "a", "b", "c"];;
    //data: [10,20,30] 
    //var labelsArr = ["05 MAR 2028", "05 APR 2028", "05 MAY 2028", "05 JUN 2028", "05 JUL 2028", "05 AUG 2028", "05 SEP 2028", "05 OCT 2028", "05 NOV 2028", "05 DEC 2028", "05 JAN 2029", "05 FEB 2029", "05 MAR 2029", "05 APR 2029"];
    var labelsArr = ["05 FEB 2021","05 MAR 2021","05 APR 2021","05 MAY 2021","05 JUN 2021","05 JUL 2021","05 AUG 2021","05 SEP 2021","05 OCT 2021","05 NOV 2021","05 DEC 2021","05 JAN 2022"];
    //Does not work//var datapointsArr = ["1,282.08", "1,282.08", "1,282.08", "1,282.08", "1,282.08", "1,282.08", "1,282.08", "1,282.08", "1,282.08", "1,282.08", "1,282.08", "1,282.08"];
    ///Does not work/var datapointsArr = ["1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08", "1282.08"];
    //var datapointsArr = [1282.08, 1282.08, 1282.08, 1282.08, 1282.08, 1282.08, 1282.08, 1082.08, 1282.08, 1282.08, 1282.08, 1282.08, 1282.08, 1282.08];
    var datapointsArr = [7828.66,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06];
    var datapointsArr2 = [7828.66,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06,9193.06];
    var dataMax = Math.round(Math.max.apply(null,datapointsArr)/100)*100;

    this.view.multiline.chartTitle = "Current repayment";
    this.view.multiline.bgColor = "#003e75";
    this.view.multiline.enableGrid = true;
    this.view.multiline.enableLegends = false;
    this.view.multiline.xAxisTitle = "Date Line";            
    this.view.multiline.enableStaticPreview = true;
    this.view.multiline.legendFontSize = "95%";
    this.view.multiline.enableGridAnimation = true;
    this.view.multiline.titleFontSize = "12";
    this.view.multiline.lowValue = 0;
    this.view.multiline.titleFontColor = "#ffffff";
    this.view.multiline.legendFontColor = "#ffffff";
    this.view.multiline.highValue = dataMax;
    this.view.multiline.bgColor = "#314ebc";
    this.view.multiline.enableChartAnimation = false;

    var series = [
        {"name":"before", "data": datapointsArr },
        {"name":"after", "data": datapointsArr2 }
    ] ;

    var colors = ["#1B9ED9", "#76C044"];

    //this.view.multiline.createChart(dataSet, lineDetails);
    this.view.multiline.createChartRawData(labelsArr,series,colors);
  }
 */
});
define("loanNavigateControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_e20af3cddd0b4c9ca1e2fe1437b0dde7: function AS_UWI_e20af3cddd0b4c9ca1e2fe1437b0dde7(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_d596066755f845b9b391133a3c31725b: function AS_UWI_d596066755f845b9b391133a3c31725b(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("loanNavigateController", ["userloanNavigateController", "loanNavigateControllerActions"], function() {
    var controller = require("userloanNavigateController");
    var controllerActions = ["loanNavigateControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
