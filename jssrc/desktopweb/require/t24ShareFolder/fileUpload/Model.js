/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {
        "object": "fileUpload",
        "objectService": "t24ShareFolder"
    };
    var setterFunctions = {
        id: function(val, state) {
            context["field"] = "id";
            context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
            state['id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        name: function(val, state) {
            context["field"] = "name";
            context["metadata"] = (objectMetadata ? objectMetadata["name"] : null);
            state['name'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        file: function(val, state) {
            context["field"] = "file";
            context["metadata"] = (objectMetadata ? objectMetadata["file"] : null);
            state['file'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        modifiedBy: function(val, state) {
            context["field"] = "modifiedBy";
            context["metadata"] = (objectMetadata ? objectMetadata["modifiedBy"] : null);
            state['modifiedBy'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        createdBy: function(val, state) {
            context["field"] = "createdBy";
            context["metadata"] = (objectMetadata ? objectMetadata["createdBy"] : null);
            state['createdBy'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        status: function(val, state) {
            context["field"] = "status";
            context["metadata"] = (objectMetadata ? objectMetadata["status"] : null);
            state['status'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };
    //Create the Model Class
    function fileUpload(defaultValues) {
        var privateState = {};
        context["field"] = "id";
        context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
        privateState.id = defaultValues ? (defaultValues["id"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["id"], context) : null) : null;
        context["field"] = "name";
        context["metadata"] = (objectMetadata ? objectMetadata["name"] : null);
        privateState.name = defaultValues ? (defaultValues["name"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["name"], context) : null) : null;
        context["field"] = "file";
        context["metadata"] = (objectMetadata ? objectMetadata["file"] : null);
        privateState.file = defaultValues ? (defaultValues["file"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["file"], context) : null) : null;
        context["field"] = "modifiedBy";
        context["metadata"] = (objectMetadata ? objectMetadata["modifiedBy"] : null);
        privateState.modifiedBy = defaultValues ? (defaultValues["modifiedBy"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["modifiedBy"], context) : null) : null;
        context["field"] = "createdBy";
        context["metadata"] = (objectMetadata ? objectMetadata["createdBy"] : null);
        privateState.createdBy = defaultValues ? (defaultValues["createdBy"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["createdBy"], context) : null) : null;
        context["field"] = "status";
        context["metadata"] = (objectMetadata ? objectMetadata["status"] : null);
        privateState.status = defaultValues ? (defaultValues["status"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["status"], context) : null) : null;
        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);
        //Defining Getter/Setters
        Object.defineProperties(this, {
            "id": {
                get: function() {
                    context["field"] = "id";
                    context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.id, context);
                },
                set: function(val) {
                    setterFunctions['id'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "name": {
                get: function() {
                    context["field"] = "name";
                    context["metadata"] = (objectMetadata ? objectMetadata["name"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.name, context);
                },
                set: function(val) {
                    setterFunctions['name'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "file": {
                get: function() {
                    context["field"] = "file";
                    context["metadata"] = (objectMetadata ? objectMetadata["file"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.file, context);
                },
                set: function(val) {
                    setterFunctions['file'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "modifiedBy": {
                get: function() {
                    context["field"] = "modifiedBy";
                    context["metadata"] = (objectMetadata ? objectMetadata["modifiedBy"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.modifiedBy, context);
                },
                set: function(val) {
                    setterFunctions['modifiedBy'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "createdBy": {
                get: function() {
                    context["field"] = "createdBy";
                    context["metadata"] = (objectMetadata ? objectMetadata["createdBy"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.createdBy, context);
                },
                set: function(val) {
                    setterFunctions['createdBy'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "status": {
                get: function() {
                    context["field"] = "status";
                    context["metadata"] = (objectMetadata ? objectMetadata["status"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.status, context);
                },
                set: function(val) {
                    setterFunctions['status'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });
        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };
        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.id = value ? (value["id"] ? value["id"] : null) : null;
            privateState.name = value ? (value["name"] ? value["name"] : null) : null;
            privateState.file = value ? (value["file"] ? value["file"] : null) : null;
            privateState.modifiedBy = value ? (value["modifiedBy"] ? value["modifiedBy"] : null) : null;
            privateState.createdBy = value ? (value["createdBy"] ? value["createdBy"] : null) : null;
            privateState.status = value ? (value["status"] ? value["status"] : null) : null;
        };
    }
    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(fileUpload);
    //Create new class level validator object
    BaseModel.Validator.call(fileUpload);
    var registerValidatorBackup = fileUpload.registerValidator;
    fileUpload.registerValidator = function() {
            var propName = arguments[0];
            if (!setterFunctions[propName].changed) {
                var setterBackup = setterFunctions[propName];
                setterFunctions[arguments[0]] = function() {
                    if (fileUpload.isValid(this, propName, val)) {
                        return setterBackup.apply(null, arguments);
                    } else {
                        throw Error("Validation failed for " + propName + " : " + val);
                    }
                }
                setterFunctions[arguments[0]].changed = true;
            }
            return registerValidatorBackup.apply(null, arguments);
        }
        //Extending Model for custom operations
    var relations = [];
    fileUpload.relations = relations;
    fileUpload.prototype.isValid = function() {
        return fileUpload.isValid(this);
    };
    fileUpload.prototype.objModelName = "fileUpload";
    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    fileUpload.registerProcessors = function(options, successCallback, failureCallback) {
        if (!options) {
            options = {};
        }
        if (options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }
        if (options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }
        kony.mvc.util.ProcessorUtils.getMetadataForObject("t24ShareFolder", "fileUpload", options, metaDataSuccess, metaDataFailure);
    };
    //clone the object provided in argument.
    fileUpload.clone = function(objectToClone) {
        var clonedObj = new fileUpload();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };
    return fileUpload;
});