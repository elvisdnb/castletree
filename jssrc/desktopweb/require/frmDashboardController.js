define("userfrmDashboardController", {
    gblCustomerID: null,
    cntALL: null,
    cntCompleted: '',
    cntPending: '',
    cntNew: '',
    roleid: '',
    dataPerPageNumber: 4,
    LoanPerPage: 4,
    _fullActivityResponse: [],
    completedPercent: "",
    _crAcvitySegData: [],
    graphCountArrays: [],
    graphXArraysWeek: [],
    graphXArraysHalfMonth: [],
    graphXArraysMonth: [],
    graphDataPersonalWeek: [],
    graphDataPersonalHalfMonth: [],
    graphDataPersonalMonth: [],
    graphDataMortgagesWeek: [],
    graphDataMortgagesHalfMonth: [],
    graphDataMortgagesMonth: [],
    XLabelSub: [],
    PersonalWeek1Count: "",
    MortgageWeek1Count: "",
    SumLoan: "",
    rolename: '',
    DetailsArray: [],
    accountArray: [],
    onNavigate: function(navObj) {
        gblCurrentContractLog = [];
        this.loadGraph();
        if (navObj && navObj.clientId !== "") {} else {
            this.view.flxMessageInfo.isVisible = false;
        }
        // for displaying the success message & hide poup while coming back
        var navigationObject = Application.validation.isNullUndefinedObj(navObj);
        if (navigationObject !== "") {
            if (navObj.messageInfo) {
                this.view.flxMessageInfo.isVisible = true;
                kony.print("message:::" + navObj.messageInfo);
                this.view.flxMessageInfo.lblMessageWithReferenceNumber.text = navObj.messageInfo;
            }
            if (navObj.isPopupShow === "false") {
                this.view.flxMessageInfo.isVisible = false;
            }
        }
        //This wil be also used when click on nav rail
        if (gblDashboardObj !== "") {
            this.roleid = gblDashboardObj.userRole;
            this.rolename = gblDashboardObj.userSignOnName;
        } else {
            gblDashboardObj = navObj;
            this.roleid = navObj.userRole;
            this.rolename = navObj.userSignOnName;
        }
        this.view.ProgressIndicator.isVisible = true;
        this.view.SegA.removeAll();
        this.bindEvents();
        this.loadAllPayAccounts();
    },
    bindEvents: function() {
        this.view.txtField1.onDone = this.searchCustomer;
        this.view.imgSrc.onTouchEnd = this.searchCustomer;
        this.view.flxMessageInfo.imgHeaderClose.onTouchEnd = this.closeSuccessPopup;
        this.view.flxMessageInfo.custBtnViewclient.onclickEvent = this.moveToCustomerAccountDetails;
        this.view.txtPartialText.onEndEditing = this.doCRSearchWithinSeg;
        this.view.txtPartialText.onDone = this.doCRSearchWithinSeg;
        this.view.txtPartialText.onKeyUp = this.doCRSearchWithinSeg;
        this.view.imgSearchIcon.onDone = this.doCRSearchWithinSeg;
        this.view.btnNew.onClick = this.newdata;
        this.view.btnAll.onClick = this.alldata;
        this.view.btnPending.onClick = this.pendingdata;
        this.view.btnCompleted.onClick = this.completeddata;
        this.view.btnCancel.onClick = this.canceldata;
        this.view.btnAddNewRequest.onClick = this.navigateToCreateLog;
        this.view.btnNext.onTouchEnd = this.showChangeRequestListData;
        this.view.btnPrev.onTouchEnd = this.showChangeRequestListData;
        this.view.btnPrevBranch.onTouchEnd = this.customerBranchPage;
        this.view.btnNextBranch.onTouchEnd = this.customerBranchPage;
        this.view.btnPrevLoan.onTouchEnd = this.LoanDetailsPage;
        this.view.btnNextLoan.onTouchEnd = this.LoanDetailsPage;
        this.view.segLoan7Days.onRowClick = this.navigatefunctionCustomer360;
        this.view.btnClient.onClick = this.navigateCustomerCreationfunction;
        this.view.custCancel.onclickEvent = this.onBranchCancelClick;
        this.view.custProceed.onclickEvent = this.onBranchSubmitClick;
        this.view.SegA.onRowClick = this.navigatefunctionToCustomerAccountList;
        this.view.lblViewGraphDetails.onTouchEnd = this.GraphPopup;
        this.view.flxHeaderSegmenPopupClose.onTouchEnd = this.GraphPopupClose;
        this.view.btnPrevious.onTouchEnd = source => this.showPaginationData(source.id);
        this.view.btnNextView.onTouchEnd = source => this.showPaginationData(source.id);
        // this.view.TPWDaysToggle1.onclickEvent = this.DaysSelection;
        this.view.segAccData.onRowClick = this.navigatefunctionCustomer360View;
        this.view.cusToggleBtn.onclickEvent = this.onClickLiabilityTogglebtn;
        // this.view.TPWDaysToggle1.onclickEvent = this.DaysSelection;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.onCloseErrorAllert;
        this.view.flxCustomerBranchBg.left = "-200%";
        this.view.flxGraphwrapper.left = "-200%";
        this.view.cusSelectMode.options = [{
            "label": "None",
            "value": "None"
        }, {
            "label": "Auto",
            "value": "AUTO"
        }, {
            "label": "Clear",
            "value": "CLEAR"
        }];
        //  this.view.TPWDaysToggle1.options ='[{"label":"7 Days","value":"7Days"},{"label":"15 Days","value":"15Days"},{"label":"1 Month","value":"1Month"}]';
        //this.view.TPWDaysToggle1.value ="7Days";
        this.view.cusSelectMode.value = "None";
        this.getNavDetails();
        this.getLoanMatureDetails();
        this.getLoanPortfolio();
        this.getBranchCustomers();
        // this.loadGraph();
    },
    navigateToCreateLog: function() {
        var params = {};
        var navToContractLog = new kony.mvc.Navigation("createContractLog");
        navToContractLog.navigate(params);
    },
    onCloseErrorAllert: function() {
        this.view.ErrorAllert.isVisible = false;
    },
    searchCustomer: function(eventObj) {
        var changedText = this.view.txtField1.text;
        if (changedText && changedText != "") {
            var customerId = changedText; //this.view.segMin2.data[0];//selectedRowItems[0];  
            var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObjet = {};
            navObjet.customerId = customerId;
            navObjet.isShowLoans = "true";
            navObjet.isPopupShow = "false";
            navToChangeInterest1.navigate(navObjet);
        } else {
            var ntpl = new kony.mvc.Navigation("frmCustomersList");
            ntpl.navigate();
        }
    },
    onPostShow: function() {},
    removeMinus: function(x) {
        try {
            return Math.abs(x);
        } catch (err) {
            kony.print("error in removeMinus:::" + err);
        }
    },
    getLoanMatureDetails: function() {
        try {
            var serviceName = "DashbordLiability";
            var operationName = "getDepositMatureSevenDays";
            var headers = {
                "companyId": "NL0020001"
            };
            var inputParams = {};
            //headers.credentials=this.rolename;
            //headers.credentials="LENDINGOFFICER/123456";
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBLoanMature, this.failureCBLoanMature);
        } catch (err) {
            kony.print("Error in getLoanMAtureDetails:::: " + err);
        }
    },
    successCBLoanMature: function(response) {
        var res = response.body;
        if (res.length >= 1) {
            this.view.segLoan7Days.isVisible = true;
            this.view.flxPaginationLoan.isVisible = true;
            this.view.lblNoRecords1.isVisible = false;
            var dataMap = {
                "lblLoanCustID": "lblLoanCustID",
                "lblLoanName": "lblLoanName",
                "lblLoanAccountID": "lblLoanAccountID",
                "lblLoanProduct": "lblLoanProduct",
                "lblLoanCurrency": "lblLoanCurrency",
                "lblLoanAmt": "lblLoanAmt",
                "lblLoanDate": "lblLoanDate"
            };
            this.view.segLoan7Days.widgetDataMap = dataMap;
            Jsondata = [];
            for (var i = 0; i < res.length; i++) {
                var depCurrency = res[i].ccy;
                depCurrency = Application.validation.isNullUndefinedObj(depCurrency);
                var depBalance = res[i].amount;
                depBalance = Application.validation.isNullUndefinedObj(depBalance);
                if (depBalance !== "") {
                    depBalance = depBalance.replace(/[\s,]+/g, ''); //remove commas
                    depBalance = Application.numberFormater.convertForDispaly(this.removeMinus(depBalance), depCurrency);
                    if (depBalance === "NaN") {
                        depBalance = "0.00";
                    }
                } else {
                    depBalance = Application.numberFormater.convertForDispaly(this.removeMinus("0"), depCurrency);
                }
                Jsondata.push({
                    "lblLoanCustID": res[i].customerId,
                    "lblLoanName": res[i].customer,
                    "lblLoanAccountID": res[i].account,
                    "lblLoanProduct": res[i].product,
                    "lblLoanCurrency": depCurrency,
                    "lblLoanAmt": depBalance,
                    "lblLoanDate": res[i].maturityDate
                });
            }
            // this._fullActivityResponse = response;
            //this.setCRcontactLogResToSeg(response);
            this._LoanDetailsdata = Jsondata;
            this.LoanDetailsPage("");
            //this.view.segLoan7Days.setData(Jsondata);
            //   this.view.segLoan7Days.isVisible=true;
            // this.view.lblNoRecords.isVisible=false;
        } else {
            this.view.segLoan7Days.isVisible = false;
            this.view.flxPaginationLoan.isVisible = false;
            this.view.lblNoRecords1.isVisible = true;
            this.view.lblNoRecords1.text = "No Deposists Maturing within 7 Days";
        }
    },
    failureCBLoanMature: function(response) {
        this.view.ProgressIndicator.isVisible = false;
        if (response.error && response.error.length !== 0) {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = response.error.message;
        } else {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = response.errmsg;
        }
    },
    GraphPopup: function() {
        this.view.flxGraphwrapper.left = "0%";
        // this.view.TPWDaysToggle1.value ="7Days";
        this.graphCountArrays = [];
        this.graphXArraysWeek = [];
        this.graphXArraysHalfMonth = [];
        this.graphXArraysMonth = [];
        this.PersonalWeek1Count = 0;
        this.MortgageWeek1Count = 0;
        for (i = 0; i < this.graphDataPersonalWeek.length; i++) {
            this.PersonalWeek1Count += this.graphDataPersonalWeek[i];
            this.MortgageWeek1Count += this.graphDataMortgagesWeek[i];
        }
        this.SumLoan = (this.PersonalWeek1Count) + (this.MortgageWeek1Count);
        this.view.lblGraphCount.text = this.SumLoan.toString();
        this.view.lblDaysCounts.text = "In the Past 7 Days";
        var series1Data = {
            label: "Personal Loans",
            backgroundColor: "rgba(88, 37, 220, 0.2)",
            pointBackgroundColor: "#5831D8",
            pointBorderColor: "#5831D8",
            borderColor: "#5831D8",
            borderWidth: 1,
            pointRadius: 0,
            data: this.graphDataPersonalWeek
        };
        var series2Data = {
            label: "Mortgage Loans",
            backgroundColor: "rgba(219, 102, 110, 0.2)",
            pointBackgroundColor: "#DB666E",
            pointBorderColor: "#DB666E",
            borderColor: "#DB666E",
            borderWidth: 1,
            pointRadius: 0,
            data: this.graphDataMortgagesWeek
        };
        this.graphCountArrays.push(series1Data);
        this.graphCountArrays.push(series2Data);
        this.graphXArraysWeek = this.XLabelSub.slice(23);
        //     this.view.tabPopupGraph.datasetsArray = JSON.stringify(this.graphCountArrays);
        //     this.view.tabPopupGraph.xlabelsArray = JSON.stringify(this.graphXArraysWeek);
        //     this.view.tabPopupGraph.updateGraph();
        this.view.cusToggleBtn.value = "Inactive";
        this.view.lblSearchPageNo.text = "";
        this.dormantView();
    },
    GraphPopupClose: function() {
        this.view.flxGraphwrapper.left = "-200%";
    },
    getLoanPortfolio: function() {
        let TapiDate = new Date(transactDate);
        var dd1 = TapiDate.getDate();
        var mm1 = (TapiDate.getMonth()) + 1;
        var yy1 = TapiDate.getFullYear();
        var CurrentDate = yy1 + "" + ("0" + mm1).slice(-2) + "" + ("0" + dd1).slice(-2);
        let now = new Date(transactDate);
        let last30days = new Date(now.setDate(now.getDate() - 29));
        var dd = now.getDate();
        var mm = (now.getMonth()) + 1;
        var yy = now.getFullYear();
        var BeforeDays = yy + "" + ("0" + mm).slice(-2) + "" + ("0" + dd).slice(-2);
        var serviceName = "LendingNew";
        var operationName = "getNewLoansGraphData";
        var headers = {};
        var inputParams = {
            "fromDate": BeforeDays,
            "toDate": CurrentDate
        };
        //headers.credentials=this.rolename;
        //headers.credentials="LENDINGOFFICER/123456";
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successLoanPortfolio, this.failureLoanPortfolio);
    },
    successLoanPortfolio: function(response) {
        this.graphCountArrays = [];
        this.graphXArraysWeek = [];
        this.graphXArraysHalfMonth = [];
        this.graphXArraysMonth = [];
        this.GraphFunction(response);
        this.PersonalWeek1Count = 0;
        this.MortgageWeek1Count = 0;
        for (i = 0; i < this.graphDataPersonalWeek.length; i++) {
            this.PersonalWeek1Count += this.graphDataPersonalWeek[i];
            this.MortgageWeek1Count += this.graphDataMortgagesWeek[i];
        }
        this.SumLoan = (this.PersonalWeek1Count) + (this.MortgageWeek1Count);
        this.view.lblLoanCount.text = this.SumLoan.toString();
        this.view.lblinWeek.text = "In Past 7 Days";
        var series1Data = {
            label: "Personal Loans",
            backgroundColor: "rgba(40, 2, 207, 0.1)",
            pointBackgroundColor: "#FF9800",
            pointBorderColor: "#ffffff",
            data: this.graphDataPersonalWeek
        };
        var series2Data = {
            label: "Mortgage Loans",
            backgroundColor: "rgba(96, 82, 201, 0.1)",
            pointBackgroundColor: "#2C8631",
            pointBorderColor: "#ffffff",
            data: this.graphDataMortgagesWeek
        };
        this.graphCountArrays.push(series1Data);
        this.graphCountArrays.push(series2Data);
        this.view.barGraph1.isVisible = true;
        // this.view.barGraph1.datasetsArray = JSON.stringify(this.graphCountArrays);
        // this.view.barGraph1.xlabelsArray = JSON.stringify(this.graphXArraysWeek);
        // this.view.barGraph1.updateGraph();
        //     this.view.tabPopupGraph.datasetsArray = JSON.stringify(this.graphCountArrays);
        //     this.view.tabPopupGraph.xlabelsArray = JSON.stringify(this.graphXArraysWeek);
        //     this.view.tabPopupGraph.updateGraph();
    },
    failureLoanPortfolio: function(error) {
        this.view.ProgressIndicator.isVisible = false;
        kony.print("Integration Service Failure:" + JSON.stringify(error));
    },
    GraphFunction: function(response) {
        var mortgageArray = response.mortgLoansCnt;
        var personalArray = response.persnlLoansCnt;
        var XLables = response.xlabels;
        var mortgageData = JSON.parse(mortgageArray);
        var XlabelData = JSON.parse(XLables);
        this.XLabelSub = [];
        var personalData = JSON.parse(personalArray);
        for (i = 0; i < XlabelData.length; i++) {
            this.XLabelSub.push(XlabelData[i].substring(0, 6));
        }
        this.graphXArraysWeek = this.XLabelSub.slice(23);
        this.graphXArraysHalfMonth = this.XLabelSub.slice(15);
        this.graphXArraysMonth = this.XLabelSub;
        this.graphDataPersonalWeek = personalData.slice(23);
        this.graphDataPersonalHalfMonth = personalData.slice(15);
        this.graphDataPersonalMonth = personalData;
        this.graphDataMortgagesWeek = mortgageData.slice(23);
        this.graphDataMortgagesHalfMonth = mortgageData.slice(15);
        this.graphDataMortgagesMonth = mortgageData;
    },
    DaysSelection: function() {
        var selectedDays = "";
        this.SumLoan = "";
        if (selectedDays === "7Days") {
            this.graphCountArrays = [];
            this.graphXArraysWeek = [];
            this.graphXArraysHalfMonth = [];
            this.graphXArraysMonth = [];
            this.PersonalWeek1Count = 0;
            this.MortgageWeek1Count = 0;
            for (i = 0; i < this.graphDataPersonalWeek.length; i++) {
                this.PersonalWeek1Count += this.graphDataPersonalWeek[i];
                this.MortgageWeek1Count += this.graphDataMortgagesWeek[i];
            }
            this.SumLoan = (this.PersonalWeek1Count) + (this.MortgageWeek1Count);
            this.view.lblGraphCount.text = this.SumLoan.toString();
            this.view.lblDaysCounts.text = "In the Past 7 Days";
            var series1Data = {
                label: "Personal Loans",
                backgroundColor: "rgba(88, 37, 220, 0.2)",
                pointBackgroundColor: "#5831D8",
                pointBorderColor: "#5831D8",
                borderColor: "#5831D8",
                borderWidth: 1,
                pointRadius: 0,
                data: this.graphDataPersonalWeek
            };
            var series2Data = {
                label: "Mortgage Loans",
                backgroundColor: "rgba(219, 102, 110, 0.2)",
                pointBackgroundColor: "#DB666E",
                pointBorderColor: "#DB666E",
                borderColor: "#DB666E",
                borderWidth: 1,
                pointRadius: 0,
                data: this.graphDataMortgagesWeek
            };
            this.graphCountArrays.push(series1Data);
            this.graphCountArrays.push(series2Data);
            this.graphXArraysWeek = this.XLabelSub.slice(23);
            //       this.view.tabPopupGraph.datasetsArray = JSON.stringify(this.graphCountArrays);
            //       this.view.tabPopupGraph.xlabelsArray = JSON.stringify(this.graphXArraysWeek);
            //       this.view.tabPopupGraph.updateGraph();
        }
        if (selectedDays === "15Days") {
            this.graphCountArrays = [];
            this.graphXArraysWeek = [];
            this.graphXArraysHalfMonth = [];
            this.graphXArraysMonth = [];
            var PersonalHalfMonthCount = 0;
            var MortgageHalfMonthCount = 0;
            for (i = 0; i < this.graphDataPersonalHalfMonth.length; i++) {
                PersonalHalfMonthCount += this.graphDataPersonalHalfMonth[i];
                MortgageHalfMonthCount += this.graphDataMortgagesHalfMonth[i];
            }
            this.SumLoan = PersonalHalfMonthCount + MortgageHalfMonthCount;
            this.view.lblGraphCount.text = this.SumLoan.toString();
            this.view.lblDaysCounts.text = "In the Past 15 Days";
            var series1Data1 = {
                label: "Personal Loans",
                backgroundColor: "rgba(88, 37, 220, 0.2)",
                pointBackgroundColor: "#5831D8",
                pointBorderColor: "#5831D8",
                borderColor: "#5831D8",
                borderWidth: 1,
                pointRadius: 0,
                data: this.graphDataPersonalHalfMonth
            };
            var series2Data1 = {
                label: "Mortgage Loans",
                backgroundColor: "rgba(219, 102, 110, 0.2)",
                pointBackgroundColor: "#DB666E",
                pointBorderColor: "#DB666E",
                borderColor: "#DB666E",
                borderWidth: 1,
                pointRadius: 0,
                data: this.graphDataMortgagesHalfMonth
            };
            this.graphCountArrays.push(series1Data1);
            this.graphCountArrays.push(series2Data1);
            this.graphXArraysHalfMonth = this.XLabelSub.slice(15);
            //       this.view.tabPopupGraph.datasetsArray = JSON.stringify(this.graphCountArrays);
            //       this.view.tabPopupGraph.xlabelsArray = JSON.stringify(this.graphXArraysHalfMonth);
            //       this.view.tabPopupGraph.updateGraph();     
        }
        if (selectedDays === "1Month") {
            this.graphCountArrays = [];
            this.graphXArraysWeek = [];
            this.graphXArraysHalfMonth = [];
            this.graphXArraysMonth = [];
            var PersonalMonthCount = 0;
            var MortgageMonthCount = 0;
            for (i = 0; i < this.graphDataPersonalMonth.length; i++) {
                PersonalMonthCount += this.graphDataPersonalMonth[i];
                MortgageMonthCount += this.graphDataMortgagesMonth[i];
            }
            this.SumLoan = PersonalMonthCount + MortgageMonthCount;
            this.view.lblGraphCount.text = this.SumLoan.toString();
            this.view.lblDaysCounts.text = "In the Past 1 Month";
            var series1Data2 = {
                label: "Personal Loans",
                backgroundColor: "rgba(88, 37, 220, 0.2)",
                pointBackgroundColor: "#5831D8",
                pointBorderColor: "#5831D8",
                borderColor: "#5831D8",
                borderWidth: 1,
                pointRadius: 0,
                data: this.graphDataPersonalMonth
            };
            var series2Data2 = {
                label: "Mortgage Loans",
                backgroundColor: "rgba(219, 102, 110, 0.2)",
                pointBackgroundColor: "#DB666E",
                pointBorderColor: "#DB666E",
                borderColor: "#DB666E",
                borderWidth: 1,
                pointRadius: 0,
                data: this.graphDataMortgagesMonth
            };
            this.graphCountArrays.push(series1Data2);
            this.graphCountArrays.push(series2Data2);
            this.graphXArraysMonth = this.XLabelSub;
            //       this.view.tabPopupGraph.datasetsArray = JSON.stringify(this.graphCountArrays);
            //       this.view.tabPopupGraph.xlabelsArray = JSON.stringify(this.graphXArraysMonth);
            //       this.view.tabPopupGraph.updateGraph();
        }
    },
    getBranchCustomers: function() {
        var serviceName = "LendingNew";
        var operationName = "getBranchCustomer";
        var headers = "";
        var inputParams = {};
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBBranchCust, this.failureCBBranchCust);
    },
    successCBBranchCust: function(response) {
        var dataMap = {
            "lblCustomerID": "lblCustomerID",
            "lblFirstName": "lblFirstName",
            "lblRm": "lblRm",
            "btnLogout": "btnLogout"
        };
        this.view.segBranchCustomer.widgetDataMap = dataMap;
        var res = response.body;
        Jsondata = [];
        for (var i = 0; i < res.length; i++) {
            Jsondata.push({
                "lblCustomerID": res[i].customerId,
                "lblFirstName": res[i].date,
                "lblRm": (res[i].checkInTime !== undefined) ? res[i].checkInTime : "00.00",
                "btnLogout": {
                    "text": "i",
                    onClick: this.checkoutFunc
                }
            });
        }
        // this._fullActivityResponse = response;
        //this.setCRcontactLogResToSeg(response);
        this._custbranchdata = Jsondata;
        this.customerBranchPage("");
        // this.view.segBranchCustomer.setData(Jsondata);
    },
    failureCBBranchCust: function(error) {
        kony.print("Integration Service Failure:" + JSON.stringify(error));
    },
    getNavDetails: function() {
        var serviceName = "LendingNew";
        var operationName = "getClientActivites";
        var headers = "";
        var inputParams = {};
        inputParams.userSignOnName = this.roleid;
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBClient, this.failureCBClient);
    },
    successCBClient: function(response) {
        this._fullActivityResponse = response;
        this.setCRcontactLogResToSeg(response);
    },
    failureCBClient: function(error) {
        this.view.SegA.setData([]);
        kony.print("Integration Service Failure:" + JSON.stringify(error));
    },
    setCRcontactLogResToSeg: function(response) {
        this.cntALL = response.cntALL;
        this.cntCompleted = response.cntCompleted;
        this.cntPending = response.cntPending;
        this.cntNew = response.cntNew;
        this.cntCancelled = response.cntCancelled;
        this.view.lblall.text = this.cntALL;
        this.view.lblCompletedcnt.text = this.cntCompleted;
        this.view.lblPendingCnt.text = this.cntPending;
        this.view.lblNewCnt.text = this.cntNew;
        this.view.lblCanelCnt.text = this.cntCancelled;
        var dataMap = {
            "lblGroup": "lblGroup",
            "lblActivity": "lblActivity",
            "lblCid": "lblCid",
            "lblDate": "lblDate",
            "lblInformed": "lblInformed"
        };
        this.view.SegA.widgetDataMap = dataMap;
        if (this.cntALL !== "0") {
            allData = this.arraygen(response, "body");
        } else {
            allData = [];
        }
        if (this.cntCompleted !== "0") {
            CompData = this.arraygen(response, "Completed");
        } else {
            CompData = [];
        }
        if (this.cntNew !== "0") {
            NewData = this.arraygen(response, "New");
        } else {
            NewData = [];
        }
        if (this.cntPending !== "0") {
            PendData = this.arraygen(response, "Pending");
        } else {
            PendData = [];
        }
        if (this.cntCancelled !== "0") {
            CanData = this.arraygen(response, "Cancelled");
        } else {
            CanData = [];
        }
        //  this.view.SegA.setData(allData);
        this._scheduledRevisedPayment = PendData;
        if (this.cntPending !== "0") {
            this.view.lblNoRecordStatus.isVisible = false;
            this._scheduledRevisedPayment = PendData;
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = true;
        } else {
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = false;
            this.view.flxPagination.isVisible = false;
            this.view.lblNoRecordStatus.isVisible = true;
        }
        this._crAcvitySegData = PendData;
        this.pendingdata();
        this.getActivities();
    },
    arraygen: function(response, typeval) {
        var res = response[typeval];
        var Jsondata = [];
        for (var i = 0; i < res.length; i++) {
            var resdate = res[i].date;
            //resdate=resdate.substring(3,6)+" "+resdate.substring(0,2);   
            var data = {
                "lblGroup": res[i].contactName,
                "lblActivity": res[i].description.length > 50 ? (res[i].description.substring(0, 50)) + ".." : res[i].description,
                "lblCid": res[i].customerId,
                "lblDate": resdate,
                "lblInformed": this.getColor(res[i].contactStatus),
                //Additional Details specially Taken for COntract Log Activity
                "channel": res[i].channel,
                "contactId": res[i].contactId,
                "contactName": res[i].contactName,
                "contactStatus": res[i].contactStatus,
                "contactType": res[i].contactType,
                "customerId": res[i].customerId,
                "date": res[i].date,
                "description": res[i].description,
                "notes": res[i].notes,
                "lastContactTime": res[i].lastContactTime,
                "staffs": res[i].staffs[0].userSignOnName,
                "channelDescription": Application.validation.isNullUndefinedObj(res[i].channelDescription),
                "fullDescription": Application.validation.isNullUndefinedObj(res[i].fullDescription)
            };
            Jsondata.push(data);
        }
        return Jsondata;
    },
    newdata: function() {
        this.view.btnNew.skin = "skn12semebold";
        this.view.btnAll.skin = "sknTabUnselected";
        this.view.btnPending.skin = "sknTabUnselected";
        this.view.btnCompleted.skin = "sknTabUnselected";
        this.view.btnCancel.skin = "sknTabUnselected";
        if (this.cntNew !== "0") {
            this._scheduledRevisedPayment = NewData;
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = true;
            this.view.lblNoRecordStatus.isVisible = false;
            this._crAcvitySegData = NewData;
        } else {
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = false;
            this.view.flxPagination.isVisible = false;
            this.view.lblNoRecordStatus.isVisible = true;
        }
        // this.view.SegA.setData(NewData);
    },
    alldata: function() {
        this.view.btnPending.skin = "sknTabUnselected";
        this.view.btnNew.skin = "sknTabUnselected";
        this.view.btnCompleted.skin = "sknTabUnselected";
        this.view.btnCancel.skin = "sknTabUnselected";
        this.view.btnAll.skin = "skn12semebold";
        if (this.cntALL !== "0") {
            this.view.lblNoRecordStatus.isVisible = false;
            this._scheduledRevisedPayment = allData;
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = true;
            this._crAcvitySegData = allData;
        } else {
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = false;
            this.view.flxPagination.isVisible = false;
            this.view.lblNoRecordStatus.isVisible = true;
        }
        //this.view.SegA.setData(allData);
    },
    pendingdata: function() {
        this.view.btnAll.skin = "sknTabUnselected";
        this.view.btnNew.skin = "sknTabUnselected";
        this.view.btnCompleted.skin = "sknTabUnselected";
        this.view.btnCancel.skin = "sknTabUnselected";
        this.view.btnPending.skin = "skn12semebold";
        if (this.cntPending !== "0") {
            this._crAcvitySegData = PendData;
            this.view.lblNoRecordStatus.isVisible = false;
            this._scheduledRevisedPayment = PendData;
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = true;
        } else {
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = false;
            this.view.flxPagination.isVisible = false;
            this.view.lblNoRecordStatus.isVisible = true;
        }
        // this.view.SegA.setData(PendData);
    },
    completeddata: function() {
        this.view.btnAll.skin = "sknTabUnselected";
        this.view.btnPending.skin = "sknTabUnselected";
        this.view.btnNew.skin = "sknTabUnselected";
        this.view.btnCancel.skin = "sknTabUnselected";
        this.view.btnCompleted.skin = "skn12semebold";
        if (this.cntCompleted !== "0") {
            this._crAcvitySegData = CompData;
            this.view.lblNoRecordStatus.isVisible = false;
            this.showChangeRequestListData("");
            this._scheduledRevisedPayment = CompData;
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = true;
        } else {
            this.view.SegA.isVisible = false;
            this.view.flxPagination.isVisible = false;
            this.view.lblNoRecordStatus.isVisible = true;
        }
        //this.view.SegA.setData(CompData);
    },
    canceldata: function() {
        this.view.btnAll.skin = "sknTabUnselected";
        this.view.btnPending.skin = "sknTabUnselected";
        this.view.btnNew.skin = "sknTabUnselected";
        this.view.btnCancel.skin = "skn12semebold";
        this.view.btnCompleted.skin = "sknTabUnselected";
        if (this.cntCancelled !== "0") {
            this._crAcvitySegData = CanData;
            this.view.lblNoRecordStatus.isVisible = false;
            this._scheduledRevisedPayment = CanData;
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = true;
        } else {
            this.showChangeRequestListData("");
            this.view.SegA.isVisible = false;
            this.view.flxPagination.isVisible = false;
            this.view.lblNoRecordStatus.isVisible = true;
        }
    },
    getColor: function(status) {
        if (status === "New") {
            return {
                "text": "New",
                "skin": "sknStatusNew"
            };
        } else if (status === "Pending") {
            return {
                "text": "Pending",
                "skin": "sknStatusPending"
            };
        } else if (status === "COMPLETED") {
            return {
                "text": "Completed",
                "skin": "sknStatusComplete"
            };
        } else if (status === "CANCELLED") {
            return {
                "text": "Cancelled",
                "skin": "sknStatusCancelled"
            };
        } else {
            return {
                "text": status,
                "skin": "sknStatusAll"
            };
        }
    },
    getActivities: function() {
        kony.print("***VIVEK****From where");
        // integrationSvc.invokeOperation(operationName, " ", {},this.getDonutSuccessCB , this.getDonutFailCB, options);
        this.getDonutSuccessCB();
        //TODO -DV - no need to wait for accounts to be loaded to dismiss dashboard
        // progress indicator. It is a silent process.
        //Move the get accounts to global JS file not this controller.
        /*
        if(gblAllaccountWidgetDate !=="" && gblAllaccountarray !=="" ){
          this.view.ProgressIndicator.isVisible = false;
        }
        */
    },
    getDonutSuccessCB: function() {
        //var total =  response.body.length;
        var total = parseInt(this.cntALL);
        var completedCount = parseInt(this.cntCompleted);
        var pendingCount = parseInt(this.cntPending);
        var newCount = parseInt(this.cntNew);
        var cancelCount = parseInt(this.cntCancelled);
        var completedPercent = Math.round((completedCount / total) * 100);
        this.completedPercent = completedPercent;
        var pendingPercent = Math.round((pendingCount / total) * 100);
        var newPercent = Math.round((newCount / total) * 100);
        var cancelPercent = Math.round((cancelCount / total) * 100);
        var DonutArray = [];
        if (completedPercent !== 0) {
            DonutArray.push({
                "colorCode": "#58cc5e",
                "label": "Completed",
                "value": completedPercent
            });
        }
        if (pendingPercent !== 0) {
            DonutArray.push({
                "colorCode": "#fbbe5b",
                "label": "Pending",
                "value": pendingPercent
            });
        }
        if (newPercent !== 0) {
            DonutArray.push({
                "colorCode": "#2E00D2",
                "label": "New",
                "value": newPercent
            });
        }
        if (cancelPercent !== 0) {
            DonutArray.push({
                "colorCode": "#ff6563",
                "label": "Cancelled",
                "value": cancelPercent
            });
        }
        var resData = {
            "data": DonutArray
        };
        var DonutChart = new com.konymp.donutchart({
            "clipBounds": true,
            "id": "DonutChart1",
            "height": "120%",
            "width": "120%",
            "top": "-10px",
            "left": "-10px",
            "isVisible": true,
            "zIndex": 1
        }, {}, {});
        DonutChart.bgColor = "#FFFFFF";
        DonutChart.enableStaticPreview = true;
        DonutChart.chartData = resData;
        DonutChart.enableLegend = true;
        this.view.flxDonut.removeAll();
        this.view.flxDonut.add(DonutChart);
        this.view.lblPercent.text = this.completedPercent + "%";
        this.view.ProgressIndicator.isVisible = false;
    },
    navigatefunctionToCustomerAccountList: function() {
        var selectedItemData = this.view.SegA.selectedRowItems;
        this.gblCustomerID = this.view.SegA.selectedRowItems[0].lblCid;
        kony.print("selectedItemData:::" + JSON.stringify(selectedItemData));
        var ntpl = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navigationObject = {};
        navigationObject.customerId = this.gblCustomerID;
        navigationObject.isShowLoans = "true";
        navigationObject.contractLog = selectedItemData;
        ntpl.navigate(navigationObject);
    },
    navigatefunctionCustomer360: function() {
        this.gblCustomerID = this.view.segLoan7Days.selectedRowItems[0].lblLoanCustID;
        var ntpl = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navigationObject = {};
        navigationObject.customerId = this.gblCustomerID;
        navigationObject.showMessageInfo = false;
        navigationObject.isShowDeposits = "true";
        ntpl.navigate(navigationObject);
        gblCutacctWidgetDate = "";
        gblCutacctArray = "";
    },
    navigatefunctionCustomer360View: function() {
        //     this.gblCustomerID=this.view.segLoan7Days.selectedRowItems[0].lblLoanCustID;
        var ntpl = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navigationObject = {};
        navigationObject.customerId = "500003";
        //     navigationObject.showMessageInfo = false;
        //     navigationObject.isShowDeposits = "true";
        ntpl.navigate(navigationObject);
        gblCutacctWidgetDate = "";
        gblCutacctArray = "";
    },
    navigateCustomerCreationfunction: function() {
        var ntpl = new kony.mvc.Navigation("frmCustomerCreation");
        var navigationObject = {};
        navigationObject.customerId = this.gblCustomerID;
        ntpl.navigate(navigationObject);
    },
    moveToCustomerAccountDetails: function() {
        var ntpl = new kony.mvc.Navigation("frmCustomerAcctDetails");
        var navigationObject = {};
        navigationObject.customerId = this.gblCustomerID;
        ntpl.navigate(navigationObject);
    },
    closeSuccessPopup: function() {
        this.view.flxMessageInfo.isVisible = false;
        //this.view.flxWrapper.flxheader.isVisible=true;
    },
    doCRSearchWithinSeg: function() {
        //TODO : change the response to add a temporary field concat with all significant claim field values to another variable for searching
        // Above will fix -e.g. search for "skin"...is giving back all records since sking a kony specific key 
        //only after 3 chars fire the filter
        var stringForSearch = this.view.txtPartialText.text.toLowerCase();
        if (stringForSearch.length === 0) {
            //Reset to show the full claims search results global variable
            // disable the clear search x mark, if any
            this.view.SegA.removeAll();
            //this.view.SegA.setData(this._crAcvitySegData);
            this._scheduledRevisedPayment = this._crAcvitySegData;
            this.showChangeRequestListData("");
            // this.setCRcontactLogResToSeg(this._fullActivityResponse);
        } else {
            //empty search string now
            // visible on for the clear search x mark
        }
        if (stringForSearch.length >= 1) {
            //use the master data
            var segData = this._crAcvitySegData; // make a copy not response
            var finalData = [];
            if (segData && segData == []) {
                return;
            }
            for (var i = 0; i < segData.length; i++) {
                var aRowData = segData[i];
                var aRowDataStrigified = JSON.stringify(aRowData);
                if (aRowDataStrigified.toLowerCase().indexOf(stringForSearch) >= 0) {
                    finalData.push(aRowData);
                } else {
                    //no need to push it - as does not meet criteria
                }
            }
            this.view.SegA.removeAll();
            //this.view.SegA.setData(finalData);
            this._scheduledRevisedPayment = finalData;
            this.showChangeRequestListData("");
        }
    },
    loadAllPayAccounts: function() {
        if (gblAllaccountWidgetDate !== "" && gblAllaccountarray !== "") {
            //this.view.ProgressIndicator.isVisible = false;
        } else {
            var serviceName = "LendingNew";
            var operationName = "getAllAccounts";
            var headers = "";
            var inputParams = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackAllaccount, this.failureCallBackAllaccount);
        }
        //this.view.ProgressIndicator.isVisible=true;
    },
    successCallBackAllaccount: function(response) {
        kony.print(response);
        var allAccountResponse = response.body.length;
        popupAccountArray = [];
        for (i = 0; i < allAccountResponse; i++) {
            var currentBalance = Application.numberFormater.convertForDispaly(response.body[i].availableBalance, response.body[i].currencyId);
            if (currentBalance === "NaN") {
                currentBalance = "";
            }
            var stringConcat = currentBalance.substring(0, 2);
            var mathSignDetermine = Math.sign(stringConcat);
            var avaliableAmount = {};
            if (mathSignDetermine === -1 || mathSignDetermine === 0) {
                avaliableAmount = {
                    text: "" + currentBalance,
                    skin: "sknflxFontD32E2FPX12"
                };
            } else {
                avaliableAmount = {
                    text: "" + currentBalance,
                    skin: "sknflxFont434343px12"
                };
            }
            popupAccountArray.push({
                accountid: response.body[i].accountId,
                customerid: response.body[i].customerId,
                currency: response.body[i].currencyId,
                product: response.body[i].productName,
                lblWorkingBal: avaliableAmount
            });
        }
        // this.view.payOutPopup.rules =popupAccountArray;
        //self.view.popupPayinPayout.segPayinPayout.widgetDataMap = dataMap;
        var widgetData = {
            "lblCustomerId": "customerid",
            "lblAccountId": "accountid",
            "lblCurrency": "currency",
            "lblWorkingBal": "lblWorkingBal",
            "lblProductId": "product"
        };
        gblAllaccountWidgetDate = widgetData;
        gblAllaccountarray = popupAccountArray;
        kony.print(JSON.stringify(gblAllaccountWidgetDate));
        kony.print(gblAllaccountarray);
        this.view.ProgressIndicator.isVisible = false;
    },
    failureCallBackAllaccount: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        if (res.t24error && res.t24error.length !== 0) {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = res.t24error.message;
        } else {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = res.errmsg;
        }
    },
    showPaginationSegmentData: function(dataPerPage, arrayOfData, btnName) {
        kony.print("showPaginationSegmentData===>");
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.flxPagination.isVisible = true;
            } else {
                this.view.flxPagination.isVisible = false;
            }
            if (btnName === "btnPrev" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName === "btnNext" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;            
            } else if (this.gblSegPageCount === 1 && this.totalpage > 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;           
            } else if (this.gblSegPageCount === 1 && this.totalpage === 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.lblSearchPage.text = "";
            } else if (this.gblSegPageCount === this.totalpage && this.totalpage > 1) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnDisable";
                //this.view.lblSearchPage.isVisible = true;            
            } else {
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.lblSearchPage.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in next previous method " + err);
        }
    },
    showChangeRequestListData: function(buttonevent) {
        kony.print("showChangeRequestListData===>");
        //showloader();
        if (this._scheduledRevisedPayment.length > 0) {
            var segmentData = this.showPaginationSegmentData(this.dataPerPageNumber, this._scheduledRevisedPayment, buttonevent.id);
            this.view.SegA.setData(segmentData);
            // this.view.flxPaymentSchedule.isVisible = true;
            kony.print("segDataSet ====>>" + JSON.stringify(segmentData));
            this.view.forceLayout();
        }
        //dismissLoader();
    },
    customerBranchPage: function(buttonevent) {
        kony.print("this._custbranchdata===>");
        //showloader();
        if (this._custbranchdata.length > 0) {
            var segmentData = this.showPaginationSegmentData1(this.branchPerPage, this._custbranchdata, buttonevent.id);
            this.view.segBranchCustomer.setData(segmentData);
            // this.view.flxPaymentSchedule.isVisible = true;
            kony.print("segDataSet ====>>" + JSON.stringify(segmentData));
            this.view.forceLayout();
        }
        //dismissLoader();
    },
    showPaginationSegmentData1: function(dataPerPage, arrayOfData, btnName) {
        kony.print("showPaginationSegmentData===>");
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.flxPaginationBranch.isVisible = true;
            } else {
                this.view.flxPaginationBranch.isVisible = false;
            }
            if (btnName === "btnPrevBranch" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName === "btnNextBranch" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.lblSearchPageBranch.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.btnPrevBranch.setEnabled(true);
                this.view.btnNextBranch.setEnabled(true);
                this.view.btnPrevBranch.skin = "sknBtnFocus";
                this.view.btnNextBranch.skin = "sknBtnFocus";
                //this.view.lblSearchPageBranch.isVisible = true;            
            } else if (this.gblSegPageCount === 1 && this.totalpage > 1) {
                this.view.btnPrevBranch.setEnabled(false);
                this.view.btnNextBranch.setEnabled(true);
                this.view.btnPrevBranch.skin = "sknBtnDisable";
                this.view.btnNextBranch.skin = "sknBtnFocus";
                //this.view.lblSearchPageBranch.isVisible = true;           
            } else if (this.gblSegPageCount === 1 && this.totalpage === 1) {
                this.view.btnPrevBranch.setEnabled(false);
                this.view.btnNextBranch.setEnabled(false);
                this.view.btnPrevBranch.skin = "sknBtnDisable";
                this.view.btnNextBranch.skin = "sknBtnDisable";
                this.view.lblSearchPageBranch.text = "";
            } else if (this.gblSegPageCount === this.totalpage && this.totalpage > 1) {
                this.view.btnPrevBranch.setEnabled(true);
                this.view.btnNextBranch.setEnabled(false);
                this.view.btnPrevBranch.skin = "sknBtnFocus";
                this.view.btnNextBranch.skin = "sknBtnDisable";
                //this.view.lblSearchPageBranch.isVisible = true;            
            } else {
                this.view.btnPrevBranch.skin = "sknBtnDisable";
                this.view.btnNextBranch.skin = "sknBtnDisable";
                this.view.btnPrevBranch.setEnabled(false);
                this.view.btnNextBranch.setEnabled(false);
                this.view.lblSearchPageBranch.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in HolidayPaymentController next previous method :::" + err);
        }
    },
    LoanDetailsPage: function(buttonevent) {
        kony.print("this._LoanDetailsdata===>");
        //showloader();
        if (this._LoanDetailsdata.length > 0) {
            var segmentData = this.showPaginationSegmentData2(this.LoanPerPage, this._LoanDetailsdata, buttonevent.id);
            this.view.segLoan7Days.setData(segmentData);
            // this.view.flxPaymentSchedule.isVisible = true;
            kony.print("segLoan7Days ====>>" + JSON.stringify(segmentData));
            this.view.forceLayout();
        }
        //dismissLoader();
    },
    showPaginationSegmentData2: function(dataPerPage, arrayOfData, btnName) {
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.flxPaginationLoan.isVisible = true;
            } else {
                this.view.flxPaginationLoan.isVisible = false;
            }
            if (btnName === "btnPrevLoan" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName === "btnNextLoan" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.lblSearchPageLoan.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.btnPrevLoan.setEnabled(true);
                this.view.btnNextLoan.setEnabled(true);
                this.view.btnPrevLoan.skin = "sknBtnFocus";
                this.view.btnNextLoan.skin = "sknBtnFocus";
                //this.view.lblSearchPageLoan.isVisible = true;            
            } else if (this.gblSegPageCount === 1 && this.totalpage > 1) {
                this.view.btnPrevLoan.setEnabled(false);
                this.view.btnNextLoan.setEnabled(true);
                this.view.btnPrevLoan.skin = "sknBtnDisable";
                this.view.btnNextLoan.skin = "sknBtnFocus";
                //this.view.lblSearchPageLoan.isVisible = true;           
            } else if (this.gblSegPageCount === 1 && this.totalpage === 1) {
                this.view.btnPrevLoan.setEnabled(false);
                this.view.btnNextLoan.setEnabled(false);
                this.view.btnPrevLoan.skin = "sknBtnDisable";
                this.view.btnNextLoan.skin = "sknBtnDisable";
                this.view.lblSearchPageLoan.text = "";
            } else if (this.gblSegPageCount === this.totalpage && this.totalpage > 1) {
                this.view.btnPrevLoan.setEnabled(true);
                this.view.btnNextLoan.setEnabled(false);
                this.view.btnPrevLoan.skin = "sknBtnFocus";
                this.view.btnNextLoan.skin = "sknBtnDisable";
                //this.view.lblSearchPageLoan.isVisible = true;            
            } else {
                this.view.btnPrevLoan.skin = "sknBtnDisable";
                this.view.btnNextLoan.skin = "sknBtnDisable";
                this.view.btnPrevLoan.setEnabled(false);
                this.view.btnNextLoan.setEnabled(false);
                this.view.lblSearchPageLoan.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in HolidayPaymentController next previous method :::" + err);
        }
    },
    checkoutFunc1: function(widgetRefTxtBx, segmentIndices) {
        var selectData1 = this.view.segBranchCustomer.data[segmentIndices.rowIndex];
        var dateBranchCust = selectData1.lblFirstName;
        var newdates = new Date(dateBranchCust);
        var monthNames = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        var dd = newdates.getDate();
        var mm = monthNames[newdates.getMonth()];
        var yy = newdates.getFullYear();
        var BranchDate = yy + "-" + mm + "-" + dd;
        this.view.cusCustomerId.disabled = true;
        this.view.cusLocation.disabled = true;
        this.view.cusContactDate.disabled = true;
        this.view.cusTimeIn.disabled = true;
        this.view.cusCustomerId.value = selectData1.lblCustomerID;
        this.view.cusLocation.value = "BSG Company";
        this.view.cusContactDate.value = BranchDate;
        this.view.cusTimeIn.value = selectData1.lblRm;
        this.view.flxCustomerBranchBg.left = "0%";
    },
    onBranchCancelClick: function() {
        this.view.flxCustomerBranchBg.left = "-200%";
        this.view.cusCustomerId.value = "";
        this.view.cusLocation.value = "";
        this.view.cusContactDate.value = "";
        this.view.cusTimeIn.value = "";
        this.view.cusTextAreaRNotes.value = "";
        this.view.cusTextTimeOut.value = "";
        this.view.cusSelectMode.value = "None";
    },
    onBranchSubmitClick: function() {
        var brcustomerID = this.view.cusCustomerId.value;
        var brcustomerLocation = this.view.cusLocation.value;
        var brcustomerDate = this.view.cusContactDate.value;
        var brcustomerTimeIn = this.view.cusTimeIn.value;
        var brcustomerNotes = this.view.cusTextAreaRNotes.value;
        var brcustomerTimeOut = this.view.cusTextTimeOut.value;
        var brcustomerMode = this.view.cusSelectMode.value;
        if (brcustomerTimeOut === "" || brcustomerNotes === "" || brcustomerMode === "None") {
            this.view.lblErrorMsg.text = "* All mandatory fields need to be filled";
        } else {
            this.view.lblErrorMsg.text = "";
        }
        kony.print(brcustomerID + brcustomerLocation + brcustomerDate + brcustomerTimeIn + brcustomerNotes + brcustomerTimeOut + brcustomerMode);
    },
    getStarted: function() {
        if (this.view.getStarted.lblHi.text === "Hi There") {
            this.view.getStarted.lblHi.text = "Explore";
            this.view.getStarted.lbl2.text = "Find what you are looking for or simply browse through our services";
            this.view.getStarted.btnStarted.text = "Next";
        } else if (this.view.lblSlide.text === "Explore") {
            this.view.getStarted.lblHi.text = "Help Centre";
            this.view.getStarted.lbl2.text = "Use it for step by step guides if you are struck somewhere";
            this.view.getStarted.btnStarted.text = "Get Started";
            //       kony.store.setItem("demoFlag", false);
            //       var myValue = kony.store.getItem("demoFlag");
            //       console.log("demoFlag -> "+ myValue);
        } else {
            return;
        }
    },
    helpCenter: function(context) {
        switch (context) {
            case "Disburse":
                {
                    this.view.helpCenter.flx1.isVisible = false;
                    this.view.helpCenter.flx2.isVisible = true;
                    this.view.helpCenter.lblSelected.text = "Disbursement";
                    this.view.helpCenter.lblDesc.text = "After a decision has been made on a loan request, Temenos retail lending allows you to disburse loan amounts in full or partial payments";
                    this.view.helpCenter.lblThumbnailDesc.text = "Check out the video on how to disburse?";
                    break;
                }
            case "Increase Commitment":
                {
                    this.view.helpCenter.flx1.isVisible = false;
                    this.view.helpCenter.flx2.isVisible = true;
                    this.view.helpCenter.lblSelected.text = "Increase Commitment";
                    this.view.helpCenter.lblDesc.text = "";
                    this.view.helpCenter.lblThumbnailDesc.text = "";
                    break;
                }
            case "Payment Holiday":
                {
                    this.view.helpCenter.flx1.isVisible = false;
                    this.view.helpCenter.flx2.isVisible = true;
                    this.view.helpCenter.lblSelected.text = "Payment Holiday";
                    this.view.helpCenter.lblDesc.text = "";
                    this.view.helpCenter.lblThumbnailDesc.text = "";
                    break;
                }
            case "Repayment":
                {
                    this.view.helpCenter.flx1.isVisible = false;
                    this.view.helpCenter.flx2.isVisible = true;
                    this.view.helpCenter.lblSelected.text = "Repayment";
                    this.view.helpCenter.lblDesc.text = "";
                    this.view.helpCenter.lblThumbnailDesc.text = "";
                    break;
                }
            case "Change Interest":
                {
                    this.view.helpCenter.flx1.isVisible = false;
                    this.view.helpCenter.flx2.isVisible = true;
                    this.view.helpCenter.lblSelected.text = "Change Interest";
                    this.view.helpCenter.lblDesc.text = "";
                    this.view.helpCenter.lblThumbnailDesc.text = "";
                    break;
                }
            case "Payoff":
                {
                    this.view.helpCenter.flx1.isVisible = false;
                    this.view.helpCenter.flx2.isVisible = true;
                    this.view.helpCenter.lblSelected.text = "Payoff";
                    this.view.helpCenter.lblDesc.text = "";
                    this.view.helpCenter.lblThumbnailDesc.text = "";
                    break;
                }
            default:
                {
                    alert('Empty action received.');
                    break;
                }
        }
    },
    showMore: function() {
        //  if(this.gblType === "Change Interest"){
        // this.view.flxSeg.isVisible = true;
        this.view.help.flxSeg.isVisible = true;
        this.view.help.flxBck.isVisible = true;
        this.view.help.segSearch.isVisible = false;
        this.view.help.lblResult.isVisible = false;
        this.view.help.showMore.isVisible = true;
        //         changeIntrest.forEach((txt)=>{
        //           txt[0].imgLocation = "blue_downarrow.png";   
        //           txt[0].template = "flxShowMoreHeader"; 
        //         });
        //         changeIntrest.forEach(([,txt])=>{
        //           txt.forEach((lbl)=>{
        //             lbl.flxRow.height = "80dp";
        //             lbl.template = "flxShowMore";
        //           });
        //         });
        //this.view.segSearch.seg1.setData(changeIntrest);
        console.log("showMoreData1 " + showMoreData1);
        this.view.help.showMore.segShowMore.setData(showMoreData1);
        //       }else if(this.gblType === "Payoff"){
        //         this.view.flxSeg.isVisible = true;
        //         this.view.flxBck.isVisible = true;
        //         this.view.showMore.isVisible = true;
        //         this.view.segSearch.isVisible = false;
        //         this.view.lblResult.isVisible = false;
        // //         changeIntrest1.forEach((txt)=>{
        // //           txt[0].imgLocation = "blue_downarrow.png";   
        // //           txt[0].template = "flxShowMoreHeader";
        // //         });
        // //         changeIntrest1.forEach(([,txt])=>{
        // //           txt.forEach((lbl)=>{
        // //             lbl.flxRow.height = "80dp";
        // //             lbl.template = "flxShowMore";
        // //           });
        // //         });
        //         //this.view.segSearch.seg1.setData(changeIntrest1);
        //         this.view.showMore.segShowMore.setData(showMoreData1);
        //       }else{
        //         return;
        //       }
    },
    //   globalVariables: function(){
    //       rowHeight = 120;
    //       this.loadAddress();
    //       mData = this.view.help.segSearch.seg1.data;
    //       this.view.help.flxServices.isVisible = true;
    //       this.view.help.flxSeg.isVisible = false;
    //       this.view.help.flxBack.isVisible = false;
    //       this.view.help.flxServiceType.isVisible = false;
    //     },
    //   loadAddress:function(){
    //       this.view.help.segSearch.seg1.widgetDataMap = {lblAddress:"lblAddress",
    //                                                 lblShelves:"lblShelves",
    //                                                 lblTitle:"lblTitle",
    //                                                 imgLocation:"imgLocation",
    //                                                 flxDown:"flxDown",
    //                                                 flxDownK:"flxDownK",
    //                                                 flxRow:"flxRow",
    //                                                 flxParent:"flxParent",
    //                                                 flxProgress:"flxProgress",
    //                                                 flxAnimate:"flxAnimate",
    //                                                 flxLine:"flxLine"
    //                                                };
    //       this.view.help.segSearch.seg1.setData(changeIntrest);
    //     },
    //     onClick: function(){   
    //       var section = this.view.help.segSearch.seg1.selectedIndices[0];
    //       // console.log("section "+JSON.stringify(section));
    //       if(this.prev_index !==-1){
    //         var data = this.view.help.segSearch.seg1.data[this.prev_index];
    //         var count = data[1].length;
    //         var rowsToAnim = [];
    //         for(i=0;i<count;i++){
    //           rowsToAnim.push({rowIndex:i,sectionIndex:this.prev_index});
    //         }
    //         data[0].imgLocation = "blue_downarrow.png";
    //         this.view.help.segSearch.seg1.setSectionAt(data, this.prev_index);
    //         var animationDef1 = {    
    //           "100": {
    //             "stepConfig": {
    //               "timingFunction": kony.anim.EASE
    //             },
    //             "height":"0dp"
    //           }
    //         };
    //         var animationConfig1 = {
    //           "delay": 0,
    //           "iterationCount": 1,
    //           "fillMode": kony.anim.FILL_MODE_FORWARDS,
    //           "duration": 0.1
    //         };
    //         var animationDefObject1 = kony.ui.createAnimation(animationDef1);
    //         this.view.help.segSearch.seg1.animateRows({     
    //           rows:rowsToAnim,
    //           widgets: ["flxRow"],
    //           animation: {       
    //             definition: animationDefObject1,
    //             config: animationConfig1     
    //           }   
    //         });
    //         console.log("rowsToAnim "+JSON.stringify(rowsToAnim));
    //         console.log("this.prev_index "+this.prev_index);
    //       }
    //       if(section == this.prev_index){
    //         this.prev_index = -1;
    //         return;    
    //       }
    //       var data1 = this.view.help.segSearch.seg1.data[section];
    //       const search = "Change OD Limit";
    //       data1[1].forEach(value=>{
    //         if(value.lblAddress.text === search){
    //           value.lblAddress.skin = "sknSelectedItem";
    //         }
    //       });
    //       // console.log(JSON.stringify(data1[1]));
    //       data1[0].imgLocation = "blue_uparrow.png";
    //       this.view.help.segSearch.seg1.setSectionAt(data1, section);
    //       var count1 = data1[1].length;
    //       var rowsToAnim1 = [];
    //       for(i=0;i<count1;i++){
    //         rowsToAnim1.push({rowIndex:i,sectionIndex:section});
    //       }
    //       var animationDef = {    
    //         "100": {
    //           "stepConfig": {
    //             "timingFunction": kony.anim.EASE
    //           },
    //           "height": rowHeight+"dp" //kony.flex.USE_PREFERED_SIZE // rowHeight+"dp"
    //         }
    //       };
    //       var animationConfig = {
    //         "delay": 0,
    //         "iterationCount": 1,
    //         "fillMode": kony.anim.FILL_MODE_FORWARDS,
    //         "duration": 0.1
    //       };
    //       var animationDefObject = kony.ui.createAnimation(animationDef);
    //       this.view.help.segSearch.seg1.animateRows({     
    //         rows: rowsToAnim1,
    //         widgets: ["flxRow"],
    //         animation: {       
    //           definition: animationDefObject,
    //           config: animationConfig     
    //         }   
    //       });
    //       this.prev_index = section;
    //       console.log("rowsToAnim1 "+JSON.stringify(rowsToAnim1));
    //     },
    //     onClickRow:function(){
    //       var row = this.view.help.segSearch.seg1.selectedRowIndex[1];
    //       var section = this.view.help.segSearch.seg1.selectedRowIndex[0];
    //       var data = this.view.help.segSearch.seg1.data[section][1][row];
    //       // this.storeData.lblAddress = data;
    //       //     var ntf = new kony.mvc.Navigation("frmMap");
    //       //     ntf.navigate(data);
    //     },
    playVideo: function(url) {
        //console.log(url);
        this.view.helpCenter.flx2.isVisible = false;
        this.view.helpCenter.flx3.isVisible = true;
        var youtubeUrl = "https://www.youtube.com/watch?v=KZ3lFblKB58&ab_channel=BadmintonHighlightsandCrazyShots";
        var browserString = "<html>\n<body>\n<iframe width=\"560\" height=\"315\" src=" + youtubeUrl + "frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen>\n</iframe>\n</body>\n</html>";
        this.view.helpCenter.brw.htmlString = browserString;
    },
    removeAmountFormat: function(amnt, currency) {
        try {
            if (amnt === "0.00" || amnt === 0.00 || amnt === "0,00") return "0.00";
            var amount = "";
            if (amnt) {
                if (amnt.charAt(amnt.length - 3) === ",") {
                    amount = amnt.replace(/\./g, ""); //remove all dots
                    amount = amount.replace(/[\s,]+/g, '.').trim(); //replace comma wih dot
                } else {
                    amount = amnt.replace(/[\s,]+/g, ""); //remove all commas
                }
            }
            kony.print("Amount after removing all formats: " + amount);
            return amount;
        } catch (err) {
            kony.print("Exception in removeAmountFormat: " + err);
        }
    },
    loadGraph: function() {
        try {
            var serviceName = "DashbordLiability";
            var operationName = "getDormantAccount";
            var headers = {
                "companyId": "NL0020001"
            };
            var inputParams = {};
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackLoadGraph, this.failureCallBackLoadGraph);
        } catch (err) {
            kony.print("Error in loadGraph::::" + err);
        }
    },
    successCallBackLoadGraph: function(data) {
        try {
            var response = data.body;
            if (data.header.status === "success") {
                if (response.length > 0) {
                    var mydatasets = [];
                    var datasets = [];
                    var myxlabels = response.map(function(value) {
                        return value.description;
                    });
                    myxlabels = myxlabels.filter((v, i, a) => a.indexOf(v) === i);
                    for (let i = 0; i < myxlabels.length; i++) {
                        var countInActive = 0;
                        var countDormant = 0;
                        for (let a = 0; a < response.length; a++) {
                            if (response[a].description === myxlabels[i]) {
                                if (response[a].dormancyStatus === "Inactive") {
                                    countInActive++;
                                }
                                if (response[a].dormancyStatus === "Dormant") {
                                    countDormant++;
                                }
                            }
                        }
                        var data1 = {
                            id: myxlabels[i],
                            countInActive: countInActive,
                            countDormant: countDormant
                        };
                        mydatasets.push(data1);
                    }
                    var dataInActive = mydatasets.map(function(value) {
                        return value.countInActive;
                    });
                    var dataDormant = mydatasets.map(function(value) {
                        return value.countDormant;
                    });
                    var series1Data = {
                        label: "Inactive",
                        backgroundColor: "#805FFF",
                        barThickness: 15,
                        data: dataInActive
                    };
                    var series2Data = {
                        label: "Dormant",
                        barThickness: 15,
                        backgroundColor: "#BAA9FF",
                        data: dataDormant
                    };
                    datasets.push(series1Data);
                    datasets.push(series2Data);
                    this.view.barGraph1.isVisible = true;
                    this.view.barGraph1.datasetsArray = JSON.stringify(datasets);
                    this.view.barGraph1.xlabelsArray = JSON.stringify(myxlabels);
                    this.view.tabPopupGraph2.datasetsArray = JSON.stringify(datasets);
                    this.view.tabPopupGraph2.xlabelsArray = JSON.stringify(myxlabels);
                    this.view.barGraph1.ySuggestedMin = 0;
                    this.view.tabPopupGraph2.ySuggestedMin = 0;
                    this.view.barGraph1.updateGraph();
                    this.view.tabPopupGraph2.updateGraph();
                }
            } else {
                this.view.ErrorAllert.lblMessage.text = res.error.message; //error.errorDetails[0].message
                this.view.ErrorAllert.isVisible = true;
            }
        } catch (err) {
            kony.print("Error in SuccessCallBackLoadGraph::::" + err);
        }
    },
    failureCallBackLoadGraph: function(data) {
        try {
            kony.print("data:::::" + JSON.stringify(data));
            this.view.ErrorAllert.lblMessage.text = res.error.message; //error.errorDetails[0].message
            this.view.ErrorAllert.isVisible = true;
        } catch (err) {
            kony.print("Error in failureCallBackLoadGraph::::" + err);
        }
    },
    dormantView: function() {
        try {
            var serviceName = "DashbordLiability";
            var operationName = "getDormantAccountView";
            var headers = {
                "companyId": "NL0020001",
                "dormancyStatus": "INACTIVE"
            };
            var inputParams = {};
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackDormantView, this.failureCallBackDormantView);
        } catch (err) {
            kony.print("Error in dormantView::::" + err);
        }
    },
    successCallBackDormantView: function(res) {
        try {
            var noOfItemsPerPage = 4;
            kony.print("response:::" + JSON.stringify(res));
            this.view.ProgressIndicator.isVisible = false;
            this.view.lblSearchPageNo.text = "";
            var dormantView = [];
            var length = res.body.length;
            var viewLength = res.body.length > 4 ? 4 : res.body.length;
            var dataMap = {
                "lblAccNo": "lblAccNo",
                "lblCurrency": "lblCurrency",
                "lblBalance": "lblBalance",
                "lblLastActivityDate": "lblLastActivityDate",
                "lblDescription": "lblDescription"
            };
            this.view.segAccData.widgetDataMap = dataMap;
            Jsondata = [];
            var uniqueDescription = [];
            for (var i = 0; i < length; i++) {
                var objDormantView = {};
                objDormantView.label = res.body[i].description;
                if (uniqueDescription.indexOf(res.body[i].description) < 0) {
                    uniqueDescription.push(res.body[i].description);
                    dormantView.push(objDormantView);
                }
                var currency = res.body[i].currency;
                currency = Application.validation.isNullUndefinedObj(currency);
                var balance = res.body[i].balance;
                balance = Application.validation.isNullUndefinedObj(balance);
                if (balance !== "") {
                    balance = balance.replace(/[\s,]+/g, '');
                    balance = Application.numberFormater.convertForDispaly(this.removeMinus(balance), currency);
                    if (balance === "NaN") {
                        balance = "0.00";
                    }
                } else {
                    balance = Application.numberFormater.convertForDispaly(this.removeMinus("0"), currency);
                }
                Jsondata.push({
                    "lblAccNo": res.body[i].accountNumber,
                    "lblCurrency": currency,
                    "lblBalance": balance,
                    "lblLastActivityDate": res.body[i].lastActivityDate,
                    "lblDescription": res.body[i].description
                });
            }
            this.DetailsArray = Jsondata;
            //}
            this.view.cusSavingsAcc.options = JSON.stringify(dormantView);
            this.view.cusSavingsAcc.valueChanged = this.filterAccount;
            var searchData = [];
            if (this.DetailsArray.length > 4) {
                for (let j = 0; j < viewLength; j++) {
                    searchData.push(this.DetailsArray[j]);
                }
                this.view.flxAccPagination.setVisibility(true);
                //           this.view.btnPrev.skin = "sknBtnDisable";
                //           this.view.btnNext.skin = "sknBtnFocus";
            } else {
                if (kony.os.toNumber(this.DetailsArray.length) <= 4) {
                    searchData.push(this.DetailsArray);
                    this.view.flxAccPagination.setVisibility(false);
                    var string = JSON.stringify(searchData);
                    string = string.slice(1, string.length - 1);
                    kony.print(string);
                    searchData = JSON.parse(string);
                }
            }
            this.totalpage = parseInt((this.DetailsArray.length / noOfItemsPerPage));
            var remainingItems = parseInt(this.DetailsArray.length) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            //       this.view.lblSearchPageNo.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            this.view.lblSearchPageNo.text = "1" + "  " + "of" + "  " + this.totalpage;
            this.view.segAccData.setData(searchData);
            this.filterAccount();
        } catch (err) {
            kony.print("Error in successCallBackDormantView:::" + err);
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    failureCallBackDormantView: function(res) {
        try {
            kony.print("response:::" + JSON.stringify(res));
            this.view.ProgressIndicator.isVisible = false;
            this.view.ErrorAllert.lblMessage.text = res.error.message; //error.errorDetails[0].message
            this.view.ErrorAllert.isVisible = true;
        } catch (err) {
            kony.print("Error in failureCallBackDormantView:::" + err);
        }
    },
    showPaginationData: function(btnName) {
        try {
            kony.print("onPreviousNxtDelivery---> ");
            var noOfItemsPerPage = 4;
            var tmpData = [];
            var segVal = this.DetailsArray;
            var len = segVal.length;
            kony.print("DetailsArray--------------------> " + (this.DetailsArray.length));
            this.totalpage = parseInt(len / noOfItemsPerPage);
            //       var totalpage= parseInt((this.DetailsArray.length/ noOfItemsPerPage));
            var remainingItems = parseInt(this.DetailsArray.length) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            //       totalpage=remainingItems>0?totalpage+1:totalpage;
            if (this.totalpage > 1) {
                this.view.flxPagination.isVisible = true;
            } else {
                this.view.flxPagination.isVisible = false;
            }
            kony.print("totalpage:: " + this.totalpage);
            kony.print("remainingItems:: " + remainingItems);
            if (btnName == "btnPrevious" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName == "btnNextView" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.lblSearchPageNo.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            kony.print("gblSegPageCount::--------------->  " + this.gblSegPageCount);
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.btnPrevious.setEnabled(true);
                this.view.btnNextView.setEnabled(true);
                this.view.btnPrevious.skin = "sknBtnFocus";
                this.view.btnNextView.skin = "sknBtnFocus"; // show Next Btn
                kony.print("gblSegPageCount case 1:: " + this.gblSegPageCount);
            } else if (this.gblSegPageCount === 1 && this.totalpage > 1) {
                this.view.btnPrevious.setEnabled(false);
                this.view.btnNextView.setEnabled(true);
                this.view.btnPrevious.skin = "sknBtnDisable";
                this.view.btnNextView.skin = "sknBtnFocus"; // show Next Btn
                kony.print("gblSegPageCount case 2:: " + this.gblSegPageCount);
            } else if (this.gblSegPageCount === 1 && this.totalpage === 1) {
                this.view.btnPrevious.setEnabled(false);
                this.view.btnNextView.setEnabled(false);
                this.view.btnPrevious.skin = "sknBtnDisable";
                this.view.btnNextView.skin = "sknBtnDisable";
                this.view.lblSearchPageNo.text = "";
            } else if (this.gblSegPageCount === this.totalpage && this.totalpage > 1) {
                this.view.btnPrevious.setEnabled(true);
                this.view.btnNextView.setEnabled(false);
                this.view.btnPrevious.skin = "sknBtnFocus";
                this.view.btnNextView.skin = "sknBtnDisable";
            } else {
                this.view.btnPrevious.skin = "sknBtnDisable";
                this.view.btnNextView.skin = "sknBtnDisable";
                this.view.btnPrevious.setEnabled(false);
                this.view.btnNextView.setEnabled(false);
                this.view.lblSearchPageNo.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            this.view.segAccData.removeAll();
            this.view.segAccData.setData(tmpData);
            this.view.forceLayout();
            return tmpData;
        } catch (err) {
            kony.print("Error in next previous method " + err);
        }
    },
    onClickLiabilityTogglebtn: function() {
        try {
            var toggleBtn = this.view.cusToggleBtn.value;
            var serviceName = "DashbordLiability";
            var operationName = "getDormantAccountView";
            var headers = {
                "companyId": "NL0020001",
                "dormancyStatus": toggleBtn.toUpperCase()
            };
            var inputParams = {};
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackLiabilityDormant, this.failureCallBackLiabilityDormant);
        } catch (err) {
            kony.print("Error in onClickLiabilityTogglebtn " + err);
        }
    },
    successCallBackLiabilityDormant: function(res) {
        try {
            var noOfItemsPerPage = 4;
            this.view.lblSearchPageNo.text = "";
            kony.print("response:::" + JSON.stringify(res));
            this.view.ProgressIndicator.isVisible = false;
            var dormantView = [];
            var length = res.body.length;
            var viewLength = res.body.length > 4 ? 4 : res.body.length;
            var dataMap = {
                "lblAccNo": "lblAccNo",
                "lblCurrency": "lblCurrency",
                "lblBalance": "lblBalance",
                "lblLastActivityDate": "lblLastActivityDate",
                "lblDescription": "lblDescription"
            };
            this.view.segAccData.widgetDataMap = dataMap;
            Jsondata = [];
            var uniqueDescription = [];
            for (var i = 0; i < length; i++) {
                var objDormantView = {};
                objDormantView.label = res.body[i].description;
                if (uniqueDescription.indexOf(res.body[i].description) < 0) {
                    uniqueDescription.push(res.body[i].description);
                    dormantView.push(objDormantView);
                }
                var currency = res.body[i].currency;
                currency = Application.validation.isNullUndefinedObj(currency);
                var balance = res.body[i].balance;
                balance = Application.validation.isNullUndefinedObj(balance);
                if (balance !== "") {
                    balance = balance.replace(/[\s,]+/g, '');
                    balance = Application.numberFormater.convertForDispaly(this.removeMinus(balance), currency);
                    if (balance === "NaN") {
                        balance = "0.00";
                    }
                } else {
                    balance = Application.numberFormater.convertForDispaly(this.removeMinus("0"), currency);
                }
                Jsondata.push({
                    "lblAccNo": res.body[i].accountNumber,
                    "lblCurrency": currency,
                    "lblBalance": balance,
                    "lblLastActivityDate": res.body[i].lastActivityDate,
                    "lblDescription": res.body[i].description
                });
            }
            this.DetailsArray = Jsondata;
            //}
            this.view.cusSavingsAcc.options = JSON.stringify(dormantView);
            this.view.cusSavingsAcc.valueChanged = this.filterAccount;
            var searchData = [];
            if (this.DetailsArray.length > 4) {
                for (let j = 0; j < viewLength; j++) {
                    searchData.push(this.DetailsArray[j]);
                }
                this.view.flxAccPagination.setVisibility(true);
                //           this.view.btnPrev.skin = "sknBtnDisable";
                //           this.view.btnNext.skin = "sknBtnFocus";
            } else {
                if (kony.os.toNumber(this.DetailsArray.length) <= 4) {
                    searchData.push(this.DetailsArray);
                    this.view.flxAccPagination.setVisibility(false);
                    var string = JSON.stringify(searchData);
                    string = string.slice(1, string.length - 1);
                    kony.print(string);
                    searchData = JSON.parse(string);
                }
            }
            this.totalpage = parseInt((this.DetailsArray.length / noOfItemsPerPage));
            var remainingItems = parseInt(this.DetailsArray.length) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            this.view.lblSearchPageNo.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            this.view.segAccData.setData(searchData);
            this.filterAccount();
        } catch (err) {
            kony.print("Error in successCallBackDormantView:::" + err);
        }
    },
    failureCallBackLiabilityDormant: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            this.view.ErrorAllert.lblMessage.text = res.error.message; //error.errorDetails[0].message
            this.view.ErrorAllert.isVisible = true;
        } catch (err) {
            kony.print("Error in failureCallBackLiabilityDormant " + err);
        }
    },
    filterAccount: function() {
        try {
            var accountSearch = this.view.cusSavingsAcc.value;
            var jsonData = [];
            var noOfItemsPerPage = 4;
            var viewLength = this.DetailsArray.length > 4 ? 4 : this.DetailsArray.length;
            for (let i = 0; i < this.DetailsArray.length; i++) {
                if (kony.string.startsWith(this.DetailsArray[i].lblDescription, accountSearch, true)) {
                    jsonData.push(this.DetailsArray[i]);
                }
            }
            this.accountArray = jsonData;
            var searchData = [];
            if (this.accountArray.length > 4) {
                for (let j = 0; j < viewLength; j++) {
                    searchData.push(this.accountArray[j]);
                }
                this.view.flxAccPagination.setVisibility(true);
                //           this.view.btnPrev.skin = "sknBtnDisable";
                //           this.view.btnNext.skin = "sknBtnFocus";
            } else {
                if (kony.os.toNumber(this.accountArray.length) <= 4) {
                    searchData.push(this.accountArray);
                    this.view.flxAccPagination.setVisibility(false);
                    var string = JSON.stringify(searchData);
                    string = string.slice(1, string.length - 1);
                    kony.print(string);
                    searchData = JSON.parse(string);
                }
            }
            this.totalpage = parseInt((this.accountArray.length / noOfItemsPerPage));
            var remainingItems = parseInt(this.accountArray.length) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            this.view.lblSearchPageNo.text = "1" + "  " + "of" + "  " + this.totalpage;
            //       this.view.segAccData.setData(searchData);
            this.view.segAccData.removeAll();
            this.view.segAccData.setData(searchData);
            this.view.forceLayout();
        } catch (err) {
            kony.print("Error in Filter accounts:::" + err);
        }
    },
});
define("frmDashboardControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Segment_ad9c9c5e47f640edabeeea09fcd99501: function AS_Segment_ad9c9c5e47f640edabeeea09fcd99501(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return
    },
    AS_Button_c06315740020458b854f2190afcaf9fb: function AS_Button_c06315740020458b854f2190afcaf9fb(eventobject) {
        var self = this;
    },
    AS_TPW_c8139e225e664599b1e0387937220c04: function AS_TPW_c8139e225e664599b1e0387937220c04(eventobject) {
        var self = this;
    },
    AS_TPW_f937666cd8aa4daebf54a21779a11059: function AS_TPW_f937666cd8aa4daebf54a21779a11059(eventobject) {
        var self = this;
        return
    },
    AS_Form_e7beed71d30141fbb9e094a4952749f8: function AS_Form_e7beed71d30141fbb9e094a4952749f8(eventobject) {
        var self = this;
        var myValue = kony.store.getItem("demoFlag");
        console.log("demoFlag -> " + myValue);
        if (myValue === null) {
            this.view.getStarted.isVisible = true;
        } else {
            this.view.getStarted.isVisible = false;
        }
    },
    AS_Form_i6dc5c383ac342f7889d0b9d5fceca10: function AS_Form_i6dc5c383ac342f7889d0b9d5fceca10(eventobject) {
        var self = this;
        this.view.help.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.getStarted.isVisible = true;
    },
    AS_UWI_fb5eadf6e648460e9d3c74f1576952e8: function AS_UWI_fb5eadf6e648460e9d3c74f1576952e8(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_b9526842e3234c3e855d4309c59dd505: function AS_UWI_b9526842e3234c3e855d4309c59dd505(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmDashboardController", ["userfrmDashboardController", "frmDashboardControllerActions"], function() {
    var controller = require("userfrmDashboardController");
    var controllerActions = ["frmDashboardControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
