define("flxSearchSegments", function() {
    return function(controller) {
        var flxSearchSegments = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSearchSegments",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxSearchSegments.setDefaultUnit(kony.flex.DP);
        var flxCustomerWrap = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxCustomerWrap",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxCustomerWrap.setDefaultUnit(kony.flex.DP);
        var lblCustomerID = new kony.ui.Label({
            "height": "100%",
            "id": "lblCustomerID",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "101146",
            "top": "0",
            "width": "120dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFirstName = new kony.ui.Label({
            "height": "100%",
            "id": "lblFirstName",
            "isVisible": true,
            "left": "25dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Rolf Gerieng",
            "top": "0dp",
            "width": "120dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLastName = new kony.ui.Label({
            "height": "100%",
            "id": "lblLastName",
            "isVisible": true,
            "left": "25dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Rolf Gerieng",
            "top": "0dp",
            "width": "120dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDateOfBirth = new kony.ui.Label({
            "height": "100%",
            "id": "lblDateOfBirth",
            "isVisible": true,
            "left": "25dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "04 Oct 1975",
            "top": "0dp",
            "width": "100dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPhoneNumber = new kony.ui.Label({
            "height": "100%",
            "id": "lblPhoneNumber",
            "isVisible": true,
            "left": "25dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "+3197010281305",
            "top": "0dp",
            "width": "140dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEmail = new kony.ui.Label({
            "height": "100%",
            "id": "lblEmail",
            "isVisible": true,
            "left": "25dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "ericdl@gmail.com",
            "top": "0dp",
            "width": "180dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountOfficer = new kony.ui.Label({
            "height": "100%",
            "id": "lblAccountOfficer",
            "isVisible": true,
            "left": "25dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Patrik Owen",
            "top": "0dp",
            "width": "160dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerWrap.add(lblCustomerID, lblFirstName, lblLastName, lblDateOfBirth, lblPhoneNumber, lblEmail, lblAccountOfficer);
        flxSearchSegments.add(flxCustomerWrap);
        return flxSearchSegments;
    }
})