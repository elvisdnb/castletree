define("flxSegLoanFixed", function() {
    return function(controller) {
        var flxSegLoanFixed = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxSegLoanFixed",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 960, 1024]
        }, {}, {});
        flxSegLoanFixed.setDefaultUnit(kony.flex.DP);
        var lblFixedRate = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "40dp",
            "id": "lblFixedRate",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "1dp",
            "secureTextEntry": false,
            "skin": "sknTxt14px434343BGffffff",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "5dp",
            "width": "145dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lstOperand = new kony.ui.ListBox({
            "height": "40dp",
            "id": "lstOperand",
            "isVisible": true,
            "left": "60dp",
            "masterData": [
                ["lb1", "Add"],
                ["lb2", "SUB"],
                ["lb3", "MUL"]
            ],
            "selectedKey": "lb1",
            "skin": "sknlst14pxBG434343NoBorder",
            "top": "5dp",
            "width": "160dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        var lblMargin = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "40dp",
            "id": "lblMargin",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "60dp",
            "secureTextEntry": false,
            "skin": "sknTxt14px434343BGffffff",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "5dp",
            "width": "145dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblEffectiveRate = new kony.ui.Label({
            "height": "40dp",
            "id": "lblEffectiveRate",
            "isVisible": true,
            "left": "60dp",
            "skin": "sknlbl14pxFT757575BGF2F2F2",
            "text": "Label",
            "top": "5dp",
            "width": "145dp",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUptoAmount = new kony.ui.Label({
            "height": "40dp",
            "id": "lblUptoAmount",
            "isVisible": true,
            "left": "60dp",
            "skin": "sknlbl14pxFT757575BGF2F2F2",
            "text": "Label",
            "top": "5dp",
            "width": "145dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDeleteButton = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDeleteButton",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "26dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "145dp",
            "zIndex": 1
        }, {}, {});
        flxDeleteButton.setDefaultUnit(kony.flex.DP);
        var imgDelete = new kony.ui.Image2({
            "height": "20dp",
            "id": "imgDelete",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "minus.png",
            "top": "10dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDelete = new kony.ui.Label({
            "height": "20dp",
            "id": "lblDelete",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblBg000000px14",
            "text": "Delete",
            "top": "10dp",
            "width": "41dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDeleteButton.add(imgDelete, lblDelete);
        flxSegLoanFixed.add(lblFixedRate, lstOperand, lblMargin, lblEffectiveRate, lblUptoAmount, flxDeleteButton);
        return flxSegLoanFixed;
    }
})