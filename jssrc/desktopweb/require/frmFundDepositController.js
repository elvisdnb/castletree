define("userfrmFundDepositController", {
    _arrangementId: "",
    _depositObj: {},
    accNo: "",
    //   customerId :"",
    DetailsArray: [],
    currentOverride: {},
    popupSegData: [],
    gblcurrency: "",
    onNavigate: function(navObj) {
        this.bindEvents();
        this.view.cusCalenderPaymentDate.value = "";
        kony.print("navObj >>>" + JSON.stringify(navObj));
        this.accNo = navObj.lblAcNum;
        this.gblcurrency = navObj.lblCCY;
        this.setDepositHeaderDetails(navObj);
        if (navObj && navObj.lblArrangementID) {
            this._arrangementId = navObj.lblArrangementID;
        }
        this.view.cusTextExpectedBalance.currencyCode = navObj.lblCCY;
        this.view.cusTextAvailableBalance.currencyCode = navObj.lblCCY;
        this.view.cusTextAmount.currencyCode = navObj.lblCCY;
    },
    // *************************************************************************************
    // Created Date : 09/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : Used to Bind the events
    // *************************************************************************************
    bindEvents: function() {
        var self = this;
        this.view.postShow = this.postShowFunctionCall;
        this.view.preShow = function() {
            self.fundingDepositStatus();
        };
        this.view.cusButtonConfirm.onclickEvent = this.onClickSubmitData;
        this.view.cusButtonCancel.onclickEvent = this.onClickCancel;
        this.view.flxHeaderSegmentOutstandingClose.onTouchStart = this.closeAccountsListPopup;
        this.view.floatLabelText.downArrowLbl.onTouchStart = this.showDropdownAccountList;
        this.view.floatLabelText.animateComponent();
        this.view.ErrorAllert.imgClose.onTouchEnd = this.onCloseErrorAllert;
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup.btnProceed.onClick = this.onClickSubmitData;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.flxSearch.onTouchStart = this.advancedSearchAccountsShow;
        this.view.popupPayinPayout.segPayinPayout.onRowClick = this.getSegRowinfo;
        this.view.popupPayinPayout.flxPopUpClose.onTouchStart = this.advancedSearchClose;
        this.view.AccountList2.segAccountList.onRowClick = this.getSegRowInfoDropdown;
        this.view.cusTextAmount.validateRequired = "required";
        this.view.flxBack.onTouchStart = this.navToSCV;
    },
    navToSCV: function() {
        var navToHome = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._depositObj.customerId;
        navObjet.showMessageInfo = false;
        navObjet.isShowDeposits = "true";
        navToHome.navigate(navObjet);
    },
    showDropdownAccountList: function() {
        try {
            this.view.AccountList2.segAccountList.widgetDataMap = gblCutacctWidgetDate;
            this.view.AccountList2.segAccountList.setData(gblCutacctArray);
            this.view.flxDropDownAccountList.isVisible = true;
            this.view.lblErrorMsg.isVisible = false;
            this.view.flxDropDownAccountList.forceLayout();
        } catch (err) {
            kony.print("Error in FundDeposit Controller showDropDownAccountList :::" + err);
        }
    },
    advancedSearchAccountsShow: function() {
        try {
            this.view.flxSearchAccount.isVisible = true;
            this.view.flxDropDownAccountList.isVisible = false;
            this.view.lblErrorMsg.isVisible = false;
            this.view.popupPayinPayout.segPayinPayout.removeAll();
            this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate;
            this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        } catch (err) {
            kony.print("Error in FundDeposit Controller advancedSearchAccountsShow :::" + err);
        }
    },
    // *************************************************************************************
    // Created Date : 09/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : Setting the widget properties state.
    // *************************************************************************************
    postShowFunctionCall: function() {
        try {
            //       this.view.floatLabelText1.txtFloatText.text ="";
            this.view.flxDebitAccount.skin = "sknFlxWhiteBg";
            this.view.floatLabelText.lblFloatLabel.skin = "CopysknFloatLblPreInput";
            //       this.view.floatLabelText1.lblFloatLabel.skin="sknFloatLblPreInput";
            //       this.view.cusTextDepositAccountNo.disabled = true;
            this.view.cusTextDepositAccountNo.value = "";
            //       this.view.cusTextDepositAccountNo.readonly = true;
            //       this.view.cusTextExpectedBalance.readonly = true;
            this.view.cusTextExpectedBalance.value = "";
            //       this.view.cusTextAvailableBalance.readonly = true;
            this.view.cusTextAvailableBalance.value = "";
            this.view.cusTextAmount.decimals = 2;
            this.view.cusTextAmount.required == "validateRequired";
            this.view.cusTextAmount.value = "";
            this.view.cusCalenderPaymentDate.displayFormat = "dd MMM yyyy";
            this.getCurrentDate();
            //this.view.cusCalenderPaymentDate.value = currentDate;
            //this.view.cusCalenderPaymentDate.min = currentDate;
            this.view.flxDropDownAccountListOutsideWrapper.setVisibility(false);
            this.view.flxSearchAccount.setVisibility(false);
            this.view.MainTabs.flxContainerdeposits.skin = "sknSelected";
            this.view.MainTabs.btnAccounts1.skin = "sknTabUnselected";
            this.view.MainTabs.btnAccounts2.skin = "sknTabSelected";
            this.view.MainTabs.btnAccounts3.skin = "sknTabUnselected";
            this.view.MainTabs.flxontainerLoans.skin = "sknNotselectedTransparent";
            this.view.floatLabelText.txtFloatText.text = "";
            this.view.lblErrorMsg.isVisible = false;
            this.view.flxDropDownAccountList.isVisible = false;
            this.view.floatLabelText.flxFloatLableGrp.centerY = "50%";
            // this.setDepositHeaderDetails();
            this.setResctricted();
            this.view.floatLabelText.animateComponent();
            this.view.ErrorAllert.setVisibility(false);
            this.view.forceLayout();
        } catch (err) {
            kony.print("Error in fundDepositController postShowFunctionCall::" + err);
        }
    },
    // *************************************************************************************
    // Created Date : 22/03/2021
    // Created by   : Sachin Kamnure
    // Organization : ITSS
    // Function purpose : To Display the Current Date With Format DD-MMM-YYYY
    // *************************************************************************************
    getCurrentDate: function() {
        try {
            var serviceName = "FundsDeposit";
            var operationName = "getDate";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {};
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgeDate, this.errorCBgetDate);
        } catch (err) {
            kony.print("Error in Fund DepositController getCurrentDate:::" + err);
        }
    },
    successCBgeDate: function(res) {
        try {
            var today = res.body[0].currentWorkingDate;
            //        var today = new Date(); 
            var dd = today.slice(8, 10);
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            shortMonth = today.slice(5, 7);
            switch (shortMonth) {
                case "01":
                    // code block
                    shortMonth = "Jan";
                    break;
                case "02":
                    // code block
                    shortMonth = "Feb";
                    break;
                case "03":
                    // code block
                    shortMonth = "Mar";
                    break;
                case "04":
                    // code block
                    shortMonth = "Epr";
                    break;
                case "05":
                    // code block
                    shortMonth = "May";
                    break;
                case "06":
                    // code block
                    shortMonth = "Jun";
                    break;
                case "07":
                    // code block
                    shortMonth = "Jul";
                    break;
                case "08":
                    // code block
                    shortMonth = "Aug";
                    break;
                case "09":
                    // code block
                    shortMonth = "Sep";
                    break;
                case "10":
                    // code block
                    shortMonth = "Oct";
                    break;
                case "11":
                    // code block
                    shortMonth = "Nov";
                    break;
                case "12":
                    // code block
                    shortMonth = "Dec";
                    break;
                default:
                    // code block
                    shortMonth = "";
            }
            var yyyy = today.slice(0, 4);
            if (dd < 10) {
                dd = '0' + dd;
            }
            var todayConverted = dd + " " + shortMonth + " " + yyyy;
            this.view.cusCalenderPaymentDate.value = todayConverted;
            this.view.cusCalenderPaymentDate.min = todayConverted;
            //  return todayConverted;
            /**this.view.cusEffectiveDateResponse.displayFormat = "dd-MMM-yyyy";
            this.view.cusEffectiveDateResponse.value = todayConverted;
            this.view.cusEffectiveDateResponse.min = todayConverted;**/
        } catch (err) {
            kony.print("Error in Fund DepositController getCurrentDate:::" + err);
        }
    },
    errorCBgetDate: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            this.view.ErrorAllert.isVisible = true;
            kony.print("error response:::" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in FundDeposit Controller errorCBgetDate::::" + err);
        }
    },
    // *************************************************************************************
    // Created Date : 15/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : restricted inputs for the amount field
    // *************************************************************************************
    setResctricted: function() {
        this.view.floatLabelText.txtFloatText.restrictCharactersSet = ` TMtmabcdefghijklnopqrsuvwxyzABCDEFGHIJKLNOPQRSUVWXYZ~!@#$%^&*()_+{}|\'";:/?><,=-`;
        //     this.view.floatLabelText.txtFloatText.restrictCharactersSet = `+-.`;
    },
    // *************************************************************************************
    // Created Date : 09/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : Mapping the deposit details to frmFundDetails
    // *************************************************************************************
    setDepositHeaderDetails: function(navObj) {
        try {
            var newObj = navObj;
            this._depositObj.currencyId = navObj.lblCCY;
            this._depositObj.arrangementId = navObj.lblArrangementID;
            this._depositObj.customerId = navObj.customerId;
            this._depositObj.produtId = navObj.loanProduct;
            //TODO : take from segment data a navigation object
            this.view.FundDetails.lblAccountNumberResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAcNum);
            this.view.FundDetails.lblAccountTypeResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAcType); //"2345678";
            this.view.FundDetails.lblCurrencyValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblCCY);
            //this.view.FundDetails.lblAmountValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblClearBal);
            this.view.FundDetails.lblStartDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblLockedAc);
            this.view.FundDetails.lblMaturityDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAvlBal);
        } catch (err) {
            kony.print("Error in setDepositHeaderDetails:::" + err);
        }
    },
    //   setDepositHeaderDetails:function(){
    //     try{
    //       // this._DepositObj.currencyId=loanSegRow.lblCCY;
    //       // this._DepositObj.arrangementId=loanSegRow.lblArrangementId;
    //       // this._DepositObj.customerId=loanSegRow.customerId;
    //       // this._DepositObj.produtId=loanSegRow.loanProduct;
    //       // this._DepositObj.loanAccountId=loanSegRow.lblHeaderAN;
    //       this.view.FundDetails.lblAccountNumberResponse.text =  "50008778";
    //       this.view.FundDetails.lblAccountTypeResponse.text = "Long Term Deposit"; //"2345678";
    //       this.view.FundDetails.lblCurrencyValueResponse.text = "USD";
    //       //       this.view.FundDetails.lblAmountValueResponse.text = this.view.cusTextAvailableBalance.value;
    //       this.view.FundDetails.lblStartDateResponse.text = "28 Jan 2021";
    //       this.view.FundDetails.lblMaturityDateResponse.text = "28 Jan 2022";
    //     } 
    //     catch(err) {
    //       kony.print("Error in Fund DepositController setDepositHeaderDetails:::"+err);
    //     }
    //   },
    removeMinus: function(x) {
        try {
            var amount = Math.abs(x);
            //       alert("amount::" + amount);
            return amount;
        } catch (err) {
            kony.print("error in removeMinus:::" + err);
        }
    },
    fundingDepositStatus: function() {
        try {
            var serviceName = "FundsDeposit";
            var operationName = "FundingDepositStatus";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {
                "contractId": this.accNo,
            };
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBFundingDepositStatus, this.errorCBFundingDepositStatus);
        } catch (err) {
            kony.print("Error in FundDeposit Controller fundingDepositStatus::::" + err);
        }
    },
    successCBFundingDepositStatus: function(res) {
        try {
            kony.print("success response::: >>>>> " + JSON.stringify(res));
            var response = res.body;
            var expectedBalance;
            var currency = "USD";
            for (var i = 0; i < response.length; i++) {
                if (response[i].ccy !== null && response[i].ccy !== undefined) {
                    currency = response[i].ccy;
                }
                if (response[i].balanceType === "EXPACCOUNT") {
                    expectedBalance = response[i].closingBalance;
                    var exp = expectedBalance.replace("-", "");
                }
                if (response[i].balanceType === "CURACCOUNT") {
                    var availableBalance = response[i].closingBalance;
                    var ava = availableBalance.replace("-", "");
                }
            }
            kony.print("curr from service >> " + currency);
            // this.view.cusTextExpectedBalance.currencyCode = currency;
            //this.view.cusTextAvailableBalance.currencyCode =currency;
            //this.view.cusTextAmount.currencyCode =currency;
            var expBalance = expectedBalance < 0 || expectedBalance === undefined ? "0.00" : exp;
            var avaBalance = availableBalance < 0 || availableBalance === undefined ? "0.00" : ava;
            //       this.view.cusTextExpectedBalance.value = expectedBalance < 0 || expectedBalance === undefined ? "0.00" :  exp  ;
            //       this.view.cusTextAvailableBalance.value = availableBalance < 0 || availableBalance === undefined ?  "0.00" :ava ;
            var depCurrency = currency;
            depCurrency = Application.validation.isNullUndefinedObj(depCurrency);
            var depBalance = expBalance;
            depBalance = Application.validation.isNullUndefinedObj(depBalance);
            if (depBalance !== "") {
                depBalance = depBalance.replace(/[\s,]+/g, '');
                depBalance = Application.numberFormater.convertForDispaly(this.removeMinus(depBalance), depCurrency);
                if (depBalance === "NaN") {
                    depBalance = "0.00";
                }
            } else {
                depBalance = Application.numberFormater.convertForDispaly(this.removeMinus("0"), depCurrency);
            }
            var depAvaBalance = avaBalance;
            depAvaBalance = Application.validation.isNullUndefinedObj(depAvaBalance);
            if (depAvaBalance !== "") {
                depAvaBalance = depAvaBalance.replace(/[\s,]+/g, '');
                depAvaBalance = Application.numberFormater.convertForDispaly(this.removeMinus(depAvaBalance), depCurrency);
                if (depAvaBalance === "NaN") {
                    depBalance = "0.00";
                }
            } else {
                depAvaBalance = Application.numberFormater.convertForDispaly(this.removeMinus("0"), depCurrency);
            }
            this.view.cusTextExpectedBalance.value = depBalance;
            this.view.cusTextAvailableBalance.value = depAvaBalance;
            this.view.cusTextDepositAccountNo.value = this.accNo;
            this.view.FundDetails.lblAmountValueResponse.text = this.view.cusTextAvailableBalance.value;
            this.view.ProgressIndicator.isVisible = false;
            this.view.flxFundDepositWrapper.forceLayout();
        } catch (err) {
            kony.print("Error in FundDeposit Controller successCBgetDeposits::::" + err);
        }
    },
    errorCBFundingDepositStatus: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print("error response:::" + JSON.stringify(res));
            this.view.ErrorAllert.lblMessage.text = res.error.message; //error.errorDetails[0].message
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.setFocus(true);
            kony.print("Service Failure:::" + JSON.stringify(res.error.errorDetails));
            this.view.forceLayout();
        } catch (err) {
            kony.print("Error in FundDeposit Controller errorCBgetDeposists::::" + err);
        }
    },
    // ****************************************START*********************************************
    // Started Date : 15/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : invoke integration service for confirm to FundDeposit
    // *************************************************************************************
    onClickSubmitData: function() {
        try {
            var scope = this;
            this.view.lblErrorMsg.isVisible = false;
            var creditAccountId = this.view.cusTextDepositAccountNo.value;
            var DEbitAccount = this.view.floatLabelText.txtFloatText.text;
            //       var  Amount= this.view.floatLabelText1.txtFloatText.text;
            var Amount = this.view.cusTextAmount.value;
            //var Numeric= Amount.replace(/[\s,]+/g,''). trim();
            var Numeric = this.removeAmountFormatNew(Amount, this.gblcurrency);
            var valueDate = this.view.cusCalenderPaymentDate.value;
            var expectedBalance = this.view.cusTextExpectedBalance.value;
            //       alert(expectedBalance);
            if (Amount !== "" && Amount !== undefined && Amount !== " " && DEbitAccount !== "" && DEbitAccount !== undefined && DEbitAccount !== " ") {
                if (expectedBalance !== "0.00" && expectedBalance !== "0,00") {
                    this.view.lblErrorMsg.isVisible = false;
                    this.view.flxDebitAccount.skin = "sknFlxWhiteBg";
                    this.view.flxAmount.skin = "sknFlxWhiteBg";
                    //           this.view.floatLabelText1. clearErrormodeSkin();
                    var serviceName = "FundsDeposit";
                    var operationName = "initiatePaymentOrder";
                    var headers = {
                        "validate_only": "true",
                        "companyId": "NL0020001"
                    };
                    kony.print("Header Params" + JSON.stringify(headers));
                    var inputParams = {
                        "creditvaluedate": valueDate,
                        "paymentOrderProductId": "ACOTHER",
                        "creditAccountId": creditAccountId,
                        "debitAccountId": DEbitAccount,
                        "amount": Numeric,
                        "orderInitiationType": "FDDEPOSIT",
                        "overrideDetails": this.currentOverride
                    };
                    kony.print("Input Params" + JSON.stringify(inputParams));
                    this.view.ProgressIndicator.isVisible = true;
                    MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, function(res) {
                        scope.submitData_SuccessCB(res, inputParams);
                    }, this.submitData_FailureCB);
                } else {
                    this.view.ErrorAllert.lblMessage.text = "This deposit is fully funded";
                    this.view.ErrorAllert.isVisible = true;
                    this.view.ErrorAllert.setFocus(true);
                    this.view.forceLayout();
                }
            } else if ((Amount === "" || Amount === undefined || Amount === " ") && (DEbitAccount === "" || DEbitAccount === undefined || DEbitAccount === " ")) {
                this.view.flxAmount.skin = "sknFlxWhiteBg";
                this.view.cusTextAmount.required = true;
                this.view.flxDebitAccount.skin = "sknFlxWhiteBgRedBorder";
                this.view.lblErrorMsg.isVisible = true;
            } else if ((Amount !== "" && Amount !== undefined && Amount !== " ") && (DEbitAccount === null || DEbitAccount === undefined || DEbitAccount === " " || DEbitAccount === "")) {
                this.view.flxDebitAccount.skin = "sknFlxWhiteBgRedBorder";
                this.view.flxAmount.skin = "sknFlxWhiteBg";
                this.view.lblErrorMsg.isVisible = true;
            } else {
                this.view.flxDebitAccount.skin = "sknFlxWhiteBg";
                this.view.flxAmount.skin = "sknFlxWhiteBg";
                //         this.view.floatLabelText1.setErrorModeSkin();
                this.view.lblErrorMsg.isVisible = true;
            }
        } catch (err) {
            kony.print("Error in Fund DepositController onClickSubmitData:::" + err);
        }
    },
    // ****************************************START*********************************************
    // Started Date : 15/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : SuccessCB of submitting data and invoke the integration service to authorize
    // ******************************************************************************************
    submitData_SuccessCB: function(res, inputParams) {
        try {
            this.currentOverride = {};
            this.view.ProgressIndicator.isVisible = false;
            this.view.SegmentPopup.isVisible = false;
            kony.print("Success CallBack :::" + JSON.stringify(res));
            var status = res.header.status;
            if (status === "success") {
                var serviceName = "FundsDeposit";
                var operationName = "initiatePaymentOrder";
                var headers = {
                    "companyId": "NL0020001"
                };
                var data = {
                    "creditvaluedate": inputParams.creditvaluedate,
                    "paymentOrderProductId": inputParams.paymentOrderProductId,
                    "creditAccountId": inputParams.creditAccountId,
                    "debitAccountId": inputParams.debitAccountId,
                    "amount": inputParams.amount,
                    "orderInitiationType": inputParams.orderInitiationType,
                    "overrideDetails": inputParams.overrideDetails
                };
                this.view.ProgressIndicator.isVisible = true;
                MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, data, this.fundDeposit_SuccessCB, this.fundDeposit_FailureCB);
            }
        } catch (err) {
            kony.print("Error in Fund DepositController submitData_SuccessCB:::" + err);
        }
    },
    // ****************************************START*********************************************
    // Started Date : 15/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : SuccessCB of Funding Deposit
    // ******************************************************************************************
    fundDeposit_SuccessCB: function(res) {
        try {
            this.currentOverride = {};
            kony.print("Success CallBack :::" + JSON.stringify(res));
            this.view.ProgressIndicator.isVisible = false;
            if (res === "" || res === null || res === undefined) {
                kony.print("Response is null or undefined");
            } else {
                var refId = res.header["id"];
                var navToFundDeposit = new kony.mvc.Navigation("frmCustomerAccountsDetails");
                var navObjet = {};
                navObjet.customerId = this._depositObj.customerId;
                navObjet.showMessageInfo = true;
                navObjet.isShowDeposits = "true";
                navObjet.messageInfo = "Fund Deposit completed successfully!   Reference :" + refId;
                navToFundDeposit.navigate(navObjet);
            }
        } catch (err) {
            kony.print("Error in Fund DepositController fundDeposi_AuthorizeSuccessCB:::" + err);
        }
    },
    // ****************************************START*********************************************
    // Started Date : 15/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : FailureCB of Funding Deposit
    // ******************************************************************************************
    fundDeposit_FailureCB: function(res) {
        try {
            this.currentOverride = {};
            this.view.ProgressIndicator.isVisible = false;
            if (res.error.errorDetails !== []) { // !== "" || res.error.errorDetails !== null || res.error.errorDetails !== undefined || res.error.errorDetails !== []) {
                kony.print("Inside ErrorDetails=====>");
                this.view.ProgressIndicator.isVisible = false;
                if (res.error.errorDetails[0] === "" || res.error.errorDetails[0] === [] || res.error.errorDetails[0] === undefined || res.error.errorDetails[0] === null) {
                    kony.print("Inside ErrorDetails Empty=====>");
                } else {
                    this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message; //error.errorDetails[0].message
                    this.view.ErrorAllert.isVisible = true;
                    this.view.ErrorAllert.setFocus(true);
                    kony.print("Service Failure:::" + JSON.stringify(res.error.errorDetails));
                    this.view.forceLayout();
                }
            }
        } catch (err) {
            kony.print("Error in Fund DepositController fundDeposite_AuthorizeFailureCB:::" + err);
        }
    },
    // ****************************************START*********************************************
    // Started Date : 09/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : FailureCB of submitting the Funding Deposit
    // ******************************************************************************************
    submitData_FailureCB: function(res) {
        try {
            this.currentOverride = {};
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Inside ErrorDetails=====>" + JSON.stringify(res.error.errorDetails));
            if (res.override !== "" || res.override !== null || res.override !== undefined) {
                var overwriteDetails = [];
                var overWriteData = [];
                var segDataForOverrite = [];
                this.currentOverride = res.override.overrideDetails;
                kony.print(JSON.stringify(this.currentOverride));
                overwriteDetails = res.override.overrideDetails;
                if (overwriteDetails.length > 0) {
                    for (var a in overwriteDetails) {
                        overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                        var json = {
                            "lblSerial": "*",
                            "lblInfo": overwriteDetails[a].description
                        };
                        segDataForOverrite.push(json);
                    }
                    this.view.SegmentPopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
                    this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
                    this.view.SegmentPopup.isVisible = true;
                    kony.print("overWriteData::::" + JSON.stringify(overWriteData));
                    kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
                    this.view.forceLayout();
                }
            }
            if (res.error.errorDetails !== []) { // !== "" || res.error.errorDetails !== null || res.error.errorDetails !== undefined || res.error.errorDetails !== []) {
                kony.print("Inside ErrorDetails=====>");
                this.view.ProgressIndicator.isVisible = false;
                if (res.error.errorDetails[0] === "" || res.error.errorDetails[0] === [] || res.error.errorDetails[0] === undefined || res.error.errorDetails[0] === null) {
                    kony.print("Inside ErrorDetails Empty=====>");
                } else {
                    this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message; //error.errorDetails[0].message
                    this.view.ErrorAllert.isVisible = true;
                    this.view.ErrorAllert.setFocus(true);
                    kony.print("Service Failure:::" + JSON.stringify(res.error.errorDetails));
                    this.view.forceLayout();
                }
            }
        } catch (err) {
            kony.print("Error in Fund DepositController submitData_FailureCB:::" + err);
        }
    },
    // ****************************************START*********************************************
    // Started Date : 09/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : Navigating to the CustomerAccountDetails
    // ******************************************************************************************
    onClickCancel: function() {
        try {
            var navToHome = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObjet = {};
            navObjet.customerId = this._depositObj.customerId;
            navObjet.showMessageInfo = false;
            navObjet.isShowDeposits = "true";
            navToHome.navigate(navObjet);
            //       navToHome.navigate();
        } catch (err) {
            kony.print("Error in Fund DepositController onClickCancel:::" + err);
        }
    },
    // *************************************************************************************
    // Created Date : 11/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : To close the Error Alert
    // *************************************************************************************
    onCloseErrorAllert: function() {
        this.view.ErrorAllert.isVisible = false;
        this.view.flxHeaderMenu.isVisible = true;
    },
    // *************************************************************************************
    // Created Date : 16/03/2021
    // Created by   : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose : To close the override popup.
    // *************************************************************************************
    closeSegmentOverrite: function() {
        this.view.SegmentPopup.isVisible = false;
        this.view.forceLayout();
    },
    advancedSearchClose: function() {
        this.view.flxSearchAccount.setVisibility(false);
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.popupSegData = gblAllaccountarray;
        this.view.popupPayinPayout.segPayinPayout.setData(this.popupSegData);
    },
    getSegRowinfo: function() {
        var getAcctId = this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].lblAccountId;
        //     var curId=this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].currency;
        //     this._creditCurrency = curId;
        this.view.flxSearchAccount.setVisibility(false);
        //this.view.tbxPayOutAccount.text=getAcctId; 
        this.view.floatLabelText.txtFloatText.text = getAcctId;
        this.view.floatLabelText.animateComponent();
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.view.popupPayinPayout.segPayinPayout.setData(this.popupSegData);
        // this.view.custPayIn.options = JSON.stringify(accountArray);
    },
    getSegRowInfoDropdown: function() {
        var getAcctId = this.view.AccountList2.segAccountList.selectedRowItems[0].lblAccountId;
        //     var curId=this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].currency;
        //     this._creditCurrency = curId;
        this.view.flxSearchAccount.setVisibility(false);
        //this.view.tbxPayOutAccount.text=getAcctId; 
        this.view.floatLabelText.txtFloatText.text = getAcctId;
        this.view.floatLabelText.animateComponent();
        //     this.view.popupPayinPayout.txtSearchBox.text=""
        this.view.flxDropDownAccountList.setVisibility(false);
        this.view.AccountList2.segAccountList.removeAll();
        this.view.AccountList2.segAccountList.setData(gblCutacctArray);
    },
    removeAmountFormatNew: function(amnt, currency) {
        try {
            if (amnt === "0.00" || amnt === 0.00 || amnt === "0,00") return "0.00";
            var amount = "";
            if (amnt) {
                if (amnt.charAt(amnt.length - 3) === ",") {
                    amount = amnt.replace(/\./g, ""); //remove all dots
                    amount = amount.replace(/[\s,]+/g, '.').trim(); //replace comma wih dot
                } else {
                    amount = amnt.replace(/[\s,]+/g, ""); //remove all commas
                }
            }
            kony.print("Amount after removing all formats: " + amount);
            return amount;
        } catch (err) {
            kony.print("Exception in removeAmountFormat: " + err);
        }
    },
});
define("frmFundDepositControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("frmFundDepositController", ["userfrmFundDepositController", "frmFundDepositControllerActions"], function() {
    var controller = require("userfrmFundDepositController");
    var controllerActions = ["frmFundDepositControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
