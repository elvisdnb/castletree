/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {
        "object": "customerObject",
        "objectService": "cdmSearchCustomer"
    };
    var setterFunctions = {
        firstName: function(val, state) {
            context["field"] = "firstName";
            context["metadata"] = (objectMetadata ? objectMetadata["firstName"] : null);
            state['firstName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        lastName: function(val, state) {
            context["field"] = "lastName";
            context["metadata"] = (objectMetadata ? objectMetadata["lastName"] : null);
            state['lastName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        dateOfBirth: function(val, state) {
            context["field"] = "dateOfBirth";
            context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirth"] : null);
            state['dateOfBirth'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        email: function(val, state) {
            context["field"] = "email";
            context["metadata"] = (objectMetadata ? objectMetadata["email"] : null);
            state['email'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        accountOfficerId: function(val, state) {
            context["field"] = "accountOfficerId";
            context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerId"] : null);
            state['accountOfficerId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerId: function(val, state) {
            context["field"] = "customerId";
            context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
            state['customerId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        location: function(val, state) {
            context["field"] = "location";
            context["metadata"] = (objectMetadata ? objectMetadata["location"] : null);
            state['location'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerName: function(val, state) {
            context["field"] = "customerName";
            context["metadata"] = (objectMetadata ? objectMetadata["customerName"] : null);
            state['customerName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        maritalStatus: function(val, state) {
            context["field"] = "maritalStatus";
            context["metadata"] = (objectMetadata ? objectMetadata["maritalStatus"] : null);
            state['maritalStatus'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        nationality: function(val, state) {
            context["field"] = "nationality";
            context["metadata"] = (objectMetadata ? objectMetadata["nationality"] : null);
            state['nationality'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        residencePhoneNo: function(val, state) {
            context["field"] = "residencePhoneNo";
            context["metadata"] = (objectMetadata ? objectMetadata["residencePhoneNo"] : null);
            state['residencePhoneNo'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        officePhoneNo: function(val, state) {
            context["field"] = "officePhoneNo";
            context["metadata"] = (objectMetadata ? objectMetadata["officePhoneNo"] : null);
            state['officePhoneNo'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        primaryAddress: function(val, state) {
            context["field"] = "primaryAddress";
            context["metadata"] = (objectMetadata ? objectMetadata["primaryAddress"] : null);
            state['primaryAddress'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        secondaryAddress: function(val, state) {
            context["field"] = "secondaryAddress";
            context["metadata"] = (objectMetadata ? objectMetadata["secondaryAddress"] : null);
            state['secondaryAddress'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        preferredChannel: function(val, state) {
            context["field"] = "preferredChannel";
            context["metadata"] = (objectMetadata ? objectMetadata["preferredChannel"] : null);
            state['preferredChannel'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        title: function(val, state) {
            context["field"] = "title";
            context["metadata"] = (objectMetadata ? objectMetadata["title"] : null);
            state['title'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        residence: function(val, state) {
            context["field"] = "residence";
            context["metadata"] = (objectMetadata ? objectMetadata["residence"] : null);
            state['residence'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        gender: function(val, state) {
            context["field"] = "gender";
            context["metadata"] = (objectMetadata ? objectMetadata["gender"] : null);
            state['gender'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        fax: function(val, state) {
            context["field"] = "fax";
            context["metadata"] = (objectMetadata ? objectMetadata["fax"] : null);
            state['fax'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerIdCT: function(val, state) {
            context["field"] = "customerIdCT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerIdCT"] : null);
            state['customerIdCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerIdBW: function(val, state) {
            context["field"] = "customerIdBW";
            context["metadata"] = (objectMetadata ? objectMetadata["customerIdBW"] : null);
            state['customerIdBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerIdMT: function(val, state) {
            context["field"] = "customerIdMT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerIdMT"] : null);
            state['customerIdMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerNameCT: function(val, state) {
            context["field"] = "customerNameCT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerNameCT"] : null);
            state['customerNameCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerNameBW: function(val, state) {
            context["field"] = "customerNameBW";
            context["metadata"] = (objectMetadata ? objectMetadata["customerNameBW"] : null);
            state['customerNameBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerNameMT: function(val, state) {
            context["field"] = "customerNameMT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerNameMT"] : null);
            state['customerNameMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        street: function(val, state) {
            context["field"] = "street";
            context["metadata"] = (objectMetadata ? objectMetadata["street"] : null);
            state['street'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        streetCT: function(val, state) {
            context["field"] = "streetCT";
            context["metadata"] = (objectMetadata ? objectMetadata["streetCT"] : null);
            state['streetCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        streetBW: function(val, state) {
            context["field"] = "streetBW";
            context["metadata"] = (objectMetadata ? objectMetadata["streetBW"] : null);
            state['streetBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        streetMT: function(val, state) {
            context["field"] = "streetMT";
            context["metadata"] = (objectMetadata ? objectMetadata["streetMT"] : null);
            state['streetMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        addressCity: function(val, state) {
            context["field"] = "addressCity";
            context["metadata"] = (objectMetadata ? objectMetadata["addressCity"] : null);
            state['addressCity'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        addressCityCT: function(val, state) {
            context["field"] = "addressCityCT";
            context["metadata"] = (objectMetadata ? objectMetadata["addressCityCT"] : null);
            state['addressCityCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        addressCityBW: function(val, state) {
            context["field"] = "addressCityBW";
            context["metadata"] = (objectMetadata ? objectMetadata["addressCityBW"] : null);
            state['addressCityBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        addressCityMT: function(val, state) {
            context["field"] = "addressCityMT";
            context["metadata"] = (objectMetadata ? objectMetadata["addressCityMT"] : null);
            state['addressCityMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        countryId: function(val, state) {
            context["field"] = "countryId";
            context["metadata"] = (objectMetadata ? objectMetadata["countryId"] : null);
            state['countryId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        countryIdCT: function(val, state) {
            context["field"] = "countryIdCT";
            context["metadata"] = (objectMetadata ? objectMetadata["countryIdCT"] : null);
            state['countryIdCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        countryIdBW: function(val, state) {
            context["field"] = "countryIdBW";
            context["metadata"] = (objectMetadata ? objectMetadata["countryIdBW"] : null);
            state['countryIdBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        countryIdMT: function(val, state) {
            context["field"] = "countryIdMT";
            context["metadata"] = (objectMetadata ? objectMetadata["countryIdMT"] : null);
            state['countryIdMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        postCode: function(val, state) {
            context["field"] = "postCode";
            context["metadata"] = (objectMetadata ? objectMetadata["postCode"] : null);
            state['postCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        postCodeCT: function(val, state) {
            context["field"] = "postCodeCT";
            context["metadata"] = (objectMetadata ? objectMetadata["postCodeCT"] : null);
            state['postCodeCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        postCodeBW: function(val, state) {
            context["field"] = "postCodeBW";
            context["metadata"] = (objectMetadata ? objectMetadata["postCodeBW"] : null);
            state['postCodeBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        postCodeMT: function(val, state) {
            context["field"] = "postCodeMT";
            context["metadata"] = (objectMetadata ? objectMetadata["postCodeMT"] : null);
            state['postCodeMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        dateOfBirthCT: function(val, state) {
            context["field"] = "dateOfBirthCT";
            context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthCT"] : null);
            state['dateOfBirthCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        dateOfBirthBW: function(val, state) {
            context["field"] = "dateOfBirthBW";
            context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthBW"] : null);
            state['dateOfBirthBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        dateOfBirthMT: function(val, state) {
            context["field"] = "dateOfBirthMT";
            context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthMT"] : null);
            state['dateOfBirthMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerStatus: function(val, state) {
            context["field"] = "customerStatus";
            context["metadata"] = (objectMetadata ? objectMetadata["customerStatus"] : null);
            state['customerStatus'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerStatusCT: function(val, state) {
            context["field"] = "customerStatusCT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerStatusCT"] : null);
            state['customerStatusCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerStatusBW: function(val, state) {
            context["field"] = "customerStatusBW";
            context["metadata"] = (objectMetadata ? objectMetadata["customerStatusBW"] : null);
            state['customerStatusBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerStatusMT: function(val, state) {
            context["field"] = "customerStatusMT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerStatusMT"] : null);
            state['customerStatusMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        sectorId: function(val, state) {
            context["field"] = "sectorId";
            context["metadata"] = (objectMetadata ? objectMetadata["sectorId"] : null);
            state['sectorId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        sectorIdCT: function(val, state) {
            context["field"] = "sectorIdCT";
            context["metadata"] = (objectMetadata ? objectMetadata["sectorIdCT"] : null);
            state['sectorIdCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        sectorIdBW: function(val, state) {
            context["field"] = "sectorIdBW";
            context["metadata"] = (objectMetadata ? objectMetadata["sectorIdBW"] : null);
            state['sectorIdBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        sectorIdMT: function(val, state) {
            context["field"] = "sectorIdMT";
            context["metadata"] = (objectMetadata ? objectMetadata["sectorIdMT"] : null);
            state['sectorIdMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        accountOfficerIdCT: function(val, state) {
            context["field"] = "accountOfficerIdCT";
            context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdCT"] : null);
            state['accountOfficerIdCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        accountOfficerIdBW: function(val, state) {
            context["field"] = "accountOfficerIdBW";
            context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdBW"] : null);
            state['accountOfficerIdBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        accountOfficerIdMT: function(val, state) {
            context["field"] = "accountOfficerIdMT";
            context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdMT"] : null);
            state['accountOfficerIdMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        industryId: function(val, state) {
            context["field"] = "industryId";
            context["metadata"] = (objectMetadata ? objectMetadata["industryId"] : null);
            state['industryId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        industryIdCT: function(val, state) {
            context["field"] = "industryIdCT";
            context["metadata"] = (objectMetadata ? objectMetadata["industryIdCT"] : null);
            state['industryIdCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        industryIdBW: function(val, state) {
            context["field"] = "industryIdBW";
            context["metadata"] = (objectMetadata ? objectMetadata["industryIdBW"] : null);
            state['industryIdBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        industryIdMT: function(val, state) {
            context["field"] = "industryIdMT";
            context["metadata"] = (objectMetadata ? objectMetadata["industryIdMT"] : null);
            state['industryIdMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        nationalityId: function(val, state) {
            context["field"] = "nationalityId";
            context["metadata"] = (objectMetadata ? objectMetadata["nationalityId"] : null);
            state['nationalityId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        nationalityIdCT: function(val, state) {
            context["field"] = "nationalityIdCT";
            context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdCT"] : null);
            state['nationalityIdCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        nationalityIdBW: function(val, state) {
            context["field"] = "nationalityIdBW";
            context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdBW"] : null);
            state['nationalityIdBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        nationalityIdMT: function(val, state) {
            context["field"] = "nationalityIdMT";
            context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdMT"] : null);
            state['nationalityIdMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        residenceId: function(val, state) {
            context["field"] = "residenceId";
            context["metadata"] = (objectMetadata ? objectMetadata["residenceId"] : null);
            state['residenceId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        residenceIdCT: function(val, state) {
            context["field"] = "residenceIdCT";
            context["metadata"] = (objectMetadata ? objectMetadata["residenceIdCT"] : null);
            state['residenceIdCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        residenceIdBW: function(val, state) {
            context["field"] = "residenceIdBW";
            context["metadata"] = (objectMetadata ? objectMetadata["residenceIdBW"] : null);
            state['residenceIdBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        residenceIdMT: function(val, state) {
            context["field"] = "residenceIdMT";
            context["metadata"] = (objectMetadata ? objectMetadata["residenceIdMT"] : null);
            state['residenceIdMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        phoneNumber: function(val, state) {
            context["field"] = "phoneNumber";
            context["metadata"] = (objectMetadata ? objectMetadata["phoneNumber"] : null);
            state['phoneNumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        phoneNumberCT: function(val, state) {
            context["field"] = "phoneNumberCT";
            context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberCT"] : null);
            state['phoneNumberCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        phoneNumberBW: function(val, state) {
            context["field"] = "phoneNumberBW";
            context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberBW"] : null);
            state['phoneNumberBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        phoneNumberMT: function(val, state) {
            context["field"] = "phoneNumberMT";
            context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberMT"] : null);
            state['phoneNumberMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerMnemonic: function(val, state) {
            context["field"] = "customerMnemonic";
            context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonic"] : null);
            state['customerMnemonic'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerMnemonicCT: function(val, state) {
            context["field"] = "customerMnemonicCT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicCT"] : null);
            state['customerMnemonicCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerMnemonicBW: function(val, state) {
            context["field"] = "customerMnemonicBW";
            context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicBW"] : null);
            state['customerMnemonicBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerMnemonicMT: function(val, state) {
            context["field"] = "customerMnemonicMT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicMT"] : null);
            state['customerMnemonicMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        firstNameCT: function(val, state) {
            context["field"] = "firstNameCT";
            context["metadata"] = (objectMetadata ? objectMetadata["firstNameCT"] : null);
            state['firstNameCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        firstNameBW: function(val, state) {
            context["field"] = "firstNameBW";
            context["metadata"] = (objectMetadata ? objectMetadata["firstNameBW"] : null);
            state['firstNameBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        firstNameMT: function(val, state) {
            context["field"] = "firstNameMT";
            context["metadata"] = (objectMetadata ? objectMetadata["firstNameMT"] : null);
            state['firstNameMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerType: function(val, state) {
            context["field"] = "customerType";
            context["metadata"] = (objectMetadata ? objectMetadata["customerType"] : null);
            state['customerType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerTypeCT: function(val, state) {
            context["field"] = "customerTypeCT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerTypeCT"] : null);
            state['customerTypeCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerTypeBW: function(val, state) {
            context["field"] = "customerTypeBW";
            context["metadata"] = (objectMetadata ? objectMetadata["customerTypeBW"] : null);
            state['customerTypeBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        customerTypeMT: function(val, state) {
            context["field"] = "customerTypeMT";
            context["metadata"] = (objectMetadata ? objectMetadata["customerTypeMT"] : null);
            state['customerTypeMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        emailCT: function(val, state) {
            context["field"] = "emailCT";
            context["metadata"] = (objectMetadata ? objectMetadata["emailCT"] : null);
            state['emailCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        emailBW: function(val, state) {
            context["field"] = "emailBW";
            context["metadata"] = (objectMetadata ? objectMetadata["emailBW"] : null);
            state['emailBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        emailMT: function(val, state) {
            context["field"] = "emailMT";
            context["metadata"] = (objectMetadata ? objectMetadata["emailMT"] : null);
            state['emailMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        mobilePhoneNumber: function(val, state) {
            context["field"] = "mobilePhoneNumber";
            context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumber"] : null);
            state['mobilePhoneNumber'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        mobilePhoneNumberCT: function(val, state) {
            context["field"] = "mobilePhoneNumberCT";
            context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberCT"] : null);
            state['mobilePhoneNumberCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        mobilePhoneNumberBW: function(val, state) {
            context["field"] = "mobilePhoneNumberBW";
            context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberBW"] : null);
            state['mobilePhoneNumberBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        mobilePhoneNumberMT: function(val, state) {
            context["field"] = "mobilePhoneNumberMT";
            context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberMT"] : null);
            state['mobilePhoneNumberMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        lastNameCT: function(val, state) {
            context["field"] = "lastNameCT";
            context["metadata"] = (objectMetadata ? objectMetadata["lastNameCT"] : null);
            state['lastNameCT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        lastNameBW: function(val, state) {
            context["field"] = "lastNameBW";
            context["metadata"] = (objectMetadata ? objectMetadata["lastNameBW"] : null);
            state['lastNameBW'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        lastNameMT: function(val, state) {
            context["field"] = "lastNameMT";
            context["metadata"] = (objectMetadata ? objectMetadata["lastNameMT"] : null);
            state['lastNameMT'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        emailAddress: function(val, state) {
            context["field"] = "emailAddress";
            context["metadata"] = (objectMetadata ? objectMetadata["emailAddress"] : null);
            state['emailAddress'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        accountOfficer: function(val, state) {
            context["field"] = "accountOfficer";
            context["metadata"] = (objectMetadata ? objectMetadata["accountOfficer"] : null);
            state['accountOfficer'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        page_start: function(val, state) {
            context["field"] = "page_start";
            context["metadata"] = (objectMetadata ? objectMetadata["page_start"] : null);
            state['page_start'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        count: function(val, state) {
            context["field"] = "count";
            context["metadata"] = (objectMetadata ? objectMetadata["count"] : null);
            state['count'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        PhoneNo: function(val, state) {
            context["field"] = "PhoneNo";
            context["metadata"] = (objectMetadata ? objectMetadata["PhoneNo"] : null);
            state['PhoneNo'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };
    //Create the Model Class
    function customerObject(defaultValues) {
        var privateState = {};
        context["field"] = "firstName";
        context["metadata"] = (objectMetadata ? objectMetadata["firstName"] : null);
        privateState.firstName = defaultValues ? (defaultValues["firstName"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["firstName"], context) : null) : null;
        context["field"] = "lastName";
        context["metadata"] = (objectMetadata ? objectMetadata["lastName"] : null);
        privateState.lastName = defaultValues ? (defaultValues["lastName"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["lastName"], context) : null) : null;
        context["field"] = "dateOfBirth";
        context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirth"] : null);
        privateState.dateOfBirth = defaultValues ? (defaultValues["dateOfBirth"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["dateOfBirth"], context) : null) : null;
        context["field"] = "email";
        context["metadata"] = (objectMetadata ? objectMetadata["email"] : null);
        privateState.email = defaultValues ? (defaultValues["email"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["email"], context) : null) : null;
        context["field"] = "accountOfficerId";
        context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerId"] : null);
        privateState.accountOfficerId = defaultValues ? (defaultValues["accountOfficerId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountOfficerId"], context) : null) : null;
        context["field"] = "customerId";
        context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
        privateState.customerId = defaultValues ? (defaultValues["customerId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerId"], context) : null) : null;
        context["field"] = "location";
        context["metadata"] = (objectMetadata ? objectMetadata["location"] : null);
        privateState.location = defaultValues ? (defaultValues["location"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["location"], context) : null) : null;
        context["field"] = "customerName";
        context["metadata"] = (objectMetadata ? objectMetadata["customerName"] : null);
        privateState.customerName = defaultValues ? (defaultValues["customerName"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerName"], context) : null) : null;
        context["field"] = "maritalStatus";
        context["metadata"] = (objectMetadata ? objectMetadata["maritalStatus"] : null);
        privateState.maritalStatus = defaultValues ? (defaultValues["maritalStatus"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["maritalStatus"], context) : null) : null;
        context["field"] = "nationality";
        context["metadata"] = (objectMetadata ? objectMetadata["nationality"] : null);
        privateState.nationality = defaultValues ? (defaultValues["nationality"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["nationality"], context) : null) : null;
        context["field"] = "residencePhoneNo";
        context["metadata"] = (objectMetadata ? objectMetadata["residencePhoneNo"] : null);
        privateState.residencePhoneNo = defaultValues ? (defaultValues["residencePhoneNo"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["residencePhoneNo"], context) : null) : null;
        context["field"] = "officePhoneNo";
        context["metadata"] = (objectMetadata ? objectMetadata["officePhoneNo"] : null);
        privateState.officePhoneNo = defaultValues ? (defaultValues["officePhoneNo"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["officePhoneNo"], context) : null) : null;
        context["field"] = "primaryAddress";
        context["metadata"] = (objectMetadata ? objectMetadata["primaryAddress"] : null);
        privateState.primaryAddress = defaultValues ? (defaultValues["primaryAddress"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["primaryAddress"], context) : null) : null;
        context["field"] = "secondaryAddress";
        context["metadata"] = (objectMetadata ? objectMetadata["secondaryAddress"] : null);
        privateState.secondaryAddress = defaultValues ? (defaultValues["secondaryAddress"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["secondaryAddress"], context) : null) : null;
        context["field"] = "preferredChannel";
        context["metadata"] = (objectMetadata ? objectMetadata["preferredChannel"] : null);
        privateState.preferredChannel = defaultValues ? (defaultValues["preferredChannel"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["preferredChannel"], context) : null) : null;
        context["field"] = "title";
        context["metadata"] = (objectMetadata ? objectMetadata["title"] : null);
        privateState.title = defaultValues ? (defaultValues["title"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["title"], context) : null) : null;
        context["field"] = "residence";
        context["metadata"] = (objectMetadata ? objectMetadata["residence"] : null);
        privateState.residence = defaultValues ? (defaultValues["residence"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["residence"], context) : null) : null;
        context["field"] = "gender";
        context["metadata"] = (objectMetadata ? objectMetadata["gender"] : null);
        privateState.gender = defaultValues ? (defaultValues["gender"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["gender"], context) : null) : null;
        context["field"] = "fax";
        context["metadata"] = (objectMetadata ? objectMetadata["fax"] : null);
        privateState.fax = defaultValues ? (defaultValues["fax"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["fax"], context) : null) : null;
        context["field"] = "customerIdCT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerIdCT"] : null);
        privateState.customerIdCT = defaultValues ? (defaultValues["customerIdCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerIdCT"], context) : null) : null;
        context["field"] = "customerIdBW";
        context["metadata"] = (objectMetadata ? objectMetadata["customerIdBW"] : null);
        privateState.customerIdBW = defaultValues ? (defaultValues["customerIdBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerIdBW"], context) : null) : null;
        context["field"] = "customerIdMT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerIdMT"] : null);
        privateState.customerIdMT = defaultValues ? (defaultValues["customerIdMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerIdMT"], context) : null) : null;
        context["field"] = "customerNameCT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerNameCT"] : null);
        privateState.customerNameCT = defaultValues ? (defaultValues["customerNameCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerNameCT"], context) : null) : null;
        context["field"] = "customerNameBW";
        context["metadata"] = (objectMetadata ? objectMetadata["customerNameBW"] : null);
        privateState.customerNameBW = defaultValues ? (defaultValues["customerNameBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerNameBW"], context) : null) : null;
        context["field"] = "customerNameMT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerNameMT"] : null);
        privateState.customerNameMT = defaultValues ? (defaultValues["customerNameMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerNameMT"], context) : null) : null;
        context["field"] = "street";
        context["metadata"] = (objectMetadata ? objectMetadata["street"] : null);
        privateState.street = defaultValues ? (defaultValues["street"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["street"], context) : null) : null;
        context["field"] = "streetCT";
        context["metadata"] = (objectMetadata ? objectMetadata["streetCT"] : null);
        privateState.streetCT = defaultValues ? (defaultValues["streetCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["streetCT"], context) : null) : null;
        context["field"] = "streetBW";
        context["metadata"] = (objectMetadata ? objectMetadata["streetBW"] : null);
        privateState.streetBW = defaultValues ? (defaultValues["streetBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["streetBW"], context) : null) : null;
        context["field"] = "streetMT";
        context["metadata"] = (objectMetadata ? objectMetadata["streetMT"] : null);
        privateState.streetMT = defaultValues ? (defaultValues["streetMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["streetMT"], context) : null) : null;
        context["field"] = "addressCity";
        context["metadata"] = (objectMetadata ? objectMetadata["addressCity"] : null);
        privateState.addressCity = defaultValues ? (defaultValues["addressCity"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["addressCity"], context) : null) : null;
        context["field"] = "addressCityCT";
        context["metadata"] = (objectMetadata ? objectMetadata["addressCityCT"] : null);
        privateState.addressCityCT = defaultValues ? (defaultValues["addressCityCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["addressCityCT"], context) : null) : null;
        context["field"] = "addressCityBW";
        context["metadata"] = (objectMetadata ? objectMetadata["addressCityBW"] : null);
        privateState.addressCityBW = defaultValues ? (defaultValues["addressCityBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["addressCityBW"], context) : null) : null;
        context["field"] = "addressCityMT";
        context["metadata"] = (objectMetadata ? objectMetadata["addressCityMT"] : null);
        privateState.addressCityMT = defaultValues ? (defaultValues["addressCityMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["addressCityMT"], context) : null) : null;
        context["field"] = "countryId";
        context["metadata"] = (objectMetadata ? objectMetadata["countryId"] : null);
        privateState.countryId = defaultValues ? (defaultValues["countryId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["countryId"], context) : null) : null;
        context["field"] = "countryIdCT";
        context["metadata"] = (objectMetadata ? objectMetadata["countryIdCT"] : null);
        privateState.countryIdCT = defaultValues ? (defaultValues["countryIdCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["countryIdCT"], context) : null) : null;
        context["field"] = "countryIdBW";
        context["metadata"] = (objectMetadata ? objectMetadata["countryIdBW"] : null);
        privateState.countryIdBW = defaultValues ? (defaultValues["countryIdBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["countryIdBW"], context) : null) : null;
        context["field"] = "countryIdMT";
        context["metadata"] = (objectMetadata ? objectMetadata["countryIdMT"] : null);
        privateState.countryIdMT = defaultValues ? (defaultValues["countryIdMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["countryIdMT"], context) : null) : null;
        context["field"] = "postCode";
        context["metadata"] = (objectMetadata ? objectMetadata["postCode"] : null);
        privateState.postCode = defaultValues ? (defaultValues["postCode"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["postCode"], context) : null) : null;
        context["field"] = "postCodeCT";
        context["metadata"] = (objectMetadata ? objectMetadata["postCodeCT"] : null);
        privateState.postCodeCT = defaultValues ? (defaultValues["postCodeCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["postCodeCT"], context) : null) : null;
        context["field"] = "postCodeBW";
        context["metadata"] = (objectMetadata ? objectMetadata["postCodeBW"] : null);
        privateState.postCodeBW = defaultValues ? (defaultValues["postCodeBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["postCodeBW"], context) : null) : null;
        context["field"] = "postCodeMT";
        context["metadata"] = (objectMetadata ? objectMetadata["postCodeMT"] : null);
        privateState.postCodeMT = defaultValues ? (defaultValues["postCodeMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["postCodeMT"], context) : null) : null;
        context["field"] = "dateOfBirthCT";
        context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthCT"] : null);
        privateState.dateOfBirthCT = defaultValues ? (defaultValues["dateOfBirthCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["dateOfBirthCT"], context) : null) : null;
        context["field"] = "dateOfBirthBW";
        context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthBW"] : null);
        privateState.dateOfBirthBW = defaultValues ? (defaultValues["dateOfBirthBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["dateOfBirthBW"], context) : null) : null;
        context["field"] = "dateOfBirthMT";
        context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthMT"] : null);
        privateState.dateOfBirthMT = defaultValues ? (defaultValues["dateOfBirthMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["dateOfBirthMT"], context) : null) : null;
        context["field"] = "customerStatus";
        context["metadata"] = (objectMetadata ? objectMetadata["customerStatus"] : null);
        privateState.customerStatus = defaultValues ? (defaultValues["customerStatus"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerStatus"], context) : null) : null;
        context["field"] = "customerStatusCT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerStatusCT"] : null);
        privateState.customerStatusCT = defaultValues ? (defaultValues["customerStatusCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerStatusCT"], context) : null) : null;
        context["field"] = "customerStatusBW";
        context["metadata"] = (objectMetadata ? objectMetadata["customerStatusBW"] : null);
        privateState.customerStatusBW = defaultValues ? (defaultValues["customerStatusBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerStatusBW"], context) : null) : null;
        context["field"] = "customerStatusMT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerStatusMT"] : null);
        privateState.customerStatusMT = defaultValues ? (defaultValues["customerStatusMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerStatusMT"], context) : null) : null;
        context["field"] = "sectorId";
        context["metadata"] = (objectMetadata ? objectMetadata["sectorId"] : null);
        privateState.sectorId = defaultValues ? (defaultValues["sectorId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["sectorId"], context) : null) : null;
        context["field"] = "sectorIdCT";
        context["metadata"] = (objectMetadata ? objectMetadata["sectorIdCT"] : null);
        privateState.sectorIdCT = defaultValues ? (defaultValues["sectorIdCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["sectorIdCT"], context) : null) : null;
        context["field"] = "sectorIdBW";
        context["metadata"] = (objectMetadata ? objectMetadata["sectorIdBW"] : null);
        privateState.sectorIdBW = defaultValues ? (defaultValues["sectorIdBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["sectorIdBW"], context) : null) : null;
        context["field"] = "sectorIdMT";
        context["metadata"] = (objectMetadata ? objectMetadata["sectorIdMT"] : null);
        privateState.sectorIdMT = defaultValues ? (defaultValues["sectorIdMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["sectorIdMT"], context) : null) : null;
        context["field"] = "accountOfficerIdCT";
        context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdCT"] : null);
        privateState.accountOfficerIdCT = defaultValues ? (defaultValues["accountOfficerIdCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountOfficerIdCT"], context) : null) : null;
        context["field"] = "accountOfficerIdBW";
        context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdBW"] : null);
        privateState.accountOfficerIdBW = defaultValues ? (defaultValues["accountOfficerIdBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountOfficerIdBW"], context) : null) : null;
        context["field"] = "accountOfficerIdMT";
        context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdMT"] : null);
        privateState.accountOfficerIdMT = defaultValues ? (defaultValues["accountOfficerIdMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountOfficerIdMT"], context) : null) : null;
        context["field"] = "industryId";
        context["metadata"] = (objectMetadata ? objectMetadata["industryId"] : null);
        privateState.industryId = defaultValues ? (defaultValues["industryId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["industryId"], context) : null) : null;
        context["field"] = "industryIdCT";
        context["metadata"] = (objectMetadata ? objectMetadata["industryIdCT"] : null);
        privateState.industryIdCT = defaultValues ? (defaultValues["industryIdCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["industryIdCT"], context) : null) : null;
        context["field"] = "industryIdBW";
        context["metadata"] = (objectMetadata ? objectMetadata["industryIdBW"] : null);
        privateState.industryIdBW = defaultValues ? (defaultValues["industryIdBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["industryIdBW"], context) : null) : null;
        context["field"] = "industryIdMT";
        context["metadata"] = (objectMetadata ? objectMetadata["industryIdMT"] : null);
        privateState.industryIdMT = defaultValues ? (defaultValues["industryIdMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["industryIdMT"], context) : null) : null;
        context["field"] = "nationalityId";
        context["metadata"] = (objectMetadata ? objectMetadata["nationalityId"] : null);
        privateState.nationalityId = defaultValues ? (defaultValues["nationalityId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["nationalityId"], context) : null) : null;
        context["field"] = "nationalityIdCT";
        context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdCT"] : null);
        privateState.nationalityIdCT = defaultValues ? (defaultValues["nationalityIdCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["nationalityIdCT"], context) : null) : null;
        context["field"] = "nationalityIdBW";
        context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdBW"] : null);
        privateState.nationalityIdBW = defaultValues ? (defaultValues["nationalityIdBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["nationalityIdBW"], context) : null) : null;
        context["field"] = "nationalityIdMT";
        context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdMT"] : null);
        privateState.nationalityIdMT = defaultValues ? (defaultValues["nationalityIdMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["nationalityIdMT"], context) : null) : null;
        context["field"] = "residenceId";
        context["metadata"] = (objectMetadata ? objectMetadata["residenceId"] : null);
        privateState.residenceId = defaultValues ? (defaultValues["residenceId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["residenceId"], context) : null) : null;
        context["field"] = "residenceIdCT";
        context["metadata"] = (objectMetadata ? objectMetadata["residenceIdCT"] : null);
        privateState.residenceIdCT = defaultValues ? (defaultValues["residenceIdCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["residenceIdCT"], context) : null) : null;
        context["field"] = "residenceIdBW";
        context["metadata"] = (objectMetadata ? objectMetadata["residenceIdBW"] : null);
        privateState.residenceIdBW = defaultValues ? (defaultValues["residenceIdBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["residenceIdBW"], context) : null) : null;
        context["field"] = "residenceIdMT";
        context["metadata"] = (objectMetadata ? objectMetadata["residenceIdMT"] : null);
        privateState.residenceIdMT = defaultValues ? (defaultValues["residenceIdMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["residenceIdMT"], context) : null) : null;
        context["field"] = "phoneNumber";
        context["metadata"] = (objectMetadata ? objectMetadata["phoneNumber"] : null);
        privateState.phoneNumber = defaultValues ? (defaultValues["phoneNumber"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["phoneNumber"], context) : null) : null;
        context["field"] = "phoneNumberCT";
        context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberCT"] : null);
        privateState.phoneNumberCT = defaultValues ? (defaultValues["phoneNumberCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["phoneNumberCT"], context) : null) : null;
        context["field"] = "phoneNumberBW";
        context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberBW"] : null);
        privateState.phoneNumberBW = defaultValues ? (defaultValues["phoneNumberBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["phoneNumberBW"], context) : null) : null;
        context["field"] = "phoneNumberMT";
        context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberMT"] : null);
        privateState.phoneNumberMT = defaultValues ? (defaultValues["phoneNumberMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["phoneNumberMT"], context) : null) : null;
        context["field"] = "customerMnemonic";
        context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonic"] : null);
        privateState.customerMnemonic = defaultValues ? (defaultValues["customerMnemonic"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerMnemonic"], context) : null) : null;
        context["field"] = "customerMnemonicCT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicCT"] : null);
        privateState.customerMnemonicCT = defaultValues ? (defaultValues["customerMnemonicCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerMnemonicCT"], context) : null) : null;
        context["field"] = "customerMnemonicBW";
        context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicBW"] : null);
        privateState.customerMnemonicBW = defaultValues ? (defaultValues["customerMnemonicBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerMnemonicBW"], context) : null) : null;
        context["field"] = "customerMnemonicMT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicMT"] : null);
        privateState.customerMnemonicMT = defaultValues ? (defaultValues["customerMnemonicMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerMnemonicMT"], context) : null) : null;
        context["field"] = "firstNameCT";
        context["metadata"] = (objectMetadata ? objectMetadata["firstNameCT"] : null);
        privateState.firstNameCT = defaultValues ? (defaultValues["firstNameCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["firstNameCT"], context) : null) : null;
        context["field"] = "firstNameBW";
        context["metadata"] = (objectMetadata ? objectMetadata["firstNameBW"] : null);
        privateState.firstNameBW = defaultValues ? (defaultValues["firstNameBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["firstNameBW"], context) : null) : null;
        context["field"] = "firstNameMT";
        context["metadata"] = (objectMetadata ? objectMetadata["firstNameMT"] : null);
        privateState.firstNameMT = defaultValues ? (defaultValues["firstNameMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["firstNameMT"], context) : null) : null;
        context["field"] = "customerType";
        context["metadata"] = (objectMetadata ? objectMetadata["customerType"] : null);
        privateState.customerType = defaultValues ? (defaultValues["customerType"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerType"], context) : null) : null;
        context["field"] = "customerTypeCT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerTypeCT"] : null);
        privateState.customerTypeCT = defaultValues ? (defaultValues["customerTypeCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerTypeCT"], context) : null) : null;
        context["field"] = "customerTypeBW";
        context["metadata"] = (objectMetadata ? objectMetadata["customerTypeBW"] : null);
        privateState.customerTypeBW = defaultValues ? (defaultValues["customerTypeBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerTypeBW"], context) : null) : null;
        context["field"] = "customerTypeMT";
        context["metadata"] = (objectMetadata ? objectMetadata["customerTypeMT"] : null);
        privateState.customerTypeMT = defaultValues ? (defaultValues["customerTypeMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerTypeMT"], context) : null) : null;
        context["field"] = "emailCT";
        context["metadata"] = (objectMetadata ? objectMetadata["emailCT"] : null);
        privateState.emailCT = defaultValues ? (defaultValues["emailCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["emailCT"], context) : null) : null;
        context["field"] = "emailBW";
        context["metadata"] = (objectMetadata ? objectMetadata["emailBW"] : null);
        privateState.emailBW = defaultValues ? (defaultValues["emailBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["emailBW"], context) : null) : null;
        context["field"] = "emailMT";
        context["metadata"] = (objectMetadata ? objectMetadata["emailMT"] : null);
        privateState.emailMT = defaultValues ? (defaultValues["emailMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["emailMT"], context) : null) : null;
        context["field"] = "mobilePhoneNumber";
        context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumber"] : null);
        privateState.mobilePhoneNumber = defaultValues ? (defaultValues["mobilePhoneNumber"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["mobilePhoneNumber"], context) : null) : null;
        context["field"] = "mobilePhoneNumberCT";
        context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberCT"] : null);
        privateState.mobilePhoneNumberCT = defaultValues ? (defaultValues["mobilePhoneNumberCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["mobilePhoneNumberCT"], context) : null) : null;
        context["field"] = "mobilePhoneNumberBW";
        context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberBW"] : null);
        privateState.mobilePhoneNumberBW = defaultValues ? (defaultValues["mobilePhoneNumberBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["mobilePhoneNumberBW"], context) : null) : null;
        context["field"] = "mobilePhoneNumberMT";
        context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberMT"] : null);
        privateState.mobilePhoneNumberMT = defaultValues ? (defaultValues["mobilePhoneNumberMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["mobilePhoneNumberMT"], context) : null) : null;
        context["field"] = "lastNameCT";
        context["metadata"] = (objectMetadata ? objectMetadata["lastNameCT"] : null);
        privateState.lastNameCT = defaultValues ? (defaultValues["lastNameCT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["lastNameCT"], context) : null) : null;
        context["field"] = "lastNameBW";
        context["metadata"] = (objectMetadata ? objectMetadata["lastNameBW"] : null);
        privateState.lastNameBW = defaultValues ? (defaultValues["lastNameBW"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["lastNameBW"], context) : null) : null;
        context["field"] = "lastNameMT";
        context["metadata"] = (objectMetadata ? objectMetadata["lastNameMT"] : null);
        privateState.lastNameMT = defaultValues ? (defaultValues["lastNameMT"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["lastNameMT"], context) : null) : null;
        context["field"] = "emailAddress";
        context["metadata"] = (objectMetadata ? objectMetadata["emailAddress"] : null);
        privateState.emailAddress = defaultValues ? (defaultValues["emailAddress"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["emailAddress"], context) : null) : null;
        context["field"] = "accountOfficer";
        context["metadata"] = (objectMetadata ? objectMetadata["accountOfficer"] : null);
        privateState.accountOfficer = defaultValues ? (defaultValues["accountOfficer"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["accountOfficer"], context) : null) : null;
        context["field"] = "page_start";
        context["metadata"] = (objectMetadata ? objectMetadata["page_start"] : null);
        privateState.page_start = defaultValues ? (defaultValues["page_start"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["page_start"], context) : null) : null;
        context["field"] = "count";
        context["metadata"] = (objectMetadata ? objectMetadata["count"] : null);
        privateState.count = defaultValues ? (defaultValues["count"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["count"], context) : null) : null;
        context["field"] = "PhoneNo";
        context["metadata"] = (objectMetadata ? objectMetadata["PhoneNo"] : null);
        privateState.PhoneNo = defaultValues ? (defaultValues["PhoneNo"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["PhoneNo"], context) : null) : null;
        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);
        //Defining Getter/Setters
        Object.defineProperties(this, {
            "firstName": {
                get: function() {
                    context["field"] = "firstName";
                    context["metadata"] = (objectMetadata ? objectMetadata["firstName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.firstName, context);
                },
                set: function(val) {
                    setterFunctions['firstName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "lastName": {
                get: function() {
                    context["field"] = "lastName";
                    context["metadata"] = (objectMetadata ? objectMetadata["lastName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.lastName, context);
                },
                set: function(val) {
                    setterFunctions['lastName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "dateOfBirth": {
                get: function() {
                    context["field"] = "dateOfBirth";
                    context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirth"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.dateOfBirth, context);
                },
                set: function(val) {
                    setterFunctions['dateOfBirth'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "email": {
                get: function() {
                    context["field"] = "email";
                    context["metadata"] = (objectMetadata ? objectMetadata["email"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.email, context);
                },
                set: function(val) {
                    setterFunctions['email'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "accountOfficerId": {
                get: function() {
                    context["field"] = "accountOfficerId";
                    context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountOfficerId, context);
                },
                set: function(val) {
                    setterFunctions['accountOfficerId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerId": {
                get: function() {
                    context["field"] = "customerId";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerId, context);
                },
                set: function(val) {
                    setterFunctions['customerId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "location": {
                get: function() {
                    context["field"] = "location";
                    context["metadata"] = (objectMetadata ? objectMetadata["location"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.location, context);
                },
                set: function(val) {
                    setterFunctions['location'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerName": {
                get: function() {
                    context["field"] = "customerName";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerName, context);
                },
                set: function(val) {
                    setterFunctions['customerName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "maritalStatus": {
                get: function() {
                    context["field"] = "maritalStatus";
                    context["metadata"] = (objectMetadata ? objectMetadata["maritalStatus"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.maritalStatus, context);
                },
                set: function(val) {
                    setterFunctions['maritalStatus'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "nationality": {
                get: function() {
                    context["field"] = "nationality";
                    context["metadata"] = (objectMetadata ? objectMetadata["nationality"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.nationality, context);
                },
                set: function(val) {
                    setterFunctions['nationality'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "residencePhoneNo": {
                get: function() {
                    context["field"] = "residencePhoneNo";
                    context["metadata"] = (objectMetadata ? objectMetadata["residencePhoneNo"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.residencePhoneNo, context);
                },
                set: function(val) {
                    setterFunctions['residencePhoneNo'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "officePhoneNo": {
                get: function() {
                    context["field"] = "officePhoneNo";
                    context["metadata"] = (objectMetadata ? objectMetadata["officePhoneNo"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.officePhoneNo, context);
                },
                set: function(val) {
                    setterFunctions['officePhoneNo'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "primaryAddress": {
                get: function() {
                    context["field"] = "primaryAddress";
                    context["metadata"] = (objectMetadata ? objectMetadata["primaryAddress"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.primaryAddress, context);
                },
                set: function(val) {
                    setterFunctions['primaryAddress'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "secondaryAddress": {
                get: function() {
                    context["field"] = "secondaryAddress";
                    context["metadata"] = (objectMetadata ? objectMetadata["secondaryAddress"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.secondaryAddress, context);
                },
                set: function(val) {
                    setterFunctions['secondaryAddress'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "preferredChannel": {
                get: function() {
                    context["field"] = "preferredChannel";
                    context["metadata"] = (objectMetadata ? objectMetadata["preferredChannel"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.preferredChannel, context);
                },
                set: function(val) {
                    setterFunctions['preferredChannel'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "title": {
                get: function() {
                    context["field"] = "title";
                    context["metadata"] = (objectMetadata ? objectMetadata["title"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.title, context);
                },
                set: function(val) {
                    setterFunctions['title'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "residence": {
                get: function() {
                    context["field"] = "residence";
                    context["metadata"] = (objectMetadata ? objectMetadata["residence"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.residence, context);
                },
                set: function(val) {
                    setterFunctions['residence'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "gender": {
                get: function() {
                    context["field"] = "gender";
                    context["metadata"] = (objectMetadata ? objectMetadata["gender"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.gender, context);
                },
                set: function(val) {
                    setterFunctions['gender'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "fax": {
                get: function() {
                    context["field"] = "fax";
                    context["metadata"] = (objectMetadata ? objectMetadata["fax"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.fax, context);
                },
                set: function(val) {
                    setterFunctions['fax'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerIdCT": {
                get: function() {
                    context["field"] = "customerIdCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerIdCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerIdCT, context);
                },
                set: function(val) {
                    setterFunctions['customerIdCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerIdBW": {
                get: function() {
                    context["field"] = "customerIdBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerIdBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerIdBW, context);
                },
                set: function(val) {
                    setterFunctions['customerIdBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerIdMT": {
                get: function() {
                    context["field"] = "customerIdMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerIdMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerIdMT, context);
                },
                set: function(val) {
                    setterFunctions['customerIdMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerNameCT": {
                get: function() {
                    context["field"] = "customerNameCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerNameCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerNameCT, context);
                },
                set: function(val) {
                    setterFunctions['customerNameCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerNameBW": {
                get: function() {
                    context["field"] = "customerNameBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerNameBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerNameBW, context);
                },
                set: function(val) {
                    setterFunctions['customerNameBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerNameMT": {
                get: function() {
                    context["field"] = "customerNameMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerNameMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerNameMT, context);
                },
                set: function(val) {
                    setterFunctions['customerNameMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "street": {
                get: function() {
                    context["field"] = "street";
                    context["metadata"] = (objectMetadata ? objectMetadata["street"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.street, context);
                },
                set: function(val) {
                    setterFunctions['street'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "streetCT": {
                get: function() {
                    context["field"] = "streetCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["streetCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.streetCT, context);
                },
                set: function(val) {
                    setterFunctions['streetCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "streetBW": {
                get: function() {
                    context["field"] = "streetBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["streetBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.streetBW, context);
                },
                set: function(val) {
                    setterFunctions['streetBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "streetMT": {
                get: function() {
                    context["field"] = "streetMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["streetMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.streetMT, context);
                },
                set: function(val) {
                    setterFunctions['streetMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "addressCity": {
                get: function() {
                    context["field"] = "addressCity";
                    context["metadata"] = (objectMetadata ? objectMetadata["addressCity"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.addressCity, context);
                },
                set: function(val) {
                    setterFunctions['addressCity'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "addressCityCT": {
                get: function() {
                    context["field"] = "addressCityCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["addressCityCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.addressCityCT, context);
                },
                set: function(val) {
                    setterFunctions['addressCityCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "addressCityBW": {
                get: function() {
                    context["field"] = "addressCityBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["addressCityBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.addressCityBW, context);
                },
                set: function(val) {
                    setterFunctions['addressCityBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "addressCityMT": {
                get: function() {
                    context["field"] = "addressCityMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["addressCityMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.addressCityMT, context);
                },
                set: function(val) {
                    setterFunctions['addressCityMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "countryId": {
                get: function() {
                    context["field"] = "countryId";
                    context["metadata"] = (objectMetadata ? objectMetadata["countryId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.countryId, context);
                },
                set: function(val) {
                    setterFunctions['countryId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "countryIdCT": {
                get: function() {
                    context["field"] = "countryIdCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["countryIdCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.countryIdCT, context);
                },
                set: function(val) {
                    setterFunctions['countryIdCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "countryIdBW": {
                get: function() {
                    context["field"] = "countryIdBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["countryIdBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.countryIdBW, context);
                },
                set: function(val) {
                    setterFunctions['countryIdBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "countryIdMT": {
                get: function() {
                    context["field"] = "countryIdMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["countryIdMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.countryIdMT, context);
                },
                set: function(val) {
                    setterFunctions['countryIdMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "postCode": {
                get: function() {
                    context["field"] = "postCode";
                    context["metadata"] = (objectMetadata ? objectMetadata["postCode"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.postCode, context);
                },
                set: function(val) {
                    setterFunctions['postCode'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "postCodeCT": {
                get: function() {
                    context["field"] = "postCodeCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["postCodeCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.postCodeCT, context);
                },
                set: function(val) {
                    setterFunctions['postCodeCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "postCodeBW": {
                get: function() {
                    context["field"] = "postCodeBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["postCodeBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.postCodeBW, context);
                },
                set: function(val) {
                    setterFunctions['postCodeBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "postCodeMT": {
                get: function() {
                    context["field"] = "postCodeMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["postCodeMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.postCodeMT, context);
                },
                set: function(val) {
                    setterFunctions['postCodeMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "dateOfBirthCT": {
                get: function() {
                    context["field"] = "dateOfBirthCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.dateOfBirthCT, context);
                },
                set: function(val) {
                    setterFunctions['dateOfBirthCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "dateOfBirthBW": {
                get: function() {
                    context["field"] = "dateOfBirthBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.dateOfBirthBW, context);
                },
                set: function(val) {
                    setterFunctions['dateOfBirthBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "dateOfBirthMT": {
                get: function() {
                    context["field"] = "dateOfBirthMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["dateOfBirthMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.dateOfBirthMT, context);
                },
                set: function(val) {
                    setterFunctions['dateOfBirthMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerStatus": {
                get: function() {
                    context["field"] = "customerStatus";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerStatus"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerStatus, context);
                },
                set: function(val) {
                    setterFunctions['customerStatus'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerStatusCT": {
                get: function() {
                    context["field"] = "customerStatusCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerStatusCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerStatusCT, context);
                },
                set: function(val) {
                    setterFunctions['customerStatusCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerStatusBW": {
                get: function() {
                    context["field"] = "customerStatusBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerStatusBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerStatusBW, context);
                },
                set: function(val) {
                    setterFunctions['customerStatusBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerStatusMT": {
                get: function() {
                    context["field"] = "customerStatusMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerStatusMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerStatusMT, context);
                },
                set: function(val) {
                    setterFunctions['customerStatusMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "sectorId": {
                get: function() {
                    context["field"] = "sectorId";
                    context["metadata"] = (objectMetadata ? objectMetadata["sectorId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.sectorId, context);
                },
                set: function(val) {
                    setterFunctions['sectorId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "sectorIdCT": {
                get: function() {
                    context["field"] = "sectorIdCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["sectorIdCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.sectorIdCT, context);
                },
                set: function(val) {
                    setterFunctions['sectorIdCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "sectorIdBW": {
                get: function() {
                    context["field"] = "sectorIdBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["sectorIdBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.sectorIdBW, context);
                },
                set: function(val) {
                    setterFunctions['sectorIdBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "sectorIdMT": {
                get: function() {
                    context["field"] = "sectorIdMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["sectorIdMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.sectorIdMT, context);
                },
                set: function(val) {
                    setterFunctions['sectorIdMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "accountOfficerIdCT": {
                get: function() {
                    context["field"] = "accountOfficerIdCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountOfficerIdCT, context);
                },
                set: function(val) {
                    setterFunctions['accountOfficerIdCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "accountOfficerIdBW": {
                get: function() {
                    context["field"] = "accountOfficerIdBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountOfficerIdBW, context);
                },
                set: function(val) {
                    setterFunctions['accountOfficerIdBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "accountOfficerIdMT": {
                get: function() {
                    context["field"] = "accountOfficerIdMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["accountOfficerIdMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountOfficerIdMT, context);
                },
                set: function(val) {
                    setterFunctions['accountOfficerIdMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "industryId": {
                get: function() {
                    context["field"] = "industryId";
                    context["metadata"] = (objectMetadata ? objectMetadata["industryId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.industryId, context);
                },
                set: function(val) {
                    setterFunctions['industryId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "industryIdCT": {
                get: function() {
                    context["field"] = "industryIdCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["industryIdCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.industryIdCT, context);
                },
                set: function(val) {
                    setterFunctions['industryIdCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "industryIdBW": {
                get: function() {
                    context["field"] = "industryIdBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["industryIdBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.industryIdBW, context);
                },
                set: function(val) {
                    setterFunctions['industryIdBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "industryIdMT": {
                get: function() {
                    context["field"] = "industryIdMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["industryIdMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.industryIdMT, context);
                },
                set: function(val) {
                    setterFunctions['industryIdMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "nationalityId": {
                get: function() {
                    context["field"] = "nationalityId";
                    context["metadata"] = (objectMetadata ? objectMetadata["nationalityId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.nationalityId, context);
                },
                set: function(val) {
                    setterFunctions['nationalityId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "nationalityIdCT": {
                get: function() {
                    context["field"] = "nationalityIdCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.nationalityIdCT, context);
                },
                set: function(val) {
                    setterFunctions['nationalityIdCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "nationalityIdBW": {
                get: function() {
                    context["field"] = "nationalityIdBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.nationalityIdBW, context);
                },
                set: function(val) {
                    setterFunctions['nationalityIdBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "nationalityIdMT": {
                get: function() {
                    context["field"] = "nationalityIdMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["nationalityIdMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.nationalityIdMT, context);
                },
                set: function(val) {
                    setterFunctions['nationalityIdMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "residenceId": {
                get: function() {
                    context["field"] = "residenceId";
                    context["metadata"] = (objectMetadata ? objectMetadata["residenceId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.residenceId, context);
                },
                set: function(val) {
                    setterFunctions['residenceId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "residenceIdCT": {
                get: function() {
                    context["field"] = "residenceIdCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["residenceIdCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.residenceIdCT, context);
                },
                set: function(val) {
                    setterFunctions['residenceIdCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "residenceIdBW": {
                get: function() {
                    context["field"] = "residenceIdBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["residenceIdBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.residenceIdBW, context);
                },
                set: function(val) {
                    setterFunctions['residenceIdBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "residenceIdMT": {
                get: function() {
                    context["field"] = "residenceIdMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["residenceIdMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.residenceIdMT, context);
                },
                set: function(val) {
                    setterFunctions['residenceIdMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "phoneNumber": {
                get: function() {
                    context["field"] = "phoneNumber";
                    context["metadata"] = (objectMetadata ? objectMetadata["phoneNumber"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.phoneNumber, context);
                },
                set: function(val) {
                    setterFunctions['phoneNumber'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "phoneNumberCT": {
                get: function() {
                    context["field"] = "phoneNumberCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.phoneNumberCT, context);
                },
                set: function(val) {
                    setterFunctions['phoneNumberCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "phoneNumberBW": {
                get: function() {
                    context["field"] = "phoneNumberBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.phoneNumberBW, context);
                },
                set: function(val) {
                    setterFunctions['phoneNumberBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "phoneNumberMT": {
                get: function() {
                    context["field"] = "phoneNumberMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["phoneNumberMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.phoneNumberMT, context);
                },
                set: function(val) {
                    setterFunctions['phoneNumberMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerMnemonic": {
                get: function() {
                    context["field"] = "customerMnemonic";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonic"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerMnemonic, context);
                },
                set: function(val) {
                    setterFunctions['customerMnemonic'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerMnemonicCT": {
                get: function() {
                    context["field"] = "customerMnemonicCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerMnemonicCT, context);
                },
                set: function(val) {
                    setterFunctions['customerMnemonicCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerMnemonicBW": {
                get: function() {
                    context["field"] = "customerMnemonicBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerMnemonicBW, context);
                },
                set: function(val) {
                    setterFunctions['customerMnemonicBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerMnemonicMT": {
                get: function() {
                    context["field"] = "customerMnemonicMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerMnemonicMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerMnemonicMT, context);
                },
                set: function(val) {
                    setterFunctions['customerMnemonicMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "firstNameCT": {
                get: function() {
                    context["field"] = "firstNameCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["firstNameCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.firstNameCT, context);
                },
                set: function(val) {
                    setterFunctions['firstNameCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "firstNameBW": {
                get: function() {
                    context["field"] = "firstNameBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["firstNameBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.firstNameBW, context);
                },
                set: function(val) {
                    setterFunctions['firstNameBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "firstNameMT": {
                get: function() {
                    context["field"] = "firstNameMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["firstNameMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.firstNameMT, context);
                },
                set: function(val) {
                    setterFunctions['firstNameMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerType": {
                get: function() {
                    context["field"] = "customerType";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerType"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerType, context);
                },
                set: function(val) {
                    setterFunctions['customerType'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerTypeCT": {
                get: function() {
                    context["field"] = "customerTypeCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerTypeCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerTypeCT, context);
                },
                set: function(val) {
                    setterFunctions['customerTypeCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerTypeBW": {
                get: function() {
                    context["field"] = "customerTypeBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerTypeBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerTypeBW, context);
                },
                set: function(val) {
                    setterFunctions['customerTypeBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "customerTypeMT": {
                get: function() {
                    context["field"] = "customerTypeMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerTypeMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerTypeMT, context);
                },
                set: function(val) {
                    setterFunctions['customerTypeMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "emailCT": {
                get: function() {
                    context["field"] = "emailCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["emailCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.emailCT, context);
                },
                set: function(val) {
                    setterFunctions['emailCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "emailBW": {
                get: function() {
                    context["field"] = "emailBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["emailBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.emailBW, context);
                },
                set: function(val) {
                    setterFunctions['emailBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "emailMT": {
                get: function() {
                    context["field"] = "emailMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["emailMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.emailMT, context);
                },
                set: function(val) {
                    setterFunctions['emailMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "mobilePhoneNumber": {
                get: function() {
                    context["field"] = "mobilePhoneNumber";
                    context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumber"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.mobilePhoneNumber, context);
                },
                set: function(val) {
                    setterFunctions['mobilePhoneNumber'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "mobilePhoneNumberCT": {
                get: function() {
                    context["field"] = "mobilePhoneNumberCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.mobilePhoneNumberCT, context);
                },
                set: function(val) {
                    setterFunctions['mobilePhoneNumberCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "mobilePhoneNumberBW": {
                get: function() {
                    context["field"] = "mobilePhoneNumberBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.mobilePhoneNumberBW, context);
                },
                set: function(val) {
                    setterFunctions['mobilePhoneNumberBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "mobilePhoneNumberMT": {
                get: function() {
                    context["field"] = "mobilePhoneNumberMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["mobilePhoneNumberMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.mobilePhoneNumberMT, context);
                },
                set: function(val) {
                    setterFunctions['mobilePhoneNumberMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "lastNameCT": {
                get: function() {
                    context["field"] = "lastNameCT";
                    context["metadata"] = (objectMetadata ? objectMetadata["lastNameCT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.lastNameCT, context);
                },
                set: function(val) {
                    setterFunctions['lastNameCT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "lastNameBW": {
                get: function() {
                    context["field"] = "lastNameBW";
                    context["metadata"] = (objectMetadata ? objectMetadata["lastNameBW"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.lastNameBW, context);
                },
                set: function(val) {
                    setterFunctions['lastNameBW'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "lastNameMT": {
                get: function() {
                    context["field"] = "lastNameMT";
                    context["metadata"] = (objectMetadata ? objectMetadata["lastNameMT"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.lastNameMT, context);
                },
                set: function(val) {
                    setterFunctions['lastNameMT'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "emailAddress": {
                get: function() {
                    context["field"] = "emailAddress";
                    context["metadata"] = (objectMetadata ? objectMetadata["emailAddress"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.emailAddress, context);
                },
                set: function(val) {
                    setterFunctions['emailAddress'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "accountOfficer": {
                get: function() {
                    context["field"] = "accountOfficer";
                    context["metadata"] = (objectMetadata ? objectMetadata["accountOfficer"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.accountOfficer, context);
                },
                set: function(val) {
                    setterFunctions['accountOfficer'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "page_start": {
                get: function() {
                    context["field"] = "page_start";
                    context["metadata"] = (objectMetadata ? objectMetadata["page_start"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.page_start, context);
                },
                set: function(val) {
                    setterFunctions['page_start'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "count": {
                get: function() {
                    context["field"] = "count";
                    context["metadata"] = (objectMetadata ? objectMetadata["count"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.count, context);
                },
                set: function(val) {
                    setterFunctions['count'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "PhoneNo": {
                get: function() {
                    context["field"] = "PhoneNo";
                    context["metadata"] = (objectMetadata ? objectMetadata["PhoneNo"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.PhoneNo, context);
                },
                set: function(val) {
                    setterFunctions['PhoneNo'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });
        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };
        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.firstName = value ? (value["firstName"] ? value["firstName"] : null) : null;
            privateState.lastName = value ? (value["lastName"] ? value["lastName"] : null) : null;
            privateState.dateOfBirth = value ? (value["dateOfBirth"] ? value["dateOfBirth"] : null) : null;
            privateState.email = value ? (value["email"] ? value["email"] : null) : null;
            privateState.accountOfficerId = value ? (value["accountOfficerId"] ? value["accountOfficerId"] : null) : null;
            privateState.customerId = value ? (value["customerId"] ? value["customerId"] : null) : null;
            privateState.location = value ? (value["location"] ? value["location"] : null) : null;
            privateState.customerName = value ? (value["customerName"] ? value["customerName"] : null) : null;
            privateState.maritalStatus = value ? (value["maritalStatus"] ? value["maritalStatus"] : null) : null;
            privateState.nationality = value ? (value["nationality"] ? value["nationality"] : null) : null;
            privateState.residencePhoneNo = value ? (value["residencePhoneNo"] ? value["residencePhoneNo"] : null) : null;
            privateState.officePhoneNo = value ? (value["officePhoneNo"] ? value["officePhoneNo"] : null) : null;
            privateState.primaryAddress = value ? (value["primaryAddress"] ? value["primaryAddress"] : null) : null;
            privateState.secondaryAddress = value ? (value["secondaryAddress"] ? value["secondaryAddress"] : null) : null;
            privateState.preferredChannel = value ? (value["preferredChannel"] ? value["preferredChannel"] : null) : null;
            privateState.title = value ? (value["title"] ? value["title"] : null) : null;
            privateState.residence = value ? (value["residence"] ? value["residence"] : null) : null;
            privateState.gender = value ? (value["gender"] ? value["gender"] : null) : null;
            privateState.fax = value ? (value["fax"] ? value["fax"] : null) : null;
            privateState.customerIdCT = value ? (value["customerIdCT"] ? value["customerIdCT"] : null) : null;
            privateState.customerIdBW = value ? (value["customerIdBW"] ? value["customerIdBW"] : null) : null;
            privateState.customerIdMT = value ? (value["customerIdMT"] ? value["customerIdMT"] : null) : null;
            privateState.customerNameCT = value ? (value["customerNameCT"] ? value["customerNameCT"] : null) : null;
            privateState.customerNameBW = value ? (value["customerNameBW"] ? value["customerNameBW"] : null) : null;
            privateState.customerNameMT = value ? (value["customerNameMT"] ? value["customerNameMT"] : null) : null;
            privateState.street = value ? (value["street"] ? value["street"] : null) : null;
            privateState.streetCT = value ? (value["streetCT"] ? value["streetCT"] : null) : null;
            privateState.streetBW = value ? (value["streetBW"] ? value["streetBW"] : null) : null;
            privateState.streetMT = value ? (value["streetMT"] ? value["streetMT"] : null) : null;
            privateState.addressCity = value ? (value["addressCity"] ? value["addressCity"] : null) : null;
            privateState.addressCityCT = value ? (value["addressCityCT"] ? value["addressCityCT"] : null) : null;
            privateState.addressCityBW = value ? (value["addressCityBW"] ? value["addressCityBW"] : null) : null;
            privateState.addressCityMT = value ? (value["addressCityMT"] ? value["addressCityMT"] : null) : null;
            privateState.countryId = value ? (value["countryId"] ? value["countryId"] : null) : null;
            privateState.countryIdCT = value ? (value["countryIdCT"] ? value["countryIdCT"] : null) : null;
            privateState.countryIdBW = value ? (value["countryIdBW"] ? value["countryIdBW"] : null) : null;
            privateState.countryIdMT = value ? (value["countryIdMT"] ? value["countryIdMT"] : null) : null;
            privateState.postCode = value ? (value["postCode"] ? value["postCode"] : null) : null;
            privateState.postCodeCT = value ? (value["postCodeCT"] ? value["postCodeCT"] : null) : null;
            privateState.postCodeBW = value ? (value["postCodeBW"] ? value["postCodeBW"] : null) : null;
            privateState.postCodeMT = value ? (value["postCodeMT"] ? value["postCodeMT"] : null) : null;
            privateState.dateOfBirthCT = value ? (value["dateOfBirthCT"] ? value["dateOfBirthCT"] : null) : null;
            privateState.dateOfBirthBW = value ? (value["dateOfBirthBW"] ? value["dateOfBirthBW"] : null) : null;
            privateState.dateOfBirthMT = value ? (value["dateOfBirthMT"] ? value["dateOfBirthMT"] : null) : null;
            privateState.customerStatus = value ? (value["customerStatus"] ? value["customerStatus"] : null) : null;
            privateState.customerStatusCT = value ? (value["customerStatusCT"] ? value["customerStatusCT"] : null) : null;
            privateState.customerStatusBW = value ? (value["customerStatusBW"] ? value["customerStatusBW"] : null) : null;
            privateState.customerStatusMT = value ? (value["customerStatusMT"] ? value["customerStatusMT"] : null) : null;
            privateState.sectorId = value ? (value["sectorId"] ? value["sectorId"] : null) : null;
            privateState.sectorIdCT = value ? (value["sectorIdCT"] ? value["sectorIdCT"] : null) : null;
            privateState.sectorIdBW = value ? (value["sectorIdBW"] ? value["sectorIdBW"] : null) : null;
            privateState.sectorIdMT = value ? (value["sectorIdMT"] ? value["sectorIdMT"] : null) : null;
            privateState.accountOfficerIdCT = value ? (value["accountOfficerIdCT"] ? value["accountOfficerIdCT"] : null) : null;
            privateState.accountOfficerIdBW = value ? (value["accountOfficerIdBW"] ? value["accountOfficerIdBW"] : null) : null;
            privateState.accountOfficerIdMT = value ? (value["accountOfficerIdMT"] ? value["accountOfficerIdMT"] : null) : null;
            privateState.industryId = value ? (value["industryId"] ? value["industryId"] : null) : null;
            privateState.industryIdCT = value ? (value["industryIdCT"] ? value["industryIdCT"] : null) : null;
            privateState.industryIdBW = value ? (value["industryIdBW"] ? value["industryIdBW"] : null) : null;
            privateState.industryIdMT = value ? (value["industryIdMT"] ? value["industryIdMT"] : null) : null;
            privateState.nationalityId = value ? (value["nationalityId"] ? value["nationalityId"] : null) : null;
            privateState.nationalityIdCT = value ? (value["nationalityIdCT"] ? value["nationalityIdCT"] : null) : null;
            privateState.nationalityIdBW = value ? (value["nationalityIdBW"] ? value["nationalityIdBW"] : null) : null;
            privateState.nationalityIdMT = value ? (value["nationalityIdMT"] ? value["nationalityIdMT"] : null) : null;
            privateState.residenceId = value ? (value["residenceId"] ? value["residenceId"] : null) : null;
            privateState.residenceIdCT = value ? (value["residenceIdCT"] ? value["residenceIdCT"] : null) : null;
            privateState.residenceIdBW = value ? (value["residenceIdBW"] ? value["residenceIdBW"] : null) : null;
            privateState.residenceIdMT = value ? (value["residenceIdMT"] ? value["residenceIdMT"] : null) : null;
            privateState.phoneNumber = value ? (value["phoneNumber"] ? value["phoneNumber"] : null) : null;
            privateState.phoneNumberCT = value ? (value["phoneNumberCT"] ? value["phoneNumberCT"] : null) : null;
            privateState.phoneNumberBW = value ? (value["phoneNumberBW"] ? value["phoneNumberBW"] : null) : null;
            privateState.phoneNumberMT = value ? (value["phoneNumberMT"] ? value["phoneNumberMT"] : null) : null;
            privateState.customerMnemonic = value ? (value["customerMnemonic"] ? value["customerMnemonic"] : null) : null;
            privateState.customerMnemonicCT = value ? (value["customerMnemonicCT"] ? value["customerMnemonicCT"] : null) : null;
            privateState.customerMnemonicBW = value ? (value["customerMnemonicBW"] ? value["customerMnemonicBW"] : null) : null;
            privateState.customerMnemonicMT = value ? (value["customerMnemonicMT"] ? value["customerMnemonicMT"] : null) : null;
            privateState.firstNameCT = value ? (value["firstNameCT"] ? value["firstNameCT"] : null) : null;
            privateState.firstNameBW = value ? (value["firstNameBW"] ? value["firstNameBW"] : null) : null;
            privateState.firstNameMT = value ? (value["firstNameMT"] ? value["firstNameMT"] : null) : null;
            privateState.customerType = value ? (value["customerType"] ? value["customerType"] : null) : null;
            privateState.customerTypeCT = value ? (value["customerTypeCT"] ? value["customerTypeCT"] : null) : null;
            privateState.customerTypeBW = value ? (value["customerTypeBW"] ? value["customerTypeBW"] : null) : null;
            privateState.customerTypeMT = value ? (value["customerTypeMT"] ? value["customerTypeMT"] : null) : null;
            privateState.emailCT = value ? (value["emailCT"] ? value["emailCT"] : null) : null;
            privateState.emailBW = value ? (value["emailBW"] ? value["emailBW"] : null) : null;
            privateState.emailMT = value ? (value["emailMT"] ? value["emailMT"] : null) : null;
            privateState.mobilePhoneNumber = value ? (value["mobilePhoneNumber"] ? value["mobilePhoneNumber"] : null) : null;
            privateState.mobilePhoneNumberCT = value ? (value["mobilePhoneNumberCT"] ? value["mobilePhoneNumberCT"] : null) : null;
            privateState.mobilePhoneNumberBW = value ? (value["mobilePhoneNumberBW"] ? value["mobilePhoneNumberBW"] : null) : null;
            privateState.mobilePhoneNumberMT = value ? (value["mobilePhoneNumberMT"] ? value["mobilePhoneNumberMT"] : null) : null;
            privateState.lastNameCT = value ? (value["lastNameCT"] ? value["lastNameCT"] : null) : null;
            privateState.lastNameBW = value ? (value["lastNameBW"] ? value["lastNameBW"] : null) : null;
            privateState.lastNameMT = value ? (value["lastNameMT"] ? value["lastNameMT"] : null) : null;
            privateState.emailAddress = value ? (value["emailAddress"] ? value["emailAddress"] : null) : null;
            privateState.accountOfficer = value ? (value["accountOfficer"] ? value["accountOfficer"] : null) : null;
            privateState.page_start = value ? (value["page_start"] ? value["page_start"] : null) : null;
            privateState.count = value ? (value["count"] ? value["count"] : null) : null;
            privateState.PhoneNo = value ? (value["PhoneNo"] ? value["PhoneNo"] : null) : null;
        };
    }
    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(customerObject);
    //Create new class level validator object
    BaseModel.Validator.call(customerObject);
    var registerValidatorBackup = customerObject.registerValidator;
    customerObject.registerValidator = function() {
            var propName = arguments[0];
            if (!setterFunctions[propName].changed) {
                var setterBackup = setterFunctions[propName];
                setterFunctions[arguments[0]] = function() {
                    if (customerObject.isValid(this, propName, val)) {
                        return setterBackup.apply(null, arguments);
                    } else {
                        throw Error("Validation failed for " + propName + " : " + val);
                    }
                }
                setterFunctions[arguments[0]].changed = true;
            }
            return registerValidatorBackup.apply(null, arguments);
        }
        //Extending Model for custom operations
        //For Operation 'getCDMcustomerQuery' with service id 'getCustomerInformation9282'
    customerObject.getCDMcustomerQuery = function(params, onCompletion) {
        return customerObject.customVerb('getCDMcustomerQuery', params, onCompletion);
    };
    //For Operation 'getCustomers2' with service id 'getCustomerInformation4683'
    customerObject.getCustomers2 = function(params, onCompletion) {
        return customerObject.customVerb('getCustomers2', params, onCompletion);
    };
    //For Operation 'getCustomers' with service id 'getCustomers2635'
    customerObject.getCustomers = function(params, onCompletion) {
        return customerObject.customVerb('getCustomers', params, onCompletion);
    };
    var relations = [];
    customerObject.relations = relations;
    customerObject.prototype.isValid = function() {
        return customerObject.isValid(this);
    };
    customerObject.prototype.objModelName = "customerObject";
    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    customerObject.registerProcessors = function(options, successCallback, failureCallback) {
        if (!options) {
            options = {};
        }
        if (options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }
        if (options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }
        kony.mvc.util.ProcessorUtils.getMetadataForObject("cdmSearchCustomer", "customerObject", options, metaDataSuccess, metaDataFailure);
    };
    //clone the object provided in argument.
    customerObject.clone = function(objectToClone) {
        var clonedObj = new customerObject();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };
    return customerObject;
});