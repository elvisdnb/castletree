define("flxBlockingHistoryDetailsResponse", function() {
    return function(controller) {
        var flxBlockingHistoryDetailsResponse = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxBlockingHistoryDetailsResponse",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxBlockingHistoryDetailsResponse.setDefaultUnit(kony.flex.DP);
        var lblRefNoTitle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRefNoTitle",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLightGrey12PxRoboMedium",
            "text": "ANBG8593110466",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStartDateTitle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStartDateTitle",
            "isVisible": true,
            "left": "151dp",
            "skin": "sknLightGrey12PxRoboMedium",
            "text": "31-01-2021",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblToDateTitle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblToDateTitle",
            "isVisible": true,
            "left": "246dp",
            "skin": "sknLightGrey12PxRoboMedium",
            "text": "23-04-2021",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmountTitle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAmountTitle",
            "isVisible": true,
            "left": "349dp",
            "skin": "sknLightGrey12PxRoboMedium",
            "text": "34,982.09",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblReasonTitle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblReasonTitle",
            "isVisible": true,
            "left": "451dp",
            "skin": "sknLightGrey12PxRoboMedium",
            "text": "Regularaty Warrant",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnEditClick = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "40dp",
            "id": "btnEditClick",
            "isVisible": true,
            "left": "616dp",
            "skin": "sknButtonEditImage",
            "top": "28dp",
            "width": "54dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnUnblockClick = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "40dp",
            "id": "btnUnblockClick",
            "isVisible": true,
            "left": "670dp",
            "skin": "sknButtonUnBlockImage",
            "top": "27dp",
            "width": "50dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxBlockingHistoryDetailsResponse.add(lblRefNoTitle, lblStartDateTitle, lblToDateTitle, lblAmountTitle, lblReasonTitle, btnEditClick, btnUnblockClick);
        return flxBlockingHistoryDetailsResponse;
    }
})