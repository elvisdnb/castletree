define("frmLogin", function() {
    return function(controller) {
        function addWidgetsfrmLogin() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0c9717a5b233142",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeft = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeft",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0dc46aeb626394d",
                "top": "0",
                "width": "60%"
            }, {}, {});
            flxLeft.setDefaultUnit(kony.flex.DP);
            var imgTemenos = new kony.ui.Image2({
                "zoomEnabled": false,
                "id": "imgTemenos",
                "isVisible": true,
                "left": "17%",
                "skin": "slImage",
                "src": "grp1.png",
                "top": "10%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "blur": {
                    "enabled": false,
                    "value": 0
                }
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "400dp",
                "id": "flxInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "100dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d005341c12fc47",
                "top": "15%",
                "width": "80%"
            }, {}, {});
            flxInnerWrapper.setDefaultUnit(kony.flex.DP);
            var flxWhiteBorder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55%",
                "id": "flxWhiteBorder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0a62758904f3e4b",
                "top": "10%",
                "width": "1%"
            }, {}, {});
            flxWhiteBorder.setDefaultUnit(kony.flex.DP);
            flxWhiteBorder.add();
            var lblRetail = new kony.ui.Label({
                "id": "lblRetail",
                "isVisible": true,
                "left": "6%",
                "skin": "CopydefLabel0f16a81cb05dc43",
                "text": "Retail Lending",
                "top": "20%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblRetail0i7bbb1bc142c4d = new kony.ui.Label({
                "id": "CopylblRetail0i7bbb1bc142c4d",
                "isVisible": true,
                "left": "6%",
                "skin": "CopydefLabel0f16a81cb05dc43",
                "text": "Intuitively manage your to-dos",
                "top": "35%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblRetail0e577c53378e548 = new kony.ui.Label({
                "id": "CopylblRetail0e577c53378e548",
                "isVisible": true,
                "left": "6%",
                "skin": "CopydefLabel0f16a81cb05dc43",
                "text": "from origination to servicing",
                "top": "51%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0fe41743300ee49 = new kony.ui.Label({
                "id": "Label0fe41743300ee49",
                "isVisible": true,
                "left": "6%",
                "skin": "CopydefLabel0b38a13b01aa843",
                "text": "The World’s No.1 Banking Software",
                "top": "70%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUrl = new kony.ui.Label({
                "id": "lblUrl",
                "isVisible": true,
                "left": "6%",
                "skin": "CopydefLabel0b38a13b01aa843",
                "text": "temenos.com",
                "top": "78%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInnerWrapper.add(flxWhiteBorder, lblRetail, CopylblRetail0i7bbb1bc142c4d, CopylblRetail0e577c53378e548, Label0fe41743300ee49, lblUrl);
            flxLeft.add(imgTemenos, flxInnerWrapper);
            var flxRight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRight",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0a34cd328d83f47",
                "top": "0",
                "width": "40%"
            }, {}, {});
            flxRight.setDefaultUnit(kony.flex.DP);
            var flxRightInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55%",
                "id": "flxRightInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "18%",
                "width": "75%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxRightInner.setDefaultUnit(kony.flex.DP);
            var lblLoginWelcome = new kony.ui.Label({
                "id": "lblLoginWelcome",
                "isVisible": true,
                "left": "8%",
                "skin": "CopydefLabel0a55addad1b9449",
                "text": "Welcome, Let’s sign you in",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0a3e6503fceab49 = new kony.ui.Label({
                "id": "Label0a3e6503fceab49",
                "isVisible": false,
                "left": "8%",
                "skin": "CopydefLabel0jb0ba69bd76645",
                "text": "Join our banking community to accelerate",
                "top": "15%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0c8ca87527d7841 = new kony.ui.Label({
                "id": "CopyLabel0c8ca87527d7841",
                "isVisible": false,
                "left": "8%",
                "skin": "CopydefLabel0jb0ba69bd76645",
                "text": "innovation and productivity in your organisation",
                "top": "3%",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelectUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxSelectUser",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "8%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15%",
                "width": "95%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxSelectUser.setDefaultUnit(kony.flex.DP);
            var flxDebitAccountInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "40dp",
                "id": "flxDebitAccountInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "Copysknflxf0b6f75ef68a3148",
                "top": "0dp",
                "width": "31dp",
                "zIndex": 1
            }, {}, {});
            flxDebitAccountInnerWrapper.setDefaultUnit(kony.flex.DP);
            var downArrowLbl = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "60%",
                "height": "20dp",
                "id": "downArrowLbl",
                "isVisible": true,
                "left": "0dp",
                "right": "5%",
                "skin": "sknLblIcon12px",
                "text": "q",
                "top": "0dp",
                "width": "20dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDebitAccountInnerWrapper.add(downArrowLbl);
            var custSelectUser = new kony.ui.CustomWidget({
                "id": "custSelectUser",
                "isVisible": true,
                "left": "0dp",
                "top": "0%",
                "width": "80%",
                "height": "55px",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Select User",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            flxSelectUser.add(flxDebitAccountInnerWrapper, custSelectUser);
            var flxCustPass = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxCustPass",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "8%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "95%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxCustPass.setDefaultUnit(kony.flex.DP);
            var flxPassImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPassImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "Copysknflxf0ed64c6dba52c43",
                "top": "5%",
                "width": "31dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxPassImage.setDefaultUnit(kony.flex.DP);
            var CopyfloatReadOnlylText0b9b56db2d81b41 = new com.lending.floatingReadOnlyText.floatReadOnlylText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "100%",
                "id": "CopyfloatReadOnlylText0b9b56db2d81b41",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxWhiteBg",
                "top": "0dp",
                "width": "100%",
                "zIndex": 4,
                "overrides": {
                    "floatReadOnlylText": {
                        "height": "100%",
                        "isVisible": false,
                        "width": "100%"
                    },
                    "lblFloatLabel": {
                        "text": "Debit Account"
                    },
                    "txtFloatText": {
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var CopyfloatLabelText0e10ae83ab13a42 = new com.lending.floatingTextBox.floatLabelText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "54dp",
                "id": "CopyfloatLabelText0e10ae83ab13a42",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxWhiteBg",
                "top": "0dp",
                "width": "100%",
                "zIndex": 4,
                "overrides": {
                    "downArrowLbl": {
                        "centerY": "40%",
                        "isVisible": true,
                        "right": "5%",
                        "zIndex": 3
                    },
                    "floatLabelText": {
                        "height": "54dp",
                        "isVisible": false
                    },
                    "flxBgColor": {
                        "height": "100%"
                    },
                    "flxBottomLine": {
                        "top": "viz.val_cleared",
                        "zIndex": 5
                    },
                    "flxFloatLableGrp": {
                        "centerY": "50%",
                        "height": "56dp",
                        "top": "viz.val_cleared"
                    },
                    "lblFloatLabel": {
                        "centerY": "40%",
                        "text": "Debit Account Number",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "rtAsterix": {
                        "centerY": "40%",
                        "left": "0dp"
                    },
                    "txtFloatText": {
                        "height": "54dp",
                        "isVisible": true,
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var CopydownArrowLbl0gd341900508245 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "60%",
                "height": "20dp",
                "id": "CopydownArrowLbl0gd341900508245",
                "isVisible": true,
                "left": "0dp",
                "right": "5%",
                "skin": "sknLblIcon12px",
                "text": "r",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPassImage.add(CopyfloatReadOnlylText0b9b56db2d81b41, CopyfloatLabelText0e10ae83ab13a42, CopydownArrowLbl0gd341900508245);
            var custPassword = new kony.ui.CustomWidget({
                "id": "custPassword",
                "isVisible": true,
                "left": "0dp",
                "top": "5%",
                "width": "80%",
                "height": "55px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "password"
                },
                "iconButtonIcon": "",
                "labelText": "Password",
                "maxLength": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            flxCustPass.add(flxPassImage, custPassword);
            var custPassword1 = new kony.ui.CustomWidget({
                "id": "custPassword1",
                "isVisible": false,
                "left": "40dp",
                "top": "20dp",
                "width": "448px",
                "height": "55px",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Password",
                "maxLength": 35,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var flxLoginButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5%",
                "clipBounds": false,
                "height": "100dp",
                "id": "flxLoginButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3%",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxLoginButton.setDefaultUnit(kony.flex.DP);
            var lblForget = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblForget",
                "isVisible": false,
                "left": "0",
                "skin": "CopydefLabel0b2374aef688646",
                "text": "Forgot password?",
                "top": 30,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custLogin = new kony.ui.CustomWidget({
                "id": "custLogin",
                "isVisible": true,
                "left": "50%",
                "width": "40%",
                "height": "40%",
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Login",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxLoginButton.add(lblForget, custLogin);
            flxRightInner.add(lblLoginWelcome, Label0a3e6503fceab49, CopyLabel0c8ca87527d7841, flxSelectUser, flxCustPass, custPassword1, flxLoginButton);
            var flxCntnr3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "5%",
                "id": "flxCntnr3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "CopyslFbox0bfb2345750d744",
                "top": "20%",
                "width": "75%",
                "zIndex": 1
            }, {}, {});
            flxCntnr3.setDefaultUnit(kony.flex.DP);
            var imgSrc1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "150dp",
                "id": "imgSrc1",
                "isVisible": true,
                "left": "8%",
                "skin": "slImage",
                "src": "temlogo_1.png",
                "width": "20%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCpy = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCpy",
                "isVisible": true,
                "left": "29%",
                "skin": "CopysknDefProfile3",
                "text": "Copyright @2021 Temenos HeadQuarters SA",
                "width": "75%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCntnr3.add(imgSrc1, lblCpy);
            flxRight.add(flxRightInner, flxCntnr3);
            flxMain.add(flxLeft, flxRight);
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1920,
                "1366": {
                    "imgTemenos": {
                        "segmentProps": []
                    },
                    "flxCntnr3": {
                        "segmentProps": []
                    },
                    "imgSrc1": {
                        "bottom": {
                            "type": "string",
                            "value": "0dp"
                        },
                        "height": {
                            "type": "string",
                            "value": "20px"
                        },
                        "segmentProps": []
                    },
                    "lblCpy": {
                        "bottom": {
                            "type": "string",
                            "value": "0dp"
                        },
                        "segmentProps": []
                    }
                },
                "1920": {
                    "CopylblRetail0i7bbb1bc142c4d": {
                        "segmentProps": []
                    },
                    "CopylblRetail0e577c53378e548": {
                        "segmentProps": []
                    }
                }
            }
            this.compInstData = {
                "CopyfloatReadOnlylText0b9b56db2d81b41": {
                    "height": "100%",
                    "width": "100%"
                },
                "CopyfloatReadOnlylText0b9b56db2d81b41.lblFloatLabel": {
                    "text": "Debit Account"
                },
                "CopyfloatLabelText0e10ae83ab13a42.downArrowLbl": {
                    "centerY": "40%",
                    "right": "5%",
                    "zIndex": 3
                },
                "CopyfloatLabelText0e10ae83ab13a42": {
                    "height": "54dp"
                },
                "CopyfloatLabelText0e10ae83ab13a42.flxBgColor": {
                    "height": "100%"
                },
                "CopyfloatLabelText0e10ae83ab13a42.flxBottomLine": {
                    "top": "",
                    "zIndex": 5
                },
                "CopyfloatLabelText0e10ae83ab13a42.flxFloatLableGrp": {
                    "centerY": "50%",
                    "height": "56dp",
                    "top": ""
                },
                "CopyfloatLabelText0e10ae83ab13a42.lblFloatLabel": {
                    "centerY": "40%",
                    "text": "Debit Account Number",
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "CopyfloatLabelText0e10ae83ab13a42.rtAsterix": {
                    "centerY": "40%",
                    "left": "0dp"
                },
                "CopyfloatLabelText0e10ae83ab13a42.txtFloatText": {
                    "height": "54dp",
                    "width": "100%"
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMain, ProgressIndicator);
        };
        return [{
            "addWidgets": addWidgetsfrmLogin,
            "enabledForIdleTimeout": false,
            "id": "frmLogin",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0i9767894f2cb40",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [1280, 1366, 1920]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});