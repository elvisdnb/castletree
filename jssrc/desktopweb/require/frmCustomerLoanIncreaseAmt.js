define("frmCustomerLoanIncreaseAmt", function() {
    return function(controller) {
        function addWidgetsfrmCustomerLoanIncreaseAmt() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5%",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFbox0i8184c83ec4b4d",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "300dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "112dp",
                "skin": "CopysknLbl0a0afbda0701b4f",
                "text": "Overview",
                "top": "26dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadboardIncreaseAmountSplash = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadboardIncreaseAmountSplash",
                "isVisible": true,
                "left": "170dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "26dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardIncreasePrinciple = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardIncreasePrinciple",
                "isVisible": true,
                "left": "185dp",
                "skin": "sknLblLoanServiceBlacl12px",
                "text": "Increase Principal",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue, lblBreadboardIncreaseAmountSplash, lblBreadBoardIncreasePrinciple);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "32dp",
                "skin": "sknFlxBorderCCE3F7",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var MainTabs = new com.lending.MainTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "MainTabs": {
                        "left": "0dp",
                        "width": "100%"
                    },
                    "btnAccounts1": {
                        "left": 40
                    },
                    "flxContainerAccount": {
                        "left": "27dp"
                    },
                    "flxontainerLoans": {
                        "left": "247dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var loanDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "loanDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "overrides": {
                    "FlexContainer0ebe191bc1cf946": {
                        "left": "-150dp"
                    },
                    "flxListBoxSelectedValue": {
                        "left": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "flxLoanDetailsvalue": {
                        "height": "63dp"
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "src": "drop_down_arrow.png"
                    },
                    "lblAccountNumberInfo": {
                        "width": "12.50%"
                    },
                    "lblAccountNumberResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblAccountServiceInfo": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "lblAmountValueInfo": {
                        "width": "12.50%"
                    },
                    "lblAmountValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblCurrencyValueInfo": {
                        "width": "12.50%"
                    },
                    "lblCurrencyValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Increase Principal"
                    },
                    "lblLoanTypeInfo": {
                        "width": "13.20%"
                    },
                    "lblLoanTypeResponse": {
                        "centerY": "50%",
                        "width": "13.60%"
                    },
                    "lblMaturityDateInfo": {
                        "width": "12.50%"
                    },
                    "lblMaturityDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblStartDateInfo": {
                        "width": "12.50%"
                    },
                    "lblStartDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "loanDetails": {
                        "height": "128dp",
                        "left": "18dp",
                        "top": "0dp",
                        "width": "98%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxIncreaseCommitment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "30dp",
                "clipBounds": true,
                "height": "450dp",
                "id": "flxIncreaseCommitment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknFlxMultiBGCBE3F8",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxIncreaseCommitment.setDefaultUnit(kony.flex.DP);
            var amtDisubrsed = new kony.ui.CustomWidget({
                "id": "amtDisubrsed",
                "isVisible": false,
                "left": "483dp",
                "right": "528dp",
                "top": "95dp",
                "width": "265dp",
                "height": "60dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "decimals": 0,
                "dense": false,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Amount Disbursed",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var amtCommitmentIncreaseFee = new kony.ui.CustomWidget({
                "id": "amtCommitmentIncreaseFee",
                "isVisible": false,
                "left": "796dp",
                "right": "215dp",
                "top": "95dp",
                "width": "265dp",
                "height": "60dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "decimals": 0,
                "dense": false,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Commitment Increase Fee",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var increaseEffectiveDate = new kony.ui.CustomWidget({
                "id": "increaseEffectiveDate",
                "isVisible": false,
                "left": "483dp",
                "right": "528dp",
                "top": "173dp",
                "width": "265dp",
                "height": "60dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": false,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Increase Effective Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "value": "",
                "valueChanged": "",
                "weekLabel": "Wk"
            });
            var lblIncreasePrincipalAmt = new kony.ui.Label({
                "height": "40dp",
                "id": "lblIncreasePrincipalAmt",
                "isVisible": false,
                "left": "15%",
                "skin": "sknLblRobotoMed16pxDarkBlue",
                "text": "Increase Principal Amount",
                "top": "24dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TitleLabel = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "70dp",
                "id": "TitleLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "923dp",
                "overrides": {
                    "TitleLabel": {
                        "centerX": "50%",
                        "height": "70dp",
                        "left": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "top": "viz.val_cleared",
                        "width": "923dp"
                    },
                    "flxTitle": {
                        "isVisible": false
                    },
                    "lblTitle": {
                        "left": "0dp",
                        "text": "Increase Principal"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSeparatorLineGrey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLineGrey",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fda5754cd9f44b",
                "width": "950dp",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLineGrey.setDefaultUnit(kony.flex.DP);
            flxSeparatorLineGrey.add();
            var FlexContainer0iffbca71c3734a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "FlexContainer0iffbca71c3734a",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fda5754cd9f44b",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0iffbca71c3734a.setDefaultUnit(kony.flex.DP);
            FlexContainer0iffbca71c3734a.add();
            var flxIncreaseCommitmentFields = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "120dp",
                "id": "flxIncreaseCommitmentFields",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "923dp",
                "zIndex": 4
            }, {}, {});
            flxIncreaseCommitmentFields.setDefaultUnit(kony.flex.DP);
            var amtCurrentCommitment1 = new kony.ui.CustomWidget({
                "id": "amtCurrentCommitment1",
                "isVisible": true,
                "left": 0,
                "width": "265dp",
                "height": "56dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Current Commitment",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var amtCurrentCommitment2 = new com.lending.floatingReadOnlyText.floatReadOnlylText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "40%",
                "height": "40dp",
                "id": "amtCurrentCommitment2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxWhiteBg",
                "width": "265dp",
                "zIndex": 4,
                "overrides": {
                    "floatReadOnlylText": {
                        "centerX": "viz.val_cleared",
                        "centerY": "40%",
                        "height": "40dp",
                        "isVisible": false,
                        "left": "0",
                        "top": "viz.val_cleared",
                        "width": "265dp"
                    },
                    "lblFloatLabel": {
                        "text": "Current Commitment"
                    },
                    "txtFloatText": {
                        "left": "0dp",
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var amtIncreaseFloatTxt = new kony.ui.CustomWidget({
                "id": "amtIncreaseFloatTxt",
                "isVisible": true,
                "left": "64dp",
                "top": "0dp",
                "width": "265dp",
                "height": "56dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Increase Amount",
                "locale": "",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "suffix": "",
                "validateRequired": "required",
                "value": ""
            });
            var amtIncreaseFloatTxt2 = new com.lending.floatingTextBox.floatLabelText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "40%",
                "height": "40dp",
                "id": "amtIncreaseFloatTxt2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxWhiteBg",
                "width": "265dp",
                "zIndex": 4,
                "overrides": {
                    "floatLabelText": {
                        "centerX": "viz.val_cleared",
                        "centerY": "40%",
                        "height": "40dp",
                        "isVisible": false,
                        "left": "64dp",
                        "top": "viz.val_cleared",
                        "width": "265dp"
                    },
                    "lblFloatLabel": {
                        "text": "Increase Amount"
                    },
                    "txtFloatText": {
                        "centerY": "50%",
                        "left": "0dp",
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var amtRevisedommitment1 = new kony.ui.CustomWidget({
                "id": "amtRevisedommitment1",
                "isVisible": true,
                "left": "64dp",
                "top": "34dp",
                "width": "265dp",
                "height": "56dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Revised Commitment",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var amtRevisedommitment2 = new com.lending.floatingReadOnlyText.floatReadOnlylText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "40%",
                "height": "40dp",
                "id": "amtRevisedommitment2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxWhiteBg",
                "width": "265dp",
                "zIndex": 4,
                "overrides": {
                    "floatReadOnlylText": {
                        "centerX": "viz.val_cleared",
                        "centerY": "40%",
                        "height": "40dp",
                        "isVisible": false,
                        "left": "64dp",
                        "top": "viz.val_cleared",
                        "width": "265dp"
                    },
                    "lblFloatLabel": {
                        "text": "Revised Commitment"
                    },
                    "txtFloatText": {
                        "centerY": "50.00%",
                        "left": "0dp",
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxIncreaseCommitmentFields.add(amtCurrentCommitment1, amtCurrentCommitment2, amtIncreaseFloatTxt, amtIncreaseFloatTxt2, amtRevisedommitment1, amtRevisedommitment2);
            var flxLblError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLblError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "923dp",
                "zIndex": 4
            }, {}, {});
            flxLblError.setDefaultUnit(kony.flex.DP);
            var lblErrorMsg = new kony.ui.Label({
                "height": "40dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRevisedPaymentConfirmation = new kony.ui.Label({
                "height": "40dp",
                "id": "lblRevisedPaymentConfirmation",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLabelErrorMessage",
                "text": "* Please enter increase amount",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLblError.add(lblErrorMsg, lblRevisedPaymentConfirmation);
            var flxSubmit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxSubmit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a61462c2ce114f",
                "top": "-10dp",
                "width": "98%",
                "zIndex": 4
            }, {}, {});
            flxSubmit.setDefaultUnit(kony.flex.DP);
            var imgRevisedPaymentSchedule = new kony.ui.Image2({
                "centerY": "50%",
                "height": "34dp",
                "id": "imgRevisedPaymentSchedule",
                "isVisible": false,
                "left": "434dp",
                "skin": "slImage",
                "src": "baseline_cached_black_18dp.png",
                "width": "46dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRevisedPaymentLabel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "79dp",
                "id": "flxRevisedPaymentLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "923dp",
                "zIndex": 1
            }, {}, {});
            flxRevisedPaymentLabel.setDefaultUnit(kony.flex.DP);
            var custImgRevised = new kony.ui.CustomWidget({
                "id": "custImgRevised",
                "isVisible": true,
                "left": "0dp",
                "top": 0,
                "width": "34px",
                "height": "34px",
                "centerY": "58%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "autorenew",
                "size": "small"
            });
            var lblRevisedPaymenySchedule = new kony.ui.Label({
                "centerY": "50%",
                "height": "21dp",
                "id": "lblRevisedPaymenySchedule",
                "isVisible": true,
                "left": "34dp",
                "skin": "lblRevisedPaymentSchedule",
                "text": "See Revised Payment Schedule",
                "width": "210dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRevisedPaymentLabel.add(custImgRevised, lblRevisedPaymenySchedule);
            var flxCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "79dp",
                "id": "flxCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "reverseLayoutDirection": false,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxCustButtons.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.CustomWidget({
                "id": "btnCancel",
                "isVisible": true,
                "left": "30dp",
                "width": "196dp",
                "height": "40dp",
                "minWidth": 184,
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var btnConfirm = new kony.ui.CustomWidget({
                "id": "btnConfirm",
                "isVisible": true,
                "left": "254dp",
                "width": "204dp",
                "height": "40dp",
                "minWidth": 184,
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxCustButtons.add(btnCancel, btnConfirm);
            flxSubmit.add(imgRevisedPaymentSchedule, flxRevisedPaymentLabel, flxCustButtons);
            flxIncreaseCommitment.add(amtDisubrsed, amtCommitmentIncreaseFee, increaseEffectiveDate, lblIncreasePrincipalAmt, TitleLabel, flxSeparatorLineGrey, FlexContainer0iffbca71c3734a, flxIncreaseCommitmentFields, flxLblError, flxSubmit);
            flxMainContainer.add(flxHeaderMenu, custInfo, MainTabs, loanDetails, flxIncreaseCommitment);
            var Popup = new com.konymp.flxoverridepopup.Popup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "Popup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "Popup": {
                        "isVisible": false
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "968dp",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "isVisible": false,
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    },
                    "segOverride": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64dp",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "2dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageWithReferenceNumber = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMessageWithReferenceNumber",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BG",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgHeaderClose = new kony.ui.Image2({
                "height": "14dp",
                "id": "imgHeaderClose",
                "isVisible": true,
                "left": "1250dp",
                "skin": "CopyslImage0f719420e2dad4a",
                "src": "ico_close.png",
                "top": "17dp",
                "width": "14dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblMessageWithReferenceNumber, imgHeaderClose);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "90dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "91%",
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "90dp",
                        "width": "91%"
                    },
                    "flxError": {
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    },
                    "imgCloseBackup": {
                        "centerY": "50%",
                        "height": "18dp",
                        "left": "viz.val_cleared",
                        "right": 20,
                        "src": "ico_close.png",
                        "top": "viz.val_cleared"
                    },
                    "lblMessage": {
                        "centerY": "50%",
                        "text": "Subtitle text goes here",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var RevisedPaymentSchedule = new com.lending.revisedPaymentSchedule.RevisedPaymentSchedule({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "RevisedPaymentSchedule",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "RevisedPaymentSchedule": {
                        "isVisible": false
                    },
                    "imgClose1": {
                        "src": "ico_close.png"
                    },
                    "imgPrint": {
                        "isVisible": false,
                        "src": "ico_print.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0bb1e602100ad4e",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0c949d63499e44f"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0ec2af3300d3144"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_a17c69504bca401d94c89676af814122;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_d6ec65ff007a4b488f3d880d25fb1bc9;
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "custInfo": {
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "MainTabs": {
                    "left": "0dp",
                    "width": "100%"
                },
                "MainTabs.btnAccounts1": {
                    "left": 40
                },
                "MainTabs.flxContainerAccount": {
                    "left": "27dp"
                },
                "MainTabs.flxontainerLoans": {
                    "left": "247dp"
                },
                "loanDetails.FlexContainer0ebe191bc1cf946": {
                    "left": "-150dp"
                },
                "loanDetails.flxListBoxSelectedValue": {
                    "left": "",
                    "minWidth": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.flxLoanDetailsvalue": {
                    "height": "63dp"
                },
                "loanDetails.imgListBoxSelectedDropDown": {
                    "src": "drop_down_arrow.png"
                },
                "loanDetails.lblAccountNumberInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAccountNumberResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountServiceInfo": {
                    "left": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblListBoxSelectedValue": {
                    "text": "Increase Principal"
                },
                "loanDetails.lblLoanTypeInfo": {
                    "width": "13.20%"
                },
                "loanDetails.lblLoanTypeResponse": {
                    "centerY": "50%",
                    "width": "13.60%"
                },
                "loanDetails.lblMaturityDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails": {
                    "height": "128dp",
                    "left": "18dp",
                    "top": "0dp",
                    "width": "98%"
                },
                "TitleLabel": {
                    "centerX": "50%",
                    "height": "70dp",
                    "left": "",
                    "minHeight": "",
                    "top": "",
                    "width": "923dp"
                },
                "TitleLabel.lblTitle": {
                    "left": "0dp",
                    "text": "Increase Principal"
                },
                "amtCurrentCommitment2": {
                    "centerX": "",
                    "centerY": "40%",
                    "height": "40dp",
                    "left": "0",
                    "top": "",
                    "width": "265dp"
                },
                "amtCurrentCommitment2.lblFloatLabel": {
                    "text": "Current Commitment"
                },
                "amtCurrentCommitment2.txtFloatText": {
                    "left": "0dp"
                },
                "amtIncreaseFloatTxt2": {
                    "centerX": "",
                    "centerY": "40%",
                    "height": "40dp",
                    "left": "64dp",
                    "top": "",
                    "width": "265dp"
                },
                "amtIncreaseFloatTxt2.lblFloatLabel": {
                    "text": "Increase Amount"
                },
                "amtIncreaseFloatTxt2.txtFloatText": {
                    "centerY": "50%",
                    "left": "0dp"
                },
                "amtRevisedommitment2": {
                    "centerX": "",
                    "centerY": "40%",
                    "height": "40dp",
                    "left": "64dp",
                    "top": "",
                    "width": "265dp"
                },
                "amtRevisedommitment2.lblFloatLabel": {
                    "text": "Revised Commitment"
                },
                "amtRevisedommitment2.txtFloatText": {
                    "centerY": "50.00%",
                    "left": "0dp",
                    "top": ""
                },
                "Popup.imgClose": {
                    "src": "ico_close.png"
                },
                "SegmentPopup": {
                    "right": "",
                    "width": "100%"
                },
                "SegmentPopup.imgClose": {
                    "src": "ico_close.png"
                },
                "ErrorAllert": {
                    "left": "90dp",
                    "width": "91%"
                },
                "ErrorAllert.flxError": {
                    "centerY": "50%",
                    "top": ""
                },
                "ErrorAllert.imgCloseBackup": {
                    "centerY": "50%",
                    "height": "18dp",
                    "left": "",
                    "right": 20,
                    "src": "ico_close.png",
                    "top": ""
                },
                "ErrorAllert.lblMessage": {
                    "centerY": "50%",
                    "text": "Subtitle text goes here",
                    "top": ""
                },
                "RevisedPaymentSchedule.imgClose1": {
                    "src": "ico_close.png"
                },
                "RevisedPaymentSchedule.imgPrint": {
                    "src": "ico_print.png"
                },
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0c949d63499e44f",
                    "skin2": "CopydefBtnNormal0ec2af3300d3144",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMainContainer, Popup, SegmentPopup, flxMessageInfo, ErrorAllert, RevisedPaymentSchedule, uuxNavigationRail, ProgressIndicator, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerLoanIncreaseAmt,
            "enabledForIdleTimeout": false,
            "id": "frmCustomerLoanIncreaseAmt",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});