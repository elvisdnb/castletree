define("FlxAccountList", function() {
    return function(controller) {
        var FlxAccountList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "FlxAccountList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0icecb3ff112841",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        FlxAccountList.setDefaultUnit(kony.flex.DP);
        var lblAccountId = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblAccountId",
            "isVisible": true,
            "left": "1%",
            "skin": "sknflxFont434343px12",
            "text": "-",
            "top": "0",
            "width": "25.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbltype = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lbltype",
            "isVisible": true,
            "left": "0",
            "skin": "sknflxFont434343px12",
            "text": "-",
            "top": "0",
            "width": "35.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblWorkingBal = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblWorkingBal",
            "isVisible": true,
            "left": "0",
            "skin": "sknflxFontD32E2FPX12",
            "text": "Final",
            "top": "0",
            "width": "38.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        FlxAccountList.add(lblAccountId, lbltype, lblWorkingBal);
        return FlxAccountList;
    }
})