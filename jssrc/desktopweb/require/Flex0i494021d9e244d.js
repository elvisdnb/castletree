define("Flex0i494021d9e244d", function() {
    return function(controller) {
        var Flex0i494021d9e244d = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "Flex0i494021d9e244d",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        Flex0i494021d9e244d.setDefaultUnit(kony.flex.DP);
        var flxScheduleheader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "90%",
            "id": "flxScheduleheader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, {}, {});
        flxScheduleheader.setDefaultUnit(kony.flex.DP);
        var lblPaymentDate = new kony.ui.Label({
            "height": "32px",
            "id": "lblPaymentDate",
            "isVisible": true,
            "left": "0%",
            "skin": "sknBlue15px",
            "text": "19 Dec 2020",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmount = new kony.ui.Label({
            "height": "32px",
            "id": "lblAmount",
            "isVisible": true,
            "left": "2%",
            "skin": "skngreen15px",
            "text": "0.00",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPrincipal = new kony.ui.Label({
            "height": "32px",
            "id": "lblPrincipal",
            "isVisible": true,
            "left": "2%",
            "skin": "sknBlue15px",
            "text": "0.00",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblInterest = new kony.ui.Label({
            "height": "32px",
            "id": "lblInterest",
            "isVisible": true,
            "left": "2%",
            "skin": "sknBlue15px",
            "text": "0.00",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTax = new kony.ui.Label({
            "height": "32px",
            "id": "lblTax",
            "isVisible": true,
            "left": "2%",
            "skin": "sknBlue15px",
            "text": "0.00",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbltotaloutstanding = new kony.ui.Label({
            "height": "32px",
            "id": "lbltotaloutstanding",
            "isVisible": true,
            "left": "2%",
            "skin": "skngreen15px",
            "text": "0.00",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxScheduleheader.add(lblPaymentDate, lblAmount, lblPrincipal, lblInterest, lblTax, lbltotaloutstanding);
        Flex0i494021d9e244d.add(flxScheduleheader);
        return Flex0i494021d9e244d;
    }
})