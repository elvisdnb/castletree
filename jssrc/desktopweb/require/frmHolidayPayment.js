define("frmHolidayPayment", function() {
    return function(controller) {
        function addWidgetsfrmHolidayPayment() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "minHeight": "968dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknflxScrlBGf2f7fd",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxMainWrapper.setDefaultUnit(kony.flex.DP);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false
                    },
                    "imgCloseBackup": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "300dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "120dp",
                "skin": "CopysknLbl0d3dccf1b0dea4f",
                "text": "Overview",
                "top": "26dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadboardHolidayPaymentSplash = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadboardHolidayPaymentSplash",
                "isVisible": true,
                "left": "180dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "26dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardPaymentHoliday = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardPaymentHoliday",
                "isVisible": true,
                "left": "200dp",
                "skin": "sknLblLoanServiceBlacl12px",
                "text": "Payment Holiday",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue, cusBackIcon, lblBreadboardHolidayPaymentSplash, lblBreadBoardPaymentHoliday);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "72dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHolidayPaymentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "600dp",
                "id": "flxHolidayPaymentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknFlxMultiBGCBE3F8",
                "top": "421dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHolidayPaymentWrapper.setDefaultUnit(kony.flex.DP);
            var flxPaymentHolidayOpted = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxPaymentHolidayOpted",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "64dp",
                "width": "240dp",
                "zIndex": 1
            }, {}, {});
            flxPaymentHolidayOpted.setDefaultUnit(kony.flex.DP);
            var lblPaymentOptedResponse = new kony.ui.Label({
                "height": "50dp",
                "id": "lblPaymentOptedResponse",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknlbl48px000000RotoMedium",
                "text": "0",
                "top": "0dp",
                "width": "27dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPaymentHolidayInfo = new kony.ui.Label({
                "height": "24dp",
                "id": "lblPaymentHolidayInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl20pxBG434343",
                "text": "Payment Holiday(s) Opted",
                "top": "66dp",
                "width": "240dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPaymentHolidayOpted.add(lblPaymentOptedResponse, lblPaymentHolidayInfo);
            var flxGraph = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "flxGraph",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "260dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxGraph.setDefaultUnit(kony.flex.DP);
            var multiline = new com.konymp.multiline({
                "height": "100%",
                "id": "multiline",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "multiline": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            multiline.chartTitle = "Line Chart - Stock Example";
            multiline.enableLegends = true;
            multiline.xAxisTitle = "x-axis";
            multiline.chartData = {
                "data": [{
                    "dataPoint1": "12",
                    "dataPoint2": "2",
                    "dataPoint3": "1",
                    "dataPoint4": "9",
                    "dataPoint5": "3",
                    "label": "d1"
                }, {
                    "dataPoint1": "9",
                    "dataPoint2": "1",
                    "dataPoint3": "3",
                    "dataPoint4": "2",
                    "dataPoint5": "9",
                    "label": "d2"
                }, {
                    "dataPoint1": "7",
                    "dataPoint2": "3.5",
                    "dataPoint3": "4",
                    "dataPoint4": "5",
                    "dataPoint5": "2",
                    "label": "d3"
                }, {
                    "dataPoint1": "8",
                    "dataPoint2": "7",
                    "dataPoint3": "5",
                    "dataPoint4": "12",
                    "dataPoint5": "7",
                    "label": "d4"
                }, {
                    "dataPoint1": "5",
                    "dataPoint2": "3",
                    "dataPoint3": "6",
                    "dataPoint4": "3",
                    "dataPoint5": "5",
                    "label": "d5"
                }, {
                    "dataPoint1": "14",
                    "dataPoint2": "5",
                    "dataPoint3": "9",
                    "dataPoint4": "7",
                    "dataPoint5": "2.5",
                    "label": "d6"
                }, {
                    "dataPoint1": "10",
                    "dataPoint2": "8",
                    "dataPoint3": "12",
                    "dataPoint4": "1",
                    "dataPoint5": "6",
                    "label": "d7"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Label Name",
                    "columnHeaderType": "text",
                    "columnID": "label",
                    "columnOnClick": null,
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f1a80a1c509a45d0ad84b3097d1e8cb7"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Data Point 1",
                    "columnHeaderType": "text",
                    "columnID": "dataPoint1",
                    "columnOnClick": null,
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "c4dc7111a5f24c0da4cf8236ddad5233"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Data Point 2",
                    "columnHeaderType": "text",
                    "columnID": "dataPoint2",
                    "columnOnClick": null,
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "f1d0650517b74932a901332535d13774"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Data Point 3",
                    "columnHeaderType": "text",
                    "columnID": "dataPoint3",
                    "columnOnClick": null,
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "a3503e93d043484181b74200a4766590"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Data Point 4",
                    "columnHeaderType": "text",
                    "columnID": "dataPoint4",
                    "columnOnClick": null,
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "ad8bace11c4149dc84a5095737d27353"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Data Point 5",
                    "columnHeaderType": "text",
                    "columnID": "dataPoint5",
                    "columnOnClick": null,
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "bdf89da9360049249fd48bfad21f5544"
                }]
            };
            multiline.enableGrid = true;
            multiline.lineDetails = {
                "data": [{
                    "color": "#1B9ED9",
                    "legendName": "blue"
                }, {
                    "color": "#76C044",
                    "legendName": "green"
                }, {
                    "color": "#F26B29",
                    "legendName": "orange"
                }, {
                    "color": "#7A54A3",
                    "legendName": "purple"
                }, {
                    "color": "#FFC522",
                    "legendName": "yellow"
                }],
                "schema": [{
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Legend Name",
                    "columnHeaderType": "text",
                    "columnID": "legendName",
                    "columnOnClick": null,
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "h0467a7d91b141dc93a6c0f7a92cc760"
                }, {
                    "columnHeaderTemplate": null,
                    "columnHeaderText": "Color",
                    "columnHeaderType": "text",
                    "columnID": "color",
                    "columnOnClick": null,
                    "columnText": "Not Defined",
                    "columnType": "text",
                    "kuid": "h785f87005da4e20ac4cc850e99e81e8"
                }]
            };
            multiline.yAxisTitle = "y-axis";
            multiline.legendFontSize = "95%";
            multiline.enableGridAnimation = false;
            multiline.titleFontSize = "12";
            multiline.lowValue = "0";
            multiline.titleFontColor = "#000000";
            multiline.legendFontColor = "#000000";
            multiline.highValue = "50";
            multiline.bgColor = "#FFFFFF";
            multiline.enableChartAnimation = true;
            multiline.enableStaticPreview = false;
            var holidayLineGraph = new com.temenos.chartjslib.barGraph1({
                "height": "95%",
                "id": "holidayLineGraph",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "barGraph1": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWbargraph1": {
                        "canvasHeight": "",
                        "canvasWidth": ""
                    }
                }
            });
            flxGraph.add(multiline, holidayLineGraph);
            var lblHolidayStartDateInfo = new kony.ui.Label({
                "height": "21dp",
                "id": "lblHolidayStartDateInfo",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Holiday Start Date",
                "top": "520dp",
                "width": "119dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxHolidayDateCalendar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxHolidayDateCalendar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "12%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "505dp",
                "width": "190dp",
                "zIndex": 5
            }, {}, {});
            flxHolidayDateCalendar.setDefaultUnit(kony.flex.DP);
            var cusHolidayDateCalendar = new kony.ui.CustomWidget({
                "id": "cusHolidayDateCalendar",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "190dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var calHolidayStartDate = new kony.ui.Calendar({
                "calendarIcon": "calendar.png",
                "dateComponents": [24, 11, 2020, 0, 0, 0],
                "dateFormat": "dd/MM/yyyy",
                "day": 24,
                "formattedDate": "24/11/2020",
                "height": "100%",
                "hour": 0,
                "id": "calHolidayStartDate",
                "isVisible": false,
                "left": "0dp",
                "minutes": 0,
                "month": 11,
                "seconds": 0,
                "skin": "CopyslCalendar0e8d17ca0b98e48",
                "top": "0dp",
                "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
                "width": "100%",
                "year": 2020,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var lblStartDateOverCalendar = new kony.ui.Label({
                "height": "100%",
                "id": "lblStartDateOverCalendar",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblBg000000px14",
                "text": "19 Nov 2020",
                "top": "-8dp",
                "width": "100%",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [10, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHolidayDateCalendar.add(cusHolidayDateCalendar, calHolidayStartDate, lblStartDateOverCalendar);
            var lblRecalculateInfo = new kony.ui.Label({
                "height": "21dp",
                "id": "lblRecalculateInfo",
                "isVisible": false,
                "left": "31%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Recalculate",
                "top": "520dp",
                "width": "76dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusDropDownRecalculate = new kony.ui.CustomWidget({
                "id": "cusDropDownRecalculate",
                "isVisible": false,
                "left": "38%",
                "top": "505dp",
                "width": "206dp",
                "height": "50dp",
                "zIndex": 5,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": true,
                "labelText": "",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var lblPaymentAmountDummyLabelInfo = new kony.ui.Label({
                "height": "56dp",
                "id": "lblPaymentAmountDummyLabelInfo",
                "isVisible": false,
                "left": "38%",
                "skin": "sknlbl14PX434343BGTransparent",
                "text": "Payment Amount",
                "top": "498dp",
                "width": "206dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubmit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "42dp",
                "id": "flxSubmit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "58dp",
                "skin": "slFbox",
                "top": "515dp",
                "width": "190dp",
                "zIndex": 1
            }, {}, {});
            flxSubmit.setDefaultUnit(kony.flex.DP);
            var cusButtonConfirm1 = new kony.ui.CustomWidget({
                "id": "cusButtonConfirm1",
                "isVisible": true,
                "left": 0,
                "right": "0%",
                "top": "0dp",
                "width": "179dp",
                "height": "42dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": " Confirm      ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxSubmit.add(cusButtonConfirm1);
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "42dp",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "272dp",
                "skin": "slFbox",
                "top": "515dp",
                "width": "190dp",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            var cusButtonCancel1 = new kony.ui.CustomWidget({
                "id": "cusButtonCancel1",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "179dp",
                "height": "42dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxCancel.add(cusButtonCancel1);
            var flxLineUpperPartButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLineUpperPartButton",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLine",
                "top": "482dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLineUpperPartButton.setDefaultUnit(kony.flex.DP);
            flxLineUpperPartButton.add();
            var flxRevisedPaymentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "230dp",
                "id": "flxRevisedPaymentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "73%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "47dp",
                "width": "25%",
                "zIndex": 5
            }, {}, {});
            flxRevisedPaymentWrapper.setDefaultUnit(kony.flex.DP);
            var lblRevisedPaymentDetailsHeader = new kony.ui.Label({
                "height": "24dp",
                "id": "lblRevisedPaymentDetailsHeader",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlbl20pxBG434343",
                "text": "Revised Payment Details",
                "top": "0dp",
                "width": "240dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRevisedPaymentAmountHeaderInfo = new kony.ui.Label({
                "height": "19dp",
                "id": "lblRevisedPaymentAmountHeaderInfo",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlbl16PX434343BGRotoRegular",
                "text": "Payment Amount",
                "top": "45dp",
                "width": "134dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRevisedPaymentAmountResponse = new kony.ui.Label({
                "height": "19dp",
                "id": "lblRevisedPaymentAmountResponse",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlbl16PX434343BG",
                "text": "EUR 8,518.30",
                "top": "75dp",
                "width": "119dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusProgressIndicatorRevisedPaymentAmount = new kony.ui.CustomWidget({
                "id": "cusProgressIndicatorRevisedPaymentAmount",
                "isVisible": false,
                "left": "0%",
                "top": "116dp",
                "width": "280dp",
                "height": "4dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxProgressIndicator",
                "indeterminate": true
            });
            var cusLinearProgressIndicatorRevisePaymentAmount = new kony.ui.CustomWidget({
                "id": "cusLinearProgressIndicatorRevisePaymentAmount",
                "isVisible": false,
                "left": "0%",
                "top": "116dp",
                "width": "280dp",
                "height": "4dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxLinearProgressStaging",
                "progress": null
            });
            var flxRevisedDateInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRevisedDateInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "134dp",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            flxRevisedDateInfo.setDefaultUnit(kony.flex.DP);
            var lblRevisedNextPaymentDateInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblRevisedNextPaymentDateInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl12px000000RotoRegular",
                "text": "Next Payment Date",
                "top": "0dp",
                "width": "107dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNextRevisedPaymentDateResponse = new kony.ui.Label({
                "height": "18dp",
                "id": "lblNextRevisedPaymentDateResponse",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl12px000000",
                "text": "19/12/2020",
                "top": "18dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRevisedMaturityDateInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblRevisedMaturityDateInfo",
                "isVisible": true,
                "left": "179dp",
                "skin": "sknlbl12px000000RotoRegular",
                "text": "New Maturity Date",
                "top": "0dp",
                "width": "116dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRevisedMaturityDateResponse = new kony.ui.Label({
                "height": "18dp",
                "id": "lblRevisedMaturityDateResponse",
                "isVisible": true,
                "left": "179dp",
                "skin": "sknlbl12px000000",
                "text": "19/12/2020",
                "top": "18dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRevisedDateInfo.add(lblRevisedNextPaymentDateInfo, lblNextRevisedPaymentDateResponse, lblRevisedMaturityDateInfo, lblRevisedMaturityDateResponse);
            var flxRevisedPaymentScheduleButton = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "flxRevisedPaymentScheduleButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRevisedPaymentScheduleButton.setDefaultUnit(kony.flex.DP);
            var lblRevisedPaymenySchedule = new kony.ui.Label({
                "height": "21dp",
                "id": "lblRevisedPaymenySchedule",
                "isVisible": true,
                "left": "40dp",
                "skin": "lblRevisedPaymentSchedule",
                "text": "See Revised Payment Schedule",
                "top": "185dp",
                "width": "210dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRevisedPaymentSchedule = new kony.ui.CustomWidget({
                "id": "imgRevisedPaymentSchedule",
                "isVisible": true,
                "left": "0dp",
                "top": "185dp",
                "width": "34px",
                "height": "34px",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "autorenew",
                "size": "small"
            });
            flxRevisedPaymentScheduleButton.add(lblRevisedPaymenySchedule, imgRevisedPaymentSchedule);
            var sldRevisedPayment = new kony.ui.Slider({
                "height": "4dp",
                "id": "sldRevisedPayment",
                "isVisible": true,
                "left": "0dp",
                "leftSkin": "sknsldBG2C8631",
                "max": 100,
                "min": 0,
                "rightSkin": "sknsldBGe5e5e5",
                "selectedValue": 20,
                "step": 1,
                "thumbImage": "slider.png",
                "top": "116dp",
                "width": "280dp",
                "zIndex": 1
            }, {}, {
                "orientation": constants.SLIDER_HORIZONTAL_ORIENTATION,
                "thickness": 4,
                "viewType": constants.SLIDER_VIEW_TYPE_PROGRESS
            });
            flxRevisedPaymentWrapper.add(lblRevisedPaymentDetailsHeader, lblRevisedPaymentAmountHeaderInfo, lblRevisedPaymentAmountResponse, cusProgressIndicatorRevisedPaymentAmount, cusLinearProgressIndicatorRevisePaymentAmount, flxRevisedDateInfo, flxRevisedPaymentScheduleButton, sldRevisedPayment);
            var flxCurrentPaymentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxCurrentPaymentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "47dp",
                "width": "320dp",
                "zIndex": 5
            }, {}, {});
            flxCurrentPaymentWrapper.setDefaultUnit(kony.flex.DP);
            var lblCurrentPaymentDetailsHeader = new kony.ui.Label({
                "height": "24dp",
                "id": "lblCurrentPaymentDetailsHeader",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlbl20pxBG434343",
                "text": "Current Payment Details",
                "top": "0dp",
                "width": "240dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPaymentAmountHeaderInfo = new kony.ui.Label({
                "height": "19dp",
                "id": "lblPaymentAmountHeaderInfo",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlbl16PX434343BGRotoRegular",
                "text": "Payment Amount",
                "top": "45dp",
                "width": "134dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPaymentAmountResponse = new kony.ui.Label({
                "height": "19dp",
                "id": "lblPaymentAmountResponse",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlbl16PX434343BG",
                "text": "EUR 8,518.30",
                "top": "75dp",
                "width": "119dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusProgressIndicatorPaymentAmount = new kony.ui.CustomWidget({
                "id": "cusProgressIndicatorPaymentAmount",
                "isVisible": false,
                "left": "0%",
                "top": "116dp",
                "width": "280dp",
                "height": "4dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxProgressIndicator",
                "indeterminate": true
            });
            var cusLinearProgressIndicatorPaymentAmount = new kony.ui.CustomWidget({
                "id": "cusLinearProgressIndicatorPaymentAmount",
                "isVisible": false,
                "left": "0%",
                "top": "116dp",
                "width": "280dp",
                "height": "4dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxLinearProgressStaging",
                "progress": null
            });
            var sldCurrentPayment = new kony.ui.Slider({
                "height": "4dp",
                "id": "sldCurrentPayment",
                "isVisible": true,
                "left": "0dp",
                "leftSkin": "sknsldBGFF9800",
                "max": 100,
                "min": 0,
                "rightSkin": "sknsldBGe5e5e5",
                "selectedValue": 2,
                "step": 1,
                "thumbImage": "slider.png",
                "top": "116dp",
                "width": "280dp",
                "zIndex": 1
            }, {}, {
                "orientation": constants.SLIDER_HORIZONTAL_ORIENTATION,
                "thickness": 4,
                "viewType": constants.SLIDER_VIEW_TYPE_PROGRESS
            });
            var flxDateInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDateInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "134dp",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            flxDateInfo.setDefaultUnit(kony.flex.DP);
            var lblNextPaymentDateInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblNextPaymentDateInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl12px000000RotoRegular",
                "text": "Next Payment Date",
                "top": "0dp",
                "width": "107dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNextPaymentDateResponse = new kony.ui.Label({
                "height": "18dp",
                "id": "lblNextPaymentDateResponse",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl12px000000",
                "text": "19/12/2020",
                "top": "18dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaturityDateInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblMaturityDateInfo",
                "isVisible": true,
                "left": "179dp",
                "skin": "sknlbl12px000000RotoRegular",
                "text": "Maturity Date",
                "top": "0dp",
                "width": "107dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaturityDateResponse = new kony.ui.Label({
                "height": "18dp",
                "id": "lblMaturityDateResponse",
                "isVisible": true,
                "left": "179dp",
                "skin": "sknlbl12px000000",
                "text": "19/12/2020",
                "top": "18dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDateInfo.add(lblNextPaymentDateInfo, lblNextPaymentDateResponse, lblMaturityDateInfo, lblMaturityDateResponse);
            flxCurrentPaymentWrapper.add(lblCurrentPaymentDetailsHeader, lblPaymentAmountHeaderInfo, lblPaymentAmountResponse, cusProgressIndicatorPaymentAmount, cusLinearProgressIndicatorPaymentAmount, sldCurrentPayment, flxDateInfo);
            var flxRightLoanLabels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxRightLoanLabels",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "190dp",
                "width": "300dp"
            }, {}, {});
            flxRightLoanLabels.setDefaultUnit(kony.flex.DP);
            var flxRound1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRound1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknFlxWhiteBgYellowBorder",
                "top": "0",
                "width": "136dp"
            }, {}, {});
            flxRound1.setDefaultUnit(kony.flex.DP);
            var blTitlePersonal = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "blTitlePersonal",
                "isVisible": true,
                "left": "8dp",
                "skin": "lblGraphLabel",
                "text": "Current Payment",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRound1.add(blTitlePersonal);
            var flxRound2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRound2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknlblGreenBorder",
                "top": "0",
                "width": "130dp"
            }, {}, {});
            flxRound2.setDefaultUnit(kony.flex.DP);
            var blTitleMortgage = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "blTitleMortgage",
                "isVisible": true,
                "left": "8dp",
                "skin": "lblGraphLabel",
                "text": "Revised Payment",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRound2.add(blTitleMortgage);
            flxRightLoanLabels.add(flxRound1, flxRound2);
            flxHolidayPaymentWrapper.add(flxPaymentHolidayOpted, flxGraph, lblHolidayStartDateInfo, flxHolidayDateCalendar, lblRecalculateInfo, cusDropDownRecalculate, lblPaymentAmountDummyLabelInfo, flxSubmit, flxCancel, flxLineUpperPartButton, flxRevisedPaymentWrapper, flxCurrentPaymentWrapper, flxRightLoanLabels);
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "64dp",
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "229dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var btnAccounts1 = new kony.ui.Button({
                "bottom": "12px",
                "height": "90%",
                "id": "btnAccounts1",
                "isVisible": true,
                "left": "20px",
                "onClick": controller.AS_Button_fb872159826b4baf9bc1a6cccf31cc32,
                "skin": "sknTabUnselected",
                "text": "Accounts",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts2 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts2",
                "isVisible": true,
                "left": "110px",
                "skin": "sknTabUnselected",
                "text": "Deposits",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts3 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts3",
                "isVisible": true,
                "left": "200px",
                "onClick": controller.AS_Button_b312ef910fe44c3da0ee24ff7d0c734d,
                "skin": "sknTabUnselected",
                "text": "Loans",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts4 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts4",
                "isVisible": false,
                "left": "290px",
                "skin": "sknTabUnselected",
                "text": "Bundles",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContainerAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxContainerAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContainerAccount.setDefaultUnit(kony.flex.DP);
            flxContainerAccount.add();
            var flxContainerdeposits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxContainerdeposits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "102dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContainerdeposits.setDefaultUnit(kony.flex.DP);
            flxContainerdeposits.add();
            var flxontainerLoans = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxontainerLoans",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "sknSelected",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxontainerLoans.setDefaultUnit(kony.flex.DP);
            flxontainerLoans.add();
            var flxContaineBundles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "9%",
                "id": "flxContaineBundles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "290dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContaineBundles.setDefaultUnit(kony.flex.DP);
            flxContaineBundles.add();
            var flxSeparatorLine1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLine1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknSeparatorBlue",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLine1.setDefaultUnit(kony.flex.DP);
            flxSeparatorLine1.add();
            flxDetails.add(btnAccounts1, btnAccounts2, btnAccounts3, btnAccounts4, flxContainerAccount, flxContainerdeposits, flxontainerLoans, flxContaineBundles, flxSeparatorLine1);
            var loanDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "loanDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "293dp",
                "width": "98%",
                "overrides": {
                    "FlexContainer0ebe191bc1cf946": {
                        "left": "-130dp"
                    },
                    "flxListBoxSelectedValue": {
                        "left": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "flxLoanDetailsvalue": {
                        "height": "63dp"
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "src": "drop_down_arrow.png"
                    },
                    "lblAccountNumberInfo": {
                        "width": "12.50%"
                    },
                    "lblAccountNumberResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblAccountServiceInfo": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "lblAmountValueInfo": {
                        "width": "12.50%"
                    },
                    "lblAmountValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblCurrencyValueInfo": {
                        "width": "12.50%"
                    },
                    "lblCurrencyValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Payment Holiday"
                    },
                    "lblLoanTypeInfo": {
                        "width": "13.20%"
                    },
                    "lblLoanTypeResponse": {
                        "centerY": "50%",
                        "width": "13.60%"
                    },
                    "lblMaturityDateInfo": {
                        "width": "12.50%"
                    },
                    "lblMaturityDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblStartDateInfo": {
                        "width": "12.50%"
                    },
                    "lblStartDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "loanDetails": {
                        "height": "128dp",
                        "left": "18dp",
                        "top": "293dp",
                        "width": "98%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainWrapper.add(ErrorAllert, flxHeaderMenu, custInfo, flxHolidayPaymentWrapper, flxDetails, loanDetails);
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "968dp",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var RevisedPaymentSchedule = new com.lending.revisedPaymentSchedule.RevisedPaymentSchedule({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "RevisedPaymentSchedule",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "RevisedPaymentSchedule": {
                        "isVisible": false
                    },
                    "imgClose1": {
                        "src": "ico_close.png"
                    },
                    "imgPrint": {
                        "src": "ico_print.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSkipUpcomingPaymentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSkipUpcomingPaymentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxSkipUpcomingPaymentWrapper.setDefaultUnit(kony.flex.DP);
            var flxSkipUpcomingSegWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSkipUpcomingSegWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "90dp",
                "isModalContainer": false,
                "skin": "sknflxBorder979797",
                "top": "20%",
                "width": "456dp",
                "zIndex": 1
            }, {}, {});
            flxSkipUpcomingSegWrapper.setDefaultUnit(kony.flex.DP);
            var flxSkipSegmentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSkipSegmentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSkipSegmentHeader.setDefaultUnit(kony.flex.DP);
            var lblSkipPaymentContentHeader = new kony.ui.Label({
                "height": "40dp",
                "id": "lblSkipPaymentContentHeader",
                "isVisible": true,
                "left": "32dp",
                "skin": "sknLbl24px005096BG",
                "text": "Skip Upcoming Payments",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBillDetailsClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxBillDetailsClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "85%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsClose.setDefaultUnit(kony.flex.DP);
            var imgCloseButtonBillDetails = new kony.ui.Button({
                "height": "100%",
                "id": "imgCloseButtonBillDetails",
                "isVisible": true,
                "right": "0dp",
                "skin": "CopysknBtnImgClose0fbefdef1633e42",
                "text": "L",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBillDetailsClose.add(imgCloseButtonBillDetails);
            flxSkipSegmentHeader.add(lblSkipPaymentContentHeader, flxBillDetailsClose);
            var flxSkipSegmentHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSkipSegmentHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "16dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSkipSegmentHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblPaymentDateSegmentHdrInfo = new kony.ui.Label({
                "height": "20dp",
                "id": "lblPaymentDateSegmentHdrInfo",
                "isVisible": true,
                "left": "15%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Payment Date",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmountSegHdrInfo = new kony.ui.Label({
                "height": "20dp",
                "id": "lblAmountSegHdrInfo",
                "isVisible": true,
                "left": "22%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Amount",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSkipSegmentHeaderInfo.add(lblPaymentDateSegmentHdrInfo, lblAmountSegHdrInfo);
            var segSkipUpcomingPayment = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "btnSelect1": "",
                    "chxBox": {
                        "masterData": [
                            ["-", "-"]
                        ],
                        "selectedKeys": ["-"],
                        "selectedKeyValues": [
                            ["-", "-"]
                        ]
                    },
                    "lblAmount1": "8,581.30",
                    "lblArrow": "",
                    "lblDate1": "19 Dec 2020"
                }, {
                    "btnSelect1": "",
                    "chxBox": {
                        "masterData": [
                            ["-", "-"]
                        ],
                        "selectedKeys": ["-"],
                        "selectedKeyValues": [
                            ["-", "-"]
                        ]
                    },
                    "lblAmount1": "8,581.30",
                    "lblArrow": "",
                    "lblDate1": "19 Dec 2020"
                }, {
                    "btnSelect1": "",
                    "chxBox": {
                        "masterData": [
                            ["-", "-"]
                        ],
                        "selectedKeys": ["-"],
                        "selectedKeyValues": [
                            ["-", "-"]
                        ]
                    },
                    "lblAmount1": "8,581.30",
                    "lblArrow": "",
                    "lblDate1": "19 Dec 2020"
                }, {
                    "btnSelect1": "",
                    "chxBox": {
                        "masterData": [
                            ["-", "-"]
                        ],
                        "selectedKeys": ["-"],
                        "selectedKeyValues": [
                            ["-", "-"]
                        ]
                    },
                    "lblAmount1": "8,581.30",
                    "lblArrow": "",
                    "lblDate1": "19 Dec 2020"
                }, {
                    "btnSelect1": "",
                    "chxBox": {
                        "masterData": [
                            ["-", "-"]
                        ],
                        "selectedKeys": ["-"],
                        "selectedKeyValues": [
                            ["-", "-"]
                        ]
                    },
                    "lblAmount1": "8,581.30",
                    "lblArrow": "",
                    "lblDate1": "19 Dec 2020"
                }, {
                    "btnSelect1": "",
                    "chxBox": {
                        "masterData": [
                            ["-", "-"]
                        ],
                        "selectedKeys": ["-"],
                        "selectedKeyValues": [
                            ["-", "-"]
                        ]
                    },
                    "lblAmount1": "8,581.30",
                    "lblArrow": "",
                    "lblDate1": "19 Dec 2020"
                }],
                "groupCells": false,
                "id": "segSkipUpcomingPayment",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxrow1",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "16dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnSelect1": "btnSelect1",
                    "chxBox": "chxBox",
                    "flxrow1": "flxrow1",
                    "lblAmount1": "lblAmount1",
                    "lblArrow": "lblArrow",
                    "lblDate1": "lblDate1"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLine3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLine",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLine3.setDefaultUnit(kony.flex.DP);
            flxLine3.add();
            var flxSkipPaymentButtonGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxSkipPaymentButtonGroup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "8dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSkipPaymentButtonGroup.setDefaultUnit(kony.flex.DP);
            var flxSkipCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "42dp",
                "id": "flxSkipCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "233dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": "179dp",
                "zIndex": 6
            }, {}, {});
            flxSkipCancel.setDefaultUnit(kony.flex.DP);
            var cusButtonSkipCancel = new kony.ui.CustomWidget({
                "id": "cusButtonSkipCancel",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "179dp",
                "height": "42dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxSkipCancel.add(cusButtonSkipCancel);
            var flxSkipConfirm1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSkipConfirm1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": "150dp",
                "zIndex": 5
            }, {}, {});
            flxSkipConfirm1.setDefaultUnit(kony.flex.DP);
            var cusButtonSkipConfirm = new kony.ui.CustomWidget({
                "id": "cusButtonSkipConfirm",
                "isVisible": true,
                "left": 0,
                "right": "0%",
                "top": "2dp",
                "width": "150dp",
                "height": "46dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": " Confirm      ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "X-large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxSkipConfirm1.add(cusButtonSkipConfirm);
            var flxSkipConfirm = new com.temenos.uuxButton({
                "height": "42dp",
                "id": "flxSkipConfirm",
                "isVisible": true,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": "179dp",
                "zIndex": 5,
                "overrides": {
                    "uuxButton": {
                        "left": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "UUXButtonWrapper": {
                        "compact": false,
                        "labelText": "Confirm",
                        "size": {
                            "optionsList": ["small", "medium", "large", "X-large"],
                            "selectedValue": "large"
                        },
                        "typeButton": {
                            "optionsList": ["button", "submit", "reset"],
                            "selectedValue": "submit"
                        }
                    }
                }
            });
            flxSkipPaymentButtonGroup.add(flxSkipCancel, flxSkipConfirm1, flxSkipConfirm);
            flxSkipUpcomingSegWrapper.add(flxSkipSegmentHeader, flxSkipSegmentHeaderInfo, segSkipUpcomingPayment, flxLine3, flxSkipPaymentButtonGroup);
            flxSkipUpcomingPaymentWrapper.add(flxSkipUpcomingSegWrapper);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0f58a94933b9d4d"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0cc9b209c00054f"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_dfaf8836f2794372be8ca22cf10d3a61;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_gfcea63cafcf45a2b255c3654117fc85;
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "ErrorAllert.imgCloseBackup": {
                    "src": "ico_close.png"
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "multiline": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "holidayLineGraph": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "loanDetails.FlexContainer0ebe191bc1cf946": {
                    "left": "-130dp"
                },
                "loanDetails.flxListBoxSelectedValue": {
                    "left": "",
                    "minWidth": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.flxLoanDetailsvalue": {
                    "height": "63dp"
                },
                "loanDetails.imgListBoxSelectedDropDown": {
                    "src": "drop_down_arrow.png"
                },
                "loanDetails.lblAccountNumberInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAccountNumberResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountServiceInfo": {
                    "left": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblListBoxSelectedValue": {
                    "text": "Payment Holiday"
                },
                "loanDetails.lblLoanTypeInfo": {
                    "width": "13.20%"
                },
                "loanDetails.lblLoanTypeResponse": {
                    "centerY": "50%",
                    "width": "13.60%"
                },
                "loanDetails.lblMaturityDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails": {
                    "height": "128dp",
                    "left": "18dp",
                    "top": "293dp",
                    "width": "98%"
                },
                "RevisedPaymentSchedule.imgClose1": {
                    "src": "ico_close.png"
                },
                "RevisedPaymentSchedule.imgPrint": {
                    "src": "ico_print.png"
                },
                "flxSkipConfirm": {
                    "left": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0f58a94933b9d4d",
                    "skin2": "CopydefBtnNormal0cc9b209c00054f",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMainWrapper, SegmentPopup, RevisedPaymentSchedule, flxSkipUpcomingPaymentWrapper, uuxNavigationRail, ProgressIndicator, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmHolidayPayment,
            "enabledForIdleTimeout": false,
            "id": "frmHolidayPayment",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});