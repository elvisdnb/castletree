define("flxRowBlockFunds", function() {
    return function(controller) {
        var flxRowBlockFunds = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "56dp",
            "id": "flxRowBlockFunds",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {
            "hoverSkin": "sknFlxRowBgE3D3FE"
        });
        flxRowBlockFunds.setDefaultUnit(kony.flex.DP);
        var lblReferenceNo = new kony.ui.Label({
            "centerY": "50%",
            "height": "21dp",
            "id": "lblReferenceNo",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknLblBg000000px14",
            "text": "REF NO",
            "top": 18,
            "width": "120dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStartDate = new kony.ui.Label({
            "centerY": "52%",
            "height": "21dp",
            "id": "lblStartDate",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblBg000000px14",
            "top": 18,
            "width": "92dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEndDate = new kony.ui.Label({
            "centerY": "52%",
            "height": "21dp",
            "id": "lblEndDate",
            "isVisible": true,
            "left": "8dp",
            "skin": "sknLblBg000000px14",
            "top": 18,
            "width": "92dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmount = new kony.ui.Label({
            "centerY": "52%",
            "height": "21dp",
            "id": "lblAmount",
            "isVisible": true,
            "left": "8dp",
            "skin": "sknLblBg000000px14",
            "text": "0.00",
            "top": 18,
            "width": "96dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblReason = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblReason",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblBg000000px14",
            "text": "Reason",
            "top": 18,
            "width": "120dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnEdit = new kony.ui.Button({
            "centerY": "50%",
            "height": "32dp",
            "id": "btnEdit",
            "isVisible": true,
            "left": "16dp",
            "skin": "skinButtonIcons",
            "text": "C",
            "width": "32dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnUnblock = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "skinButtonIcons",
            "height": "32dp",
            "id": "btnUnblock",
            "isVisible": true,
            "left": "24dp",
            "skin": "skinButtonIcons",
            "text": "U",
            "width": "32dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRowBlockFunds.add(lblReferenceNo, lblStartDate, lblEndDate, lblAmount, lblReason, btnEdit, btnUnblock);
        return flxRowBlockFunds;
    }
})