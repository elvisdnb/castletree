define("flxAddCutomerRow", function() {
    return function(controller) {
        var flxAddCutomerRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddCutomerRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxAddCutomerRow.setDefaultUnit(kony.flex.DP);
        var flxBenName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55px",
            "id": "flxBenName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "CopyslFbox0jeb2ba62ae294c",
            "top": "0",
            "width": "295dp"
        }, {}, {});
        flxBenName.setDefaultUnit(kony.flex.DP);
        var lblBenLabel = new kony.ui.Label({
            "height": "20px",
            "id": "lblBenLabel",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopydefLabel0d27046b6aa674d",
            "text": "Beneficiary Name",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRole = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "CopydefTextBoxNormal0c79c0816252940",
            "height": "30px",
            "id": "lblRole",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "10dp",
            "secureTextEntry": false,
            "skin": "CopydefTextBoxNormal0c79c0816252940",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0",
            "width": "75%",
            "isSensitiveText": false
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "hoverSkin": "CopydefTextBoxNormal0c79c0816252940",
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var flxImgClicks = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxImgClicks",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 10,
            "skin": "sknSearch",
            "top": "-35dp",
            "width": "25dp"
        }, {}, {});
        flxImgClicks.setDefaultUnit(kony.flex.DP);
        flxImgClicks.add();
        flxBenName.add(lblBenLabel, lblRole, flxImgClicks);
        var flxBenRole = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55px",
            "id": "flxBenRole",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0jeb2ba62ae294c",
            "top": "0",
            "width": "295dp"
        }, {}, {});
        flxBenRole.setDefaultUnit(kony.flex.DP);
        var lblBenLabel1 = new kony.ui.Label({
            "height": "20px",
            "id": "lblBenLabel1",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopydefLabel0d27046b6aa674d",
            "text": "Role",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbxBenRole = new kony.ui.ListBox({
            "focusSkin": "lbxNoBorder",
            "height": "30dp",
            "id": "lbxBenRole",
            "isVisible": true,
            "left": "0",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "lbxNoBorder",
            "top": "0",
            "width": "100%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "lbxNoBorder",
            "multiSelect": false
        });
        flxBenRole.add(lblBenLabel1, lbxBenRole);
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55dp",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100dp"
        }, {}, {});
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblDeleteIcon = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDeleteIcon",
            "isVisible": true,
            "left": "0",
            "skin": "castleicon",
            "text": "Q",
            "top": "0",
            "width": "40dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDelete = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDelete",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0g1225c742c9844",
            "text": "Delete",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(lblDeleteIcon, lblDelete);
        flxAddCutomerRow.add(flxBenName, flxBenRole, flxDelete);
        return flxAddCutomerRow;
    }
})