define("flxOutstandingBillDetails", function() {
    return function(controller) {
        var flxOutstandingBillDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxOutstandingBillDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxOutstandingBillDetails.setDefaultUnit(kony.flex.DP);
        var lblBillIdInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblBillIdInfo",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknlblBG0075DBpx14Underline",
            "text": "   Bill ID",
            "top": "2dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDueDateInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblDueDateInfo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlbl14PX434343BG",
            "text": "   Due Date",
            "top": "2dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTypeInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblTypeInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl14PX434343BG",
            "text": "   Type",
            "top": "2dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblBilledInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblBilledInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl14PX434343BG",
            "text": "   Billed",
            "top": "2dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOutstandingInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblOutstandingInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl14PX434343BG",
            "text": "   Outstanding",
            "top": "2dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatusInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblStatusInfo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblFFF1F1BG12pxRedBorder",
            "text": "Status",
            "top": "2dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOutstandingBillDetails.add(lblBillIdInfo, lblDueDateInfo, lblTypeInfo, lblBilledInfo, lblOutstandingInfo, lblStatusInfo);
        return flxOutstandingBillDetails;
    }
})