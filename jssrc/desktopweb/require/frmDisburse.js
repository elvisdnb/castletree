define("frmDisburse", function() {
    return function(controller) {
        function addWidgetsfrmDisburse() {
            this.setDefaultUnit(kony.flex.DP);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0i02334c0f3cf43"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0cfa169294d7b41"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_b113ea7d342147d3aed08d12d072c086;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_b7da5a251fac4ab0a3f9940d5ff973fa;
            var flxMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5%",
                "isModalContainer": false,
                "right": 24,
                "skin": "CopyslFbox0hd3813399c724e",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "300dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "112dp",
                "skin": "CopysknLbl0d3827a02244b46",
                "text": "Overview",
                "top": "26dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadboardDisburseSplash = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadboardDisburseSplash",
                "isVisible": true,
                "left": "180dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "26dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardDisbursment = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardDisbursment",
                "isVisible": true,
                "left": "200dp",
                "skin": "sknLblLoanServiceBlacl12px",
                "text": "Disburse",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue, lblBreadboardDisburseSplash, lblBreadBoardDisbursment);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "64dp",
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var btnAccounts1 = new kony.ui.Button({
                "bottom": "12px",
                "height": "90%",
                "id": "btnAccounts1",
                "isVisible": true,
                "left": "20px",
                "onClick": controller.AS_Button_fb872159826b4baf9bc1a6cccf31cc32,
                "skin": "sknTabUnselected",
                "text": "Accounts",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts2 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts2",
                "isVisible": true,
                "left": "110px",
                "skin": "sknTabUnselected",
                "text": "Deposits",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts3 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts3",
                "isVisible": true,
                "left": "200px",
                "onClick": controller.AS_Button_b312ef910fe44c3da0ee24ff7d0c734d,
                "skin": "sknTabUnselected",
                "text": "Loans",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts4 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts4",
                "isVisible": false,
                "left": "290px",
                "skin": "sknTabUnselected",
                "text": "Bundles",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContainerAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxContainerAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContainerAccount.setDefaultUnit(kony.flex.DP);
            flxContainerAccount.add();
            var flxContainerdeposits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxContainerdeposits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "102dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContainerdeposits.setDefaultUnit(kony.flex.DP);
            flxContainerdeposits.add();
            var flxontainerLoans = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxontainerLoans",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "sknSelected",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxontainerLoans.setDefaultUnit(kony.flex.DP);
            flxontainerLoans.add();
            var flxContaineBundles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "9%",
                "id": "flxContaineBundles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "290dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContaineBundles.setDefaultUnit(kony.flex.DP);
            flxContaineBundles.add();
            flxDetails.add(btnAccounts1, btnAccounts2, btnAccounts3, btnAccounts4, flxContainerAccount, flxContainerdeposits, flxontainerLoans, flxContaineBundles);
            var flxSeparatorLine1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLine1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknSeparatorBlue",
                "top": "1dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLine1.setDefaultUnit(kony.flex.DP);
            flxSeparatorLine1.add();
            var loanDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "loanDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "overrides": {
                    "FlexContainer0ebe191bc1cf946": {
                        "width": "270dp"
                    },
                    "flxListBoxSelectedValue": {
                        "left": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "flxLoanDetailsvalue": {
                        "height": "63dp"
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "src": "drop_down_arrow.png"
                    },
                    "lblAccountNumberInfo": {
                        "width": "12.50%"
                    },
                    "lblAccountNumberResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblAccountServiceInfo": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "lblAmountValueInfo": {
                        "width": "12.50%"
                    },
                    "lblAmountValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblCurrencyValueInfo": {
                        "width": "12.50%"
                    },
                    "lblCurrencyValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Disburse"
                    },
                    "lblLoanTypeInfo": {
                        "width": "13.20%"
                    },
                    "lblLoanTypeResponse": {
                        "centerY": "50%",
                        "width": "13.60%"
                    },
                    "lblMaturityDateInfo": {
                        "width": "12.50%"
                    },
                    "lblMaturityDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblStartDateInfo": {
                        "width": "12.50%"
                    },
                    "lblStartDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "loanDetails": {
                        "height": "128dp",
                        "left": "18dp",
                        "top": "0dp",
                        "width": "98%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDisburse = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxDisburse",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0cc51b62fbfd947",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxDisburse.setDefaultUnit(kony.flex.DP);
            var TitleLabel = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "47%",
                "height": "70dp",
                "id": "TitleLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "880dp",
                "overrides": {
                    "TitleLabel": {
                        "centerX": "47%",
                        "height": "70dp",
                        "width": "880dp"
                    },
                    "flxTitle": {
                        "isVisible": true
                    },
                    "lblTitle": {
                        "centerX": "18%",
                        "isVisible": true,
                        "left": "10dp",
                        "text": "New Disbursement"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var FlexContainer0c3e385c337024d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "FlexContainer0c3e385c337024d",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fda5754cd9f44b",
                "top": "0dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0c3e385c337024d.setDefaultUnit(kony.flex.DP);
            FlexContainer0c3e385c337024d.add();
            var flxNewDisbursement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "40dp",
                "centerX": "47%",
                "clipBounds": false,
                "id": "flxNewDisbursement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f90ec32563c34c",
                "top": "2dp",
                "width": "880px",
                "zIndex": 4
            }, {}, {});
            flxNewDisbursement.setDefaultUnit(kony.flex.DP);
            var custTotalLoanAmount1 = new kony.ui.CustomWidget({
                "id": "custTotalLoanAmount1",
                "isVisible": false,
                "left": "10dp",
                "top": "120dp",
                "width": "200dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": true,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Total Loan Amount",
                "maxLength": "",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var custAviableCommit1 = new kony.ui.CustomWidget({
                "id": "custAviableCommit1",
                "isVisible": false,
                "left": "322dp",
                "top": "120dp",
                "width": "200dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": true,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Available Commitment",
                "maxLength": "",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var lblNewDisburse = new kony.ui.Label({
                "id": "lblNewDisburse",
                "isVisible": false,
                "left": "8%",
                "skin": "sknLblBlue16px",
                "text": "New Disbursement",
                "top": "33dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectMode = new kony.ui.Label({
                "height": "21dp",
                "id": "lblSelectMode",
                "isVisible": false,
                "left": "99dp",
                "skin": "CopydefLabel0jd9f808d624d45",
                "text": "Select mode",
                "top": "95dp",
                "width": "94dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custAccountNumber = new kony.ui.CustomWidget({
                "id": "custAccountNumber",
                "isVisible": false,
                "left": "102dp",
                "top": "152dp",
                "width": "200dp",
                "height": "60dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Loan Account No.",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custAvaliableCommitment = new kony.ui.CustomWidget({
                "id": "custAvaliableCommitment",
                "isVisible": false,
                "left": "334dp",
                "top": "152dp",
                "width": "200dp",
                "height": "61dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Available Commitment",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var tbxPayOutAccount = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "56dp",
                "id": "tbxPayOutAccount",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "634dp",
                "onTouchStart": controller.AS_TextField_j4b048bc619c4ce1b26bba5669ca874f,
                "placeholder": "Credit Account No. *",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ha41baabc32443",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "120dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxpayOutSearch1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayOutSearch1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 830,
                "isModalContainer": false,
                "skin": "searchflex",
                "top": 20,
                "width": "40dp"
            }, {}, {});
            flxpayOutSearch1.setDefaultUnit(kony.flex.DP);
            var iconPayoutsearch1 = new kony.ui.CustomWidget({
                "id": "iconPayoutsearch1",
                "isVisible": true,
                "left": "0dp",
                "top": "0",
                "width": "40dp",
                "height": "40dp",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-light",
                "iconName": "search",
                "size": "medium"
            });
            var iconPayoutsearch2 = new kony.ui.Button({
                "height": "40dp",
                "id": "iconPayoutsearch2",
                "isVisible": true,
                "left": "0dp",
                "skin": "castleicon150pctprimary",
                "text": "k",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxpayOutSearch1.add(iconPayoutsearch1, iconPayoutsearch2);
            var custDisburseAmount1 = new kony.ui.CustomWidget({
                "id": "custDisburseAmount1",
                "isVisible": false,
                "left": "10dp",
                "top": "186dp",
                "width": "200dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Amount To Disburse",
                "maxLength": "",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validationMessage": "Please enter disburse amount",
                "value": ""
            });
            var lblErrorMsg = new kony.ui.Label({
                "height": "40dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "10dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "162dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custSelectMode = new kony.ui.CustomWidget({
                "id": "custSelectMode",
                "isVisible": false,
                "left": "220dp",
                "top": "89dp",
                "width": "300dp",
                "height": "27dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRadioGroup",
                "labelText": "",
                "options": {
                    "selectedValue": ""
                },
                "value": ""
            });
            var flxPayoutComp = new com.konymp.AccountList.AccountList({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 27,
                "height": "180dp",
                "id": "flxPayoutComp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "634dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxWhiteBGBorder0075db",
                "top": "60dp",
                "width": "40%",
                "zIndex": 10,
                "overrides": {
                    "AccountList": {
                        "bottom": 27,
                        "height": "180dp",
                        "isVisible": false,
                        "left": "634dp",
                        "top": "60dp",
                        "width": "40%"
                    },
                    "segAccountList": {
                        "height": "140dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var custTotalLoanAmount = new kony.ui.CustomWidget({
                "id": "custTotalLoanAmount",
                "isVisible": true,
                "left": "10dp",
                "top": "20dp",
                "width": "200dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Total Loan Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var custAviableCommit = new kony.ui.CustomWidget({
                "id": "custAviableCommit",
                "isVisible": true,
                "left": "322dp",
                "top": "20dp",
                "width": "200dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": null,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Available Commitment",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var floatCreditAccount = new com.lending.floatingTextBox.floatLabelText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "floatCreditAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "634dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "flxTemenosLighest",
                "top": "20dp",
                "width": "200dp",
                "zIndex": 3,
                "overrides": {
                    "downArrowLbl": {
                        "height": "30%",
                        "isVisible": true
                    },
                    "floatLabelText": {
                        "centerX": "viz.val_cleared",
                        "height": "40dp",
                        "left": "634dp",
                        "top": "20dp",
                        "width": "200dp"
                    },
                    "lblFloatLabel": {
                        "left": "10dp",
                        "text": "Credit Account No."
                    },
                    "rtAsterix": {
                        "centerY": "56%",
                        "height": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var custDisburseAmount = new kony.ui.CustomWidget({
                "id": "custDisburseAmount",
                "isVisible": true,
                "left": "10dp",
                "top": "88dp",
                "width": "200dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "EUR",
                "decimals": null,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Amount To Disburse",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "suffix": "",
                "validateRequired": "required",
                "value": ""
            });
            var custDisbursementDate = new kony.ui.CustomWidget({
                "id": "custDisbursementDate",
                "isVisible": true,
                "left": "322dp",
                "top": "88dp",
                "width": "200dp",
                "height": "55dp",
                "zIndex": 2,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "dd MMM yyyy",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Disbursement Date",
                "max": "",
                "min": "1900-01-01",
                "onDone": "",
                "showWeekNumber": true,
                "value": "",
                "weekLabel": "Wk"
            });
            var custButtonCancel = new kony.ui.CustomWidget({
                "id": "custButtonCancel",
                "isVisible": true,
                "left": "414dp",
                "top": "190dp",
                "width": "196px",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custButtonConfirm = new kony.ui.CustomWidget({
                "id": "custButtonConfirm",
                "isVisible": true,
                "left": "646dp",
                "top": "190dp",
                "width": "204px",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxpayOutSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayOutSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "830dp",
                "isModalContainer": false,
                "skin": "searchflex",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxpayOutSearch.setDefaultUnit(kony.flex.DP);
            var CopyiconPayoutsearch0b12dc253ba9647 = new kony.ui.CustomWidget({
                "id": "CopyiconPayoutsearch0b12dc253ba9647",
                "isVisible": false,
                "left": "0dp",
                "top": "0",
                "width": "40dp",
                "height": "40dp",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-light",
                "iconName": "search",
                "size": "medium"
            });
            var iconPayoutsearch = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "iconPayoutsearch",
                "isVisible": true,
                "left": "0%",
                "skin": "iconFontPurple20px",
                "text": "k",
                "top": "0",
                "width": "20dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxpayOutSearch.add(CopyiconPayoutsearch0b12dc253ba9647, iconPayoutsearch);
            flxNewDisbursement.add(custTotalLoanAmount1, custAviableCommit1, lblNewDisburse, lblSelectMode, custAccountNumber, custAvaliableCommitment, tbxPayOutAccount, flxpayOutSearch1, custDisburseAmount1, lblErrorMsg, custSelectMode, flxPayoutComp, custTotalLoanAmount, custAviableCommit, floatCreditAccount, custDisburseAmount, custDisbursementDate, custButtonCancel, custButtonConfirm, flxpayOutSearch);
            var flxSeparatorLineGrey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLineGrey",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSeparatorGrey979797",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLineGrey.setDefaultUnit(kony.flex.DP);
            flxSeparatorLineGrey.add();
            var flxDisburseHistory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "400px",
                "id": "flxDisburseHistory",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ef3b54af18e04f",
                "top": "-459dp",
                "width": "407px",
                "zIndex": 4
            }, {}, {});
            flxDisburseHistory.setDefaultUnit(kony.flex.DP);
            var lbDisburseHistory = new kony.ui.Label({
                "height": "20px",
                "id": "lbDisburseHistory",
                "isVisible": true,
                "left": "17px",
                "skin": "sknLblBlue16px",
                "text": "Disbursement History",
                "top": "33dp",
                "width": "161px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDisburseSpace = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxDisburseSpace",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a3a24d2b191e4d",
                "top": "77dp",
                "width": "407px",
                "zIndex": 1
            }, {}, {});
            flxDisburseSpace.setDefaultUnit(kony.flex.DP);
            flxDisburseSpace.add();
            flxDisburseHistory.add(lbDisburseHistory, flxDisburseSpace);
            flxDisburse.add(TitleLabel, FlexContainer0c3e385c337024d, flxNewDisbursement, flxSeparatorLineGrey, flxDisburseHistory);
            flxMainContainer.add(flxHeaderMenu, custInfo, flxDetails, flxSeparatorLine1, loanDetails, flxDisburse);
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "968dp",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "isVisible": false,
                        "right": "viz.val_cleared",
                        "width": "100%"
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    },
                    "segOverride": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64dp",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "2dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageWithReferenceNumber = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMessageWithReferenceNumber",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BG",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgHeaderClose = new kony.ui.Image2({
                "height": "14dp",
                "id": "imgHeaderClose",
                "isVisible": true,
                "left": "1250dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "top": "17dp",
                "width": "14dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblMessageWithReferenceNumber, imgHeaderClose);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "87dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 37,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "ErrorAllert": {
                        "height": "70dp",
                        "isVisible": false,
                        "left": "87dp",
                        "right": 37
                    },
                    "flxError": {
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    },
                    "imgCloseBackup": {
                        "centerY": "50%",
                        "height": "18dp",
                        "left": "viz.val_cleared",
                        "right": 20,
                        "src": "ico_close.png",
                        "top": "viz.val_cleared"
                    },
                    "lblMessage": {
                        "centerY": "50%",
                        "text": "Subtitle text goes here",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "930dp",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxpayInOutPOP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "800dp",
                "id": "flxpayInOutPOP",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": 24,
                "skin": "CopyslFbox0g815ca6a691d4d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxpayInOutPOP.setDefaultUnit(kony.flex.DP);
            var flxPayout = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "flxPayout",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0e5562f0a71eb42",
                "top": "0",
                "width": "50%"
            }, {}, {});
            flxPayout.setDefaultUnit(kony.flex.DP);
            var popupPayinPayout2 = new com.konymp.popupPayinPayout.popupPayinPayout({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popupPayinPayout2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0e816618aedef47",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgClose1": {
                        "src": "ico_close.png"
                    },
                    "imgSrc1": {
                        "src": "search.png"
                    },
                    "popupPayinPayout": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            popupPayinPayout2.imgClose1.onTouchEnd = controller.AS_Image_ae9428a3f7d54919b7720ac15c9c7941;
            popupPayinPayout2.segPayinPayout.onRowClick = controller.AS_Segment_cd66c81de52e4de29e68381495b002db;
            var popupPayinPayout = new com.konymp.popupPayinPayout.popupPayinPayout({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "id": "popupPayinPayout",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0e816618aedef47",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "FlexContainer0e3c37c0e40204d": {
                        "centerX": "50%",
                        "height": "250dp"
                    },
                    "flxInnerSearch": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "top": "0px"
                    },
                    "lblAccountIdhead": {
                        "width": "23%"
                    },
                    "lblCustomerIdhead": {
                        "width": "15%"
                    },
                    "lblProductName": {
                        "width": "28%"
                    },
                    "popupPayinPayout": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "segPayinPayout": {
                        "height": "200dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPayout.add(popupPayinPayout2, popupPayinPayout);
            flxpayInOutPOP.add(flxPayout);
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0i02334c0f3cf43",
                    "skin2": "CopydefBtnNormal0cfa169294d7b41",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "loanDetails.FlexContainer0ebe191bc1cf946": {
                    "width": "270dp"
                },
                "loanDetails.flxListBoxSelectedValue": {
                    "left": "",
                    "minWidth": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.flxLoanDetailsvalue": {
                    "height": "63dp"
                },
                "loanDetails.imgListBoxSelectedDropDown": {
                    "src": "drop_down_arrow.png"
                },
                "loanDetails.lblAccountNumberInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAccountNumberResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountServiceInfo": {
                    "left": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblListBoxSelectedValue": {
                    "text": "Disburse"
                },
                "loanDetails.lblLoanTypeInfo": {
                    "width": "13.20%"
                },
                "loanDetails.lblLoanTypeResponse": {
                    "centerY": "50%",
                    "width": "13.60%"
                },
                "loanDetails.lblMaturityDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails": {
                    "height": "128dp",
                    "left": "18dp",
                    "top": "0dp",
                    "width": "98%"
                },
                "TitleLabel": {
                    "centerX": "47%",
                    "height": "70dp",
                    "width": "880dp"
                },
                "TitleLabel.lblTitle": {
                    "centerX": "18%",
                    "left": "10dp",
                    "text": "New Disbursement"
                },
                "flxPayoutComp": {
                    "bottom": 27,
                    "height": "180dp",
                    "left": "634dp",
                    "top": "60dp",
                    "width": "40%"
                },
                "flxPayoutComp.segAccountList": {
                    "height": "140dp"
                },
                "floatCreditAccount.downArrowLbl": {
                    "height": "30%"
                },
                "floatCreditAccount": {
                    "centerX": "",
                    "height": "40dp",
                    "left": "634dp",
                    "top": "20dp",
                    "width": "200dp"
                },
                "floatCreditAccount.lblFloatLabel": {
                    "left": "10dp",
                    "text": "Credit Account No."
                },
                "floatCreditAccount.rtAsterix": {
                    "centerY": "56%",
                    "height": "20dp"
                },
                "SegmentPopup": {
                    "right": "",
                    "width": "100%"
                },
                "SegmentPopup.imgClose": {
                    "src": "ico_close.png"
                },
                "ErrorAllert": {
                    "height": "70dp",
                    "left": "87dp",
                    "right": 37
                },
                "ErrorAllert.flxError": {
                    "centerY": "50%",
                    "top": ""
                },
                "ErrorAllert.imgCloseBackup": {
                    "centerY": "50%",
                    "height": "18dp",
                    "left": "",
                    "right": 20,
                    "src": "ico_close.png",
                    "top": ""
                },
                "ErrorAllert.lblMessage": {
                    "centerY": "50%",
                    "text": "Subtitle text goes here",
                    "top": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "popupPayinPayout2.imgClose1": {
                    "src": "ico_close.png"
                },
                "popupPayinPayout2.imgSrc1": {
                    "src": "search.png"
                },
                "popupPayinPayout2": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "height": "100%",
                    "left": "0dp",
                    "width": "100%"
                },
                "popupPayinPayout.FlexContainer0e3c37c0e40204d": {
                    "centerX": "50%",
                    "height": "250dp"
                },
                "popupPayinPayout.flxInnerSearch": {
                    "left": "0dp",
                    "top": "0px"
                },
                "popupPayinPayout.lblAccountIdhead": {
                    "width": "23%"
                },
                "popupPayinPayout.lblCustomerIdhead": {
                    "width": "15%"
                },
                "popupPayinPayout.lblProductName": {
                    "width": "28%"
                },
                "popupPayinPayout.segPayinPayout": {
                    "height": "200dp"
                }
            }
            this.add(uuxNavigationRail, flxMainContainer, SegmentPopup, flxMessageInfo, ErrorAllert, ProgressIndicator, flxpayInOutPOP, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmDisburse,
            "enabledForIdleTimeout": false,
            "id": "frmDisburse",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});