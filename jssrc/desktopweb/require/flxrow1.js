define("flxrow1", function() {
    return function(controller) {
        var flxrow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxrow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 3,
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxrow1.setDefaultUnit(kony.flex.DP);
        var chxBox = new kony.ui.CheckBoxGroup({
            "height": "70dp",
            "id": "chxBox",
            "isVisible": false,
            "left": "3dp",
            "masterData": [
                ["-", "-"]
            ],
            "selectedKeys": ["-"],
            "skin": "CopyslCheckBoxGroup0d72a4aebc0ea4f",
            "top": "0dp",
            "width": "101dp",
            "zIndex": 1
        }, {
            "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnSelect1 = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "40dp",
            "id": "btnSelect1",
            "isVisible": false,
            "left": "2%",
            "skin": "sknicongreen",
            "text": "",
            "top": "5dp",
            "width": "40px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblArrow = new kony.ui.Label({
            "height": "18dp",
            "id": "lblArrow",
            "isVisible": true,
            "left": "5%",
            "skin": "sknlblArrowGreen",
            "text": "",
            "top": "16dp",
            "width": "18dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate1 = new kony.ui.Label({
            "height": "40dp",
            "id": "lblDate1",
            "isVisible": true,
            "left": "5%",
            "skin": "sknflxFont434343px14",
            "text": "19 Dec 2020",
            "top": "5dp",
            "width": "30%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmount1 = new kony.ui.Label({
            "height": "40dp",
            "id": "lblAmount1",
            "isVisible": true,
            "left": "12%",
            "skin": "sknlblBGf2f7fdpx14bg434343",
            "text": "8,581.30",
            "top": "5dp",
            "width": "35%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxrow1.add(chxBox, btnSelect1, lblArrow, lblDate1, lblAmount1);
        return flxrow1;
    }
})