define("userfrmFullDetailsController", {
    custid: "",
    //Type your controller code here 
    onNavigate: function(navigationObj) {
        this.custid = navigationObj.cid;
        this.bindEvents();
    },
    bindEvents: function() {
        this.view.postShow = this.postShowFunctionCall;
        this.view.lblCustId.text = this.custid;
        this.view.lblOverview.onTouchEnd = this.navtoAccountList;
        this.view.lblDashboard.onTouchEnd = this.navtoHome;
    },
    postShowFunctionCall: function() {
        this.cdmSearchCustomer();
        this.cdmChannels();
        this.cdmReportingStatus();
        this.cdmPersonalConsents();
        this.cdmRelationShip();
    },
    cdmSearchCustomer: function() {
        var self = this;
        var objSvc1 = kony.sdk.getCurrentInstance().getObjectService("cdmSearchCustomer", {
            "access": "online"
        });
        var dataObject = new kony.sdk.dto.DataObject("customerObject");
        dataObject.addField("customerId", this.custid);
        var options = {
            "dataObject": dataObject
        };
        objSvc1.customVerb("getCDMcustomerQuery", options, function(response) {
            var item = response.body[0];
            self.view.lblCustName.text = (item.customerName !== undefined) ? item.customerName : "--";
            self.view.lblFlxName.text = (item.title !== undefined) ? item.title : "--";
            self.view.lblShortnameValue.text = (item.firstName !== undefined) ? item.firstName : "--";
            self.view.lblMaritalValue.text = (item.maritalStatus !== undefined) ? item.maritalStatus : "--";
            self.view.lblNationValue.text = (item.nationalityName !== undefined) ? item.nationalityName : "--";
            self.view.lblFullNameValue.text = (item.customerName !== undefined) ? item.customerName : "--";
            self.view.lblSalutation.text = (item.title !== undefined) ? item.title : "--";
            self.view.lblDOB.text = (item.dateOfBirth !== undefined) ? item.dateOfBirth : "--";
            self.view.lblDependants.text = "--";
            self.view.lblRes.text = (item.residenceName !== undefined) ? item.residenceName : "--";
            self.view.lblGender.text = (item.gender !== undefined) ? item.gender : "--";
            if (item.addresses !== undefined) {
                var country = ((item.addresses[0].country !== undefined) ? item.addresses[0].country + " " : " ");
                var street = ((item.addresses[0].street !== undefined) ? item.addresses[0].street + " " : " ");
                var postCode = ((item.addresses[0].postCode !== undefined) ? item.addresses[0].postCode + " " : " ");
                var addressCity = ((item.addresses[0].addressCity !== undefined) ? item.addresses[0].addressCity + " " : " ");
                self.view.lblAddressValue.text = street + addressCity + country + postCode;
            }
            //var address= (item.addresses[0]!==undefined)? item.primaryAddress :"--" item.addresses
            //self.view.lblAddressValue.text = (item.primaryAddress!==undefined)? item.primaryAddress :"--";
            if (item.secondaryAddress !== undefined) self.view.lblSEcondaryValue.text = item.secondaryAddress;
            else self.view.lblSEcondaryValue.text = "--"; //self.view.lblAddressValue.text;
            if (item.contactDetails !== undefined) {
                self.view.lblResidenceValue.text = (item.contactDetails[0].phoneNumbers[0].phoneNumber !== undefined) ? item.contactDetails[0].phoneNumbers[0].phoneNumber : "--";
                self.view.lblEmailValue.text = (item.contactDetails[0].emails[0].email !== undefined) ? item.contactDetails[0].emails[0].email : "--";
                self.view.lblMobileValue.text = (item.contactDetails[0].mobilePhoneNumbers[0].mobilePhoneNumber !== undefined) ? item.contactDetails[0].mobilePhoneNumbers[0].mobilePhoneNumber : "--";
            } else {
                self.view.lblOfficePhoneValue.text = "--";
                self.view.lblMobileValue.text = "--";
                self.view.lblEmailValue.text = "--";
            }
            self.view.lblOfficePhoneValue.text = (item.PhoneNo !== undefined) ? item.PhoneNo : "--";
            if (item.preferredChannels !== undefined) {
                self.view.lblPreferredValue.text = (item.preferredChannels[0].preferredChannel !== undefined) ? item.preferredChannels[0].preferredChannel : "--";
            } else {
                self.view.lblPreferredValue.text = "--";
            }
            self.view.lblFaxValue.text = item.faxId !== undefined ? item.faxId : "--";
            kony.print("Record created: " + JSON.stringify(response));
            //});
        }, function(error) {
            // self.view.ProgressIndicator.isVisible = false;
            kony.print("Error in record creation: " + JSON.stringify(error));
        });
    },
    cdmChannels: function() {
        var self = this;
        var objSvc = kony.sdk.getCurrentInstance().getObjectService("cdmFullDetail", {
            "access": "online"
        });
        var dataObject = new kony.sdk.dto.DataObject("customerChannel");
        dataObject.addField("customerId", this.custid);
        var options = {
            "dataObject": dataObject
        };
        objSvc.customVerb("getCustomerChannels", options, function(response) {
            if (response.customerChannel !== undefined) {
                var item = response.customerChannel[0];
                if (item.isSecureChannel === "true") self.view.lblSecurelblValue.text = 'Yes';
                else self.view.lblSecurelblValue.text = 'No';
                if (item.isMobileBanking === "true") self.view.lblMobileBankingVal.text = 'Yes';
                else self.view.lblMobileBankingVal.text = 'No';
                if (item.isInternetBanking === "true") self.view.lblInternetBnkLabel.text = 'Yes';
                else self.view.lblInternetBnkLabel.text = 'No';
                kony.print("Record created: " + JSON.stringify(response));
            } else {
                self.view.lblSecurelblValue.text = 'No';
                self.view.lblMobileBankingVal.text = 'No';
                self.view.lblInternetBnkLabel.text = 'No';
            }
        }, function(error) {
            //alert("getCDMCustomerProfile Error"+JSON.stringify(error));
            self.view.lblSecurelblValue.text = 'No';
            self.view.lblMobileBankingVal.text = 'No';
            self.view.lblInternetBnkLabel.text = 'No';
            kony.print("Error in record creation: " + JSON.stringify(error));
        });
    },
    cdmReportingStatus: function() {
        var self = this;
        var objSvc = kony.sdk.getCurrentInstance().getObjectService("cdmFullDetail", {
            "access": "online"
        });
        var dataObject = new kony.sdk.dto.DataObject("customerKyc");
        dataObject.addField("customerId", this.custid);
        var options = {
            "dataObject": dataObject
        };
        objSvc.customVerb("getCKYC", options, function(response) {
            // alert("getCDMCustomerProfile Error"+JSON.stringify(response));
            var item = response;
            self.view.lblAMLValue.text = (item.amlRisk !== undefined) ? item.amlRisk : "Normal";
            self.view.lblFATCAValue.text = (item.factaStatus !== undefined) ? item.factaStatus : "None";
            self.view.lblPostingValue.text = (item.postingRestriction !== undefined) ? item.postingRestriction : "None";
            self.view.lblCRSJURUValue.text = (item.crsJurisdiction !== undefined) ? item.crsJurisdiction : "Non reportable";
            self.view.lblCRSValue.text = (item.crsstatus !== undefined) ? item.crsStatus.substring(1, item.crsStatus.length - 1).split(",") : "None";
            //self.view.ProgressIndicator.isVisible = false;
        }, function(error) {
            self.view.lblCRSValue.text = "Non reportable";
            self.view.lblCRSJURUValue.text = "None";
            self.view.lblPostingValue.text = "None";
            self.view.lblAMLValue.text = "Normal";
            self.view.lblFATCAValue.text = "None";
            kony.print("Error in record creation: " + JSON.stringify(error));
            //self.view.ProgressIndicator.isVisible = false;
        });
    },
    cdmPersonalConsents: function() {
        var self = this;
        var objSvc = kony.sdk.getCurrentInstance().getObjectService("cdmFullDetail", {
            "access": "online"
        });
        var dataObject = new kony.sdk.dto.DataObject("customerConsent");
        dataObject.addField("customerId", this.custid);
        var options = {
            "dataObject": dataObject
        };
        objSvc.customVerb("getCustomerConsent", options, function(response) {
            var consent = response;
            if (consent.consentType !== undefined) {
                if (consent.consentType === "CREDITCHECK") {
                    if (consent.isConsentGiven) self.view.lblCreditValue.text = "Yes";
                    else self.view.lblCreditValue.text = "No";
                }
                if (consent.consentType === "DIRECTMKTING") {
                    if (consent.isConsentGiven) self.view.lblDirectingValue.text = "Yes";
                    else self.view.lblDirectingValue.text = "No";
                }
            } else {
                self.view.lblDirectingValue.text = "No";
                self.view.lblCreditValue.text = "No";
            }
            self.view.lblPCLabel.text = (consent.arrangementId !== null && consent.arrangementId !== undefined) ? consent.arrangementId : "--";
        }, function(error) {
            self.view.lblDirectingValue.text = "No";
            self.view.lblCreditValue.text = "No";
            self.view.lblPCValue.text = "Not Authorised";
            self.view.lblPCLabel.text = "";
            kony.print("Error in record creation: " + JSON.stringify(error));
        });
    },
    cdmRelationShip: function() {
        var self = this;
        var objSvc = kony.sdk.getCurrentInstance().getObjectService("cdmFullDetail", {
            "access": "online"
        });
        var dataObject = new kony.sdk.dto.DataObject("customerRelationShip");
        dataObject.addField("customerId", this.custid);
        var options = {
            "dataObject": dataObject
        };
        objSvc.customVerb("getCDMCustomerRelationShip", options, function(response) {
            var relationShipDetail = [];
            var item = response.body;
            for (var i = 0; i < item.length; i++) {
                if (item[i].relatedCustomerId !== undefined && item[i].reverseRelationCodeName !== null && item[i].relatedCustomerName !== "") {
                    var finaldata = item[i].relatedCustomerName + " ( " + item[i].relatedCustomerId + " ) " + " - " + item[i].reverseRelationCodeName;
                    var data = {
                        "relatedPartyName": finaldata
                    };
                    data.relatedPartyName.template = "CopyCopyflxRelationship";
                    relationShipDetail.push(data);
                }
            }
            if (relationShipDetail !== undefined && relationShipDetail.length !== 0) {
                self.view.lblDependants.text = (relationShipDetail.length).toString();
                self.view.relationship.segRelationship.widgetDataMap = {
                    relatedPartyNameVal: "relatedPartyName",
                    "CopyCopyflxRelationship": "CopyCopyflxRelationship"
                };
                self.view.lblNoRel.setVisibility(false);
                self.view.relationship.segRelationship.setData(relationShipDetail);
                self.view.forceLayout();
            }
            //kony.print(" response getCustomerRelationShip: " + JSON.stringify(response));
            self.view.ProgressIndicator.isVisible = false;
        }, function(error) {
            self.view.lblDependants.text = "0";
            self.view.lblNoRel.setVisibility(true);
            self.view.relationship.segRelationship.setData([]);
            kony.print("Error in record creation: " + JSON.stringify(error));
            self.view.ProgressIndicator.isVisible = false;
        });
    },
    navtoAccountList: function() {
        var navToAccounts = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this.custid;
        navObjet.messageInfo = "";
        navToAccounts.navigate(navObjet);
    },
    navtoHome: function() {
        var params = {};
        params.isPopupShow = "false";
        var navObject = new kony.mvc.Navigation("frmDashboard");
        navObject.navigate(params);
    },
});
define("frmFullDetailsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("frmFullDetailsController", ["userfrmFullDetailsController", "frmFullDetailsControllerActions"], function() {
    var controller = require("userfrmFullDetailsController");
    var controllerActions = ["frmFullDetailsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
