var countID = 0;
define("userfrmpostingRestrictionController", {
    onNavigate: function(navObj) {
        try {
            this.bindEvents();
        } catch (err) {
            alert("onNavigate:::::::" + err);
        }
    },
    bindEvents: function() {
        try {
            this.view.postShow = this.postShowFunctionCall;
            this.view.cusButtonConfirm.onclickEvent = this.validationOnSubmit;
            this.view.cusButtonCancel.onclickEvent = this.validationOnCancel;
            this.view.btnAddWidgets.onClick = this.addListBoxFunction;
            this.view.btnDelete.onClick = this.deleteListBoxFunction2;
            this.view.btnDelete3.onClick = this.deleteListBoxFunction3;
            this.view.btnDelete4.onClick = this.deleteListBoxFunction4;
            this.view.btnDelete5.onClick = this.deleteListBoxFunction5;
        } catch (err) {
            alert("bindEvents::::::::::::" + err);
        }
    },
    postShowFunctionCall: function() {
        try {
            this.initializeComponent();
            this.view.cusListBoxRestrictPostion2.isVisible = false;
            this.view.cusListBoxReason2.isVisible = false;
            this.view.cusCalenderFromDate2.isVisible = false;
            this.view.cusCalenderExpireDate2.isVisible = false;
            this.view.cusListBoxUnblockReason2.isVisible = false;
            this.view.btnDelete.isVisible = false;
            //       this.view.cusCalenderFromDate.iconButtonTrailingAriaLabel="close date calendar";
            //flxContainerListBox3
            this.view.cusListBoxRestrictPostion3.isVisible = false;
            this.view.cusListBoxReason3.isVisible = false;
            this.view.cusCalenderFromDate3.isVisible = false;
            this.view.cusCalenderExpireDate3.isVisible = false;
            this.view.cusListBoxUnblockReason3.isVisible = false;
            this.view.btnDelete3.isVisible = false;
            //flxContainerListBox4
            this.view.cusListBoxRestrictPostion4.isVisible = false;
            this.view.cusListBoxReason4.isVisible = false;
            this.view.cusCalenderFromDate4.isVisible = false;
            this.view.cusCalenderExpireDate4.isVisible = false;
            this.view.cusListBoxUnblockReason4.isVisible = false;
            this.view.btnDelete4.isVisible = false;
            //flxContainerListBox5
            this.view.cusListBoxRestrictPostion5.isVisible = false;
            this.view.cusListBoxReason5.isVisible = false;
            this.view.cusCalenderFromDate5.isVisible = false;
            this.view.cusCalenderExpireDate5.isVisible = false;
            this.view.cusListBoxUnblockReason5.isVisible = false;
            this.view.btnDelete5.isVisible = false;
            this.postingRestrictionFunction();
            this.getBlockReasonFunction();
            this.getUnblockReasonFunction();
            //       this.getPostingRestrctionMethod();
        } catch (err) {
            alert("postShowFunctionCall::::::" + err);
        }
    },
    initializeComponent: function() {
        try {
            this.view.flxContainerAccount.skin = "sknSelected";
            this.view.flxontainerLoans.skin = "sknNotselectedTransparent";
            this.view.btnAccounts1.skin = "sknTabSelected";
            this.view.loanDetails.lblAccountNumberResponse.skin = "sknTabSelected";
            this.view.loanDetails.lblLoanTypeResponse.skin = "sknTabSelected";
            this.view.loanDetails.lblCurrencyValueResponse.skin = "sknTabSelected";
            this.view.loanDetails.lblAmountValueResponse.skin = "sknTabSelected";
            this.view.loanDetails.lblStartDateResponse.skin = "sknTabSelected";
            this.view.loanDetails.lblListBoxSelectedValue.skin = "sknTabSelected";
            this.view.lblRestrictPostion.skin = "060607";
            //       this.view.lblAdd.skin="sknTabSelected";
            //       this.view.btnAddWidgets.skin="sknTabSelected";
            //       this.view.btnAddWidgets.borderColor="E0D8FF";
            //       this.view.btnAddWidgets.borderStyle="BO''RDER_STYLE_COMPLETE_ROUNDED_CORNER";
            // this.view.loanDetails.lblListBoxSelectedValue.text="Restrict Postion";
            //       this.view.cusListBoxUnblockReason.readonly=true;
        } catch (err) {
            alert("initializeComponent::::::::" + err);
        }
    },
    validationOnSubmit: function() {
        try {
            this.view.lblErrorMsg.isVisible = false;
            var payment = this.view.cusListBoxRestrictPayment.value;
            var reason = this.view.cusListBoxReason.value;
            var fromDate = this.view.cusCalenderFromDate.value;
            if (payment === "" || payment === undefined || payment === null) {
                this.view.lblErrorMsg.isVisible = true;
            } else if (reason === "" || reason === undefined || reason === null) {
                this.view.lblErrorMsg.isVisible = true;
            } else if (fromDate === "" || fromDate === undefined || fromDate === null) {
                this.view.lblErrorMsg.isVisible = true;
            }
        } catch (err) {
            alert("validationOnSubmit::::::::::" + err);
        }
    },
    validationOnCancel: function() {
        try {
            this.view.lblErrorMsg.isVisible = false;
        } catch (err) {
            alert("validationOnCancel:::::::::::" + err);
        }
    },
    addListBoxFunction: function() {
        try {
            if (countID === 0) {
                this.view.cusListBoxRestrictPostion2.isVisible = true;
                this.view.cusListBoxReason2.isVisible = true;
                this.view.cusCalenderFromDate2.isVisible = true;
                this.view.cusCalenderExpireDate2.isVisible = true;
                this.view.cusListBoxUnblockReason2.isVisible = true;
                this.view.btnDelete.isVisible = true;
                this.view.flxListBoxContainer.height = "10%";
                this.view.lblErrorMsg.top = "5%";
                this.view.flxContainerGroupButton.top = "5%";
            } else if (countID === 1) {
                this.view.cusListBoxRestrictPostion3.isVisible = true;
                this.view.cusListBoxReason3.isVisible = true;
                this.view.cusCalenderFromDate3.isVisible = true;
                this.view.cusCalenderExpireDate3.isVisible = true;
                this.view.cusListBoxUnblockReason3.isVisible = true;
                this.view.btnDelete3.isVisible = true;
                this.view.flxListBoxContainer.height = "10%";
                this.view.lblErrorMsg.top = "13%";
                this.view.flxContainerGroupButton.top = "13%";
            } else if (countID === 2) {
                this.view.cusListBoxRestrictPostion4.isVisible = true;
                this.view.cusListBoxReason4.isVisible = true;
                this.view.cusCalenderFromDate4.isVisible = true;
                this.view.cusCalenderExpireDate4.isVisible = true;
                this.view.cusListBoxUnblockReason4.isVisible = true;
                this.view.btnDelete4.isVisible = true;
                this.view.flxListBoxContainer.height = "10%";
                this.view.lblErrorMsg.top = "20%";
                this.view.flxContainerGroupButton.top = "20%";
            } else if (countID === 3) {
                this.view.cusListBoxRestrictPostion5.isVisible = true;
                this.view.cusListBoxReason5.isVisible = true;
                this.view.cusCalenderFromDate5.isVisible = true;
                this.view.cusCalenderExpireDate5.isVisible = true;
                this.view.cusListBoxUnblockReason5.isVisible = true;
                this.view.btnDelete5.isVisible = true;
                this.view.flxListBoxContainer.height = "10%";
                this.view.lblErrorMsg.top = "25";
                this.view.flxContainerGroupButton.top = "25%";
            }
            countID++;
            //       alert(countID);
        } catch (err) {
            alert("addListBoxFunction:::::::::" + err);
        }
    },
    deleteListBoxFunction2: function() {
        try {
            this.view.cusListBoxRestrictPostion2.isVisible = false;
            this.view.cusListBoxReason2.isVisible = false;
            this.view.cusCalenderFromDate2.isVisible = false;
            this.view.cusCalenderExpireDate2.isVisible = false;
            this.view.cusListBoxUnblockReason2.isVisible = false;
            this.view.btnDelete.isVisible = false;
            countID--;
            this.view.lblErrorMsg.top = "-3%";
            this.view.flxContainerGroupButton.top = "-3%";
        } catch (err) {
            alert("deleteListBoxFunction2:::::::::::::" + err);
        }
    },
    deleteListBoxFunction3: function() {
        try {
            this.view.cusListBoxRestrictPostion3.isVisible = false;
            this.view.cusListBoxReason3.isVisible = false;
            this.view.cusCalenderFromDate3.isVisible = false;
            this.view.cusCalenderExpireDate3.isVisible = false;
            this.view.cusListBoxUnblockReason3.isVisible = false;
            this.view.btnDelete3.isVisible = false;
            countID--;
            this.view.lblErrorMsg.top = "5%";
            this.view.flxContainerGroupButton.top = "5%";
        } catch (err) {
            alert("deleteListBoxFunction3::::::::::::::" + err);
        }
    },
    deleteListBoxFunction4: function() {
        try {
            this.view.cusListBoxRestrictPostion4.isVisible = false;
            this.view.cusListBoxReason4.isVisible = false;
            this.view.cusCalenderFromDate4.isVisible = false;
            this.view.cusCalenderExpireDate4.isVisible = false;
            this.view.cusListBoxUnblockReason4.isVisible = false;
            this.view.btnDelete4.isVisible = false;
            countID--;
            this.view.lblErrorMsg.top = "12%";
            this.view.flxContainerGroupButton.top = "12%";
        } catch (err) {
            alert("deleteListBoxFunction2::::::::::::::" + err);
        }
    },
    deleteListBoxFunction5: function() {
        try {
            this.view.cusListBoxRestrictPostion5.isVisible = false;
            this.view.cusListBoxReason5.isVisible = false;
            this.view.cusCalenderFromDate5.isVisible = false;
            this.view.cusCalenderExpireDate5.isVisible = false;
            this.view.cusListBoxUnblockReason5.isVisible = false;
            this.view.btnDelete5.isVisible = false;
            countID--;
            this.view.lblErrorMsg.top = "20%";
            this.view.flxContainerGroupButton.top = "20%";
        } catch (err) {
            alert("deleteListBoxFunction5::::::::::::::" + err);
        }
    },
    postingRestrictionFunction: function() {
        try {
            var serviceName = "PostingRestriction";
            var operationName = "GetPostingRestrictCode";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successPostingRestriction, this.failurePostingRestriction);
        } catch (err) {
            alert("postingRestrictionFunction::::::" + err);
        }
    },
    successPostingRestriction: function(res) {
        try {
            //       alert("successPostingRestriction Success response: "+JSON.stringify(res));
            var restrictionType;
            var lenght = res.body.length;
            for (i = 0; i < lenght; i++) {
                restrictionType = res.body[i].description;
                this.view.cusListBoxRestrictPayment.options = JSON.stringify(restrictionType);
                this.view.cusListBoxRestrictPostion2.options = JSON.stringify(restrictionType);
                this.view.cusListBoxRestrictPostion3.options = JSON.stringify(restrictionType);
                this.view.cusListBoxRestrictPostion4.options = JSON.stringify(restrictionType);
                this.view.cusListBoxRestrictPostion5.options = JSON.stringify(restrictionType);
                //         alert("DATA'S::::" + JSON.stringify(restrictionType));
            }
            this.view.forceLayout();
        } catch (err) {
            //       alert("successPostingRestriction::::"+err);
        }
    },
    failurePostingRestriction: function(res) {
        try {
            alert("failurePostingRestriction:::" + JSON.stringify(res));
        } catch (err) {
            alert("failurePostingRestriction::::::::" + err);
        }
    },
    //   getPostingRestrctionMethod : function(){
    //     try{
    //       var data   = [
    //         { "label": "CREDIT",
    //         },{
    //           "label": "DEBIT", 
    //         }
    //       ];
    //       this.view.cusListBoxRestrictPayment.options = JSON.stringify(data);
    //       //       this.view.ErrorAllert.setVisibility(false);
    //     }catch(err){
    //       kony.print("Error in getPaymentMethod" +err);
    //     }
    //   },
    getBlockReasonFunction: function() {
        try {
            var serviceName = "PostingRestriction";
            var operationName = "GetBlockReason";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successGetBlockReason, this.failureGetBlockReason);
        } catch (err) {
            alert("getBlockReasonFunction:::::::::::" + err);
        }
    },
    successGetBlockReason: function(res) {
        try {
            //       alert("successGetBlockReason Success response: "+JSON.stringify(res));
            var blockReason;
            var lght = res.body.length;
            for (i = 0; i < lght; i++) {
                blockReason = res.body[i].description;
                this.view.cusListBoxReason.options = JSON.stringify(blockReason);
                this.view.cusListBoxReason2.options = JSON.stringify(blockReason);
                this.view.cusListBoxReason3.options = JSON.stringify(blockReason);
                this.view.cusListBoxReason4.options = JSON.stringify(blockReason);
                this.view.cusListBoxReason5.options = JSON.stringify(blockReason);
                //         alert("RESPONSE OUTPUT:::::::"+JSON.stringify(blockReason));
            }
        } catch (err) {
            alert("successGetBlockReason::::" + err);
        }
    },
    failureGetBlockReason: function(res) {
        try {
            alert("failureGetBlockReason:::" + JSON.stringify(res));
        } catch (err) {
            alert("failureGetBlockReason::::::::" + err);
        }
    },
    getUnblockReasonFunction: function() {
        try {
            var serviceName = "PostingRestriction";
            var operationName = "GetUnblockReason";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successGetUnBlockReason, this.failureUnGetBlockReason);
        } catch (err) {
            alert("getUnblockReasonFunction:::::::::" + err);
        }
    },
    successGetUnBlockReason: function(res) {
        try {
            //       alert("successGetUnBlockReason Success response: "+JSON.stringify(res));
            var unBlockReason;
            var length = res.body.length;
            for (i = 0; i < length; i++) {
                unBlockReason = res.body[i].description;
                this.view.cusListBoxUnblockReason.options = JSON.stringify(unBlockReason);
                this.view.cuasListBoxUnblockReason2.options = JSON.stringify(unBlockReason);
                this.view.cusListBoxUnblockReason3.options = JSON.stringify(unBlockReason);
                this.view.cusListBoxUnblockReason4.options = JSON.stringify(unBlockReason);
                this.view.cusListBoxUnblockReason5.options = JSON.stringify(unBlockReason);
                //         alert("RESPONSE OUTPUT:::::::"+JSON.stringify(unBlockReason));
            }
        } catch (err) {
            alert("successGetUnBlockReason::::::::::::" + err);
        }
    },
    failureUnGetBlockReason: function(res) {
        try {
            alert("failureUnGetBlockReason:::" + JSON.stringify(res));
        } catch (err) {
            alert("failureUnGetBlockReason::::::::" + err);
        }
    },
});
define("frmpostingRestrictionControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("frmpostingRestrictionController", ["userfrmpostingRestrictionController", "frmpostingRestrictionControllerActions"], function() {
    var controller = require("userfrmpostingRestrictionController");
    var controllerActions = ["frmpostingRestrictionControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
