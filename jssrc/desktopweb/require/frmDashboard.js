define("frmDashboard", function() {
    return function(controller) {
        function addWidgetsfrmDashboard() {
            this.setDefaultUnit(kony.flex.PX);
            var flxWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0bcbf1cdac9554f",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxWrapper.setDefaultUnit(kony.flex.PX);
            var flxheader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75px",
                "id": "flxheader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64dp",
                "isModalContainer": false,
                "skin": "sknHeader",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxheader.setDefaultUnit(kony.flex.DP);
            var imgSrrc2 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgSrrc2",
                "isVisible": true,
                "right": "180px",
                "skin": "slImage",
                "src": "notification.png",
                "top": "10dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgChatBot = new kony.ui.Image2({
                "height": "86.50%",
                "id": "imgChatBot",
                "isVisible": true,
                "right": "260px",
                "skin": "slImage",
                "src": "hdr44.png",
                "top": "10dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgLogo = new kony.ui.Image2({
                "height": "100%",
                "id": "imgLogo",
                "isVisible": true,
                "left": "103dp",
                "skin": "slImage",
                "src": "temlogo_1.png",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgmenu = new kony.ui.Image2({
                "height": "100%",
                "id": "imgmenu",
                "isVisible": true,
                "left": "30dp",
                "skin": "slImage",
                "src": "menu_header.png",
                "top": "0dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnLogin = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnLogin",
                "isVisible": true,
                "onClick": controller.AS_Button_c06315740020458b854f2190afcaf9fb,
                "right": "40dp",
                "skin": "defBtnNormal",
                "text": "Logout",
                "top": "28dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "580px"
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var imgSrc = new kony.ui.Image2({
                "centerY": "50%",
                "height": "40px",
                "id": "imgSrc",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "search.png",
                "width": "60px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtField1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "focusSkin": "CopydefTextBoxFocus0j19a3dc7b3c44d",
                "height": "40px",
                "id": "txtField1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "4px",
                "placeholder": "Search Customer",
                "right": "4px",
                "secureTextEntry": false,
                "skin": "skngreybg",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [10, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxSearch.add(imgSrc, txtField1);
            flxheader.add(imgSrrc2, imgChatBot, imgLogo, imgmenu, btnLogin, flxSearch);
            var flxmain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "1000dp",
                "id": "flxmain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "14px",
                "skin": "slFbox",
                "top": "20dp",
                "width": "94%"
            }, {}, {});
            flxmain.setDefaultUnit(kony.flex.DP);
            var flxLeftMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "800px",
                "id": "flxLeftMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxWhiteRdCorners",
                "top": "87px",
                "width": "70px",
                "zIndex": 1
            }, {}, {});
            flxLeftMenu.setDefaultUnit(kony.flex.DP);
            var imgPx = new kony.ui.Image2({
                "height": "50px",
                "id": "imgPx",
                "isVisible": true,
                "left": "10px",
                "skin": "slImage",
                "src": "menu_propic.png",
                "top": "10px",
                "width": "50px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgpx1 = new kony.ui.Image2({
                "height": "50px",
                "id": "imgpx1",
                "isVisible": true,
                "left": "10px",
                "skin": "slImage",
                "src": "menu_dashboard.png",
                "top": "40px",
                "width": "50px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgPx5 = new kony.ui.Image2({
                "height": "98px",
                "id": "imgPx5",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "menu_selected.png",
                "top": "-71px",
                "width": "70px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgPx6 = new kony.ui.Image2({
                "height": "50px",
                "id": "imgPx6",
                "isVisible": true,
                "left": "10px",
                "skin": "slImage",
                "src": "menu_product.png",
                "top": "10px",
                "width": "50px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgPx2 = new kony.ui.Image2({
                "height": "50px",
                "id": "imgPx2",
                "isVisible": true,
                "left": "10px",
                "skin": "slImage",
                "src": "menu_agile_kanban.png",
                "top": "10px",
                "width": "50px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgPx3 = new kony.ui.Image2({
                "height": "50px",
                "id": "imgPx3",
                "isVisible": true,
                "left": "10px",
                "skin": "slImage",
                "src": "menu_contacts.png",
                "top": "10px",
                "width": "50px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgPx4 = new kony.ui.Image2({
                "height": "50px",
                "id": "imgPx4",
                "isVisible": true,
                "left": "10px",
                "skin": "slImage",
                "src": "menu_current_account.png",
                "top": "10px",
                "width": "50px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgPx7 = new kony.ui.Image2({
                "height": "50px",
                "id": "imgPx7",
                "isVisible": true,
                "left": "10px",
                "skin": "slImage",
                "src": "menu_settings.png",
                "top": "300px",
                "width": "50px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLeftMenu.add(imgPx, imgpx1, imgPx5, imgPx6, imgPx2, imgPx3, imgPx4, imgPx7);
            var flxCenterActivities = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "880dp",
                "id": "flxCenterActivities",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64%"
            }, {}, {});
            flxCenterActivities.setDefaultUnit(kony.flex.DP);
            var lblClientActivities = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblClientActivities",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknHeading20px",
                "text": "Client activities",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxClientActivities = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "510dp",
                "id": "flxClientActivities",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknDashboardFlex",
                "top": "16px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxClientActivities.setDefaultUnit(kony.flex.DP);
            var flxCntnr4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxCntnr4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCntnr4.setDefaultUnit(kony.flex.DP);
            var btnAddNewRequest = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtn12selibold",
                "height": "36dp",
                "id": "btnAddNewRequest",
                "isVisible": true,
                "right": "174dp",
                "skin": "sknbtn12selibold",
                "text": "New Request +",
                "top": 0,
                "width": "132dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnFilter = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "CopydefBtnFocus0a835c60219ec43",
                "height": "30px",
                "id": "btnFilter",
                "isVisible": false,
                "left": "50%",
                "skin": "Copysknbtn0jd8bd0e5794640",
                "text": "Filter",
                "top": "0dp",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnVerifyChart = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "30dp",
                "id": "btnVerifyChart",
                "isVisible": false,
                "right": "20px",
                "skin": "CopybtnDefProfile1",
                "text": "Verify Chart",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchInSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchInSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "28px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "55%"
            }, {}, {});
            flxSearchInSegment.setDefaultUnit(kony.flex.DP);
            var imgSearchIcon = new kony.ui.Image2({
                "centerY": "50.00%",
                "height": "16px",
                "id": "imgSearchIcon",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "search.png",
                "width": "20dp",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtPartialText = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "focusSkin": "sknTxtBoxNormal",
                "height": "40px",
                "id": "txtPartialText",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Search for client, name, ID, activity or status",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "textBoxClr",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "24dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "skntxtBoxPH"
            });
            var lblSearchIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblSearchIcon",
                "isVisible": true,
                "right": "15dp",
                "skin": "iconFontPurple20px",
                "text": "k",
                "top": "0",
                "width": "20dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchInSegment.add(imgSearchIcon, txtPartialText, lblSearchIcon);
            var Image0d6bf9e8ffa894f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "30dp",
                "id": "Image0d6bf9e8ffa894f",
                "isVisible": false,
                "left": "50%",
                "skin": "slImage",
                "src": "ico_filter.png",
                "top": "19dp",
                "width": "40dp",
                "zIndex": 3
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnClient = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtn12selibold",
                "height": "36px",
                "id": "btnClient",
                "isVisible": true,
                "onClick": controller.AS_Button_g3b21a88dd1f4517a686b97e4639b4af,
                "right": "20dp",
                "skin": "sknBtnBlueBgWhiteFont",
                "text": "Add New Client + ",
                "top": "0dp",
                "width": "130dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCntnr4.add(btnAddNewRequest, btnFilter, btnVerifyChart, flxSearchInSegment, Image0d6bf9e8ffa894f, btnClient);
            var flxborderline = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxborderline",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "right": "1%",
                "skin": "CopyslFbox0j59aee3388974e",
                "top": "0",
                "width": "98%"
            }, {}, {});
            flxborderline.setDefaultUnit(kony.flex.DP);
            flxborderline.add();
            var flxCntnr6 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxCntnr6",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxCntnr6.setDefaultUnit(kony.flex.DP);
            var btnPending = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "skn12semebold",
                "height": "30dp",
                "id": "btnPending",
                "isVisible": true,
                "left": "10dp",
                "skin": "skn12semebold",
                "text": "  Pending",
                "top": "0dp",
                "width": "13%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPendingCnt = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblPendingCnt",
                "isVisible": true,
                "left": "9.50%",
                "skin": "sknProcessing",
                "text": "0",
                "top": "0dp",
                "width": "3.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNew = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "skn12semebold",
                "height": "30dp",
                "id": "btnNew",
                "isVisible": true,
                "left": "25%",
                "skin": "sknTabUnselected",
                "text": "  New",
                "top": "0dp",
                "width": "9.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNewCnt = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblNewCnt",
                "isVisible": true,
                "left": "30%",
                "skin": "sknInreview",
                "text": "0",
                "top": "0dp",
                "width": "3.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNew = new kony.ui.Label({
                "height": "40dp",
                "id": "lblNew",
                "isVisible": false,
                "left": "610dp",
                "skin": "defLabel",
                "top": "13dp",
                "width": "10%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnCompleted = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "skn12semebold",
                "height": "30dp",
                "id": "btnCompleted",
                "isVisible": true,
                "left": "40%",
                "skin": "sknTabUnselected",
                "text": "  Completed",
                "top": "0dp",
                "width": "14.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCompletedcnt = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblCompletedcnt",
                "isVisible": true,
                "left": "50%",
                "skin": "sknCompleted",
                "text": "0",
                "top": "0dp",
                "width": "3.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnCancel = new kony.ui.Button({
                "centerY": "52.00%",
                "focusSkin": "skn12semebold",
                "height": "30dp",
                "id": "btnCancel",
                "isVisible": true,
                "left": "60%",
                "skin": "sknTabUnselected",
                "text": "  Cancelled",
                "top": "0dp",
                "width": "13.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCanelCnt = new kony.ui.Label({
                "centerY": "52.00%",
                "height": "20px",
                "id": "lblCanelCnt",
                "isVisible": true,
                "left": "69%",
                "skin": "sknMeetingScheduled",
                "text": "0",
                "top": "0dp",
                "width": "3.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAll = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "skn12semebold",
                "height": "30dp",
                "id": "btnAll",
                "isVisible": true,
                "left": "80%",
                "skin": "sknTabUnselected",
                "text": "  All",
                "top": "0dp",
                "width": "8%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblall = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblall",
                "isVisible": true,
                "left": "83.50%",
                "skin": "sknAll",
                "text": "0",
                "top": "0dp",
                "width": "3.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxseparatotline = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "7px",
                "clipBounds": true,
                "height": "4dp",
                "id": "flxseparatotline",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e76341dc019440",
                "top": "45dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxseparatotline.setDefaultUnit(kony.flex.DP);
            flxseparatotline.add();
            var flxselected = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "8px",
                "clipBounds": true,
                "height": "2px",
                "id": "flxselected",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-63%",
                "isModalContainer": false,
                "skin": "sknAllselected",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxselected.setDefaultUnit(kony.flex.DP);
            flxselected.add();
            flxCntnr6.add(btnPending, lblPendingCnt, btnNew, lblNewCnt, lblNew, btnCompleted, lblCompletedcnt, btnCancel, lblCanelCnt, btnAll, lblall, flxseparatotline, flxselected);
            var flxborderline1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxborderline1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "right": "1%",
                "skin": "CopyslFbox0j59aee3388974e",
                "top": "0",
                "width": "98%"
            }, {}, {});
            flxborderline1.setDefaultUnit(kony.flex.DP);
            flxborderline1.add();
            var flxTemp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxTemp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "right": "1%",
                "skin": "sknGrey",
                "top": "10dp",
                "width": "98%"
            }, {}, {});
            flxTemp.setDefaultUnit(kony.flex.DP);
            var lblGroup = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroup",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLblGrey15Sbold",
                "text": "Client",
                "top": "0dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblExpires = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExpires",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Activity",
                "top": "0dp",
                "width": "27%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Date",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInformed = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblInformed",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Status",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTemp.add(lblGroup, lblExpires, lblDate, lblInformed);
            var flxborderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxborderline2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "right": "1%",
                "skin": "CopyslFbox0j59aee3388974e",
                "top": "0",
                "width": "98%"
            }, {}, {});
            flxborderline2.setDefaultUnit(kony.flex.DP);
            flxborderline2.add();
            var lblNoRecordStatus = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoRecordStatus",
                "isVisible": false,
                "left": 0,
                "skin": "CopydefLabel0d72c9e640ae24c",
                "text": "No Records in the Selected Status",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SegA = new kony.ui.SegmentedUI2({
                "alternateRowSkin": "seg2Normal",
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblActivity": "",
                    "lblCid": "",
                    "lblDate": "",
                    "lblGroup": "",
                    "lblInformed": ""
                }],
                "groupCells": false,
                "height": "245dp",
                "id": "SegA",
                "isVisible": true,
                "left": "1%",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_ad9c9c5e47f640edabeeea09fcd99501,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "1%",
                "rowFocusSkin": "Copyseg0g7c649e772fa4c",
                "rowSkin": "sknRemoveBorder",
                "rowTemplate": "flxHdr",
                "sectionHeaderSkin": "sknHeaderSeg",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "70707055",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": true,
                "top": "10dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxHdr": "flxHdr",
                    "flxName": "flxName",
                    "flxRowAct": "flxRowAct",
                    "flxStatus": "flxStatus",
                    "flxStatusinner": "flxStatusinner",
                    "lblActivity": "lblActivity",
                    "lblCid": "lblCid",
                    "lblDate": "lblDate",
                    "lblGroup": "lblGroup",
                    "lblInformed": "lblInformed"
                },
                "width": "98%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var btnPrev1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnPrev1",
                "isVisible": false,
                "right": "45dp",
                "skin": "slImage",
                "src": "arrow_back.png",
                "top": "5dp",
                "width": "19dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnNext1",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "arrow_forward.png",
                "top": "5dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPrev = new kony.ui.Button({
                "height": "30dp",
                "id": "btnPrev",
                "isVisible": true,
                "right": "45dp",
                "skin": "sknBtnFocus",
                "text": "O",
                "top": "5dp",
                "width": 30,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext = new kony.ui.Button({
                "height": "30dp",
                "id": "btnNext",
                "isVisible": true,
                "right": "8dp",
                "skin": "sknBtnDisable",
                "text": "N",
                "top": "5dp",
                "width": 30,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPage = new kony.ui.Label({
                "centerY": "50%",
                "height": "30dp",
                "id": "lblSearchPage",
                "isVisible": true,
                "right": "90dp",
                "skin": "CopydefLabel0b044b7d672a44a",
                "text": "1-6 of 18",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRowPerPageNumber = new kony.ui.Label({
                "id": "lblRowPerPageNumber",
                "isVisible": false,
                "left": "354dp",
                "skin": "CopydefLabel0bfa17407e4df49",
                "text": "Rows Per Page",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextField0a340c02fa20749 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "20dp",
                "id": "TextField0a340c02fa20749",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "494dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ca88ed6f06d246",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxPagination.add(btnPrev1, btnNext1, btnPrev, btnNext, lblSearchPage, lblRowPerPageNumber, TextField0a340c02fa20749);
            var flxdividerline1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxdividerline1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLine",
                "width": "98%",
                "zIndex": 2
            }, {}, {});
            flxdividerline1.setDefaultUnit(kony.flex.DP);
            flxdividerline1.add();
            flxClientActivities.add(flxCntnr4, flxborderline, flxCntnr6, flxborderline1, flxTemp, flxborderline2, lblNoRecordStatus, SegA, flxPagination, flxdividerline1);
            var lblLoansheading = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblLoansheading",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknHeading20px",
                "text": "Deposits Maturing within 7 Days",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLoanMaturing7Days = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "340dp",
                "id": "flxLoanMaturing7Days",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknDashboardFlex",
                "top": 20,
                "width": "100%"
            }, {}, {});
            flxLoanMaturing7Days.setDefaultUnit(kony.flex.DP);
            var flxLoanHead = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxLoanHead",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "right": "1%",
                "skin": "sknGrey",
                "top": "20dp",
                "width": "98%"
            }, {}, {});
            flxLoanHead.setDefaultUnit(kony.flex.DP);
            var lblLoanCustID = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanCustID",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLblGrey15Sbold",
                "text": "Customer ID",
                "top": "0dp",
                "width": "12.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanName",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Name",
                "top": "0dp",
                "width": "14.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanAccountID = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanAccountID",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Account No",
                "top": "0dp",
                "width": "13.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanProduct = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanProduct",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Product",
                "top": "0dp",
                "width": "21.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanCurrency = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanCurrency",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Currency",
                "top": "0dp",
                "width": "11%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanAmt = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanAmt",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Amount",
                "top": "0dp",
                "width": "13.50%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLoanDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Maturity Date",
                "top": "0dp",
                "width": "14%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoanHead.add(lblLoanCustID, lblLoanName, lblLoanAccountID, lblLoanProduct, lblLoanCurrency, lblLoanAmt, lblLoanDate);
            var flxborder5 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxborder5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "right": "1%",
                "skin": "CopyslFbox0j59aee3388974e",
                "top": "0",
                "width": "98%"
            }, {}, {});
            flxborder5.setDefaultUnit(kony.flex.DP);
            flxborder5.add();
            var segLoan7Days = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblLoanAccountID": "",
                    "lblLoanAmt": "",
                    "lblLoanCurrency": "",
                    "lblLoanCustID": "",
                    "lblLoanDate": "",
                    "lblLoanName": "",
                    "lblLoanProduct": ""
                }],
                "groupCells": false,
                "height": "220dp",
                "id": "segLoan7Days",
                "isVisible": true,
                "left": "1%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxLoan7daysMain",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "70707055",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": 20,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxLoan7daysMain": "flxLoan7daysMain",
                    "lblLoanAccountID": "lblLoanAccountID",
                    "lblLoanAmt": "lblLoanAmt",
                    "lblLoanCurrency": "lblLoanCurrency",
                    "lblLoanCustID": "lblLoanCustID",
                    "lblLoanDate": "lblLoanDate",
                    "lblLoanName": "lblLoanName",
                    "lblLoanProduct": "lblLoanProduct"
                },
                "width": "99%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoRecords1 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50dp",
                "id": "lblNoRecords1",
                "isVisible": false,
                "left": "0",
                "skin": "CopydefLabel0a20195ef06fa41",
                "text": "No Records Found",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPaginationLoan = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPaginationLoan",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPaginationLoan.setDefaultUnit(kony.flex.DP);
            var btnPrevLoan1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnPrevLoan1",
                "isVisible": false,
                "right": "45dp",
                "skin": "slImage",
                "src": "arrow_back.png",
                "top": "5dp",
                "width": "19dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNextLoan1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnNextLoan1",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "arrow_forward.png",
                "top": "5dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPrevLoan = new kony.ui.Button({
                "height": "30dp",
                "id": "btnPrevLoan",
                "isVisible": true,
                "right": "45dp",
                "skin": "sknBtnFocus",
                "text": "O",
                "top": "5dp",
                "width": 30,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNextLoan = new kony.ui.Button({
                "height": "30dp",
                "id": "btnNextLoan",
                "isVisible": true,
                "right": "8dp",
                "skin": "sknBtnDisable",
                "text": "N",
                "top": "5dp",
                "width": 30,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPageLoan = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSearchPageLoan",
                "isVisible": true,
                "right": "90dp",
                "skin": "CopydefLabel0b044b7d672a44a",
                "text": "1-6 of 18",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRowPerPageNumberLoan = new kony.ui.Label({
                "id": "lblRowPerPageNumberLoan",
                "isVisible": false,
                "left": "354dp",
                "skin": "CopydefLabel0bfa17407e4df49",
                "text": "Rows Per Page",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextFieldLoan = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "20dp",
                "id": "TextFieldLoan",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "494dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ca88ed6f06d246",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxPaginationLoan.add(btnPrevLoan1, btnNextLoan1, btnPrevLoan, btnNextLoan, lblSearchPageLoan, lblRowPerPageNumberLoan, TextFieldLoan);
            flxLoanMaturing7Days.add(flxLoanHead, flxborder5, segLoan7Days, lblNoRecords1, flxPaginationLoan);
            var FlxInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "40px",
                "clipBounds": false,
                "height": "200px",
                "id": "FlxInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            FlxInfo.setDefaultUnit(kony.flex.DP);
            var flxAHIN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxAHIN",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteRdCorners",
                "top": "0dp",
                "width": "59%",
                "zIndex": 1
            }, {}, {});
            flxAHIN.setDefaultUnit(kony.flex.DP);
            var CopySegA0jed4be0b618440 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [
                    [{
                            "lblChannel": "Channel",
                            "lblDate": "Date/Time",
                            "lblExpires": "Activity",
                            "lblGroup": "Client ID"
                        },
                        [{
                            "lblChannel": "OLB",
                            "lblDate": "Nov 22    04:10AM",
                            "lblExpires": "Request for Business",
                            "lblGroup": "101145"
                        }, {
                            "lblChannel": "MB",
                            "lblDate": "Dec 26    05:10AM",
                            "lblExpires": "Request for Deposit",
                            "lblGroup": "101146"
                        }, {
                            "lblChannel": "IPA",
                            "lblDate": "April 22   09:10AM",
                            "lblExpires": "Request for Loan",
                            "lblGroup": "101147"
                        }]
                    ]
                ],
                "groupCells": false,
                "height": "150px",
                "id": "CopySegA0jed4be0b618440",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "CopyflxHdr0e952e967ea4c45",
                "sectionHeaderSkin": "sknHeaderSeg",
                "sectionHeaderTemplate": "CopyflxTemp0decad958122647",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "f2f2f200",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "CopyflxHdr0e952e967ea4c45": "CopyflxHdr0e952e967ea4c45",
                    "CopyflxTemp0decad958122647": "CopyflxTemp0decad958122647",
                    "lblChannel": "lblChannel",
                    "lblDate": "lblDate",
                    "lblExpires": "lblExpires",
                    "lblGroup": "lblGroup"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0ab8f0d33900b43 = new kony.ui.Label({
                "id": "CopyLabel0ab8f0d33900b43",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblRowHeading",
                "text": "Appointments with HNI customers",
                "top": 10,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0id40b56be1d741 = new kony.ui.Label({
                "id": "CopyLabel0id40b56be1d741",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopydefLabel0b03ee0c4234d41",
                "text": "Updated today, 8:56 AM",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAHIN.add(CopySegA0jed4be0b618440, CopyLabel0ab8f0d33900b43, CopyLabel0id40b56be1d741);
            var flxgrpProFee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxgrpProFee",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlxWhiteRdCorners",
                "top": "0dp",
                "width": "39%",
                "zIndex": 1
            }, {}, {});
            flxgrpProFee.setDefaultUnit(kony.flex.DP);
            var flcProducts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flcProducts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flcProducts.setDefaultUnit(kony.flex.DP);
            var CopyLabel0c32a584e3dc84c = new kony.ui.Label({
                "id": "CopyLabel0c32a584e3dc84c",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblRowHeading",
                "text": "Products",
                "top": 10,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [10, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0ee164f6ef81c4c = new kony.ui.Label({
                "id": "CopyLabel0ee164f6ef81c4c",
                "isVisible": true,
                "left": "0px",
                "skin": "CopydefLabel0b03ee0c4234d41",
                "text": "New consumer loan rate",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [10, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var FlexGroup0c3cd59d1a6ef43 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "FlexGroup0c3cd59d1a6ef43",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%"
            }, {}, {});
            FlexGroup0c3cd59d1a6ef43.setDefaultUnit(kony.flex.DP);
            var Label0b67643db275c42 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "Label0b67643db275c42",
                "isVisible": true,
                "left": 50,
                "right": "0px",
                "skin": "CopydefLabel0e5802349f02d46",
                "text": "Loans",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Image0c231a3fd48df4b = new kony.ui.Image2({
                "centerY": "50%",
                "height": "30px",
                "id": "Image0c231a3fd48df4b",
                "isVisible": true,
                "left": 15,
                "skin": "slImage",
                "src": "ico_loans.png",
                "top": 0,
                "width": "30px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0c3cd59d1a6ef43.add(Label0b67643db275c42, Image0c231a3fd48df4b);
            var CopyFlexGroup0dd3eb6ba9de347 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "height": "44px",
                "id": "CopyFlexGroup0dd3eb6ba9de347",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%"
            }, {}, {});
            CopyFlexGroup0dd3eb6ba9de347.setDefaultUnit(kony.flex.DP);
            var CopyLabel0a77b5410842d44 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "CopyLabel0a77b5410842d44",
                "isVisible": true,
                "left": 50,
                "right": "0px",
                "skin": "CopydefLabel0e5802349f02d46",
                "text": "Deposit ",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyImage0h1f26e9639a14d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "30px",
                "id": "CopyImage0h1f26e9639a14d",
                "isVisible": true,
                "left": 15,
                "skin": "slImage",
                "src": "ico_cards.png",
                "top": 0,
                "width": "30px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0dd3eb6ba9de347.add(CopyLabel0a77b5410842d44, CopyImage0h1f26e9639a14d);
            flcProducts.add(CopyLabel0c32a584e3dc84c, CopyLabel0ee164f6ef81c4c, FlexGroup0c3cd59d1a6ef43, CopyFlexGroup0dd3eb6ba9de347);
            var flxfees = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "190px",
                "id": "flxfees",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxfees.setDefaultUnit(kony.flex.DP);
            var CopyLabel0d72422eb721a41 = new kony.ui.Label({
                "id": "CopyLabel0d72422eb721a41",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblRowHeading",
                "text": "Fees",
                "top": 10,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [10, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyLabel0cff9ff7ee07740 = new kony.ui.Label({
                "id": "CopyLabel0cff9ff7ee07740",
                "isVisible": true,
                "left": "0px",
                "skin": "CopydefLabel0b03ee0c4234d41",
                "text": "Updated on Nov 15,2020",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [10, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyFlexGroup0cd923fdeac8c4b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "CopyFlexGroup0cd923fdeac8c4b",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%"
            }, {}, {});
            CopyFlexGroup0cd923fdeac8c4b.setDefaultUnit(kony.flex.DP);
            var CopyLabel0f6519967593c48 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "CopyLabel0f6519967593c48",
                "isVisible": true,
                "left": 50,
                "right": "0px",
                "skin": "CopydefLabel0e5802349f02d46",
                "text": "DD Issuance",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyImage0bc9ce3189c464e = new kony.ui.Image2({
                "centerY": "50%",
                "height": "30px",
                "id": "CopyImage0bc9ce3189c464e",
                "isVisible": true,
                "left": 15,
                "skin": "slImage",
                "src": "ico_wallet.png",
                "top": 0,
                "width": "30px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0cd923fdeac8c4b.add(CopyLabel0f6519967593c48, CopyImage0bc9ce3189c464e);
            var CopyFlexGroup0caaa28879c8844 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "height": "44px",
                "id": "CopyFlexGroup0caaa28879c8844",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%"
            }, {}, {});
            CopyFlexGroup0caaa28879c8844.setDefaultUnit(kony.flex.DP);
            var CopyLabel0befd8682f3ba41 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "CopyLabel0befd8682f3ba41",
                "isVisible": true,
                "left": 50,
                "right": "0px",
                "skin": "CopydefLabel0e5802349f02d46",
                "text": "Money Transfer",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyImage0be470c1d92a242 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "30px",
                "id": "CopyImage0be470c1d92a242",
                "isVisible": true,
                "left": 15,
                "skin": "slImage",
                "src": "ico_transfer.png",
                "top": 0,
                "width": "30px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyFlexGroup0caaa28879c8844.add(CopyLabel0befd8682f3ba41, CopyImage0be470c1d92a242);
            flxfees.add(CopyLabel0d72422eb721a41, CopyLabel0cff9ff7ee07740, CopyFlexGroup0cd923fdeac8c4b, CopyFlexGroup0caaa28879c8844);
            var flxline = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "centerX": "49%",
                "clipBounds": true,
                "height": "184px",
                "id": "flxline",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "sknLine",
                "top": "10px",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxline.setDefaultUnit(kony.flex.DP);
            flxline.add();
            flxgrpProFee.add(flcProducts, flxfees, flxline);
            FlxInfo.add(flxAHIN, flxgrpProFee);
            flxCenterActivities.add(lblClientActivities, flxClientActivities, lblLoansheading, flxLoanMaturing7Days, FlxInfo);
            var flxRightGraphs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "880dp",
                "id": "flxRightGraphs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "0%",
                "skin": "slFbox",
                "top": "5dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxRightGraphs.setDefaultUnit(kony.flex.DP);
            var lblRequestStatus = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblRequestStatus",
                "isVisible": true,
                "left": "5%",
                "skin": "sknHeading20px",
                "text": "Request Status",
                "top": "5dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDonutContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "31%",
                "id": "flxDonutContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "isModalContainer": false,
                "skin": "sknDashboardFlex",
                "top": "14dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxDonutContainer.setDefaultUnit(kony.flex.DP);
            var flxDonut = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "250dp",
                "id": "flxDonut",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "width": "500dp",
                "zIndex": 1
            }, {}, {});
            flxDonut.setDefaultUnit(kony.flex.DP);
            flxDonut.add();
            var flxhidescroll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "245dp",
                "id": "flxhidescroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "450dp",
                "zIndex": 10
            }, {}, {});
            flxhidescroll.setDefaultUnit(kony.flex.DP);
            var flxchartcontent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxchartcontent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "110dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "100dp",
                "zIndex": 100
            }, {}, {});
            flxchartcontent.setDefaultUnit(kony.flex.DP);
            var lblPercent = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblPercent",
                "isVisible": true,
                "left": 0,
                "skin": "CopysknDef0hf223e7d2b714a",
                "text": "52%",
                "top": "10dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCompleted = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblCompleted",
                "isVisible": true,
                "left": 0,
                "skin": "Copyskn0fe09bac25b2941",
                "text": "Completed",
                "top": "2dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxchartcontent.add(lblPercent, lblCompleted);
            flxhidescroll.add(flxchartcontent);
            var imgDonutchart = new kony.ui.Image2({
                "centerY": "50%",
                "height": "89%",
                "id": "imgDonutchart",
                "isVisible": false,
                "left": "-30px",
                "skin": "slImage",
                "src": "sales_rates1.png",
                "width": "30%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDonutContainer.add(flxDonut, flxhidescroll, imgDonutchart);
            var stackbarchart = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableNativeCommunication": false,
                "enableZoom": false,
                "height": "660dp",
                "id": "stackbarchart",
                "isVisible": false,
                "left": "20dp",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "Copychart_sVbar/sVbar.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "20dp",
                "width": "92%",
                "zIndex": 1
            }, {}, {});
            var lblTopCustomer = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblTopCustomer",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknHeading20px",
                "text": "Top Customer",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTopCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "255dp",
                "id": "flxTopCustomer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknDashboardFlex",
                "top": "0dp",
                "width": "89%",
                "zIndex": 1
            }, {}, {});
            flxTopCustomer.setDefaultUnit(kony.flex.DP);
            var flxCustomerWrap = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCustomerWrap",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxCustomerWrap.setDefaultUnit(kony.flex.DP);
            var lblheaderCustomerID = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderCustomerID",
                "isVisible": true,
                "left": "1%",
                "skin": "segBlueColor",
                "text": "Customer ID",
                "top": "0",
                "width": "25.30%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderFirstName = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderFirstName",
                "isVisible": true,
                "left": 0,
                "skin": "segBlueColor",
                "text": "Name",
                "top": "0dp",
                "width": "33.30%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderRM = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderRM",
                "isVisible": true,
                "left": "0%",
                "skin": "segBlueColor",
                "text": "Relationship Manager",
                "top": "0dp",
                "width": "40.30%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerWrap.add(lblheaderCustomerID, lblheaderFirstName, lblheaderRM);
            var flxborder2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxborder2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "right": "0%",
                "skin": "CopyslFbox0j59aee3388974e",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxborder2.setDefaultUnit(kony.flex.DP);
            flxborder2.add();
            var lblNoRecords = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50dp",
                "id": "lblNoRecords",
                "isVisible": false,
                "left": "0",
                "skin": "CopydefLabel0a20195ef06fa41",
                "text": "No Records Found",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segTopCustomer = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }, {
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng"
                }],
                "groupCells": false,
                "height": "170dp",
                "id": "segTopCustomer",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTopCust",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "bcbfc22d",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCustomerWrap": "flxCustomerWrap",
                    "flxTopCust": "flxTopCust",
                    "lblCustomerID": "lblCustomerID",
                    "lblFirstName": "lblFirstName",
                    "lblRm": "lblRm"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPaginationTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPaginationTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPaginationTop.setDefaultUnit(kony.flex.DP);
            var btnPrevTop = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnPrevTop",
                "isVisible": true,
                "right": "45dp",
                "skin": "slImage",
                "src": "arrow_back.png",
                "top": "5dp",
                "width": "19dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNextTop = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnNextTop",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "arrow_forward.png",
                "top": "5dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPageTop = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSearchPageTop",
                "isVisible": true,
                "right": "90dp",
                "skin": "CopydefLabel0b044b7d672a44a",
                "text": "1-6 of 18",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var blRowPerPageNumberTop = new kony.ui.Label({
                "id": "blRowPerPageNumberTop",
                "isVisible": false,
                "left": "354dp",
                "skin": "CopydefLabel0bfa17407e4df49",
                "text": "Rows Per Page",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextFieldTop = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "20dp",
                "id": "TextFieldTop",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "494dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ca88ed6f06d246",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxPaginationTop.add(btnPrevTop, btnNextTop, lblSearchPageTop, blRowPerPageNumberTop, TextFieldTop);
            flxTopCustomer.add(flxCustomerWrap, flxborder2, lblNoRecords, segTopCustomer, flxPaginationTop);
            var lblBranchCustomer = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblBranchCustomer",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknHeading20px",
                "text": "Branch Customer",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBranchCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "260dp",
                "id": "flxBranchCustomer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknDashboardFlex",
                "top": "0dp",
                "width": "89%",
                "zIndex": 1
            }, {}, {});
            flxBranchCustomer.setDefaultUnit(kony.flex.DP);
            var flxBranchCustomerHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxBranchCustomerHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxBranchCustomerHeader.setDefaultUnit(kony.flex.DP);
            var lblheaderCustomerID1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderCustomerID1",
                "isVisible": true,
                "left": "1%",
                "skin": "segBlueColor",
                "text": "Customer ID",
                "top": "0",
                "width": "25.30%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderFirstName1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderFirstName1",
                "isVisible": true,
                "left": 0,
                "skin": "segBlueColor",
                "text": "Date",
                "top": "0dp",
                "width": "33.30%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderTime = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderTime",
                "isVisible": true,
                "left": "0%",
                "skin": "segBlueColor",
                "text": "Time",
                "top": "0dp",
                "width": "20.30%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBranchCustomerHeader.add(lblheaderCustomerID1, lblheaderFirstName1, lblheaderTime);
            var flxborder4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxborder4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": 1,
                "skin": "CopyslFbox0j59aee3388974e",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxborder4.setDefaultUnit(kony.flex.DP);
            flxborder4.add();
            var segBranchCustomer = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }, {
                    "btnLogout": "i",
                    "lblCustomerID": "101146",
                    "lblFirstName": "Rolf Gerieng",
                    "lblRm": "Rolf Gerieng",
                    "lbllogout": "i"
                }],
                "groupCells": false,
                "height": "170dp",
                "id": "segBranchCustomer",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "CustomerBranch",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "bcbfc22d",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "CustomerBranch": "CustomerBranch",
                    "btnLogout": "btnLogout",
                    "flxBranchWrap": "flxBranchWrap",
                    "lblCustomerID": "lblCustomerID",
                    "lblFirstName": "lblFirstName",
                    "lblRm": "lblRm",
                    "lbllogout": "lbllogout"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPaginationBranch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPaginationBranch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPaginationBranch.setDefaultUnit(kony.flex.DP);
            var btnPrevBranch = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnPrevBranch",
                "isVisible": true,
                "right": "45dp",
                "skin": "slImage",
                "src": "arrow_back.png",
                "top": "5dp",
                "width": "19dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNextBranch = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnNextBranch",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "arrow_forward.png",
                "top": "5dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPageBranch = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSearchPageBranch",
                "isVisible": true,
                "right": "90dp",
                "skin": "CopydefLabel0b044b7d672a44a",
                "text": "1-6 of 18",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRowPerPageNumberBranch = new kony.ui.Label({
                "id": "lblRowPerPageNumberBranch",
                "isVisible": false,
                "left": "354dp",
                "skin": "CopydefLabel0bfa17407e4df49",
                "text": "Rows Per Page",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextFieldBranch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "20dp",
                "id": "TextFieldBranch",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "494dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ca88ed6f06d246",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxPaginationBranch.add(btnPrevBranch, btnNextBranch, lblSearchPageBranch, lblRowPerPageNumberBranch, TextFieldBranch);
            flxBranchCustomer.add(flxBranchCustomerHeader, flxborder4, segBranchCustomer, flxPaginationBranch);
            var lbLoansGraphSubTitle = new kony.ui.Label({
                "bottom": "10dp",
                "height": "30dp",
                "id": "lbLoansGraphSubTitle",
                "isVisible": true,
                "left": "5%",
                "skin": "sknHeading20px",
                "text": "Inoperative Accounts",
                "top": "30dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 1, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNewLoansBarGraph = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "568dp",
                "id": "flxNewLoansBarGraph",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5%",
                "isModalContainer": false,
                "skin": "sknDashboardFlex",
                "top": "20dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxNewLoansBarGraph.setDefaultUnit(kony.flex.DP);
            var flxTopGraphPart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxTopGraphPart",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 5,
                "width": "100%"
            }, {}, {});
            flxTopGraphPart.setDefaultUnit(kony.flex.DP);
            var flxdataBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxdataBar",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2%",
                "width": "40%"
            }, {}, {});
            flxdataBar.setDefaultUnit(kony.flex.DP);
            var lblLoanCount = new kony.ui.Label({
                "height": "30dp",
                "id": "lblLoanCount",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknCountNo",
                "text": "92",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanCount1 = new kony.ui.Label({
                "id": "lblLoanCount1",
                "isVisible": false,
                "left": "0",
                "text": "92",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblinWeek = new kony.ui.Label({
                "height": "30dp",
                "id": "lblinWeek",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0ea358d3b808647",
                "text": "In a Week",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxdataBar.add(lblLoanCount, lblLoanCount1, lblinWeek);
            var flxRightLoanLabels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "72%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxRightLoanLabels",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2%",
                "width": "55%",
                "zIndex": 1
            }, {}, {});
            flxRightLoanLabels.setDefaultUnit(kony.flex.DP);
            var flxRound1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12dp",
                "id": "flxRound1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxBgBlueInactiveAccount",
                "top": "0",
                "width": "12dp"
            }, {}, {});
            flxRound1.setDefaultUnit(kony.flex.DP);
            flxRound1.add();
            var blTitlePersonal = new kony.ui.Label({
                "centerY": "50%",
                "id": "blTitlePersonal",
                "isVisible": true,
                "left": "8dp",
                "skin": "sknFont14px757575",
                "text": "Inactive",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRound2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12dp",
                "id": "flxRound2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknFlxBgLightBlueDormant",
                "top": "0",
                "width": "12dp"
            }, {}, {});
            flxRound2.setDefaultUnit(kony.flex.DP);
            flxRound2.add();
            var blTitleMortgage = new kony.ui.Label({
                "centerY": "50%",
                "id": "blTitleMortgage",
                "isVisible": true,
                "left": "8dp",
                "skin": "sknFont14px757575",
                "text": "Dormant",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightLoanLabels.add(flxRound1, blTitlePersonal, flxRound2, blTitleMortgage);
            flxTopGraphPart.add(flxdataBar, flxRightLoanLabels);
            var FlexContainer0ic0a72d08b2648 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "450dp",
                "id": "FlexContainer0ic0a72d08b2648",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "95%"
            }, {}, {});
            FlexContainer0ic0a72d08b2648.setDefaultUnit(kony.flex.DP);
            var barGraph11 = new com.temenos.chartjslib.barGraph2({
                "centerX": "50%",
                "height": "350px",
                "id": "barGraph11",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "95%",
                "zIndex": 1,
                "overrides": {
                    "barGraph2": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWBarGraph2": {
                        "canvasHeight": "",
                        "canvasWidth": "",
                        "datasetsArray": "[{\"label\":\"Personal Loans\",\"backgroundColor\":\"rgba(40, 2, 207, 0.1)\",\"pointBackgroundColor\" : \"#FF9800\",\"pointBorderColor\": \"#ffffff\",\"data\":[10,11,9,8,7,6,10]},{\"label\":\"Mortgage Loans\",\"backgroundColor\":\"rgba(96, 82, 201, 0.1)\",\"pointBackgroundColor\" : \"#2C8631\",\"pointBorderColor\": \"#ffffff\",\"data\":[8,4,4,5,9,6,4]}]",
                        "lineAreaFill": false,
                        "xlabelsArray": " [\"Day 1\", \"Day 2\", \"Day 3\", \"Day 4\", \"Day 5\", \"Day 6\", \"Day 7\"]",
                        "yBeginsAtZero": false
                    }
                }
            });
            var barGraph1 = new com.temenos.chartjslib.barGraph2({
                "height": "85%",
                "id": "barGraph1",
                "isVisible": true,
                "left": "8%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10%",
                "width": "92%",
                "zIndex": 1,
                "overrides": {
                    "barGraph2": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWBarGraph2": {
                        "canvasHeight": "",
                        "canvasWidth": "",
                        "datasetsArray": "[]",
                        "xlabelsArray": "[]",
                        "yBeginsAtZero": false,
                        "ySuggestedMin": 0
                    }
                }
            });
            var lblXValues = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblXValues",
                "isVisible": true,
                "left": "-25dp",
                "skin": "rotateXaxis",
                "text": "Number of Accounts",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0ic0a72d08b2648.add(barGraph11, barGraph1, lblXValues);
            var FlexContainer0i98c091ce7f34c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "55dp",
                "id": "FlexContainer0i98c091ce7f34c",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            FlexContainer0i98c091ce7f34c.setDefaultUnit(kony.flex.DP);
            var flxGrpahAxis = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxGrpahAxis",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "55%"
            }, {}, {});
            flxGrpahAxis.setDefaultUnit(kony.flex.DP);
            var lblXAxis = new kony.ui.Label({
                "id": "lblXAxis",
                "isVisible": true,
                "left": "0",
                "skin": "sknAxis",
                "text": "X-axis - Period of Time",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblYAxis = new kony.ui.Label({
                "id": "lblYAxis",
                "isVisible": true,
                "left": 0,
                "skin": "sknAxis",
                "text": "Y-axis - No of Loans Taken",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGrpahAxis.add(lblXAxis, lblYAxis);
            var lblViewGraphDetails = new kony.ui.Label({
                "centerX": "83%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblViewGraphDetails",
                "isVisible": true,
                "right": "20dp",
                "skin": "CopydefLabel0ad48ca1a46d34e",
                "text": "View Details",
                "width": "33%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0i98c091ce7f34c.add(flxGrpahAxis, lblViewGraphDetails);
            flxNewLoansBarGraph.add(flxTopGraphPart, FlexContainer0ic0a72d08b2648, FlexContainer0i98c091ce7f34c);
            flxRightGraphs.add(lblRequestStatus, flxDonutContainer, stackbarchart, lblTopCustomer, flxTopCustomer, lblBranchCustomer, flxBranchCustomer, lbLoansGraphSubTitle, flxNewLoansBarGraph);
            flxmain.add(flxLeftMenu, flxCenterActivities, flxRightGraphs);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "isModalContainer": false,
                "right": "16dp",
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": false,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageWithReferenceNumber = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMessageWithReferenceNumber",
                "isVisible": true,
                "left": "222dp",
                "right": "60dp",
                "skin": "sknlbl16PX434343BGcss",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgHeaderClose = new kony.ui.Label({
                "centerY": "50%",
                "height": "24dp",
                "id": "imgHeaderClose",
                "isVisible": true,
                "right": "35dp",
                "skin": "sknlblGreenIcon14px",
                "text": "H",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custBtnViewclient = new kony.ui.CustomWidget({
                "id": "custBtnViewclient",
                "isVisible": false,
                "right": 80,
                "top": "17dp",
                "width": "160dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "onclickEvent": controller.AS_TPW_c8139e225e664599b1e0387937220c04,
                "labelText": "View Client ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxTightIconWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "26dp",
                "id": "flxTightIconWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22dp",
                "isModalContainer": false,
                "skin": "sknFlxGreenRounder",
                "width": "26dp",
                "zIndex": 1
            }, {}, {});
            flxTightIconWrapper.setDefaultUnit(kony.flex.DP);
            var lblTightIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "80%",
                "id": "lblTightIcon",
                "isVisible": true,
                "skin": "sknlblGreenIcon",
                "text": "Z",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTightIconWrapper.add(lblTightIcon);
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblMessageWithReferenceNumber, imgHeaderClose, custBtnViewclient, flxTightIconWrapper);
            var flxCustomerBranchBg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "988dp",
                "id": "flxCustomerBranchBg",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0ec00ec6cd2644e",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCustomerBranchBg.setDefaultUnit(kony.flex.DP);
            var flxContentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "128dp",
                "centerX": "50%",
                "clipBounds": false,
                "height": "750dp",
                "id": "flxContentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxBGffffffBorderCCE3F7",
                "top": "75dp",
                "width": "615dp",
                "zIndex": 1
            }, {}, {});
            flxContentWrapper.setDefaultUnit(kony.flex.DP);
            var flxInnerContentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "700dp",
                "id": "flxInnerContentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxInnerContentWrapper.setDefaultUnit(kony.flex.DP);
            var lblCreateLogInfo = new kony.ui.Label({
                "height": "20dp",
                "id": "lblCreateLogInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl16pxBG003E75",
                "text": "Checkout",
                "top": "0dp",
                "width": "81dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyflxLine0d76cda13406440 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxLine0d76cda13406440",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSeparatorGrey979797",
                "top": "16dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxLine0d76cda13406440.setDefaultUnit(kony.flex.DP);
            CopyflxLine0d76cda13406440.add();
            var flxTypeChannelWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "55dp",
                "id": "flxTypeChannelWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTypeChannelWrapper.setDefaultUnit(kony.flex.DP);
            var cusCustomerId = new kony.ui.CustomWidget({
                "id": "cusCustomerId",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "disabled": true,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Customer",
                "maxLength": "",
                "placeholder": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            var cusLocation = new kony.ui.CustomWidget({
                "id": "cusLocation",
                "isVisible": true,
                "right": 0,
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "disabled": true,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Location",
                "maxLength": "",
                "placeholder": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            flxTypeChannelWrapper.add(cusCustomerId, cusLocation);
            var flxContactWrap = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "55dp",
                "id": "flxContactWrap",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContactWrap.setDefaultUnit(kony.flex.DP);
            var cusContactDate = new kony.ui.CustomWidget({
                "id": "cusContactDate",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "disabled": true,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Contact Date",
                "maxLength": "",
                "placeholder": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            var cusTimeIn = new kony.ui.CustomWidget({
                "id": "cusTimeIn",
                "isVisible": true,
                "right": 0,
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "disabled": true,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "time"
                },
                "iconButtonIcon": "",
                "labelText": "Time in",
                "maxLength": "",
                "placeholder": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            flxContactWrap.add(cusContactDate, cusTimeIn);
            var cusTextAreaRNotes = new kony.ui.CustomWidget({
                "id": "cusTextAreaRNotes",
                "isVisible": true,
                "left": "0dp",
                "top": "30dp",
                "width": "100%",
                "height": "105dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextArea",
                "labelText": "Notes",
                "maxLength": 256,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            var flxDateTimeWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "55dp",
                "id": "flxDateTimeWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDateTimeWrapper.setDefaultUnit(kony.flex.DP);
            var cusTextTimeOut = new kony.ui.CustomWidget({
                "id": "cusTextTimeOut",
                "isVisible": true,
                "left": 0,
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "time"
                },
                "iconButtonIcon": "",
                "labelText": "Time out",
                "maxLength": "",
                "placeholder": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            flxDateTimeWrapper.add(cusTextTimeOut);
            var flxSelectMode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "55dp",
                "id": "flxSelectMode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelectMode.setDefaultUnit(kony.flex.DP);
            var lblSelectModeInfo = new kony.ui.Label({
                "centerY": "50%",
                "height": "50dp",
                "id": "lblSelectModeInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblBg000000px14",
                "text": "Select Mode",
                "top": "0dp",
                "width": "94dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 1],
                "paddingInPixel": false
            }, {});
            var cusSelectMode = new kony.ui.CustomWidget({
                "id": "cusSelectMode",
                "isVisible": true,
                "left": "26dp",
                "top": "0dp",
                "width": "400dp",
                "height": "50dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRadioGroup",
                "labelText": "",
                "onRadioChange": "",
                "options": {
                    "selectedValue": ""
                },
                "value": ""
            });
            flxSelectMode.add(lblSelectModeInfo, cusSelectMode);
            var lblErrorMsg = new kony.ui.Label({
                "height": "40dp",
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLabelErrorMessage",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustButtons.setDefaultUnit(kony.flex.DP);
            var custCancel = new kony.ui.CustomWidget({
                "id": "custCancel",
                "isVisible": true,
                "right": 170,
                "top": "0dp",
                "width": "150dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custProceed = new kony.ui.CustomWidget({
                "id": "custProceed",
                "isVisible": true,
                "right": 0,
                "top": "0dp",
                "width": "150dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "onclickEvent": controller.AS_TPW_f937666cd8aa4daebf54a21779a11059,
                "labelText": "Submit",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxCustButtons.add(custCancel, custProceed);
            flxInnerContentWrapper.add(lblCreateLogInfo, CopyflxLine0d76cda13406440, flxTypeChannelWrapper, flxContactWrap, cusTextAreaRNotes, flxDateTimeWrapper, flxSelectMode, lblErrorMsg, flxCustButtons);
            flxContentWrapper.add(flxInnerContentWrapper);
            flxCustomerBranchBg.add(flxContentWrapper);
            flxWrapper.add(flxheader, flxmain, flxMessageInfo, flxCustomerBranchBg);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "6.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "92.50%",
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "6.50%",
                        "width": "92.50%"
                    },
                    "imgCloseBackup": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxGraphwrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxGraphwrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-200%",
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0%",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxGraphwrapper.setDefaultUnit(kony.flex.DP);
            var flxInnerGraph = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "500dp",
                "id": "flxInnerGraph",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "sknFlxOutstanding6pxRadius",
                "width": "75%",
                "zIndex": 1
            }, {}, {});
            flxInnerGraph.setDefaultUnit(kony.flex.DP);
            var flxHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblHeaderInfo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl24px005096BG",
                "text": "Inoperative Accounts",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusIconPrinter = new kony.ui.CustomWidget({
                "id": "cusIconPrinter",
                "isVisible": false,
                "right": "80dp",
                "top": "2dp",
                "width": "26dp",
                "height": "26dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "print",
                "size": "small"
            });
            var flxHeaderSegmenPopupClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeaderSegmenPopupClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "CopyslFbox0f4b50a6cacaa42",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderSegmenPopupClose.setDefaultUnit(kony.flex.DP);
            var cusIconClose = new kony.ui.CustomWidget({
                "id": "cusIconClose",
                "isVisible": false,
                "left": "0dp",
                "top": "2dp",
                "width": "100%",
                "height": "26dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "highlight_off",
                "size": "small"
            });
            var imgIconClose = new kony.ui.Image2({
                "height": "100%",
                "id": "imgIconClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSegmenPopupClose.add(cusIconClose, imgIconClose);
            flxHeaderInfo.add(lblHeaderInfo, cusIconPrinter, flxHeaderSegmenPopupClose);
            var flxGraphPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "400dp",
                "id": "flxGraphPopup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "96%"
            }, {}, {});
            flxGraphPopup.setDefaultUnit(kony.flex.DP);
            var flxGraphCard = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "370dp",
                "id": "flxGraphCard",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknFlxChartBG",
                "top": "0px",
                "width": "35%"
            }, {}, {});
            flxGraphCard.setDefaultUnit(kony.flex.DP);
            var tabPopupGraph = new com.temenos.chartjslib.barGraph2({
                "height": "100%",
                "id": "tabPopupGraph",
                "isVisible": false,
                "left": "0%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0%",
                "width": "96%",
                "zIndex": 1,
                "overrides": {
                    "barGraph2": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWBarGraph2": {
                        "canvasHeight": "",
                        "canvasWidth": "",
                        "datasetsArray": "[]",
                        "xlabelsArray": "[]",
                        "yBeginsAtZero": false,
                        "ySuggestedMin": 0
                    }
                }
            });
            var flxTopGraphPart2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxTopGraphPart2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxTopGraphPart2.setDefaultUnit(kony.flex.DP);
            var CopyflxdataBar0ab54871380464f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "CopyflxdataBar0ab54871380464f",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2%",
                "width": "40%"
            }, {}, {});
            CopyflxdataBar0ab54871380464f.setDefaultUnit(kony.flex.DP);
            var CopylblLoanCount0a306b0503c2145 = new kony.ui.Label({
                "height": "30dp",
                "id": "CopylblLoanCount0a306b0503c2145",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknCountNo",
                "text": "92",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblLoanCount0af27d73a6a4742 = new kony.ui.Label({
                "id": "CopylblLoanCount0af27d73a6a4742",
                "isVisible": false,
                "left": "0",
                "text": "92",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblinWeek0f22cbcc082524c = new kony.ui.Label({
                "height": "30dp",
                "id": "CopylblinWeek0f22cbcc082524c",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0ea358d3b808647",
                "text": "In a Week",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxdataBar0ab54871380464f.add(CopylblLoanCount0a306b0503c2145, CopylblLoanCount0af27d73a6a4742, CopylblinWeek0f22cbcc082524c);
            var flxRightAccountLabels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "72%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRightAccountLabels",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0%",
                "width": "55%",
                "zIndex": 1
            }, {}, {});
            flxRightAccountLabels.setDefaultUnit(kony.flex.DP);
            var CopyflxRound0fcd2530843684a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12dp",
                "id": "CopyflxRound0fcd2530843684a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxBgBlueInactiveAccount",
                "top": "0",
                "width": "12dp"
            }, {}, {});
            CopyflxRound0fcd2530843684a.setDefaultUnit(kony.flex.DP);
            CopyflxRound0fcd2530843684a.add();
            var CopyblTitlePersonal0c322d1227cd84e = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyblTitlePersonal0c322d1227cd84e",
                "isVisible": true,
                "left": "8dp",
                "skin": "sknFont14px757575",
                "text": "Inactive",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyflxRound0h079c095a2cf4a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12dp",
                "id": "CopyflxRound0h079c095a2cf4a",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknFlxBgLightBlueDormant",
                "top": "0",
                "width": "12dp"
            }, {}, {});
            CopyflxRound0h079c095a2cf4a.setDefaultUnit(kony.flex.DP);
            CopyflxRound0h079c095a2cf4a.add();
            var CopyblTitleMortgage0b2865e01751343 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyblTitleMortgage0b2865e01751343",
                "isVisible": true,
                "left": "8dp",
                "skin": "sknFont14px757575",
                "text": "Dormant",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightAccountLabels.add(CopyflxRound0fcd2530843684a, CopyblTitlePersonal0c322d1227cd84e, CopyflxRound0h079c095a2cf4a, CopyblTitleMortgage0b2865e01751343);
            flxTopGraphPart2.add(CopyflxdataBar0ab54871380464f, flxRightAccountLabels);
            var CopylblXValues0d6b67b0bdefb4a = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblXValues0d6b67b0bdefb4a",
                "isVisible": true,
                "left": "-25dp",
                "skin": "rotateXaxis",
                "text": "Number of Accounts",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tabPopupGraph2 = new com.temenos.chartjslib.barGraph2({
                "height": "83%",
                "id": "tabPopupGraph2",
                "isVisible": true,
                "left": "8%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55dp",
                "width": "93%",
                "zIndex": 1,
                "overrides": {
                    "barGraph2": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWBarGraph2": {
                        "canvasHeight": "",
                        "canvasWidth": "",
                        "datasetsArray": "[]",
                        "xlabelsArray": "[]",
                        "yBeginsAtZero": false,
                        "ySuggestedMin": 0
                    }
                }
            });
            flxGraphCard.add(tabPopupGraph, flxTopGraphPart2, CopylblXValues0d6b67b0bdefb4a, tabPopupGraph2);
            var flxAccData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "370dp",
                "id": "flxAccData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "60%"
            }, {}, {});
            flxAccData.setDefaultUnit(kony.flex.DP);
            var flxAccFilters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "50dp",
                "id": "flxAccFilters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "98%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxAccFilters.setDefaultUnit(kony.flex.DP);
            var lblTypeOfAcc = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTypeOfAcc",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknHeader17PxBg000000",
                "text": "Type of Account",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusSavingsAcc = new kony.ui.CustomWidget({
                "id": "cusSavingsAcc",
                "isVisible": true,
                "left": "10px",
                "top": "0",
                "width": "208dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "dense": "dense",
                "disabled": false,
                "labelText": "",
                "options": "",
                "readonly": false,
                "value": "o,jj,jk"
            });
            var cusToggleBtn = new kony.ui.CustomWidget({
                "id": "cusToggleBtn",
                "isVisible": true,
                "left": "0dp",
                "top": "16%",
                "width": "30%",
                "height": "100%",
                "centerX": "24%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButtonToggle",
                "options": "[{\"label\":\"Inactive\",\"value\":\"Inactive\"},{\"label\":\"Dormant\",\"value\":\"Dormant\"}]",
                "value": ""
            });
            flxAccFilters.add(lblTypeOfAcc, cusSavingsAcc, cusToggleBtn);
            var flxAccDataTable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "310dp",
                "id": "flxAccDataTable",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknDashboardFlex",
                "top": 0,
                "width": "98%"
            }, {}, {});
            flxAccDataTable.setDefaultUnit(kony.flex.DP);
            var flxAccHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxAccHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "right": "1%",
                "skin": "sknGrey",
                "top": "20dp",
                "width": "98%"
            }, {}, {});
            flxAccHeaders.setDefaultUnit(kony.flex.DP);
            var lblAccNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccNo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Account No",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrency = new kony.ui.Label({
                "centerY": "48%",
                "id": "lblCurrency",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Currency",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBalance = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBalance",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Balance",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLatAct = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLatAct",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblGrey15Sbold",
                "text": "Last Activity Date",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccHeaders.add(lblAccNo, lblCurrency, lblBalance, lblLatAct);
            var flxAccLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAccLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "right": "1%",
                "skin": "CopyslFbox0j59aee3388974e",
                "top": "0",
                "width": "98%"
            }, {}, {});
            flxAccLine.setDefaultUnit(kony.flex.DP);
            flxAccLine.add();
            var segAccData = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }, {
                    "lblAccNo": "50033656",
                    "lblBalance": "10000",
                    "lblCurrency": "USD",
                    "lblLastActivityDate": "10 March 2020"
                }],
                "groupCells": false,
                "height": "200dp",
                "id": "segAccData",
                "isVisible": true,
                "left": "1%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "CopyflxLoan0icf6efa07da643",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "70707055",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": 0,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "CopyflxLoan0icf6efa07da643": "CopyflxLoan0icf6efa07da643",
                    "lblAccNo": "lblAccNo",
                    "lblBalance": "lblBalance",
                    "lblCurrency": "lblCurrency",
                    "lblLastActivityDate": "lblLastActivityDate"
                },
                "width": "99%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAccNoRecords = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50dp",
                "id": "flxAccNoRecords",
                "isVisible": false,
                "left": "0",
                "skin": "CopydefLabel0a20195ef06fa41",
                "text": "No Records Found",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAccPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAccPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccPagination.setDefaultUnit(kony.flex.DP);
            var btnPrevious = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnPrevious",
                "isVisible": true,
                "right": "45dp",
                "skin": "slImage",
                "src": "arrow_back.png",
                "top": "5dp",
                "width": "19dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNextView = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnNextView",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "arrow_forward.png",
                "top": "5dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPageNo = new kony.ui.Label({
                "id": "lblSearchPageNo",
                "isVisible": false,
                "left": 20,
                "skin": "skinGraphAxis",
                "text": "Y-axis - No of Loans Taken",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblRowPerPageNumberLoan0d376e3807b454b = new kony.ui.Label({
                "id": "CopylblRowPerPageNumberLoan0d376e3807b454b",
                "isVisible": false,
                "left": "354dp",
                "skin": "CopydefLabel0bfa17407e4df49",
                "text": "Rows Per Page",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyTextFieldLoan0f630d7404b674a = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "20dp",
                "id": "CopyTextFieldLoan0f630d7404b674a",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "494dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ca88ed6f06d246",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxAccPagination.add(btnPrevious, btnNextView, lblSearchPageNo, CopylblRowPerPageNumberLoan0d376e3807b454b, CopyTextFieldLoan0f630d7404b674a);
            flxAccDataTable.add(flxAccHeaders, flxAccLine, segAccData, flxAccNoRecords, flxAccPagination);
            flxAccData.add(flxAccFilters, flxAccDataTable);
            flxGraphPopup.add(flxGraphCard, flxAccData);
            var flxOutstandingError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxOutstandingError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "90%"
            }, {}, {});
            flxOutstandingError.setDefaultUnit(kony.flex.DP);
            var lblOutError = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblOutError",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0gc960a890bbf4c",
                "text": "Label",
                "top": "0",
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOutstandingError.add(lblOutError);
            flxInnerGraph.add(flxHeaderInfo, flxGraphPopup, flxOutstandingError);
            flxGraphwrapper.add(flxInnerGraph);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyflxNavRailGradientSkin0j4b7bcb578604a",
                "top": "0%",
                "width": "64dp",
                "zIndex": 5,
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0g6e177d3e47442"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0fac15adf524b4e"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_fb5eadf6e648460e9d3c74f1576952e8;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_b9526842e3234c3e855d4309c59dd505;
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var getStarted = new com.konymp.getStarted({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "getStarted",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f4260580003b4d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 15,
                "overrides": {
                    "getStarted": {
                        "isVisible": false,
                        "zIndex": 15
                    },
                    "img1": {
                        "src": "start3.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
                "1366": {
                    "btnLogin": {
                        "focusSkin": "CopybtnDefProfile4",
                        "height": {
                            "type": "string",
                            "value": "40dp"
                        },
                        "skin": "CopybtnDefProfile2",
                        "segmentProps": []
                    }
                }
            }
            this.compInstData = {
                "barGraph11": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerY": ""
                },
                "barGraph1": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ErrorAllert": {
                    "left": "6.50%",
                    "width": "92.50%"
                },
                "ErrorAllert.imgCloseBackup": {
                    "src": "ico_close.png"
                },
                "tabPopupGraph": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "tabPopupGraph2": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0g6e177d3e47442",
                    "skin2": "CopydefBtnNormal0fac15adf524b4e",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "getStarted": {
                    "zIndex": 15
                },
                "getStarted.img1": {
                    "src": "start3.png"
                }
            }
            this.add(flxWrapper, ErrorAllert, flxGraphwrapper, uuxNavigationRail, ProgressIndicator, getStarted, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmDashboard,
            "enabledForIdleTimeout": false,
            "id": "frmDashboard",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_e7beed71d30141fbb9e094a4952749f8,
            "preShow": function(eventobject) {
                controller.AS_Form_i6dc5c383ac342f7889d0b9d5fceca10(eventobject);
                kony.visualizer.syncComponentInstanceDataCache(eventobject);
            },
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});