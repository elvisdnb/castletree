define("flxAddRowWrap", function() {
    return function(controller) {
        var flxAddRowWrap = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddRowWrap",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxAddRowWrap.setDefaultUnit(kony.flex.DP);
        var flxBenName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxBenName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "CopyslFbox0jeb2ba62ae294c",
            "top": "0",
            "width": "295dp"
        }, {}, {});
        flxBenName.setDefaultUnit(kony.flex.DP);
        var lblBenLabel = new kony.ui.Label({
            "height": "20px",
            "id": "lblBenLabel",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopydefLabel0d27046b6aa674d",
            "text": "Beneficiary Name",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRole = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "30px",
            "id": "lblRole",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "10dp",
            "secureTextEntry": false,
            "skin": "CopydefTextBoxNormal0c79c0816252940",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0",
            "width": "80%",
            "isSensitiveText": false
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var flxImgClicks = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxImgClicks",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 10,
            "skin": "sknSearch",
            "top": "-35dp",
            "width": "25dp"
        }, {}, {});
        flxImgClicks.setDefaultUnit(kony.flex.DP);
        flxImgClicks.add();
        flxBenName.add(lblBenLabel, lblRole, flxImgClicks);
        var flxBenRole = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxBenRole",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0jeb2ba62ae294c",
            "top": "0",
            "width": "295dp"
        }, {}, {});
        flxBenRole.setDefaultUnit(kony.flex.DP);
        var lblBenLabel1 = new kony.ui.Label({
            "height": "20px",
            "id": "lblBenLabel1",
            "isVisible": true,
            "left": "10dp",
            "skin": "CopydefLabel0d27046b6aa674d",
            "text": "Role",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRole1 = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "30px",
            "id": "lblRole1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "10dp",
            "secureTextEntry": false,
            "skin": "CopydefTextBoxNormal0c79c0816252940",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0",
            "width": "80%"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var flxImgClicks1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxImgClicks1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 10,
            "skin": "sknSearch",
            "top": "-35dp",
            "width": "25dp"
        }, {}, {});
        flxImgClicks1.setDefaultUnit(kony.flex.DP);
        flxImgClicks1.add();
        flxBenRole.add(lblBenLabel1, lblRole1, flxImgClicks1);
        flxAddRowWrap.add(flxBenName, flxBenRole);
        return flxAddRowWrap;
    }
})