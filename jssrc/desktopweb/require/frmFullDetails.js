define("frmFullDetails", function() {
    return function(controller) {
        function addWidgetsfrmFullDetails() {
            this.setDefaultUnit(kony.flex.DP);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxFullDetailsMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxFullDetailsMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "64dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0",
                "verticalScrollIndicator": true,
                "width": "95%"
            }, {}, {});
            flxFullDetailsMain.setDefaultUnit(kony.flex.DP);
            var flxBasicHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxBasicHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxBasicHeader.setDefaultUnit(kony.flex.DP);
            var titleHeader = new com.TitleTop.titleHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "125dp",
                "id": "titleHeader",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "topNameBlock",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "imgPrevArrow": {
                        "src": "bread_arrow_1.png"
                    },
                    "lblDashboard": {
                        "text": "Home /"
                    },
                    "titleHeader": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            titleHeader.flxTitleHeader.onClick = controller.AS_FlexContainer_ca99da93965c47ff910f574fcb49582f;
            var flxFullDetailstitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "125dp",
                "id": "flxFullDetailstitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxFullDetailstitle.setDefaultUnit(kony.flex.DP);
            var flxTitleHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTitleHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "32dp",
                "width": "90%"
            }, {}, {});
            flxTitleHeader.setDefaultUnit(kony.flex.DP);
            var imgPrevArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgPrevArrow",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "bread_arrow_1.png",
                "top": "0",
                "width": "8dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDashboard = new kony.ui.Label({
                "id": "lblDashboard",
                "isVisible": true,
                "left": "3dp",
                "skin": "defLabel2",
                "text": "Home /",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOverview = new kony.ui.Label({
                "id": "lblOverview",
                "isVisible": true,
                "left": "3dp",
                "skin": "defLabel2",
                "text": "Overview  /",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustId = new kony.ui.Label({
                "id": "lblCustId",
                "isVisible": true,
                "left": 3,
                "skin": "defLabel11",
                "text": "140597-7199",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTitleHeader.add(imgPrevArrow, lblDashboard, lblOverview, lblCustId);
            var lblCustName = new kony.ui.Label({
                "id": "lblCustName",
                "isVisible": true,
                "left": "0dp",
                "skin": "defLabel3",
                "text": "James May",
                "top": "14dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFullDetailstitle.add(flxTitleHeader, lblCustName);
            flxBasicHeader.add(titleHeader, flxFullDetailstitle);
            var flxBasicMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "450dp",
                "id": "flxBasicMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBasicMain.setDefaultUnit(kony.flex.DP);
            var flxBasicInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerY": "50%",
                "clipBounds": false,
                "height": "96%",
                "id": "flxBasicInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "lFbox23",
                "top": 0,
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxBasicInner.setDefaultUnit(kony.flex.DP);
            var flxBasicContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBasicContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxSCVBlock1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBasicContent.setDefaultUnit(kony.flex.DP);
            var flxBasicTitletop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxBasicTitletop",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSCVTitle1",
                "top": "1dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBasicTitletop.setDefaultUnit(kony.flex.DP);
            var flxBasicIconTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "flxBasicIconTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxBasicIconTitle.setDefaultUnit(kony.flex.DP);
            var imgBasicTitleIcon = new kony.ui.Image2({
                "centerY": "50%",
                "height": "23dp",
                "id": "imgBasicTitleIcon",
                "isVisible": false,
                "left": "21dp",
                "skin": "slImage",
                "src": "user_1.png",
                "width": "23dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBasicTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBasicTitle",
                "isVisible": true,
                "left": "16dp",
                "skin": "sknLblSCVTitle1",
                "text": "Basic Details",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBasicIconTitle.add(imgBasicTitleIcon, lblBasicTitle);
            flxBasicTitletop.add(flxBasicIconTitle);
            var flxBasicInnerContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxBasicInnerContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBasicInnerContent.setDefaultUnit(kony.flex.DP);
            var flxPersonalContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "370dp",
                "id": "flxPersonalContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "lFbox57",
                "top": "0dp",
                "width": "30%"
            }, {}, {});
            flxPersonalContent.setDefaultUnit(kony.flex.DP);
            var spinnerPersonal = new com.fulldetails.spinner({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "spinnerPersonal",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0ff6a8d8239db42",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSpinnerFulldetails": {
                        "src": "loadingblue.gif"
                    },
                    "spinner": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxPersonalColumn2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "371dp",
                "id": "flxPersonalColumn2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxPersonalColumn2.setDefaultUnit(kony.flex.DP);
            var flxPersonalTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPersonalTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "onTouchStart": controller.AS_FlexContainer_j35afdeced2b40a0ad7837378b89ee05,
                "skin": "sknFlxOnHover",
                "top": "27dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxPersonalTitle.setDefaultUnit(kony.flex.DP);
            var lblPersonal = new kony.ui.Label({
                "id": "lblPersonal",
                "isVisible": true,
                "left": "0",
                "skin": "sknLblModTitleNormal",
                "text": "Personal",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblModTitleHover"
            });
            var imgEditPersonal = new kony.ui.Image2({
                "height": "20dp",
                "id": "imgEditPersonal",
                "isVisible": false,
                "left": 10,
                "skin": "slImage",
                "src": "editicongray.png",
                "top": "0",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPersonalTitle.add(lblPersonal, imgEditPersonal);
            var flxtitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxtitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxtitle.setDefaultUnit(kony.flex.DP);
            var lblNameLabel = new kony.ui.Label({
                "id": "lblNameLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "TITLE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFlxName = new kony.ui.Label({
                "id": "lblFlxName",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Mr",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxtitle.add(lblNameLabel, lblFlxName);
            var flxShortName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxShortName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxShortName.setDefaultUnit(kony.flex.DP);
            var lblShortNameLabel = new kony.ui.Label({
                "id": "lblShortNameLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "SHORT NAME",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblShortnameValue = new kony.ui.Label({
                "id": "lblShortnameValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "James May",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxShortName.add(lblShortNameLabel, lblShortnameValue);
            var flxMarital = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxMarital",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMarital.setDefaultUnit(kony.flex.DP);
            var lblMaritallabel = new kony.ui.Label({
                "id": "lblMaritallabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "MARITAL STATUS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaritalValue = new kony.ui.Label({
                "id": "lblMaritalValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Married",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMarital.add(lblMaritallabel, lblMaritalValue);
            var flxNatioanality = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxNatioanality",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNatioanality.setDefaultUnit(kony.flex.DP);
            var lblNationalLabel = new kony.ui.Label({
                "id": "lblNationalLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "NATIONALITY",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNationValue = new kony.ui.Label({
                "id": "lblNationValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "British",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNatioanality.add(lblNationalLabel, lblNationValue);
            var flxFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxFullName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFullName.setDefaultUnit(kony.flex.DP);
            var lblFullNameLabel = new kony.ui.Label({
                "id": "lblFullNameLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "FULL NAME",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFullNameValue = new kony.ui.Label({
                "id": "lblFullNameValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "James Arthur May",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFullName.add(lblFullNameLabel, lblFullNameValue);
            flxPersonalColumn2.add(flxPersonalTitle, flxtitle, flxShortName, flxMarital, flxNatioanality, flxFullName);
            var flxPersonalColumn1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPersonalColumn1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "65dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxPersonalColumn1.setDefaultUnit(kony.flex.DP);
            var flxSalutation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSalutation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSalutation.setDefaultUnit(kony.flex.DP);
            var lblSalutationLabel = new kony.ui.Label({
                "id": "lblSalutationLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "SALUTATION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSalutation = new kony.ui.Label({
                "id": "lblSalutation",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Mr",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSalutation.add(lblSalutationLabel, lblSalutation);
            var flxDOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDOB.setDefaultUnit(kony.flex.DP);
            var lblDOBLabel = new kony.ui.Label({
                "id": "lblDOBLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "DATE OF BIRTH",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDOB = new kony.ui.Label({
                "id": "lblDOB",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "15/08/1971",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDOB.add(lblDOBLabel, lblDOB);
            var flxDependants = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDependants",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDependants.setDefaultUnit(kony.flex.DP);
            var lblDependantsLabel = new kony.ui.Label({
                "id": "lblDependantsLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "NO.OF DEPENDANTS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDependants = new kony.ui.Label({
                "id": "lblDependants",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "0",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDependants.add(lblDependantsLabel, lblDependants);
            var flxResidence = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxResidence",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlx1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxResidence.setDefaultUnit(kony.flex.DP);
            var lblResidenceLabel = new kony.ui.Label({
                "id": "lblResidenceLabel",
                "isVisible": true,
                "left": 0,
                "skin": "sknLbl3",
                "text": "RESIDENCE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRes = new kony.ui.Label({
                "id": "lblRes",
                "isVisible": true,
                "left": 0,
                "skin": "sknLbl1",
                "text": "Great Britain",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResidence.add(lblResidenceLabel, lblRes);
            var flxGender = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxGender",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlx3",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGender.setDefaultUnit(kony.flex.DP);
            var lblGenderLabel = new kony.ui.Label({
                "id": "lblGenderLabel",
                "isVisible": true,
                "left": 0,
                "skin": "sknLbl4",
                "text": "GENDER",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGender = new kony.ui.Label({
                "id": "lblGender",
                "isVisible": true,
                "left": 0,
                "skin": "sknLbl2",
                "text": "Male",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGender.add(lblGenderLabel, lblGender);
            flxPersonalColumn1.add(flxSalutation, flxDOB, flxDependants, flxResidence, flxGender);
            flxPersonalContent.add(spinnerPersonal, flxPersonalColumn2, flxPersonalColumn1);
            var flxContactContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "370dp",
                "id": "flxContactContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "lFbox47",
                "top": "0dp",
                "width": "38%"
            }, {}, {});
            flxContactContent.setDefaultUnit(kony.flex.DP);
            var spinnerContactDetails = new com.fulldetails.spinner({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "spinnerContactDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0ff6a8d8239db42",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSpinnerFulldetails": {
                        "src": "loadingblue.gif"
                    },
                    "spinner": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContactColumn1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "371dp",
                "id": "flxContactColumn1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "7%",
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "0dp",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxContactColumn1.setDefaultUnit(kony.flex.DP);
            var flxContactDetailsTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContactDetailsTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "onTouchStart": controller.AS_FlexContainer_i4ebbd12e9224a1b808e7cc615af0a32,
                "skin": "slFbox",
                "top": "27dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxContactDetailsTitle.setDefaultUnit(kony.flex.DP);
            var lblContactLabel = new kony.ui.Label({
                "id": "lblContactLabel",
                "isVisible": true,
                "skin": "sknLblModTitleNormal",
                "text": "Contact details",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblModTitleHover"
            });
            var imgEditContact = new kony.ui.Image2({
                "height": "20dp",
                "id": "imgEditContact",
                "isVisible": false,
                "left": 10,
                "skin": "slImage",
                "src": "editicongray.png",
                "top": "0",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContactDetailsTitle.add(lblContactLabel, imgEditContact);
            var flxPrimaryAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "15dp",
                "clipBounds": true,
                "height": "25%",
                "id": "flxPrimaryAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPrimaryAddress.setDefaultUnit(kony.flex.DP);
            var lblPrimaryLabel = new kony.ui.Label({
                "id": "lblPrimaryLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "PRIMARY ADDRESS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddressValue = new kony.ui.Label({
                "id": "lblAddressValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "The Met Building, 24 Percy Street London W1T 2BS UK",
                "top": "17dp",
                "width": "125dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrimaryAddress.add(lblPrimaryLabel, lblAddressValue);
            var flxSecondaryAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "40%",
                "id": "flxSecondaryAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSecondaryAddress.setDefaultUnit(kony.flex.DP);
            var lblSecondaryLabel = new kony.ui.Label({
                "id": "lblSecondaryLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "SECONDARY ADDRESS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSEcondaryValue = new kony.ui.Label({
                "id": "lblSEcondaryValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "34 Kensington Church St, Kensington, London W8 4HA, United Kingdom",
                "top": "17dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSecondaryAddress.add(lblSecondaryLabel, lblSEcondaryValue);
            flxContactColumn1.add(flxContactDetailsTitle, flxPrimaryAddress, flxSecondaryAddress);
            var flxContactColumn2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContactColumn2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "65dp",
                "width": "48%",
                "zIndex": 1
            }, {}, {});
            flxContactColumn2.setDefaultUnit(kony.flex.DP);
            var flxResidencePhone = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxResidencePhone",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxResidencePhone.setDefaultUnit(kony.flex.DP);
            var lblResidence = new kony.ui.Label({
                "id": "lblResidence",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "RESIDENCE PHONE NO.",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblResidenceValue = new kony.ui.Label({
                "id": "lblResidenceValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "+44 7792491167",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResidencePhone.add(lblResidence, lblResidenceValue);
            var flxOfficePhone = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOfficePhone",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOfficePhone.setDefaultUnit(kony.flex.DP);
            var lblOfficePhone = new kony.ui.Label({
                "id": "lblOfficePhone",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "OFFICE PHONE NO.",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOfficePhoneValue = new kony.ui.Label({
                "id": "lblOfficePhoneValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "+44 7792491167",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOfficePhone.add(lblOfficePhone, lblOfficePhoneValue);
            var flxMobile = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxMobile",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMobile.setDefaultUnit(kony.flex.DP);
            var lblMobileLabel = new kony.ui.Label({
                "id": "lblMobileLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "MOBILE NO",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMobileValue = new kony.ui.Label({
                "id": "lblMobileValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "+44 7792491167",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMobile.add(lblMobileLabel, lblMobileValue);
            var flxEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEmail",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmail.setDefaultUnit(kony.flex.DP);
            var lblEmailLabel = new kony.ui.Label({
                "id": "lblEmailLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "EMAIL ADDRESS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailValue = new kony.ui.Label({
                "id": "lblEmailValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "jimmy.m@gmail.com",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmail.add(lblEmailLabel, lblEmailValue);
            var flxFax = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxFax",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFax.setDefaultUnit(kony.flex.DP);
            var lblFaxLabel = new kony.ui.Label({
                "id": "lblFaxLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "FAX",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFaxValue = new kony.ui.Label({
                "id": "lblFaxValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "-",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFax.add(lblFaxLabel, lblFaxValue);
            flxContactColumn2.add(flxResidencePhone, flxOfficePhone, flxMobile, flxEmail, flxFax);
            flxContactContent.add(spinnerContactDetails, flxContactColumn1, flxContactColumn2);
            var flxContactMethod = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "370dp",
                "id": "flxContactMethod",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox53",
                "top": "0dp",
                "width": "28%"
            }, {}, {});
            flxContactMethod.setDefaultUnit(kony.flex.DP);
            var spinnerContactMethod = new com.fulldetails.spinner({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "spinnerContactMethod",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0ff6a8d8239db42",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSpinnerFulldetails": {
                        "src": "loadingblue.gif"
                    },
                    "spinner": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContactMethodColumn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "371dp",
                "id": "flxContactMethodColumn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "7%",
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxContactMethodColumn.setDefaultUnit(kony.flex.DP);
            var flxContactMethodTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContactMethodTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "onTouchStart": controller.AS_FlexContainer_c025905a64fd46fc8ec09c93f26e774a,
                "skin": "slFbox",
                "top": "27dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxContactMethodTitle.setDefaultUnit(kony.flex.DP);
            var lblContactMethodTitle = new kony.ui.Label({
                "id": "lblContactMethodTitle",
                "isVisible": true,
                "left": "0",
                "skin": "sknLblModTitleNormal",
                "text": "Contact Method",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblModTitleHover"
            });
            var imgEditContactMethod = new kony.ui.Image2({
                "height": "20dp",
                "id": "imgEditContactMethod",
                "isVisible": false,
                "left": 10,
                "skin": "slImage",
                "src": "editicongray.png",
                "top": "0",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContactMethodTitle.add(lblContactMethodTitle, imgEditContactMethod);
            var flxPreferredChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxPreferredChannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPreferredChannel.setDefaultUnit(kony.flex.DP);
            var lblPreferredLabel = new kony.ui.Label({
                "id": "lblPreferredLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "PREFERRED CHANNEL",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPreferredValue = new kony.ui.Label({
                "id": "lblPreferredValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Email",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreferredChannel.add(lblPreferredLabel, lblPreferredValue);
            var flxInternetBanking = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxInternetBanking",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInternetBanking.setDefaultUnit(kony.flex.DP);
            var lblInternetBanking = new kony.ui.Label({
                "id": "lblInternetBanking",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "INTERNET BANKING",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInternetBnkLabel = new kony.ui.Label({
                "id": "lblInternetBnkLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Yes",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInternetBanking.add(lblInternetBanking, lblInternetBnkLabel);
            var flxSecureMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxSecureMsg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSecureMsg.setDefaultUnit(kony.flex.DP);
            var lblSecureMsgLabel = new kony.ui.Label({
                "id": "lblSecureMsgLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "SECURE MESSAGE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSecurelblValue = new kony.ui.Label({
                "id": "lblSecurelblValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Yes",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSecureMsg.add(lblSecureMsgLabel, lblSecurelblValue);
            var flxMobileBanking = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxMobileBanking",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMobileBanking.setDefaultUnit(kony.flex.DP);
            var lblMobileBnklbl = new kony.ui.Label({
                "id": "lblMobileBnklbl",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "MOBILE BANKING",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMobileBankingVal = new kony.ui.Label({
                "id": "lblMobileBankingVal",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Yes",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMobileBanking.add(lblMobileBnklbl, lblMobileBankingVal);
            flxContactMethodColumn.add(flxContactMethodTitle, flxPreferredChannel, flxInternetBanking, flxSecureMsg, flxMobileBanking);
            flxContactMethod.add(spinnerContactMethod, flxContactMethodColumn);
            flxBasicInnerContent.add(flxPersonalContent, flxContactContent, flxContactMethod);
            flxBasicContent.add(flxBasicTitletop, flxBasicInnerContent);
            flxBasicInner.add(flxBasicContent);
            flxBasicMain.add(flxBasicInner);
            var flxKYCMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxKYCMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxKYCMain.setDefaultUnit(kony.flex.DP);
            var flxKYCInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": false,
                "height": "96%",
                "id": "flxKYCInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "lFbox23",
                "top": 0,
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxKYCInner.setDefaultUnit(kony.flex.DP);
            var flxKYCContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxKYCContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxSCVBlock1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxKYCContent.setDefaultUnit(kony.flex.DP);
            var flxKYCTitletop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxKYCTitletop",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSCVTitle1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxKYCTitletop.setDefaultUnit(kony.flex.DP);
            var flxKYCIconTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "flxKYCIconTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxKYCIconTitle.setDefaultUnit(kony.flex.DP);
            var imgKYCTitleIcon = new kony.ui.Image2({
                "centerY": "50%",
                "height": "23dp",
                "id": "imgKYCTitleIcon",
                "isVisible": false,
                "left": "21dp",
                "skin": "slImage",
                "src": "user_1.png",
                "width": "23dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblKYCTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblKYCTitle",
                "isVisible": true,
                "left": "16dp",
                "skin": "sknLblSCVTitle1",
                "text": "Know Your Customer",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxKYCIconTitle.add(imgKYCTitleIcon, lblKYCTitle);
            flxKYCTitletop.add(flxKYCIconTitle);
            var flxKYCInnerContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxKYCInnerContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxKYCInnerContent.setDefaultUnit(kony.flex.DP);
            var flxKYCCompliance = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "228dp",
                "id": "flxKYCCompliance",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "KYCLine",
                "top": "0dp",
                "width": "49%"
            }, {}, {});
            flxKYCCompliance.setDefaultUnit(kony.flex.DP);
            var spinnerKYC = new com.fulldetails.spinner({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "spinnerKYC",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0ff6a8d8239db42",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSpinnerFulldetails": {
                        "src": "loadingblue.gif"
                    },
                    "spinner": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxComplianceColumn1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxComplianceColumn1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxComplianceColumn1.setDefaultUnit(kony.flex.DP);
            var flxComplianceTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxComplianceTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "onTouchStart": controller.AS_FlexContainer_cfe836c96add454c90179211351729b3,
                "skin": "slFbox",
                "top": "27dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxComplianceTitle.setDefaultUnit(kony.flex.DP);
            var lblCompliance = new kony.ui.Label({
                "id": "lblCompliance",
                "isVisible": true,
                "skin": "sknLblModTitleNormal",
                "text": "Compliance",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblModTitleHover"
            });
            var imgEditCompliance = new kony.ui.Image2({
                "height": "20dp",
                "id": "imgEditCompliance",
                "isVisible": false,
                "left": 10,
                "skin": "slImage",
                "src": "editicongray.png",
                "top": "0",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxComplianceTitle.add(lblCompliance, imgEditCompliance);
            var flxPostingTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxPostingTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPostingTitle.setDefaultUnit(kony.flex.DP);
            var lblPostingLabel = new kony.ui.Label({
                "id": "lblPostingLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "POSTING RESTRICTIONS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPostingValue = new kony.ui.Label({
                "id": "lblPostingValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "None",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPostingTitle.add(lblPostingLabel, lblPostingValue);
            var flxAMLRisk = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxAMLRisk",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAMLRisk.setDefaultUnit(kony.flex.DP);
            var lblAMLLabel = new kony.ui.Label({
                "id": "lblAMLLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "AML RISK",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAMLValue = new kony.ui.Label({
                "id": "lblAMLValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Normal",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAMLRisk.add(lblAMLLabel, lblAMLValue);
            flxComplianceColumn1.add(flxComplianceTitle, flxPostingTitle, flxAMLRisk);
            var flxComplianceColumn2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxComplianceColumn2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "65dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxComplianceColumn2.setDefaultUnit(kony.flex.DP);
            var flxCRSStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxCRSStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCRSStatus.setDefaultUnit(kony.flex.DP);
            var lblCRSLabel = new kony.ui.Label({
                "id": "lblCRSLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "CRS STATUS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCRSValue = new kony.ui.Label({
                "id": "lblCRSValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Non reportable",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCRSStatus.add(lblCRSLabel, lblCRSValue);
            var flxCRSJURU = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxCRSJURU",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCRSJURU.setDefaultUnit(kony.flex.DP);
            var lblCRSJURULabel = new kony.ui.Label({
                "id": "lblCRSJURULabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "CRS JURISDICTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCRSJURUValue = new kony.ui.Label({
                "id": "lblCRSJURUValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "None",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCRSJURU.add(lblCRSJURULabel, lblCRSJURUValue);
            flxComplianceColumn2.add(flxCRSStatus, flxCRSJURU);
            var flxComplianceColumn3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxComplianceColumn3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "65dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxComplianceColumn3.setDefaultUnit(kony.flex.DP);
            var flxFATCA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxFATCA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFATCA.setDefaultUnit(kony.flex.DP);
            var lblFATCAlabel = new kony.ui.Label({
                "id": "lblFATCAlabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "FATCA STATUS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFATCAValue = new kony.ui.Label({
                "id": "lblFATCAValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "None",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFATCA.add(lblFATCAlabel, lblFATCAValue);
            flxComplianceColumn3.add(flxFATCA);
            flxKYCCompliance.add(spinnerKYC, flxComplianceColumn1, flxComplianceColumn2, flxComplianceColumn3);
            var flxConsents = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "228dp",
                "id": "flxConsents",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "540dp"
            }, {}, {});
            flxConsents.setDefaultUnit(kony.flex.DP);
            var spinnerConsents = new com.fulldetails.spinner({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "spinnerConsents",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0ff6a8d8239db42",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSpinnerFulldetails": {
                        "src": "loadingblue.gif"
                    },
                    "spinner": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxProtection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "228dp",
                "id": "flxProtection",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "lFbox39",
                "top": "0dp",
                "width": "240dp"
            }, {}, {});
            flxProtection.setDefaultUnit(kony.flex.DP);
            var flxDataprotection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDataprotection",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "8%",
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxDataprotection.setDefaultUnit(kony.flex.DP);
            var flxDataProtectionTtile = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDataProtectionTtile",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "27dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDataProtectionTtile.setDefaultUnit(kony.flex.DP);
            var lblDataprtLabel = new kony.ui.Label({
                "id": "lblDataprtLabel",
                "isVisible": true,
                "skin": "sknLblModTitleNormal",
                "text": "Data Protection consents",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgEditDataProtection = new kony.ui.Image2({
                "height": "20dp",
                "id": "imgEditDataProtection",
                "isVisible": false,
                "left": 5,
                "skin": "slImage",
                "src": "editicongray.png",
                "top": "0",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDataProtectionTtile.add(lblDataprtLabel, imgEditDataProtection);
            var flxDirectMarketing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDirectMarketing",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxDirectMarketing.setDefaultUnit(kony.flex.DP);
            var lblDirectingMarketing = new kony.ui.Label({
                "id": "lblDirectingMarketing",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "DIRECT MARKETING",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDirectingValue = new kony.ui.Label({
                "id": "lblDirectingValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Yes",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDirectMarketing.add(lblDirectingMarketing, lblDirectingValue);
            var flxCreditCheck = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxCreditCheck",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxCreditCheck.setDefaultUnit(kony.flex.DP);
            var lblCreditLabel = new kony.ui.Label({
                "id": "lblCreditLabel",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel60",
                "text": "CREDIT CHECK",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCreditValue = new kony.ui.Label({
                "id": "lblCreditValue",
                "isVisible": true,
                "left": 0,
                "skin": "skndefLabel39",
                "text": "Yes",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCreditCheck.add(lblCreditLabel, lblCreditValue);
            flxDataprotection.add(flxDataProtectionTtile, flxDirectMarketing, flxCreditCheck);
            flxProtection.add(flxDataprotection);
            var flxPersonalConsents = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPersonalConsents",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox58",
                "top": "0dp",
                "width": "300dp"
            }, {}, {});
            flxPersonalConsents.setDefaultUnit(kony.flex.DP);
            var flxPCContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPCContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxPCContent.setDefaultUnit(kony.flex.DP);
            var flxPersonalConsentsTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPersonalConsentsTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "27dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxPersonalConsentsTitle.setDefaultUnit(kony.flex.DP);
            var lblPCTitle = new kony.ui.Label({
                "id": "lblPCTitle",
                "isVisible": true,
                "left": "0",
                "skin": "sknLblModTitleNormal",
                "text": "Personal consents",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgEditPersonalConsents = new kony.ui.Image2({
                "height": "20dp",
                "id": "imgEditPersonalConsents",
                "isVisible": false,
                "left": 10,
                "skin": "slImage",
                "src": "editicongray.png",
                "top": "0",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPersonalConsentsTitle.add(lblPCTitle, imgEditPersonalConsents);
            var flxPCInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "24dp",
                "clipBounds": true,
                "height": "75dp",
                "id": "flxPCInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxPCInner.setDefaultUnit(kony.flex.DP);
            var flxTmpPersonal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxTmpPersonal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSegFlxTmpInner2",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxTmpPersonal.setDefaultUnit(kony.flex.DP);
            var flxPersonalConsentInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "45dp",
                "id": "flxPersonalConsentInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxPersonalConsentInner.setDefaultUnit(kony.flex.DP);
            var lblPCLabel = new kony.ui.Label({
                "id": "lblPCLabel",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndef2label",
                "text": "AA19107TJ1FL",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPCValue = new kony.ui.Label({
                "id": "lblPCValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndefLabel57",
                "text": "Authorised",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPersonalConsentInner.add(lblPCLabel, lblPCValue);
            flxTmpPersonal.add(flxPersonalConsentInner);
            flxPCInner.add(flxTmpPersonal);
            flxPCContent.add(flxPersonalConsentsTitle, flxPCInner);
            flxPersonalConsents.add(flxPCContent);
            flxConsents.add(spinnerConsents, flxProtection, flxPersonalConsents);
            flxKYCInnerContent.add(flxKYCCompliance, flxConsents);
            flxKYCContent.add(flxKYCTitletop, flxKYCInnerContent);
            flxKYCInner.add(flxKYCContent);
            flxKYCMain.add(flxKYCInner);
            var flxRelationFinancial = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRelationFinancial",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "99%"
            }, {}, {});
            flxRelationFinancial.setDefaultUnit(kony.flex.DP);
            var flxRelFinInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "400dp",
                "id": "flxRelFinInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRelFinInner.setDefaultUnit(kony.flex.DP);
            var flxRelationChart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerY": "50%",
                "clipBounds": false,
                "height": "96%",
                "id": "flxRelationChart",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "lFbox23",
                "top": 0,
                "width": "48%",
                "zIndex": 1
            }, {}, {});
            flxRelationChart.setDefaultUnit(kony.flex.DP);
            var flxRelChartInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRelChartInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxSCVBlock1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRelChartInner.setDefaultUnit(kony.flex.DP);
            var flxRelTitletop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxRelTitletop",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSCVTitle1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRelTitletop.setDefaultUnit(kony.flex.DP);
            var flxRelIconTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "flxRelIconTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxRelIconTitle.setDefaultUnit(kony.flex.DP);
            var imgRelTitleIcon = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "imgRelTitleIcon",
                "isVisible": false,
                "left": "21dp",
                "skin": "slImage",
                "src": "relationuser.png",
                "width": "25dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRelTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRelTitle",
                "isVisible": true,
                "left": "16dp",
                "skin": "sknLblSCVTitle1",
                "text": "Relationships",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyimgEditPersonal0e465e2d4bf7f40 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20dp",
                "id": "CopyimgEditPersonal0e465e2d4bf7f40",
                "isVisible": false,
                "left": 10,
                "skin": "slImage",
                "src": "editicongray.png",
                "top": "0",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRelIconTitle.add(imgRelTitleIcon, lblRelTitle, CopyimgEditPersonal0e465e2d4bf7f40);
            flxRelTitletop.add(flxRelIconTitle);
            var spinnerRelation = new com.fulldetails.spinner({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "spinnerRelation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0ff6a8d8239db42",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSpinnerFulldetails": {
                        "src": "loadingblue.gif"
                    },
                    "spinner": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxOverContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxOverContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxOverContent.setDefaultUnit(kony.flex.DP);
            var flxDetail1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "400dp",
                "id": "flxDetail1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "lFbox21",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetail1.setDefaultUnit(kony.flex.DP);
            var lblNoRel = new kony.ui.Label({
                "id": "lblNoRel",
                "isVisible": false,
                "left": "0",
                "skin": "CopydefLabel0j3890b7f04f34d",
                "text": "No relationship data found.",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var relationship = new com.customer.relationship.relationship({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "relationship",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "relationship": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "segRelationship": {
                        "data": [{
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }, {
                            "relatedPartyNameVal": "No record"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDetail1.add(lblNoRel, relationship);
            flxOverContent.add(flxDetail1);
            flxRelChartInner.add(flxRelTitletop, spinnerRelation, flxOverContent);
            flxRelationChart.add(flxRelChartInner);
            var flxDocument = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": false,
                "height": "96%",
                "id": "flxDocument",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "lFbox23",
                "top": 0,
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxDocument.setDefaultUnit(kony.flex.DP);
            var flxDocumentInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDocumentInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxSCVBlock1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDocumentInner.setDefaultUnit(kony.flex.DP);
            var flxDocTitletop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxDocTitletop",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSCVTitle1",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDocTitletop.setDefaultUnit(kony.flex.DP);
            var flxDocIconTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "60%",
                "id": "flxDocIconTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxDocIconTitle.setDefaultUnit(kony.flex.DP);
            var imgDocTitleIcon = new kony.ui.Image2({
                "centerY": "50%",
                "height": "26dp",
                "id": "imgDocTitleIcon",
                "isVisible": false,
                "left": "21dp",
                "skin": "slImage",
                "src": "document.png",
                "width": "21dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDocTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDocTitle",
                "isVisible": true,
                "left": "16dp",
                "skin": "sknLblSCVTitle1",
                "text": "Documents & Images",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDocIconTitle.add(imgDocTitleIcon, lblDocTitle);
            var flxUpload = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "70%",
                "id": "flxUpload",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxUpload1",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxUpload.setDefaultUnit(kony.flex.DP);
            var btnUpload = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnSCVTitleRightHover",
                "height": "35dp",
                "id": "btnUpload",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnSCVTitleRight1",
                "text": "UPLOAD",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "displayText": true,
                "padding": [7, 0, 7, 0],
                "paddingInPixel": false
            }, {});
            flxUpload.add(btnUpload);
            flxDocTitletop.add(flxDocIconTitle, flxUpload);
            var spinnerDoc = new com.fulldetails.spinner({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "spinnerDoc",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0ff6a8d8239db42",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSpinnerFulldetails": {
                        "src": "loadingblue.gif"
                    },
                    "spinner": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDocContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxDocContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxDocContent.setDefaultUnit(kony.flex.DP);
            var flxDocsDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxDocsDown",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "docsDownSec",
                "top": "0dp",
                "width": "48%",
                "zIndex": 1
            }, {}, {});
            flxDocsDown.setDefaultUnit(kony.flex.DP);
            var lblDocsDown = new kony.ui.Label({
                "id": "lblDocsDown",
                "isVisible": true,
                "left": "0",
                "skin": "sknLblModTitleNormal",
                "text": "Documents",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDocPayOff = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": false,
                "height": "60dp",
                "id": "flxDocPayOff",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSegFlxTmpInner2",
                "top": "20dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxDocPayOff.setDefaultUnit(kony.flex.DP);
            var flxPAyOffInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "45dp",
                "id": "flxPAyOffInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxPAyOffInner.setDefaultUnit(kony.flex.DP);
            var lblPayOffLabel = new kony.ui.Label({
                "id": "lblPayOffLabel",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndef2label",
                "text": "RECEIVED 16/04/10  /  UPDATED 16/04/10",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPayOffValue = new kony.ui.Label({
                "id": "lblPayOffValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndefLabel57",
                "text": "Payoff Statement",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPAyOffInner.add(lblPayOffLabel, lblPayOffValue);
            var FlexContainer0i423b23fc00648 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "19dp",
                "id": "FlexContainer0i423b23fc00648",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "16dp"
            }, {}, {});
            FlexContainer0i423b23fc00648.setDefaultUnit(kony.flex.DP);
            var imgDownDoc = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "imgDownDoc",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "download.png",
                "top": "0",
                "width": "16dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            FlexContainer0i423b23fc00648.add(imgDownDoc);
            flxDocPayOff.add(flxPAyOffInner, FlexContainer0i423b23fc00648);
            var flxDX = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": false,
                "height": "60dp",
                "id": "flxDX",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSegFlxTmpInner2",
                "top": "0dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxDX.setDefaultUnit(kony.flex.DP);
            var flxDXInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "45dp",
                "id": "flxDXInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxDXInner.setDefaultUnit(kony.flex.DP);
            var lblDXLabel = new kony.ui.Label({
                "id": "lblDXLabel",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndef2label",
                "text": "RECEIVED 16/04/10  /  UPDATED 16/04/10",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDXValue = new kony.ui.Label({
                "id": "lblDXValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndefLabel57",
                "text": "DX Trading",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDXInner.add(lblDXLabel, lblDXValue);
            var flxDXDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "19dp",
                "id": "flxDXDown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "16dp"
            }, {}, {});
            flxDXDown.setDefaultUnit(kony.flex.DP);
            var imgDownDocDX = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "imgDownDocDX",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "download.png",
                "top": "0",
                "width": "16dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDXDown.add(imgDownDocDX);
            flxDX.add(flxDXInner, flxDXDown);
            var flxIntroductory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": false,
                "height": "60dp",
                "id": "flxIntroductory",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSegFlxTmpInner2",
                "top": "0dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxIntroductory.setDefaultUnit(kony.flex.DP);
            var flxIntroInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "45dp",
                "id": "flxIntroInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxIntroInner.setDefaultUnit(kony.flex.DP);
            var lblIntroLabel = new kony.ui.Label({
                "id": "lblIntroLabel",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndef2label",
                "text": "NOT RECEIVED 15/05/10",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIntroValue = new kony.ui.Label({
                "id": "lblIntroValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndefLabel57",
                "text": "Introductory Document",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIntroInner.add(lblIntroLabel, lblIntroValue);
            var flxIntroDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "19dp",
                "id": "flxIntroDown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "16dp"
            }, {}, {});
            flxIntroDown.setDefaultUnit(kony.flex.DP);
            var imgIntroDown = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "imgIntroDown",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "download.png",
                "top": "0",
                "width": "16dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIntroDown.add(imgIntroDown);
            flxIntroductory.add(flxIntroInner, flxIntroDown);
            var lbl2More = new kony.ui.Label({
                "id": "lbl2More",
                "isVisible": false,
                "left": "0",
                "skin": "defLabel1",
                "text": "2 MORE",
                "top": "12dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoRecordDoc = new kony.ui.Label({
                "id": "lblNoRecordDoc",
                "isVisible": false,
                "left": "0",
                "skin": "CopydefLabel0a420ecc77bed42",
                "text": "No Records Found",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var documents = new com.documentImages.doc({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "280dp",
                "id": "documents",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "doc": {
                        "bottom": "viz.val_cleared",
                        "height": "280dp",
                        "top": "10dp"
                    },
                    "segDoc": {
                        "bottom": "viz.val_cleared",
                        "data": [{
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }],
                        "height": "280dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            documents.segDoc.onRowClick = controller.AS_Segment_hdbf87504a9f44d09e29c6d9df92f953;
            flxDocsDown.add(lblDocsDown, flxDocPayOff, flxDX, flxIntroductory, lbl2More, lblNoRecordDoc, documents);
            var flxImgDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxImgDown",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "docsDownSec",
                "top": "0dp",
                "width": "48%",
                "zIndex": 1
            }, {}, {});
            flxImgDown.setDefaultUnit(kony.flex.DP);
            var lblImgTitle = new kony.ui.Label({
                "id": "lblImgTitle",
                "isVisible": true,
                "left": "0",
                "skin": "sknLblModTitleNormal",
                "text": "Images",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPhotoId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": false,
                "height": "60dp",
                "id": "flxPhotoId",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSegFlxTmpInner2",
                "top": "20dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxPhotoId.setDefaultUnit(kony.flex.DP);
            var flxPhotoIdInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "45dp",
                "id": "flxPhotoIdInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxPhotoIdInner.setDefaultUnit(kony.flex.DP);
            var lblPhotoIdLabel = new kony.ui.Label({
                "id": "lblPhotoIdLabel",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndef2label",
                "text": "RECEIVED 16/04/10  /  UPDATED 16/04/10",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPhotoIdValue = new kony.ui.Label({
                "id": "lblPhotoIdValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndefLabel57",
                "text": "Photo ID",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPhotoIdInner.add(lblPhotoIdLabel, lblPhotoIdValue);
            var flxPhotoDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "19dp",
                "id": "flxPhotoDown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "16dp"
            }, {}, {});
            flxPhotoDown.setDefaultUnit(kony.flex.DP);
            var imgPhotoIdDown = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "imgPhotoIdDown",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "download.png",
                "top": "0",
                "width": "16dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPhotoDown.add(imgPhotoIdDown);
            flxPhotoId.add(flxPhotoIdInner, flxPhotoDown);
            var flxSignature = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": false,
                "height": "60dp",
                "id": "flxSignature",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSegFlxTmpInner2",
                "top": "0dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxSignature.setDefaultUnit(kony.flex.DP);
            var flxSignatureInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "45dp",
                "id": "flxSignatureInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxSignatureInner.setDefaultUnit(kony.flex.DP);
            var lblSigLabel = new kony.ui.Label({
                "id": "lblSigLabel",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndef2label",
                "text": "RECEIVED 16/04/10  /  UPDATED 16/04/10",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSignatureValue = new kony.ui.Label({
                "id": "lblSignatureValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "skndefLabel57",
                "text": "Signature",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSignatureInner.add(lblSigLabel, lblSignatureValue);
            var flxSigDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "19dp",
                "id": "flxSigDown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "16dp"
            }, {}, {});
            flxSigDown.setDefaultUnit(kony.flex.DP);
            var imgSignatureDown = new kony.ui.Image2({
                "centerY": "50%",
                "height": "18dp",
                "id": "imgSignatureDown",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "download.png",
                "top": "0",
                "width": "16dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSigDown.add(imgSignatureDown);
            flxSignature.add(flxSignatureInner, flxSigDown);
            var lblNoRecordImg = new kony.ui.Label({
                "id": "lblNoRecordImg",
                "isVisible": false,
                "left": "0",
                "skin": "CopydefLabel0a579ad22bc0d4f",
                "text": "No Records Found",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var images = new com.documentImages.doc({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "280dp",
                "id": "images",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "doc": {
                        "bottom": "viz.val_cleared",
                        "height": "280dp",
                        "minWidth": "viz.val_cleared",
                        "top": "10dp"
                    },
                    "segDoc": {
                        "bottom": "viz.val_cleared",
                        "data": [{
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }, {
                            "imgDownload": "download_2.png",
                            "lblName": "Name",
                            "recievedDate": "Received",
                            "updatedDate": "Updated"
                        }],
                        "height": "280dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            images.segDoc.onRowClick = controller.AS_Segment_b7520f12d410480ea1e0bfbe2814c4fd;
            flxImgDown.add(lblImgTitle, flxPhotoId, flxSignature, lblNoRecordImg, images);
            flxDocContent.add(flxDocsDown, flxImgDown);
            flxDocumentInner.add(flxDocTitletop, spinnerDoc, flxDocContent);
            flxDocument.add(flxDocumentInner);
            flxRelFinInner.add(flxRelationChart, flxDocument);
            flxRelationFinancial.add(flxRelFinInner);
            flxFullDetailsMain.add(flxBasicHeader, flxBasicMain, flxKYCMain, flxRelationFinancial);
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1366,
                "640": {
                    "flxBasicMain": {
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "segmentProps": []
                    },
                    "flxBasicInner": {
                        "segmentProps": []
                    },
                    "flxKYCMain": {
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "segmentProps": []
                    },
                    "flxKYCInner": {
                        "segmentProps": []
                    },
                    "flxRelFinInner": {
                        "layoutType": kony.flex.FLOW_VERTICAL,
                        "segmentProps": []
                    },
                    "flxRelationChart": {
                        "segmentProps": []
                    },
                    "flxDocument": {
                        "segmentProps": []
                    }
                },
                "1200": {
                    "flxBasicMain": {
                        "segmentProps": []
                    },
                    "flxKYCMain": {
                        "segmentProps": []
                    },
                    "flxRelFinInner": {
                        "segmentProps": []
                    }
                }
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "titleHeader.imgPrevArrow": {
                    "src": "bread_arrow_1.png"
                },
                "titleHeader.lblDashboard": {
                    "text": "Home /"
                },
                "spinnerPersonal.imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinnerContactDetails.imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinnerContactMethod.imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinnerKYC.imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinnerConsents.imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinnerRelation.imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "relationship.segRelationship": {
                    "data": [{
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }]
                },
                "spinnerDoc.imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "documents": {
                    "bottom": "",
                    "height": "280dp",
                    "top": "10dp"
                },
                "documents.segDoc": {
                    "bottom": "",
                    "data": [{
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }],
                    "height": "280dp"
                },
                "images": {
                    "bottom": "",
                    "height": "280dp",
                    "minWidth": "",
                    "top": "10dp"
                },
                "images.segDoc": {
                    "bottom": "",
                    "data": [{
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }],
                    "height": "280dp"
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(uuxNavigationRail, flxFullDetailsMain, ProgressIndicator);
        };
        return [{
            "addWidgets": addWidgetsfrmFullDetails,
            "enabledForIdleTimeout": false,
            "id": "frmFullDetails",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1200, 1366]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});