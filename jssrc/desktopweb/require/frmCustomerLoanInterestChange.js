define("frmCustomerLoanInterestChange", function() {
    return function(controller) {
        function addWidgetsfrmCustomerLoanInterestChange() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "pagingEnabled": false,
                "right": "24dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxContainerBGF0e8ea705f084647",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxSeparatorLine1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLine1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "sknSeparatorBlue",
                "top": "291dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLine1.setDefaultUnit(kony.flex.DP);
            flxSeparatorLine1.add();
            var flxLoanInterestChange = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxLoanInterestChange",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknFlxMultiBGCBE3F8",
                "top": "421dp",
                "width": "98%",
                "zIndex": 5
            }, {}, {});
            flxLoanInterestChange.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "45%",
                "clipBounds": false,
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "1300dp",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var TitleLabel = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "19%",
                "height": "70dp",
                "id": "TitleLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "500dp",
                "overrides": {
                    "TitleLabel": {
                        "centerX": "19%",
                        "height": "70dp",
                        "width": "500dp"
                    },
                    "flxTitle": {
                        "isVisible": false,
                        "left": "200dp"
                    },
                    "lblTitle": {
                        "centerX": "viz.val_cleared",
                        "left": "220dp",
                        "text": "Change Interest"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblChangeInterest = new kony.ui.Label({
                "height": "40dp",
                "id": "lblChangeInterest",
                "isVisible": false,
                "left": "10%",
                "skin": "sknLbl16pxBG003E75",
                "text": "Change Interest",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustomFields = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "80dp",
                "id": "flxCustomFields",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxCustomFields.setDefaultUnit(kony.flex.DP);
            var lblSelectModeInfo = new kony.ui.Label({
                "centerX": "20%",
                "height": "60dp",
                "id": "lblSelectModeInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblBg000000px14",
                "text": "Select Mode",
                "top": "20dp",
                "width": "94dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 1],
                "paddingInPixel": false
            }, {});
            var cusRadioButtonSelectionModeValue = new kony.ui.CustomWidget({
                "id": "cusRadioButtonSelectionModeValue",
                "isVisible": true,
                "left": "26dp",
                "top": "20dp",
                "width": "200dp",
                "height": "60dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRadioGroup",
                "labelText": "",
                "onRadioChange": "",
                "options": {
                    "selectedValue": ""
                },
                "value": ""
            });
            var cusTierListResponse = new kony.ui.CustomWidget({
                "id": "cusTierListResponse",
                "isVisible": true,
                "left": "40dp",
                "top": "20dp",
                "width": "265dp",
                "height": "60dp",
                "zIndex": 2,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "labelText": "Tier Type",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": true,
                "validateRequired": "",
                "validationMessage": "",
                "value": "Single Rate"
            });
            var cusEffectiveDateResponse = new kony.ui.CustomWidget({
                "id": "cusEffectiveDateResponse",
                "isVisible": true,
                "left": "45dp",
                "top": "20dp",
                "width": "313dp",
                "height": "60dp",
                "zIndex": 5,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Effective Date",
                "max": "",
                "min": "1900-01-01",
                "onDone": "",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusDropDownTierListResponse = new kony.ui.CustomWidget({
                "id": "cusDropDownTierListResponse",
                "isVisible": false,
                "left": "523dp",
                "top": "20dp",
                "width": "265dp",
                "height": "40dp",
                "centerX": "0%",
                "zIndex": 2,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "disabled": false,
                "labelText": "Tier Type",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            flxCustomFields.add(lblSelectModeInfo, cusRadioButtonSelectionModeValue, cusTierListResponse, cusEffectiveDateResponse, cusDropDownTierListResponse);
            var flxVerticalFlow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxVerticalFlow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "160dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxVerticalFlow.setDefaultUnit(kony.flex.DP);
            var flxFixedModeWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFixedModeWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxFixedModeWrapper.setDefaultUnit(kony.flex.DP);
            var cusFixedRateResponse = new kony.ui.CustomWidget({
                "id": "cusFixedRateResponse",
                "isVisible": false,
                "left": "74dp",
                "top": "1dp",
                "width": "145dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": false,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "labelText": "Label",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            var cusOperandResponse = new kony.ui.CustomWidget({
                "id": "cusOperandResponse",
                "isVisible": false,
                "left": "265dp",
                "top": "1dp",
                "width": "145dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "disabled": false,
                "labelText": "Operand",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var cusMarginResponse = new kony.ui.CustomWidget({
                "id": "cusMarginResponse",
                "isVisible": false,
                "left": "518dp",
                "top": "1dp",
                "width": "145dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": false,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "labelText": "Label",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            var cusEffectiveRateResponse = new kony.ui.CustomWidget({
                "id": "cusEffectiveRateResponse",
                "isVisible": false,
                "left": "703dp",
                "top": "1dp",
                "width": "153dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": false,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Label",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            var cusUptoAmountResponse = new kony.ui.CustomWidget({
                "id": "cusUptoAmountResponse",
                "isVisible": false,
                "left": "888dp",
                "top": "1dp",
                "width": "145dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": false,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "labelText": "Label",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            var cusButtonDelete = new kony.ui.CustomWidget({
                "id": "cusButtonDelete",
                "isVisible": false,
                "left": "1075dp",
                "top": "11dp",
                "width": "145dp",
                "height": "60dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "remove_circle_outline",
                "labelText": "Delete",
                "outlined": false,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxSegHeaderFixed = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSegHeaderFixed",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSegHeaderFixed.setDefaultUnit(kony.flex.DP);
            var lblFixedRateInfo = new kony.ui.Label({
                "height": "21dp",
                "id": "lblFixedRateInfo",
                "isVisible": true,
                "left": "15%",
                "skin": "sknLbl14px0074E3BG",
                "text": "Fixed Rate *",
                "top": "9dp",
                "width": "146dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOperandInfo = new kony.ui.Label({
                "height": "21dp",
                "id": "lblOperandInfo",
                "isVisible": true,
                "left": "60dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Operand",
                "top": "9dp",
                "width": "160dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMarginInfo = new kony.ui.Label({
                "height": "21dp",
                "id": "lblMarginInfo",
                "isVisible": true,
                "left": "60dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Margin",
                "top": "9dp",
                "width": "145dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEffectiveRateInfo = new kony.ui.Label({
                "height": "21dp",
                "id": "lblEffectiveRateInfo",
                "isVisible": true,
                "left": "60dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Effective Rate",
                "top": "9dp",
                "width": "145dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUptoAmountInfo = new kony.ui.Label({
                "height": "21dp",
                "id": "lblUptoAmountInfo",
                "isVisible": true,
                "left": "60dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Upto Amount",
                "top": "9dp",
                "width": "145dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegHeaderFixed.add(lblFixedRateInfo, lblOperandInfo, lblMarginInfo, lblEffectiveRateInfo, lblUptoAmountInfo);
            var segFixed = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgDelete": "minus.png",
                    "lblDelete": "Delete",
                    "lblEffectiveRate": "Label",
                    "lblFixedRate": "",
                    "lblMargin": "",
                    "lblUptoAmount": "Label",
                    "lstOperand": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    }
                }, {
                    "imgDelete": "minus.png",
                    "lblDelete": "Delete",
                    "lblEffectiveRate": "Label",
                    "lblFixedRate": "",
                    "lblMargin": "",
                    "lblUptoAmount": "Label",
                    "lstOperand": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    }
                }, {
                    "imgDelete": "minus.png",
                    "lblDelete": "Delete",
                    "lblEffectiveRate": "Label",
                    "lblFixedRate": "",
                    "lblMargin": "",
                    "lblUptoAmount": "Label",
                    "lstOperand": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    }
                }],
                "groupCells": false,
                "id": "segFixed",
                "isVisible": true,
                "left": "15%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "sknSegTransparent",
                "rowSkin": "sknSegTransparent",
                "rowTemplate": "flxSegLoanFixed",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646464",
                "separatorRequired": true,
                "separatorThickness": 20,
                "showScrollbars": false,
                "top": "45dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDeleteButton": "flxDeleteButton",
                    "flxSegLoanFixed": "flxSegLoanFixed",
                    "imgDelete": "imgDelete",
                    "lblDelete": "lblDelete",
                    "lblEffectiveRate": "lblEffectiveRate",
                    "lblFixedRate": "lblFixedRate",
                    "lblMargin": "lblMargin",
                    "lblUptoAmount": "lblUptoAmount",
                    "lstOperand": "lstOperand"
                },
                "width": "1080dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFixedModeWrapper.add(cusFixedRateResponse, cusOperandResponse, cusMarginResponse, cusEffectiveRateResponse, cusUptoAmountResponse, cusButtonDelete, flxSegHeaderFixed, segFixed);
            var flxFloatingModeWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFloatingModeWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxFloatingModeWrapper.setDefaultUnit(kony.flex.DP);
            var flxSgHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "45%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSgHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxSgHeader.setDefaultUnit(kony.flex.DP);
            var lblHeaderFloatingRate = new kony.ui.Label({
                "height": "21dp",
                "id": "lblHeaderFloatingRate",
                "isVisible": true,
                "left": "17%",
                "skin": "sknLbl14px0074E3BG",
                "text": "Floating Rate *",
                "top": "9dp",
                "width": "210dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeaderInterestRate = new kony.ui.Label({
                "height": "21dp",
                "id": "lblHeaderInterestRate",
                "isVisible": true,
                "left": "22dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Interest Rate",
                "top": "9dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeaderOperand = new kony.ui.Label({
                "height": "21dp",
                "id": "lblHeaderOperand",
                "isVisible": true,
                "left": "24dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Operand",
                "top": "9dp",
                "width": "162dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeaderMargin = new kony.ui.Label({
                "height": "21dp",
                "id": "lblHeaderMargin",
                "isVisible": true,
                "left": "22dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Margin",
                "top": "9dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeaderEffectiverate = new kony.ui.Label({
                "height": "21dp",
                "id": "lblHeaderEffectiverate",
                "isVisible": true,
                "left": "22dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Effective Rate",
                "top": "9dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeaderUptoAmount = new kony.ui.Label({
                "height": "21dp",
                "id": "lblHeaderUptoAmount",
                "isVisible": true,
                "left": "22dp",
                "skin": "sknLbl14px0074E3BG",
                "text": "Upto Amount",
                "top": "9dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSgHeader.add(lblHeaderFloatingRate, lblHeaderInterestRate, lblHeaderOperand, lblHeaderMargin, lblHeaderEffectiverate, lblHeaderUptoAmount);
            var segFloating = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgDelete": "minus.png",
                    "lblDelete": "Delete",
                    "lblEffectiveRate": "Label",
                    "lblInterestRate": "Label",
                    "lblMargin": "",
                    "lblUptoAmount": "Label",
                    "lstFloatingrate": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    },
                    "lstOperand": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    }
                }, {
                    "imgDelete": "minus.png",
                    "lblDelete": "Delete",
                    "lblEffectiveRate": "Label",
                    "lblInterestRate": "Label",
                    "lblMargin": "",
                    "lblUptoAmount": "Label",
                    "lstFloatingrate": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    },
                    "lstOperand": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    }
                }, {
                    "imgDelete": "minus.png",
                    "lblDelete": "Delete",
                    "lblEffectiveRate": "Label",
                    "lblInterestRate": "Label",
                    "lblMargin": "",
                    "lblUptoAmount": "Label",
                    "lstFloatingrate": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    },
                    "lstOperand": {
                        "masterData": [
                            ["lb1", "Add"],
                            ["lb2", "SUB"],
                            ["lb3", "MUL"]
                        ],
                        "selectedKey": "lb1",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb1", "Add"]
                    }
                }],
                "groupCells": false,
                "id": "segFloating",
                "isVisible": true,
                "left": "15%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "sknSegTransparent",
                "rowTemplate": "flxSegLoanFloating",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646464",
                "separatorRequired": true,
                "separatorThickness": 20,
                "showScrollbars": false,
                "top": "45dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDeleteButton": "flxDeleteButton",
                    "flxSegLoanFloating": "flxSegLoanFloating",
                    "imgDelete": "imgDelete",
                    "lblDelete": "lblDelete",
                    "lblEffectiveRate": "lblEffectiveRate",
                    "lblInterestRate": "lblInterestRate",
                    "lblMargin": "lblMargin",
                    "lblUptoAmount": "lblUptoAmount",
                    "lstFloatingrate": "lstFloatingrate",
                    "lstOperand": "lstOperand"
                },
                "width": "1080dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFloatingModeWrapper.add(flxSgHeader, segFloating);
            var lblErrorMsg = new kony.ui.Label({
                "height": "21dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "15%",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "16dp",
                "width": "257dp",
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtonsHzFlow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxButtonsHzFlow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "24dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtonsHzFlow.setDefaultUnit(kony.flex.DP);
            var cusButtonConfirm = new kony.ui.CustomWidget({
                "id": "cusButtonConfirm",
                "isVisible": true,
                "left": "970dp",
                "top": "16dp",
                "width": "204dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusButtonCancel = new kony.ui.CustomWidget({
                "id": "cusButtonCancel",
                "isVisible": true,
                "left": "750dp",
                "top": "16dp",
                "width": "196dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxButtonsHzFlow.add(cusButtonConfirm, cusButtonCancel);
            flxVerticalFlow.add(flxFixedModeWrapper, flxFloatingModeWrapper, lblErrorMsg, flxButtonsHzFlow);
            var flxSeparatorLineGrey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLineGrey",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15%",
                "isModalContainer": false,
                "skin": "CopyslFbox0fda5754cd9f44b",
                "top": 70,
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLineGrey.setDefaultUnit(kony.flex.DP);
            flxSeparatorLineGrey.add();
            flxMain.add(TitleLabel, lblChangeInterest, flxCustomFields, flxVerticalFlow, flxSeparatorLineGrey);
            flxLoanInterestChange.add(flxMain);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false
                    },
                    "imgCloseBackup": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "300dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "45dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "105dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "122dp",
                "skin": "CopysknLbl0cb213a34c30f4f",
                "text": "Overview",
                "top": "26dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadboardChangeInterestSplash = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadboardChangeInterestSplash",
                "isVisible": true,
                "left": "180dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "26dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardChangeInterest = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardChangeInterest",
                "isVisible": true,
                "left": "200dp",
                "skin": "sknLblLoanServiceBlacl12px",
                "text": "Change Interest",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue, cusBackIcon, lblBreadboardChangeInterestSplash, lblBreadBoardChangeInterest);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "32dp",
                "skin": "sknFlxBorderCCE3F7",
                "top": "70dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var MainTabs = new com.lending.MainTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "227dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "MainTabs": {
                        "left": "0dp",
                        "top": "227dp",
                        "width": "98%"
                    },
                    "btnAccounts1": {
                        "left": 40
                    },
                    "flxContainerAccount": {
                        "left": "27dp"
                    },
                    "flxontainerLoans": {
                        "left": "245dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var loanDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "loanDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "291dp",
                "width": "98%",
                "overrides": {
                    "flxListBoxSelectedValue": {
                        "left": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "flxLoanDetailsvalue": {
                        "height": "63dp"
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "left": "8dp",
                        "src": "drop_down_arrow.png"
                    },
                    "lblAccountNumberInfo": {
                        "width": "12.50%"
                    },
                    "lblAccountNumberResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblAccountServiceInfo": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "lblAmountValueInfo": {
                        "width": "12.50%"
                    },
                    "lblAmountValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblCurrencyValueInfo": {
                        "width": "12.50%"
                    },
                    "lblCurrencyValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Change Interest"
                    },
                    "lblLoanTypeInfo": {
                        "width": "13.20%"
                    },
                    "lblLoanTypeResponse": {
                        "centerY": "50%",
                        "width": "13.60%"
                    },
                    "lblMaturityDateInfo": {
                        "width": "12.50%"
                    },
                    "lblMaturityDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblStartDateInfo": {
                        "width": "12.50%"
                    },
                    "lblStartDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "loanDetails": {
                        "height": "128dp",
                        "left": "18dp",
                        "top": "291dp",
                        "width": "98%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContainer.add(flxSeparatorLine1, flxLoanInterestChange, ErrorAllert, flxHeaderMenu, custInfo, MainTabs, loanDetails);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64dp",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageWithReferenceNumber = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMessageWithReferenceNumber",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BG",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgHeaderClose = new kony.ui.Image2({
                "height": "14dp",
                "id": "imgHeaderClose",
                "isVisible": true,
                "left": "1230dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "top": "17dp",
                "width": "14dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblMessageWithReferenceNumber, imgHeaderClose);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0ccb780a8f7c140"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0gc1da2577cf445"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_b6da1c9231af46438a5fb2acd9dacea7;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_bbd783e212c6498b83b01a8438674b10;
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "SegmentPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "height": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "TitleLabel": {
                    "centerX": "19%",
                    "height": "70dp",
                    "width": "500dp"
                },
                "TitleLabel.flxTitle": {
                    "left": "200dp"
                },
                "TitleLabel.lblTitle": {
                    "centerX": "",
                    "left": "220dp",
                    "text": "Change Interest"
                },
                "ErrorAllert.imgCloseBackup": {
                    "src": "ico_close.png"
                },
                "custInfo": {
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "MainTabs": {
                    "left": "0dp",
                    "top": "227dp",
                    "width": "98%"
                },
                "MainTabs.btnAccounts1": {
                    "left": 40
                },
                "MainTabs.flxContainerAccount": {
                    "left": "27dp"
                },
                "MainTabs.flxontainerLoans": {
                    "left": "245dp"
                },
                "loanDetails.flxListBoxSelectedValue": {
                    "left": "",
                    "minWidth": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.flxLoanDetailsvalue": {
                    "height": "63dp"
                },
                "loanDetails.imgListBoxSelectedDropDown": {
                    "left": "8dp",
                    "src": "drop_down_arrow.png"
                },
                "loanDetails.lblAccountNumberInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAccountNumberResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountServiceInfo": {
                    "left": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblListBoxSelectedValue": {
                    "text": "Change Interest"
                },
                "loanDetails.lblLoanTypeInfo": {
                    "width": "13.20%"
                },
                "loanDetails.lblLoanTypeResponse": {
                    "centerY": "50%",
                    "width": "13.60%"
                },
                "loanDetails.lblMaturityDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails": {
                    "height": "128dp",
                    "left": "18dp",
                    "top": "291dp",
                    "width": "98%"
                },
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0ccb780a8f7c140",
                    "skin2": "CopydefBtnNormal0gc1da2577cf445",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "SegmentPopup": {
                    "height": "100%"
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMainContainer, flxMessageInfo, uuxNavigationRail, SegmentPopup, ProgressIndicator, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerLoanInterestChange,
            "enabledForIdleTimeout": false,
            "id": "frmCustomerLoanInterestChange",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});