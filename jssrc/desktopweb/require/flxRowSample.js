define("flxRowSample", function() {
    return function(controller) {
        var flxRowSample = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRowSample",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxRowSample.setDefaultUnit(kony.flex.DP);
        var flxMore = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxMore",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxMore.setDefaultUnit(kony.flex.DP);
        var rtxAns = new kony.ui.RichText({
            "id": "rtxAns",
            "isVisible": true,
            "left": "6dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0jcab0da614c646",
            "text": "RichText",
            "top": "8dp",
            "width": "99%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMore.add(rtxAns);
        flxRowSample.add(flxMore);
        return flxRowSample;
    }
})