define("flxMoreHeader", function() {
    return function(controller) {
        var flxMoreHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "26dp",
            "id": "flxMoreHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_db7214693412436293d629e64223d100,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxMoreHeader.setDefaultUnit(kony.flex.DP);
        var lblQues = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblQues",
            "isVisible": true,
            "left": "5dp",
            "skin": "CopydefLabel0d14095a88a5f48",
            "text": "Label",
            "width": "93%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUp = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUp",
            "isVisible": true,
            "right": "3%",
            "skin": "CopydefLabel0dbd6417f92304a",
            "text": "J",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMoreHeader.add(lblQues, lblUp);
        return flxMoreHeader;
    }
})