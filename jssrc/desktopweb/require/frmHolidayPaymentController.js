define("userfrmHolidayPaymentController", {
    //Type your controller code here 
    _loanObj: {},
    arrangementId: "",
    dataPerPageNumber: 0,
    currentCurrency: "",
    currentLoanAccountId: "",
    currentOverride: [],
    currentOverrideOperation: "",
    paymentScheduledArray: [],
    countnumber: 0,
    revisedPaymentDataArray: [],
    labelGraphArray: [],
    dataPointArray1: [],
    selectedDateArray: [],
    selectedIndexArray: [],
    previousIndexDetails: 0,
    startDate: "",
    maturityDate: "",
    graphCountArrays: {},
    onNavigate: function(navObj) {
        this._loanObj = {};
        this._loanObj.customerId = navObj.customerId;
        this.arrangementId = Application.validation.isNullUndefinedObj(navObj.lblArrangementId);
        this.currentCurrency = Application.validation.isNullUndefinedObj(navObj.lblCCY);
        this.currentLoanAccountId = Application.validation.isNullUndefinedObj(navObj.lblHeaderAN);
        this.bindEvents();
        this.setLoanHeaderDetails(navObj);
        //this.getPaymentScheduleInformation(); // Changed service call serious
        this.getNextPaymentDateInformation();
    },
    bindEvents: function() {
        //this.view.flxBack.onTouchEnd = this.navigateCustomerAccountDetails;
        this.view.lblBreadBoardOverviewvalue.onTouchEnd = this.navigateCustomerAccountDetails;
        this.view.lblBreadBoardHomeValue.onTouchEnd = this.navigateHome;
        this.view.flxCancel.onTouchEnd = this.navigateCustomerAccountDetails;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup.btnProceed.onClick = this.getCurrentOverrideModule;
        this.view.postShow = this.postShowFuns;
        this.view.flxHolidayDateCalendar.onTouchEnd = this.openSkipPaymentWrapper;
        this.view.flxBillDetailsClose.onTouchEnd = this.openSkipPaymentWrapper;
        this.view.segSkipUpcomingPayment.onRowClick = this.segSkipPaymentOnRowClick;
        //this.view.flxSkipConfirm.onTouchEnd = this.onClickConfirmSkipPayment;
        this.view.flxSkipConfirm.onTouchEnd = this.onClickConfirmSkipPayment;
        this.view.flxSkipCancel.onTouchEnd = this.openSkipPaymentWrapper;
        this.view.flxRevisedPaymentScheduleButton.onTouchEnd = this.onClickRevisedaymentScheduleButton;
        this.view.RevisedPaymentSchedule.imgClose.onTouchEnd = this.closeRevisedaymentSchedulePopup;
        this.view.RevisedPaymentSchedule.btnPrev.onTouchEnd = this.setDataToRevisedPaymentScheduleSegment;
        this.view.RevisedPaymentSchedule.btnNext.onTouchEnd = this.setDataToRevisedPaymentScheduleSegment;
        //this.view.flxCancel.onTouchEnd = this.navigateCustomerAccountDetails;
        this.view.flxSubmit.onTouchEnd = this.postHolidayPayment;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.closeErrorAlertPoup;
    },
    navigateHome: function() {
        var navToHome = new kony.mvc.Navigation("frmDashboard");
        var navObjet = {};
        navObjet.clientId = "";
        navToHome.navigate(navObjet);
    },
    postShowFuns: function() {
        //added newly
        this.view.flxSubmit.setEnabled(false);
        this.currentOverrideOperation = "";
        this.currentOverride = [];
        this.graphCountArrays = {};
        this.view.flxRound1.isVisible = false;
        this.view.blTitlePersonal.isVisible = false;
        this.view.flxRound2.isVisible = false;
        this.view.blTitleMortgage.isVisible = false;
        this.view.SegmentPopup.isVisible = false;
        this.view.flxSkipUpcomingPaymentWrapper.isVisible = false;
        this.view.RevisedPaymentSchedule.isVisible = false;
        this.view.flxRevisedPaymentWrapper.isVisible = false;
        this.view.sldCurrentPayment.setEnabled(false);
        this.view.sldRevisedPayment.setEnabled(false);
        //this.view.cusDropDownRecalculate.value = "Payment Amount";
        //this.view.cusDropDownRecalculate.options = '[{"label":"Recalculate","value":"Payment Amount"}]';
        //this.view.cusDropDownRecalculate.value = "Recalculate";
        this.view.cusDropDownRecalculate.disabled = true;
        //dv//this.view.flxGraph.isVisible = false;
        //this.reloadGraph();
    },
    navigateCustomerAccountDetails: function() {
        var navToCusLoans = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navObjet.isPopupShow = "false";
        navToCusLoans.navigate(navObjet);
    },
    closeSegmentOverrite: function() {
        this.currentOverrideOperation = "";
        this.currentOverride = [];
        this.view.SegmentPopup.isVisible = false;
        this.view.forceLayout();
    },
    setLoanHeaderDetails: function(navObj) {
        try {
            var newObj = navObj;
            this._loanObj.currencyId = navObj.lblCCY;
            this._loanObj.arrangementId = navObj.lblArrangementId;
            this._loanObj.customerId = navObj.customerId;
            this._loanObj.produtId = navObj.loanProduct;
            //TODO : take from segment data a navigation object
            this.view.loanDetails.lblAccountNumberResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHeaderAN);
            this.view.loanDetails.lblLoanTypeResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHeaderAccountType); //"2345678";
            this.view.loanDetails.lblCurrencyValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblCCY);
            this.view.loanDetails.lblAmountValueResponse.text = Application.validation.isNullUndefinedObj(newObj.lblClearedBalance);
            this.view.loanDetails.lblStartDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblAvailableLimit);
            this.view.loanDetails.lblMaturityDateResponse.text = Application.validation.isNullUndefinedObj(newObj.lblHdr);
        } catch (err) {
            kony.print("Error in HolidayPaymentController setLoanHeaderDetails:::" + err);
        }
    },
    reloadGraph(chartTitle, labels, data1, data2) {
        var dataMax = Math.round(Math.max.apply(null, data1) / 100) * 100;
        this.view.multiline.lowValue = 0;
        this.view.multiline.highValue = dataMax;
        this.view.multiline.chartTitle = "" + chartTitle;
        this.view.multiline.enableGrid = true;
        this.view.multiline.enableLegends = false;
        this.view.multiline.xAxisTitle = "Date";
        this.view.multiline.yAxisTitle = "Amount";
        this.view.multiline.enableStaticPreview = true;
        this.view.multiline.legendFontSize = "95%";
        this.view.multiline.enableGridAnimation = true;
        this.view.multiline.titleFontSize = "12";
        this.view.multiline.titleFontColor = "#616161";
        this.view.multiline.legendFontColor = "#616161";
        //this.view.multiline.bgColor = "#CBE3F8";
        this.view.multiline.bgColor = "#ffffff";
        //this.view.multiline.bgColor = "#003e75";
        this.view.multiline.enableChartAnimation = false;
        var series = [];
        var colors = [];
        if (isNullorEmpty(data2)) {
            series = [{
                "name": "Original",
                "data": data1
            }];
            //colors = ["#1B9ED9"];
            colors = ["#FF9800"];
        } else {
            series = [{
                "name": "Original",
                "data": data1
            }, {
                "name": "Revised",
                "data": data2
            }];
            //colors = ["#1B9ED9", "#76C044"];
            colors = ["#FF9800", "#2C8631"];
            var dataMax2 = Math.round(Math.max.apply(null, data2) / 100) * 100;
            this.view.multiline.highValue = dataMax2;
        }
        kony.print("labelsarr---->" + JSON.stringify(labels));
        kony.print("series---->" + JSON.stringify(series));
        //    kony.print("data2---->"+JSON.stringify(data2));
        //this.view.multiline.createChart(dataSet, lineDetails);
        this.view.multiline.createChartRawData(labels, series, colors);
    },
    openSkipPaymentWrapper: function(eventObject) {
        var eventId = eventObject.id;
        if (eventId === "flxHolidayDateCalendar") {
            this.skipPaymentDataLoading(); // calling Data loading part and then making visible on.
            //this.view.flxSkipUpcomingPaymentWrapper.isVisible = true;
        } else if (eventId === "flxBillDetailsClose") {
            this.view.flxSkipUpcomingPaymentWrapper.isVisible = false;
        } else if (eventId === "flxSkipCancel") {
            this.view.flxSkipUpcomingPaymentWrapper.isVisible = false;
        }
    },
    showPaginationSegmentData: function(dataPerPage, arrayOfData, btnName) {
        kony.print("showPaginationSegmentData===>");
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.flxPagination.isVisible = true;
            } else {
                this.view.RevisedPaymentSchedule.flxPagination.isVisible = false;
            }
            if (btnName == "btnPrev" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName == "btnNext" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.RevisedPaymentSchedule.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnFocus";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;            
            } else if (this.gblSegPageCount == 1 && this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;           
            } else if (this.gblSegPageCount == 1 && this.totalpage == 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.lblSearchPage.text = "";
            } else if (this.gblSegPageCount == this.totalpage && this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnFocus";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                //this.view.lblSearchPage.isVisible = true;            
            } else {
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.lblSearchPage.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in HolidayPaymentController next previous method :::" + err);
        }
    },
    getPaymentScheduleInformation: function(date) {
        try {
            this.currentOverrideOperation = "";
            this.currentOverride = [];
            this.graphCountArrays = {};
            this.view.ErrorAllert.isVisible = false;
            this.view.ProgressIndicator.isVisible = true;
            this.view.lblPaymentOptedResponse.text = "0";
            this.paymentScheduledArray = [];
            var serviceName = "LendingNew";
            var operationName = "getPaymentSchedule";
            var headers = {};
            var inputParams = {};
            inputParams.loanAccountId = this.currentLoanAccountId;
            inputParams.arrangementId = this.arrangementId;
            //inputParams.dateFrom = transactDate;
            inputParams.dateFrom = this.formatDate(date);
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBGetPaymentScheduleInformation, this.failureCBGetPaymentScheduleInformation);
        } catch (err) {
            kony.print("Error in HolidayPaymentController getPaymentScheduleInformation :::" + err);
        }
    },
    successCBGetPaymentScheduleInformation: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success Response successCBGetPaymentScheduleInformation---->" + JSON.stringify(res));
            var responseBody = res.body;
            //TODO : not sure why do we need next payment date information
            // the get payment schedule should have the information already ?
            //We can orchestrate the service on fabric?
            //this.getNextPaymentDateInformation(); // calling next service for getting Data related info
            if (responseBody.length > 0) {
                //setting original pay schedule to controller level
                this.paymentScheduledArray = responseBody;
                var totalDueAmount = responseBody[0].totalDue; //commented this line - need to check mapping is correct
                //var totalDueAmount = responseBody[0].principal;
                if (totalDueAmount === 0 || totalDueAmount === "0.00") {
                    totalDueAmount = responseBody[1].totalDue;
                }
                if (Application.validation.isNullUndefinedObj(totalDueAmount) !== "") {
                    totalDueAmount = totalDueAmount.replace(/,/g, '');
                    totalDueAmount = Application.numberFormater.convertForDispaly(totalDueAmount, this.currentCurrency);
                }
                this.view.lblPaymentAmountResponse.text = this.currentCurrency + " " + totalDueAmount;
                this.getFirstDataForGraph();
                //this.view.lblNextPaymentDateResponse.text = responseBody[0].date;
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController successCBGetPaymentScheduleInformation :::" + err);
        }
    },
    failureCBGetPaymentScheduleInformation: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Failure Response failureCBGetPaymentScheduleInformation---->" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in HolidayPaymentController failureCBGetPaymentScheduleInformation :::" + err);
        }
    },
    skipPaymentDataLoading: function() {
        try {
            this.selectedDateArray = [];
            this.selectedIndexArray = [];
            this.previousIndexDetails;
            var dataToLoad = [];
            dataToLoad = this.paymentScheduledArray;
            var combData = [];
            if (dataToLoad.length > 0) {
                if (dataToLoad.length < 7) {
                    for (var a in dataToLoad) {
                        var json1 = {
                            "lblAmount1": dataToLoad[a].totalDue,
                            "lblDate1": dataToLoad[a].date,
                            "lblArrow": {
                                skin: "sknlblArrowGrey"
                            },
                        };
                        combData.push(json1);
                    }
                } else {
                    for (var b = 0; b < 6; b++) {
                        var json = {
                            "lblAmount1": dataToLoad[b].totalDue,
                            "lblDate1": dataToLoad[b].date,
                            "lblArrow": {
                                skin: "sknlblArrowGrey"
                            },
                        };
                        combData.push(json);
                    }
                }
                kony.print("combData---->" + JSON.stringify(combData));
                this.view.segSkipUpcomingPayment.setData(combData);
                this.view.flxSkipUpcomingPaymentWrapper.isVisible = true;
                this.view.flxSkipUpcomingPaymentWrapper.forceLayout();
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController skipPaymentDataLoading :::" + err);
        }
    },
    getFirstDataForGraph: function() {
        var labelsarr = [];
        this.labelGraphArray = [];
        this.dataPointArray1 = [];
        var datapointsarr = [];
        var body = this.paymentScheduledArray;
        var responseDataLength = body.length;
        this.view.flxRound1.isVisible = true;
        this.view.blTitlePersonal.isVisible = true;
        this.view.flxRound2.isVisible = false;
        this.view.blTitleMortgage.isVisible = false;
        /*
    if(responseDataLength < 13) {
      for(var i =0; i < body.length ; i++){
        labelsarr.push(body[i].date);
        datapointsarr.push(Number(body[i].totalDue.replace(/,/g, '')));
      }
    } else {
      for(var j =0; j < 12 ; j++){
        labelsarr.push(body[j].date);
        datapointsarr.push(Number(body[j].totalDue.replace(/,/g, '')));
      }
    }
  */
        //take the first 12 records
        for (var j = 0; j < 12 && j < responseDataLength; j++) {
            labelsarr.push(body[j].date);
            datapointsarr.push(Number(body[j].totalDue.replace(/,/g, '')));
        }
        this.labelGraphArray = labelsarr;
        this.dataPointArray1 = datapointsarr;
        var chartTitle = "Current Repayment";
        this.reloadGraph(chartTitle, labelsarr, datapointsarr, null);
        var series1Data = {
            label: " Current Payment",
            backgroundColor: "#DB666E", //"#FF9800",//"rgba(40, 2, 207, 0.1)",
            //pointBackgroundColor : "#FF9800",
            pointBorderColor: "#ffffff",
            borderColor: "#DB666E",
            pointRadius: 4,
            pointBackgroundColor: "#DB666E", //"#6052c9",
            data: datapointsarr
        };
        this.graphCountArrays = series1Data;
        this.newGraphFunctionality(series1Data, {});
        //DV//this.view.flxGraph.isVisible = true;
        //this.view.flxGraph.forceLayout();
    },
    segSkipPaymentOnRowClick: function() {
        try {
            var data = this.view.segSkipUpcomingPayment.selectedRowItems;
            var index = this.view.segSkipUpcomingPayment.selectedRowIndex[1];
            if (data[0].lblArrow.skin == "sknlblArrowGrey") {
                data[0].lblArrow.skin = "sknlblArrowGreen";
                data[0].lblArrow.text = "Z";
            } else {
                data[0].lblArrow.skin = "sknlblArrowGrey";
            }
            kony.print("data[0]====>" + JSON.stringify(data[0]));
            this.view.segSkipUpcomingPayment.setDataAt(data[0], index);
        } catch (err) {
            kony.print("Error in HolidayPaymentController segSkipPaymentOnRowClick :::" + err);
        }
    },
    getNextPaymentDateInformation: function() {
        try {
            this.view.ProgressIndicator.isVisible = true;
            //this.paymentScheduledArray = [];
            var serviceName = "LendingNew";
            var operationName = "getNextPaymentDate";
            var headers = {};
            var inputParams = {};
            inputParams.loanAccountId = this.currentLoanAccountId;
            inputParams.arrangementId = this.arrangementId;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBGetNextPaymentDateInformation, this.failureCBGetNextPaymentDateInformation);
        } catch (err) {
            kony.print("Error in HolidayPaymentController getNextPaymentDateInformation :::" + err);
        }
    },
    successCBGetNextPaymentDateInformation: function(res) {
        try {
            this.startDate = "";
            this.maturityDate = "";
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success Response successCBGetNextPaymentDateInformation---->" + JSON.stringify(res));
            var responseBody = res.body;
            if (responseBody.length > 0) {
                this.view.lblNextPaymentDateResponse.text = responseBody[0].nextPaymentDate;
                this.view.lblMaturityDateResponse.text = responseBody[0].maturityDate;
                this.view.lblStartDateOverCalendar.text = responseBody[0].nextPaymentDate;
                // additional line added - In Term wise no changes as of now
                this.view.lblRevisedMaturityDateResponse.text = responseBody[0].maturityDate;
                //added new service call sequence
                if (Application.validation.isNullUndefinedObj(responseBody[0].nextPaymentDate) !== "") {
                    this.getPaymentScheduleInformation(responseBody[0].nextPaymentDate);
                }
                //newly add block for Dynamic Payment Indicator 
                this.startDate = responseBody[0].startDate;
                this.maturityDate = responseBody[0].maturityDate;
                var differentDate = this.differentCalculatorMonth(responseBody[0].maturityDate, responseBody[0].startDate);
                kony.print("differentDate====>" + differentDate);
                var currentPaymentDifferent;
                var finalProgressValue;
                var finalAfterHundred;
                if (differentDate < 13) {
                    currentPaymentDifferent = this.differentCalculatorMonth(responseBody[0].nextPaymentDate, responseBody[0].startDate);
                    kony.print("currentPaymentDifferent====>" + currentPaymentDifferent);
                } else {
                    differentDate = differentDate / 12;
                    differentDate = parseInt(differentDate);
                    kony.print("differentDate====>" + differentDate);
                    currentPaymentDifferent = this.differentCalculatorYear(responseBody[0].nextPaymentDate, responseBody[0].startDate);
                    kony.print("finalAftecurrentPaymentDifferentrHundred====>" + currentPaymentDifferent);
                    if (currentPaymentDifferent === 0) {
                        currentPaymentDifferent = 1;
                    }
                }
                finalProgressValue = parseInt(currentPaymentDifferent) / parseInt(differentDate);
                //this.view.sldCurrentPayment.selectedValue = currentPaymentDifferent;
                finalAfterHundred = finalProgressValue * 100;
                finalAfterHundred = parseInt(finalAfterHundred);
                kony.print("finalAfterHundred====>" + finalAfterHundred);
                if (finalAfterHundred === 0) {
                    finalAfterHundred = 1;
                }
                this.view.sldCurrentPayment.selectedValue = finalAfterHundred;
                //this.view.cusLinearProgressIndicatorPaymentAmount.progress = finalProgressValue;
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController successCBGetNextPaymentDateInformation :::" + err);
        }
    },
    failureCBGetNextPaymentDateInformation: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Failure Response failureCBGetNextPaymentDateInformation---->" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in HolidayPaymentController failureCBGetNextPaymentDateInformation :::" + err);
        }
    },
    onClickConfirmSkipPayment: function() {
        try {
            this.countnumber = 0;
            this.selectedDateArray = [];
            //this.selectedIndexArray = [];
            var data = this.view.segSkipUpcomingPayment.data;
            var count = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].lblArrow.skin == "sknlblArrowGreen") {
                    count = count + 1;
                    this.selectedDateArray.push(data[i].lblDate1);
                    //this.selectedIndexArray.push(i);
                }
            }
            this.countnumber = count;
            this.view.lblPaymentOptedResponse.text = "" + count;
            this.view.flxSkipUpcomingPaymentWrapper.isVisible = false;
            kony.print("this.selectedDateArray---->" + JSON.stringify(this.selectedDateArray));
            //kony.print("this.selectedIndexArray---->"+JSON.stringify(this.selectedIndexArray));
            this.getRevisedPaymentDetails(this.countnumber, this.arrangementId);
        } catch (err) {
            kony.print("Error in HolidayPaymentController onClickConfirmSkipPayment :::" + err);
        }
    },
    checkForIncrementNumber: function(arrayIndex) {
        try {
            if (arrayIndex.length > 0) {
                for (var a in arrayIndex) {
                    if (parseInt(arrayIndex[a]) > parseInt(arrayIndex[a + 1])) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController checkForIncrementNumber :::" + err);
        }
    },
    formatDate: function(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return year + "" + month + "" + day;
    },
    getRevisedPaymentDetails: function(count, arrangementId) {
        try {
            this.view.flxSubmit.setEnabled(true);
            this.view.SegmentPopup.isVisible = false;
            //this.closeSegmentOverrite();
            this.view.ErrorAllert.isVisible = false;
            this.view.flxHeaderMenu.isVisible = true;
            this.dataPerPageNumber = 6;
            this.view.ProgressIndicator.isVisible = true;
            this.revisedPaymentDataArray = [];
            var serviceName = "LendingNew";
            var operationName = "getSimPayHolidaySchedule";
            var headers = {};
            var inputParams = {};
            var fromDate = this.formatDate(this.selectedDateArray[0]);
            inputParams.fromDate = fromDate;
            inputParams.arrangementId = this.arrangementId; //this.countnumber,this.arrangementId
            inputParams.term = this.countnumber;
            inputParams.currencyId = this.currentCurrency;
            inputParams.customerId = this._loanObj.customerId;
            inputParams.productId = this._loanObj.produtId;
            inputParams.t24Today = this.formatDate(transactDate);
            inputParams.overrideDetails = this.currentOverride;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBGetRevisedPaymentDetails, this.failureCBGetRevisedPaymentDetails);
        } catch (err) {
            kony.print("Error in HolidayPaymentController getRevisedPaymentDetails :::" + err);
        }
    },
    successCBGetRevisedPaymentDetails: function(res) {
        try {
            this.currentOverrideOperation = "";
            this.currentOverride = [];
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success Response successCBGetRevisedPaymentDetails---->" + JSON.stringify(res));
            var responseBody = res.schedules;
            var revisedPaymentData = [];
            var responseLength = responseBody.length;
            if (responseBody.length > 0) {
                for (var a in responseBody) {
                    var json = {
                        "lblPaymentDate": "" + Application.validation.isNullUndefinedObj(responseBody[a].paymentDueDate),
                        "lblAmount": "" + Application.validation.isNullUndefinedObj(responseBody[a].totalPayment),
                        "lblPrincipal": "" + Application.validation.isNullUndefinedObj(responseBody[a].propertyAmount),
                        "lblInterest": "" + Application.validation.isNullUndefinedObj(responseBody[a].prinipalInterest),
                        "lblTax": "0.00",
                        "lbltotaloutstanding": "" + Application.validation.isNullUndefinedObj(responseBody[a].outstandingAmount),
                    };
                    revisedPaymentData.push(json);
                }
                //TODO//DV-take the first time -which is the next payment term amounts from todayint24
                var lastItem = parseInt(responseLength) - 1;
                //this.view.lblRevisedPaymentAmountResponse.text = this.currentCurrency+" "+responseBody[lastItem].totalPayment;//totalPayment
                //this.view.lblRevisedPaymentAmountResponse.text = this.currentCurrency+" "+responseBody[0].totalPayment;//totalPayment - changed
                var revisedPaymentDate = Application.validation.isNullUndefinedObj(responseBody[0].paymentDueDate);
                if (revisedPaymentDate !== "") {
                    this.view.lblNextRevisedPaymentDateResponse.text = revisedPaymentDate.toUpperCase();
                } else {
                    this.view.lblNextRevisedPaymentDateResponse.text = Application.validation.isNullUndefinedObj(responseBody[0].paymentDueDate);
                }
                if (Application.validation.isNullUndefinedObj(responseBody[0].totalPayment) !== "") {
                    var responseData1 = parseInt(responseBody[0].totalPayment);
                    if (Math.sign(responseData1) === -1) {
                        var totalAmount = responseBody[1].totalPayment;
                        totalAmount = totalAmount.replace(/,/g, '');
                        totalAmount = Application.numberFormater.convertForDispaly(totalAmount, this.currentCurrency);
                        this.view.lblRevisedPaymentAmountResponse.text = this.currentCurrency + " " + totalAmount;
                        this.view.lblNextRevisedPaymentDateResponse.text = responseBody[1].paymentDueDate.toUpperCase();
                    } else {
                        var totalAmount1 = responseBody[0].totalPayment;
                        totalAmount1 = totalAmount1.replace(/,/g, '');
                        totalAmount1 = Application.numberFormater.convertForDispaly(totalAmount1, this.currentCurrency);
                        this.view.lblRevisedPaymentAmountResponse.text = this.currentCurrency + " " + totalAmount1;
                    }
                } else {
                    var totalAmount2 = responseBody[1].totalPayment;
                    totalAmount2 = totalAmount2.replace(/,/g, '');
                    totalAmount2 = Application.numberFormater.convertForDispaly(totalAmount2, this.currentCurrency);
                    this.view.lblRevisedPaymentAmountResponse.text = this.currentCurrency + " " + totalAmount2;
                    this.view.lblNextRevisedPaymentDateResponse.text = responseBody[1].paymentDueDate.toUpperCase();
                }
                kony.print("revisedPaymentData======>" + JSON.stringify(revisedPaymentData));
                kony.print("responseBody[0].paymentDueDate======>" + JSON.stringify(responseBody[0].paymentDueDate.toUpperCase()));
                //set data to revised popup here itself?
                this.revisedPaymentDataArray = revisedPaymentData;
                //TODO: do not use this function to show the visibility/on/off
                this.view.flxRevisedPaymentWrapper.isVisible = true;
                //this.getRevisedDataForGraph();
                this.getRevisedDataForGraphUpdated();
                //TODO : call to update the graph here ?
                //TO update the graph with second line, we need to 
                //write lookup logic
                //newly add block for Dynamic Payment Indicator 
                var currentPaymentDifferent;
                var finalProgressValue;
                var differentDate = this.differentCalculatorMonth(this.maturityDate, this.startDate);
                kony.print("differentDate====>" + differentDate);
                if (differentDate < 13) {
                    currentPaymentDifferent = this.differentCalculatorMonth(revisedPaymentDate, this.startDate);
                    if (parseInt(currentPaymentDifferent) === 0) {
                        currentPaymentDifferent = 1;
                    }
                    kony.print("currentPaymentDifferent====>" + currentPaymentDifferent);
                    //kony.print("finalProgressValue====>"+finalProgressValue);
                } else {
                    differentDate = differentDate / 12;
                    differentDate = parseInt(differentDate);
                    kony.print("differentDate====>" + differentDate);
                    currentPaymentDifferent = this.differentCalculatorYear(revisedPaymentDate, this.startDate);
                    kony.print("currentPaymentDifferent====>" + currentPaymentDifferent);
                    //finalProgressValue = parseInt(currentPaymentDifferent) / parseInt(differentDate);
                    // kony.print("finalProgressValue====>"+finalProgressValue);
                    if (parseInt(currentPaymentDifferent) === 0) {
                        currentPaymentDifferent = 1;
                    }
                }
                finalProgressValue = parseInt(currentPaymentDifferent) / parseInt(differentDate);
                kony.print("finalProgressValue====>" + finalProgressValue);
                //this.view.cusLinearProgressIndicatorRevisePaymentAmount.progress = finalProgressValue;
                var finalAfterHundred = finalProgressValue * 100;
                finalAfterHundred = parseInt(finalAfterHundred);
                kony.print("finalAfterHundred====>" + finalAfterHundred);
                this.view.sldRevisedPayment.selectedValue = finalAfterHundred;
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController successCBGetRevisedPaymentDetails :::" + err);
        }
    },
    getRevisedDataForGraph: function() {
        try {
            //var labelsarr = []; 
            var datapointsarr2 = [];
            var value_datapointsarr2 = [];
            var body = this.revisedPaymentDataArray;
            var responseDataLength = body.length;
            //var jsonRevisedPayment = {};
            //take the first 12 records
            for (var j = 0; j < 12 && j < responseDataLength; j++) {
                var paymentdata = body[j].lblPaymentDate
                var json = {
                    "lblPaymentDate": paymentdata.toUpperCase(),
                    "lblAmount": Number(body[j].lblAmount.replace(/,/g, ''))
                };
                datapointsarr2.push(json);
                //labelsarr.push(body[j].date);
                //datapointsarr.push(Number(body[j].totalDue.replace(/,/g, '')));
                // jsonRevisedPayment[body[j].lblPaymentDate] = Number(body[j].lblAmount.replace(/,/g, ''));
            }
            kony.print("datapointsarr2====>" + JSON.stringify(datapointsarr2));
            kony.print("labelGraphArray====>" + JSON.stringify(this.labelGraphArray));
            for (var a in this.labelGraphArray) {
                for (var b in datapointsarr2) {
                    if (datapointsarr2[b].lblPaymentDate === this.labelGraphArray[a]) {
                        value_datapointsarr2.push(datapointsarr2[b].lblAmount);
                    }
                }
                value_datapointsarr2.push(0);
            }
            kony.print("value_datapointsarr2====>" + JSON.stringify(value_datapointsarr2));
            /**for(var k in this.labelGraphArray){
      var data = ''+this.labelGraphArray[k];
      kony.print(data);
      //if(jsonRevisedPayment['key'].hasOwnProperty(data)) {
        if(data in jsonRevisedPayment) {
        datapointsarr2.push(jsonRevisedPayment[data]);
      } else {
        datapointsarr2.push(0);
      }
    }**/
            this.reloadGraph(this.labelGraphArray, this.dataPointArray1, value_datapointsarr2);
            //this.newGraphFunctionality(this.dataPointArray1, value_datapointsarr2);
            //DV//this.view.flxGraph.isVisible = true;
            //this.view.flxGraph.forceLayout();
        } catch (err) {
            kony.print("Error in HolidayPaymentController getRevisedDataForGraph :::" + err);
        }
    },
    getRevisedDataForGraphUpdated: function() {
        try {
            var datapointsarr2 = [];
            this.view.flxRound1.isVisible = true;
            this.view.blTitlePersonal.isVisible = true;
            this.view.flxRound2.isVisible = true;
            this.view.blTitleMortgage.isVisible = true;
            var body = this.revisedPaymentDataArray;
            var responseDataLength = body.length;
            var jsonRevisedPayment = {};
            //take the first 12 records
            for (var j = 0; j < 12 && j < responseDataLength; j++) {
                var paymentdata = body[j].lblPaymentDate
                jsonRevisedPayment[paymentdata.toUpperCase()] = Number(body[j].lblAmount.replace(/,/g, ''));
            }
            kony.print("jsonRevisedPayment====>" + JSON.stringify(jsonRevisedPayment));
            kony.print("labelGraphArray====>" + JSON.stringify(this.labelGraphArray));
            for (var k in this.labelGraphArray) {
                var data = this.labelGraphArray[k];
                kony.print(data);
                if (jsonRevisedPayment.hasOwnProperty(data)) {
                    //if(data in jsonRevisedPayment) {
                    datapointsarr2.push(jsonRevisedPayment[data]);
                } else {
                    datapointsarr2.push(0);
                }
            }
            kony.print("datapointsarr2====>" + JSON.stringify(datapointsarr2));
            var chartTitle = "Revised Repayment";
            this.reloadGraph(chartTitle, this.labelGraphArray, this.dataPointArray1, datapointsarr2);
            var series2Data = {
                label: " Revised Payment",
                backgroundColor: "#5831D8", //"#2C8631",//"rgba(96, 82, 201, 0.1)",
                //pointBackgroundColor : "#2C8631",
                pointBorderColor: "#ffffff",
                borderColor: "#5831D8",
                pointRadius: 4,
                pointBackgroundColor: "#5831D8", //"#6052c9",
                data: datapointsarr2
            };
            this.newGraphFunctionality(this.graphCountArrays, series2Data);
        } catch (err) {
            kony.print("Error in HolidayPaymentController getRevisedDataForGraphUpdated :::" + err);
        }
    },
    failureCBGetRevisedPaymentDetails: function(res) {
        try {
            this.currentOverrideOperation = "";
            this.currentOverride = [];
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Service Failure:::" + JSON.stringify(res));
            var errorData = Application.validation.isNullUndefinedObj(res.error);
            var genericError = Application.validation.isNullUndefinedObj(res.errmsg);
            if (genericError !== "") {
                this.view.ErrorAllert.lblMessage.text = "Unable to process, please try again";
            }
            if (errorData !== "") {
                this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message;
            }
            //kony.print("Service Failure:::"+JSON.stringify(res.error.errorDetails));
            this.view.ErrorAllert.isVisible = true;
            this.view.flxHeaderMenu.isVisible = false;
            if (res.override !== "" && res.override !== undefined) {
                var overwriteDetails = [];
                var overWriteData = [];
                var segDataForOverrite = [];
                this.currentOverride = res.override.overrideDetails;
                overwriteDetails = res.override.overrideDetails;
                if (overwriteDetails.length > 0) {
                    for (var a in overwriteDetails) {
                        overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                        var json = {
                            "lblSerial": "*",
                            "lblInfo": overwriteDetails[a].description
                        }
                        segDataForOverrite.push(json);
                    }
                    this.view.SegmentPopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.currentOverrideOperation = "getSimulation";
                    this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
                    this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
                    this.view.SegmentPopup.isVisible = true;
                    this.view.ErrorAllert.isVisible = false;
                    this.view.flxHeaderMenu.isVisible = true;
                    kony.print("overWriteData::::" + JSON.stringify(overWriteData));
                    kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
                    this.view.forceLayout();
                }
            }
            kony.print("Failure Response failureCBGetRevisedPaymentDetails---->" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in HolidayPaymentController failureCBGetRevisedPaymentDetails :::" + err);
        }
    },
    setDataToRevisedPaymentScheduleSegment: function(buttonevent) {
        try {
            if (this.revisedPaymentDataArray.length > 0) {
                var segmentData = this.showPaginationSegmentData(this.dataPerPageNumber, this.revisedPaymentDataArray, buttonevent.id);
                kony.print("segDataSet ====>>" + JSON.stringify(segmentData));
                var widgetMap = {
                    "lblPaymentDate": "lblPaymentDate",
                    "lblAmount": "lblAmount",
                    "lblPrincipal": "lblPrincipal",
                    "lblInterest": "lblInterest",
                    "lblTax": "lblTax",
                    "lbltotaloutstanding": "lbltotaloutstanding",
                };
                //this.view.RevisedPaymentSchedule.segRevised.rowTemplate = "flxOutstandingBillDetails";
                this.view.RevisedPaymentSchedule.segRevised.widgetDataMap = widgetMap;
                this.view.RevisedPaymentSchedule.segRevised.setData(segmentData);
                this.view.RevisedPaymentSchedule.isVisible = true;
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController setDataToRevisedPaymentScheduleSegment :::" + err);
        }
    },
    onClickRevisedaymentScheduleButton: function() {
        try {
            this.setDataToRevisedPaymentScheduleSegment("");
        } catch (err) {
            kony.print("Error in HolidayPaymentController onClickRevisedaymentScheduleButton :::" + err);
        }
    },
    closeRevisedaymentSchedulePopup: function() {
        this.view.RevisedPaymentSchedule.isVisible = false;
    },
    closeErrorAlertPoup: function() {
        this.view.ErrorAllert.isVisible = false;
        this.view.flxHeaderMenu.isVisible = true;
    },
    postHolidayPayment: function() {
        try {
            this.view.ErrorAllert.isVisible = false;
            this.view.flxHeaderMenu.isVisible = true;
            this.view.SegmentPopup.isVisible = false;
            //this.closeSegmentOverrite();
            this.view.ProgressIndicator.isVisible = true;
            var serviceName = "LendingNew";
            var operationName = "confirmPaymentSchedule";
            var headers = {};
            var inputParams = {};
            var fromDate = this.formatDate(transactDate);
            inputParams.fromDate = fromDate;
            inputParams.arrangementId = this.arrangementId;
            inputParams.term = this.countnumber;
            inputParams.currencyId = this.currentCurrency;
            inputParams.customerId = this._loanObj.customerId;
            inputParams.productId = this._loanObj.produtId;
            inputParams.overrideDetails = this.currentOverride;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBPostHolidayPayment, this.failureCBPostHolidayPayment);
        } catch (err) {
            kony.print("Error in HolidayPaymentController postHolidayPayment :::" + err);
        }
    },
    successCBPostHolidayPayment: function(res) {
        try {
            //Application.loader.dismissLoader();
            this.currentOverrideOperation = "";
            this.currentOverride = [];
            this.view.ProgressIndicator.isVisible = false;
            this.view.lblPaymentOptedResponse.text = "";
            kony.print("Success CallBack :::" + JSON.stringify(res));
            if (res === "" || res === null || res === undefined) {
                kony.print("Response is null or undefined");
            } else {
                var resId = res.header;
                kony.print("RES___ID***" + JSON.stringify(resId));
                var data_id = Application.validation.isNullUndefinedObj(resId.aaaId);
                //this.view.lblMessageWithReferenceNumber.text = "Interest Change applied successfully!      Reference : Interest Change applied successfully!      Reference : "+data_id;
                var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
                var navObjet = {};
                navObjet.customerId = this._loanObj.customerId;
                navObjet.isShowLoans = "true";
                navObjet.isPopupShow = "true";
                navObjet.messageInfo = "Payment Holiday completed successfully!   Reference : " + data_id;
                navToChangeInterest1.navigate(navObjet);
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController successCBPostHolidayPayment:::" + err);
        }
    },
    failureCBPostHolidayPayment: function(res) {
        try {
            this.currentOverride = [];
            this.currentOverrideOperation = "";
            //Application.loader.dismissLoader();
            this.view.ProgressIndicator.isVisible = false;
            //alert("Service failure");
            kony.print("Inside ErrorDetails=====>" + JSON.stringify(res));
            if (res.override !== "" && res.override !== undefined) {
                var overwriteDetails = [];
                var overWriteData = [];
                var segDataForOverrite = [];
                this.currentOverride = res.override.overrideDetails;
                overwriteDetails = res.override.overrideDetails;
                if (overwriteDetails.length > 0) {
                    for (var a in overwriteDetails) {
                        overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                        var json = {
                            "lblSerial": "*",
                            "lblInfo": overwriteDetails[a].description
                        }
                        segDataForOverrite.push(json);
                    }
                    this.view.SegmentPopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.currentOverrideOperation = "confirmPaymentHoliday";
                    this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
                    this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
                    this.view.SegmentPopup.isVisible = true;
                    kony.print("overWriteData::::" + JSON.stringify(overWriteData));
                    kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
                    this.view.forceLayout();
                }
            }
            if (res.error !== []) { // !== "" || res.error.errorDetails !== null || res.error.errorDetails !== undefined || res.error.errorDetails !== []) {
                kony.print("Inside ErrorDetails=====>");
                this.view.ProgressIndicator.isVisible = false;
                if (res.error.errorDetails[0] === "" || res.error.errorDetails[0] === [] || res.error.errorDetails[0] === undefined || res.error.errorDetails[0] === null) {
                    kony.print("Inside ErrorDetails Empty=====>");
                } else {
                    this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message; //error.errorDetails[0].message
                    this.view.ErrorAllert.isVisible = true;
                    kony.print("Service Failure:::" + JSON.stringify(res.error.errorDetails));
                    this.view.flxHeaderMenu.isVisible = false;
                    //this.view.flxMessageInfo.isVisible = false;
                    this.view.forceLayout();
                }
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController failureCBPostHolidayPayment:::" + err);
        }
    },
    getCurrentOverrideModule: function() {
        try {
            if (this.currentOverrideOperation === "confirmPaymentHoliday") {
                this.postHolidayPayment();
            } else if (this.currentOverrideOperation === "getSimulation") {
                this.getRevisedPaymentDetails();
            }
        } catch (err) {
            kony.print("Error in HolidayPaymentController getCurrentOverrideModule:::" + err);
        }
    },
    differentCalculatorMonth: function(dt2, dt1) {
        try {
            var past_date = new Date(dt1);
            var future_date = new Date(dt2);
            var difference = (future_date.getFullYear() * 12 + future_date.getMonth()) - (past_date.getFullYear() * 12 + past_date.getMonth());
            return difference;
        } catch (err) {
            kony.print("Error in HolidayPaymentController differentCalculatorMonth:::" + err);
        }
    },
    differentCalculatorYear: function(dt2, dt1) {
        try {
            var past_date = new Date(dt1);
            past_date = past_date.getFullYear();
            var future_date = new Date(dt2);
            future_date = future_date.getFullYear();
            var diff = future_date - past_date;
            return diff;
        } catch (err) {
            kony.print("Error in HolidayPaymentController differentCalculatorYear:::" + err);
        }
    },
    newGraphFunctionality: function(series1Data, series2Data) {
        try {
            var graphArray = [];
            /**var series1Data = {label:"Personal Loans" , 
                       backgroundColor: "rgba(40, 2, 207, 0.1)",
                       pointBackgroundColor : "#FF9800",
                       pointBorderColor: "#ffffff",
                       data:this.graphDataPersonalWeek};
    var series2Data = {label:"Mortgage Loans" , 
                       backgroundColor: "rgba(96, 82, 201, 0.1)",
                       pointBackgroundColor : "#2C8631",
                       pointBorderColor: "#ffffff",
                       data:this.graphDataMortgagesWeek};**/
            graphArray.push(series1Data);
            if (Object.keys(series2Data).length > 0) {
                graphArray.push(series2Data);
            }
            //this.view.holidayLineGraph.isVisible = true; 
            this.view.holidayLineGraph.datasetsArray = JSON.stringify(graphArray);
            this.view.holidayLineGraph.xlabelsArray = JSON.stringify(this.labelGraphArray);
            this.view.holidayLineGraph.updateGraph();
        } catch (err) {
            kony.print("Error in HolidayPaymentController newGraphFunctionality:::" + err);
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    }
});
define("frmHolidayPaymentControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_dfaf8836f2794372be8ca22cf10d3a61: function AS_UWI_dfaf8836f2794372be8ca22cf10d3a61(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_gfcea63cafcf45a2b255c3654117fc85: function AS_UWI_gfcea63cafcf45a2b255c3654117fc85(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmHolidayPaymentController", ["userfrmHolidayPaymentController", "frmHolidayPaymentControllerActions"], function() {
    var controller = require("userfrmHolidayPaymentController");
    var controllerActions = ["frmHolidayPaymentControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
