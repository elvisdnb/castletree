define("loanNavigate", function() {
    return function(controller) {
        function addWidgetsloanNavigate() {
            this.setDefaultUnit(kony.flex.DP);
            var DPWSelectLoan = new kony.ui.CustomWidget({
                "id": "DPWSelectLoan",
                "isVisible": true,
                "left": "5%",
                "top": "0",
                "width": "300dp",
                "height": "50dp",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "disabled": false,
                "labelText": "Loan Type",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var btnTestGraph = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnTestGraph",
                "isVisible": true,
                "left": "5%",
                "skin": "defBtnNormal",
                "text": "Test Graph",
                "top": "111dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btntestGraph2 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btntestGraph2",
                "isVisible": true,
                "left": "19.76%",
                "skin": "defBtnNormal",
                "text": "udpate graph2",
                "top": "180dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnGetValue = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50dp",
                "id": "btnGetValue",
                "isVisible": true,
                "left": "35.87%",
                "skin": "defBtnNormal",
                "text": "Get UUX Value",
                "top": "111dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxgraphNew = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "30.75%",
                "clipBounds": true,
                "height": "150dp",
                "id": "flxgraphNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50.00%",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxgraphNew.setDefaultUnit(kony.flex.DP);
            var barGraph1 = new com.temenos.chartjslib.barGraph1({
                "height": "100%",
                "id": "barGraph1",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "barGraph1": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWbargraph1": {
                        "canvasHeight": "50",
                        "canvasWidth": "100"
                    }
                }
            });
            flxgraphNew.add(barGraph1);
            var fullGraphFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "59.30%",
                "clipBounds": true,
                "height": "150dp",
                "id": "fullGraphFlex",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50.00%",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            fullGraphFlex.setDefaultUnit(kony.flex.DP);
            var fullGraph1 = new com.temenos.chartjslib.barGraph1({
                "height": "100%",
                "id": "fullGraph1",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "barGraph1": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWbargraph1": {
                        "canvasHeight": "50",
                        "canvasWidth": "100"
                    }
                }
            });
            fullGraphFlex.add(fullGraph1);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0e73c12f8313148"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0i6b28f45587a47"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_e20af3cddd0b4c9ca1e2fe1437b0dde7;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_d596066755f845b9b391133a3c31725b;
            var UUXReccurencePicker = new kony.ui.CustomWidget({
                "id": "UUXReccurencePicker",
                "isVisible": true,
                "left": "320dp",
                "top": "320dp",
                "width": "400dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRecurrenceDatePicker",
                "dailyMax": "",
                "formattedValue": "",
                "lableText": "Recurrence Date",
                "messagePrefix": "",
                "monthlyDayMax": "",
                "monthlyMax": "",
                "value": "",
                "weeklyMax": "",
                "yearlyDayMax": "",
                "yearlyMax": ""
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1366,
            }
            this.compInstData = {
                "barGraph1": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "fullGraph1": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0e73c12f8313148",
                    "skin2": "CopydefBtnNormal0i6b28f45587a47",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(DPWSelectLoan, btnTestGraph, btntestGraph2, btnGetValue, flxgraphNew, fullGraphFlex, uuxNavigationRail, UUXReccurencePicker, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsloanNavigate,
            "enabledForIdleTimeout": false,
            "id": "loanNavigate",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});