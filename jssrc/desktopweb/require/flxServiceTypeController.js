define("userflxServiceTypeController", {
    onRowBtnClick: function(context) {
        this.executeOnParent('selectedRowTxt', context._kwebfw_.prop.text);
    }
});
define("flxServiceTypeControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_h6ae5433157f408e8322926ad9b86c64: function AS_Button_h6ae5433157f408e8322926ad9b86c64(eventobject, context) {
        var self = this;
        return self.onRowBtnClick.call(this, eventobject);
    }
});
define("flxServiceTypeController", ["userflxServiceTypeController", "flxServiceTypeControllerActions"], function() {
    var controller = require("userflxServiceTypeController");
    var controllerActions = ["flxServiceTypeControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
