define("flxSegment", function() {
    return function(controller) {
        var flxSegment = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxSegment.setDefaultUnit(kony.flex.DP);
        var flxStatementRows = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxStatementRows",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "90%"
        }, {}, {});
        flxStatementRows.setDefaultUnit(kony.flex.DP);
        var lblPrincipalPayTitle = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblPrincipalPayTitle",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0h31b4b422d5b45",
            "text": "Label",
            "top": "0",
            "width": "55%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPrincipalPayCurrency = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblPrincipalPayCurrency",
            "isVisible": true,
            "left": "0",
            "skin": "fontBlue",
            "text": "EUR",
            "top": "0",
            "width": "15%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPrincipalPayAmount = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblPrincipalPayAmount",
            "isVisible": true,
            "left": "0",
            "skin": "fontBlue",
            "text": "EUR",
            "top": "0",
            "width": "30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatementRows.add(lblPrincipalPayTitle, lblPrincipalPayCurrency, lblPrincipalPayAmount);
        flxSegment.add(flxStatementRows);
        return flxSegment;
    }
})