define("flxSegDebitAccount", function() {
    return function(controller) {
        var flxSegDebitAccount = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSegDebitAccount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxSegDebitAccount.setDefaultUnit(kony.flex.DP);
        var lblAccountNumber = new kony.ui.Label({
            "height": "40dp",
            "id": "lblAccountNumber",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblRobotoReg14px",
            "text": "Label",
            "top": "0dp",
            "width": "38%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAvaliableBalance = new kony.ui.Label({
            "height": "40dp",
            "id": "lblAvaliableBalance",
            "isVisible": true,
            "left": "5%",
            "skin": "sknLblRobotoReg14px",
            "text": "Label",
            "top": "0dp",
            "width": "45%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSegDebitAccount.add(lblAccountNumber, lblAvaliableBalance);
        return flxSegDebitAccount;
    }
})