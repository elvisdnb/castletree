define("CopyflxTemp0d4943827dcd74a", function() {
    return function(controller) {
        var CopyflxTemp0d4943827dcd74a = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "CopyflxTemp0d4943827dcd74a",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxWhiteBg",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        CopyflxTemp0d4943827dcd74a.setDefaultUnit(kony.flex.DP);
        var FlexContainer0e9ca59adb7ed43 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "FlexContainer0e9ca59adb7ed43",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {});
        FlexContainer0e9ca59adb7ed43.setDefaultUnit(kony.flex.DP);
        var lblHeaderAccountType = new kony.ui.Label({
            "height": "100%",
            "id": "lblHeaderAccountType",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopysknLoanRow0d880c91570f14d",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblHeaderAN = new kony.ui.Label({
            "height": "100%",
            "id": "lblHeaderAN",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCCY = new kony.ui.Label({
            "height": "100%",
            "id": "lblCCY",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblClearedBalance = new kony.ui.Label({
            "height": "100%",
            "id": "lblClearedBalance",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        var lblLockedAnalysis = new kony.ui.Label({
            "id": "lblLockedAnalysis",
            "isVisible": false,
            "left": "600dp",
            "skin": "CopyskinDefProfile",
            "top": "12dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAvailableLimit = new kony.ui.Label({
            "height": "100%",
            "id": "lblAvailableLimit",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblHdr = new kony.ui.Label({
            "height": "100%",
            "id": "lblHdr",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknLoanRow",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lstBox = new kony.ui.ListBox({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBox",
            "isVisible": false,
            "left": "642dp",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "defListBoxNormal",
            "top": "10dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        var lblServices = new kony.ui.ListBox({
            "centerY": "50.00%",
            "height": "41dp",
            "id": "lblServices",
            "isVisible": true,
            "left": "5%",
            "masterData": [
                ["select", "Please Select"],
                ["loanService1", "Apply Payment Holiday"],
                ["loanService2", "Change Interest"],
                ["loanService3", "Disburse"],
                ["loanService4", "Increase Principal"],
                ["loanService5", "Payoff"],
                ["loanService6", "Repay"]
            ],
            "selectedKey": "select",
            "skin": "CopydefListBoxNormal0f85e1426b9f54c",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        FlexContainer0e9ca59adb7ed43.add(lblHeaderAccountType, lblHeaderAN, lblCCY, lblClearedBalance, lblLockedAnalysis, lblAvailableLimit, lblHdr, lstBox, lblServices);
        CopyflxTemp0d4943827dcd74a.add(FlexContainer0e9ca59adb7ed43);
        return CopyflxTemp0d4943827dcd74a;
    }
})