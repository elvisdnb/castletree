define("userfrmLoanPayOffController", {
    _arrangementId: "",
    _loanObj: {},
    _scheduledRevisedPayment: [],
    dataPerPageNumber: 0,
    _navObj: {},
    currentOverride: {},
    submitOverride: {},
    loanProduct: "",
    loanCurrency: "",
    loanSuccess: "",
    arrayoutStandingBills: [],
    navResponse: "",
    graphValues: [],
    principalAmount: "",
    getprincipalAmount: "",
    graphSelectBox: [],
    popupSegData: gblAllaccountarray,
    totalOutStandingValues: "",
    //Type your controller code here 
    onNavigate: function(navObj) {
        this._navObj = navObj;
        if (navObj.messageInfo !== "") {
            //this.view.flxHeaderMenu.isVisible= false;
            //this.view.flxMessageInfo.isVisible = true;
            this.view.lblLoansSuccessMessage.text = navObj.messageInfo;
        } else {
            this.view.flxMessageInfo.isVisible = false;
            //this.view.flxHeaderMenu.isVisible = true;
        }
        this.bindEvents();
        //nav obj has the selected loan details
        this._loanObj = {};
        this.currentOverride = {};
        this.submitOverride = {};
        this.setLoanHeaderDetails(navObj);
        this.view.custTotalOutStanding.value = "";
        this.view.custDebitAccount.value = "";
        this.view.lblNetPrincipalPayAmount.text = "";
        this.view.lblNetPrincipalPayCurrency.text = "";
        this.view.custTotalOutStanding.currencyCode = navObj.lblCCY;
        if (navObj && navObj.lblArrangementId) {
            this._arrangementId = navObj.lblArrangementId;
        }
    },
    bindEvents: function() {
        this.view.postShow = this.postShowFunctionCall;
        this.graphValues = [];
        this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = false;
        this.view.custCancel.onclickEvent = this.onClickCancel;
        this.view.flxBillReport.onTouchEnd = this.OpenOutStandingPopup;
        this.view.imgIconClose.onTouchEnd = this.CloseOutStandingPopup;
        this.view.custProceed.onclickEvent = this.onClickSubmitData;
        this.view.btnNext.onTouchEnd = this.showChangeRequestListData;
        this.view.btnPrev.onTouchEnd = this.showChangeRequestListData;
        this.view.custDateSelectpayoff.valueChanged = this.onPayOffGraphLoad;
        this.view.flxpayInSearch.onTouchEnd = this.popupAccountList;
        this.view.popupPayinPayout.segPayinPayout.onRowClick = this.getSegRowinfo;
        this.view.popupPayinPayout.imgClose.onTouchEnd = this.payInOutPopclose;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.onCloseErrorAllert;
        this.view.popupPayinPayout.txtSearchBox.onKeyUp = this.doCRSearchWithinSeg;
        this.view.popupPayinPayout.txtSearchBox.onDone = this.doCRSearchWithinSeg;
        this.view.popupPayinPayout.imgSrc.onClick = this.doCRSearchWithinSeg;
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup.btnProceed.onClick = this.postCalculatePayoff;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.SegmentPopup2.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup2.btnProceed.onClick = this.onClickSubmitData;
        this.view.SegmentPopup2.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.cusBackIcon.onTouchEnd = this.backnavToDashboard;
        this.view.lblBreadBoardHomeValue.onTouchEnd = this.backnavToDashboard;
        this.view.lblBreadBoardOverviewvalue.onTouchEnd = this.backnavigateCustomerAccountDetails;
        this.view.segSettlement.removeAll();
    },
    backnavigateCustomerAccountDetails: function() {
        var navToCusLoans = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navObjet.isPopupShow = "false";
        navToCusLoans.navigate(navObjet);
    },
    backnavToDashboard: function() {
        var params = {};
        params.isPopupShow = "false";
        var navObject = new kony.mvc.Navigation("frmDashboardLending");
        navObject.navigate(params);
    },
    postShowFunctionCall: function() {
        try {
            this.dataPerPageNumber = 6;
            this.view.ErrorAllert.isVisible = false;
            this.view.flxPayOffTab.left = "-200%";
            this.view.flxOverallGraphPart.left = "-200%";
            this.view.custDateSelectpayoff.left = "-200%";
            this.view.iconReport.color = "temenos-Dark";
            this.view.iconTick.color = "status-success-primary";
            this.view.flxPayoffStatement.skin = "PayOffBlueBg";
            this.view.flxColorWrapper.skin = "sknBlueBorder";
            this.view.flxAccountMoved.isVisible = false;
            this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = false;
            this.view.flxOutstandingError.isVisible = false;
            this.view.ProgressIndicator.progresstext = "Fetching details...";
            this.view.ProgressIndicator.isVisible = true;
            this.view.flxCustButtons.isVisible = true;
            this.view.flxProgressLoad.isVisible = false;
            this.view.custSettlementMode.disabled = true;
            this.scheduleTimer1();
            this.loadOutStandingBills();
            this.getPayInAccounts();
            //this.pauseNavigation();
            this.postCalculatePayoff();
            //this.view.flxPayOffTab.left = "0%";
            //kony.timer.schedule("timer4",this.reloadGraph, 10, true);
            this.view.custSettlementMode.options = '[{"label": "Select","value":"None"},{"label": "Account Transfer","value":"AccountTransfer"}]';
            this.view.custSettlementMode.value = "AccountTransfer";
        } catch (err) {
            //alert("Error in postFunctionCall:::"+err);
            //this.view.ErrorAllert.lblMessage.text=response.error.message;
        }
    },
    reloadGraph: function() {
        //alert(this.graphValues.length);
        if (this.graphValues.length === 0) {
            this.view.flxOverallGraphPart.left = "-200%";
            this.view.custDateSelectpayoff.left = "-200%";
        } else {
            stepsizeValue = "";
            var nextDaysCount = this.graphValues.length;
            this.view.lblNextDays.text = "For Next " + nextDaysCount + " days";
            this.view.flxOverallGraphPart.left = "12%";
            this.view.custDateSelectpayoff.left = "10dp";
            var graphlabels = [];
            var graphDatas = [];
            var graphDatas2 = [];
            var graphDatasNewArray = [];
            for (i = 0; i < this.graphValues.length; i++) {
                graphlabels.push((this.graphValues[i].infoPayDate).substring(0, 6));
                this.graphSelectBox.push({
                    "label": this.graphValues[i].infoPayDate,
                    "value": this.graphValues[i].infoPayDate
                });
                graphDatas.push((Number((this.graphValues[i].totPayAmount).replace(/,/g, ""))));
                graphDatas2.push((Number(this.principalAmount.replace(/,/g, ""))));
            }
            this.view.blTitlePersonal.text = "Principal : " + this.getprincipalAmount;
            graphlowValues = graphDatas[0];
            var graphhighValues = graphDatas[graphDatas.length - 1];
            const diff = (a, b) => {
                return Math.abs(a - b);
            };
            var diffvlaues = diff(graphlowValues, graphhighValues);
            var roundoff = diffvlaues / (this.graphValues.length - 1);
            if (roundoff > 1) {
                roundoff = Math.ceil(roundoff / 10) * 10;
                stepsizeValue = roundoff;
                var substrings = Number(Math.ceil(graphlowValues));
                //var subSlice = substrings%roundoff;
                setgraphlowValues = ((Math.ceil(graphlowValues / roundoff) * roundoff) - roundoff);
                //setgraphHighValues =((Math.ceil(graphhighValues / roundoff) * roundoff));
            } else {
                setgraphlowValues = graphlowValues;
                stepsizeValue = roundoff.toFixed(2);
            }
            var series2Data = {
                label: "Net Settlement",
                backgroundColor: "#2E00D2",
                pointBackgroundColor: "#2E00D2",
                pointBorderColor: "#ffffff",
                borderColor: "#2E00D2",
                barThickness: 20,
                pointRadius: 0,
                data: graphDatas
            };
            graphDatasNewArray.push(series2Data);
            //alert((Math.round(graphlowValues/100)*100) - 100);
            //this.view.barGraph2.setminValue =  500;
            this.view.barGraph2.ySuggestedMin = setgraphlowValues;
            //this.view.barGraph2.ySuggestedMax= setgraphHighValues;
            this.view.barGraph2.yStepSize = stepsizeValue;
            this.view.barGraph2.xlabelsArray = JSON.stringify(graphlabels);
            this.view.barGraph2.datasetsArray = JSON.stringify(graphDatasNewArray);
            kony.timer.schedule("timer4", this.view.barGraph2.updateGraph(), 3, false);
            this.view.lblXValues.text = "Net Settlement(In " + this.loanCurrency + ")";
            this.rotateLabl();
            //this.view.flxPayoffGraph.isVisible = true;
        }
        this.scheduleTimer();
    },
    rotateLabl: function() {
        var animDefinition = {
            "100": {
                "rotate": 60
            }
        };
        animDef = kony.ui.createAnimation(animDefinition);
        var config = {
            "duration": 1,
            "iterationCount": 1,
            "delay": 0,
            "fillMode": kony.anim.FILL_MODE_FORWARDS
        };
        this.view.lblXValues.animate(animDef, config, null);
    },
    hideLoader: function() {
        this.view.ProgressIndicator.isVisible = false;
    },
    scheduleTimer: function() {
        kony.timer.schedule("timer4", this.hideLoader, 2, false);
    },
    setLoanHeaderDetails: function(loanSegRow) {
        this._loanObj.currencyId = loanSegRow.lblCCY;
        this._loanObj.arrangementId = loanSegRow.lblArrangementId;
        this._loanObj.customerId = loanSegRow.customerId;
        this._loanObj.produtId = loanSegRow.loanProduct;
        this.view.loanDetails.lblAccountNumberResponse.text = loanSegRow.lblHeaderAN;
        this.view.loanDetails.lblLoanTypeResponse.text = loanSegRow.lblHeaderAccountType; //"2345678";
        this.view.loanDetails.lblCurrencyValueResponse.text = loanSegRow.lblCCY;
        this.view.loanDetails.lblAmountValueResponse.text = loanSegRow.lblClearedBalance;
        this.view.loanDetails.lblStartDateResponse.text = loanSegRow.lblAvailableLimit;
        this.view.loanDetails.lblMaturityDateResponse.text = loanSegRow.lblHdr;
        this.loanProduct = loanSegRow.lblHeaderAccountType;
        this.loanCurrency = loanSegRow.lblCCY;
    },
    OpenOutStandingPopup: function() {
        this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = true;
    },
    CloseOutStandingPopup: function() {
        this.view.flxOutstandingRepaymentOutsideWrapper.isVisible = false;
    },
    popupAccountList: function() {
        this.view.ProgressIndicator.isVisible = true;
        this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate;
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        this.view.flxpayInOutPOP.isVisible = true;
        this.view.ProgressIndicator.isVisible = false;
    },
    getSegRowinfo: function() {
        var getAcctId = this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].accountid;
        this.view.custDebitAccount.value = getAcctId;
        this.view.flxpayInOutPOP.isVisible = false;
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    },
    payInOutPopclose: function() {
        this.view.flxpayInOutPOP.isVisible = false;
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    },
    loadOutStandingBills: function() {
        //alert(this._arrangementId);
        var serviceName = "LendingNew";
        var operationName = "getOutstandingBills";
        var headers = "";
        var inputParams = {
            "arrangementID": this._arrangementId
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackbills, this.failureCallBackbills);
    },
    successCallBackbills: function(response) {
        var alloutStandingBills = response.body.length;
        var length = alloutStandingBills.toString();
        this.view.flxOutStandingSegment.isVisible = true;
        this.view.flxOutstandingRepaymentInnerWrapper.height = "360dp";
        this.view.flxOutstandingError.isVisible = false;
        if (length < 6) {
            this.view.flxSegmentHeaderInfo.width = "96%";
        } else {
            this.view.flxSegmentHeaderInfo.width = "94%";
        }
        for (i = 0; i < alloutStandingBills; i++) {
            this.arrayoutStandingBills.push({
                billDate: response.body[i].billDate,
                billedAmount: response.body[i].billedAmount,
                billType: response.body[i].billType,
                outstandingAmount: response.body[i].outstandingAmount,
                billStatus: response.body[i].billStatus
            });
        }
        this._scheduledRevisedPayment = this.arrayoutStandingBills;
        this.showChangeRequestListData("");
    },
    failureCallBackbills: function(response) {
        this.view.flxOutStandingSegment.isVisible = false;
        this.view.flxOutstandingRepaymentInnerWrapper.height = "200dp";
        this.view.flxOutstandingError.isVisible = true;
        this.view.lblOutError.text = JSON.stringify(response.error.message);
    },
    getPayInAccounts: function() {
        var serviceName = "LendingNew";
        var operationName = "getPayInAccountDetails";
        var headers = "";
        var inputParams = {
            "arrangementID": this._arrangementId
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackaccount, this.failureCallBackaccount);
        // this.view.ProgressIndicator.isVisible=true;
    },
    successCallBackaccount: function(response) {
        if (response.body[0].payinAccount !== undefined) {
            this.view.custDebitAccount.value = response.body[0].payinAccount;
        } else {
            this.view.custDebitAccount.value = "";
        }
        //this.view.ProgressIndicator.isVisible=false;
    },
    failureCallBackaccount: function(response) {
        //this.view.ProgressIndicator.isVisible=false;
        if (response.error && response.error.length !== 0) {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = response.error.message;
        }
        //alert("Error in Fetching current commitment"+JSON.stringify(response)); 
    },
    postCalculatePayoff: function() {
        //this.view.SegmentPopup.isVisible = false;
        this.view.ProgressIndicator.progresstext === "Fetching details...";
        this.view.ProgressIndicator.isVisible = true;
        this.scheduleTimer1();
        var serviceName = "LendingNew";
        var operationName = "";
        if (this.loanProduct === "Standard Personal Loan") {
            operationName = "postPersonalCalculatePayoff";
        } else {
            operationName = "postMortgageCalculatePayoff";
        }
        var headers = "";
        var inputParams = {
            "arrangementId": this._arrangementId,
            "overrideDetails": this.currentOverride
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackCalPayOff, this.failureCallBackCalPayOff);
        //  this.view.ProgressIndicator.isVisible=true;
    },
    successCallBackCalPayOff: function(response) {
        if (response.body[0].infoPayments) {
            this.graphValues = response.body[0].infoPayments;
        }
        var responseProperty = response.body[0].properties;
        payOffStatement = [];
        for (i = 0; i < responseProperty.length; i++) {
            if (responseProperty[i].propertyAmount) {
                currentBalance = Application.numberFormater.convertForDispaly(this.removeMinus((responseProperty[i].propertyAmount).replace(/,/g, "")), this.loanCurrency);
                if (currentBalance === "NaN") {
                    currentBalance = "";
                }
            } else {
                currentBalance = "";
            }
            if (responseProperty[i].propertyName === "Account") {
                this.principalAmount = responseProperty[i].propertyAmount;
                this.getprincipalAmount = currentBalance;
            }
            temppayOffStatement = {
                lblPrincipalPayTitle: responseProperty[i].propertyName,
                lblPrincipalPayCurrency: this.loanCurrency,
                lblPrincipalPayAmount: {
                    "text": currentBalance,
                    "skin": "fontBlue"
                }
            };
            if (temppayOffStatement.lblPrincipalPayTitle === "Payoff Fee") {
                if (temppayOffStatement.lblPrincipalPayAmount.text !== undefined) {
                    temppayOffStatement.lblPrincipalPayAmount.text = currentBalance;
                    temppayOffStatement.lblPrincipalPayAmount.skin = "fontBlue";
                } else {
                    temppayOffStatement.lblPrincipalPayAmount.text = "-";
                    temppayOffStatement.lblPrincipalPayAmount.skin = "fontBlue";
                }
            } else {
                if (temppayOffStatement.lblPrincipalPayAmount.text !== undefined) {
                    temppayOffStatement.lblPrincipalPayAmount = currentBalance;
                    temppayOffStatement.lblPrincipalPayAmount.skin = "fontBlue";
                } else {
                    temppayOffStatement.lblPrincipalPayAmount = "-";
                    temppayOffStatement.lblPrincipalPayAmount.skin = "fontBlue";
                }
            }
            payOffStatement.push(temppayOffStatement);
            //alert(JSON.stringify(payOffStatement));
        }
        var widgetData = {
            "lblPrincipalPayTitle": "lblPrincipalPayTitle",
            "lblPrincipalPayCurrency": "lblPrincipalPayCurrency",
            "lblPrincipalPayAmount": "lblPrincipalPayAmount"
        };
        this.view.segSettlement.widgetDataMap = widgetData;
        this.view.segSettlement.setData(payOffStatement);
        var currentBalance2 = Application.numberFormater.convertForDispaly(this.removeMinus((response.body[0].payoffs[0].totPayoffAmount).replace(/,/g, "")), this.loanCurrency);
        if (currentBalance2 === "NaN") {
            currentBalance2 = "";
        }
        this.view.lblNetPrincipalPayAmount.text = currentBalance2;
        //this.view.datePayValue.value = response.body[0].payoffs[0].payoffDate;
        this.view.custTotalOutStanding.value = currentBalance2;
        //this.resumeNavigation();
        var outSTandValue = currentBalance2;
        if (this.loanCurrency === "EUR") {
            this.totalOutStandingValues = outSTandValue.replace(".", "");
        } else {
            this.totalOutStandingValues = outSTandValue.replace(/,/g, '');
        }
        this.view.lblNetPrincipalPayCurrency.text = this.loanCurrency;
        this.reloadGraph();
        //this.view.SegmentPopup.isVisible = false;
        this.currentOverride = {};
        this.view.flxPayOffTab.left = "0%";
    },
    failureCallBackCalPayOff: function(response) {
        this.view.ProgressIndicator.isVisible = false;
        var errorData = Application.validation.isNullUndefinedObj(response.error);
        var genericError = Application.validation.isNullUndefinedObj(response.errmsg);
        var overrideError = Application.validation.isNullUndefinedObj(response.override);
        if (overrideError) {} else {
            if (errorData !== "") {
                if (errorData.errorDetails) {
                    this.view.ErrorAllert.lblMessage.text = errorData.errorDetails[0].message;
                    this.view.ErrorAllert.isVisible = true;
                    this.view.flxPayOffTab.left = "0%";
                } else {
                    this.view.ErrorAllert.lblMessage.text = errorData.message;
                    this.view.ErrorAllert.isVisible = true;
                    this.view.flxPayOffTab.left = "0%";
                }
            } else {
                if (genericError !== "") {
                    this.view.ErrorAllert.lblMessage.text = "Unable to process, please try again";
                    this.view.ErrorAllert.isVisible = true;
                    this.view.flxPayOffTab.left = "0%";
                }
            }
        }
        if (response.override && !isNullorEmpty(response.override.overrideDetails)) {
            //       var overwriteDetails = [];
            //       var overWriteData = [];
            //       var segDataForOverrite = [];
            this.currentOverride = response.override.overrideDetails;
            this.postCalculatePayoff();
            //       overwriteDetails = response.override.overrideDetails;
            //       for(var a in overwriteDetails) {
            //         overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
            //         var json = {
            //           "lblSerial" : "*",
            //           "lblInfo" : overwriteDetails[a].description
            //         };
            //         segDataForOverrite.push(json);
            //       }
            //       this.view.SegmentPopup.segOverride.widgetDataMap = {
            //         "lblSerial" : "lblSerial",
            //         "lblInfo" : "lblInfo"
            //       };
            //       this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
            //       this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
            //       this.view.SegmentPopup.isVisible = true;
            //       this.view.forceLayout();
        }
        // this.resumeNavigation();
        //alert("Error in Fetching current commitment"+JSON.stringify(response));
    },
    removeMinus: function(x) {
        return Math.abs(x);
    },
    onPayOffGraphLoad: function() {
        if (this.graphValues.length === 0) {} else {
            this.view.custDateSelectpayoff.disabled = false;
            var arrayNumber = this.graphValues.findIndex(this.graphFindArrayindex);
            if (arrayNumber === "-1") {} else {
                this.view.custInterestpayoff.value = this.graphValues[arrayNumber].totInterestAmount;
                this.view.custNetPayoff.value = this.graphValues[arrayNumber].totPayAmount;
            }
        }
    },
    graphFindArrayindex: function(arrays1) {
        var currvalue = this.view.custDateSelectpayoff.value;
        return arrays1.infoPayDate === currvalue;
    },
    //   animateLoadShow: function(){
    //     //this.view.flxProgressLoad.isVisible=true;
    //     kony.timer.schedule("timer4",this.animateFunction, 2, false);
    //   },
    animateFunction: function() {
        var self = this;

        function SCALE_ACTION_d88531a6052541d7a23f0d3e7eb7ae71_Callback() {
            kony.timer.cancel("timer2");
            self.view.flxProgressTitles.text = "Zeroize Account Balance…";
            self.view.flxActiveBar.animate(kony.ui.createAnimation({
                "0": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "width": "50%"
                },
                "100": {
                    "stepConfig": {
                        "timingFunction": kony.anim.EASE
                    },
                    "width": "100%"
                },
            }), {
                "delay": 0,
                "iterationCount": 1,
                "fillMode": kony.anim.FILL_MODE_FORWARDS,
                "duration": 1
            }, {
                "animationEnd": function endCalbck() {
                    return;
                }
            });
            kony.timer.cancel("timer4");
        }
        kony.timer.schedule("timer2", self.view.flxActiveBar.animate(kony.ui.createAnimation({
            "0": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "width": "0%"
            },
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "width": "50%"
            },
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 1
        }, {
            "animationEnd": SCALE_ACTION_d88531a6052541d7a23f0d3e7eb7ae71_Callback
        }), 2, false);
        kony.timer.cancel("timer4");
        this.view.ProgressIndicator.isVisible = true;
    },
    showPaginationSegmentData: function(dataPerPage, arrayOfData, btnName) {
        kony.print("showPaginationSegmentData===>");
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.flxPagination.isVisible = true;
            } else {
                this.view.flxPagination.isVisible = false;
            }
            if (btnName === "btnPrev" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName === "btnNext" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;            
            } else if (this.gblSegPageCount === 1 && this.totalpage > 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;           
            } else if (this.gblSegPageCount === 1 && this.totalpage === 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.lblSearchPage.text = "";
            } else if (this.gblSegPageCount === this.totalpage && this.totalpage > 1) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnDisable";
                //this.view.lblSearchPage.isVisible = true;            
            } else {
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.lblSearchPage.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in next previous method " + err);
        }
    },
    showChangeRequestListData: function(buttonevent) {
        kony.print("showChangeRequestListData===>");
        //showloader();
        if (this._scheduledRevisedPayment.length > 0) {
            kony.print("segDataSet ====>>" + JSON.stringify(this.arrayoutStandingBills));
            var segmentData = this.showPaginationSegmentData(this.dataPerPageNumber, this._scheduledRevisedPayment, buttonevent.id);
            var widgetData = {
                "lblDueDateInfo": "billDate",
                "lblTypeInfo": "billType",
                "lblBilledInfo": "billedAmount",
                "lblOutstandingInfo": "outstandingAmount",
                "lblStatusInfo": "billStatus"
            };
            this.view.segOutstandingBill.rowTemplate = "flxPayOffOutstandingBillDetails";
            this.view.segOutstandingBill.widgetDataMap = widgetData;
            this.view.segOutstandingBill.setData(segmentData);
            this.view.forceLayout();
        }
        //dismissLoader();
    },
    onClickSubmitData: function() {
        isValidData = this.validateData();
        if (isValidData === 0) {
            kony.timer.schedule("timer4", this.animateFunction, 2, false);
            this.view.SegmentPopup2.isVisible = false;
            this.view.ProgressIndicator.progresstext = "Progress";
            this.view.ProgressIndicator.isVisible = true;
            if (this.loanProduct === "Standard Personal Loan") {
                this.loanSuccess = "Personal Loan";
            } else {
                this.loanSuccess = "Mortgage";
            }
            var amountField = this.totalOutStandingValues;
            var debitAccountId = this.view.custDebitAccount.value;
            var currentAccountId = this.view.loanDetails.lblAccountNumberResponse.text;
            this.view.flxCustButtons.isVisible = false;
            var serviceName = "LendingNew";
            var operationName = "postPayOff";
            var headers = "";
            var inputParams = {
                "paymentOrderProductId": "ACOTHER",
                "debitAccountId": debitAccountId,
                "creditAccountId": currentAccountId,
                "amount": amountField,
                "waiveCharges": "YES",
                "orderType": "CUSTOMER",
                "orderInitiationType": "LOANPAYOFF",
                "idType": "PRIVATE",
                "overrideDetails": this.submitOverride
            };
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunctionresponse, this.failureCallBackfunctionresponse);
        } else {}
    },
    successCallBackfunctionresponse: function(response) {
        this.navResponse = response;
        this.view.flxProgressLoad.isVisible = false;
        this.view.flxAccountMoved.isVisible = true;
        this.view.flxPayoffStatement.skin = "PayOffGreenBg";
        this.view.flxColorWrapper.skin = "sknGreenBorder";
        this.view.ProgressIndicator.isVisible = false;
        this.navigateScheduleTimer();
    },
    failureCallBackfunctionresponse: function(response) {
        this.view.ProgressIndicator.isVisible = false;
        //alert("error"+JSON.stringify(response));
        var errorData = Application.validation.isNullUndefinedObj(response.error);
        var genericError = Application.validation.isNullUndefinedObj(response.errmsg);
        var overrideError = Application.validation.isNullUndefinedObj(response.override);
        if (errorData) {
            if (response.error && errorData.errorDetails) {
                //array of errorDetails
                this.view.ErrorAllert.lblMessage.text = errorData.errorDetails[0].message;
            } else {
                //single error -system error
                this.view.ErrorAllert.lblMessage.text = errorData.message;
            }
            this.view.ErrorAllert.isVisible = true;
        }
        /*
    else if(!overrideError && genericError){
      this.view.ErrorAllert.lblMessage.text = "Unable to process, please try again";
      this.view.ErrorAllert.isVisible=true;
    }
	*/
        if (response.overrideDetails && !isNullorEmpty(response.overrideDetails)) {
            var overwriteDetails = [];
            var overWriteData = [];
            var segDataForOverrite = [];
            this.submitOverride = response.overrideDetails;
            overwriteDetails = response.overrideDetails;
            for (var a in overwriteDetails) {
                overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                var json = {
                    "lblSerial": "*",
                    "lblInfo": overwriteDetails[a].description
                };
                segDataForOverrite.push(json);
            }
            this.view.SegmentPopup2.segOverride.widgetDataMap = {
                "lblSerial": "lblSerial",
                "lblInfo": "lblInfo"
            };
            this.view.SegmentPopup2.lblPaymentInfo.text = "Override Confirmation";
            this.view.SegmentPopup2.segOverride.setData(segDataForOverrite);
            this.view.SegmentPopup2.isVisible = true;
            this.view.forceLayout();
        }
    },
    navigateScheduleTimer: function() {
        kony.timer.schedule("timer4", this.navigateToLoanFunc, 3, false);
    },
    navigateToLoanFunc: function() {
        var refId = this.navResponse.header.id;
        var navToLoanPage = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navObjet.messageInfo = this.loanSuccess + " paid off successfully!   Reference : " + refId;
        navToLoanPage.navigate(navObjet);
    },
    onClickCancel: function() {
        var navToHome = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navToHome.navigate(navObjet);
    },
    onCloseErrorAllert: function() {
        this.view.ErrorAllert.isVisible = false;
    },
    onLoadingText: function() {
        if (this.view.ProgressIndicator.progresstext === "Fetching details...") {
            this.view.ProgressIndicator.progresstext = "Fetching details...";
        } else if (this.view.ProgressIndicator.progresstext === "Fetching details...") {
            this.view.ProgressIndicator.progresstext = "Fetching details...";
        } else {
            kony.timer.cancel("timer11");
        }
    },
    scheduleTimer1: function() {
        kony.timer.schedule("timer11", this.onLoadingText, 4, true);
    },
    doCRSearchWithinSeg: function() {
        kony.print('edit');
        var stringForSearch = this.view.popupPayinPayout.txtSearchBox.text.toLowerCase();
        if (stringForSearch.length === 0) {
            //Reset to show the full claims search results global variable
            // disable the clear search x mark, if any
            this.view.popupPayinPayout.segPayinPayout.removeAll();
            this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        } else {
            //empty search string now
            // visible on for the clear search x mark
        }
        if (stringForSearch.length >= 3) {
            //this.view.popupPayinPayout.segPayinPayout
            var segData = gblAllaccountarray; // make a copy not response
            var finalData = [];
            if (segData && segData === []) {
                return;
            }
            for (var i = 0; i < segData.length; i++) {
                var aRowData = segData[i];
                var aRowDataStrigified = JSON.stringify(aRowData);
                if (aRowDataStrigified.toLowerCase().indexOf(stringForSearch) >= 0) {
                    finalData.push(aRowData);
                } else {
                    //no need to push it - as does not meet criteria
                }
            }
            this.view.popupPayinPayout.segPayinPayout.removeAll();
            this.view.popupPayinPayout.segPayinPayout.setData(finalData);
        }
    },
    closeSegmentOverrite: function() {
        //this.view.SegmentPopup.isVisible = false;
        this.view.SegmentPopup2.isVisible = false;
        //this.currentOverride = {};
        this.submitOverride = {};
        this.view.forceLayout();
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    validateData: function() {
        var Requiredflag = 0;
        var FormId = document.getElementsByTagName("form")[0].getAttribute("id");
        var form = document.getElementById(FormId);
        var Requiredfilter = form.querySelectorAll('*[required]');
        for (var i = 0; i < Requiredfilter.length; i++) {
            if (Requiredfilter[i].disabled !== true && Requiredfilter[i].value === '') {
                var ElementId = Requiredfilter[i].id;
                var Element = document.getElementById(ElementId);
                Element.reportValidity();
                Requiredflag = 1;
            }
        }
        return Requiredflag;
    }
});
define("frmLoanPayOffControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_bb61b37313204be3833502157fa6d2d9: function AS_UWI_bb61b37313204be3833502157fa6d2d9(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_a47c5b20b2604db1b286d9916959a04c: function AS_UWI_a47c5b20b2604db1b286d9916959a04c(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmLoanPayOffController", ["userfrmLoanPayOffController", "frmLoanPayOffControllerActions"], function() {
    var controller = require("userfrmLoanPayOffController");
    var controllerActions = ["frmLoanPayOffControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
