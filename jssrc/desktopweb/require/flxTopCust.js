define("flxTopCust", function() {
    return function(controller) {
        var flxTopCust = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxTopCust",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxTopCust.setDefaultUnit(kony.flex.DP);
        var flxCustomerWrap = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxCustomerWrap",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxCustomerWrap.setDefaultUnit(kony.flex.DP);
        var lblCustomerID = new kony.ui.Label({
            "height": "100%",
            "id": "lblCustomerID",
            "isVisible": true,
            "left": "1%",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "101146",
            "top": "0",
            "width": "25.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFirstName = new kony.ui.Label({
            "height": "100%",
            "id": "lblFirstName",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Rolf Gerieng",
            "top": "0dp",
            "width": "33.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRm = new kony.ui.Label({
            "height": "100%",
            "id": "lblRm",
            "isVisible": true,
            "left": 0,
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Rolf Gerieng",
            "top": "0dp",
            "width": "40.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerWrap.add(lblCustomerID, lblFirstName, lblRm);
        flxTopCust.add(flxCustomerWrap);
        return flxTopCust;
    }
})