define("userflxOverrideTemplateController", {
    //Type your controller code here 
});
define("flxOverrideTemplateControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxOverrideTemplateController", ["userflxOverrideTemplateController", "flxOverrideTemplateControllerActions"], function() {
    var controller = require("userflxOverrideTemplateController");
    var controllerActions = ["flxOverrideTemplateControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
