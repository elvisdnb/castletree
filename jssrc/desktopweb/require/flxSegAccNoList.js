define("flxSegAccNoList", function() {
    return function(controller) {
        var flxSegAccNoList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSegAccNoList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxSegAccNoList.setDefaultUnit(kony.flex.DP);
        var lblAccID = new kony.ui.Label({
            "height": "36dp",
            "id": "lblAccID",
            "isVisible": true,
            "left": "2dp",
            "onTouchStart": controller.AS_Label_ic605b3d62f64d1aa203ac47f03d96fb,
            "skin": "CopysknlblBG4",
            "text": "Account Id",
            "top": "2dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustomerID = new kony.ui.Label({
            "height": "36dp",
            "id": "lblCustomerID",
            "isVisible": true,
            "left": "10dp",
            "skin": "Copysknlbl4",
            "text": "Customer ID",
            "top": "2dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMnemonic = new kony.ui.Label({
            "height": "36dp",
            "id": "lblMnemonic",
            "isVisible": false,
            "left": "10dp",
            "skin": "Copysknlbl4",
            "text": "Mnenonic",
            "top": "2dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblProduct = new kony.ui.Label({
            "height": "36dp",
            "id": "lblProduct",
            "isVisible": true,
            "left": "20dp",
            "skin": "Copysknlbl4",
            "text": "Product",
            "top": "2dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCCY = new kony.ui.Label({
            "height": "36dp",
            "id": "lblCCY",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl14PX434343BG",
            "text": "CCY",
            "top": "2dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsableBlance = new kony.ui.Label({
            "height": "36dp",
            "id": "lblUsableBlance",
            "isVisible": false,
            "left": "20dp",
            "skin": "CopysknlblFFF",
            "text": "Usable Balance",
            "top": "2dp",
            "width": "18%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsableBalance = new kony.ui.Label({
            "height": "36dp",
            "id": "lblUsableBalance",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopypayRed",
            "text": "Usable Balance",
            "top": "2dp",
            "width": "18%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSegAccNoList.add(lblAccID, lblCustomerID, lblMnemonic, lblProduct, lblCCY, lblUsableBlance, lblUsableBalance);
        return flxSegAccNoList;
    }
})