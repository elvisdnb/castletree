define("frmFundDeposit", function() {
    return function(controller) {
        function addWidgetsfrmFundDeposit() {
            this.setDefaultUnit(kony.flex.DP);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRailWeb({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e8c886e2225540",
                "top": "0%",
                "width": "64dp",
                "overrides": {
                    "uuxNavigationRailWeb": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxContainerBGF0iba764965a424b",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "72dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "right": "32dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "168dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.CustomWidget({
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "0dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "8dp",
                "skin": "sknLbl12pxBG005AA0Underline",
                "text": "Home",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopysknLbl0c3756e5a60504a",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopysknLbl5",
                "text": "Overview",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "72dp",
                "width": "96%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var customerDetails = new com.lending.customerDetails.customerDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "157dp",
                "id": "customerDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "32dp",
                "skin": "CopysknFlxBorderCCE1",
                "top": "72dp",
                "zIndex": 1,
                "overrides": {
                    "customerDetails": {
                        "isVisible": false
                    },
                    "imgCustomerProfilePic": {
                        "src": "pficon_1_1.png"
                    },
                    "lblAccountOfficerInfo": {
                        "left": "70%"
                    },
                    "lblAccountOfficerResponse": {
                        "left": "70%"
                    },
                    "lblEmailInfo": {
                        "left": "25%"
                    },
                    "lblEmailResponse": {
                        "left": "25%",
                        "width": "170dp"
                    },
                    "lblInternetBankingInfo": {
                        "left": "70%"
                    },
                    "lblInternetBankingResponse": {
                        "left": "70%"
                    },
                    "lblLocationInfo": {
                        "left": "55%"
                    },
                    "lblLocationResponse": {
                        "left": "55%"
                    },
                    "lblMobileBankingInfo": {
                        "left": "85%"
                    },
                    "lblMobileBankingResponse": {
                        "left": "85%"
                    },
                    "lblPhoneInfo": {
                        "left": "40%"
                    },
                    "lblPhoneResponse": {
                        "left": "40%"
                    },
                    "lblProfileInfo": {
                        "left": "55%"
                    },
                    "lblProfileResponse": {
                        "left": "55%"
                    },
                    "lblRelationInfo": {
                        "left": "40%"
                    },
                    "lblRelationResponse": {
                        "left": "40%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var MainTabs = new com.lending.MainTabsCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "11dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopysknFlxContainerBGF",
                "top": "230dp",
                "width": "94%",
                "zIndex": 1,
                "overrides": {
                    "MainTabsCopy": {
                        "left": "11dp",
                        "top": "230dp",
                        "width": "94%"
                    },
                    "flxContainerAccount": {
                        "height": "2dp"
                    },
                    "flxContainerdeposits": {
                        "height": "5%"
                    },
                    "flxSeparatorLine1": {
                        "isVisible": false
                    },
                    "flxontainerLoans": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllertCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyCopyslFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "ErrorAllertCopy": {
                        "isVisible": false
                    },
                    "imgClose": {
                        "src": "ico_close_1.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var FundDetails = new com.lending.fundDetails.FundDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "FundDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "32dp",
                "skin": "slFbox",
                "top": "292dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "FundDetails": {
                        "left": "18dp",
                        "right": "32dp",
                        "top": "292dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxDepositsDetailsvalue": {
                        "left": "0dp",
                        "top": "70dp",
                        "width": "100%"
                    },
                    "flxFundDetailsHeader": {
                        "right": "32dp",
                        "width": "100%"
                    },
                    "flxListBoxSelectedValue": {
                        "left": "20dp",
                        "width": "140dp"
                    },
                    "flxontainerLoans": {
                        "bottom": "-1px",
                        "top": "7dp"
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "left": "8dp",
                        "src": "drop_down_arrow_1.png"
                    },
                    "lblAccountNumberInfo": {
                        "left": "27dp",
                        "width": "160dp"
                    },
                    "lblAccountNumberResponse": {
                        "left": "27dp",
                        "width": "160dp"
                    },
                    "lblAccountServiceInfo": {
                        "left": "20dp",
                        "text": "Deposit Services"
                    },
                    "lblAccountTypeInfo": {
                        "left": "20dp",
                        "width": "160dp"
                    },
                    "lblAccountTypeResponse": {
                        "left": "20dp",
                        "width": "160dp"
                    },
                    "lblAmountValueInfo": {
                        "left": "20dp",
                        "width": "155dp"
                    },
                    "lblAmountValueResponse": {
                        "left": "20dp",
                        "text": "83,550.50",
                        "width": "155dp"
                    },
                    "lblCurrencyValueInfo": {
                        "left": "20dp",
                        "width": "120dp"
                    },
                    "lblCurrencyValueResponse": {
                        "left": "20dp",
                        "width": "120dp"
                    },
                    "lblListBoxSelectedValue": {
                        "centerX": "50%",
                        "left": "viz.val_cleared"
                    },
                    "lblMaturityDateInfo": {
                        "left": "20dp",
                        "width": "155dp"
                    },
                    "lblMaturityDateResponse": {
                        "left": "20dp",
                        "width": "155dp"
                    },
                    "lblStartDateInfo": {
                        "left": "20dp",
                        "width": "150dp"
                    },
                    "lblStartDateResponse": {
                        "left": "20dp",
                        "width": "150dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxFundDepositWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "460dp",
                "id": "flxFundDepositWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "right": "32dp",
                "skin": "CopysknFlxBorderCCE1",
                "top": "418dp",
                "zIndex": 1
            }, {}, {});
            flxFundDepositWrapper.setDefaultUnit(kony.flex.DP);
            var lblFundDepositInfo = new kony.ui.Label({
                "height": "20dp",
                "id": "lblFundDepositInfo",
                "isVisible": true,
                "left": "10%",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "Fund Deposit",
                "top": "50dp",
                "width": "124dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLine1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "90dp",
                "isModalContainer": false,
                "right": "60dp",
                "skin": "CopysknLine",
                "top": "90dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxLine1.setDefaultUnit(kony.flex.DP);
            flxLine1.add();
            var cusTextDepositAccountNo = new kony.ui.CustomWidget({
                "id": "cusTextDepositAccountNo",
                "isVisible": true,
                "left": "10%",
                "top": "120dp",
                "width": "25%",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 0,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "Deposit Account No",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "required": "",
                "suffix": "",
                "value": ""
            });
            var cusTextExpectedBalance = new kony.ui.CustomWidget({
                "id": "cusTextExpectedBalance",
                "isVisible": true,
                "left": "37%",
                "top": "120dp",
                "width": "25%",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "decimals": 0,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "Expected Balance",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "required": "",
                "suffix": "",
                "value": ""
            });
            var cusTextAvailableBalance = new kony.ui.CustomWidget({
                "id": "cusTextAvailableBalance",
                "isVisible": true,
                "left": "64%",
                "top": "120dp",
                "width": "25%",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "decimals": 0,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "Available Balance",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "required": "",
                "suffix": "",
                "value": ""
            });
            var cusTextDepositAccountNoDummy = new kony.ui.CustomWidget({
                "id": "cusTextDepositAccountNoDummy",
                "isVisible": false,
                "left": "10%",
                "top": 140,
                "width": "25%",
                "height": "50dp",
                "minWidth": "291dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountField",
                "decimals": 2,
                "disabled": true,
                "endAligned": false,
                "labelValue": "Deposit Account No",
                "locale": "",
                "placeholderValue": "",
                "prefix": "",
                "readonly": false,
                "required": false,
                "suffix": "",
                "value": ""
            });
            var cusTextExpectedBalanceDummy = new kony.ui.CustomWidget({
                "id": "cusTextExpectedBalanceDummy",
                "isVisible": false,
                "left": "37%",
                "top": 140,
                "width": "25%",
                "height": "50dp",
                "minWidth": "291dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountField",
                "decimals": 2,
                "disabled": true,
                "endAligned": false,
                "labelValue": "Expected Balance",
                "locale": "",
                "placeholderValue": "",
                "prefix": "",
                "readonly": false,
                "required": false,
                "suffix": "",
                "value": ""
            });
            var cusTextAvailableBalanceDummy = new kony.ui.CustomWidget({
                "id": "cusTextAvailableBalanceDummy",
                "isVisible": false,
                "left": "64%",
                "top": 140,
                "width": "25%",
                "height": "50dp",
                "minWidth": "291dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountField",
                "decimals": 2,
                "disabled": true,
                "endAligned": false,
                "labelValue": "Available Balance",
                "locale": "",
                "placeholderValue": "",
                "prefix": "",
                "readonly": false,
                "required": false,
                "suffix": "",
                "value": ""
            });
            var flxDebitAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDebitAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "210dp",
                "width": "25%"
            }, {}, {});
            flxDebitAccount.setDefaultUnit(kony.flex.DP);
            var flxDebitAccountInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDebitAccountInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "85%",
                "zIndex": 1
            }, {}, {});
            flxDebitAccountInnerWrapper.setDefaultUnit(kony.flex.DP);
            var floatLabelText = new com.lending.floatingTextBox.floatLabelTextCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "40dp",
                "id": "floatLabelText",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyflxTemenosLighest",
                "top": "0dp",
                "width": "100%",
                "zIndex": 3,
                "overrides": {
                    "downArrowLbl": {
                        "isVisible": true
                    },
                    "floatLabelTextCopy": {
                        "height": "40dp"
                    },
                    "lblFloatLabel": {
                        "text": "Debit Account No"
                    },
                    "txtFloatText": {
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDebitAccountInnerWrapper.add(floatLabelText);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30dp"
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var lblSearch = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "40dp",
                "id": "lblSearch",
                "isVisible": true,
                "skin": "Copycastleicon",
                "text": "k",
                "width": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearch.add(lblSearch);
            flxDebitAccount.add(flxDebitAccountInnerWrapper, flxSearch);
            var cusCalenderPaymentDateDummy = new kony.ui.CustomWidget({
                "id": "cusCalenderPaymentDateDummy",
                "isVisible": false,
                "left": "37%",
                "top": "250dp",
                "width": "25%",
                "height": "50dp",
                "zIndex": 5,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": false,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "dd MMM yyy",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Payment Date",
                "max": "",
                "min": "",
                "showWeekNumber": false,
                "value": "",
                "weekLabel": "Wk"
            });
            var cusCalenderPaymentDate = new kony.ui.CustomWidget({
                "id": "cusCalenderPaymentDate",
                "isVisible": true,
                "left": "37%",
                "top": "210dp",
                "width": "25%",
                "height": "55dp",
                "zIndex": 2,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "dd MMM yyy",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Payment Date",
                "max": "",
                "min": "",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var flxDropDownAccountList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "250dp",
                "id": "flxDropDownAccountList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250dp",
                "width": "280dp",
                "zIndex": 1
            }, {}, {});
            flxDropDownAccountList.setDefaultUnit(kony.flex.DP);
            var AccountList2 = new com.konymp.AccountList.AccountList({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "240dp",
                "id": "AccountList2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxWhiteBGBorder0075db",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDropDownAccountList.add(AccountList2);
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "210dp",
                "width": "25%"
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var cusTextAmount = new kony.ui.CustomWidget({
                "id": "cusTextAmount",
                "isVisible": true,
                "left": "0%",
                "top": "0dp",
                "width": "100%",
                "height": "100%",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "required": "",
                "suffix": "",
                "validateRequired": "required",
                "value": ""
            });
            flxAmount.add(cusTextAmount);
            var lblErrorMsg = new kony.ui.Label({
                "height": "50dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "10%",
                "skin": "CopysknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "310dp",
                "width": "280dp",
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtonContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxButtonContainer1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "11%",
                "skin": "slFbox",
                "top": "340dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxButtonContainer1.setDefaultUnit(kony.flex.DP);
            var flxSubmit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "id": "flxSubmit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0%",
                "skin": "slFbox",
                "zIndex": 500
            }, {}, {});
            flxSubmit.setDefaultUnit(kony.flex.DP);
            var cusButtonConfirm1 = new kony.ui.CustomWidget({
                "id": "cusButtonConfirm1",
                "isVisible": true,
                "left": 0,
                "right": "0%",
                "top": "0dp",
                "width": "179dp",
                "height": "100%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": " Confirm      ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxSubmit.add(cusButtonConfirm1);
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "24dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": "147dp",
                "zIndex": 500
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            var cusButtonCancel1 = new kony.ui.CustomWidget({
                "id": "cusButtonCancel1",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "147dp",
                "height": "100%",
                "minWidth": "200dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxCancel.add(cusButtonCancel1);
            flxButtonContainer1.add(flxSubmit, flxCancel);
            var flxButtonContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "50dp",
                "clipBounds": false,
                "height": "43dp",
                "id": "flxButtonContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "6%",
                "skin": "slFbox",
                "top": "340dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxButtonContainer.setDefaultUnit(kony.flex.DP);
            var cusButtonCancel = new kony.ui.CustomWidget({
                "id": "cusButtonCancel",
                "isVisible": true,
                "right": "200dp",
                "top": "1dp",
                "width": "184dp",
                "height": "45dp",
                "minWidth": 184,
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusButtonConfirm = new kony.ui.CustomWidget({
                "id": "cusButtonConfirm",
                "isVisible": true,
                "right": "0dp",
                "top": "1dp",
                "width": "184dp",
                "height": "45dp",
                "minWidth": 184,
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "X-large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxButtonContainer.add(cusButtonCancel, cusButtonConfirm);
            flxFundDepositWrapper.add(lblFundDepositInfo, flxLine1, cusTextDepositAccountNo, cusTextExpectedBalance, cusTextAvailableBalance, cusTextDepositAccountNoDummy, cusTextExpectedBalanceDummy, cusTextAvailableBalanceDummy, flxDebitAccount, cusCalenderPaymentDateDummy, cusCalenderPaymentDate, flxDropDownAccountList, flxAmount, lblErrorMsg, flxButtonContainer1, flxButtonContainer);
            flxMainContainer.add(flxHeaderMenu, custInfo, customerDetails, MainTabs, ErrorAllert, FundDetails, flxFundDepositWrapper);
            var flxNavigationRail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxNavigationRail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "zIndex": 10
            }, {}, {});
            flxNavigationRail.setDefaultUnit(kony.flex.DP);
            var cusNavigateRail = new kony.ui.CustomWidget({
                "id": "cusNavigateRail",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "64dp",
                "height": "900dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxNavigationRail",
                "bottomBtnIcon": "",
                "bottomBtnItems": "",
                "hideBottomBtn": false,
                "logoAlt": "Temenos logo",
                "menuItems": "",
                "positionValue": {
                    "optionsList": ["left", "right"],
                    "selectedValue": "left"
                }
            });
            flxNavigationRail.add(cusNavigateRail);
            var flxDropDownAccountListOutsideWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDropDownAccountListOutsideWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDropDownAccountListOutsideWrapper.setDefaultUnit(kony.flex.DP);
            var flxDropDownAccountListInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxDropDownAccountListInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "CopysknFlxOutstanding",
                "width": "65%",
                "zIndex": 1
            }, {}, {});
            flxDropDownAccountListInnerWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblHeaderInfo = new kony.ui.Label({
                "id": "lblHeaderInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknLbl1",
                "text": "Accounts List",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxHeaderSegmentOutstandingClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxHeaderSegmentOutstandingClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "19dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderSegmentOutstandingClose.setDefaultUnit(kony.flex.DP);
            var imgIconClose = new kony.ui.Image2({
                "height": "100%",
                "id": "imgIconClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "ico_close_1.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSegmentOutstandingClose.add(imgIconClose);
            flxHeaderInfo.add(lblHeaderInfo, flxHeaderSegmentOutstandingClose);
            var flxSegmentHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSegmentHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSegmentHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblAccountIDInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblAccountIDInfo",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Account ID",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerIDInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblCustomerIDInfo",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Customer ID",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProductInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblProductInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Product",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCCYInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblCCYInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "CCY",
                "top": "2dp",
                "width": "12%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUsableBalanceInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblUsableBalanceInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Usable Balance",
                "top": "2dp",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentHeaderInfo.add(lblAccountIDInfo, lblCustomerIDInfo, lblProductInfo, lblCCYInfo, lblUsableBalanceInfo);
            var flxAccountsSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountsSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "8dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxAccountsSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var segAccountsList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }],
                "groupCells": false,
                "id": "segAccountsList",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxSegAccNoList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSegAccNoList": "flxSegAccNoList",
                    "lblAccID": "lblAccID",
                    "lblCCY": "lblCCY",
                    "lblCustomerID": "lblCustomerID",
                    "lblMnemonic": "lblMnemonic",
                    "lblProduct": "lblProduct",
                    "lblUsableBalance": "lblUsableBalance",
                    "lblUsableBlance": "lblUsableBlance"
                },
                "width": "100%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountsSegmentWrapper.add(segAccountsList);
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "34dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var lblPaginationInfo = new kony.ui.Label({
                "height": "40dp",
                "id": "lblPaginationInfo",
                "isVisible": false,
                "right": "270dp",
                "skin": "Copysknlbl4",
                "text": "Rows per page :",
                "top": "0dp",
                "width": "103dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtDataPerPageNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "txtDataPerPageNumber",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "placeholder": "Placeholder",
                "right": "220dp",
                "secureTextEntry": false,
                "skin": "CopysknTxt",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var lblSearchPage = new kony.ui.Label({
                "height": "40dp",
                "id": "lblSearchPage",
                "isVisible": true,
                "right": "140dp",
                "skin": "Copysknlbl4",
                "top": "0dp",
                "width": "70dp",
                "zIndex": 50
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPrev = new kony.ui.Button({
                "height": "40dp",
                "id": "btnPrev",
                "isVisible": true,
                "right": "90dp",
                "skin": "CopysknBtnDisable",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 50
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext = new kony.ui.Button({
                "height": "40dp",
                "id": "btnNext",
                "isVisible": true,
                "right": "50dp",
                "skin": "CopysknBtnFocus",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 50
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPagination.add(lblPaginationInfo, txtDataPerPageNumber, lblSearchPage, btnPrev, btnNext);
            var flxDummyBelowOutstandingRepay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDummyBelowOutstandingRepay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDummyBelowOutstandingRepay.setDefaultUnit(kony.flex.DP);
            flxDummyBelowOutstandingRepay.add();
            flxDropDownAccountListInnerWrapper.add(flxHeaderInfo, flxSegmentHeaderInfo, flxAccountsSegmentWrapper, flxPagination, flxDummyBelowOutstandingRepay);
            flxDropDownAccountListOutsideWrapper.add(flxDropDownAccountListInnerWrapper);
            var flxSearchAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxSearchAccount.setDefaultUnit(kony.flex.DP);
            var flxSearchInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxSearchInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "CopysknFlxOutstanding",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxSearchInnerWrapper.setDefaultUnit(kony.flex.DP);
            var popupPayinPayout = new com.konymp.popupPayinPayout.popupPayinPayoutCopy({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "id": "popupPayinPayout",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0a1af6ccf824346",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgClose": {
                        "src": "ico_close_1.png"
                    },
                    "imgSrc": {
                        "src": "search_2.png"
                    },
                    "segPayinPayout": {
                        "data": [{
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDummyBelow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDummyBelow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDummyBelow.setDefaultUnit(kony.flex.DP);
            flxDummyBelow.add();
            flxSearchInnerWrapper.add(popupPayinPayout, flxDummyBelow);
            flxSearchAccount.add(flxSearchInnerWrapper);
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopupCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopupCopy": {
                        "height": "100%",
                        "isVisible": false
                    },
                    "btnCancel": {
                        "width": "165dp"
                    },
                    "btnProceed": {
                        "width": "165dp"
                    },
                    "flxFooter": {
                        "top": "5dp"
                    },
                    "flxPopup": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "centerY": "50%",
                        "left": "viz.val_cleared",
                        "top": "viz.val_cleared",
                        "layoutType": kony.flex.FLOW_VERTICAL
                    },
                    "flxPopupMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "5dp"
                    },
                    "imgClose": {
                        "centerY": "50%",
                        "src": "ico_close_1.png"
                    },
                    "lblPaymentInfo": {
                        "centerY": "50%"
                    },
                    "segOverride": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "data": [{
                            "lblInfo": "Label",
                            "lblSerial": "1"
                        }, {
                            "lblInfo": "Label",
                            "lblSerial": "1"
                        }, {
                            "lblInfo": "Label",
                            "lblSerial": "1"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicatorCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicatorCopy": {
                        "height": "100%",
                        "isVisible": true,
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "cusProgressIndicator": {
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
                "1024": {
                    "FundDetails.flxListBoxSelectedValue": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "120dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.imgListBoxSelectedDropDown": {
                        "left": {
                            "type": "string",
                            "value": "3dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblAccountNumberInfo": {
                        "width": {
                            "type": "string",
                            "value": "130dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblAccountNumberResponse": {
                        "width": {
                            "type": "string",
                            "value": "130dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblAccountServiceInfo": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblAccountTypeInfo": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "150dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblAccountTypeResponse": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "150dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblAmountValueInfo": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "120dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblAmountValueResponse": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "120dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblCurrencyValueInfo": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "80dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblCurrencyValueResponse": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "80dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblListBoxSelectedValue": {
                        "segmentProps": []
                    },
                    "FundDetails.lblMaturityDateInfo": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "130dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblMaturityDateResponse": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "130dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblStartDateInfo": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "120dp"
                        },
                        "segmentProps": []
                    },
                    "FundDetails.lblStartDateResponse": {
                        "left": {
                            "type": "string",
                            "value": "1dp"
                        },
                        "width": {
                            "type": "string",
                            "value": "120dp"
                        },
                        "segmentProps": []
                    },
                    "flxDebitAccountInnerWrapper": {
                        "width": {
                            "type": "string",
                            "value": "80%"
                        },
                        "segmentProps": []
                    }
                },
                "1366": {
                    "flxFundDepositWrapper": {
                        "segmentProps": []
                    }
                }
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "customerDetails.imgCustomerProfilePic": {
                    "src": "pficon_1_1.png"
                },
                "customerDetails.lblAccountOfficerInfo": {
                    "left": "70%"
                },
                "customerDetails.lblAccountOfficerResponse": {
                    "left": "70%"
                },
                "customerDetails.lblEmailInfo": {
                    "left": "25%"
                },
                "customerDetails.lblEmailResponse": {
                    "left": "25%",
                    "width": "170dp"
                },
                "customerDetails.lblInternetBankingInfo": {
                    "left": "70%"
                },
                "customerDetails.lblInternetBankingResponse": {
                    "left": "70%"
                },
                "customerDetails.lblLocationInfo": {
                    "left": "55%"
                },
                "customerDetails.lblLocationResponse": {
                    "left": "55%"
                },
                "customerDetails.lblMobileBankingInfo": {
                    "left": "85%"
                },
                "customerDetails.lblMobileBankingResponse": {
                    "left": "85%"
                },
                "customerDetails.lblPhoneInfo": {
                    "left": "40%"
                },
                "customerDetails.lblPhoneResponse": {
                    "left": "40%"
                },
                "customerDetails.lblProfileInfo": {
                    "left": "55%"
                },
                "customerDetails.lblProfileResponse": {
                    "left": "55%"
                },
                "customerDetails.lblRelationInfo": {
                    "left": "40%"
                },
                "customerDetails.lblRelationResponse": {
                    "left": "40%"
                },
                "MainTabs": {
                    "left": "11dp",
                    "top": "230dp",
                    "width": "94%"
                },
                "MainTabs.flxContainerAccount": {
                    "height": "2dp"
                },
                "MainTabs.flxContainerdeposits": {
                    "height": "5%"
                },
                "ErrorAllert.imgClose": {
                    "src": "ico_close_1.png"
                },
                "FundDetails": {
                    "left": "18dp",
                    "right": "32dp",
                    "top": "292dp",
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "FundDetails.flxDepositsDetailsvalue": {
                    "left": "0dp",
                    "top": "70dp",
                    "width": "100%"
                },
                "FundDetails.flxFundDetailsHeader": {
                    "right": "32dp",
                    "width": "100%"
                },
                "FundDetails.flxListBoxSelectedValue": {
                    "left": "20dp",
                    "width": "140dp"
                },
                "FundDetails.flxontainerLoans": {
                    "bottom": "-1px",
                    "top": "7dp"
                },
                "FundDetails.imgListBoxSelectedDropDown": {
                    "left": "8dp",
                    "src": "drop_down_arrow_1.png"
                },
                "FundDetails.lblAccountNumberInfo": {
                    "left": "27dp",
                    "width": "160dp"
                },
                "FundDetails.lblAccountNumberResponse": {
                    "left": "27dp",
                    "width": "160dp"
                },
                "FundDetails.lblAccountServiceInfo": {
                    "left": "20dp",
                    "text": "Deposit Services"
                },
                "FundDetails.lblAccountTypeInfo": {
                    "left": "20dp",
                    "width": "160dp"
                },
                "FundDetails.lblAccountTypeResponse": {
                    "left": "20dp",
                    "width": "160dp"
                },
                "FundDetails.lblAmountValueInfo": {
                    "left": "20dp",
                    "width": "155dp"
                },
                "FundDetails.lblAmountValueResponse": {
                    "left": "20dp",
                    "text": "83,550.50",
                    "width": "155dp"
                },
                "FundDetails.lblCurrencyValueInfo": {
                    "left": "20dp",
                    "width": "120dp"
                },
                "FundDetails.lblCurrencyValueResponse": {
                    "left": "20dp",
                    "width": "120dp"
                },
                "FundDetails.lblListBoxSelectedValue": {
                    "centerX": "50%",
                    "left": ""
                },
                "FundDetails.lblMaturityDateInfo": {
                    "left": "20dp",
                    "width": "155dp"
                },
                "FundDetails.lblMaturityDateResponse": {
                    "left": "20dp",
                    "width": "155dp"
                },
                "FundDetails.lblStartDateInfo": {
                    "left": "20dp",
                    "width": "150dp"
                },
                "FundDetails.lblStartDateResponse": {
                    "left": "20dp",
                    "width": "150dp"
                },
                "floatLabelText": {
                    "height": "40dp"
                },
                "floatLabelText.lblFloatLabel": {
                    "text": "Debit Account No"
                },
                "popupPayinPayout.imgClose": {
                    "src": "ico_close_1.png"
                },
                "popupPayinPayout.imgSrc": {
                    "src": "search_2.png"
                },
                "popupPayinPayout.segPayinPayout": {
                    "data": [{
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }]
                },
                "SegmentPopup": {
                    "height": "100%"
                },
                "SegmentPopup.btnCancel": {
                    "width": "165dp"
                },
                "SegmentPopup.btnProceed": {
                    "width": "165dp"
                },
                "SegmentPopup.flxFooter": {
                    "top": "5dp"
                },
                "SegmentPopup.flxPopup": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "left": "",
                    "top": "",
                    "layoutType": kony.flex.FLOW_VERTICAL
                },
                "SegmentPopup.flxPopupMessage": {
                    "top": "5dp"
                },
                "SegmentPopup.imgClose": {
                    "centerY": "50%",
                    "src": "ico_close_1.png"
                },
                "SegmentPopup.lblPaymentInfo": {
                    "centerY": "50%"
                },
                "SegmentPopup.segOverride": {
                    "data": [{
                        "lblInfo": "Label",
                        "lblSerial": "1"
                    }, {
                        "lblInfo": "Label",
                        "lblSerial": "1"
                    }, {
                        "lblInfo": "Label",
                        "lblSerial": "1"
                    }]
                },
                "ProgressIndicator": {
                    "height": "100%",
                    "left": "0dp",
                    "top": "0dp"
                },
                "ProgressIndicator.cusProgressIndicator": {
                    "zIndex": 10
                }
            }
            this.add(uuxNavigationRail, flxMainContainer, flxNavigationRail, flxDropDownAccountListOutsideWrapper, flxSearchAccount, SegmentPopup, ProgressIndicator);
        };
        return [{
            "addWidgets": addWidgetsfrmFundDeposit,
            "enabledForIdleTimeout": false,
            "id": "frmFundDeposit",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});