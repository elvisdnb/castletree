define("flxServiceType", function() {
    return function(controller) {
        var flxServiceType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxServiceType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxServiceType.setDefaultUnit(kony.flex.DP);
        var flxIndicator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45%",
            "id": "flxIndicator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "flxSelectedSkin",
            "width": "2dp",
            "zIndex": 1
        }, {}, {});
        flxIndicator.setDefaultUnit(kony.flex.DP);
        flxIndicator.add();
        var lblServiceType = new kony.ui.Button({
            "focusSkin": "btnsknSelectedType",
            "height": "100%",
            "id": "lblServiceType",
            "isVisible": true,
            "left": "10%",
            "onClick": controller.AS_Button_h6ae5433157f408e8322926ad9b86c64,
            "skin": "btnsknSelectedType",
            "text": "Button",
            "top": "0dp",
            "width": "90%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServiceType.add(flxIndicator, lblServiceType);
        return flxServiceType;
    }
})