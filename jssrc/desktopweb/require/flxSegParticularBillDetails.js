define("flxSegParticularBillDetails", function() {
    return function(controller) {
        var flxSegParticularBillDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSegParticularBillDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxSegParticularBillDetails.setDefaultUnit(kony.flex.DP);
        var lblTypeInfo = new kony.ui.Label({
            "height": "40dp",
            "id": "lblTypeInfo",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblBg000000px14",
            "text": "Label",
            "width": "38%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCurrency = new kony.ui.Label({
            "height": "40dp",
            "id": "lblCurrency",
            "isVisible": true,
            "left": "24%",
            "skin": "sknLbl14px005096BG",
            "text": "Label",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmount = new kony.ui.Label({
            "height": "40dp",
            "id": "lblAmount",
            "isVisible": true,
            "left": "5%",
            "skin": "sknLbl14px005096BG",
            "text": "Label",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSegParticularBillDetails.add(lblTypeInfo, lblCurrency, lblAmount);
        return flxSegParticularBillDetails;
    }
})