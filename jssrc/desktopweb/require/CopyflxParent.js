define("CopyflxParent", function() {
    return function(controller) {
        var CopyflxParent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "CopyflxParent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox3",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        CopyflxParent.setDefaultUnit(kony.flex.DP);
        var flxLine = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLine",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "sknVerticalLine",
            "top": "0dp",
            "width": "3dp",
            "zIndex": 1
        }, {}, {});
        flxLine.setDefaultUnit(kony.flex.DP);
        flxLine.add();
        var flxRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox4",
            "top": "0dp",
            "width": "99%"
        }, {}, {});
        flxRow.setDefaultUnit(kony.flex.DP);
        var lblAddress1 = new kony.ui.Label({
            "id": "lblAddress1",
            "isVisible": false,
            "left": "2%",
            "skin": "sknUnselected",
            "text": "Dashboard",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAddress = new kony.ui.RichText({
            "id": "lblAddress",
            "isVisible": true,
            "left": "1%",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0be0eee345d9a4b",
            "text": "RichText",
            "top": "10dp",
            "width": "97%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow.add(lblAddress1, lblAddress);
        var lblLine = new kony.ui.Label({
            "bottom": "2dp",
            "height": "1dp",
            "id": "lblLine",
            "isVisible": false,
            "left": "0dp",
            "skin": "CopyCopydefLabel4",
            "text": "label",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var FlexContainer0c2ca7dc5c88140 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10dp",
            "id": "FlexContainer0c2ca7dc5c88140",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        FlexContainer0c2ca7dc5c88140.setDefaultUnit(kony.flex.DP);
        FlexContainer0c2ca7dc5c88140.add();
        CopyflxParent.add(flxLine, flxRow, lblLine, FlexContainer0c2ca7dc5c88140);
        return CopyflxParent;
    }
})