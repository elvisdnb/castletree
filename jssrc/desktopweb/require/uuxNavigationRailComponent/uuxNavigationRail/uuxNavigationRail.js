define(function() {
    return function(controller) {
        var uuxNavigationRail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "uuxNavigationRail",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyflxNavRailGradientSkin0j4b7bcb578604a",
            "top": "0%",
            "width": "120dp",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "uuxNavigationRail"), extendConfig({}, controller.args[1], "uuxNavigationRail"), extendConfig({}, controller.args[2], "uuxNavigationRail"));
        uuxNavigationRail.setDefaultUnit(kony.flex.DP);
        var TPW0bbc8a8d4b8b44b = new kony.ui.CustomWidget(extendConfig({
            "id": "TPW0bbc8a8d4b8b44b",
            "isVisible": false,
            "left": "0",
            "top": "0",
            "width": "100%",
            "height": "100%",
            "clipBounds": true
        }, controller.args[0], "TPW0bbc8a8d4b8b44b"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "TPW0bbc8a8d4b8b44b"), extendConfig({
            "widgetName": "uuxNavigationRail",
            "bottomBtnIcon": "account_circle",
            "bottomBtnItems": "[{\"label\":\"Settings\"},{\"label\":\"Last Updated\"},{\"label\":\"Logout\"}]",
            "hideBottomBtn": true,
            "logo": "temenosLogo",
            "logoAlt": "Temenos logo",
            "menuItems": "[{ \"icon\": \"home\", \"label\": \"Dashboard\" }, { \"icon\": \"search\", \"label\": \"Search Customer\" }]",
            "positionDescription": {
                "optionsList": ["left", "right"],
                "selectedValue": "left"
            }
        }, controller.args[2], "TPW0bbc8a8d4b8b44b"));
        var flxNavRail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "96%",
            "id": "flxNavRail",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "-20%",
            "isModalContainer": false,
            "skin": "flxNavRailGradientSkin",
            "top": "2%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNavRail"), extendConfig({}, controller.args[1], "flxNavRail"), extendConfig({}, controller.args[2], "flxNavRail"));
        flxNavRail.setDefaultUnit(kony.flex.DP);
        var flxTemenosLogo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "58%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxTemenosLogo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "iconRoundFlxBgDarkBlue",
            "top": "30dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxTemenosLogo"), extendConfig({}, controller.args[1], "flxTemenosLogo"), extendConfig({}, controller.args[2], "flxTemenosLogo"));
        flxTemenosLogo.setDefaultUnit(kony.flex.DP);
        var lblLogo = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblLogo",
            "isVisible": false,
            "skin": "castleIconTemLightest",
            "text": "T",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLogo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLogo"), extendConfig({}, controller.args[2], "lblLogo"));
        var imgLogo = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "24dp",
            "id": "imgLogo",
            "isVisible": true,
            "skin": "slImage",
            "src": "temenlogo24.png",
            "width": "24dp",
            "zIndex": 1
        }, controller.args[0], "imgLogo"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLogo"), extendConfig({}, controller.args[2], "imgLogo"));
        flxTemenosLogo.add(lblLogo, imgLogo);
        var flxHome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "58%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxHome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "120dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxHome"), extendConfig({}, controller.args[1], "flxHome"), extendConfig({}, controller.args[2], "flxHome"));
        flxHome.setDefaultUnit(kony.flex.DP);
        var lblHome = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblHome",
            "isVisible": true,
            "skin": "castleIconTemLightest",
            "text": "F",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHome"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHome"), extendConfig({}, controller.args[2], "lblHome"));
        flxHome.add(lblHome);
        var flxSearchCust = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "58%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSearchCust",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "210dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxSearchCust"), extendConfig({}, controller.args[1], "flxSearchCust"), extendConfig({}, controller.args[2], "flxSearchCust"));
        flxSearchCust.setDefaultUnit(kony.flex.DP);
        var lblFlxSearchCust = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblFlxSearchCust",
            "isVisible": true,
            "skin": "castleIconTemLightest",
            "text": "k",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblFlxSearchCust"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlxSearchCust"), extendConfig({}, controller.args[2], "lblFlxSearchCust"));
        flxSearchCust.add(lblFlxSearchCust);
        var flxExplorer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "150dp",
            "centerX": "58%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxExplorer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_onClickExplorer_j4782f367b454137acc061034476f68e,
            "skin": "slFbox",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "flxExplorer"), extendConfig({}, controller.args[1], "flxExplorer"), extendConfig({}, controller.args[2], "flxExplorer"));
        flxExplorer.setDefaultUnit(kony.flex.DP);
        var lblExplorer = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblExplorer",
            "isVisible": true,
            "skin": "castleIconTemLightest",
            "text": "n",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblExplorer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblExplorer"), extendConfig({}, controller.args[2], "lblExplorer"));
        var btnExplorer = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0dc9b48cc42c943",
            "height": "100%",
            "id": "btnExplorer",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_onClickBtnExplorer_ie224d39a5354ab4b4aa1a2990e66fde,
            "skin": "CopydefBtnNormal0dc9b48cc42c943",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnExplorer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnExplorer"), extendConfig({}, controller.args[2], "btnExplorer"));
        flxExplorer.add(lblExplorer, btnExplorer);
        var flxHelp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "100dp",
            "centerX": "58%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxHelp",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_onClickHelp_e8a008cc71a04cb1b2a7356256e45b60,
            "skin": "slFbox",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "flxHelp"), extendConfig({}, controller.args[1], "flxHelp"), extendConfig({}, controller.args[2], "flxHelp"));
        flxHelp.setDefaultUnit(kony.flex.DP);
        var lblHelp = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblHelp",
            "isVisible": true,
            "skin": "castleIconTemLightest",
            "text": "o",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHelp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHelp"), extendConfig({}, controller.args[2], "lblHelp"));
        var btnHelp = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0f0b11c91229e40",
            "height": "100%",
            "id": "btnHelp",
            "isVisible": true,
            "left": "0dp",
            "onClick": controller.AS_onClickBtnHelp_b3c0ba7e1e86462a8ceedab4de9fa748,
            "skin": "CopydefBtnNormal0f0b11c91229e40",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnHelp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnHelp"), extendConfig({}, controller.args[2], "btnHelp"));
        flxHelp.add(lblHelp, btnHelp);
        var FlxLogout = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "50dp",
            "centerX": "58%",
            "clipBounds": true,
            "height": "45dp",
            "id": "FlxLogout",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "28dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "FlxLogout"), extendConfig({}, controller.args[1], "FlxLogout"), extendConfig({}, controller.args[2], "FlxLogout"));
        FlxLogout.setDefaultUnit(kony.flex.DP);
        var BtnLogout = new kony.ui.Button(extendConfig({
            "focusSkin": "CopydefBtnNormal0j0db067fe96345",
            "height": "100%",
            "id": "BtnLogout",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefBtnNormal0j0db067fe96345",
            "text": "i",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "BtnLogout"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "BtnLogout"), extendConfig({
            "hoverSkin": "CopydefBtnNormal0j0db067fe96345"
        }, controller.args[2], "BtnLogout"));
        FlxLogout.add(BtnLogout);
        flxNavRail.add(flxTemenosLogo, flxHome, flxSearchCust, flxExplorer, flxHelp, FlxLogout);
        uuxNavigationRail.add(TPW0bbc8a8d4b8b44b, flxNavRail);
        uuxNavigationRail.compInstData = {}
        return uuxNavigationRail;
    }
})