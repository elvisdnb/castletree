define("uuxNavigationRailComponent/uuxNavigationRail/useruuxNavigationRailController", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {
            this.bindEvents();
        },
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {},
        bindEvents: function() {
            this.view.flxHome.onTouchEnd = this.navigateToDashboard;
            this.view.flxSearchCust.onTouchEnd = this.navigateToSearchCustomer;
            this.view.FlxLogout.onTouchEnd = this.navigateLogin;
        },
        navigateToDashboard: function() {
            var navToDashboard = new kony.mvc.Navigation("frmDashboardLending");
            var navObjet = {};
            navObjet.isPopupShow = "false"; // TODO added newly to dismiss success popup
            navToDashboard.navigate(navObjet);
        },
        navigateToSearchCustomer: function() {
            var navToSearchCustomer = new kony.mvc.Navigation("frmSearchCustomer");
            var navObjet = {};
            navToSearchCustomer.navigate(navObjet);
        },
        navigateLogin: function() {
            var navToSearchCustomer = new kony.mvc.Navigation("frmLogin");
            var navObjet = {};
            navToSearchCustomer.navigate(navObjet);
        },
    };
});
define("uuxNavigationRailComponent/uuxNavigationRail/uuxNavigationRailControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_a61f899003eb4de69bf0066e57d562a9: function AS_FlexContainer_a61f899003eb4de69bf0066e57d562a9(eventobject) {
        var self = this;
        this.view.isVisible = false;
    }
});
define("uuxNavigationRailComponent/uuxNavigationRail/uuxNavigationRailController", ["uuxNavigationRailComponent/uuxNavigationRail/useruuxNavigationRailController", "uuxNavigationRailComponent/uuxNavigationRail/uuxNavigationRailControllerActions"], function() {
    var controller = require("uuxNavigationRailComponent/uuxNavigationRail/useruuxNavigationRailController");
    var actions = require("uuxNavigationRailComponent/uuxNavigationRail/uuxNavigationRailControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "bottomBtnIcon", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.bottomBtnIcon = val;
        });
        defineGetter(this, "bottomBtnIcon", function() {
            return this.view.TPW0bbc8a8d4b8b44b.bottomBtnIcon;
        });
        defineSetter(this, "bottomBtnItems", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.bottomBtnItems = val;
        });
        defineGetter(this, "bottomBtnItems", function() {
            return this.view.TPW0bbc8a8d4b8b44b.bottomBtnItems;
        });
        defineSetter(this, "hideBottomBtn", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.hideBottomBtn = val;
        });
        defineGetter(this, "hideBottomBtn", function() {
            return this.view.TPW0bbc8a8d4b8b44b.hideBottomBtn;
        });
        defineSetter(this, "logo", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.logo = val;
        });
        defineGetter(this, "logo", function() {
            return this.view.TPW0bbc8a8d4b8b44b.logo;
        });
        defineSetter(this, "logoAlt", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.logoAlt = val;
        });
        defineGetter(this, "logoAlt", function() {
            return this.view.TPW0bbc8a8d4b8b44b.logoAlt;
        });
        defineSetter(this, "menuItems", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.menuItems = val;
        });
        defineGetter(this, "menuItems", function() {
            return this.view.TPW0bbc8a8d4b8b44b.menuItems;
        });
        defineSetter(this, "positionDescription", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.positionDescription = val;
        });
        defineGetter(this, "positionDescription", function() {
            return this.view.TPW0bbc8a8d4b8b44b.positionDescription;
        });
        defineSetter(this, "skin1", function(val) {
            this.view.btnExplorer.skin = val;
        });
        defineGetter(this, "skin1", function() {
            return this.view.btnExplorer.skin;
        });
        defineSetter(this, "skin2", function(val) {
            this.view.btnHelp.skin = val;
        });
        defineGetter(this, "skin2", function() {
            return this.view.btnHelp.skin;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    controller.AS_onClickExplorer_j4782f367b454137acc061034476f68e = function() {
        if (this.onClickExplorer) {
            this.onClickExplorer.apply(this, arguments);
        } else if (this.AS_FlexContainer_a61f899003eb4de69bf0066e57d562a9) {
            this.AS_FlexContainer_a61f899003eb4de69bf0066e57d562a9.apply(this, arguments);
        }
    }
    controller.AS_onClickBtnExplorer_ie224d39a5354ab4b4aa1a2990e66fde = function() {
        if (this.onClickBtnExplorer) {
            this.onClickBtnExplorer.apply(this, arguments);
        }
    }
    controller.AS_onClickHelp_e8a008cc71a04cb1b2a7356256e45b60 = function() {
        if (this.onClickHelp) {
            this.onClickHelp.apply(this, arguments);
        }
    }
    controller.AS_onClickBtnHelp_b3c0ba7e1e86462a8ceedab4de9fa748 = function() {
        if (this.onClickBtnHelp) {
            this.onClickBtnHelp.apply(this, arguments);
        }
    }
    return controller;
});
