define(function() {
    return function(controller) {
        var uuxNavigationRailWeb = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "uuxNavigationRailWeb",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFboxf",
            "top": "0%",
            "width": "120dp",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, controller.args[0], "uuxNavigationRailWeb"), extendConfig({}, controller.args[1], "uuxNavigationRailWeb"), extendConfig({}, controller.args[2], "uuxNavigationRailWeb"));
        uuxNavigationRailWeb.setDefaultUnit(kony.flex.DP);
        var TPW0bbc8a8d4b8b44b = new kony.ui.CustomWidget(extendConfig({
            "id": "TPW0bbc8a8d4b8b44b",
            "isVisible": false,
            "left": "0",
            "top": "0",
            "width": "100%",
            "height": "100%",
            "clipBounds": true
        }, controller.args[0], "TPW0bbc8a8d4b8b44b"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "TPW0bbc8a8d4b8b44b"), extendConfig({
            "widgetName": "uuxNavigationRail",
            "bottomBtnIcon": "account_circle",
            "bottomBtnItems": "[{\"label\":\"Settings\"},{\"label\":\"Last Updated\"},{\"label\":\"Logout\"}]",
            "hideBottomBtn": true,
            "logo": "temenosLogo",
            "logoAlt": "Temenos logo",
            "menuItems": "[{ \"icon\": \"home\", \"label\": \"Dashboard\" }, { \"icon\": \"search\", \"label\": \"Search Customer\" }]",
            "positionDescription": {
                "optionsList": ["left", "right"],
                "selectedValue": "left"
            }
        }, controller.args[2], "TPW0bbc8a8d4b8b44b"));
        var flxNavRail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "96%",
            "id": "flxNavRail",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "-20%",
            "isModalContainer": false,
            "skin": "CopyflxNavRailGradientSkin",
            "top": "2%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNavRail"), extendConfig({}, controller.args[1], "flxNavRail"), extendConfig({}, controller.args[2], "flxNavRail"));
        flxNavRail.setDefaultUnit(kony.flex.DP);
        var flxTemenosLogo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "58%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxTemenosLogo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyiconRoundFlxBgDarkBlue",
            "top": "30dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxTemenosLogo"), extendConfig({}, controller.args[1], "flxTemenosLogo"), extendConfig({}, controller.args[2], "flxTemenosLogo"));
        flxTemenosLogo.setDefaultUnit(kony.flex.DP);
        var lblLogo = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblLogo",
            "isVisible": false,
            "skin": "CopycastleIconTemLightest",
            "text": "T",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLogo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLogo"), extendConfig({}, controller.args[2], "lblLogo"));
        var imgLogo = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "24dp",
            "id": "imgLogo",
            "isVisible": true,
            "skin": "slImage",
            "src": "temenlogo24_1.png",
            "width": "24dp",
            "zIndex": 1
        }, controller.args[0], "imgLogo"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLogo"), extendConfig({}, controller.args[2], "imgLogo"));
        flxTemenosLogo.add(lblLogo, imgLogo);
        var flxHome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "58%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxHome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "120dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxHome"), extendConfig({}, controller.args[1], "flxHome"), extendConfig({}, controller.args[2], "flxHome"));
        flxHome.setDefaultUnit(kony.flex.DP);
        var lblHome = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblHome",
            "isVisible": true,
            "skin": "CopycastleIconTemLightest",
            "text": "F",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHome"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHome"), extendConfig({}, controller.args[2], "lblHome"));
        flxHome.add(lblHome);
        var flxSearchCust = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "58%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSearchCust",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "210dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxSearchCust"), extendConfig({}, controller.args[1], "flxSearchCust"), extendConfig({}, controller.args[2], "flxSearchCust"));
        flxSearchCust.setDefaultUnit(kony.flex.DP);
        var lblFlxSearchCust = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblFlxSearchCust",
            "isVisible": true,
            "skin": "CopycastleIconTemLightest",
            "text": "k",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblFlxSearchCust"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlxSearchCust"), extendConfig({}, controller.args[2], "lblFlxSearchCust"));
        flxSearchCust.add(lblFlxSearchCust);
        var FlxLogout = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "50dp",
            "centerX": "58%",
            "clipBounds": true,
            "height": "40dp",
            "id": "FlxLogout",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "28dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "FlxLogout"), extendConfig({}, controller.args[1], "FlxLogout"), extendConfig({}, controller.args[2], "FlxLogout"));
        FlxLogout.setDefaultUnit(kony.flex.DP);
        var BtnLogout = new kony.ui.Button(extendConfig({
            "focusSkin": "CopyCopydefBtnNormal",
            "height": "100%",
            "id": "BtnLogout",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyCopydefBtnNormal",
            "text": "i",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "BtnLogout"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "BtnLogout"), extendConfig({
            "hoverSkin": "CopyCopydefBtnNormal"
        }, controller.args[2], "BtnLogout"));
        FlxLogout.add(BtnLogout);
        flxNavRail.add(flxTemenosLogo, flxHome, flxSearchCust, FlxLogout);
        uuxNavigationRailWeb.add(TPW0bbc8a8d4b8b44b, flxNavRail);
        uuxNavigationRailWeb.compInstData = {}
        return uuxNavigationRailWeb;
    }
})