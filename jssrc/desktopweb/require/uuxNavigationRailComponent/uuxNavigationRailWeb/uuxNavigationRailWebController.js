define("uuxNavigationRailComponent/uuxNavigationRailWeb/useruuxNavigationRailWebController", function() {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {
            this.bindEvents();
        },
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {},
        bindEvents: function() {
            this.view.flxHome.onTouchEnd = this.navigateToDashboard;
            this.view.flxSearchCust.onTouchEnd = this.navigateToSearchCustomer;
            this.view.FlxLogout.onTouchEnd = this.navigateLogin;
        },
        navigateToDashboard: function() {
            var navToDashboard = new kony.mvc.Navigation("frmDashboard");
            var navObjet = {};
            navObjet.isPopupShow = "false"; // TODO added newly to dismiss success popup
            navToDashboard.navigate(navObjet);
        },
        navigateToSearchCustomer: function() {
            var navToSearchCustomer = new kony.mvc.Navigation("frmSearchCustomer");
            var navObjet = {};
            navToSearchCustomer.navigate(navObjet);
        },
        navigateLogin: function() {
            var navToSearchCustomer = new kony.mvc.Navigation("frmLogin");
            var navObjet = {};
            navToSearchCustomer.navigate(navObjet);
        }
    };
});
define("uuxNavigationRailComponent/uuxNavigationRailWeb/uuxNavigationRailWebControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("uuxNavigationRailComponent/uuxNavigationRailWeb/uuxNavigationRailWebController", ["uuxNavigationRailComponent/uuxNavigationRailWeb/useruuxNavigationRailWebController", "uuxNavigationRailComponent/uuxNavigationRailWeb/uuxNavigationRailWebControllerActions"], function() {
    var controller = require("uuxNavigationRailComponent/uuxNavigationRailWeb/useruuxNavigationRailWebController");
    var actions = require("uuxNavigationRailComponent/uuxNavigationRailWeb/uuxNavigationRailWebControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        defineSetter(this, "bottomBtnIcon", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.bottomBtnIcon = val;
        });
        defineGetter(this, "bottomBtnIcon", function() {
            return this.view.TPW0bbc8a8d4b8b44b.bottomBtnIcon;
        });
        defineSetter(this, "bottomBtnItems", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.bottomBtnItems = val;
        });
        defineGetter(this, "bottomBtnItems", function() {
            return this.view.TPW0bbc8a8d4b8b44b.bottomBtnItems;
        });
        defineSetter(this, "hideBottomBtn", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.hideBottomBtn = val;
        });
        defineGetter(this, "hideBottomBtn", function() {
            return this.view.TPW0bbc8a8d4b8b44b.hideBottomBtn;
        });
        defineSetter(this, "logo", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.logo = val;
        });
        defineGetter(this, "logo", function() {
            return this.view.TPW0bbc8a8d4b8b44b.logo;
        });
        defineSetter(this, "logoAlt", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.logoAlt = val;
        });
        defineGetter(this, "logoAlt", function() {
            return this.view.TPW0bbc8a8d4b8b44b.logoAlt;
        });
        defineSetter(this, "menuItems", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.menuItems = val;
        });
        defineGetter(this, "menuItems", function() {
            return this.view.TPW0bbc8a8d4b8b44b.menuItems;
        });
        defineSetter(this, "positionDescription", function(val) {
            this.view.TPW0bbc8a8d4b8b44b.positionDescription = val;
        });
        defineGetter(this, "positionDescription", function() {
            return this.view.TPW0bbc8a8d4b8b44b.positionDescription;
        });
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});
