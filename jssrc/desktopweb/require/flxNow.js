define("flxNow", function() {
    return function(controller) {
        var flxNow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxNow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxNow.setDefaultUnit(kony.flex.DP);
        var FlexGroup0b883b054ed3940 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "FlexGroup0b883b054ed3940",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        FlexGroup0b883b054ed3940.setDefaultUnit(kony.flex.DP);
        var lblAcNum = new kony.ui.Label({
            "height": "100%",
            "id": "lblAcNum",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoansHeading",
            "text": "Account Number",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnsort1 = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "100%",
            "id": "btnsort1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnSort",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "displayText": true,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAcType = new kony.ui.Label({
            "height": "100%",
            "id": "lblAcType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoansHeading",
            "text": "Account Type",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnsort2 = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "100%",
            "id": "btnsort2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnSort",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "displayText": true,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCCY = new kony.ui.Label({
            "height": "100%",
            "id": "lblCCY",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoansHeading",
            "text": "CCY",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnsort3 = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "100%",
            "id": "btnsort3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnSort",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "displayText": true,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblClearBal = new kony.ui.Label({
            "height": "100%",
            "id": "lblClearBal",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoansHeading",
            "text": "Cleared Balance",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnsort4 = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "100%",
            "id": "btnsort4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnSort",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "displayText": true,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLockedAc = new kony.ui.Label({
            "height": "100%",
            "id": "lblLockedAc",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoansHeading",
            "text": "Locked Amount",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnsort5 = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "100%",
            "id": "btnsort5",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnSort",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "displayText": true,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAvlBal = new kony.ui.Label({
            "height": "100%",
            "id": "lblAvlBal",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoansHeading",
            "text": "Available Limit ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnsort6 = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "100%",
            "id": "btnsort6",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnSort",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "displayText": true,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAcServices = new kony.ui.Label({
            "height": "100%",
            "id": "lblAcServices",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoansHeading",
            "text": "Account Services ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnsort7 = new kony.ui.Button({
            "focusSkin": "defBtnFocus",
            "height": "100%",
            "id": "btnsort7",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnSort",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
            "displayText": true,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        FlexGroup0b883b054ed3940.add(lblAcNum, btnsort1, lblAcType, btnsort2, lblCCY, btnsort3, lblClearBal, btnsort4, lblLockedAc, btnsort5, lblAvlBal, btnsort6, lblAcServices, btnsort7);
        var FlexContainer0gedc2f7c182c4c = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "centerX": "50%",
            "clipBounds": true,
            "height": "1dp",
            "id": "FlexContainer0gedc2f7c182c4c",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknLine",
            "width": "98%",
            "zIndex": 1
        }, {}, {});
        FlexContainer0gedc2f7c182c4c.setDefaultUnit(kony.flex.DP);
        FlexContainer0gedc2f7c182c4c.add();
        flxNow.add(FlexGroup0b883b054ed3940, FlexContainer0gedc2f7c182c4c);
        return flxNow;
    }
})