define("userfrmCustomerLoanIncreaseAmtController", {
    _arrangementId: "",
    _loanObj: {},
    _scheduledRevisedPayment: [],
    dataPerPageNumber: 0,
    _navObj: {},
    currentOverride: [],
    currentCommittment: "",
    //Type your controller code here 
    onNavigate: function(navObj) {
        this._navObj = navObj;
        this.currentOverride = [];
        this.currentCommittment = "";
        /* if(navObj.messageInfo !== ""){
          this.view.flxHeaderMenu.isVisible= false;
          this.view.flxMessageInfo.isVisible = true;
          this.view.lblMessageWithReferenceNumber.text= navObj.messageInfo;
        }else{
          this.view.flxMessageInfo.isVisible = false;
          this.view.flxHeaderMenu.isVisible = true;
          this.view.ErrorAllert.isVisible = false;

        }*/
        this.view.amtIncreaseFloatTxt.value = "";
        //this.view.amtRevisedommitment1.txtFloatText.text="";
        // this.view.amtRevisedommitment1.resetReadOnlyText();
        this.view.SegmentPopup.isVisible = false;
        this.view.ErrorAllert.isVisible = false;
        this.view.lblRevisedPaymentConfirmation.isVisible = false;
        this.view.lblErrorMsg.isVisible = false;
        this.bindEvents();
        //nav obj has the selected loan details
        this._loanObj = {};
        this.setLoanHeaderDetails(navObj);
        if (navObj && navObj.lblArrangementId) {
            this._arrangementId = navObj.lblArrangementId; //"AA20079J0GJ0";
            this.view.amtCurrentCommitment1.currencyCode = this._loanObj.currencyId;
            this.view.amtIncreaseFloatTxt.currencyCode = this._loanObj.currencyId;
            this.view.amtRevisedommitment1.currencyCode = this._loanObj.currencyId;
            this.fetchCurrentCommitment(this._arrangementId);
        }
        //this.pauseNavigation();
    },
    bindEvents: function() {
        this.initializeCustomComponent();
        this.view.postShow = this.postShowFunctionCall;
        this.view.RevisedPaymentSchedule.imgClose.onTouchEnd = this.closeRevisedPaymentPopup;
        this.view.lblRevisedPaymenySchedule.onTouchStart = this.onClickRevisedPaymentSchedule;
        //this.view.amtIncreaseFloatTxt.txtFloatText.onTouchEnd = this.updateRevisedPaymentAmt;
        //this.view.amtIncreaseFloatTxt.txtFloatText.onDone = this.updateRevisedPaymentAmt;
        //this.view.amtIncreaseFloatTxt.txtFloatText.onTextChange = this.onTextChangeOfImpl;
        //this.view.amtIncreaseFloatTxt.txtFloatText.onEndEditing = this.amtIncreaseChange;
        this.view.RevisedPaymentSchedule.btnNext.onTouchEnd = this.showChangeRequestListData;
        this.view.RevisedPaymentSchedule.btnPrev.onTouchEnd = this.showChangeRequestListData;
        this.view.imgHeaderClose.onTouchEnd = this.onCloseMsgHeader;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.onCloseErrorAllert;
        this.view.Popup.imgClose.onTouchEnd = this.closeOverridePopup;
        this.view.SegmentPopup.btnProceed.onClick = this.onClickSubmitData;
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.amtIncreaseFloatTxt.onChangeEvent = this.updateRevisedPaymentAmt;
        this.view.lblBreadBoardHomeValue.onTouchEnd = this.navigateHome;
        this.view.lblBreadBoardOverviewvalue.onTouchEnd = this.navigateOverview;
        //this.view.txtpagenumber.value ="6";
    },
    postShowFunctionCall: function() {
        try {
            this.dataPerPageNumber = 6; // this.view.txtpagenumber.text;
            this.view.btnConfirm.onclickEvent = this.validateAndSubmitData;
            this.view.btnCancel.onclickEvent = this.onClickCancel;
            //this.view.amtRevisedommitment1.lblFloatLabel.skin=sknFloatLblPreInput;
        } catch (err) {
            alert("Error in CustomerLoanIncreaseCommitmentController postFunctionCall:::" + err);
        }
    },
    setLoanHeaderDetails: function(loanSegRow) {
        this._loanObj.currencyId = loanSegRow.lblCCY;
        this._loanObj.arrangementId = loanSegRow.lblArrangementId;
        this._loanObj.customerId = loanSegRow.customerId;
        this._loanObj.produtId = loanSegRow.loanProduct;
        this.view.loanDetails.lblAccountNumberResponse.text = loanSegRow.lblHeaderAN;
        this.view.loanDetails.lblLoanTypeResponse.text = loanSegRow.lblHeaderAccountType; //"2345678";
        this.view.loanDetails.lblCurrencyValueResponse.text = loanSegRow.lblCCY;
        this.view.loanDetails.lblAmountValueResponse.text = loanSegRow.lblClearedBalance;
        this.view.loanDetails.lblStartDateResponse.text = loanSegRow.lblAvailableLimit;
        this.view.loanDetails.lblMaturityDateResponse.text = loanSegRow.lblHdr;
    },
    initializeCustomComponent: function() {
        // this.view.amtCurrentCommitment1.txtFloatText.setEnabled(false);
        // this.view.amtRevisedommitment1.txtFloatText.setEnabled(false);
        //this.view.amtRevisedommitment1.lblFloatLabel.skin=sknFloatLblPreInput;
    },
    closeSegmentOverrite: function() {
        this.view.SegmentPopup.isVisible = false;
        this.currentOverride = {};
        this.view.forceLayout();
    },
    updateRevisedPaymentAmt: function() {
        //kony.print("text:::::::"+this.view.amtIncreaseFloatTxt.t);
        var incAmt = this.view.amtIncreaseFloatTxt.value;
        if (incAmt !== "") {
            //var curCommitment=this.view.amtCurrentCommitment1.value;
            var result = parseFloat(this.currentCommittment) + parseFloat(incAmt);
            result = Application.numberFormater.convertForDispaly(result, this._loanObj.currencyId);
            this.view.amtRevisedommitment1.value = result;
            // result=result.toLocaleString(undefined, {  minimumFractionDigits: 2,maximumFractionDigits: 2}); 
            //DV : why this statement is needed here ?
            //Shall we do at component level ??
            //this.view.amtIncreaseFloatTxt.txtFloatText.text=parseFloat(incAmt).toFixed(2);
            //this.view.amtRevisedommitment1.animateComponent();
            //this.view.amtRevisedommitment1.txtFloatText.left="0px";
            //this.view.amtRevisedommitment1.txtFloatText.text = result.toString();
            //this.view.amtRevisedommitment1.setReadOnlyText(result.toString())
        } else {
            this.view.amtRevisedommitment1.value = "";
            //this.view.amtRevisedommitment1.txtFloatText.text="";
            //this.view.amtRevisedommitment1.lblFloatLabel.skin=sknFloatLblPreInput;
            // this.view.amtRevisedommitment1.resetReadOnlyText();
        }
    },
    amtIncreaseChange: function() {
        kony.print("enter in change::::::");
        var curCommitment = this.view.amtIncreaseFloatTxt.value;
        //  curCommitment=parseFloat(curCommitment);
        // this.view .amtIncreaseFloatTxt.txtFloatText.text=parseFloat(curCommitment).toLocaleString(undefined, {  minimumFractionDigits: 2,maximumFractionDigits: 2}); 
    },
    closeRevisedPaymentPopup: function() {
        this.view.RevisedPaymentSchedule.isVisible = false;
        this.view.flxMainContainer.isVisible = true;
        this.currentOverride = {};
    },
    onCloseErrorAllert: function() {
        this.view.ErrorAllert.isVisible = false;
        this.view.flxHeaderMenu.isVisible = true;
    },
    closeOverridePopup: function() {
        this.view.Popup.isVisible = false;
    },
    isMandatoryFieldsPass: function() {
        var incAmt = this.view.amtIncreaseFloatTxt.value;
        if (null === incAmt || "" === incAmt) {
            // this.view.amtIncreaseFloatTxt.setErrorModeSkin();
            this.view.lblRevisedPaymentConfirmation.isVisible = false;
            this.view.lblErrorMsg.isVisible = true;
            return false;
            //highlight the validation fail skin
        }
        return true;
    },
    validateAndSubmitData: function() {
        var isValidData = this.validateData();
        if (isValidData === 0) {
            if (this.isMandatoryFieldsPass()) {
                this.onClickSubmitData();
            }
        } else {
            this.view.lblErrorMsg.isVisible = true;
            //do nothing..As the validation highlights the fields
        }
    },
    onClickSubmitData: function() {
        try {
            this.view.lblErrorMsg.isVisible = false;
            this.view.lblRevisedPaymentConfirmation.isVisible = false;
            var revAmt = this.view.amtRevisedommitment1.value;
            if (revAmt !== "") {
                var increaseAmt = this.view.amtIncreaseFloatTxt.value;
                increaseAmt = increaseAmt.replace(/,/g, "");
                this.view.ProgressIndicator.isVisible = true;
                var serviceName = "LendingNew";
                var operationName = "postIncreaseCommitment";
                var headers = "";
                var inputParams = {
                    "arrangmentId": this._arrangementId,
                    "changeAmount": increaseAmt
                };
                inputParams.overrideDetails = this.currentOverride;
                MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunction90res, this.failureCallBackfunction);
            } else {
                this.view.lblRevisedPaymentConfirmation.isVisible = false;
            }
            //         var res = {} ;
            //         res.header = {};
            //         res.header["aaaId"] = "1234567";
            //         this.successCallBackfunction90res(res);
        } catch (err) {
            kony.print("Error in CustomerLoanIncreaseCommitment onClickSubmitData:::" + err);
        }
    },
    successCallBackfunction90res: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        this.currentOverride = [];
        //var selectedSegData=this._navObj;
        /*if(res.override !== ""){
          //this.view.Popup.isVisible=true;
          this.view.SegmentPopup.isVisible = true;
          var description=res.override.overrideDetails[0].description;
          this.view.Popup.flxPopup.flxPopupHeader.lblPaymentInfo.text="Increase Amount Confirmation";
          this.view.Popup.flxPopup.flxPopupMessage.lblMessage.text=description;
        }else{*/
        var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navObjet.messageInfo = "Commitment increased successfully!   Reference : " + res.header["aaaId"];
        navToChangeInterest1.navigate(navObjet);
        //}
        // this.view.amtIncreaseFloatTxt.txtFloatText.text="";
        //this.view.amtRevisedommitment1.txtFloatText.text="";
    },
    failureCallBackfunction: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        kony.print("Error in updating Increase commitment" + JSON.stringify(res));
        if (res.error && res.error.errorDetails.length !== 0) {
            this.view.flxHeaderMenu.isVisible = false;
            this.view.ErrorAllert.isVisible = true;
            this.view.SegmentPopup.isVisible = false;
            kony.print("message::::" + JSON.stringify(res.error.errorDetails));
            kony.print("message::::" + JSON.stringify(res.error.errorDetails[0]));
            kony.print("message::::" + JSON.stringify(res.error.errorDetails[0].message));
            this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message;
        }
        if (res.override && !isNullorEmpty(res.override.overrideDetails)) {
            var overwriteDetails = [];
            var overWriteData = [];
            var segDataForOverrite = [];
            this.currentOverride = res.override.overrideDetails;
            overwriteDetails = res.override.overrideDetails;
            for (var a in overwriteDetails) {
                overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                if (overwriteDetails[a].id === "DUP.CONTRACT") {
                    // this._duplicateId=overwriteDetails[a].id;
                    //kony.print("Id:::::"+this._duplicateId);
                }
                var json = {
                    "lblSerial": "*",
                    "lblInfo": overwriteDetails[a].description
                };
                segDataForOverrite.push(json);
            }
            this.view.SegmentPopup.segOverride.widgetDataMap = {
                "lblSerial": "lblSerial",
                "lblInfo": "lblInfo"
            };
            this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
            this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
            this.view.SegmentPopup.isVisible = true;
            kony.print("overWriteData::::" + JSON.stringify(overWriteData));
            kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
            this.view.forceLayout();
        }
    },
    onClickCancel: function() {
        this.view.amtIncreaseFloatTxt.value = "";
        //this.view.amtRevisedommitment1.txtFloatText.text="";
        //this.view.amtRevisedommitment1.resetReadOnlyText();
        //this.view.amtRevisedommitment1.lblFloatLabel.skin=sknFloatLblPreInput;
        this.navigateCustomerAccountDetails();
    },
    onCloseMsgHeader: function() {
        this.view.flxMessageInfo.isVisible = false;
        this.view.flxHeaderMenu.isVisible = true;
    },
    navigateCustomerAccountDetails: function() {
        var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navToChangeInterest1.navigate(navObjet);
    },
    fetchCurrentCommitment: function(arrangementId) {
        this.currentCommittment = "";
        this.view.ProgressIndicator.isVisible = true;
        var serviceName = "LendingNew";
        var operationName = "getCurrentCommitment";
        var headers = "";
        var inputParams = {
            "arrangementId": arrangementId
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunc, this.failureCallBackfunc);
    },
    successCallBackfunc: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        kony.print("response from post increase commitment" + JSON.stringify(res));
        var respone = res;
        var amount = (respone.body[0].termAmount);
        // amount=parseFloat(amount).toLocaleString(undefined, {  minimumFractionDigits: 2,maximumFractionDigits: 2}); 
        kony.print("current commitment:::::" + amount);
        amount = parseInt(amount);
        amount = Application.numberFormater.convertForDispaly(amount, this._loanObj.currencyId);
        this.view.amtCurrentCommitment1.value = amount; //parseInt(amount) ;
        this.currentCommittment = respone.body[0].termAmount;
        //this.resumeNavigation();
        // this.view.amtCurrentCommitment1.decimals = "2";
        //this.view.amtCurrentCommitment1.setReadOnlyText(amount);
    },
    failureCallBackfunc: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        kony.print("Error in Fetching current commitment" + JSON.stringify(res));
        //this.resumeNavigation();
    },
    onClickRevisedPaymentSchedule: function() {
        var revisedPayment = this.view.amtIncreaseFloatTxt.value;
        if (revisedPayment !== "") {
            if (this._loanObj.produtId === "MORTGAGE") {
                this.view.ErrorAllert.lblMessage.text = "Schedule is not rebuilt, Loan to be disbursed Manually";
                this.view.ErrorAllert.isVisible = true;
            } else {
                this.view.lblErrorMsg.isVisible = false;
                this.view.lblRevisedPaymentConfirmation.isVisible = false;
                revisedPayment = parseFloat(revisedPayment.replace(/[^\d\.]*/g, ''));
                this.view.ProgressIndicator.isVisible = true;
                var serviceName = "LendingNew";
                var operationName = "getIncreseCommitmentScheduleSym";
                var headers = "";
                var inputParams = {};
                inputParams.customerId = this._loanObj.customerId;
                inputParams.increaseAmt = revisedPayment;
                inputParams.produtId = this._loanObj.produtId;
                inputParams.arrangementId = this._loanObj.arrangementId;
                inputParams.currencyId = this._loanObj.currencyId;
                inputParams.overrideDetails = this.currentOverride;
                MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunct, this.failureCallBackfunct);
            }
        } else {
            this.view.lblRevisedPaymentConfirmation.isVisible = true;
            this.view.lblErrorMsg.isVisible = false;
        }
    },
    successCallBackfunct: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        kony.print("fecthing revised payment schedule...." + JSON.stringify(res["body"]));
        var array = [];
        var responseData = res.schedules;
        if (responseData.length > 0) {
            for (var i in responseData) {
                var jsonData = {
                    lblPaymentDate: responseData[i].paymentDueDate,
                    lblAmount: responseData[i].totalPayment,
                    lblPrincipal: responseData[i].totalPayment,
                    lblInterest: responseData[i].prinipalInterest,
                    lblTax: "0.0",
                    lbltotaloutstanding: responseData[i].outstandingAmount
                };
                array.push(jsonData);
            }
        }
        this._scheduledRevisedPayment = array;
        this.showChangeRequestListData("");
    },
    failureCallBackfunct: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        this.view.RevisedPaymentSchedule.isVisible = false;
        if (res.error) {
            if (res.error.errorDetails) {
                this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message;
            } else {
                this.view.ErrorAllert.lblMessage.text = res.error.message;
            }
            this.view.ErrorAllert.isVisible = true;
        }
        /*
        else{
          this.view.ErrorAllert.lblMessage.text = res.errmsg;
        }
        */
        //if override, please resubmit with override details for once
        //TODO : DV - will it go in infite loop?
        if (res.override) {
            this.currentOverride = res.override.overrideDetails;
            this.onClickRevisedPaymentSchedule();
        }
        kony.print("Error in getting simulation response to increase coomitment" + res);
    },
    showPaginationSegmentData: function(dataPerPage, arrayOfData, btnName) {
        kony.print("showPaginationSegmentData===>");
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.flxPagination.isVisible = true;
            } else {
                this.view.RevisedPaymentSchedule.flxPagination.isVisible = false;
            }
            if (btnName == "btnPrev" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName == "btnNext" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.RevisedPaymentSchedule.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnFocus";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;            
            } else if (this.gblSegPageCount == 1 && this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;           
            } else if (this.gblSegPageCount == 1 && this.totalpage == 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.lblSearchPage.text = "";
            } else if (this.gblSegPageCount == this.totalpage && this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnFocus";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                //this.view.lblSearchPage.isVisible = true;            
            } else {
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.lblSearchPage.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in HolidayPaymentController next previous method :::" + err);
        }
    },
    showChangeRequestListData: function(buttonevent) {
        kony.print("showChangeRequestListData===>");
        //showloader();
        if (this._scheduledRevisedPayment.length > 0) {
            var segmentData = this.showPaginationSegmentData(this.dataPerPageNumber, this._scheduledRevisedPayment, buttonevent.id);
            this.view.RevisedPaymentSchedule.segRevised.setData(segmentData);
            this.view.RevisedPaymentSchedule.isVisible = true;
            kony.print("segDataSet ====>>" + JSON.stringify(segmentData));
            this.view.forceLayout();
        }
        //dismissLoader();
    },
    onTextChangeOfImpl: function() {
        //This will execute base logic plus additional logic
        this.view.amtIncreaseFloatTxt.onTxtOnChng(this.updateRevisedPaymentAmt);
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    validateData: function() {
        var Requiredflag = 0;
        var FormId = document.getElementsByTagName("form")[0].getAttribute("id");
        var form = document.getElementById(FormId);
        var Requiredfilter = form.querySelectorAll('*[required]');
        for (var i = 0; i < Requiredfilter.length; i++) {
            if (Requiredfilter[i].disabled !== true && Requiredfilter[i].value === '') {
                var ElementId = Requiredfilter[i].id;
                var Element = document.getElementById(ElementId);
                Element.reportValidity();
                Requiredflag = 1;
            }
        }
        return Requiredflag;
    },
    navigateHome: function() {
        var navToHome = new kony.mvc.Navigation("frmDashboard");
        var navObjet = {};
        navObjet.clientId = "";
        navToHome.navigate(navObjet);
    },
    navigateOverview: function() {
        var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navToChangeInterest1.navigate(navObjet);
    },
});
define("frmCustomerLoanIncreaseAmtControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_a17c69504bca401d94c89676af814122: function AS_UWI_a17c69504bca401d94c89676af814122(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_d6ec65ff007a4b488f3d880d25fb1bc9: function AS_UWI_d6ec65ff007a4b488f3d880d25fb1bc9(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmCustomerLoanIncreaseAmtController", ["userfrmCustomerLoanIncreaseAmtController", "frmCustomerLoanIncreaseAmtControllerActions"], function() {
    var controller = require("userfrmCustomerLoanIncreaseAmtController");
    var controllerActions = ["frmCustomerLoanIncreaseAmtControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
