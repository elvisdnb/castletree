define("frmCustomersList", function() {
    return function(controller) {
        function addWidgetsfrmCustomersList() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMainPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainPanel.setDefaultUnit(kony.flex.DP);
            var seg1 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "data": [{
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }, {
                    "lbl1": "Label",
                    "lbl2": "Label",
                    "lbl3": "Label",
                    "lbl4": "Label"
                }],
                "groupCells": false,
                "height": "690dp",
                "id": "seg1",
                "isVisible": true,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxListCustomers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "20%",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxListCustomers": "flxListCustomers",
                    "lbl1": "lbl1",
                    "lbl2": "lbl2",
                    "lbl3": "lbl3",
                    "lbl4": "lbl4"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMainPanel.add(seg1);
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.compInstData = {
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMainPanel, ProgressIndicator);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomersList,
            "enabledForIdleTimeout": false,
            "id": "frmCustomersList",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});