define("frmpostingRestriction", function() {
    return function(controller) {
        function addWidgetsfrmpostingRestriction() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "7.24%",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "0dp",
                "width": "90%",
                "zIndex": 9
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoansSuccessMessage = new kony.ui.Label({
                "height": "20dp",
                "id": "lblLoansSuccessMessage",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BG",
                "text": "New loan account 51237781231 created for John Doe",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblLoansSuccessMessage);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "6%",
                "pagingEnabled": false,
                "right": "24dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxContainerMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "850dp",
                "id": "flxContainerMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxContainerMain.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "200dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.CustomWidget({
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "112dp",
                "skin": "sknLbl12pxBG4D4D4D",
                "text": "Overview",
                "top": "26dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "64dp",
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var btnAccounts1 = new kony.ui.Button({
                "bottom": "12px",
                "height": "90%",
                "id": "btnAccounts1",
                "isVisible": true,
                "left": "20px",
                "onClick": controller.AS_Button_fb872159826b4baf9bc1a6cccf31cc32,
                "skin": "sknTabUnselected",
                "text": "Accounts",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts2 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts2",
                "isVisible": true,
                "left": "110px",
                "skin": "sknTabUnselected",
                "text": "Deposits",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts3 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts3",
                "isVisible": true,
                "left": "200px",
                "onClick": controller.AS_Button_b312ef910fe44c3da0ee24ff7d0c734d,
                "skin": "sknTabUnselected",
                "text": "Loans",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts4 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts4",
                "isVisible": false,
                "left": "290px",
                "skin": "sknTabUnselected",
                "text": "Bundles",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContainerAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": false,
                "height": "2%",
                "id": "flxContainerAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContainerAccount.setDefaultUnit(kony.flex.DP);
            flxContainerAccount.add();
            var flxContainerdeposits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": false,
                "height": "2%",
                "id": "flxContainerdeposits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "102dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContainerdeposits.setDefaultUnit(kony.flex.DP);
            flxContainerdeposits.add();
            var flxontainerLoans = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": false,
                "height": "2%",
                "id": "flxontainerLoans",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "sknSelected",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxontainerLoans.setDefaultUnit(kony.flex.DP);
            flxontainerLoans.add();
            var flxContaineBundles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": false,
                "height": "9%",
                "id": "flxContaineBundles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "290dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContaineBundles.setDefaultUnit(kony.flex.DP);
            flxContaineBundles.add();
            var flxSeparatorLine1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": false,
                "height": "1dp",
                "id": "flxSeparatorLine1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknSeparatorBlue",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLine1.setDefaultUnit(kony.flex.DP);
            flxSeparatorLine1.add();
            flxDetails.add(btnAccounts1, btnAccounts2, btnAccounts3, btnAccounts4, flxContainerAccount, flxContainerdeposits, flxontainerLoans, flxContaineBundles, flxSeparatorLine1);
            var loanDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "loanDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "overrides": {
                    "FlexContainer0ebe191bc1cf946": {
                        "clipBounds": false,
                        "width": "155dp"
                    },
                    "flxListBoxSelectedValue": {
                        "clipBounds": false,
                        "left": "50dp",
                        "minWidth": "viz.val_cleared",
                        "width": "12.50%"
                    },
                    "flxLoanDetailsHeader": {
                        "clipBounds": false,
                        "zIndex": kony.flex.ZINDEX_AUTO
                    },
                    "flxLoanDetailsvalue": {
                        "clipBounds": false,
                        "height": "63dp",
                        "zIndex": kony.flex.ZINDEX_AUTO
                    },
                    "flxseparatorLinedetails": {
                        "clipBounds": false,
                        "zIndex": kony.flex.ZINDEX_AUTO
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "left": "58dp",
                        "src": "drop_down_arrow.png"
                    },
                    "lblAccountNumberInfo": {
                        "text": "Account Type",
                        "width": "12.50%"
                    },
                    "lblAccountNumberResponse": {
                        "centerY": "50%",
                        "text": "Savings Account",
                        "width": "12.50%"
                    },
                    "lblAccountServiceInfo": {
                        "left": "60dp",
                        "text": "Account Services",
                        "width": "12.50%"
                    },
                    "lblAmountValueInfo": {
                        "text": "Ledger/Clered Balance",
                        "width": "12.50%"
                    },
                    "lblAmountValueResponse": {
                        "centerY": "50%",
                        "text": "327,720.00",
                        "width": "12.50%"
                    },
                    "lblCurrencyValueInfo": {
                        "width": "12.50%"
                    },
                    "lblCurrencyValueResponse": {
                        "centerY": "50%",
                        "text": "EUR",
                        "width": "12.50%"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Restrict Position"
                    },
                    "lblLoanTypeInfo": {
                        "text": "Account Number",
                        "width": "12.50%"
                    },
                    "lblLoanTypeResponse": {
                        "centerY": "50%",
                        "text": "50003014",
                        "width": "12.50%"
                    },
                    "lblMaturityDateInfo": {
                        "isVisible": false,
                        "width": "12.50%"
                    },
                    "lblMaturityDateResponse": {
                        "centerY": "50%",
                        "isVisible": false,
                        "width": "12.50%"
                    },
                    "lblStartDateInfo": {
                        "left": "120dp",
                        "text": "Available Limit",
                        "width": "12.50%"
                    },
                    "lblStartDateResponse": {
                        "centerY": "50%",
                        "left": "120dp",
                        "text": "327,720.00",
                        "width": "12.50%"
                    },
                    "loanDetails": {
                        "height": "128dp",
                        "left": "0dp",
                        "top": "1dp",
                        "width": "100%",
                        "zIndex": kony.flex.ZINDEX_AUTO
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblRestrictPostion = new kony.ui.Label({
                "id": "lblRestrictPostion",
                "isVisible": true,
                "left": "28dp",
                "text": "Restrict postion",
                "top": "22dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeperatorLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": false,
                "height": "2dp",
                "id": "flxSeperatorLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknSeparatorBlue",
                "top": "18dp",
                "width": "98%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxSeperatorLine.setDefaultUnit(kony.flex.DP);
            flxSeperatorLine.add();
            var flxListBoxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "70dp",
                "id": "flxListBoxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "9dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxListBoxContainer.setDefaultUnit(kony.flex.DP);
            var cusListBoxRestrictPayment = new kony.ui.CustomWidget({
                "id": "cusListBoxRestrictPayment",
                "isVisible": true,
                "left": "0dp",
                "top": 0,
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Restriction Postion",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusListBoxReason = new kony.ui.CustomWidget({
                "id": "cusListBoxReason",
                "isVisible": true,
                "left": "230dp",
                "top": 0,
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Reason",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusCalenderFromDate = new kony.ui.CustomWidget({
                "id": "cusCalenderFromDate",
                "isVisible": true,
                "left": "470dp",
                "top": 0,
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "From Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusCalenderExpireDate = new kony.ui.CustomWidget({
                "id": "cusCalenderExpireDate",
                "isVisible": true,
                "left": "710dp",
                "top": 0,
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Expire Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusListBoxUnblockReason = new kony.ui.CustomWidget({
                "id": "cusListBoxUnblockReason",
                "isVisible": true,
                "left": "940dp",
                "top": 0,
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": true,
                "labelText": "Unblock Reason",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var cusListBoxRestrictPostion2 = new kony.ui.CustomWidget({
                "id": "cusListBoxRestrictPostion2",
                "isVisible": true,
                "left": "0dp",
                "top": "70dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Restriction Postion",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusListBoxReason2 = new kony.ui.CustomWidget({
                "id": "cusListBoxReason2",
                "isVisible": true,
                "left": "230dp",
                "top": "70dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Reason",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusCalenderFromDate2 = new kony.ui.CustomWidget({
                "id": "cusCalenderFromDate2",
                "isVisible": true,
                "left": "470dp",
                "top": "70dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "From Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusCalenderExpireDate2 = new kony.ui.CustomWidget({
                "id": "cusCalenderExpireDate2",
                "isVisible": true,
                "left": "710dp",
                "top": "70dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Expire Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusListBoxUnblockReason2 = new kony.ui.CustomWidget({
                "id": "cusListBoxUnblockReason2",
                "isVisible": true,
                "left": "940dp",
                "top": "70dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": true,
                "labelText": "Unblock Reason",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var btnDelete = new kony.ui.Button({
                "height": "30dp",
                "id": "btnDelete",
                "isVisible": true,
                "left": "1150dp",
                "skin": "sknTabSelected",
                "text": "Delete",
                "top": "80dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusListBoxRestrictPostion3 = new kony.ui.CustomWidget({
                "id": "cusListBoxRestrictPostion3",
                "isVisible": true,
                "left": "0dp",
                "top": "130dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Restriction Postion",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusListBoxReason3 = new kony.ui.CustomWidget({
                "id": "cusListBoxReason3",
                "isVisible": true,
                "left": "230dp",
                "top": 130,
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Reason",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusCalenderFromDate3 = new kony.ui.CustomWidget({
                "id": "cusCalenderFromDate3",
                "isVisible": true,
                "left": "470dp",
                "top": "130dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "From Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusCalenderExpireDate3 = new kony.ui.CustomWidget({
                "id": "cusCalenderExpireDate3",
                "isVisible": true,
                "left": "710dp",
                "top": "130dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Expire Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusListBoxUnblockReason3 = new kony.ui.CustomWidget({
                "id": "cusListBoxUnblockReason3",
                "isVisible": true,
                "left": "940dp",
                "top": "130dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": true,
                "labelText": "Unblock Reason",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var btnDelete3 = new kony.ui.Button({
                "height": "30dp",
                "id": "btnDelete3",
                "isVisible": true,
                "left": "1150dp",
                "skin": "sknTabSelected",
                "text": "Delete",
                "top": "140dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusListBoxRestrictPostion4 = new kony.ui.CustomWidget({
                "id": "cusListBoxRestrictPostion4",
                "isVisible": true,
                "left": "0dp",
                "top": "190dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Restriction Postion",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusListBoxReason4 = new kony.ui.CustomWidget({
                "id": "cusListBoxReason4",
                "isVisible": true,
                "left": "230dp",
                "top": "190dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Reason",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusCalenderFromDate4 = new kony.ui.CustomWidget({
                "id": "cusCalenderFromDate4",
                "isVisible": true,
                "left": "470dp",
                "top": "190dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "From Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusCalenderExpireDate4 = new kony.ui.CustomWidget({
                "id": "cusCalenderExpireDate4",
                "isVisible": true,
                "left": "710dp",
                "top": "190dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Expire Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusListBoxUnblockReason4 = new kony.ui.CustomWidget({
                "id": "cusListBoxUnblockReason4",
                "isVisible": true,
                "left": "940dp",
                "top": "190dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": true,
                "labelText": "Unblock Reason",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var btnDelete4 = new kony.ui.Button({
                "height": "30dp",
                "id": "btnDelete4",
                "isVisible": true,
                "left": "1150dp",
                "skin": "sknTabSelected",
                "text": "Delete",
                "top": "200dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusListBoxRestrictPostion5 = new kony.ui.CustomWidget({
                "id": "cusListBoxRestrictPostion5",
                "isVisible": true,
                "left": "0dp",
                "top": "250dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Restriction Postion",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusListBoxReason5 = new kony.ui.CustomWidget({
                "id": "cusListBoxReason5",
                "isVisible": true,
                "left": "230dp",
                "top": "250dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Reason",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var cusCalenderFromDate5 = new kony.ui.CustomWidget({
                "id": "cusCalenderFromDate5",
                "isVisible": true,
                "left": "470dp",
                "top": "250dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "From Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusCalenderExpireDate5 = new kony.ui.CustomWidget({
                "id": "cusCalenderExpireDate5",
                "isVisible": true,
                "left": "710dp",
                "top": "250dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Expire Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusListBoxUnblockReason5 = new kony.ui.CustomWidget({
                "id": "cusListBoxUnblockReason5",
                "isVisible": true,
                "left": "940dp",
                "top": "250dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": true,
                "labelText": "Unblock Reason",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var btnDelete5 = new kony.ui.Button({
                "height": "30dp",
                "id": "btnDelete5",
                "isVisible": true,
                "left": "1150dp",
                "skin": "sknTabSelected",
                "text": "Delete",
                "top": "260dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxListBoxContainer.add(cusListBoxRestrictPayment, cusListBoxReason, cusCalenderFromDate, cusCalenderExpireDate, cusListBoxUnblockReason, cusListBoxRestrictPostion2, cusListBoxReason2, cusCalenderFromDate2, cusCalenderExpireDate2, cusListBoxUnblockReason2, btnDelete, cusListBoxRestrictPostion3, cusListBoxReason3, cusCalenderFromDate3, cusCalenderExpireDate3, cusListBoxUnblockReason3, btnDelete3, cusListBoxRestrictPostion4, cusListBoxReason4, cusCalenderFromDate4, cusCalenderExpireDate4, cusListBoxUnblockReason4, btnDelete4, cusListBoxRestrictPostion5, cusListBoxReason5, cusCalenderFromDate5, cusCalenderExpireDate5, cusListBoxUnblockReason5, btnDelete5);
            var lblErrorMsg = new kony.ui.Label({
                "height": "40dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContainerGroupButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "50dp",
                "id": "flxContainerGroupButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxContainerGroupButton.setDefaultUnit(kony.flex.DP);
            var cusButtonCancel = new kony.ui.CustomWidget({
                "id": "cusButtonCancel",
                "isVisible": true,
                "left": "700dp",
                "top": "5dp",
                "width": "196dp",
                "height": "40dp",
                "minWidth": "200dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            var cusButtonConfirm = new kony.ui.CustomWidget({
                "id": "cusButtonConfirm",
                "isVisible": true,
                "left": "950dp",
                "top": "5dp",
                "width": "204dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": " Confirm      ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            var btnAddWidgets = new kony.ui.Button({
                "height": "40dp",
                "id": "btnAddWidgets",
                "isVisible": true,
                "left": "18dp",
                "skin": "sknTabSelected",
                "text": "Add",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContainerGroupButton.add(cusButtonCancel, cusButtonConfirm, btnAddWidgets);
            flxContainerMain.add(flxHeaderMenu, custInfo, flxDetails, loanDetails, lblRestrictPostion, flxSeperatorLine, flxListBoxContainer, lblErrorMsg, flxContainerGroupButton);
            flxMainContainer.add(flxContainerMain);
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1366,
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "loanDetails.FlexContainer0ebe191bc1cf946": {
                    "width": "155dp"
                },
                "loanDetails.flxListBoxSelectedValue": {
                    "left": "50dp",
                    "minWidth": "",
                    "width": "12.50%"
                },
                "loanDetails.flxLoanDetailsHeader": {
                    "zIndex": kony.flex.ZINDEX_AUTO
                },
                "loanDetails.flxLoanDetailsvalue": {
                    "height": "63dp",
                    "zIndex": kony.flex.ZINDEX_AUTO
                },
                "loanDetails.flxseparatorLinedetails": {
                    "zIndex": kony.flex.ZINDEX_AUTO
                },
                "loanDetails.imgListBoxSelectedDropDown": {
                    "left": "58dp",
                    "src": "drop_down_arrow.png"
                },
                "loanDetails.lblAccountNumberInfo": {
                    "text": "Account Type",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountNumberResponse": {
                    "centerY": "50%",
                    "text": "Savings Account",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountServiceInfo": {
                    "left": "60dp",
                    "text": "Account Services",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueInfo": {
                    "text": "Ledger/Clered Balance",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueResponse": {
                    "centerY": "50%",
                    "text": "327,720.00",
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueResponse": {
                    "centerY": "50%",
                    "text": "EUR",
                    "width": "12.50%"
                },
                "loanDetails.lblListBoxSelectedValue": {
                    "text": "Restrict Position"
                },
                "loanDetails.lblLoanTypeInfo": {
                    "text": "Account Number",
                    "width": "12.50%"
                },
                "loanDetails.lblLoanTypeResponse": {
                    "centerY": "50%",
                    "text": "50003014",
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateInfo": {
                    "left": "120dp",
                    "text": "Available Limit",
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateResponse": {
                    "centerY": "50%",
                    "left": "120dp",
                    "text": "327,720.00",
                    "width": "12.50%"
                },
                "loanDetails": {
                    "height": "128dp",
                    "left": "0dp",
                    "top": "1dp",
                    "width": "100%",
                    "zIndex": kony.flex.ZINDEX_AUTO
                }
            }
            this.add(flxMessageInfo, uuxNavigationRail, flxMainContainer);
        };
        return [{
            "addWidgets": addWidgetsfrmpostingRestriction,
            "enabledForIdleTimeout": false,
            "id": "frmpostingRestriction",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});