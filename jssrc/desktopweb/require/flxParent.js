define("flxParent", function() {
    return function(controller) {
        var flxParent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxParent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0cc50129cd62340",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxParent.setDefaultUnit(kony.flex.DP);
        var flxLine = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "sknVerticalLine",
            "top": "0dp",
            "width": "3dp",
            "zIndex": 1
        }, {}, {});
        flxLine.setDefaultUnit(kony.flex.DP);
        flxLine.add();
        var flxRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "CopyslFbox0e910315f15294e",
            "top": "0dp",
            "width": "99%"
        }, {}, {});
        flxRow.setDefaultUnit(kony.flex.DP);
        var lblAddress = new kony.ui.Label({
            "id": "lblAddress",
            "isVisible": false,
            "left": "2%",
            "skin": "CopydefLabel0gc11ea6ce07940",
            "text": "Dashboard",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var rtxAddress = new kony.ui.RichText({
            "id": "rtxAddress",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "defRichTextLink",
            "skin": "CopydefRichTextNormal0be0eee345d9a4b",
            "text": "RichText",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow.add(lblAddress, rtxAddress);
        var lblLine = new kony.ui.Label({
            "bottom": "2dp",
            "height": "1dp",
            "id": "lblLine",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0a54af298166146",
            "text": "label",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxParent.add(flxLine, flxRow, lblLine);
        return flxParent;
    }
})