define("flxListCustomers", function() {
    return function(controller) {
        var flxListCustomers = new kony.ui.FlexContainer({
            "clipBounds": true,
            "height": "80dp",
            "id": "flxListCustomers",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxListCustomers.setDefaultUnit(kony.flex.DP);
        var lbl1 = new kony.ui.Label({
            "id": "lbl1",
            "isVisible": true,
            "left": "10dp",
            "minWidth": "260dp",
            "skin": "defLabel",
            "text": "Label",
            "top": "22dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbl2 = new kony.ui.Label({
            "id": "lbl2",
            "isVisible": true,
            "left": "20dp",
            "minWidth": "260dp",
            "skin": "defLabel",
            "text": "Label",
            "top": "22dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbl3 = new kony.ui.Label({
            "id": "lbl3",
            "isVisible": true,
            "left": "20dp",
            "minWidth": "260dp",
            "skin": "defLabel",
            "text": "Label",
            "top": "22dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbl4 = new kony.ui.Label({
            "id": "lbl4",
            "isVisible": true,
            "left": "20dp",
            "minWidth": "260dp",
            "skin": "defLabel",
            "text": "Label",
            "top": "22dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxListCustomers.add(lbl1, lbl2, lbl3, lbl4);
        return flxListCustomers;
    }
})