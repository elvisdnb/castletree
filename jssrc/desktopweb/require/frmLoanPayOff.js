define("frmLoanPayOff", function() {
    return function(controller) {
        function addWidgetsfrmLoanPayOff() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "7.24%",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "0dp",
                "width": "90%",
                "zIndex": 9
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoansSuccessMessage = new kony.ui.Label({
                "height": "20dp",
                "id": "lblLoansSuccessMessage",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BG",
                "text": "New loan account 51237781231 created for John Doe",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblLoansSuccessMessage);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0c1b889249cc54c"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0b184d8e60fb048"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_bb61b37313204be3833502157fa6d2d9;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_a47c5b20b2604db1b286d9916959a04c;
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "pagingEnabled": false,
                "right": "24dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0c444a067f87d41",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "95%"
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var FlexContainer0e48343e0b1f644 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlexContainer0e48343e0b1f644",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "99%"
            }, {}, {});
            FlexContainer0e48343e0b1f644.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "300dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "112dp",
                "skin": "CopysknLbl0cfbb3108973948",
                "text": "Overview",
                "top": "26dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardPayoffSplash = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardPayoffSplash",
                "isVisible": true,
                "left": "175dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "26dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardPayoff = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardPayoff",
                "isVisible": true,
                "left": "190dp",
                "skin": "sknLblLoanServiceBlacl12px",
                "text": "Payoff",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue, lblBreadBoardPayoffSplash, lblBreadBoardPayoff, cusBackIcon);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "64dp",
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var btnAccounts1 = new kony.ui.Button({
                "bottom": "12px",
                "height": "90%",
                "id": "btnAccounts1",
                "isVisible": true,
                "left": "20px",
                "onClick": controller.AS_Button_fb872159826b4baf9bc1a6cccf31cc32,
                "skin": "sknTabUnselected",
                "text": "Accounts",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts2 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts2",
                "isVisible": true,
                "left": "110px",
                "skin": "sknTabUnselected",
                "text": "Deposits",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts3 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts3",
                "isVisible": true,
                "left": "200px",
                "onClick": controller.AS_Button_b312ef910fe44c3da0ee24ff7d0c734d,
                "skin": "sknTabUnselected",
                "text": "Loans",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAccounts4 = new kony.ui.Button({
                "height": "90%",
                "id": "btnAccounts4",
                "isVisible": false,
                "left": "290px",
                "skin": "sknTabUnselected",
                "text": "Bundles",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContainerAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxContainerAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContainerAccount.setDefaultUnit(kony.flex.DP);
            flxContainerAccount.add();
            var flxContainerdeposits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxContainerdeposits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "102dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContainerdeposits.setDefaultUnit(kony.flex.DP);
            flxContainerdeposits.add();
            var flxontainerLoans = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "2%",
                "id": "flxontainerLoans",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "sknSelected",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxontainerLoans.setDefaultUnit(kony.flex.DP);
            flxontainerLoans.add();
            var flxContaineBundles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "9%",
                "id": "flxContaineBundles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "290dp",
                "isModalContainer": false,
                "skin": "sknNotselectedTransparent",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxContaineBundles.setDefaultUnit(kony.flex.DP);
            flxContaineBundles.add();
            var flxSeparatorLine1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLine1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknSeparatorBlue",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLine1.setDefaultUnit(kony.flex.DP);
            flxSeparatorLine1.add();
            flxDetails.add(btnAccounts1, btnAccounts2, btnAccounts3, btnAccounts4, flxContainerAccount, flxContainerdeposits, flxontainerLoans, flxContaineBundles, flxSeparatorLine1);
            var loanDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "loanDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "1dp",
                "width": "100%",
                "overrides": {
                    "FlexContainer0ebe191bc1cf946": {
                        "width": "270dp"
                    },
                    "flxListBoxSelectedValue": {
                        "left": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "flxLoanDetailsvalue": {
                        "height": "63dp"
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "left": "58dp",
                        "src": "drop_down_arrow.png"
                    },
                    "lblAccountNumberInfo": {
                        "width": "12.50%"
                    },
                    "lblAccountNumberResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblAccountServiceInfo": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "lblAmountValueInfo": {
                        "width": "12.50%"
                    },
                    "lblAmountValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblCurrencyValueInfo": {
                        "width": "12.50%"
                    },
                    "lblCurrencyValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Payoff"
                    },
                    "lblLoanTypeInfo": {
                        "width": "13.20%"
                    },
                    "lblLoanTypeResponse": {
                        "centerY": "50%",
                        "width": "13.60%"
                    },
                    "lblMaturityDateInfo": {
                        "width": "12.50%"
                    },
                    "lblMaturityDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblStartDateInfo": {
                        "width": "12.50%"
                    },
                    "lblStartDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "loanDetails": {
                        "height": "128dp",
                        "left": "0dp",
                        "top": "1dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxPayOffTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 40,
                "clipBounds": true,
                "id": "flxPayOffTab",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopyslFbox0a0d1087168f844",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPayOffTab.setDefaultUnit(kony.flex.DP);
            var flxPayOff = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPayOff",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": 0,
                "skin": "CopyslFbox0a61462c2ce114f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPayOff.setDefaultUnit(kony.flex.DP);
            var datePayValue = new kony.ui.CustomWidget({
                "id": "datePayValue",
                "isVisible": false,
                "left": "14%",
                "bottom": 10,
                "top": "10dp",
                "width": "190dp",
                "height": "50px",
                "zIndex": 9,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": false,
                "disabled": true,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Payment Value Date",
                "max": "",
                "min": "1900-01-01",
                "onDone": "",
                "showWeekNumber": false,
                "value": "",
                "weekLabel": "Wk"
            });
            var flxLoanPayoffWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "580dp",
                "id": "flxLoanPayoffWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxLoanPayoffWrapper.setDefaultUnit(kony.flex.DP);
            var flxPayoffStatement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayoffStatement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "PayOffBlueBg",
                "top": "0",
                "width": "35%",
                "zIndex": 9
            }, {}, {});
            flxPayoffStatement.setDefaultUnit(kony.flex.DP);
            var lblPayStatement = new kony.ui.Label({
                "bottom": "10px",
                "height": "50px",
                "id": "lblPayStatement",
                "isVisible": false,
                "left": "5.08%",
                "skin": "CopydefLabel0ff05216c98f043",
                "text": "Payoff Statement",
                "top": "10dp",
                "width": "33%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPayDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxPayDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0fda5754cd9f44b",
                "top": "67dp",
                "width": "100%"
            }, {}, {});
            flxPayDate.setDefaultUnit(kony.flex.DP);
            flxPayDate.add();
            var TitleLabel = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "TitleLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "TitleLabel": {
                        "height": "70dp"
                    },
                    "flxTitle": {
                        "isVisible": false
                    },
                    "lblTitle": {
                        "left": "24dp",
                        "text": "Payoff Statement"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxStatementFields = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatementFields",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxStatementFields.setDefaultUnit(kony.flex.DP);
            var segSettlement = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "data": [{
                    "lblPrincipalPayAmount": "EUR",
                    "lblPrincipalPayCurrency": "EUR",
                    "lblPrincipalPayTitle": "Label"
                }, {
                    "lblPrincipalPayAmount": "EUR",
                    "lblPrincipalPayCurrency": "EUR",
                    "lblPrincipalPayTitle": "Label"
                }, {
                    "lblPrincipalPayAmount": "EUR",
                    "lblPrincipalPayCurrency": "EUR",
                    "lblPrincipalPayTitle": "Label"
                }, {
                    "lblPrincipalPayAmount": "EUR",
                    "lblPrincipalPayCurrency": "EUR",
                    "lblPrincipalPayTitle": "Label"
                }],
                "groupCells": false,
                "id": "segSettlement",
                "isVisible": true,
                "left": "0",
                "maxHeight": 250,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0f39e79049f6342",
                "rowTemplate": "flxSegment",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSegment": "flxSegment",
                    "flxStatementRows": "flxStatementRows",
                    "lblPrincipalPayAmount": "lblPrincipalPayAmount",
                    "lblPrincipalPayCurrency": "lblPrincipalPayCurrency",
                    "lblPrincipalPayTitle": "lblPrincipalPayTitle"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxColorWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxColorWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknBlueBorder",
                "top": 10,
                "width": "100%"
            }, {}, {});
            flxColorWrapper.setDefaultUnit(kony.flex.DP);
            var flxStatementRows = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "55dp",
                "id": "flxStatementRows",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0dbb3783b0a1240",
                "top": "0",
                "width": "90%"
            }, {}, {});
            flxStatementRows.setDefaultUnit(kony.flex.DP);
            var flxIconWrapper1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxIconWrapper1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0ic34df10375441",
                "top": "0",
                "width": "55%"
            }, {}, {});
            flxIconWrapper1.setDefaultUnit(kony.flex.DP);
            var lblNettleSettlementPayTitle = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblNettleSettlementPayTitle",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0h31b4b422d5b45",
                "text": "Net Settlement",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBillReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxBillReport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0cbc23910d38e4b",
                "top": "15dp",
                "width": "25dp",
                "zIndex": 9
            }, {}, {});
            flxBillReport.setDefaultUnit(kony.flex.DP);
            var iconReport = new kony.ui.CustomWidget({
                "id": "iconReport",
                "isVisible": true,
                "left": "0dp",
                "top": 0,
                "width": "25dp",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "",
                "iconName": "receipt",
                "size": "small"
            });
            flxBillReport.add(iconReport);
            flxIconWrapper1.add(lblNettleSettlementPayTitle, flxBillReport);
            var lblNetPrincipalPayCurrency = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblNetPrincipalPayCurrency",
                "isVisible": true,
                "left": "0",
                "skin": "fontBlue",
                "top": "0",
                "width": "15%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNetPrincipalPayAmount = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblNetPrincipalPayAmount",
                "isVisible": true,
                "left": "0",
                "skin": "fontBlue",
                "top": "0",
                "width": "30%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatementRows.add(flxIconWrapper1, lblNetPrincipalPayCurrency, lblNetPrincipalPayAmount);
            flxColorWrapper.add(flxStatementRows);
            var flxAccountMoved = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountMoved",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "45dp",
                "width": "100%"
            }, {}, {});
            flxAccountMoved.setDefaultUnit(kony.flex.DP);
            var flxClosureWrap = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxClosureWrap",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "72%"
            }, {}, {});
            flxClosureWrap.setDefaultUnit(kony.flex.DP);
            var iconTick = new kony.ui.CustomWidget({
                "id": "iconTick",
                "isVisible": true,
                "left": "0",
                "top": -2,
                "width": "28dp",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "",
                "iconName": "done",
                "size": "small"
            });
            var lblAccountClosure = new kony.ui.Label({
                "id": "lblAccountClosure",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0ddfa8a4bac904c",
                "text": "Account moved to Pending closure!",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClosureWrap.add(iconTick, lblAccountClosure);
            flxAccountMoved.add(flxClosureWrap);
            flxStatementFields.add(segSettlement, flxColorWrapper, flxAccountMoved);
            flxPayoffStatement.add(lblPayStatement, flxPayDate, TitleLabel, flxStatementFields);
            var flxLoanPayoffRight = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoanPayoffRight",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "top": "0dp",
                "width": "810dp"
            }, {}, {});
            flxLoanPayoffRight.setDefaultUnit(kony.flex.DP);
            var FlexContainer0e40ace0494aa43 = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "FlexContainer0e40ace0494aa43",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "pagingEnabled": false,
                "right": "20dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFbox0ddb3c5d2942a4a",
                "top": "0",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            FlexContainer0e40ace0494aa43.setDefaultUnit(kony.flex.DP);
            var flxPayOffRightWrap = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxPayOffRightWrap",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPayOffRightWrap.setDefaultUnit(kony.flex.DP);
            var TitleLabel1 = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "TitleLabel1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "TitleLabel": {
                        "height": "70dp",
                        "left": "0dp",
                        "width": "100%"
                    },
                    "flxTitle": {
                        "left": "10dp"
                    },
                    "lblTitle": {
                        "left": "20dp",
                        "text": "Loan Payoff"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxPayOfFlex = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "50px",
                "id": "flxPayOfFlex",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxPayOfFlex.setDefaultUnit(kony.flex.DP);
            var lblPayOffStatementTitle = new kony.ui.Label({
                "height": "100%",
                "id": "lblPayOffStatementTitle",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0a1df0ea2cf4b40",
                "text": "Loan Payoff",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPayOfFlex.add(lblPayOffStatementTitle);
            var custTotalOutStanding1 = new kony.ui.CustomWidget({
                "id": "custTotalOutStanding1",
                "isVisible": false,
                "left": "0dp",
                "top": "120dp",
                "width": "208dp",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Total Outstanding",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": true,
                "showIconButtonTrailing": true,
                "value": ""
            });
            var custTotalOutStanding = new kony.ui.CustomWidget({
                "id": "custTotalOutStanding",
                "isVisible": true,
                "left": "20dp",
                "top": "90dp",
                "width": "208dp",
                "height": "56dp",
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Total Outstanding",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var custSettlementMode = new kony.ui.CustomWidget({
                "id": "custSettlementMode",
                "isVisible": true,
                "left": "272dp",
                "top": "90dp",
                "width": "208dp",
                "height": "56px",
                "zIndex": 9,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "dense": "dense",
                "disabled": false,
                "labelText": "Settlement Mode",
                "options": "",
                "readonly": true,
                "value": ""
            });
            var custDebitAccount = new kony.ui.CustomWidget({
                "id": "custDebitAccount",
                "isVisible": true,
                "left": "550dp",
                "top": "90dp",
                "width": "168dp",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Debit Account",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": true,
                "validateRequired": "required",
                "value": ""
            });
            var flxpayInSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayInSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "700dp",
                "isModalContainer": false,
                "skin": "searchflex",
                "top": 90,
                "width": "40dp"
            }, {}, {});
            flxpayInSearch.setDefaultUnit(kony.flex.DP);
            var lblSearchIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblSearchIcon",
                "isVisible": true,
                "left": 0,
                "skin": "iconFontPurple20px",
                "text": "k",
                "top": "0",
                "width": "20dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxpayInSearch.add(lblSearchIcon);
            var flxOverallGraphPart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "250dp",
                "id": "flxOverallGraphPart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "12%",
                "isModalContainer": false,
                "skin": "CopyslFbox0j3f9bd32e68e49",
                "top": 160,
                "width": "75%"
            }, {}, {});
            flxOverallGraphPart.setDefaultUnit(kony.flex.DP);
            var flxPayoffGraph = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "flxPayoffGraph",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 00,
                "isModalContainer": false,
                "skin": "CopyslFbox0ee13033c9b444f",
                "top": "10dp",
                "width": "97%"
            }, {}, {});
            flxPayoffGraph.setDefaultUnit(kony.flex.DP);
            var flxRightLoanLabels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRightLoanLabels",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "width": "300dp"
            }, {}, {});
            flxRightLoanLabels.setDefaultUnit(kony.flex.DP);
            var flxRound1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "5dp",
                "id": "flxRound1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknPersonalClrPayoff",
                "top": "0",
                "width": "25dp"
            }, {}, {});
            flxRound1.setDefaultUnit(kony.flex.DP);
            flxRound1.add();
            var blTitlePersonal = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "blTitlePersonal",
                "isVisible": true,
                "left": "8dp",
                "skin": "lblGraphLabel",
                "text": "Principal",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRound2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "5dp",
                "id": "flxRound2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40px",
                "isModalContainer": false,
                "skin": "sknMortgagePayoff",
                "top": "0",
                "width": "25dp"
            }, {}, {});
            flxRound2.setDefaultUnit(kony.flex.DP);
            flxRound2.add();
            var blTitleMortgage = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "blTitleMortgage",
                "isVisible": true,
                "left": "8dp",
                "skin": "lblGraphLabel",
                "text": "Net Settlement",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightLoanLabels.add(flxRound1, blTitlePersonal, flxRound2, blTitleMortgage);
            var barGraph1 = new com.temenos.chartjslib.barGraph1({
                "height": "85%",
                "id": "barGraph1",
                "isVisible": false,
                "left": "2%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10%",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "barGraph1": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWbargraph1": {
                        "canvasHeight": "",
                        "canvasWidth": "",
                        "datasetsArray": "[]",
                        "xlabelsArray": "[]",
                        "yBeginsAtZero": false
                    }
                }
            });
            var barGraph2 = new com.temenos.chartjslib.barGraph2({
                "height": "85%",
                "id": "barGraph2",
                "isVisible": true,
                "left": "3%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10%",
                "width": "97%",
                "zIndex": 1,
                "overrides": {
                    "barGraph2": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "TPWBarGraph2": {
                        "canvasHeight": "",
                        "canvasWidth": "",
                        "datasetsArray": "[]",
                        "xlabelsArray": "[]",
                        "yBeginsAtZero": false,
                        "ySuggestedMin": 0
                    }
                }
            });
            var lblNextDays = new kony.ui.Label({
                "id": "lblNextDays",
                "isVisible": true,
                "left": "30%",
                "skin": "CopydefLabel0e80c2b93982e46",
                "text": "Label",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblYValues = new kony.ui.Label({
                "bottom": 0,
                "centerX": "50%",
                "id": "lblYValues",
                "isVisible": true,
                "left": "32%",
                "skin": "CopydefLabel0e80c2b93982e46",
                "text": "Payment Value Dates",
                "width": "200dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblXValues = new kony.ui.Label({
                "centerY": "50%",
                "height": "30px",
                "id": "lblXValues",
                "isVisible": true,
                "left": "-38dp",
                "skin": "rotateXaxis",
                "text": "Label",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoanPayoff = new kony.ui.Label({
                "id": "lblLoanPayoff",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopydefLabel0d829ca6557cd45",
                "text": "Payoff Simulation",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPayoffGraph.add(flxRightLoanLabels, barGraph1, barGraph2, lblNextDays, lblYValues, lblXValues, lblLoanPayoff);
            var custDateSelectpayoff = new kony.ui.CustomWidget({
                "id": "custDateSelectpayoff",
                "isVisible": false,
                "left": "20dp",
                "top": "245dp",
                "width": "208dp",
                "height": "56px",
                "zIndex": 9,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Select Date",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custInterestpayoff = new kony.ui.CustomWidget({
                "id": "custInterestpayoff",
                "isVisible": false,
                "left": "302dp",
                "top": 245,
                "width": "200dp",
                "height": "56dp",
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Interest",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var custNetPayoff = new kony.ui.CustomWidget({
                "id": "custNetPayoff",
                "isVisible": false,
                "left": "580dp",
                "top": 245,
                "width": "200dp",
                "height": "56dp",
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Net",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            flxOverallGraphPart.add(flxPayoffGraph, custDateSelectpayoff, custInterestpayoff, custNetPayoff);
            var flxProgressLoad = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProgressLoad",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 440,
                "width": "100%"
            }, {}, {});
            flxProgressLoad.setDefaultUnit(kony.flex.DP);
            var flxProgressTitles = new kony.ui.Label({
                "id": "flxProgressTitles",
                "isVisible": true,
                "left": 70,
                "skin": "CopydefLabel0h37bedfd3e7546",
                "text": "Settling All Outstanding Bills…",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLoaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLoaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 70,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "85%"
            }, {}, {});
            flxLoaders.setDefaultUnit(kony.flex.DP);
            var flxUnactiveBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "4px",
                "id": "flxUnactiveBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxUnactiveBar.setDefaultUnit(kony.flex.DP);
            flxUnactiveBar.add();
            var flxActiveBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "4px",
                "id": "flxActiveBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0i071440a864a47",
                "top": "0",
                "width": "0%"
            }, {}, {});
            flxActiveBar.setDefaultUnit(kony.flex.DP);
            flxActiveBar.add();
            flxLoaders.add(flxUnactiveBar, flxActiveBar);
            flxProgressLoad.add(flxProgressTitles, flxLoaders);
            var flxCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "500dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustButtons.setDefaultUnit(kony.flex.DP);
            var custProceed = new kony.ui.CustomWidget({
                "id": "custProceed",
                "isVisible": true,
                "left": "545dp",
                "top": "0dp",
                "width": "204dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custCancel = new kony.ui.CustomWidget({
                "id": "custCancel",
                "isVisible": true,
                "left": "330dp",
                "top": "0dp",
                "width": "196dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxCustButtons.add(custProceed, custCancel);
            flxPayOffRightWrap.add(TitleLabel1, flxPayOfFlex, custTotalOutStanding1, custTotalOutStanding, custSettlementMode, custDebitAccount, flxpayInSearch, flxOverallGraphPart, flxProgressLoad, flxCustButtons);
            var CopyflxPayDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxPayDate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0i73cfcca49ae45",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            CopyflxPayDate.setDefaultUnit(kony.flex.DP);
            CopyflxPayDate.add();
            FlexContainer0e40ace0494aa43.add(flxPayOffRightWrap, CopyflxPayDate);
            flxLoanPayoffRight.add(FlexContainer0e40ace0494aa43);
            flxLoanPayoffWrapper.add(flxPayoffStatement, flxLoanPayoffRight);
            flxPayOff.add(datePayValue, flxLoanPayoffWrapper);
            flxPayOffTab.add(flxPayOff);
            FlexContainer0e48343e0b1f644.add(flxHeaderMenu, custInfo, flxDetails, loanDetails, flxPayOffTab);
            flxMainContainer.add(FlexContainer0e48343e0b1f644);
            var flxOutstandingRepaymentOutsideWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxOutstandingRepaymentOutsideWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxOutstandingRepaymentOutsideWrapper.setDefaultUnit(kony.flex.DP);
            var flxOutstandingRepaymentInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "360dp",
                "id": "flxOutstandingRepaymentInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxOutstanding6pxRadius",
                "width": "72%",
                "zIndex": 1
            }, {}, {});
            flxOutstandingRepaymentInnerWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblHeaderInfo = new kony.ui.Label({
                "id": "lblHeaderInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl24px005096BG",
                "text": "Outstanding Repayments",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusIconPrinter = new kony.ui.CustomWidget({
                "id": "cusIconPrinter",
                "isVisible": true,
                "right": "80dp",
                "top": "2dp",
                "width": "26dp",
                "height": "26dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "print",
                "size": "small"
            });
            var flxHeaderSegmentOutstandingClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeaderSegmentOutstandingClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderSegmentOutstandingClose.setDefaultUnit(kony.flex.DP);
            var cusIconClose = new kony.ui.CustomWidget({
                "id": "cusIconClose",
                "isVisible": false,
                "left": "0dp",
                "top": "2dp",
                "width": "100%",
                "height": "26dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "highlight_off",
                "size": "small"
            });
            var imgIconClose1 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgIconClose1",
                "isVisible": false,
                "left": "0dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgIconClose = new kony.ui.Button({
                "height": "40dp",
                "id": "imgIconClose",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknBtnImgClose",
                "text": "L",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSegmentOutstandingClose.add(cusIconClose, imgIconClose1, imgIconClose);
            flxHeaderInfo.add(lblHeaderInfo, cusIconPrinter, flxHeaderSegmentOutstandingClose);
            var flxOutStandingSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxOutStandingSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxOutStandingSegment.setDefaultUnit(kony.flex.DP);
            var flxSegmentHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSegmentHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSegmentHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblDueDate = new kony.ui.Label({
                "height": "26dp",
                "id": "lblDueDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Due Date",
                "top": "2dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTypeInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblTypeInfo",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Type",
                "top": "2dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilledInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblBilledInfo",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Billed",
                "top": "2dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOutstandingInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblOutstandingInfo",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Outstanding",
                "top": "2dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatusInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblStatusInfo",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Status",
                "top": "2dp",
                "width": "25%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentHeaderInfo.add(lblDueDate, lblTypeInfo, lblBilledInfo, lblOutstandingInfo, lblStatusInfo);
            var flxOutstandingRepaymentSegmentWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "200dp",
                "horizontalScrollIndicator": true,
                "id": "flxOutstandingRepaymentSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "40dp",
                "verticalScrollIndicator": true,
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxOutstandingRepaymentSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var segOutstandingBill = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }],
                "groupCells": false,
                "height": "200dp",
                "id": "segOutstandingBill",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0ba4027c6a12449",
                "rowTemplate": "flxPayOffOutstandingBillDetails",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxPayOffOutstandingBillDetails": "flxPayOffOutstandingBillDetails",
                    "lblBilledInfo": "lblBilledInfo",
                    "lblDueDateInfo": "lblDueDateInfo",
                    "lblOutstandingInfo": "lblOutstandingInfo",
                    "lblStatusInfo": "lblStatusInfo",
                    "lblTypeInfo": "lblTypeInfo"
                },
                "width": "100%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOutstandingRepaymentSegmentWrapper.add(segOutstandingBill);
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var btnPrev = new kony.ui.Image2({
                "centerY": "40%",
                "height": "17dp",
                "id": "btnPrev",
                "isVisible": true,
                "left": "930dp",
                "skin": "slImage",
                "src": "arrow_back.png",
                "top": "5dp",
                "width": "19dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext = new kony.ui.Image2({
                "centerY": "40%",
                "height": "17dp",
                "id": "btnNext",
                "isVisible": true,
                "left": "950dp",
                "skin": "slImage",
                "src": "arrow_forward.png",
                "top": "5dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPage = new kony.ui.Label({
                "centerY": "40%",
                "id": "lblSearchPage",
                "isVisible": true,
                "left": "830dp",
                "skin": "CopydefLabel0b044b7d672a44a",
                "text": "1-6 of 18",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRowPerPageNumber = new kony.ui.Label({
                "id": "lblRowPerPageNumber",
                "isVisible": false,
                "left": "354dp",
                "skin": "CopydefLabel0bfa17407e4df49",
                "text": "Rows Per Page",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextField0a340c02fa20749 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "20dp",
                "id": "TextField0a340c02fa20749",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "494dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ca88ed6f06d246",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxPagination.add(btnPrev, btnNext, lblSearchPage, lblRowPerPageNumber, TextField0a340c02fa20749);
            flxOutStandingSegment.add(flxSegmentHeaderInfo, flxOutstandingRepaymentSegmentWrapper, flxPagination);
            var flxOutstandingError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxOutstandingError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "90%"
            }, {}, {});
            flxOutstandingError.setDefaultUnit(kony.flex.DP);
            var lblOutError = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblOutError",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0gc960a890bbf4c",
                "text": "Label",
                "top": "0",
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOutstandingError.add(lblOutError);
            flxOutstandingRepaymentInnerWrapper.add(flxHeaderInfo, flxOutStandingSegment, flxOutstandingError);
            flxOutstandingRepaymentOutsideWrapper.add(flxOutstandingRepaymentInnerWrapper);
            var flxpayInOutPOP = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxpayInOutPOP",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0g815ca6a691d4d",
                "top": "0",
                "width": "100%",
                "zIndex": 9
            }, {}, {});
            flxpayInOutPOP.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c8da61cffdb244 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "500dp",
                "id": "FlexContainer0c8da61cffdb244",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0e5562f0a71eb42",
                "top": "0",
                "width": "60%"
            }, {}, {});
            FlexContainer0c8da61cffdb244.setDefaultUnit(kony.flex.DP);
            var popupPayinPayout = new com.konymp.popupPayinPayout.popupPayinPayout({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popupPayinPayout",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0e816618aedef47",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxInnerSearch": {
                        "height": "400dp"
                    },
                    "imgClose1": {
                        "src": "ico_close.png"
                    },
                    "imgSrc1": {
                        "src": "search.png"
                    },
                    "popupPayinPayout": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexContainer0c8da61cffdb244.add(popupPayinPayout);
            flxpayInOutPOP.add(FlexContainer0c8da61cffdb244);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "32dp",
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "89%",
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "5%",
                        "right": "32dp",
                        "width": "89%"
                    },
                    "imgCloseBackup": {
                        "left": "1200dp",
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "ProgressIndicator": {
                        "isVisible": true,
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "height": "100%",
                        "isVisible": false
                    },
                    "flxPopup": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "left": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var SegmentPopup2 = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "SegmentPopup2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "height": "100%",
                        "isVisible": false,
                        "left": "10dp",
                        "top": "10dp"
                    },
                    "flxPopup": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "left": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0c1b889249cc54c",
                    "skin2": "CopydefBtnNormal0b184d8e60fb048",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "loanDetails.FlexContainer0ebe191bc1cf946": {
                    "width": "270dp"
                },
                "loanDetails.flxListBoxSelectedValue": {
                    "left": "",
                    "minWidth": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.flxLoanDetailsvalue": {
                    "height": "63dp"
                },
                "loanDetails.imgListBoxSelectedDropDown": {
                    "left": "58dp",
                    "src": "drop_down_arrow.png"
                },
                "loanDetails.lblAccountNumberInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAccountNumberResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountServiceInfo": {
                    "left": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblListBoxSelectedValue": {
                    "text": "Payoff"
                },
                "loanDetails.lblLoanTypeInfo": {
                    "width": "13.20%"
                },
                "loanDetails.lblLoanTypeResponse": {
                    "centerY": "50%",
                    "width": "13.60%"
                },
                "loanDetails.lblMaturityDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails": {
                    "height": "128dp",
                    "left": "0dp",
                    "top": "1dp",
                    "width": "100%"
                },
                "TitleLabel": {
                    "height": "70dp"
                },
                "TitleLabel.lblTitle": {
                    "left": "24dp",
                    "text": "Payoff Statement"
                },
                "TitleLabel1": {
                    "height": "70dp",
                    "left": "0dp",
                    "width": "100%"
                },
                "TitleLabel1.flxTitle": {
                    "left": "10dp"
                },
                "TitleLabel1.lblTitle": {
                    "left": "20dp",
                    "text": "Loan Payoff"
                },
                "barGraph1": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "barGraph2": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "popupPayinPayout.flxInnerSearch": {
                    "height": "400dp"
                },
                "popupPayinPayout.imgClose1": {
                    "src": "ico_close.png"
                },
                "popupPayinPayout.imgSrc1": {
                    "src": "search.png"
                },
                "popupPayinPayout": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "height": "100%",
                    "left": "0dp",
                    "width": "100%"
                },
                "ErrorAllert": {
                    "left": "5%",
                    "right": "32dp",
                    "width": "89%"
                },
                "ErrorAllert.imgCloseBackup": {
                    "left": "1200dp",
                    "src": "ico_close.png"
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "SegmentPopup": {
                    "height": "100%"
                },
                "SegmentPopup.flxPopup": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "left": "",
                    "top": ""
                },
                "SegmentPopup.imgClose": {
                    "src": "ico_close.png"
                },
                "SegmentPopup2": {
                    "height": "100%",
                    "left": "10dp",
                    "top": "10dp"
                },
                "SegmentPopup2.flxPopup": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "left": "",
                    "top": ""
                },
                "SegmentPopup2.imgClose": {
                    "src": "ico_close.png"
                }
            }
            this.add(flxMessageInfo, uuxNavigationRail, flxMainContainer, flxOutstandingRepaymentOutsideWrapper, flxpayInOutPOP, ErrorAllert, ProgressIndicator, SegmentPopup, SegmentPopup2, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmLoanPayOff,
            "enabledForIdleTimeout": false,
            "id": "frmLoanPayOff",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});