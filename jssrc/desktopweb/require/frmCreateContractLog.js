define("frmCreateContractLog", function() {
    return function(controller) {
        function addWidgetsfrmCreateContractLog() {
            this.setDefaultUnit(kony.flex.DP);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "5%",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0d3724d83eb2b4e"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0h7d9c3def7974c"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_g0dc773a41c94c5f9aaaa0086637ad4d;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_fe2d9f3cb5114e7dbe2d8c6113b2ca05;
            var flxMainContainerWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "minHeight": 968,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknflxScrlBGf2f7fd",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxMainContainerWrapper.setDefaultUnit(kony.flex.DP);
            var flxContentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "128dp",
                "clipBounds": false,
                "id": "flxContentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "minHeight": "850dp",
                "isModalContainer": false,
                "skin": "sknflxBGffffffBorderCCE3F7",
                "top": "75dp",
                "width": "641dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxContentWrapper.setDefaultUnit(kony.flex.DP);
            var flxInnerContentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "800dp",
                "id": "flxInnerContentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "50dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "541dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxInnerContentWrapper.setDefaultUnit(kony.flex.DP);
            var lblCreateLogInfo = new kony.ui.Label({
                "height": "20dp",
                "id": "lblCreateLogInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl16pxBG003E75",
                "text": "Create Log",
                "top": "0dp",
                "width": "81dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSeparatorGrey979797",
                "top": "16dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            var flxContactClientWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxContactClientWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "24dp",
                "width": "530dp",
                "zIndex": 1
            }, {}, {});
            flxContactClientWrapper.setDefaultUnit(kony.flex.DP);
            var cusTextContactClient = new kony.ui.CustomWidget({
                "id": "cusTextContactClient",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "480dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Contact Client",
                "maxLength": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            var imgSearch = new kony.ui.Image2({
                "height": "40dp",
                "id": "imgSearch",
                "isVisible": true,
                "left": "10dp",
                "skin": "slImage",
                "src": "search.png",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 5
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContactClientWrapper.add(cusTextContactClient, imgSearch);
            var cusTextAreaRequestDescription = new kony.ui.CustomWidget({
                "id": "cusTextAreaRequestDescription",
                "isVisible": true,
                "left": "0dp",
                "top": "30dp",
                "width": "530dp",
                "height": "119dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextArea",
                "disabled": false,
                "labelText": "Request Description",
                "maxLength": 256,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            var cusTextAreaAdditionalNotes = new kony.ui.CustomWidget({
                "id": "cusTextAreaAdditionalNotes",
                "isVisible": true,
                "left": "0dp",
                "top": "30dp",
                "width": "530dp",
                "height": "119dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextArea",
                "disabled": false,
                "labelText": "Additional Notes",
                "maxLength": 40,
                "validateRequired": "",
                "validationMessage": "",
                "value": ""
            });
            var flxTypeChannelWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "55dp",
                "id": "flxTypeChannelWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxTypeChannelWrapper.setDefaultUnit(kony.flex.DP);
            var cusDropDownContractType = new kony.ui.CustomWidget({
                "id": "cusDropDownContractType",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            var cusDropDownContractChannel = new kony.ui.CustomWidget({
                "id": "cusDropDownContractChannel",
                "isVisible": true,
                "right": "0dp",
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            flxTypeChannelWrapper.add(cusDropDownContractType, cusDropDownContractChannel);
            var flxDateTimeWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "55dp",
                "id": "flxDateTimeWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxDateTimeWrapper.setDefaultUnit(kony.flex.DP);
            var cusCalendarContractDate = new kony.ui.CustomWidget({
                "id": "cusCalendarContractDate",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Contact Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusTextContractTime = new kony.ui.CustomWidget({
                "id": "cusTextContractTime",
                "isVisible": true,
                "right": 0,
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "time"
                },
                "iconButtonIcon": "",
                "labelText": "Contact Time",
                "maxLength": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            flxDateTimeWrapper.add(cusCalendarContractDate, cusTextContractTime);
            var flxStaffStatusWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "55dp",
                "id": "flxStaffStatusWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxStaffStatusWrapper.setDefaultUnit(kony.flex.DP);
            var cusDropDownContractStaff = new kony.ui.CustomWidget({
                "id": "cusDropDownContractStaff",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var cusDropDownContractStatus = new kony.ui.CustomWidget({
                "id": "cusDropDownContractStatus",
                "isVisible": true,
                "right": "0dp",
                "top": "0dp",
                "width": "250dp",
                "height": "55dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            flxStaffStatusWrapper.add(cusDropDownContractStaff, cusDropDownContractStatus);
            var lblErrorMsg = new kony.ui.Label({
                "height": "40dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtonGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "40dp",
                "id": "flxButtonGroup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtonGroup.setDefaultUnit(kony.flex.DP);
            var cusButtonCancel = new com.temenos.uuxButton({
                "height": "40dp",
                "id": "cusButtonCancel",
                "isVisible": true,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "179dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "147dp",
                "overrides": {
                    "uuxButton": {
                        "left": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "UUXButtonWrapper": {
                        "compact": false,
                        "cta": false,
                        "labelText": "Cancel",
                        "outlined": true,
                        "size": {
                            "optionsList": ["small", "medium", "large", "X-large"],
                            "selectedValue": "large"
                        },
                        "typeButton": {
                            "optionsList": ["button", "submit", "reset"],
                            "selectedValue": "submit"
                        }
                    }
                }
            });
            var cusButtonSubmit = new com.temenos.uuxButton({
                "height": "40dp",
                "id": "cusButtonSubmit",
                "isVisible": true,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "147dp",
                "overrides": {
                    "uuxButton": {
                        "left": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {
                    "UUXButtonWrapper": {
                        "cta": true,
                        "labelText": "Submit",
                        "outlined": true,
                        "size": {
                            "optionsList": ["small", "medium", "large", "X-large"],
                            "selectedValue": "large"
                        }
                    }
                }
            });
            flxButtonGroup.add(cusButtonCancel, cusButtonSubmit);
            flxInnerContentWrapper.add(lblCreateLogInfo, flxLine, flxContactClientWrapper, cusTextAreaRequestDescription, cusTextAreaAdditionalNotes, flxTypeChannelWrapper, flxDateTimeWrapper, flxStaffStatusWrapper, lblErrorMsg, flxButtonGroup);
            flxContentWrapper.add(flxInnerContentWrapper);
            var flxDummy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxDummy",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "925dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDummy.setDefaultUnit(kony.flex.DP);
            flxDummy.add();
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "200dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "112dp",
                "skin": "sknLbl12pxBG4D4D4D",
                "text": "Create Log",
                "top": "26dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false
                    },
                    "imgCloseBackup": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContainerWrapper.add(flxContentWrapper, flxDummy, flxHeaderMenu, ErrorAllert);
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "height": "100%",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0d3724d83eb2b4e",
                    "skin2": "CopydefBtnNormal0h7d9c3def7974c",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "cusButtonCancel": {
                    "left": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "cusButtonSubmit": {
                    "left": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ErrorAllert.imgCloseBackup": {
                    "src": "ico_close.png"
                },
                "SegmentPopup": {
                    "height": "100%"
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(uuxNavigationRail, flxMainContainerWrapper, SegmentPopup, ProgressIndicator, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmCreateContractLog,
            "enabledForIdleTimeout": false,
            "id": "frmCreateContractLog",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});