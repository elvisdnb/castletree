define("frmBlockUnblockAccounts", function() {
    return function(controller) {
        function addWidgetsfrmBlockUnblockAccounts() {
            this.setDefaultUnit(kony.flex.DP);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0%",
                "width": "65dp",
                "overrides": {
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "6.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "91%",
                "zIndex": 10,
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "6.50%",
                        "right": "viz.val_cleared",
                        "width": "91%",
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "65dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0c444a067f87d41",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderBlockUnblockMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderBlockUnblockMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "96%",
                "zIndex": 2
            }, {}, {});
            flxHeaderBlockUnblockMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsHeaderButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsHeaderButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentHeaderButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentHeaderButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBackContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBackContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "200dp",
                "zIndex": 5
            }, {}, {});
            flxBackContainer.setDefaultUnit(kony.flex.DP);
            var cusBackIcons = new kony.ui.CustomWidget({
                "id": "cusBackIcons",
                "isVisible": true,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHome = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHome",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplash = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplash",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverview = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverview",
                "isVisible": true,
                "left": "112dp",
                "skin": "sknLbl12pxBG4D4D4D",
                "text": "Overview",
                "top": "26dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBackContainer.add(cusBackIcons, lblBreadBoardHome, lblBreadBoardSplash, lblBreadBoardOverview);
            flxHeaderBlockUnblockMenu.add(cusFillDetailsHeaderButton, cusViewDocumentHeaderButton, flxBackContainer);
            var flxBlockUnblockChange = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "840dp",
                "id": "flxBlockUnblockChange",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxBlockUnblockChange.setDefaultUnit(kony.flex.DP);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "1%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "0dp",
                "width": "99%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var MainTabs = new com.lending.MainTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "157dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "MainTabs": {
                        "left": "0dp",
                        "top": "157dp",
                        "width": "100%",
                        "zIndex": 1
                    },
                    "flxSelectedLine": {
                        "isVisible": false,
                        "left": "-307dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var HeaderBlockUnlock = new com.konymp.HeaderBlockUnlock({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "HeaderBlockUnlock",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "221dp",
                "width": "99%",
                "zIndex": 1,
                "overrides": {
                    "HeaderBlockUnlock": {
                        "centerX": "viz.val_cleared",
                        "height": "128dp",
                        "left": "1%",
                        "top": "221dp",
                        "width": "99%",
                        "zIndex": 1
                    },
                    "flxAccountBlockUnblockDetailsvalue": {
                        "height": "65dp",
                        "top": "63dp"
                    },
                    "flxButtomLine": {
                        "left": "-172dp"
                    },
                    "flxListBoxSelectedValue": {
                        "left": "34dp",
                        "width": "180dp"
                    },
                    "lblAccountNumberResponse": {
                        "text": "Account Number"
                    },
                    "lblAccountTypeResponse": {
                        "text": "Account Type"
                    },
                    "lblAmountValueInfo": {
                        "left": "24dp",
                        "width": "220dp"
                    },
                    "lblAmountValueResponse": {
                        "left": "24dp",
                        "text": "0.00",
                        "width": "128dp"
                    },
                    "lblAvailableLimitInfo": {
                        "left": "28dp"
                    },
                    "lblAvailableLimitResponse": {
                        "left": "28dp",
                        "text": "0.00"
                    },
                    "lblCurrencyValueResponse": {
                        "text": "CCY"
                    },
                    "lblFundHeldResponse": {
                        "left": "8dp",
                        "text": "Funds held!",
                        "width": "84dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxBlockLabels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "88dp",
                "id": "flxBlockLabels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e4e26b9b45fe4e",
                "top": "349dp",
                "width": "99%",
                "zIndex": 1
            }, {}, {});
            flxBlockLabels.setDefaultUnit(kony.flex.DP);
            var lblBlockingHistoryDetail = new kony.ui.Label({
                "height": "21dp",
                "id": "lblBlockingHistoryDetail",
                "isVisible": true,
                "left": "2%",
                "skin": "sknHeader17PxBg000000",
                "text": "Blocking History",
                "top": "24dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBlockFundsDetail = new kony.ui.Label({
                "height": "21dp",
                "id": "lblBlockFundsDetail",
                "isVisible": true,
                "left": "63%",
                "skin": "sknHeader17PxBg000000",
                "text": "Block Funds",
                "top": "24dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSaperatorLine1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "2dp",
                "id": "flxSaperatorLine1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "sknSeparatorGrey979797",
                "width": "59%",
                "zIndex": 1
            }, {}, {});
            flxSaperatorLine1.setDefaultUnit(kony.flex.DP);
            flxSaperatorLine1.add();
            var flxSaperatorLine2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "2dp",
                "id": "flxSaperatorLine2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "63%",
                "isModalContainer": false,
                "skin": "sknSeparatorGrey979797",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxSaperatorLine2.setDefaultUnit(kony.flex.DP);
            flxSaperatorLine2.add();
            var lblRefNoBlockFundsHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRefNoBlockFundsHeader",
                "isVisible": false,
                "left": "1033dp",
                "skin": "sknlbl12Px2E00DABgPurple",
                "text": "Ref No",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRefNoBlockFundsHeaderResponse = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRefNoBlockFundsHeaderResponse",
                "isVisible": false,
                "left": "1078dp",
                "skin": "sknlbl12Px000000BgBlack",
                "text": "AQW2954R66Y390",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBlockLabels.add(lblBlockingHistoryDetail, lblBlockFundsDetail, flxSaperatorLine1, flxSaperatorLine2, lblRefNoBlockFundsHeader, lblRefNoBlockFundsHeaderResponse);
            var flxBlockFundDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "387dp",
                "id": "flxBlockFundDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e4e26b9b45fe4e",
                "top": "435dp",
                "width": "99%",
                "zIndex": 1
            }, {}, {});
            flxBlockFundDetails.setDefaultUnit(kony.flex.DP);
            var BlockUnblockLabels = new com.konymp.BlockUnblockLabels({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "94%",
                "id": "BlockUnblockLabels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F2FF",
                "top": "0dp",
                "width": "59%",
                "zIndex": 1,
                "overrides": {
                    "BlockUnblockLabels": {
                        "height": "94%",
                        "left": "2%",
                        "top": "0dp",
                        "width": "59%"
                    },
                    "flxBlockFundsList": {
                        "height": "312dp"
                    },
                    "lblNoRecord": {
                        "text": "No funds blocked for this account"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var cusDateFromDate = new kony.ui.CustomWidget({
                "id": "cusDateFromDate",
                "isVisible": true,
                "left": "63%",
                "top": "0dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Start Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusDatePickerEnd = new kony.ui.CustomWidget({
                "id": "cusDatePickerEnd",
                "isVisible": true,
                "left": "82%",
                "top": "0dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "To Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "",
                "weekLabel": "Wk"
            });
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "48dp",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "63%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var cusAmountBlockAmount = new kony.ui.CustomWidget({
                "id": "cusAmountBlockAmount",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "100%",
                "height": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 0,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            flxAmount.add(cusAmountBlockAmount);
            var flxReasonDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "49dp",
                "id": "flxReasonDropdown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "81.90%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "79dp",
                "width": "204dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxReasonDropdown.setDefaultUnit(kony.flex.DP);
            flxReasonDropdown.add();
            var cusDropdownReasons = new kony.ui.CustomWidget({
                "id": "cusDropdownReasons",
                "isVisible": true,
                "left": "82%",
                "top": "80dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Reason",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var lblErrorMessage = new kony.ui.Label({
                "height": "24dp",
                "id": "lblErrorMessage",
                "isVisible": false,
                "left": "63%",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "140dp",
                "width": "320dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubmit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "64dp",
                "id": "flxSubmit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "63%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "200dp",
                "width": "36%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxSubmit.setDefaultUnit(kony.flex.DP);
            var custButtonCancel = new kony.ui.CustomWidget({
                "id": "custButtonCancel",
                "isVisible": true,
                "left": "40dp",
                "width": "184dp",
                "height": "46dp",
                "minWidth": 184,
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custButtonConfirm = new kony.ui.CustomWidget({
                "id": "custButtonConfirm",
                "isVisible": true,
                "left": "240dp",
                "width": "184dp",
                "height": "46dp",
                "minWidth": 184,
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxSubmit.add(custButtonCancel, custButtonConfirm);
            flxBlockFundDetails.add(BlockUnblockLabels, cusDateFromDate, cusDatePickerEnd, flxAmount, flxReasonDropdown, cusDropdownReasons, lblErrorMessage, flxSubmit);
            flxBlockUnblockChange.add(custInfo, MainTabs, HeaderBlockUnlock, flxBlockLabels, flxBlockFundDetails);
            flxMainContainer.add(flxHeaderBlockUnblockMenu, flxBlockUnblockChange);
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var OverridePopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "968dp",
                "id": "OverridePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "isVisible": false
                    },
                    "flxPopup": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "left": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ErrorAllert": {
                    "left": "6.50%",
                    "right": "",
                    "width": "91%",
                    "zIndex": 10
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "MainTabs": {
                    "left": "0dp",
                    "top": "157dp",
                    "width": "100%",
                    "zIndex": 1
                },
                "MainTabs.flxSelectedLine": {
                    "left": "-307dp"
                },
                "HeaderBlockUnlock": {
                    "centerX": "",
                    "height": "128dp",
                    "left": "1%",
                    "top": "221dp",
                    "width": "99%",
                    "zIndex": 1
                },
                "HeaderBlockUnlock.flxAccountBlockUnblockDetailsvalue": {
                    "height": "65dp",
                    "top": "63dp"
                },
                "HeaderBlockUnlock.flxButtomLine": {
                    "left": "-172dp"
                },
                "HeaderBlockUnlock.flxListBoxSelectedValue": {
                    "left": "34dp",
                    "width": "180dp"
                },
                "HeaderBlockUnlock.lblAccountNumberResponse": {
                    "text": "Account Number"
                },
                "HeaderBlockUnlock.lblAccountTypeResponse": {
                    "text": "Account Type"
                },
                "HeaderBlockUnlock.lblAmountValueInfo": {
                    "left": "24dp",
                    "width": "220dp"
                },
                "HeaderBlockUnlock.lblAmountValueResponse": {
                    "left": "24dp",
                    "text": "0.00",
                    "width": "128dp"
                },
                "HeaderBlockUnlock.lblAvailableLimitInfo": {
                    "left": "28dp"
                },
                "HeaderBlockUnlock.lblAvailableLimitResponse": {
                    "left": "28dp",
                    "text": "0.00"
                },
                "HeaderBlockUnlock.lblCurrencyValueResponse": {
                    "text": "CCY"
                },
                "HeaderBlockUnlock.lblFundHeldResponse": {
                    "left": "8dp",
                    "text": "Funds held!",
                    "width": "84dp"
                },
                "BlockUnblockLabels": {
                    "height": "94%",
                    "left": "2%",
                    "top": "0dp",
                    "width": "59%"
                },
                "BlockUnblockLabels.flxBlockFundsList": {
                    "height": "312dp"
                },
                "BlockUnblockLabels.lblNoRecord": {
                    "text": "No funds blocked for this account"
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "OverridePopup.flxPopup": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "left": "",
                    "top": ""
                },
                "OverridePopup.imgClose": {
                    "src": "ico_close.png"
                }
            }
            this.add(uuxNavigationRail, ErrorAllert, flxMainContainer, ProgressIndicator, OverridePopup);
        };
        return [{
            "addWidgets": addWidgetsfrmBlockUnblockAccounts,
            "enabledForIdleTimeout": false,
            "id": "frmBlockUnblockAccounts",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});