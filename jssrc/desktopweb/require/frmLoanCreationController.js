define("userfrmLoanCreationController", {
    BeneficiaryName: '',
    addRowArray: [],
    idtocreate: '',
    selectedLoanType: '',
    popupSegData: gblAllaccountarray,
    accttype: '',
    acctflex: '',
    productLoanId: '',
    currencyval: '',
    dataPerPageNumber: 0,
    partyIds: '',
    custName: '',
    Apidate: transactDate,
    loadbasic: 0,
    loanTypeName: '',
    accountFlag: 0,
    currentOverride: {},
    currentOverride2: {},
    overridearray: [],
    overridearray2: [],
    overflag: 0,
    overflag1: 0,
    onNavigate: function(navOvj) {
        this.partyIds = "";
        if (navOvj && navOvj.selectedValues) {
            this.selectedLoanType = navOvj.selectedValues;
            this.productLoanId = navOvj.selectedValues;
            this.partyIds = navOvj.customerId;
        }
        this.bindEvents();
    },
    bindEvents: function() {
        this.view.postShow = this.postShowFunctionCall;
        this.view.custProceed.onclickEvent = this.OnProceedClick;
        this.view.btnRowAdd.onclickEvent = this.AddRowDyn;
        this.view.custSubmit.onclickEvent = this.onSubmitClick;
        this.view.SegmentPopup.btnProceed.onClick = this.onSubmitClick;
        this.view.custCurrency.valueChanged = this.getCurrencyValues;
        this.view.custLoanTerm.valueChanged = this.termValidation;
        this.view.tbxPayOutAccount.downArrowLbl.onTouchEnd = this.acctflexPayout;
        this.view.tbxPayInAccount.downArrowLbl.onTouchEnd = this.acctflexPayout1;
        this.view.flxpayOutSearch.onTouchEnd = this.popupAccountList1;
        this.view.flxpayInSearch.onTouchEnd = this.popupAccountList;
        this.view.lblPaymentSchedule.onTouchEnd = this.PaymentSchedule;
        this.view.SegmentPopup1.btnProceed.onClick = this.PaymentSchedule;
        this.view.RevisedPaymentSchedule.btnNext.onTouchEnd = this.showChangeRequestListData;
        this.view.RevisedPaymentSchedule.btnPrev.onTouchEnd = this.showChangeRequestListData;
        //this.view.flxpayInSearch.onTouchEnd = this.popupAccountList;
        //this.view.popupPayinPayout.txtSearchBox.onKeyUp = this.doCRSearchWithinSeg;
        // this.view.popupPayinPayout.txtSearchBox.onDone = this.doCRSearchWithinSeg;
        this.view.popupPayinPayout.segPayinPayout.onRowClick = this.getSegRowinfo;
        this.view.AccountList.segAccountList.onRowClick = this.getAccountSeginfo;
        this.view.AccountList2.segAccountList.onRowClick = this.getAccountSeginfo1;
        this.view.popupPayinPayout.imgClose.onTouchEnd = this.payInOutPopclose;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.onCloseErrorAllert;
        this.view.RevisedPaymentSchedule.imgClose.onTouchEnd = this.paymentScheduleclose;
        this.view.flxClickEnable.onClick = this.hideAccount;
        //this.view.custCancel.onclickEvent = this.onLoadBasicDetails;
        this.view.custCancel.onclickEvent = this.navtoAccountLsit;
        this.view.popupPayinPayout.imgSrc.onClick = this.doCRSearchWithinSeg;
        this.view.popupPayinPayout.txtSearchBox.onDone = this.doCRSearchWithinSeg;
        this.view.popupPayinPayout.txtSearchBox.onKeyUp = this.doCRSearchWithinSeg;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup1.btnCancel.onClick = this.closeSegmentOverrite1;
        this.view.SegmentPopup1.imgClose.onTouchEnd = this.closeSegmentOverrite1;
        this.view.tbxPayInAccount.txtFloatText.onTouchStart = this.validateFieldState1;
        this.view.tbxPayInAccount.txtFloatText.onTextChange = this.onFocusField1;
        this.view.tbxPayOutAccount.txtFloatText.onTouchStart = this.validateFieldState;
        this.view.tbxPayOutAccount.txtFloatText.onTextChange = this.onFocusField;
        this.view.cusBackIcon.onTouchEnd = this.backnavToDashboard;
        this.view.lblBreadBoardHomeValue.onTouchEnd = this.backnavToDashboard;
        this.view.lblBreadBoardOverview.onTouchEnd = this.backnavigateCustomerAccountDetails;
        this.view.custLoanAmountEUR.currencyCode = "EUR";
        this.view.custLoanAmountUSD.currencyCode = "USD";
    },
    backnavigateCustomerAccountDetails: function() {
        var navToCusLoans = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this.partyIds;
        navObjet.isShowLoans = "true";
        navObjet.isPopupShow = "false";
        navToCusLoans.navigate(navObjet);
    },
    backnavToDashboard: function() {
        var params = {};
        params.isPopupShow = "false";
        var navObject = new kony.mvc.Navigation("frmDashboard");
        navObject.navigate(params);
    },
    postShowFunctionCall: function() {
        this.dataPerPageNumber = 6;
        this.loadBeneficiary();
        this.loadProductCurrency();
        this.onLoadloanSpecifies();
        this.onLoadBasicDetails();
        this.view.custLoanType.value = this.loanTypeName;
        this.view.custCurrency.value = "";
        this.view.TPWBenName.value = "";
        this.view.AccountList.segAccountList.removeAll();
        this.view.AccountList2.segAccountList.removeAll();
        //this.loadPayAccounts();
        // this.loadAllPayAccounts();
        this.view.ErrorAllert.isVisible = false;
        this.view.flxStepClick.skin = "skinBlue";
        this.view.flxStepClick2.skin = "stepUnActive";
        kony.timer.schedule("timer4", this.accountDynamicLoad, 3, false);
        this.view.tbxPayInAccount.onFocusOutValidateField();
        this.view.tbxPayOutAccount.onFocusOutValidateField();
    },
    accountDynamicLoad: function() {
        //kony.print("accountvalue"+JSON.stringify(gblCutacctArray));
        if (gblCutacctArray.length === 0) {
            this.view.AccountList.lblNoAccounts.isVisible = true;
            this.view.AccountList2.lblNoAccounts.isVisible = true;
            this.view.AccountList.segAccountList.isVisible = false;
            this.view.AccountList2.segAccountList.isVisible = false;
        } else {
            this.view.AccountList.segAccountList.widgetDataMap = gblCutacctWidgetDate;
            this.view.AccountList.segAccountList.setData(gblCutacctArray);
            this.view.AccountList2.segAccountList.widgetDataMap = gblCutacctWidgetDate;
            this.view.AccountList2.segAccountList.setData(gblCutacctArray);
            this.view.AccountList.lblNoAccounts.isVisible = false;
            this.view.AccountList2.lblNoAccounts.isVisible = false;
            this.view.AccountList.segAccountList.isVisible = true;
            this.view.AccountList2.segAccountList.isVisible = true;
        }
    },
    navtoAccountLsit: function() {
        if (this.loadbasic === 0) {
            var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObjet = {};
            navObjet.customerId = this.partyIds;
            navObjet.isShowLoans = "true";
            navObjet.messageInfo = "";
            navToChangeInterest1.navigate(navObjet);
        } else {
            this.onLoadBasicDetails();
        }
    },
    onLoadBasicDetails: function() {
        if (this.selectedLoanType === "PERSONAL.LOAN.STANDARD") {
            this.loanTypeName = "Personal Loan";
            this.productName = "personalLoans";
        } else {
            this.loanTypeName = "Mortgage Loan";
            this.productName = "mortgages";
        }
        this.loadbasic = 0;
        //this.view.custLoanType.value =  this.selectedLoanType;
        //this.view.custLoanType.disabled=true;
        this.view.frmLoanSpecifiesTab.isVisible = false;
        //this.view.TPWBenName.disabled=true;
        //     this.view.TPWBenName.iconButtonIcon="search";
        //     this.view.TPWBenName.showIconButtonTrailing=true;
        //this.view.TPWBenRole.disabled=true;
        //     this.view.TPWBenRole.iconButtonIcon="search";
        //     this.view.TPWBenRole.showIconButtonTrailing=true;
        this.view.TPWFlag1.color = "temenos-lightest";
        this.view.TPWFlag2.color = "temenos-primary";
        this.view.TPWArrow.color = "temenos-primary";
        this.view.iconPayoutsearch.color = "temenos-light";
        this.view.iconPayinsearch.color = "temenos-light";
        this.view.custSubmit.isVisible = false;
        this.view.flxSeePayment.isVisible = false;
        this.view.custCurrency.isVisible = true;
        this.view.custPayout.isVisible = false;
        this.view.custPayIn.isVisible = false;
        this.view.flxBasicDetails.isVisible = true;
        this.view.custProceed.isVisible = true;
        this.view.flxPayinComp.isVisible = false;
        this.view.flxPayoutComp.isVisible = false;
    },
    onLoadloanSpecifies: function() {
        //this.view.custLoanTerm.options = '[{"label": "Select","value":"None"},{"label": "1Y","value":"1Y"},{"label": "2Y","value":"2Y"}]';
        // this.view.custRepaymentFrequency.options = '[{"label": "Select","value":"None"},{"label": "e0Y e1M e0W e0D e0F","value":"e0Y e1M e0W e0D e0F"}]';
        //this.view.custPrincipalInterest.disabled=true;
        //this.view.custPenaltyInterest.disabled=true;
        //this.view.custMatDate.disabled=true;
        //this.view.custRepaymentFrequency.value="";
        // this.view.custLoanAmount.iconButtonIcon="attach_money";
        //this.view.custLoanAmount.showIconButtonTrailing=true;
    },
    payInOutPopclose: function() {
        this.view.flxpayInOutPOP.isVisible = false;
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate;
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    },
    paymentScheduleclose: function() {
        this.view.RevisedPaymentSchedule.isVisible = false;
    },
    acctflexPayout: function() {
        this.view.flxPayinComp.isVisible = false;
        this.acctflex = "payout";
        if (this.view.flxPayoutComp.isVisible) {
            this.view.flxPayoutComp.isVisible = false;
        } else {
            this.view.flxPayoutComp.isVisible = true;
        }
        //this.view.flxClickEnable.left="-5%";
    },
    acctflexPayout1: function() {
        this.acctflex = "payin";
        this.view.flxPayoutComp.isVisible = false;
        if (this.view.flxPayinComp.isVisible) {
            this.view.flxPayinComp.isVisible = false;
        } else {
            this.view.flxPayinComp.isVisible = true;
        }
        //this.view.flxClickEnable.left="-5%";
    },
    popupAccountList: function() {
        this.view.flxPayinComp.isVisible = false;
        this.view.flxPayoutComp.isVisible = false;
        this.accttype = "payout";
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate;
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        this.view.ProgressIndicator.isVisible = true;
        this.view.flxpayInOutPOP.isVisible = true;
        this.view.ProgressIndicator.isVisible = false;
    },
    popupAccountList1: function() {
        this.view.flxPayinComp.isVisible = false;
        this.view.flxPayoutComp.isVisible = false;
        this.accttype = "payin";
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.ProgressIndicator.isVisible = true;
        this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate;
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        this.view.flxpayInOutPOP.isVisible = true;
        this.view.ProgressIndicator.isVisible = false;
    },
    OnProceedClick: function() {
        var isValidData = this.validateData();
        if (isValidData === 0) {
            this.loadbasic = 1;
            if (this.view.custCurrency.value === "") {
                this.view.lblErrorMsg.isVisible = true;
            } else {
                this.view.frmLoanSpecifiesTab.isVisible = true;
                //this.view.custLoanAmount.currencyCode=this.currencyval;
                if (this.currencyval === "EUR") {
                    this.view.custLoanAmountEUR.value = "";
                    this.view.custLoanAmountUSD.isVisible = false;
                    this.view.custLoanAmountEUR.isVisible = true;
                    //this.view.custLoanAmountUSD.value="-";
                } else {
                    this.view.custLoanAmountUSD.value = "";
                    this.view.custLoanAmountUSD.isVisible = true;
                    this.view.custLoanAmountEUR.isVisible = false;
                    //this.view.custLoanAmountEUR.value="-";
                }
                //this.view.custRepaymentFrequency.value="";
                //this.view.custLoanAmountEUR.value ="";
                //this.view.custLoanAmountUSD.value ="";
                this.view.custLoanTerm.value = "";
                this.view.custMatDate.value = "";
                this.view.tbxPayInAccount.txtFloatText.text = "";
                this.view.tbxPayOutAccount.txtFloatText.text = "";
                this.view.custPrincipalInterest.text = "";
                this.view.custPenaltyInterest.text = "";
                this.loadProductConditions();
                this.view.lblErrorMsg.isVisible = false;
                this.view.custProceed.isVisible = false;
                this.view.custSubmit.isVisible = true;
                this.view.flxBasicDetails.isVisible = false;
                this.view.custPayout.isVisible = true;
                this.view.custPayIn.isVisible = true;
                this.view.custCurrency.isVisible = false;
                this.view.flxSeePayment.isVisible = true;
                this.view.flxStepClick.skin = "skinGreen";
                this.view.flxStepClick2.skin = "skinBlue";
                this.view.TPWFlag2.color = "temenos-lightest";
                this.view.custLoanTerm.validateRequired = 'required';
            }
        }
    },
    getCurrencyValues: function() {
        this.currencyval = this.view.custCurrency.value;
        this.view.lblErrorMsg.isVisible = false;
    },
    closeSegmentOverrite: function() {
        this.view.SegmentPopup.isVisible = false;
        this.currentOverride = {};
        this.view.forceLayout();
    },
    closeSegmentOverrite1: function() {
        this.view.SegmentPopup1.isVisible = false;
        this.currentOverride2 = {};
        this.view.forceLayout();
    },
    validateData: function() {
        var Requiredflag = 0;
        var FormId = document.getElementsByTagName("form")[0].getAttribute("id");
        var form = document.getElementById(FormId);
        var Requiredfilter = form.querySelectorAll('*[required]');
        for (var i = 0; i < Requiredfilter.length; i++) {
            if (Requiredfilter[i].disabled !== true && Requiredfilter[i].value === '') {
                var ElementId = Requiredfilter[i].id;
                var Element = document.getElementById(ElementId);
                Element.reportValidity();
                Requiredflag = 1;
            }
        }
        return Requiredflag;
    },
    onSubmitClick: function() {
        getloanAmount = "";
        if (this.currencyval === "EUR") {
            getloanAmount = this.view.custLoanAmountEUR.value;
        } else {
            getloanAmount = this.view.custLoanAmountUSD.value;
        }
        var loanTerm = this.view.custLoanTerm.value;
        var loanMaturityDate = this.view.custMatDate.value;
        var loanRepayFrequency = this.view.custRepaymentFrequency.value;
        var custPayout = this.view.tbxPayOutAccount.txtFloatText.text;
        var custPayIn = this.view.tbxPayInAccount.txtFloatText.text;
        var isValidData = this.validateData();
        if (isValidData === 0) {
            this.view.ErrorAllert.isVisible = false;
            if (this.view.custLoanTerm.value === "" || getloanAmount === "" || this.view.custRepaymentFrequency.value === "" || (this.view.custRepaymentFrequency.value).toLowerCase() === "none" || this.view.tbxPayOutAccount.txtFloatText.text === "" || this.view.tbxPayOutAccount.txtFloatText.text === "none" || this.view.tbxPayInAccount.txtFloatText.text === "" || this.view.tbxPayInAccount.txtFloatText.text === "none") {
                this.view.lblErrorMsg.isVisible = true;
            } else {
                this.view.lblErrorMsg.isVisible = false;
                this.view.SegmentPopup.isVisible = false;
                var partyRole = this.view.TPWBenRole.value;
                //  var loanType =  this.view.custLoanType.value;
                //var loanPrincipalInterest = this.view.custPrincipalInterest.value;
                //var loanPenaltyInterest = this.view.custPenaltyInterest.value;
                var serviceName = "LendingNew";
                var operationName;
                if (this.productLoanId === "PERSONAL.LOAN.STANDARD") {
                    operationName = "postNewPersonalLoan";
                } else {
                    operationName = "postNewMortgageLoan";
                }
                var headers = "";
                var inputParams = {};
                inputParams.partyId = this.partyIds;
                inputParams.partyRole = "OWNER";
                inputParams.currency = this.currencyval;
                inputParams.arrangementEffectiveDate = this.Apidate.replace(/-/g, "");
                inputParams.amount = getloanAmount;
                inputParams.term = loanTerm;
                inputParams.paymentFrequency = loanRepayFrequency;
                inputParams.payinAccount = custPayIn;
                inputParams.payoutAccount = custPayout;
                if (this.overflag === 1) {
                    for (i = 0; i < this.currentOverride.length; i++) {
                        if (this.currentOverride[i].type === "Warning") {
                            // this.currentOverride[i].options="";           
                            delete this.currentOverride[i].options;
                            // overridearray.push(this.currentOverride[i]["responseCode"]= "RECEIVED");
                        }
                        this.overridearray.push(this.currentOverride[i]);
                    }
                    kony.print(JSON.stringify(this.currentOverride));
                    // alert(JSON.stringify(this.currentOverride));
                    var lastoverval = this.overridearray;
                    kony.print("lastoverval" + JSON.stringify(lastoverval));
                    //this.overridearray.push(lastoverval.toString().replace(/^\[(.+)\]$/,'$1'));
                    // kony.print(JSON.stringify(this.overridearray));
                    inputParams.overrideDetails = lastoverval;
                }
                kony.print(JSON.stringify(inputParams));
                MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackloan, this.failureCallBackloan);
                this.view.ProgressIndicator.isVisible = true;
            }
        } else {
            this.view.lblErrorMsg.isVisible = true;
            if (custPayIn === "") {
                this.view.tbxPayInAccount.validateEMptyFiled();
            }
            if (custPayout === "") {
                this.view.tbxPayOutAccount.validateEMptyFiled();
            }
        }
    },
    successCallBackloan: function(response) {
        this.view.ProgressIndicator.isVisible = false;
        this.currentOverride = {};
        var loanArrangementId = (response.body.arrangementActivity.arrangementId);
        //this.view.flxMessageInfo.isVisible = true;
        var navToLoanPage = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.arrangementID = loanArrangementId;
        navObjet.customerId = this.partyIds;
        navObjet.isShowLoans = "true";
        navObjet.messageInfo = "New loan account " + loanArrangementId + " created for " + this.custName;
        navToLoanPage.navigate(navObjet);
    },
    failureCallBackloan: function(res) {
        this.currentOverride = {};
        this.view.ProgressIndicator.isVisible = false;
        if (Application.validation.isNullUndefinedObj(res.error) !== "") {
            if (res.error.errorDetails && res.error.errorDetails.length !== 0) {
                this.view.ErrorAllert.isVisible = true;
                this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message;
            } else {
                this.view.ErrorAllert.isVisible = true;
                this.view.ErrorAllert.lblMessage.text = res.errmsg;
            }
        }
        if (res.override && !isNullorEmpty(res.override.overrideDetails)) {
            this.overflag = 1;
            var overwriteDetails = [];
            var overWriteData = [];
            var segDataForOverrite = [];
            this.currentOverride = res.override.overrideDetails;
            overwriteDetails = res.override.overrideDetails;
            for (var a in overwriteDetails) {
                overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                var json = {
                    "lblSerial": "*",
                    "lblInfo": overwriteDetails[a].description
                };
                segDataForOverrite.push(json);
            }
            this.view.SegmentPopup.segOverride.widgetDataMap = {
                "lblSerial": "lblSerial",
                "lblInfo": "lblInfo"
            };
            this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
            this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
            this.view.SegmentPopup.isVisible = true;
            kony.print("overWriteData::::" + JSON.stringify(overWriteData));
            kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
            this.view.forceLayout();
        }
    },
    onCloseErrorAllert: function() {
        this.view.ErrorAllert.isVisible = false;
    },
    PaymentSchedule: function() {
        sheduleloanAmount = "";
        if (this.currencyval === "EUR") {
            sheduleloanAmount = this.view.custLoanAmountEUR.value;
        } else {
            sheduleloanAmount = this.view.custLoanAmountUSD.value;
        }
        this.view.ErrorAllert.isVisible = false;
        if (this.view.custLoanTerm.value === "" || sheduleloanAmount === "" || this.view.custRepaymentFrequency.value === "" || (this.view.custRepaymentFrequency.value).toLowerCase() === "none" || this.view.tbxPayOutAccount.txtFloatText.text === "" || this.view.tbxPayOutAccount.txtFloatText.text === "none" || this.view.tbxPayInAccount.txtFloatText.text === "" || this.view.tbxPayInAccount.txtFloatText.text === "none") {
            this.view.lblErrorMsg.isVisible = true;
        } else {
            this.view.lblErrorMsg.isVisible = false;
            this.view.SegmentPopup1.isVisible = false;
            var partyRole = this.view.TPWBenRole.value;
            var loanTerm = this.view.custLoanTerm.value;
            var loanMaturityDate = this.view.custMaturityDate.value;
            var loanRepayFrequency = this.view.custRepaymentFrequency.value;
            var custPayout = this.view.tbxPayOutAccount.txtFloatText.text;
            var custPayIn = this.view.tbxPayInAccount.txtFloatText.text;
            var serviceName = "LendingNew";
            var operationName;
            if (this.productLoanId === "PERSONAL.LOAN.STANDARD") {
                operationName = "postPersonalSimulation";
            } else {
                operationName = "postMortgageSimulation";
            }
            var headers = "";
            var inputParams = {};
            inputParams.partyId = this.partyIds;
            inputParams.partyRole = "OWNER";
            inputParams.currency = this.currencyval;
            inputParams.arrangementEffectiveDate = this.Apidate.replace(/-/g, "");
            inputParams.amount = sheduleloanAmount;
            inputParams.term = loanTerm;
            inputParams.paymentFrequency = loanRepayFrequency;
            inputParams.payinAccount = custPayIn;
            inputParams.payoutAccount = custPayout;
            if (this.overflag1 === 1) {
                for (i = 0; i < this.currentOverride2.length; i++) {
                    if (this.currentOverride2[i].type === "Warning") {
                        delete this.currentOverride2[i].options;
                        //delete this.currentOverride[i].options;
                        // overridearray.push(this.currentOverride[i]["responseCode"]= "RECEIVED");
                    }
                    this.overridearray2.push(this.currentOverride2[i]);
                }
                kony.print(JSON.stringify(this.overridearray2));
                // alert(JSON.stringify(this.currentOverride));
                var lastoverval1 = this.overridearray2;
                kony.print(JSON.stringify(lastoverval1));
                //this.overridearray.push(lastoverval.toString().replace(/^\[(.+)\]$/,'$1'));
                // kony.print(JSON.stringify(this.overridearray));
                inputParams.overrideDetails = lastoverval1;
            }
            kony.print(JSON.stringify(inputParams));
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallPaymentSchedule, this.failureCallPaymentSchedule);
            this.view.ProgressIndicator.isVisible = true;
        }
    },
    successCallPaymentSchedule: function(response) {
        this.currentOverride2 = {};
        this.view.ProgressIndicator.isVisible = false;
        //alert(JSON.stringify(response));
        var poparray = [];
        var responseData = response.schedules;
        if (responseData.length > 0) {
            for (var i in responseData) {
                var jsonData = {
                    lblPaymentDate: responseData[i].paymentDueDate,
                    lblAmount: responseData[i].totalPayment,
                    lblPrincipal: responseData[i].totalPayment,
                    lblInterest: responseData[i].prinipalInterest,
                    lblTax: "0.0",
                    lbltotaloutstanding: responseData[i].outstandingAmount
                };
                poparray.push(jsonData);
            }
        }
        kony.print(JSON.stringify(poparray));
        this._scheduledRevisedPayment = poparray;
        this.showChangeRequestListData("");
    },
    failureCallPaymentSchedule: function(res) {
        this.currentOverride2 = {};
        this.view.ProgressIndicator.isVisible = false;
        if (Application.validation.isNullUndefinedObj(res.errorDetails) !== "") {
            if (res.errorDetails && res.errorDetails.length !== 0) {
                this.view.ErrorAllert.isVisible = true;
                this.view.ErrorAllert.lblMessage.text = res.errorDetails[0].message;
            } else {
                this.view.ErrorAllert.isVisible = true;
                this.view.ErrorAllert.lblMessage.text = res.errmsg;
            }
        }
        if (res.override && !isNullorEmpty(res.override.overrideDetails)) {
            this.overflag1 = 1;
            var overwriteDetails = [];
            var overWriteData = [];
            var segDataForOverrite = [];
            this.currentOverride2 = res.override.overrideDetails;
            overwriteDetails = res.override.overrideDetails;
            for (var a in overwriteDetails) {
                overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                var json = {
                    "lblSerial": "*",
                    "lblInfo": overwriteDetails[a].description
                };
                segDataForOverrite.push(json);
            }
            this.view.SegmentPopup1.segOverride.widgetDataMap = {
                "lblSerial": "lblSerial",
                "lblInfo": "lblInfo"
            };
            this.view.SegmentPopup1.lblPaymentInfo.text = "Override Confirmation";
            this.view.SegmentPopup1.segOverride.setData(segDataForOverrite);
            this.view.SegmentPopup1.isVisible = true;
            kony.print("overWriteData::::" + JSON.stringify(overWriteData));
            kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride2));
            this.view.forceLayout();
        }
    },
    termValidation: function() {
        if (this.view.custLoanTerm.value !== "") {
            var termValues = this.view.custLoanTerm.value;
            var termText = termValues.match(/[a-zA-Z]+/g);
            var termNumber = termValues.match(/\d+/g);
            if (termText !== null && termNumber !== null) {
                //var termString = (termText.toString()).toLowerCase();
                var termString = (termText.toString());
                var termNumberString = termNumber.toString();
                if (termString.length === 1 && (termString === "D" || termString === "M" || termString === "Y" || termString === "W") && termNumberString >= 1 && kony.string.isNumeric(termNumberString)) {
                    this.view.ErrorAllert.isVisible = false;
                    var textvalue = termString;
                    var textnumber = termNumberString;
                    var newdates;
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    //var APIdate='20210128';
                    //         var year = this.Apidate.substring(0, 4);
                    //         var month = this.Apidate.substring(4, 6);
                    //         var day = this.Apidate.substring(6, 8);
                    //         var formattedDate= year+"-"+month+"-"+day;
                    var curDate = new Date(this.Apidate);
                    if (textvalue === "D") {
                        newdates = new Date(curDate.setDate(curDate.getDate() + parseInt(textnumber)));
                    }
                    if (textvalue === "W") {
                        textnumber = textnumber * 7;
                        newdates = new Date(curDate.setDate(curDate.getDate() + parseInt(textnumber)));
                    }
                    if (textvalue === "M") {
                        newdates = new Date(curDate.setMonth(curDate.getMonth() + parseInt(textnumber)));
                    }
                    if (textvalue === "Y") {
                        newdates = new Date(curDate.setFullYear(curDate.getFullYear() + parseInt(textnumber)));
                    }
                    //var txt="";
                    var dd = newdates.getDate();
                    var mm = monthNames[newdates.getMonth()];
                    var yy = newdates.getFullYear();
                    var maturityDate = ("0" + dd).slice(-2) + " " + mm + " " + yy;
                    //this.view.custMaturityDate.displayFormat = "dd-MMM-yyyy";
                    this.view.custMatDate.value = maturityDate;
                    //if(termString=== "w"){txt="W";} if(termString=== "y"){txt="Y";}  if(termString=== "d"){txt="D";}if(termString=== "m"){txt="M";}
                    //this.view.custLoanTerm.value=termNumber+""+txt;
                } else {
                    this.view.custMatDate.value = "-";
                    this.view.ErrorAllert.isVisible = true;
                    this.view.ErrorAllert.lblMessage.text = "Suffix of Loan Term Should be anyone of the D,W,M,Y character &  Should be Capital";
                }
            } else {
                this.view.custMatDate.value = "-";
                this.view.ErrorAllert.isVisible = true;
                this.view.ErrorAllert.lblMessage.text = "Enter the valid Loan Term Format (eg: 10D,10W,10M,10Y)";
            }
        }
    },
    //   loadAddRow:function(){  
    //     var json_data = {
    //       "lblRoles" : {text:this.BeneficiaryName },
    //       "lblRoles1" : {text:"Beneficial Owner"}
    //     };
    //     this.addRowArray.push(json_data);
    //     var widgetData = {
    //       "lblRole" : "lblRoles",
    //       "lblRole1" : "lblRoles1"
    //     };
    //     this.view.segAddRow.rowTemplate = "flxAddRowWrap";
    //     //this.view.cusTierListResponse.value = interestType;
    //     this.view.segAddRow.widgetDataMap = widgetData;
    //     this.view.segAddRow.setData(this.addRowArray);
    //   },
    //   kannan:function(){
    //     var json_data = {
    //       "lblRoles" : "",
    //       "lblRoles1" : ""
    //     };
    //     this.view.segAddRow.addDataAt(json_data,1);  
    //   },
    AddRowDyn: function() {
        this.idtocreate++;
        var flxBenRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65px",
            "id": "flxBenRow" + this.idtocreate,
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxBenRow.setDefaultUnit(kony.flex.DP);
        var TPWBenName = new kony.ui.CustomWidget({
            "id": "TPWBenName" + this.idtocreate,
            "isVisible": true,
            "left": "0",
            "right": "2%",
            "top": "5px",
            "width": "295px",
            "height": "50px",
        }, {
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "skin": "addRowBg"
        }, {
            "widgetName": "uuxTextField",
            "fieldType": {
                "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                "selectedValue": "search"
            },
            "iconButtonIcon": "search",
            "labelText": "Beneficiary Name",
            "showIconButtonTrailing": true,
            "showIconButtonTrailingWithValue": false,
            "value": ""
        });
        var TPWBenRole = new kony.ui.CustomWidget({
            "id": "TPWBenRole" + this.idtocreate,
            "isVisible": true,
            "left": "2%",
            "top": "5px",
            "width": "315px",
            "height": "50px",
        }, {
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false,
            "skin": "addRowBg"
        }, {
            "widgetName": "uuxTextField",
            "fieldType": {
                "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                "selectedValue": "search"
            },
            "iconButtonIcon": "",
            "labelText": "Role",
            "showIconButtonTrailing": false,
            "showIconButtonTrailingWithValue": false,
            "value": ""
        });
        flxBenRow.add(TPWBenName, TPWBenRole);
        this.view.flxTopBenRows.add(flxBenRow);
    },
    loadBeneficiary: function() {
        var custid = this.partyIds;
        var serviceName = "LendingNew";
        var operationName = "getCustomers";
        var headers = "";
        var inputParams = {
            "customerId": custid
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackcustomer, this.failureCallBackfunccustomer);
        this.view.ProgressIndicator.isVisible = true;
    },
    successCallBackcustomer: function(res) {
        //kony.print("response from post increase commitment"+JSON.stringify(res));
        var respone = res;
        var customerName = (respone.customerName);
        this.view.TPWBenName.value = customerName;
        this.view.lblCustomerName.text = customerName;
        this.custName = customerName;
        //this.BeneficiaryName = customerName;
        //this.loadAddRow();
        this.view.ProgressIndicator.isVisible = false;
    },
    failureCallBackfunccustomer: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        alert("Error" + JSON.stringify(res));
    },
    loadProductCurrency: function() {
        var serviceName = "LendingNew";
        var operationName = "getProductCurrency";
        var headers = "";
        var inputParams = {
            "productId": this.productLoanId
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunc1, this.failureCallBackfunc1);
        this.view.ProgressIndicator.isVisible = true;
    },
    successCallBackfunc1: function(response) {
        //kony.print("response from post increase commitment"+JSON.stringify(res));
        var currencyResponse = response.ccySpecific.length;
        //alert(JSON.stringify(currencyArray));
        currencyArray = [{
            "label": "",
            "value": ""
        }];
        for (i = 0; i < currencyResponse; i++) {
            currencyArray.push({
                label: response.ccySpecific[i].currency,
                value: response.ccySpecific[i].currency
            });
            this.view.custCurrency.options = JSON.stringify(currencyArray);
        }
        this.view.ProgressIndicator.isVisible = false;
    },
    failureCallBackfunc1: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        //alert("Error in Fetching getProductCurrency "+JSON.stringify(res));
        if (res.error && res.error.length !== 0) {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = res.error.message;
        }
    },
    loadProductConditions: function() {
        var serviceName = "LendingNew";
        var operationName = "getProductConditions";
        var headers = "";
        var inputParams = {
            "pid": this.productLoanId,
            "pname": this.productName
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfuncPc, this.failureCallBackfuncPc);
        this.view.ProgressIndicator.isVisible = true;
    },
    successCallBackfuncPc: function(response) {
        var res = response.body;
        var arrayindex = res.findIndex(this.findArrayindex);
        var PrincipalInterest = response.body[arrayindex].principal + "% - " + response.body[arrayindex].rateType + " Rate";
        var PenaltyInterest = response.body[arrayindex].interestRate + "% - " + response.body[arrayindex].rateType + " Rate";
        var Repayment = response.body[arrayindex].frequency;
        //alert(Repayment);
        this.view.custPrincipalInterest.value = PrincipalInterest;
        this.view.custPenaltyInterest.value = PenaltyInterest;
        //this.view.custRepaymentFrequency.value="e0Y e1M e0W e0D e0F";
        this.view.custRepaymentFrequency.value = (Repayment === "Monthly") ? "e0Y e1M e0W e0D e0F" : "";
        //TODO : DV - default the service response frequency
        // For not not defaulting to anything
        //DV//this.view.custRepaymentFrequency.options = '[{"label": "Select","value":"None"},{"label":"'+Repayment+'","value":"e0Y e1M e0W e0D e0F"}]';
        //DV//this.view.custRepaymentFrequency.value = Repayment;
        //this.view.custLoanTerm.value="test";
        //alert(+response.body[0].principal[0]); 
        this.view.ProgressIndicator.isVisible = false;
    },
    findArrayindex: function(res1) {
        var currvalue = this.currencyval;
        return res1.currency === currvalue;
    },
    failureCallBackfuncPc: function(response) {
        this.view.ProgressIndicator.isVisible = false;
        if (response.error && response.error.length !== 0) {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = response.error.message;
        }
    },
    getAccountSeginfo: function() {
        var getAcctId = this.view.AccountList.segAccountList.selectedRowItems[0].accountid;
        if (this.acctflex === "payin") {
            this.view.tbxPayInAccount.txtFloatText.text = getAcctId;
            this.view.tbxPayInAccount.animateComponent();
            this.view.tbxPayInAccount.onFocusOutValidateField();
        }
        this.view.flxPayinComp.isVisible = false;
        this.view.flxClickEnable.left = "-210%";
    },
    getAccountSeginfo1: function() {
        var getAcctId = this.view.AccountList2.segAccountList.selectedRowItems[0].accountid;
        if (this.acctflex === "payout") {
            this.view.tbxPayOutAccount.txtFloatText.text = getAcctId;
            this.view.tbxPayOutAccount.animateComponent();
            this.view.tbxPayOutAccount.onFocusOutValidateField();
        }
        this.view.flxPayoutComp.isVisible = false;
        this.view.flxClickEnable.left = "-210%";
    },
    getSegRowinfo: function() {
        var getAcctId = this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].accountid;
        if (this.accttype === "payout") {
            this.view.tbxPayOutAccount.txtFloatText.text = getAcctId;
            this.view.tbxPayOutAccount.animateComponent();
            this.view.tbxPayOutAccount.onFocusOutValidateField();
        } else {
            this.view.tbxPayInAccount.txtFloatText.text = getAcctId; //
            this.view.tbxPayInAccount.animateComponent();
            this.view.tbxPayInAccount.onFocusOutValidateField();
        }
        this.view.flxpayInOutPOP.isVisible = false;
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate;
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    },
    showPaginationSegmentData: function(dataPerPage, arrayOfData, btnName) {
        kony.print("showPaginationSegmentData===>");
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.flxPagination.isVisible = true;
            } else {
                this.view.RevisedPaymentSchedule.flxPagination.isVisible = false;
            }
            if (btnName == "btnPrev" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName == "btnNext" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.RevisedPaymentSchedule.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnFocus";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;            
            } else if (this.gblSegPageCount == 1 && this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;           
            } else if (this.gblSegPageCount == 1 && this.totalpage == 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.lblSearchPage.text = "";
            } else if (this.gblSegPageCount == this.totalpage && this.totalpage > 1) {
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(true);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnFocus";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                //this.view.lblSearchPage.isVisible = true;            
            } else {
                this.view.RevisedPaymentSchedule.btnPrev.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnNext.skin = "sknBtnDisable";
                this.view.RevisedPaymentSchedule.btnPrev.setEnabled(false);
                this.view.RevisedPaymentSchedule.btnNext.setEnabled(false);
                this.view.RevisedPaymentSchedule.lblSearchPage.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in HolidayPaymentController next previous method :::" + err);
        }
    },
    showChangeRequestListData: function(buttonevent) {
        kony.print("showChangeRequestListData===>");
        //showloader();
        if (this._scheduledRevisedPayment.length > 0) {
            var segmentData = this.showPaginationSegmentData(this.dataPerPageNumber, this._scheduledRevisedPayment, buttonevent.id);
            this.view.RevisedPaymentSchedule.segRevised.setData(segmentData);
            this.view.RevisedPaymentSchedule.isVisible = true;
            kony.print("segDataSet ====>>" + JSON.stringify(segmentData));
            this.view.forceLayout();
        }
        //dismissLoader();
    },
    hideAccount: function() {
        if (this.view.flxPayoutComp.isVisible) {
            this.view.flxPayoutComp.isVisible = false;
        }
        if (this.view.flxPayinComp.isVisible) {
            this.view.flxPayinComp.isVisible = false;
        }
        this.view.flxClickEnable.left = "-210%";
    },
    doCRSearchWithinSeg: function() {
        kony.print('edit');
        var stringForSearch = this.view.popupPayinPayout.txtSearchBox.text.toLowerCase();
        if (stringForSearch.length === 0) {
            //Reset to show the full claims search results global variable
            // disable the clear search x mark, if any
            this.view.popupPayinPayout.segPayinPayout.removeAll();
            this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        } else {
            //empty search string now
            // visible on for the clear search x mark
        }
        if (stringForSearch.length >= 3) {
            //this.view.popupPayinPayout.segPayinPayout
            var segData = gblAllaccountarray; // make a copy not response
            var finalData = [];
            if (segData && segData === []) {
                return;
            }
            for (var i = 0; i < segData.length; i++) {
                var aRowData = segData[i];
                var aRowDataStrigified = JSON.stringify(aRowData);
                if (aRowDataStrigified.toLowerCase().indexOf(stringForSearch) >= 0) {
                    finalData.push(aRowData);
                } else {
                    //no need to push it - as does not meet criteria
                }
            }
            this.view.popupPayinPayout.segPayinPayout.removeAll();
            this.view.popupPayinPayout.segPayinPayout.setData(finalData);
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    validateFieldState: function() {
        var custPayout = this.view.tbxPayOutAccount.txtFloatText.text;
        if (custPayout === "") {
            this.view.tbxPayOutAccount.validateEMptyFiled();
        }
    },
    validateFieldState1: function() {
        var custPayIn = this.view.tbxPayInAccount.txtFloatText.text;
        if (custPayIn === "") {
            this.view.tbxPayInAccount.validateEMptyFiled();
        }
    },
    onFocusField1: function() {
        this.view.tbxPayInAccount.onFocusOutValidateField();
    },
    onFocusField: function() {
            this.view.tbxPayOutAccount.onFocusOutValidateField();
        }
        //   valueSeperator : function(){  
        //   var incAmount = this.view.custLoanAmount.value;
        //     if(incAmount !== ""){
        //       var Amountvalues = incAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        //       this.view.custLoanAmount.value =Amountvalues;
        //       }    
        //   },
});
define("frmLoanCreationControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_TPW_f937666cd8aa4daebf54a21779a11059: function AS_TPW_f937666cd8aa4daebf54a21779a11059(eventobject) {
        var self = this;
        return
    },
    AS_UWI_a64fb2afbc5c436db0490ee3a7636646: function AS_UWI_a64fb2afbc5c436db0490ee3a7636646(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_f1e6b3c2205a4598a4711462563af3fc: function AS_UWI_f1e6b3c2205a4598a4711462563af3fc(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmLoanCreationController", ["userfrmLoanCreationController", "frmLoanCreationControllerActions"], function() {
    var controller = require("userfrmLoanCreationController");
    var controllerActions = ["frmLoanCreationControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
