define("userCopyflxTemp0d4943827dcd74aController", {
    //Type your controller code here 
    getdata: function(widget, context) {
        //     var eventobj = widget.selectedKey;
        //     if(eventobj === "loanService2") {
        //       var navToChangeInterest = new kony.mvc.Navigation("CustomerLoanInterestChange");
        //       navToChangeInterest.navigate({});
        //     } else if(eventobj === "loanService4") {
        //       var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerLoanIncreaseAmt");
        //       navToChangeInterest1.navigate({});
        //     } else {
        //       kony.application.getCurrentForm().flxOnclickDummy.onClick(context);
        //       this.getLoad();
        //       //kony.application.getCurrentForm().segMin.setVisibility(false); 
        //     }
    },
    getLoad: function() {
        kony.application.getCurrentForm().flxPaymentholidaydetails.setVisibility(false);
        kony.application.showLoadingScreen("blockUI3", "Loading", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {});
        kony.timer.schedule("timerid8", this.dismissPool8, 5, false);
    },
    dismissPool8: function() {
        kony.application.dismissLoadingScreen();
        kony.timer.cancel("timerid8");
        kony.application.getCurrentForm().flxPaymentholidaydetails.setVisibility(true);
        // this.getActivities();
    },
});
define("CopyflxTemp0d4943827dcd74aControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onSelection defined for lblServices **/
    AS_ListBox_b67175ffaba04b1a86f3696d1f7a2aea: function AS_ListBox_b67175ffaba04b1a86f3696d1f7a2aea(eventobject, context) {
        var self = this;
        return self.getdata.call(this, eventobject, context);
    }
});
define("CopyflxTemp0d4943827dcd74aController", ["userCopyflxTemp0d4943827dcd74aController", "CopyflxTemp0d4943827dcd74aControllerActions"], function() {
    var controller = require("userCopyflxTemp0d4943827dcd74aController");
    var controllerActions = ["CopyflxTemp0d4943827dcd74aControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
