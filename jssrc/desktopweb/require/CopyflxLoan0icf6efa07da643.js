define("CopyflxLoan0icf6efa07da643", function() {
    return function(controller) {
        var CopyflxLoan0icf6efa07da643 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "CopyflxLoan0icf6efa07da643",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {
            "hoverSkin": "CopyslFbox0d6f2870369004e"
        });
        CopyflxLoan0icf6efa07da643.setDefaultUnit(kony.flex.DP);
        var lblAccNo = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccNo",
            "isVisible": true,
            "left": "1%",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "50033656",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 2, 0],
            "paddingInPixel": false
        }, {});
        var lblCurrency = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrency",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "USD",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblBalance = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblBalance",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "10000",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        var lblLastActivityDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLastActivityDate",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "10 March 2020",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        CopyflxLoan0icf6efa07da643.add(lblAccNo, lblCurrency, lblBalance, lblLastActivityDate);
        return CopyflxLoan0icf6efa07da643;
    }
})