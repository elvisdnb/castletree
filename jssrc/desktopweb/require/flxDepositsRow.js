define("flxDepositsRow", function() {
    return function(controller) {
        var flxDepositsRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDepositsRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxDepositsRow.setDefaultUnit(kony.flex.DP);
        var flxNow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxNow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 4
        }, {}, {});
        flxNow.setDefaultUnit(kony.flex.DP);
        var lblAcNum = new kony.ui.Label({
            "height": "100%",
            "id": "lblAcNum",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopysknLoanRow0d880c91570f14d",
            "text": "243567891234",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAcType = new kony.ui.Label({
            "height": "100%",
            "id": "lblAcType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "text": "Savings",
            "top": "0dp",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCCY = new kony.ui.Label({
            "height": "100%",
            "id": "lblCCY",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "text": "EUR",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblClearBal = new kony.ui.Label({
            "height": "100%",
            "id": "lblClearBal",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "text": "234,897.00",
            "top": "0dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLockedAc = new kony.ui.Label({
            "height": "100%",
            "id": "lblLockedAc",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "text": "234,897.00",
            "top": "0dp",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAvlBal = new kony.ui.Label({
            "height": "100%",
            "id": "lblAvlBal",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRow",
            "text": "300,897.00",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var ListBox0c039746869c84b = new kony.ui.ListBox({
            "centerY": "50%",
            "focusSkin": "CopydefListBoxFocus0g3b31d79872145",
            "height": "44dp",
            "id": "ListBox0c039746869c84b",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["select", "Services"]
            ],
            "selectedKey": "select",
            "skin": "CopydefListBoxNormal0f85e1426b9f54c",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 1, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        flxNow.add(lblAcNum, lblAcType, lblCCY, lblClearBal, lblLockedAc, lblAvlBal, ListBox0c039746869c84b);
        var flxDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "560px",
            "id": "flxDetails",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "-2dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0cdb6261050ad4f",
            "top": "80dp",
            "width": "100%",
            "zIndex": 3
        }, {}, {});
        flxDetails.setDefaultUnit(kony.flex.DP);
        var flxbgBlue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxbgBlue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "flxLoanHeader",
            "top": "-80dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxbgBlue.setDefaultUnit(kony.flex.DP);
        var lblAcNumOn = new kony.ui.Label({
            "height": "100%",
            "id": "lblAcNumOn",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRowWhite",
            "text": "243567891234",
            "top": "0dp",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAcTypeOn = new kony.ui.Label({
            "height": "100%",
            "id": "lblAcTypeOn",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRowWhite",
            "text": "Savinings",
            "top": "0dp",
            "width": "11%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCCYOn = new kony.ui.Label({
            "height": "100%",
            "id": "lblCCYOn",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRowWhite",
            "text": "EUR",
            "top": "0dp",
            "width": "5.70%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblClearBalOn = new kony.ui.Label({
            "height": "100%",
            "id": "lblClearBalOn",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRowWhite",
            "text": "234,897.00",
            "top": "0dp",
            "width": "12.50%",
            "zIndex": 3
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLockedAcOn = new kony.ui.Label({
            "height": "100%",
            "id": "lblLockedAcOn",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRowWhite",
            "text": "234,897.00",
            "top": "0dp",
            "width": "12.50%",
            "zIndex": 3
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAvlBalOn = new kony.ui.Label({
            "height": "100%",
            "id": "lblAvlBalOn",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLoanRowWhite",
            "text": "300,897.00",
            "top": "0dp",
            "width": "14%",
            "zIndex": 3
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var ListBox0On = new kony.ui.ListBox({
            "centerY": "50%",
            "focusSkin": "defListBoxFocus",
            "height": "44dp",
            "id": "ListBox0On",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["lb1", "Services"],
                ["lb2", "Apply Payment Hiliday"],
                ["lb3", "Change Interest"],
                ["lb4", "Disburse"],
                ["lb5", "Increase Principal"],
                ["lb6", "Payoff"],
                ["lb7", "Repay"]
            ],
            "skin": "CopydefListBoxNormal0f85e1426b9f54c",
            "width": "26%",
            "zIndex": 3
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 1, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        flxbgBlue.add(lblAcNumOn, lblAcTypeOn, lblCCYOn, lblClearBalOn, lblLockedAcOn, lblAvlBalOn, ListBox0On);
        var FlxCurrentPaymentDetailss = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "240px",
            "id": "FlxCurrentPaymentDetailss",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        FlxCurrentPaymentDetailss.setDefaultUnit(kony.flex.DP);
        var lblHdr = new kony.ui.Label({
            "height": "28px",
            "id": "lblHdr",
            "isVisible": true,
            "left": "20px",
            "skin": "skn20pxwhite",
            "text": "Current Payment Details",
            "top": "30px",
            "width": "269px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPayAmount = new kony.ui.Label({
            "height": "28px",
            "id": "lblPayAmount",
            "isVisible": true,
            "left": "20px",
            "skin": "skn15pxwhite",
            "text": "Payment Amount",
            "top": "78px",
            "width": "269px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPayAmountOpted = new kony.ui.Label({
            "centerX": "50%",
            "id": "lblPayAmountOpted",
            "isVisible": true,
            "skin": "skn20pxwhitee",
            "text": "Payment Holiday Opted",
            "top": "140px",
            "width": "169px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEur = new kony.ui.Label({
            "height": "28px",
            "id": "lblEur",
            "isVisible": true,
            "left": "20px",
            "skin": "skn20pxBols",
            "text": "EUR 8,518.20",
            "top": "102px",
            "width": "269px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var CopylblEur0j19f012f82814c = new kony.ui.Label({
            "centerX": "50%",
            "id": "CopylblEur0j19f012f82814c",
            "isVisible": true,
            "left": "27dp",
            "skin": "skn50pxwhite",
            "text": "0",
            "top": "80px",
            "width": "500px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblNext = new kony.ui.Label({
            "height": "20px",
            "id": "lblNext",
            "isVisible": true,
            "left": "20px",
            "skin": "skn15pxwhite",
            "text": "Next Payment Date",
            "top": "156px",
            "width": "148px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var CopylblNext0f22d02f0470044 = new kony.ui.Label({
            "height": "20px",
            "id": "CopylblNext0f22d02f0470044",
            "isVisible": true,
            "left": "20px",
            "skin": "skn15pxBoldwhite",
            "text": "19/12/2020",
            "top": "180px",
            "width": "148px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var CopylblNext0e3352c17f17645 = new kony.ui.Label({
            "height": "20px",
            "id": "CopylblNext0e3352c17f17645",
            "isVisible": true,
            "left": "212dp",
            "skin": "skn15pxBoldwhite",
            "text": "19/20/2021",
            "top": "180px",
            "width": "148px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMaturityDate = new kony.ui.Label({
            "height": "20px",
            "id": "lblMaturityDate",
            "isVisible": true,
            "left": "212dp",
            "skin": "skn15pxwhite",
            "text": "Maturity Date",
            "top": "156px",
            "width": "148px",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCurrentPayment = new kony.ui.Label({
            "centerX": "50%",
            "height": "40dp",
            "id": "lblCurrentPayment",
            "isVisible": true,
            "skin": "sknLtBlue",
            "text": "Current Payment",
            "top": "170dp",
            "width": "180dp",
            "zIndex": 3
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        FlxCurrentPaymentDetailss.add(lblHdr, lblPayAmount, lblPayAmountOpted, lblEur, CopylblEur0j19f012f82814c, lblNext, CopylblNext0f22d02f0470044, CopylblNext0e3352c17f17645, lblMaturityDate, lblCurrentPayment);
        var FlxDAteRecalculate = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "80px",
            "id": "FlxDAteRecalculate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, {}, {});
        FlxDAteRecalculate.setDefaultUnit(kony.flex.DP);
        var lblHoliday = new kony.ui.Label({
            "height": "40px",
            "id": "lblHoliday",
            "isVisible": true,
            "left": "20dp",
            "skin": "CopydefLabel0hd88f370a24544",
            "text": "Holiday Start Date",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 6
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRecalulate = new kony.ui.Label({
            "height": "40px",
            "id": "lblRecalulate",
            "isVisible": true,
            "left": "430dp",
            "skin": "CopydefLabel0hd88f370a24544",
            "text": "Recalculate",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStartDate = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxStartDate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "160dp",
            "isModalContainer": false,
            "skin": "sknDefProfile5",
            "top": "20px",
            "width": "220dp",
            "zIndex": 7
        }, {}, {});
        flxStartDate.setDefaultUnit(kony.flex.DP);
        var Calendar0hdacae9433e24c = new kony.ui.Calendar({
            "calendarIcon": "calendar.png",
            "dateComponents": [24, 11, 2020, 0, 0, 0],
            "dateFormat": "dd/MM/yyyy",
            "day": 24,
            "formattedDate": "24/11/2020",
            "height": "100%",
            "hour": 0,
            "id": "Calendar0hdacae9433e24c",
            "isVisible": true,
            "left": "0dp",
            "minutes": 0,
            "month": 11,
            "seconds": 0,
            "skin": "CopyslCalendar0e8d17ca0b98e48",
            "top": "0dp",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
            "width": "100%",
            "year": 2020,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "noOfMonths": 1
        });
        var lblDefhu = new kony.ui.Label({
            "height": "100%",
            "id": "lblDefhu",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0fe3381987af94d",
            "text": "19 Nov 2020",
            "top": "0dp",
            "width": "100%",
            "zIndex": 3
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [10, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStartDate.add(Calendar0hdacae9433e24c, lblDefhu);
        var flxRecalculate = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxRecalculate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "527dp",
            "isModalContainer": false,
            "skin": "sknDefProfile5",
            "top": "20px",
            "width": "220dp",
            "zIndex": 7
        }, {}, {});
        flxRecalculate.setDefaultUnit(kony.flex.DP);
        var CopyListBox0i4da39afd4404c = new kony.ui.ListBox({
            "focusSkin": "defListBoxFocus",
            "height": "100%",
            "id": "CopyListBox0i4da39afd4404c",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "Payment Amount"],
                ["lb2", "Term"]
            ],
            "skin": "CopydefListBoxNormal0f85e1426b9f54c",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [10, 0, 1, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        flxRecalculate.add(CopyListBox0i4da39afd4404c);
        var btnCancel = new kony.ui.Button({
            "focusSkin": "sknFocus",
            "height": "40px",
            "id": "btnCancel",
            "isVisible": true,
            "right": "240px",
            "skin": "sknLineBtn",
            "text": "Cancel",
            "top": "20px",
            "width": "200dp",
            "zIndex": 7
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnConfirm = new kony.ui.Button({
            "focusSkin": "sknFocus",
            "height": "40px",
            "id": "btnConfirm",
            "isVisible": true,
            "right": "20px",
            "skin": "sknOrangebtn",
            "text": "Confirm",
            "top": "20px",
            "width": "200dp",
            "zIndex": 7
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        FlxDAteRecalculate.add(lblHoliday, lblRecalulate, flxStartDate, flxRecalculate, btnCancel, btnConfirm);
        var flxgraph = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80px",
            "clipBounds": true,
            "height": "250px",
            "id": "flxgraph",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 230,
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxgraph.setDefaultUnit(kony.flex.DP);
        flxgraph.add();
        flxDetails.add(flxbgBlue, FlxCurrentPaymentDetailss, FlxDAteRecalculate, flxgraph);
        var flxdividerline1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "centerX": "50%",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxdividerline1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknLine",
            "width": "98%",
            "zIndex": 2
        }, {}, {});
        flxdividerline1.setDefaultUnit(kony.flex.DP);
        flxdividerline1.add();
        flxDepositsRow.add(flxNow, flxDetails, flxdividerline1);
        return flxDepositsRow;
    }
})