define("frmBlockUnblockDeposits", function() {
    return function(controller) {
        function addWidgetsfrmBlockUnblockDeposits() {
            this.setDefaultUnit(kony.flex.DP);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0%",
                "width": "64dp",
                "overrides": {
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "0dp",
                "width": "93%",
                "zIndex": 2
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var flxDesign = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "82dp",
                "id": "flxDesign",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0j394a0adf4dd4a",
                "top": "0dp",
                "width": "4dp",
                "zIndex": 1
            }, {}, {});
            flxDesign.setDefaultUnit(kony.flex.DP);
            flxDesign.add();
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": false,
                "left": "22dp",
                "width": "24dp",
                "height": "24dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var flxTightIconWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "26dp",
                "id": "flxTightIconWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22dp",
                "isModalContainer": false,
                "skin": "sknFlxGreenRounder",
                "width": "26dp",
                "zIndex": 1
            }, {}, {});
            flxTightIconWrapper.setDefaultUnit(kony.flex.DP);
            var lblTightIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "80%",
                "id": "lblTightIcon",
                "isVisible": true,
                "skin": "sknlblGreenIcon",
                "text": "Z",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTightIconWrapper.add(lblTightIcon);
            var lblSuccessNotification = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageWithReferenceNumber = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMessageWithReferenceNumber",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BGcss",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgHeaderClose1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "14dp",
                "id": "imgHeaderClose1",
                "isVisible": false,
                "right": "24dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "width": "14dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxImageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxImageHeader.setDefaultUnit(kony.flex.DP);
            var imgHeaderClose = new kony.ui.Button({
                "centerX": "50%",
                "centerY": "50%",
                "height": "24dp",
                "id": "imgHeaderClose",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknlblCloseGreen",
                "text": "L",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageHeader.add(imgHeaderClose);
            flxMessageInfo.add(flxDesign, cusStatusIcon, flxTightIconWrapper, lblSuccessNotification, lblMessageWithReferenceNumber, imgHeaderClose1, flxImageHeader);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "92%",
                "zIndex": 10,
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "5.50%",
                        "right": "viz.val_cleared",
                        "width": "92%",
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "64dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0c444a067f87d41",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "72dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "right": "32dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.CustomWidget({
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "0dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopysknLbl0f277218670ea4e",
                "text": "Home",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopysknLbl0a7ae912c8f354a",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopysknLbl0fe4f3bc868e24c",
                "text": "Overview",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(flxBack);
            var flxBlockUnblockChange = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "856dp",
                "id": "flxBlockUnblockChange",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxBlockUnblockChange.setDefaultUnit(kony.flex.DP);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var MainTabs = new com.lending.MainTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-1%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "157dp",
                "width": "101%",
                "zIndex": 1,
                "overrides": {
                    "MainTabs": {
                        "height": "64dp",
                        "left": "-1%",
                        "top": "157dp",
                        "width": "101%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var BlockUnblockHeader = new com.lending.BlockUnblockDetails.Deposits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "129dp",
                "id": "BlockUnblockHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "221dp",
                "width": "100%",
                "overrides": {
                    "Deposits": {
                        "left": "0dp",
                        "top": "221dp"
                    },
                    "FlexContainer0ebe191bc1cf946": {
                        "left": "-160dp",
                        "width": "220dp"
                    },
                    "flxListBoxSelectedValue": {
                        "width": "168dp"
                    },
                    "flxLoanDetailsHeader": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "lblAccountServiceInfo": {
                        "text": "Deposit Services"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Block/UnBlock Funds"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxBlockLabels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "88dp",
                "id": "flxBlockLabels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknFlxMultiBGCB",
                "top": "352dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBlockLabels.setDefaultUnit(kony.flex.DP);
            var flxSaperator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "1dp",
                "id": "flxSaperator1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0cb53db894d3149",
                "top": "2dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSaperator1.setDefaultUnit(kony.flex.DP);
            flxSaperator1.add();
            var lblBlockingHistory = new kony.ui.Label({
                "height": "20dp",
                "id": "lblBlockingHistory",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLbl16px000000BG",
                "text": "Blocking History",
                "top": "24dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBlockFunds = new kony.ui.Label({
                "height": "20dp",
                "id": "lblBlockFunds",
                "isVisible": true,
                "left": "64%",
                "skin": "sknLbl16px000000BG",
                "text": "Block Funds",
                "top": "24dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSaperator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSaperator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0cb53db894d3149",
                "width": "59%",
                "zIndex": 1
            }, {}, {});
            flxSaperator2.setDefaultUnit(kony.flex.DP);
            flxSaperator2.add();
            var flxSaperator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSaperator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64%",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0cb53db894d3149",
                "width": "34%",
                "zIndex": 1
            }, {}, {});
            flxSaperator3.setDefaultUnit(kony.flex.DP);
            flxSaperator3.add();
            flxBlockLabels.add(flxSaperator1, lblBlockingHistory, lblBlockFunds, flxSaperator2, flxSaperator3);
            var flxBlockFundDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "400dp",
                "id": "flxBlockFundDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0e4e26b9b45fe4e",
                "top": "435dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBlockFundDetails.setDefaultUnit(kony.flex.DP);
            var BlockUnblockLabels = new com.konymp.BlockUnblockLabels({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "94%",
                "id": "BlockUnblockLabels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F2FF",
                "top": "0dp",
                "width": "59%",
                "zIndex": 1,
                "overrides": {
                    "BlockUnblockLabels": {
                        "height": "94%",
                        "left": "2%",
                        "top": "0dp",
                        "width": "59%"
                    },
                    "flxBlockFundsList": {
                        "height": "312dp"
                    },
                    "lblNoRecord": {
                        "isVisible": false,
                        "text": "No funds blocked for this account"
                    },
                    "segBlockFundsList": {
                        "data": [{
                            "btnEdit": "",
                            "btnUnblock": "",
                            "lblAmount": "",
                            "lblEndDate": "",
                            "lblReason": "",
                            "lblReferenceNo": "",
                            "lblStartDate": ""
                        }],
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var cusDateFromDate = new kony.ui.CustomWidget({
                "id": "cusDateFromDate",
                "isVisible": true,
                "left": "64%",
                "top": "0dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "From Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var cusDatePickerEnd = new kony.ui.CustomWidget({
                "id": "cusDatePickerEnd",
                "isVisible": true,
                "left": "82%",
                "top": "0dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "To Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "value": "",
                "weekLabel": "Wk"
            });
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "48dp",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var cusAmountBlockAmount = new kony.ui.CustomWidget({
                "id": "cusAmountBlockAmount",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "200dp",
                "height": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "USD",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Block Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "suffix": "",
                "validateRequired": "required",
                "value": ""
            });
            flxAmount.add(cusAmountBlockAmount);
            var flxReasonDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "49dp",
                "id": "flxReasonDropdown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "81.90%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "79dp",
                "width": "204dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxReasonDropdown.setDefaultUnit(kony.flex.DP);
            flxReasonDropdown.add();
            var cusDropdownReasons = new kony.ui.CustomWidget({
                "id": "cusDropdownReasons",
                "isVisible": true,
                "left": "82%",
                "top": "80dp",
                "width": "200dp",
                "height": "48dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Reason",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            var lblErrorMessage = new kony.ui.Label({
                "id": "lblErrorMessage",
                "isVisible": true,
                "left": "63%",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "148dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubmit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxSubmit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "63%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "200dp",
                "width": "37%",
                "zIndex": 1
            }, {}, {});
            flxSubmit.setDefaultUnit(kony.flex.DP);
            var custButtonCancel = new kony.ui.CustomWidget({
                "id": "custButtonCancel",
                "isVisible": true,
                "left": "56dp",
                "width": "184dp",
                "height": "46dp",
                "minWidth": 184,
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custButtonConfirm = new kony.ui.CustomWidget({
                "id": "custButtonConfirm",
                "isVisible": true,
                "left": "248dp",
                "width": "184dp",
                "height": "46dp",
                "minWidth": 184,
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxSubmit.add(custButtonCancel, custButtonConfirm);
            flxBlockFundDetails.add(BlockUnblockLabels, cusDateFromDate, cusDatePickerEnd, flxAmount, flxReasonDropdown, cusDropdownReasons, lblErrorMessage, flxSubmit);
            flxBlockUnblockChange.add(custInfo, MainTabs, BlockUnblockHeader, flxBlockLabels, flxBlockFundDetails);
            flxMainContainer.add(flxHeaderMenu, flxBlockUnblockChange);
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var OverridePopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "OverridePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "height": "100%",
                        "isVisible": false
                    },
                    "flxPopup": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "left": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ErrorAllert": {
                    "left": "5.50%",
                    "right": "",
                    "width": "92%",
                    "zIndex": 10
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "MainTabs": {
                    "height": "64dp",
                    "left": "-1%",
                    "top": "157dp",
                    "width": "101%"
                },
                "BlockUnblockHeader": {
                    "left": "0dp",
                    "top": "221dp"
                },
                "BlockUnblockHeader.FlexContainer0ebe191bc1cf946": {
                    "left": "-160dp",
                    "width": "220dp"
                },
                "BlockUnblockHeader.flxListBoxSelectedValue": {
                    "width": "168dp"
                },
                "BlockUnblockHeader.flxLoanDetailsHeader": {
                    "left": "0dp",
                    "top": "0dp"
                },
                "BlockUnblockHeader.lblAccountServiceInfo": {
                    "text": "Deposit Services"
                },
                "BlockUnblockHeader.lblListBoxSelectedValue": {
                    "text": "Block/UnBlock Funds"
                },
                "BlockUnblockLabels": {
                    "height": "94%",
                    "left": "2%",
                    "top": "0dp",
                    "width": "59%"
                },
                "BlockUnblockLabels.flxBlockFundsList": {
                    "height": "312dp"
                },
                "BlockUnblockLabels.lblNoRecord": {
                    "text": "No funds blocked for this account"
                },
                "BlockUnblockLabels.segBlockFundsList": {
                    "data": [{
                        "btnEdit": "",
                        "btnUnblock": "",
                        "lblAmount": "",
                        "lblEndDate": "",
                        "lblReason": "",
                        "lblReferenceNo": "",
                        "lblStartDate": ""
                    }]
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "OverridePopup": {
                    "height": "100%"
                },
                "OverridePopup.flxPopup": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "left": "",
                    "top": ""
                },
                "OverridePopup.imgClose": {
                    "src": "ico_close.png"
                }
            }
            this.add(uuxNavigationRail, flxMessageInfo, ErrorAllert, flxMainContainer, ProgressIndicator, OverridePopup);
        };
        return [{
            "addWidgets": addWidgetsfrmBlockUnblockDeposits,
            "enabledForIdleTimeout": false,
            "id": "frmBlockUnblockDeposits",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});