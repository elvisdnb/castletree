define({
    appInit: function(params) {
        skinsInit();
        kony.mvc.registry.add("com.customer.relationship.relationship", "relationship", "relationshipController");
        kony.application.registerMaster({
            "namespace": "com.customer.relationship",
            "classname": "relationship",
            "name": "com.customer.relationship.relationship"
        });
        kony.mvc.registry.add("com.documentImages.doc", "doc", "docController");
        kony.application.registerMaster({
            "namespace": "com.documentImages",
            "classname": "doc",
            "name": "com.documentImages.doc"
        });
        kony.mvc.registry.add("com.fulldetails.spinner", "spinner", "spinnerController");
        kony.application.registerMaster({
            "namespace": "com.fulldetails",
            "classname": "spinner",
            "name": "com.fulldetails.spinner"
        });
        kony.mvc.registry.add("com.konymp.AccountList.AccountList", "AccountList", "AccountListController");
        kony.application.registerMaster({
            "namespace": "com.konymp.AccountList",
            "classname": "AccountList",
            "name": "com.konymp.AccountList.AccountList"
        });
        kony.mvc.registry.add("com.konymp.AccountList2.AccountList2", "AccountList2", "AccountList2Controller");
        kony.application.registerMaster({
            "namespace": "com.konymp.AccountList2",
            "classname": "AccountList2",
            "name": "com.konymp.AccountList2.AccountList2"
        });
        kony.mvc.registry.add("com.konymp.BlockUnblockHistoryHeader", "BlockUnblockHistoryHeader", "BlockUnblockHistoryHeaderController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "BlockUnblockHistoryHeader",
            "name": "com.konymp.BlockUnblockHistoryHeader"
        });
        kony.mvc.registry.add("com.konymp.BlockUnblockLabels", "BlockUnblockLabels", "BlockUnblockLabelsController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "BlockUnblockLabels",
            "name": "com.konymp.BlockUnblockLabels"
        });
        kony.mvc.registry.add("com.konymp.donutchart", "donutchart", "donutchartController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "donutchart",
            "name": "com.konymp.donutchart"
        });
        kony.mvc.registry.add("com.konymp.flxoverridepopup.Popup", "Popup", "PopupController");
        kony.application.registerMaster({
            "namespace": "com.konymp.flxoverridepopup",
            "classname": "Popup",
            "name": "com.konymp.flxoverridepopup.Popup"
        });
        kony.mvc.registry.add("com.konymp.flxoverrideSegmentpopup.SegmentPopup", "SegmentPopup", "SegmentPopupController");
        kony.application.registerMaster({
            "namespace": "com.konymp.flxoverrideSegmentpopup",
            "classname": "SegmentPopup",
            "name": "com.konymp.flxoverrideSegmentpopup.SegmentPopup"
        });
        kony.mvc.registry.add("com.konymp.flxoverrideSegmentpopup.SegmentPopupCopy", "SegmentPopupCopy", "SegmentPopupCopyController");
        kony.application.registerMaster({
            "namespace": "com.konymp.flxoverrideSegmentpopup",
            "classname": "SegmentPopupCopy",
            "name": "com.konymp.flxoverrideSegmentpopup.SegmentPopupCopy"
        });
        kony.mvc.registry.add("com.konymp.getStarted", "getStarted", "getStartedController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "getStarted",
            "name": "com.konymp.getStarted"
        });
        kony.mvc.registry.add("com.konymp.HeaderBlockUnlock", "HeaderBlockUnlock", "HeaderBlockUnlockController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "HeaderBlockUnlock",
            "name": "com.konymp.HeaderBlockUnlock"
        });
        kony.mvc.registry.add("com.konymp.help", "help", "helpController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "help",
            "name": "com.konymp.help"
        });
        kony.mvc.registry.add("com.konymp.helpCenter", "helpCenter", "helpCenterController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "helpCenter",
            "name": "com.konymp.helpCenter"
        });
        kony.mvc.registry.add("com.konymp.multiline", "multiline", "multilineController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "multiline",
            "name": "com.konymp.multiline"
        });
        kony.mvc.registry.add("com.konymp.popupPayinPayout.popupPayinPayout", "popupPayinPayout", "popupPayinPayoutController");
        kony.application.registerMaster({
            "namespace": "com.konymp.popupPayinPayout",
            "classname": "popupPayinPayout",
            "name": "com.konymp.popupPayinPayout.popupPayinPayout"
        });
        kony.mvc.registry.add("com.konymp.popupPayinPayout.popupPayinPayoutCopy", "popupPayinPayoutCopy", "popupPayinPayoutCopyController");
        kony.application.registerMaster({
            "namespace": "com.konymp.popupPayinPayout",
            "classname": "popupPayinPayoutCopy",
            "name": "com.konymp.popupPayinPayout.popupPayinPayoutCopy"
        });
        kony.mvc.registry.add("com.konymp.ProgressIndicator", "ProgressIndicator", "ProgressIndicatorController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "ProgressIndicator",
            "name": "com.konymp.ProgressIndicator"
        });
        kony.mvc.registry.add("com.konymp.questions", "questions", "questionsController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "questions",
            "name": "com.konymp.questions"
        });
        kony.mvc.registry.add("com.konymp.segSearch", "segSearch", "segSearchController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "segSearch",
            "name": "com.konymp.segSearch"
        });
        kony.mvc.registry.add("com.konymp.showMore", "showMore", "showMoreController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "showMore",
            "name": "com.konymp.showMore"
        });
        kony.mvc.registry.add("com.konymp.Tabs", "Tabs", "TabsController");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "Tabs",
            "name": "com.konymp.Tabs"
        });
        kony.mvc.registry.add("com.lending.BlockUnblockDetails.Deposits", "Deposits", "DepositsController");
        kony.application.registerMaster({
            "namespace": "com.lending.BlockUnblockDetails",
            "classname": "Deposits",
            "name": "com.lending.BlockUnblockDetails.Deposits"
        });
        kony.mvc.registry.add("com.lending.contractRequestDescription.ContractRequestDescription", "ContractRequestDescription", "ContractRequestDescriptionController");
        kony.application.registerMaster({
            "namespace": "com.lending.contractRequestDescription",
            "classname": "ContractRequestDescription",
            "name": "com.lending.contractRequestDescription.ContractRequestDescription"
        });
        kony.mvc.registry.add("com.lending.customerDetails.custInfo", "custInfo", "custInfoController");
        kony.application.registerMaster({
            "namespace": "com.lending.customerDetails",
            "classname": "custInfo",
            "name": "com.lending.customerDetails.custInfo"
        });
        kony.mvc.registry.add("com.lending.customerDetails.customerDetails", "customerDetails", "customerDetailsController");
        kony.application.registerMaster({
            "namespace": "com.lending.customerDetails",
            "classname": "customerDetails",
            "name": "com.lending.customerDetails.customerDetails"
        });
        kony.mvc.registry.add("com.lending.floatingReadOnlyText.floatReadOnlylText", "floatReadOnlylText", "floatReadOnlylTextController");
        kony.application.registerMaster({
            "namespace": "com.lending.floatingReadOnlyText",
            "classname": "floatReadOnlylText",
            "name": "com.lending.floatingReadOnlyText.floatReadOnlylText"
        });
        kony.mvc.registry.add("com.lending.floatingReadOnlyText.floatReadOnlylTextCopy", "floatReadOnlylTextCopy", "floatReadOnlylTextCopyController");
        kony.application.registerMaster({
            "namespace": "com.lending.floatingReadOnlyText",
            "classname": "floatReadOnlylTextCopy",
            "name": "com.lending.floatingReadOnlyText.floatReadOnlylTextCopy"
        });
        kony.mvc.registry.add("com.lending.floatingTextBox.floatLabelText", "floatLabelText", "floatLabelTextController");
        kony.application.registerMaster({
            "namespace": "com.lending.floatingTextBox",
            "classname": "floatLabelText",
            "name": "com.lending.floatingTextBox.floatLabelText"
        });
        kony.mvc.registry.add("com.lending.floatingTextBox.floatLabelTextCopy", "floatLabelTextCopy", "floatLabelTextCopyController");
        kony.application.registerMaster({
            "namespace": "com.lending.floatingTextBox",
            "classname": "floatLabelTextCopy",
            "name": "com.lending.floatingTextBox.floatLabelTextCopy"
        });
        kony.mvc.registry.add("com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert", "ErrorAllert", "ErrorAllertController");
        kony.application.registerMaster({
            "namespace": "com.lending.FlxErrorAllert.ErrorAllert",
            "classname": "ErrorAllert",
            "name": "com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert"
        });
        kony.mvc.registry.add("com.lending.FlxErrorAllert.ErrorAllert.ErrorAllertCopy", "ErrorAllertCopy", "ErrorAllertCopyController");
        kony.application.registerMaster({
            "namespace": "com.lending.FlxErrorAllert.ErrorAllert",
            "classname": "ErrorAllertCopy",
            "name": "com.lending.FlxErrorAllert.ErrorAllert.ErrorAllertCopy"
        });
        kony.mvc.registry.add("com.lending.FlxTitle.titleLabel.TitleLabel", "TitleLabel", "TitleLabelController");
        kony.application.registerMaster({
            "namespace": "com.lending.FlxTitle.titleLabel",
            "classname": "TitleLabel",
            "name": "com.lending.FlxTitle.titleLabel.TitleLabel"
        });
        kony.mvc.registry.add("com.lending.fundDetails.FundDetails", "FundDetails", "FundDetailsController");
        kony.application.registerMaster({
            "namespace": "com.lending.fundDetails",
            "classname": "FundDetails",
            "name": "com.lending.fundDetails.FundDetails"
        });
        kony.mvc.registry.add("com.lending.loanDetails.loanDetails", "loanDetails", "loanDetailsController");
        kony.application.registerMaster({
            "namespace": "com.lending.loanDetails",
            "classname": "loanDetails",
            "name": "com.lending.loanDetails.loanDetails"
        });
        kony.mvc.registry.add("com.lending.MainTabs", "MainTabs", "MainTabsController");
        kony.application.registerMaster({
            "namespace": "com.lending",
            "classname": "MainTabs",
            "name": "com.lending.MainTabs"
        });
        kony.mvc.registry.add("com.lending.MainTabsCopy", "MainTabsCopy", "MainTabsCopyController");
        kony.application.registerMaster({
            "namespace": "com.lending",
            "classname": "MainTabsCopy",
            "name": "com.lending.MainTabsCopy"
        });
        kony.mvc.registry.add("com.lending.revisedPaymentSchedule.RevisedPaymentSchedule", "RevisedPaymentSchedule", "RevisedPaymentScheduleController");
        kony.application.registerMaster({
            "namespace": "com.lending.revisedPaymentSchedule",
            "classname": "RevisedPaymentSchedule",
            "name": "com.lending.revisedPaymentSchedule.RevisedPaymentSchedule"
        });
        kony.mvc.registry.add("com.lending.uuxProgressIndicator.ProgressIndicator", "ProgressIndicator", "ProgressIndicatorController");
        kony.application.registerMaster({
            "namespace": "com.lending.uuxProgressIndicator",
            "classname": "ProgressIndicator",
            "name": "com.lending.uuxProgressIndicator.ProgressIndicator"
        });
        kony.mvc.registry.add("com.lending.uuxProgressIndicator.ProgressIndicatorCopy", "ProgressIndicatorCopy", "ProgressIndicatorCopyController");
        kony.application.registerMaster({
            "namespace": "com.lending.uuxProgressIndicator",
            "classname": "ProgressIndicatorCopy",
            "name": "com.lending.uuxProgressIndicator.ProgressIndicatorCopy"
        });
        kony.mvc.registry.add("com.temenos.chartjslib.barGraph1", "barGraph1", "barGraph1Controller");
        kony.application.registerMaster({
            "namespace": "com.temenos.chartjslib",
            "classname": "barGraph1",
            "name": "com.temenos.chartjslib.barGraph1"
        });
        kony.mvc.registry.add("com.temenos.chartjslib.barGraph2", "barGraph2", "barGraph2Controller");
        kony.application.registerMaster({
            "namespace": "com.temenos.chartjslib",
            "classname": "barGraph2",
            "name": "com.temenos.chartjslib.barGraph2"
        });
        kony.mvc.registry.add("com.temenos.uuxButton", "uuxButton", "uuxButtonController");
        kony.application.registerMaster({
            "namespace": "com.temenos",
            "classname": "uuxButton",
            "name": "com.temenos.uuxButton"
        });
        kony.mvc.registry.add("com.TitleTop.titleHeader", "titleHeader", "titleHeaderController");
        kony.application.registerMaster({
            "namespace": "com.TitleTop",
            "classname": "titleHeader",
            "name": "com.TitleTop.titleHeader"
        });
        kony.mvc.registry.add("uuxNavigationRailComponent.uuxNavigationRail", "uuxNavigationRail", "uuxNavigationRailController");
        kony.application.registerMaster({
            "namespace": "uuxNavigationRailComponent",
            "classname": "uuxNavigationRail",
            "name": "uuxNavigationRailComponent.uuxNavigationRail"
        });
        kony.mvc.registry.add("uuxNavigationRailComponent.uuxNavigationRailWeb", "uuxNavigationRailWeb", "uuxNavigationRailWebController");
        kony.application.registerMaster({
            "namespace": "uuxNavigationRailComponent",
            "classname": "uuxNavigationRailWeb",
            "name": "uuxNavigationRailComponent.uuxNavigationRailWeb"
        });
        kony.mvc.registry.add("com.search", "search", "searchController");
        kony.application.registerMaster({
            "namespace": "com",
            "classname": "search",
            "name": "com.search"
        });
        kony.mvc.registry.add("fullDetails.CDM", "CDM", "CDMController");
        kony.application.registerMaster({
            "namespace": "fullDetails",
            "classname": "CDM",
            "name": "fullDetails.CDM"
        });
        kony.mvc.registry.add("CopyflxDown", "CopyflxDown", "CopyflxDownController");
        kony.mvc.registry.add("CopyflxParent", "CopyflxParent", "CopyflxParentController");
        kony.mvc.registry.add("CopyflxOverrideTemplate", "CopyflxOverrideTemplate", "CopyflxOverrideTemplateController");
        kony.mvc.registry.add("CopyFlxAccountList", "CopyFlxAccountList", "CopyFlxAccountListController");
        kony.mvc.registry.add("CopyflxRowPayinPayout1", "CopyflxRowPayinPayout1", "CopyflxRowPayinPayout1Controller");
        kony.mvc.registry.add("flxDown", "flxDown", "flxDownController");
        kony.mvc.registry.add("CopyflxLoan0icf6efa07da643", "CopyflxLoan0icf6efa07da643", "CopyflxLoan0icf6efa07da643Controller");
        kony.mvc.registry.add("flxrow1", "flxrow1", "flxrow1Controller");
        kony.mvc.registry.add("flxParent", "flxParent", "flxParentController");
        kony.mvc.registry.add("flxBlockingHistoryDetailsResponse", "flxBlockingHistoryDetailsResponse", "flxBlockingHistoryDetailsResponseController");
        kony.mvc.registry.add("flxNow", "flxNow", "flxNowController");
        kony.mvc.registry.add("flxInitial1", "flxInitial1", "flxInitial1Controller");
        kony.mvc.registry.add("flxAddCutomerRow", "flxAddCutomerRow", "flxAddCutomerRowController");
        kony.mvc.registry.add("flxAddRowWrap", "flxAddRowWrap", "flxAddRowWrapController");
        kony.mvc.registry.add("flxAnswers", "flxAnswers", "flxAnswersController");
        kony.mvc.registry.add("flxDepositsRow", "flxDepositsRow", "flxDepositsRowController");
        kony.mvc.registry.add("CopyflxTemp0d4943827dcd74a", "CopyflxTemp0d4943827dcd74a", "CopyflxTemp0d4943827dcd74aController");
        kony.mvc.registry.add("CopyflxTemp0decad958122647", "CopyflxTemp0decad958122647", "CopyflxTemp0decad958122647Controller");
        kony.mvc.registry.add("flxHdr", "flxHdr", "flxHdrController");
        kony.mvc.registry.add("flxMoreHeader", "flxMoreHeader", "flxMoreHeaderController");
        kony.mvc.registry.add("flxOverrideTemplate", "flxOverrideTemplate", "flxOverrideTemplateController");
        kony.mvc.registry.add("flxPayOffOutstandingBillDetails", "flxPayOffOutstandingBillDetails", "flxPayOffOutstandingBillDetailsController");
        kony.mvc.registry.add("Flex0i494021d9e244d", "Flex0i494021d9e244d", "Flex0i494021d9e244dController");
        kony.mvc.registry.add("flxRowBlockFunds", "flxRowBlockFunds", "flxRowBlockFundsController");
        kony.mvc.registry.add("flxRowSample", "flxRowSample", "flxRowSampleController");
        kony.mvc.registry.add("CopyflxHdr0e952e967ea4c45", "CopyflxHdr0e952e967ea4c45", "CopyflxHdr0e952e967ea4c45Controller");
        kony.mvc.registry.add("flxListCustomers", "flxListCustomers", "flxListCustomersController");
        kony.mvc.registry.add("flxSegDebitAccount", "flxSegDebitAccount", "flxSegDebitAccountController");
        kony.mvc.registry.add("flxSegLoanFixed", "flxSegLoanFixed", "flxSegLoanFixedController");
        kony.mvc.registry.add("flxSegLoanFloating", "flxSegLoanFloating", "flxSegLoanFloatingController");
        kony.mvc.registry.add("flxOutstandingBillDetails", "flxOutstandingBillDetails", "flxOutstandingBillDetailsController");
        kony.mvc.registry.add("flxSegParticularBillDetails", "flxSegParticularBillDetails", "flxSegParticularBillDetailsController");
        kony.mvc.registry.add("flxSegAccNoList", "flxSegAccNoList", "flxSegAccNoListController");
        kony.mvc.registry.add("flxServiceType", "flxServiceType", "flxServiceTypeController");
        kony.mvc.registry.add("flxShowMore", "flxShowMore", "flxShowMoreController");
        kony.mvc.registry.add("flxShowMoreHeader", "flxShowMoreHeader", "flxShowMoreHeaderController");
        kony.mvc.registry.add("FlxAccountList", "FlxAccountList", "FlxAccountListController");
        kony.mvc.registry.add("CustomerBranch", "CustomerBranch", "CustomerBranchController");
        kony.mvc.registry.add("flxLoan7daysMain", "flxLoan7daysMain", "flxLoan7daysMainController");
        kony.mvc.registry.add("flxRowPayinPayout", "flxRowPayinPayout", "flxRowPayinPayoutController");
        kony.mvc.registry.add("flxSearchSegments", "flxSearchSegments", "flxSearchSegmentsController");
        kony.mvc.registry.add("flxSegment", "flxSegment", "flxSegmentController");
        kony.mvc.registry.add("flxTopCust", "flxTopCust", "flxTopCustController");
        kony.mvc.registry.add("frmBlockUnblockAccounts", "frmBlockUnblockAccounts", "frmBlockUnblockAccountsController");
        kony.mvc.registry.add("frmBlockUnblockDeposits", "frmBlockUnblockDeposits", "frmBlockUnblockDepositsController");
        kony.mvc.registry.add("frmChangeInterestPayoutTerms", "frmChangeInterestPayoutTerms", "frmChangeInterestPayoutTermsController");
        kony.mvc.registry.add("createContractLog", "frmCreateContractLog", "frmCreateContractLogController");
        kony.mvc.registry.add("frmCustomerAccountsDetails", "frmCustomerAccountsDetails", "frmCustomerAccountsDetailsController");
        kony.mvc.registry.add("frmCustomerCreation", "frmCustomerCreation", "frmCustomerCreationController");
        kony.mvc.registry.add("frmCustomerLoanIncreaseAmt", "frmCustomerLoanIncreaseAmt", "frmCustomerLoanIncreaseAmtController");
        kony.mvc.registry.add("CustomerLoanInterestChange", "frmCustomerLoanInterestChange", "frmCustomerLoanInterestChangeController");
        kony.mvc.registry.add("frmCustomersList", "frmCustomersList", "frmCustomersListController");
        kony.mvc.registry.add("frmDashboard", "frmDashboard", "frmDashboardController");
        kony.mvc.registry.add("frmDashboardLending", "frmDashboardLending", "frmDashboardLendingController");
        kony.mvc.registry.add("frmDepositCreation", "frmDepositCreation", "frmDepositCreationController");
        kony.mvc.registry.add("frmDepositWithdrawal", "frmDepositWithdrawal", "frmDepositWithdrawalController");
        kony.mvc.registry.add("frmDisburse", "frmDisburse", "frmDisburseController");
        kony.mvc.registry.add("frmFullDetails", "frmFullDetails", "frmFullDetailsController");
        kony.mvc.registry.add("frmFundDeposit", "frmFundDeposit", "frmFundDepositController");
        kony.mvc.registry.add("holidayPayment", "frmHolidayPayment", "frmHolidayPaymentController");
        kony.mvc.registry.add("frmLoanCreation", "frmLoanCreation", "frmLoanCreationController");
        kony.mvc.registry.add("frmLoanPayoff", "frmLoanPayOff", "frmLoanPayOffController");
        kony.mvc.registry.add("frmLogin", "frmLogin", "frmLoginController");
        kony.mvc.registry.add("frmpopups", "frmpopups", "frmpopupsController");
        kony.mvc.registry.add("frmpostingRestriction", "frmpostingRestriction", "frmpostingRestrictionController");
        kony.mvc.registry.add("repayment", "frmRepayment", "frmRepaymentController");
        kony.mvc.registry.add("frmSearchCustomer", "frmSearchCustomer", "frmSearchCustomerController");
        kony.mvc.registry.add("loanNavigate", "loanNavigate", "loanNavigateController");
        setAppBehaviors();
    },
    postAppInitCallBack: function(eventObj) {},
    appmenuseq: function() {
        new kony.mvc.Navigation("frmLogin").navigate();
    }
});