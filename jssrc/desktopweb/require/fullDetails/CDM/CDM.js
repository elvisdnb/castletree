define(function() {
    return function(controller) {
        var CDM = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "isMaster": true,
            "height": "93%",
            "horizontalScrollIndicator": true,
            "id": "CDM",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "5%",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1200],
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "7%",
            "verticalScrollIndicator": true,
            "width": "95%"
        }, controller.args[0], "CDM"), extendConfig({}, controller.args[1], "CDM"), extendConfig({}, controller.args[2], "CDM"));
        CDM.setDefaultUnit(kony.flex.DP);
        var flxBasicHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxBasicHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxBasicHeader"), extendConfig({}, controller.args[1], "flxBasicHeader"), extendConfig({}, controller.args[2], "flxBasicHeader"));
        flxBasicHeader.setDefaultUnit(kony.flex.DP);
        var titleHeader = new com.TitleTop.titleHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "125dp",
            "id": "titleHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "topNameBlock",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "overrides": {
                "imgPrevArrow": {
                    "src": "bread_arrow_1.png"
                }
            }
        }, controller.args[0], "titleHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "titleHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "titleHeader"));
        titleHeader.flxTitleHeader.onClick = controller.AS_FlexContainer_ca99da93965c47ff910f574fcb49582f;
        flxBasicHeader.add(titleHeader);
        var flxBasicMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "450dp",
            "id": "flxBasicMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBasicMain"), extendConfig({}, controller.args[1], "flxBasicMain"), extendConfig({}, controller.args[2], "flxBasicMain"));
        flxBasicMain.setDefaultUnit(kony.flex.DP);
        var flxBasicInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerY": "50%",
            "clipBounds": false,
            "height": "96%",
            "id": "flxBasicInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "1%",
            "isModalContainer": false,
            "skin": "lFbox23",
            "top": 0,
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxBasicInner"), extendConfig({}, controller.args[1], "flxBasicInner"), extendConfig({}, controller.args[2], "flxBasicInner"));
        flxBasicInner.setDefaultUnit(kony.flex.DP);
        var flxBasicContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxBasicContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxSCVBlock1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBasicContent"), extendConfig({}, controller.args[1], "flxBasicContent"), extendConfig({}, controller.args[2], "flxBasicContent"));
        flxBasicContent.setDefaultUnit(kony.flex.DP);
        var flxBasicTitletop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "60dp",
            "id": "flxBasicTitletop",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSCVTitle1",
            "top": "1dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBasicTitletop"), extendConfig({}, controller.args[1], "flxBasicTitletop"), extendConfig({}, controller.args[2], "flxBasicTitletop"));
        flxBasicTitletop.setDefaultUnit(kony.flex.DP);
        var flxBasicIconTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60%",
            "id": "flxBasicIconTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxBasicIconTitle"), extendConfig({}, controller.args[1], "flxBasicIconTitle"), extendConfig({}, controller.args[2], "flxBasicIconTitle"));
        flxBasicIconTitle.setDefaultUnit(kony.flex.DP);
        var imgBasicTitleIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "23dp",
            "id": "imgBasicTitleIcon",
            "isVisible": false,
            "left": "21dp",
            "skin": "slImage",
            "src": "user_1.png",
            "width": "23dp",
            "zIndex": 1
        }, controller.args[0], "imgBasicTitleIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgBasicTitleIcon"), extendConfig({}, controller.args[2], "imgBasicTitleIcon"));
        var lblBasicTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBasicTitle",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblSCVTitle1",
            "text": "Basic Details",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBasicTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBasicTitle"), extendConfig({}, controller.args[2], "lblBasicTitle"));
        flxBasicIconTitle.add(imgBasicTitleIcon, lblBasicTitle);
        flxBasicTitletop.add(flxBasicIconTitle);
        var flxBasicInnerContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxBasicInnerContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBasicInnerContent"), extendConfig({}, controller.args[1], "flxBasicInnerContent"), extendConfig({}, controller.args[2], "flxBasicInnerContent"));
        flxBasicInnerContent.setDefaultUnit(kony.flex.DP);
        var flxPersonalContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "370dp",
            "id": "flxPersonalContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "lFbox57",
            "top": "0dp",
            "width": "30%"
        }, controller.args[0], "flxPersonalContent"), extendConfig({}, controller.args[1], "flxPersonalContent"), extendConfig({}, controller.args[2], "flxPersonalContent"));
        flxPersonalContent.setDefaultUnit(kony.flex.DP);
        var spinnerPersonal = new com.fulldetails.spinner(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "spinnerPersonal",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0ff6a8d8239db42",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinner": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "spinnerPersonal"), extendConfig({
            "overrides": {}
        }, controller.args[1], "spinnerPersonal"), extendConfig({
            "overrides": {}
        }, controller.args[2], "spinnerPersonal"));
        var flxPersonalColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "371dp",
            "id": "flxPersonalColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "0dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxPersonalColumn2"), extendConfig({}, controller.args[1], "flxPersonalColumn2"), extendConfig({}, controller.args[2], "flxPersonalColumn2"));
        flxPersonalColumn2.setDefaultUnit(kony.flex.DP);
        var flxPersonalTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPersonalTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "onTouchStart": controller.AS_FlexContainer_afb29883f78c4004a93766bcb1143ec6,
            "skin": "sknFlxOnHover",
            "top": "27dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxPersonalTitle"), extendConfig({}, controller.args[1], "flxPersonalTitle"), extendConfig({}, controller.args[2], "flxPersonalTitle"));
        flxPersonalTitle.setDefaultUnit(kony.flex.DP);
        var lblPersonal = new kony.ui.Label(extendConfig({
            "id": "lblPersonal",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblModTitleNormal",
            "text": "Personal",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblPersonal"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPersonal"), extendConfig({
            "hoverSkin": "sknLblModTitleHover"
        }, controller.args[2], "lblPersonal"));
        var imgEditPersonal = new kony.ui.Image2(extendConfig({
            "height": "20dp",
            "id": "imgEditPersonal",
            "isVisible": true,
            "left": 10,
            "skin": "slImage",
            "src": "editicongray.png",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgEditPersonal"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgEditPersonal"), extendConfig({}, controller.args[2], "imgEditPersonal"));
        flxPersonalTitle.add(lblPersonal, imgEditPersonal);
        var flxtitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxtitle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "19dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxtitle"), extendConfig({}, controller.args[1], "flxtitle"), extendConfig({}, controller.args[2], "flxtitle"));
        flxtitle.setDefaultUnit(kony.flex.DP);
        var lblNameLabel = new kony.ui.Label(extendConfig({
            "id": "lblNameLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "TITLE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNameLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameLabel"), extendConfig({}, controller.args[2], "lblNameLabel"));
        var lblFlxName = new kony.ui.Label(extendConfig({
            "id": "lblFlxName",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Mr",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlxName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlxName"), extendConfig({}, controller.args[2], "lblFlxName"));
        flxtitle.add(lblNameLabel, lblFlxName);
        var flxShortName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxShortName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxShortName"), extendConfig({}, controller.args[1], "flxShortName"), extendConfig({}, controller.args[2], "flxShortName"));
        flxShortName.setDefaultUnit(kony.flex.DP);
        var lblShortNameLabel = new kony.ui.Label(extendConfig({
            "id": "lblShortNameLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "SHORT NAME",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblShortNameLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShortNameLabel"), extendConfig({}, controller.args[2], "lblShortNameLabel"));
        var lblShortnameValue = new kony.ui.Label(extendConfig({
            "id": "lblShortnameValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "James May",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblShortnameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShortnameValue"), extendConfig({}, controller.args[2], "lblShortnameValue"));
        flxShortName.add(lblShortNameLabel, lblShortnameValue);
        var flxMarital = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxMarital",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMarital"), extendConfig({}, controller.args[1], "flxMarital"), extendConfig({}, controller.args[2], "flxMarital"));
        flxMarital.setDefaultUnit(kony.flex.DP);
        var lblMaritallabel = new kony.ui.Label(extendConfig({
            "id": "lblMaritallabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "MARITAL STATUS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMaritallabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaritallabel"), extendConfig({}, controller.args[2], "lblMaritallabel"));
        var lblMaritalValue = new kony.ui.Label(extendConfig({
            "id": "lblMaritalValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Married",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMaritalValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaritalValue"), extendConfig({}, controller.args[2], "lblMaritalValue"));
        flxMarital.add(lblMaritallabel, lblMaritalValue);
        var flxNatioanality = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxNatioanality",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNatioanality"), extendConfig({}, controller.args[1], "flxNatioanality"), extendConfig({}, controller.args[2], "flxNatioanality"));
        flxNatioanality.setDefaultUnit(kony.flex.DP);
        var lblNationalLabel = new kony.ui.Label(extendConfig({
            "id": "lblNationalLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "NATIONALITY",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNationalLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNationalLabel"), extendConfig({}, controller.args[2], "lblNationalLabel"));
        var lblNationValue = new kony.ui.Label(extendConfig({
            "id": "lblNationValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "British",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNationValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNationValue"), extendConfig({}, controller.args[2], "lblNationValue"));
        flxNatioanality.add(lblNationalLabel, lblNationValue);
        var flxFullName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxFullName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFullName"), extendConfig({}, controller.args[1], "flxFullName"), extendConfig({}, controller.args[2], "flxFullName"));
        flxFullName.setDefaultUnit(kony.flex.DP);
        var lblFullNameLabel = new kony.ui.Label(extendConfig({
            "id": "lblFullNameLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "FULL NAME",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFullNameLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFullNameLabel"), extendConfig({}, controller.args[2], "lblFullNameLabel"));
        var lblFullNameValue = new kony.ui.Label(extendConfig({
            "id": "lblFullNameValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "James Arthur May",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFullNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFullNameValue"), extendConfig({}, controller.args[2], "lblFullNameValue"));
        flxFullName.add(lblFullNameLabel, lblFullNameValue);
        flxPersonalColumn2.add(flxPersonalTitle, flxtitle, flxShortName, flxMarital, flxNatioanality, flxFullName);
        var flxPersonalColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPersonalColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "65dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxPersonalColumn1"), extendConfig({}, controller.args[1], "flxPersonalColumn1"), extendConfig({}, controller.args[2], "flxPersonalColumn1"));
        flxPersonalColumn1.setDefaultUnit(kony.flex.DP);
        var flxSalutation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSalutation",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSalutation"), extendConfig({}, controller.args[1], "flxSalutation"), extendConfig({}, controller.args[2], "flxSalutation"));
        flxSalutation.setDefaultUnit(kony.flex.DP);
        var lblSalutationLabel = new kony.ui.Label(extendConfig({
            "id": "lblSalutationLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "SALUTATION",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSalutationLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSalutationLabel"), extendConfig({}, controller.args[2], "lblSalutationLabel"));
        var lblSalutation = new kony.ui.Label(extendConfig({
            "id": "lblSalutation",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Mr",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSalutation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSalutation"), extendConfig({}, controller.args[2], "lblSalutation"));
        flxSalutation.add(lblSalutationLabel, lblSalutation);
        var flxDOB = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxDOB",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDOB"), extendConfig({}, controller.args[1], "flxDOB"), extendConfig({}, controller.args[2], "flxDOB"));
        flxDOB.setDefaultUnit(kony.flex.DP);
        var lblDOBLabel = new kony.ui.Label(extendConfig({
            "id": "lblDOBLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "DATE OF BIRTH",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDOBLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDOBLabel"), extendConfig({}, controller.args[2], "lblDOBLabel"));
        var lblDOB = new kony.ui.Label(extendConfig({
            "id": "lblDOB",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "15/08/1971",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDOB"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDOB"), extendConfig({}, controller.args[2], "lblDOB"));
        flxDOB.add(lblDOBLabel, lblDOB);
        var flxDependants = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxDependants",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDependants"), extendConfig({}, controller.args[1], "flxDependants"), extendConfig({}, controller.args[2], "flxDependants"));
        flxDependants.setDefaultUnit(kony.flex.DP);
        var lblDependantsLabel = new kony.ui.Label(extendConfig({
            "id": "lblDependantsLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "NO.OF DEPENDANTS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDependantsLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependantsLabel"), extendConfig({}, controller.args[2], "lblDependantsLabel"));
        var lblDependants = new kony.ui.Label(extendConfig({
            "id": "lblDependants",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "0",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDependants"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependants"), extendConfig({}, controller.args[2], "lblDependants"));
        flxDependants.add(lblDependantsLabel, lblDependants);
        var flxResidence = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxResidence",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlx1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxResidence"), extendConfig({}, controller.args[1], "flxResidence"), extendConfig({}, controller.args[2], "flxResidence"));
        flxResidence.setDefaultUnit(kony.flex.DP);
        var lblResidenceLabel = new kony.ui.Label(extendConfig({
            "id": "lblResidenceLabel",
            "isVisible": true,
            "left": 0,
            "skin": "sknLbl3",
            "text": "RESIDENCE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResidenceLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResidenceLabel"), extendConfig({}, controller.args[2], "lblResidenceLabel"));
        var lblRes = new kony.ui.Label(extendConfig({
            "id": "lblRes",
            "isVisible": true,
            "left": 0,
            "skin": "sknLbl1",
            "text": "Great Britain",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRes"), extendConfig({}, controller.args[2], "lblRes"));
        flxResidence.add(lblResidenceLabel, lblRes);
        var flxGender = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxGender",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlx3",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxGender"), extendConfig({}, controller.args[1], "flxGender"), extendConfig({}, controller.args[2], "flxGender"));
        flxGender.setDefaultUnit(kony.flex.DP);
        var lblGenderLabel = new kony.ui.Label(extendConfig({
            "id": "lblGenderLabel",
            "isVisible": true,
            "left": 0,
            "skin": "sknLbl4",
            "text": "GENDER",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGenderLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGenderLabel"), extendConfig({}, controller.args[2], "lblGenderLabel"));
        var lblGender = new kony.ui.Label(extendConfig({
            "id": "lblGender",
            "isVisible": true,
            "left": 0,
            "skin": "sknLbl2",
            "text": "Male",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGender"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGender"), extendConfig({}, controller.args[2], "lblGender"));
        flxGender.add(lblGenderLabel, lblGender);
        flxPersonalColumn1.add(flxSalutation, flxDOB, flxDependants, flxResidence, flxGender);
        flxPersonalContent.add(spinnerPersonal, flxPersonalColumn2, flxPersonalColumn1);
        var flxContactContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "370dp",
            "id": "flxContactContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "lFbox47",
            "top": "0dp",
            "width": "38%"
        }, controller.args[0], "flxContactContent"), extendConfig({}, controller.args[1], "flxContactContent"), extendConfig({}, controller.args[2], "flxContactContent"));
        flxContactContent.setDefaultUnit(kony.flex.DP);
        var spinnerContactDetails = new com.fulldetails.spinner(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "spinnerContactDetails",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0ff6a8d8239db42",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinner": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "spinnerContactDetails"), extendConfig({
            "overrides": {}
        }, controller.args[1], "spinnerContactDetails"), extendConfig({
            "overrides": {}
        }, controller.args[2], "spinnerContactDetails"));
        var flxContactColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "371dp",
            "id": "flxContactColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "7%",
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "0dp",
            "width": "45%",
            "zIndex": 1
        }, controller.args[0], "flxContactColumn1"), extendConfig({}, controller.args[1], "flxContactColumn1"), extendConfig({}, controller.args[2], "flxContactColumn1"));
        flxContactColumn1.setDefaultUnit(kony.flex.DP);
        var flxContactDetailsTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContactDetailsTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "onTouchStart": controller.AS_FlexContainer_cbf907cc5ac348b493bfe83ddc496f42,
            "skin": "slFbox",
            "top": "27dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxContactDetailsTitle"), extendConfig({}, controller.args[1], "flxContactDetailsTitle"), extendConfig({}, controller.args[2], "flxContactDetailsTitle"));
        flxContactDetailsTitle.setDefaultUnit(kony.flex.DP);
        var lblContactLabel = new kony.ui.Label(extendConfig({
            "id": "lblContactLabel",
            "isVisible": true,
            "skin": "sknLblModTitleNormal",
            "text": "Contact details",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblContactLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactLabel"), extendConfig({
            "hoverSkin": "sknLblModTitleHover"
        }, controller.args[2], "lblContactLabel"));
        var imgEditContact = new kony.ui.Image2(extendConfig({
            "height": "20dp",
            "id": "imgEditContact",
            "isVisible": true,
            "left": 10,
            "skin": "slImage",
            "src": "editicongray.png",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgEditContact"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgEditContact"), extendConfig({}, controller.args[2], "imgEditContact"));
        flxContactDetailsTitle.add(lblContactLabel, imgEditContact);
        var flxPrimaryAddress = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "25%",
            "id": "flxPrimaryAddress",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "19dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrimaryAddress"), extendConfig({}, controller.args[1], "flxPrimaryAddress"), extendConfig({}, controller.args[2], "flxPrimaryAddress"));
        flxPrimaryAddress.setDefaultUnit(kony.flex.DP);
        var lblPrimaryLabel = new kony.ui.Label(extendConfig({
            "id": "lblPrimaryLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "PRIMARY ADDRESS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrimaryLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimaryLabel"), extendConfig({}, controller.args[2], "lblPrimaryLabel"));
        var lblAddressValue = new kony.ui.Label(extendConfig({
            "id": "lblAddressValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "The Met Building, 24 Percy Street London W1T 2BS UK",
            "top": "17dp",
            "width": "125dp",
            "zIndex": 1
        }, controller.args[0], "lblAddressValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressValue"), extendConfig({}, controller.args[2], "lblAddressValue"));
        flxPrimaryAddress.add(lblPrimaryLabel, lblAddressValue);
        var flxSecondaryAddress = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "40%",
            "id": "flxSecondaryAddress",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSecondaryAddress"), extendConfig({}, controller.args[1], "flxSecondaryAddress"), extendConfig({}, controller.args[2], "flxSecondaryAddress"));
        flxSecondaryAddress.setDefaultUnit(kony.flex.DP);
        var lblSecondaryLabel = new kony.ui.Label(extendConfig({
            "id": "lblSecondaryLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "SECONDARY ADDRESS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSecondaryLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSecondaryLabel"), extendConfig({}, controller.args[2], "lblSecondaryLabel"));
        var lblSEcondaryValue = new kony.ui.Label(extendConfig({
            "id": "lblSEcondaryValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "34 Kensington Church St, Kensington, London W8 4HA, United Kingdom",
            "top": "17dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "lblSEcondaryValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSEcondaryValue"), extendConfig({}, controller.args[2], "lblSEcondaryValue"));
        flxSecondaryAddress.add(lblSecondaryLabel, lblSEcondaryValue);
        flxContactColumn1.add(flxContactDetailsTitle, flxPrimaryAddress, flxSecondaryAddress);
        var flxContactColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContactColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "65dp",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxContactColumn2"), extendConfig({}, controller.args[1], "flxContactColumn2"), extendConfig({}, controller.args[2], "flxContactColumn2"));
        flxContactColumn2.setDefaultUnit(kony.flex.DP);
        var flxResidencePhone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxResidencePhone",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxResidencePhone"), extendConfig({}, controller.args[1], "flxResidencePhone"), extendConfig({}, controller.args[2], "flxResidencePhone"));
        flxResidencePhone.setDefaultUnit(kony.flex.DP);
        var lblResidence = new kony.ui.Label(extendConfig({
            "id": "lblResidence",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "RESIDENCE PHONE NO.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResidence"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResidence"), extendConfig({}, controller.args[2], "lblResidence"));
        var lblResidenceValue = new kony.ui.Label(extendConfig({
            "id": "lblResidenceValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "+44 7792491167",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResidenceValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResidenceValue"), extendConfig({}, controller.args[2], "lblResidenceValue"));
        flxResidencePhone.add(lblResidence, lblResidenceValue);
        var flxOfficePhone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOfficePhone",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOfficePhone"), extendConfig({}, controller.args[1], "flxOfficePhone"), extendConfig({}, controller.args[2], "flxOfficePhone"));
        flxOfficePhone.setDefaultUnit(kony.flex.DP);
        var lblOfficePhone = new kony.ui.Label(extendConfig({
            "id": "lblOfficePhone",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "OFFICE PHONE NO.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOfficePhone"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOfficePhone"), extendConfig({}, controller.args[2], "lblOfficePhone"));
        var lblOfficePhoneValue = new kony.ui.Label(extendConfig({
            "id": "lblOfficePhoneValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "+44 7792491167",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOfficePhoneValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOfficePhoneValue"), extendConfig({}, controller.args[2], "lblOfficePhoneValue"));
        flxOfficePhone.add(lblOfficePhone, lblOfficePhoneValue);
        var flxMobile = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxMobile",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMobile"), extendConfig({}, controller.args[1], "flxMobile"), extendConfig({}, controller.args[2], "flxMobile"));
        flxMobile.setDefaultUnit(kony.flex.DP);
        var lblMobileLabel = new kony.ui.Label(extendConfig({
            "id": "lblMobileLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "MOBILE NO",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMobileLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMobileLabel"), extendConfig({}, controller.args[2], "lblMobileLabel"));
        var lblMobileValue = new kony.ui.Label(extendConfig({
            "id": "lblMobileValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "+44 7792491167",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMobileValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMobileValue"), extendConfig({}, controller.args[2], "lblMobileValue"));
        flxMobile.add(lblMobileLabel, lblMobileValue);
        var flxEmail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxEmail",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEmail"), extendConfig({}, controller.args[1], "flxEmail"), extendConfig({}, controller.args[2], "flxEmail"));
        flxEmail.setDefaultUnit(kony.flex.DP);
        var lblEmailLabel = new kony.ui.Label(extendConfig({
            "id": "lblEmailLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "EMAIL ADDRESS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmailLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailLabel"), extendConfig({}, controller.args[2], "lblEmailLabel"));
        var lblEmailValue = new kony.ui.Label(extendConfig({
            "id": "lblEmailValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "jimmy.m@gmail.com",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmailValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailValue"), extendConfig({}, controller.args[2], "lblEmailValue"));
        flxEmail.add(lblEmailLabel, lblEmailValue);
        var flxFax = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxFax",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFax"), extendConfig({}, controller.args[1], "flxFax"), extendConfig({}, controller.args[2], "flxFax"));
        flxFax.setDefaultUnit(kony.flex.DP);
        var lblFaxLabel = new kony.ui.Label(extendConfig({
            "id": "lblFaxLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "FAX",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFaxLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFaxLabel"), extendConfig({}, controller.args[2], "lblFaxLabel"));
        var lblFaxValue = new kony.ui.Label(extendConfig({
            "id": "lblFaxValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "-",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFaxValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFaxValue"), extendConfig({}, controller.args[2], "lblFaxValue"));
        flxFax.add(lblFaxLabel, lblFaxValue);
        flxContactColumn2.add(flxResidencePhone, flxOfficePhone, flxMobile, flxEmail, flxFax);
        flxContactContent.add(spinnerContactDetails, flxContactColumn1, flxContactColumn2);
        var flxContactMethod = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "370dp",
            "id": "flxContactMethod",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox53",
            "top": "0dp",
            "width": "28%"
        }, controller.args[0], "flxContactMethod"), extendConfig({}, controller.args[1], "flxContactMethod"), extendConfig({}, controller.args[2], "flxContactMethod"));
        flxContactMethod.setDefaultUnit(kony.flex.DP);
        var spinnerContactMethod = new com.fulldetails.spinner(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "spinnerContactMethod",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0ff6a8d8239db42",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinner": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "spinnerContactMethod"), extendConfig({
            "overrides": {}
        }, controller.args[1], "spinnerContactMethod"), extendConfig({
            "overrides": {}
        }, controller.args[2], "spinnerContactMethod"));
        var flxContactMethodColumn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "371dp",
            "id": "flxContactMethodColumn",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "7%",
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "0dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxContactMethodColumn"), extendConfig({}, controller.args[1], "flxContactMethodColumn"), extendConfig({}, controller.args[2], "flxContactMethodColumn"));
        flxContactMethodColumn.setDefaultUnit(kony.flex.DP);
        var flxContactMethodTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContactMethodTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "onTouchStart": controller.AS_FlexContainer_h6cbd1fc4ec74a97bade97600528ef6a,
            "skin": "slFbox",
            "top": "27dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxContactMethodTitle"), extendConfig({}, controller.args[1], "flxContactMethodTitle"), extendConfig({}, controller.args[2], "flxContactMethodTitle"));
        flxContactMethodTitle.setDefaultUnit(kony.flex.DP);
        var lblContactMethodTitle = new kony.ui.Label(extendConfig({
            "id": "lblContactMethodTitle",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblModTitleNormal",
            "text": "Contact Method",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblContactMethodTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactMethodTitle"), extendConfig({
            "hoverSkin": "sknLblModTitleHover"
        }, controller.args[2], "lblContactMethodTitle"));
        var imgEditContactMethod = new kony.ui.Image2(extendConfig({
            "height": "20dp",
            "id": "imgEditContactMethod",
            "isVisible": true,
            "left": 10,
            "skin": "slImage",
            "src": "editicongray.png",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgEditContactMethod"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgEditContactMethod"), extendConfig({}, controller.args[2], "imgEditContactMethod"));
        flxContactMethodTitle.add(lblContactMethodTitle, imgEditContactMethod);
        var flxPreferredChannel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxPreferredChannel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "19dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPreferredChannel"), extendConfig({}, controller.args[1], "flxPreferredChannel"), extendConfig({}, controller.args[2], "flxPreferredChannel"));
        flxPreferredChannel.setDefaultUnit(kony.flex.DP);
        var lblPreferredLabel = new kony.ui.Label(extendConfig({
            "id": "lblPreferredLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "PREFERRED CHANNEL",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPreferredLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPreferredLabel"), extendConfig({}, controller.args[2], "lblPreferredLabel"));
        var lblPreferredValue = new kony.ui.Label(extendConfig({
            "id": "lblPreferredValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Email",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPreferredValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPreferredValue"), extendConfig({}, controller.args[2], "lblPreferredValue"));
        flxPreferredChannel.add(lblPreferredLabel, lblPreferredValue);
        var flxInternetBanking = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxInternetBanking",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxInternetBanking"), extendConfig({}, controller.args[1], "flxInternetBanking"), extendConfig({}, controller.args[2], "flxInternetBanking"));
        flxInternetBanking.setDefaultUnit(kony.flex.DP);
        var lblInternetBanking = new kony.ui.Label(extendConfig({
            "id": "lblInternetBanking",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "INTERNET BANKING",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblInternetBanking"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInternetBanking"), extendConfig({}, controller.args[2], "lblInternetBanking"));
        var lblInternetBnkLabel = new kony.ui.Label(extendConfig({
            "id": "lblInternetBnkLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Yes",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblInternetBnkLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInternetBnkLabel"), extendConfig({}, controller.args[2], "lblInternetBnkLabel"));
        flxInternetBanking.add(lblInternetBanking, lblInternetBnkLabel);
        var flxSecureMsg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSecureMsg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSecureMsg"), extendConfig({}, controller.args[1], "flxSecureMsg"), extendConfig({}, controller.args[2], "flxSecureMsg"));
        flxSecureMsg.setDefaultUnit(kony.flex.DP);
        var lblSecureMsgLabel = new kony.ui.Label(extendConfig({
            "id": "lblSecureMsgLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "SECURE MESSAGE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSecureMsgLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSecureMsgLabel"), extendConfig({}, controller.args[2], "lblSecureMsgLabel"));
        var lblSecurelblValue = new kony.ui.Label(extendConfig({
            "id": "lblSecurelblValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Yes",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSecurelblValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSecurelblValue"), extendConfig({}, controller.args[2], "lblSecurelblValue"));
        flxSecureMsg.add(lblSecureMsgLabel, lblSecurelblValue);
        var flxMobileBanking = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxMobileBanking",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMobileBanking"), extendConfig({}, controller.args[1], "flxMobileBanking"), extendConfig({}, controller.args[2], "flxMobileBanking"));
        flxMobileBanking.setDefaultUnit(kony.flex.DP);
        var lblMobileBnklbl = new kony.ui.Label(extendConfig({
            "id": "lblMobileBnklbl",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "MOBILE BANKING",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMobileBnklbl"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMobileBnklbl"), extendConfig({}, controller.args[2], "lblMobileBnklbl"));
        var lblMobileBankingVal = new kony.ui.Label(extendConfig({
            "id": "lblMobileBankingVal",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Yes",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMobileBankingVal"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMobileBankingVal"), extendConfig({}, controller.args[2], "lblMobileBankingVal"));
        flxMobileBanking.add(lblMobileBnklbl, lblMobileBankingVal);
        flxContactMethodColumn.add(flxContactMethodTitle, flxPreferredChannel, flxInternetBanking, flxSecureMsg, flxMobileBanking);
        flxContactMethod.add(spinnerContactMethod, flxContactMethodColumn);
        flxBasicInnerContent.add(flxPersonalContent, flxContactContent, flxContactMethod);
        flxBasicContent.add(flxBasicTitletop, flxBasicInnerContent);
        flxBasicInner.add(flxBasicContent);
        flxBasicMain.add(flxBasicInner);
        var flxKYCMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "300dp",
            "id": "flxKYCMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxKYCMain"), extendConfig({}, controller.args[1], "flxKYCMain"), extendConfig({}, controller.args[2], "flxKYCMain"));
        flxKYCMain.setDefaultUnit(kony.flex.DP);
        var flxKYCInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "96%",
            "id": "flxKYCInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "1%",
            "isModalContainer": false,
            "skin": "lFbox23",
            "top": 0,
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxKYCInner"), extendConfig({}, controller.args[1], "flxKYCInner"), extendConfig({}, controller.args[2], "flxKYCInner"));
        flxKYCInner.setDefaultUnit(kony.flex.DP);
        var flxKYCContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxKYCContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxSCVBlock1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxKYCContent"), extendConfig({}, controller.args[1], "flxKYCContent"), extendConfig({}, controller.args[2], "flxKYCContent"));
        flxKYCContent.setDefaultUnit(kony.flex.DP);
        var flxKYCTitletop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "60dp",
            "id": "flxKYCTitletop",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSCVTitle1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxKYCTitletop"), extendConfig({}, controller.args[1], "flxKYCTitletop"), extendConfig({}, controller.args[2], "flxKYCTitletop"));
        flxKYCTitletop.setDefaultUnit(kony.flex.DP);
        var flxKYCIconTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60%",
            "id": "flxKYCIconTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxKYCIconTitle"), extendConfig({}, controller.args[1], "flxKYCIconTitle"), extendConfig({}, controller.args[2], "flxKYCIconTitle"));
        flxKYCIconTitle.setDefaultUnit(kony.flex.DP);
        var imgKYCTitleIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "23dp",
            "id": "imgKYCTitleIcon",
            "isVisible": false,
            "left": "21dp",
            "skin": "slImage",
            "src": "user_1.png",
            "width": "23dp",
            "zIndex": 1
        }, controller.args[0], "imgKYCTitleIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgKYCTitleIcon"), extendConfig({}, controller.args[2], "imgKYCTitleIcon"));
        var lblKYCTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblKYCTitle",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblSCVTitle1",
            "text": "Know Your Customer",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblKYCTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblKYCTitle"), extendConfig({}, controller.args[2], "lblKYCTitle"));
        flxKYCIconTitle.add(imgKYCTitleIcon, lblKYCTitle);
        flxKYCTitletop.add(flxKYCIconTitle);
        var flxKYCInnerContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxKYCInnerContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxKYCInnerContent"), extendConfig({}, controller.args[1], "flxKYCInnerContent"), extendConfig({}, controller.args[2], "flxKYCInnerContent"));
        flxKYCInnerContent.setDefaultUnit(kony.flex.DP);
        var flxKYCCompliance = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "228dp",
            "id": "flxKYCCompliance",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "KYCLine",
            "top": "0dp",
            "width": "49%"
        }, controller.args[0], "flxKYCCompliance"), extendConfig({}, controller.args[1], "flxKYCCompliance"), extendConfig({}, controller.args[2], "flxKYCCompliance"));
        flxKYCCompliance.setDefaultUnit(kony.flex.DP);
        var spinnerKYC = new com.fulldetails.spinner(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "spinnerKYC",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0ff6a8d8239db42",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinner": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "spinnerKYC"), extendConfig({
            "overrides": {}
        }, controller.args[1], "spinnerKYC"), extendConfig({
            "overrides": {}
        }, controller.args[2], "spinnerKYC"));
        var flxComplianceColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxComplianceColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "0dp",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxComplianceColumn1"), extendConfig({}, controller.args[1], "flxComplianceColumn1"), extendConfig({}, controller.args[2], "flxComplianceColumn1"));
        flxComplianceColumn1.setDefaultUnit(kony.flex.DP);
        var flxComplianceTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxComplianceTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "onTouchStart": controller.AS_FlexContainer_eb6ec3c5e0774321b530255314041bdb,
            "skin": "slFbox",
            "top": "27dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxComplianceTitle"), extendConfig({}, controller.args[1], "flxComplianceTitle"), extendConfig({}, controller.args[2], "flxComplianceTitle"));
        flxComplianceTitle.setDefaultUnit(kony.flex.DP);
        var lblCompliance = new kony.ui.Label(extendConfig({
            "id": "lblCompliance",
            "isVisible": true,
            "skin": "sknLblModTitleNormal",
            "text": "Compliance",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblCompliance"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCompliance"), extendConfig({
            "hoverSkin": "sknLblModTitleHover"
        }, controller.args[2], "lblCompliance"));
        var imgEditCompliance = new kony.ui.Image2(extendConfig({
            "height": "20dp",
            "id": "imgEditCompliance",
            "isVisible": true,
            "left": 10,
            "skin": "slImage",
            "src": "editicongray.png",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgEditCompliance"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgEditCompliance"), extendConfig({}, controller.args[2], "imgEditCompliance"));
        flxComplianceTitle.add(lblCompliance, imgEditCompliance);
        var flxPostingTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxPostingTitle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "19dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPostingTitle"), extendConfig({}, controller.args[1], "flxPostingTitle"), extendConfig({}, controller.args[2], "flxPostingTitle"));
        flxPostingTitle.setDefaultUnit(kony.flex.DP);
        var lblPostingLabel = new kony.ui.Label(extendConfig({
            "id": "lblPostingLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "POSTING RESTRICTIONS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPostingLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPostingLabel"), extendConfig({}, controller.args[2], "lblPostingLabel"));
        var lblPostingValue = new kony.ui.Label(extendConfig({
            "id": "lblPostingValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "None",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPostingValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPostingValue"), extendConfig({}, controller.args[2], "lblPostingValue"));
        flxPostingTitle.add(lblPostingLabel, lblPostingValue);
        var flxAMLRisk = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxAMLRisk",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAMLRisk"), extendConfig({}, controller.args[1], "flxAMLRisk"), extendConfig({}, controller.args[2], "flxAMLRisk"));
        flxAMLRisk.setDefaultUnit(kony.flex.DP);
        var lblAMLLabel = new kony.ui.Label(extendConfig({
            "id": "lblAMLLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "AML RISK",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAMLLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAMLLabel"), extendConfig({}, controller.args[2], "lblAMLLabel"));
        var lblAMLValue = new kony.ui.Label(extendConfig({
            "id": "lblAMLValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Normal",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAMLValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAMLValue"), extendConfig({}, controller.args[2], "lblAMLValue"));
        flxAMLRisk.add(lblAMLLabel, lblAMLValue);
        flxComplianceColumn1.add(flxComplianceTitle, flxPostingTitle, flxAMLRisk);
        var flxComplianceColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxComplianceColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "65dp",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxComplianceColumn2"), extendConfig({}, controller.args[1], "flxComplianceColumn2"), extendConfig({}, controller.args[2], "flxComplianceColumn2"));
        flxComplianceColumn2.setDefaultUnit(kony.flex.DP);
        var flxCRSStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxCRSStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCRSStatus"), extendConfig({}, controller.args[1], "flxCRSStatus"), extendConfig({}, controller.args[2], "flxCRSStatus"));
        flxCRSStatus.setDefaultUnit(kony.flex.DP);
        var lblCRSLabel = new kony.ui.Label(extendConfig({
            "id": "lblCRSLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "CRS STATUS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCRSLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCRSLabel"), extendConfig({}, controller.args[2], "lblCRSLabel"));
        var lblCRSValue = new kony.ui.Label(extendConfig({
            "id": "lblCRSValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Non reportable",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCRSValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCRSValue"), extendConfig({}, controller.args[2], "lblCRSValue"));
        flxCRSStatus.add(lblCRSLabel, lblCRSValue);
        var flxCRSJURU = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxCRSJURU",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCRSJURU"), extendConfig({}, controller.args[1], "flxCRSJURU"), extendConfig({}, controller.args[2], "flxCRSJURU"));
        flxCRSJURU.setDefaultUnit(kony.flex.DP);
        var lblCRSJURULabel = new kony.ui.Label(extendConfig({
            "id": "lblCRSJURULabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "CRS JURISDICTION",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCRSJURULabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCRSJURULabel"), extendConfig({}, controller.args[2], "lblCRSJURULabel"));
        var lblCRSJURUValue = new kony.ui.Label(extendConfig({
            "id": "lblCRSJURUValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "None",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCRSJURUValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCRSJURUValue"), extendConfig({}, controller.args[2], "lblCRSJURUValue"));
        flxCRSJURU.add(lblCRSJURULabel, lblCRSJURUValue);
        flxComplianceColumn2.add(flxCRSStatus, flxCRSJURU);
        var flxComplianceColumn3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxComplianceColumn3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "65dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxComplianceColumn3"), extendConfig({}, controller.args[1], "flxComplianceColumn3"), extendConfig({}, controller.args[2], "flxComplianceColumn3"));
        flxComplianceColumn3.setDefaultUnit(kony.flex.DP);
        var flxFATCA = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxFATCA",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFATCA"), extendConfig({}, controller.args[1], "flxFATCA"), extendConfig({}, controller.args[2], "flxFATCA"));
        flxFATCA.setDefaultUnit(kony.flex.DP);
        var lblFATCAlabel = new kony.ui.Label(extendConfig({
            "id": "lblFATCAlabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "FATCA STATUS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFATCAlabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFATCAlabel"), extendConfig({}, controller.args[2], "lblFATCAlabel"));
        var lblFATCAValue = new kony.ui.Label(extendConfig({
            "id": "lblFATCAValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "None",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFATCAValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFATCAValue"), extendConfig({}, controller.args[2], "lblFATCAValue"));
        flxFATCA.add(lblFATCAlabel, lblFATCAValue);
        flxComplianceColumn3.add(flxFATCA);
        flxKYCCompliance.add(spinnerKYC, flxComplianceColumn1, flxComplianceColumn2, flxComplianceColumn3);
        var flxConsents = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "228dp",
            "id": "flxConsents",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "540dp"
        }, controller.args[0], "flxConsents"), extendConfig({}, controller.args[1], "flxConsents"), extendConfig({}, controller.args[2], "flxConsents"));
        flxConsents.setDefaultUnit(kony.flex.DP);
        var spinnerConsents = new com.fulldetails.spinner(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "spinnerConsents",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0ff6a8d8239db42",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinner": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "spinnerConsents"), extendConfig({
            "overrides": {}
        }, controller.args[1], "spinnerConsents"), extendConfig({
            "overrides": {}
        }, controller.args[2], "spinnerConsents"));
        var flxProtection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "228dp",
            "id": "flxProtection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "lFbox39",
            "top": "0dp",
            "width": "240dp"
        }, controller.args[0], "flxProtection"), extendConfig({}, controller.args[1], "flxProtection"), extendConfig({}, controller.args[2], "flxProtection"));
        flxProtection.setDefaultUnit(kony.flex.DP);
        var flxDataprotection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDataprotection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "8%",
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "0dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxDataprotection"), extendConfig({}, controller.args[1], "flxDataprotection"), extendConfig({}, controller.args[2], "flxDataprotection"));
        flxDataprotection.setDefaultUnit(kony.flex.DP);
        var flxDataProtectionTtile = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDataProtectionTtile",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "27dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxDataProtectionTtile"), extendConfig({}, controller.args[1], "flxDataProtectionTtile"), extendConfig({}, controller.args[2], "flxDataProtectionTtile"));
        flxDataProtectionTtile.setDefaultUnit(kony.flex.DP);
        var lblDataprtLabel = new kony.ui.Label(extendConfig({
            "id": "lblDataprtLabel",
            "isVisible": true,
            "skin": "sknLblModTitleNormal",
            "text": "Data Protection consents",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblDataprtLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDataprtLabel"), extendConfig({}, controller.args[2], "lblDataprtLabel"));
        var imgEditDataProtection = new kony.ui.Image2(extendConfig({
            "height": "20dp",
            "id": "imgEditDataProtection",
            "isVisible": false,
            "left": 5,
            "skin": "slImage",
            "src": "editicongray.png",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgEditDataProtection"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgEditDataProtection"), extendConfig({}, controller.args[2], "imgEditDataProtection"));
        flxDataProtectionTtile.add(lblDataprtLabel, imgEditDataProtection);
        var flxDirectMarketing = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxDirectMarketing",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDirectMarketing"), extendConfig({}, controller.args[1], "flxDirectMarketing"), extendConfig({}, controller.args[2], "flxDirectMarketing"));
        flxDirectMarketing.setDefaultUnit(kony.flex.DP);
        var lblDirectingMarketing = new kony.ui.Label(extendConfig({
            "id": "lblDirectingMarketing",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "DIRECT MARKETING",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDirectingMarketing"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDirectingMarketing"), extendConfig({}, controller.args[2], "lblDirectingMarketing"));
        var lblDirectingValue = new kony.ui.Label(extendConfig({
            "id": "lblDirectingValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Yes",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDirectingValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDirectingValue"), extendConfig({}, controller.args[2], "lblDirectingValue"));
        flxDirectMarketing.add(lblDirectingMarketing, lblDirectingValue);
        var flxCreditCheck = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxCreditCheck",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxCreditCheck"), extendConfig({}, controller.args[1], "flxCreditCheck"), extendConfig({}, controller.args[2], "flxCreditCheck"));
        flxCreditCheck.setDefaultUnit(kony.flex.DP);
        var lblCreditLabel = new kony.ui.Label(extendConfig({
            "id": "lblCreditLabel",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel60",
            "text": "CREDIT CHECK",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCreditLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditLabel"), extendConfig({}, controller.args[2], "lblCreditLabel"));
        var lblCreditValue = new kony.ui.Label(extendConfig({
            "id": "lblCreditValue",
            "isVisible": true,
            "left": 0,
            "skin": "skndefLabel39",
            "text": "Yes",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCreditValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditValue"), extendConfig({}, controller.args[2], "lblCreditValue"));
        flxCreditCheck.add(lblCreditLabel, lblCreditValue);
        flxDataprotection.add(flxDataProtectionTtile, flxDirectMarketing, flxCreditCheck);
        flxProtection.add(flxDataprotection);
        var flxPersonalConsents = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPersonalConsents",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox58",
            "top": "0dp",
            "width": "300dp"
        }, controller.args[0], "flxPersonalConsents"), extendConfig({}, controller.args[1], "flxPersonalConsents"), extendConfig({}, controller.args[2], "flxPersonalConsents"));
        flxPersonalConsents.setDefaultUnit(kony.flex.DP);
        var flxPCContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPCContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxPCContent"), extendConfig({}, controller.args[1], "flxPCContent"), extendConfig({}, controller.args[2], "flxPCContent"));
        flxPCContent.setDefaultUnit(kony.flex.DP);
        var flxPersonalConsentsTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPersonalConsentsTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "27dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxPersonalConsentsTitle"), extendConfig({}, controller.args[1], "flxPersonalConsentsTitle"), extendConfig({}, controller.args[2], "flxPersonalConsentsTitle"));
        flxPersonalConsentsTitle.setDefaultUnit(kony.flex.DP);
        var lblPCTitle = new kony.ui.Label(extendConfig({
            "id": "lblPCTitle",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblModTitleNormal",
            "text": "Personal consents",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblPCTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPCTitle"), extendConfig({}, controller.args[2], "lblPCTitle"));
        var imgEditPersonalConsents = new kony.ui.Image2(extendConfig({
            "height": "20dp",
            "id": "imgEditPersonalConsents",
            "isVisible": false,
            "left": 10,
            "skin": "slImage",
            "src": "editicongray.png",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "imgEditPersonalConsents"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgEditPersonalConsents"), extendConfig({}, controller.args[2], "imgEditPersonalConsents"));
        flxPersonalConsentsTitle.add(lblPCTitle, imgEditPersonalConsents);
        var flxPCInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "24dp",
            "clipBounds": true,
            "height": "75dp",
            "id": "flxPCInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxPCInner"), extendConfig({}, controller.args[1], "flxPCInner"), extendConfig({}, controller.args[2], "flxPCInner"));
        flxPCInner.setDefaultUnit(kony.flex.DP);
        var flxTmpPersonal = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "60dp",
            "id": "flxTmpPersonal",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSegFlxTmpInner2",
            "top": "0dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxTmpPersonal"), extendConfig({}, controller.args[1], "flxTmpPersonal"), extendConfig({}, controller.args[2], "flxTmpPersonal"));
        flxTmpPersonal.setDefaultUnit(kony.flex.DP);
        var flxPersonalConsentInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxPersonalConsentInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "16dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxPersonalConsentInner"), extendConfig({}, controller.args[1], "flxPersonalConsentInner"), extendConfig({}, controller.args[2], "flxPersonalConsentInner"));
        flxPersonalConsentInner.setDefaultUnit(kony.flex.DP);
        var lblPCLabel = new kony.ui.Label(extendConfig({
            "id": "lblPCLabel",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndef2label",
            "text": "AA19107TJ1FL",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPCLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPCLabel"), extendConfig({}, controller.args[2], "lblPCLabel"));
        var lblPCValue = new kony.ui.Label(extendConfig({
            "id": "lblPCValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndefLabel57",
            "text": "Authorised",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPCValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPCValue"), extendConfig({}, controller.args[2], "lblPCValue"));
        flxPersonalConsentInner.add(lblPCLabel, lblPCValue);
        flxTmpPersonal.add(flxPersonalConsentInner);
        flxPCInner.add(flxTmpPersonal);
        flxPCContent.add(flxPersonalConsentsTitle, flxPCInner);
        flxPersonalConsents.add(flxPCContent);
        flxConsents.add(spinnerConsents, flxProtection, flxPersonalConsents);
        flxKYCInnerContent.add(flxKYCCompliance, flxConsents);
        flxKYCContent.add(flxKYCTitletop, flxKYCInnerContent);
        flxKYCInner.add(flxKYCContent);
        flxKYCMain.add(flxKYCInner);
        var flxRelationFinancial = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxRelationFinancial",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "99%"
        }, controller.args[0], "flxRelationFinancial"), extendConfig({}, controller.args[1], "flxRelationFinancial"), extendConfig({}, controller.args[2], "flxRelationFinancial"));
        flxRelationFinancial.setDefaultUnit(kony.flex.DP);
        var flxRelFinInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "400dp",
            "id": "flxRelFinInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRelFinInner"), extendConfig({}, controller.args[1], "flxRelFinInner"), extendConfig({}, controller.args[2], "flxRelFinInner"));
        flxRelFinInner.setDefaultUnit(kony.flex.DP);
        var flxRelationChart = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerY": "50%",
            "clipBounds": false,
            "height": "96%",
            "id": "flxRelationChart",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "1%",
            "isModalContainer": false,
            "skin": "lFbox23",
            "top": 0,
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxRelationChart"), extendConfig({}, controller.args[1], "flxRelationChart"), extendConfig({}, controller.args[2], "flxRelationChart"));
        flxRelationChart.setDefaultUnit(kony.flex.DP);
        var flxRelChartInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRelChartInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxSCVBlock1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRelChartInner"), extendConfig({}, controller.args[1], "flxRelChartInner"), extendConfig({}, controller.args[2], "flxRelChartInner"));
        flxRelChartInner.setDefaultUnit(kony.flex.DP);
        var flxRelTitletop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "60dp",
            "id": "flxRelTitletop",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSCVTitle1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRelTitletop"), extendConfig({}, controller.args[1], "flxRelTitletop"), extendConfig({}, controller.args[2], "flxRelTitletop"));
        flxRelTitletop.setDefaultUnit(kony.flex.DP);
        var flxRelIconTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60%",
            "id": "flxRelIconTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxRelIconTitle"), extendConfig({}, controller.args[1], "flxRelIconTitle"), extendConfig({}, controller.args[2], "flxRelIconTitle"));
        flxRelIconTitle.setDefaultUnit(kony.flex.DP);
        var imgRelTitleIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgRelTitleIcon",
            "isVisible": false,
            "left": "21dp",
            "skin": "slImage",
            "src": "relationuser.png",
            "width": "25dp",
            "zIndex": 1
        }, controller.args[0], "imgRelTitleIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRelTitleIcon"), extendConfig({}, controller.args[2], "imgRelTitleIcon"));
        var lblRelTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRelTitle",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblSCVTitle1",
            "text": "Relationships",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRelTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRelTitle"), extendConfig({}, controller.args[2], "lblRelTitle"));
        var CopyimgEditPersonal0e465e2d4bf7f40 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "CopyimgEditPersonal0e465e2d4bf7f40",
            "isVisible": false,
            "left": 10,
            "skin": "slImage",
            "src": "editicongray.png",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "CopyimgEditPersonal0e465e2d4bf7f40"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "CopyimgEditPersonal0e465e2d4bf7f40"), extendConfig({}, controller.args[2], "CopyimgEditPersonal0e465e2d4bf7f40"));
        flxRelIconTitle.add(imgRelTitleIcon, lblRelTitle, CopyimgEditPersonal0e465e2d4bf7f40);
        flxRelTitletop.add(flxRelIconTitle);
        var spinnerRelation = new com.fulldetails.spinner(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "spinnerRelation",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0ff6a8d8239db42",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinner": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "spinnerRelation"), extendConfig({
            "overrides": {}
        }, controller.args[1], "spinnerRelation"), extendConfig({
            "overrides": {}
        }, controller.args[2], "spinnerRelation"));
        var flxOverContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxOverContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "flxOverContent"), extendConfig({}, controller.args[1], "flxOverContent"), extendConfig({}, controller.args[2], "flxOverContent"));
        flxOverContent.setDefaultUnit(kony.flex.DP);
        var flxDetail1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "400dp",
            "id": "flxDetail1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "lFbox21",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetail1"), extendConfig({}, controller.args[1], "flxDetail1"), extendConfig({}, controller.args[2], "flxDetail1"));
        flxDetail1.setDefaultUnit(kony.flex.DP);
        var lblNoRel = new kony.ui.Label(extendConfig({
            "id": "lblNoRel",
            "isVisible": false,
            "left": "0",
            "skin": "CopydefLabel0j3890b7f04f34d",
            "text": "No relationship data found.",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblNoRel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoRel"), extendConfig({}, controller.args[2], "lblNoRel"));
        var relationship = new com.customer.relationship.relationship(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "relationship",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "relationship": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                },
                "segRelationship": {
                    "data": [{
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }, {
                        "relatedPartyNameVal": "No record"
                    }]
                }
            }
        }, controller.args[0], "relationship"), extendConfig({
            "overrides": {}
        }, controller.args[1], "relationship"), extendConfig({
            "overrides": {}
        }, controller.args[2], "relationship"));
        flxDetail1.add(lblNoRel, relationship);
        flxOverContent.add(flxDetail1);
        flxRelChartInner.add(flxRelTitletop, spinnerRelation, flxOverContent);
        flxRelationChart.add(flxRelChartInner);
        var flxDocument = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "96%",
            "id": "flxDocument",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "1%",
            "isModalContainer": false,
            "skin": "lFbox23",
            "top": 0,
            "width": "49%",
            "zIndex": 1
        }, controller.args[0], "flxDocument"), extendConfig({}, controller.args[1], "flxDocument"), extendConfig({}, controller.args[2], "flxDocument"));
        flxDocument.setDefaultUnit(kony.flex.DP);
        var flxDocumentInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDocumentInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxSCVBlock1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDocumentInner"), extendConfig({}, controller.args[1], "flxDocumentInner"), extendConfig({}, controller.args[2], "flxDocumentInner"));
        flxDocumentInner.setDefaultUnit(kony.flex.DP);
        var flxDocTitletop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "60dp",
            "id": "flxDocTitletop",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSCVTitle1",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDocTitletop"), extendConfig({}, controller.args[1], "flxDocTitletop"), extendConfig({}, controller.args[2], "flxDocTitletop"));
        flxDocTitletop.setDefaultUnit(kony.flex.DP);
        var flxDocIconTitle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60%",
            "id": "flxDocIconTitle",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxDocIconTitle"), extendConfig({}, controller.args[1], "flxDocIconTitle"), extendConfig({}, controller.args[2], "flxDocIconTitle"));
        flxDocIconTitle.setDefaultUnit(kony.flex.DP);
        var imgDocTitleIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "26dp",
            "id": "imgDocTitleIcon",
            "isVisible": false,
            "left": "21dp",
            "skin": "slImage",
            "src": "document.png",
            "width": "21dp",
            "zIndex": 1
        }, controller.args[0], "imgDocTitleIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDocTitleIcon"), extendConfig({}, controller.args[2], "imgDocTitleIcon"));
        var lblDocTitle = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDocTitle",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknLblSCVTitle1",
            "text": "Documents & Images",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDocTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDocTitle"), extendConfig({}, controller.args[2], "lblDocTitle"));
        flxDocIconTitle.add(imgDocTitleIcon, lblDocTitle);
        var flxUpload = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "70%",
            "id": "flxUpload",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "flxUpload1",
            "top": "0dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxUpload"), extendConfig({}, controller.args[1], "flxUpload"), extendConfig({}, controller.args[2], "flxUpload"));
        flxUpload.setDefaultUnit(kony.flex.DP);
        var btnUpload = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnSCVTitleRightHover",
            "height": "35dp",
            "id": "btnUpload",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknbtnSCVTitleRight1",
            "text": "UPLOAD",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnUpload"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [7, 0, 7, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnUpload"), extendConfig({}, controller.args[2], "btnUpload"));
        flxUpload.add(btnUpload);
        flxDocTitletop.add(flxDocIconTitle, flxUpload);
        var spinnerDoc = new com.fulldetails.spinner(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "spinnerDoc",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0ff6a8d8239db42",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "imgSpinnerFulldetails": {
                    "src": "loadingblue.gif"
                },
                "spinner": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "spinnerDoc"), extendConfig({
            "overrides": {}
        }, controller.args[1], "spinnerDoc"), extendConfig({
            "overrides": {}
        }, controller.args[2], "spinnerDoc"));
        var flxDocContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxDocContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "flxDocContent"), extendConfig({}, controller.args[1], "flxDocContent"), extendConfig({}, controller.args[2], "flxDocContent"));
        flxDocContent.setDefaultUnit(kony.flex.DP);
        var flxDocsDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "300dp",
            "id": "flxDocsDown",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "docsDownSec",
            "top": "0dp",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxDocsDown"), extendConfig({}, controller.args[1], "flxDocsDown"), extendConfig({}, controller.args[2], "flxDocsDown"));
        flxDocsDown.setDefaultUnit(kony.flex.DP);
        var lblDocsDown = new kony.ui.Label(extendConfig({
            "id": "lblDocsDown",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblModTitleNormal",
            "text": "Documents",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblDocsDown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDocsDown"), extendConfig({}, controller.args[2], "lblDocsDown"));
        var flxDocPayOff = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": false,
            "height": "60dp",
            "id": "flxDocPayOff",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSegFlxTmpInner2",
            "top": "20dp",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "flxDocPayOff"), extendConfig({}, controller.args[1], "flxDocPayOff"), extendConfig({}, controller.args[2], "flxDocPayOff"));
        flxDocPayOff.setDefaultUnit(kony.flex.DP);
        var flxPAyOffInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxPAyOffInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "16dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "flxPAyOffInner"), extendConfig({}, controller.args[1], "flxPAyOffInner"), extendConfig({}, controller.args[2], "flxPAyOffInner"));
        flxPAyOffInner.setDefaultUnit(kony.flex.DP);
        var lblPayOffLabel = new kony.ui.Label(extendConfig({
            "id": "lblPayOffLabel",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndef2label",
            "text": "RECEIVED 16/04/10  /  UPDATED 16/04/10",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayOffLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayOffLabel"), extendConfig({}, controller.args[2], "lblPayOffLabel"));
        var lblPayOffValue = new kony.ui.Label(extendConfig({
            "id": "lblPayOffValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndefLabel57",
            "text": "Payoff Statement",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayOffValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayOffValue"), extendConfig({}, controller.args[2], "lblPayOffValue"));
        flxPAyOffInner.add(lblPayOffLabel, lblPayOffValue);
        var FlexContainer0i423b23fc00648 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19dp",
            "id": "FlexContainer0i423b23fc00648",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "FlexContainer0i423b23fc00648"), extendConfig({}, controller.args[1], "FlexContainer0i423b23fc00648"), extendConfig({}, controller.args[2], "FlexContainer0i423b23fc00648"));
        FlexContainer0i423b23fc00648.setDefaultUnit(kony.flex.DP);
        var imgDownDoc = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgDownDoc",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "download.png",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "imgDownDoc"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDownDoc"), extendConfig({}, controller.args[2], "imgDownDoc"));
        FlexContainer0i423b23fc00648.add(imgDownDoc);
        flxDocPayOff.add(flxPAyOffInner, FlexContainer0i423b23fc00648);
        var flxDX = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": false,
            "height": "60dp",
            "id": "flxDX",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSegFlxTmpInner2",
            "top": "0dp",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "flxDX"), extendConfig({}, controller.args[1], "flxDX"), extendConfig({}, controller.args[2], "flxDX"));
        flxDX.setDefaultUnit(kony.flex.DP);
        var flxDXInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxDXInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "16dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "flxDXInner"), extendConfig({}, controller.args[1], "flxDXInner"), extendConfig({}, controller.args[2], "flxDXInner"));
        flxDXInner.setDefaultUnit(kony.flex.DP);
        var lblDXLabel = new kony.ui.Label(extendConfig({
            "id": "lblDXLabel",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndef2label",
            "text": "RECEIVED 16/04/10  /  UPDATED 16/04/10",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDXLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDXLabel"), extendConfig({}, controller.args[2], "lblDXLabel"));
        var lblDXValue = new kony.ui.Label(extendConfig({
            "id": "lblDXValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndefLabel57",
            "text": "DX Trading",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDXValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDXValue"), extendConfig({}, controller.args[2], "lblDXValue"));
        flxDXInner.add(lblDXLabel, lblDXValue);
        var flxDXDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19dp",
            "id": "flxDXDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "flxDXDown"), extendConfig({}, controller.args[1], "flxDXDown"), extendConfig({}, controller.args[2], "flxDXDown"));
        flxDXDown.setDefaultUnit(kony.flex.DP);
        var imgDownDocDX = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgDownDocDX",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "download.png",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "imgDownDocDX"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDownDocDX"), extendConfig({}, controller.args[2], "imgDownDocDX"));
        flxDXDown.add(imgDownDocDX);
        flxDX.add(flxDXInner, flxDXDown);
        var flxIntroductory = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": false,
            "height": "60dp",
            "id": "flxIntroductory",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSegFlxTmpInner2",
            "top": "0dp",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "flxIntroductory"), extendConfig({}, controller.args[1], "flxIntroductory"), extendConfig({}, controller.args[2], "flxIntroductory"));
        flxIntroductory.setDefaultUnit(kony.flex.DP);
        var flxIntroInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxIntroInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "16dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "flxIntroInner"), extendConfig({}, controller.args[1], "flxIntroInner"), extendConfig({}, controller.args[2], "flxIntroInner"));
        flxIntroInner.setDefaultUnit(kony.flex.DP);
        var lblIntroLabel = new kony.ui.Label(extendConfig({
            "id": "lblIntroLabel",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndef2label",
            "text": "NOT RECEIVED 15/05/10",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIntroLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIntroLabel"), extendConfig({}, controller.args[2], "lblIntroLabel"));
        var lblIntroValue = new kony.ui.Label(extendConfig({
            "id": "lblIntroValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndefLabel57",
            "text": "Introductory Document",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIntroValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIntroValue"), extendConfig({}, controller.args[2], "lblIntroValue"));
        flxIntroInner.add(lblIntroLabel, lblIntroValue);
        var flxIntroDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19dp",
            "id": "flxIntroDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "flxIntroDown"), extendConfig({}, controller.args[1], "flxIntroDown"), extendConfig({}, controller.args[2], "flxIntroDown"));
        flxIntroDown.setDefaultUnit(kony.flex.DP);
        var imgIntroDown = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgIntroDown",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "download.png",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "imgIntroDown"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgIntroDown"), extendConfig({}, controller.args[2], "imgIntroDown"));
        flxIntroDown.add(imgIntroDown);
        flxIntroductory.add(flxIntroInner, flxIntroDown);
        var lbl2More = new kony.ui.Label(extendConfig({
            "id": "lbl2More",
            "isVisible": false,
            "left": "0",
            "skin": "defLabel1",
            "text": "2 MORE",
            "top": "12dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lbl2More"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl2More"), extendConfig({}, controller.args[2], "lbl2More"));
        var lblNoRecordDoc = new kony.ui.Label(extendConfig({
            "id": "lblNoRecordDoc",
            "isVisible": false,
            "left": "0",
            "skin": "CopydefLabel0a420ecc77bed42",
            "text": "No Records Found",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblNoRecordDoc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoRecordDoc"), extendConfig({}, controller.args[2], "lblNoRecordDoc"));
        var documents = new com.documentImages.doc(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "280dp",
            "id": "documents",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "overrides": {
                "doc": {
                    "bottom": "viz.val_cleared",
                    "height": "280dp",
                    "top": "10dp"
                },
                "segDoc": {
                    "bottom": "viz.val_cleared",
                    "data": [{
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }],
                    "height": "280dp"
                }
            }
        }, controller.args[0], "documents"), extendConfig({
            "overrides": {}
        }, controller.args[1], "documents"), extendConfig({
            "overrides": {}
        }, controller.args[2], "documents"));
        documents.segDoc.onRowClick = controller.AS_Segment_hdbf87504a9f44d09e29c6d9df92f953;
        flxDocsDown.add(lblDocsDown, flxDocPayOff, flxDX, flxIntroductory, lbl2More, lblNoRecordDoc, documents);
        var flxImgDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "300dp",
            "id": "flxImgDown",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "docsDownSec",
            "top": "0dp",
            "width": "48%",
            "zIndex": 1
        }, controller.args[0], "flxImgDown"), extendConfig({}, controller.args[1], "flxImgDown"), extendConfig({}, controller.args[2], "flxImgDown"));
        flxImgDown.setDefaultUnit(kony.flex.DP);
        var lblImgTitle = new kony.ui.Label(extendConfig({
            "id": "lblImgTitle",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblModTitleNormal",
            "text": "Images",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblImgTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgTitle"), extendConfig({}, controller.args[2], "lblImgTitle"));
        var flxPhotoId = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": false,
            "height": "60dp",
            "id": "flxPhotoId",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSegFlxTmpInner2",
            "top": "20dp",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "flxPhotoId"), extendConfig({}, controller.args[1], "flxPhotoId"), extendConfig({}, controller.args[2], "flxPhotoId"));
        flxPhotoId.setDefaultUnit(kony.flex.DP);
        var flxPhotoIdInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxPhotoIdInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "16dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "flxPhotoIdInner"), extendConfig({}, controller.args[1], "flxPhotoIdInner"), extendConfig({}, controller.args[2], "flxPhotoIdInner"));
        flxPhotoIdInner.setDefaultUnit(kony.flex.DP);
        var lblPhotoIdLabel = new kony.ui.Label(extendConfig({
            "id": "lblPhotoIdLabel",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndef2label",
            "text": "RECEIVED 16/04/10  /  UPDATED 16/04/10",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPhotoIdLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPhotoIdLabel"), extendConfig({}, controller.args[2], "lblPhotoIdLabel"));
        var lblPhotoIdValue = new kony.ui.Label(extendConfig({
            "id": "lblPhotoIdValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndefLabel57",
            "text": "Photo ID",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPhotoIdValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPhotoIdValue"), extendConfig({}, controller.args[2], "lblPhotoIdValue"));
        flxPhotoIdInner.add(lblPhotoIdLabel, lblPhotoIdValue);
        var flxPhotoDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19dp",
            "id": "flxPhotoDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "flxPhotoDown"), extendConfig({}, controller.args[1], "flxPhotoDown"), extendConfig({}, controller.args[2], "flxPhotoDown"));
        flxPhotoDown.setDefaultUnit(kony.flex.DP);
        var imgPhotoIdDown = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgPhotoIdDown",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "download.png",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "imgPhotoIdDown"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPhotoIdDown"), extendConfig({}, controller.args[2], "imgPhotoIdDown"));
        flxPhotoDown.add(imgPhotoIdDown);
        flxPhotoId.add(flxPhotoIdInner, flxPhotoDown);
        var flxSignature = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": false,
            "height": "60dp",
            "id": "flxSignature",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSegFlxTmpInner2",
            "top": "0dp",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "flxSignature"), extendConfig({}, controller.args[1], "flxSignature"), extendConfig({}, controller.args[2], "flxSignature"));
        flxSignature.setDefaultUnit(kony.flex.DP);
        var flxSignatureInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "45dp",
            "id": "flxSignatureInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "16dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "flxSignatureInner"), extendConfig({}, controller.args[1], "flxSignatureInner"), extendConfig({}, controller.args[2], "flxSignatureInner"));
        flxSignatureInner.setDefaultUnit(kony.flex.DP);
        var lblSigLabel = new kony.ui.Label(extendConfig({
            "id": "lblSigLabel",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndef2label",
            "text": "RECEIVED 16/04/10  /  UPDATED 16/04/10",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSigLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSigLabel"), extendConfig({}, controller.args[2], "lblSigLabel"));
        var lblSignatureValue = new kony.ui.Label(extendConfig({
            "id": "lblSignatureValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "skndefLabel57",
            "text": "Signature",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSignatureValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSignatureValue"), extendConfig({}, controller.args[2], "lblSignatureValue"));
        flxSignatureInner.add(lblSigLabel, lblSignatureValue);
        var flxSigDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19dp",
            "id": "flxSigDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "flxSigDown"), extendConfig({}, controller.args[1], "flxSigDown"), extendConfig({}, controller.args[2], "flxSigDown"));
        flxSigDown.setDefaultUnit(kony.flex.DP);
        var imgSignatureDown = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "18dp",
            "id": "imgSignatureDown",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "download.png",
            "top": "0",
            "width": "16dp"
        }, controller.args[0], "imgSignatureDown"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSignatureDown"), extendConfig({}, controller.args[2], "imgSignatureDown"));
        flxSigDown.add(imgSignatureDown);
        flxSignature.add(flxSignatureInner, flxSigDown);
        var lblNoRecordImg = new kony.ui.Label(extendConfig({
            "id": "lblNoRecordImg",
            "isVisible": false,
            "left": "0",
            "skin": "CopydefLabel0a579ad22bc0d4f",
            "text": "No Records Found",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblNoRecordImg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoRecordImg"), extendConfig({}, controller.args[2], "lblNoRecordImg"));
        var images = new com.documentImages.doc(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "280dp",
            "id": "images",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "overrides": {
                "doc": {
                    "bottom": "viz.val_cleared",
                    "height": "280dp",
                    "minWidth": "viz.val_cleared",
                    "top": "10dp"
                },
                "segDoc": {
                    "bottom": "viz.val_cleared",
                    "data": [{
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }, {
                        "imgDownload": "download_2.png",
                        "lblName": "Name",
                        "recievedDate": "Received",
                        "updatedDate": "Updated"
                    }],
                    "height": "280dp"
                }
            }
        }, controller.args[0], "images"), extendConfig({
            "overrides": {}
        }, controller.args[1], "images"), extendConfig({
            "overrides": {}
        }, controller.args[2], "images"));
        images.segDoc.onRowClick = controller.AS_Segment_b7520f12d410480ea1e0bfbe2814c4fd;
        flxImgDown.add(lblImgTitle, flxPhotoId, flxSignature, lblNoRecordImg, images);
        flxDocContent.add(flxDocsDown, flxImgDown);
        flxDocumentInner.add(flxDocTitletop, spinnerDoc, flxDocContent);
        flxDocument.add(flxDocumentInner);
        flxRelFinInner.add(flxRelationChart, flxDocument);
        flxRelationFinancial.add(flxRelFinInner);
        CDM.add(flxBasicHeader, flxBasicMain, flxKYCMain, flxRelationFinancial);
        CDM.breakpointResetData = {};
        CDM.breakpointData = {
            maxBreakpointWidth: 1200,
            "640": {
                "flxBasicMain": {
                    "layoutType": kony.flex.FLOW_VERTICAL,
                    "segmentProps": []
                },
                "flxBasicInner": {
                    "segmentProps": []
                },
                "flxKYCMain": {
                    "layoutType": kony.flex.FLOW_VERTICAL,
                    "segmentProps": []
                },
                "flxKYCInner": {
                    "segmentProps": []
                },
                "flxRelFinInner": {
                    "layoutType": kony.flex.FLOW_VERTICAL,
                    "segmentProps": []
                },
                "flxRelationChart": {
                    "segmentProps": []
                },
                "flxDocument": {
                    "segmentProps": []
                }
            },
            "1200": {
                "flxBasicMain": {
                    "segmentProps": []
                },
                "flxKYCMain": {
                    "segmentProps": []
                },
                "flxRelFinInner": {
                    "segmentProps": []
                }
            }
        }
        CDM.compInstData = {
            "titleHeader.imgPrevArrow": {
                "src": "bread_arrow_1.png"
            },
            "spinnerPersonal.imgSpinnerFulldetails": {
                "src": "loadingblue.gif"
            },
            "spinnerContactDetails.imgSpinnerFulldetails": {
                "src": "loadingblue.gif"
            },
            "spinnerContactMethod.imgSpinnerFulldetails": {
                "src": "loadingblue.gif"
            },
            "spinnerKYC.imgSpinnerFulldetails": {
                "src": "loadingblue.gif"
            },
            "spinnerConsents.imgSpinnerFulldetails": {
                "src": "loadingblue.gif"
            },
            "spinnerRelation.imgSpinnerFulldetails": {
                "src": "loadingblue.gif"
            },
            "relationship.segRelationship": {
                "data": [{
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }, {
                    "relatedPartyNameVal": "No record"
                }]
            },
            "spinnerDoc.imgSpinnerFulldetails": {
                "src": "loadingblue.gif"
            },
            "documents": {
                "bottom": "",
                "height": "280dp",
                "top": "10dp"
            },
            "documents.segDoc": {
                "bottom": "",
                "data": [{
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }],
                "height": "280dp"
            },
            "images": {
                "bottom": "",
                "height": "280dp",
                "minWidth": "",
                "top": "10dp"
            },
            "images.segDoc": {
                "bottom": "",
                "data": [{
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }, {
                    "imgDownload": "download_2.png",
                    "lblName": "Name",
                    "recievedDate": "Received",
                    "updatedDate": "Updated"
                }],
                "height": "280dp"
            }
        }
        return CDM;
    }
})