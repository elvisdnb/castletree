define("fullDetails/CDM/userCDMController", function() {
    return {};
});
define("fullDetails/CDM/CDMControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("fullDetails/CDM/CDMController", ["fullDetails/CDM/userCDMController", "fullDetails/CDM/CDMControllerActions"], function() {
    var controller = require("fullDetails/CDM/userCDMController");
    var actions = require("fullDetails/CDM/CDMControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
