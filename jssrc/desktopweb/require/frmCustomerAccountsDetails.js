define("frmCustomerAccountsDetails", function() {
    return function(controller) {
        function addWidgetsfrmCustomerAccountsDetails() {
            this.setDefaultUnit(kony.flex.PX);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0i0f529ea88df4b",
                "top": "0%",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0c70bfb5d2e724d"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0i897dea905994f"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_be20201bc2ec4a94b104c0c955d5f1ea;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_b5fdff74b36a4ea69d10cb4a2eee5257;
            var flxA = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxA",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "64dp",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0be837352e79b42",
                "top": "0dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxA.setDefaultUnit(kony.flex.DP);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "0dp",
                "width": "98%",
                "zIndex": 2
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var flxDesign = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "82dp",
                "id": "flxDesign",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0j394a0adf4dd4a",
                "top": "0dp",
                "width": "4dp",
                "zIndex": 1
            }, {}, {});
            flxDesign.setDefaultUnit(kony.flex.DP);
            flxDesign.add();
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": false,
                "left": "22dp",
                "width": "24dp",
                "height": "24dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var flxTightIconWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "26dp",
                "id": "flxTightIconWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22dp",
                "isModalContainer": false,
                "skin": "sknFlxGreenRounder",
                "width": "26dp",
                "zIndex": 1
            }, {}, {});
            flxTightIconWrapper.setDefaultUnit(kony.flex.DP);
            var lblTightIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "80%",
                "id": "lblTightIcon",
                "isVisible": true,
                "skin": "sknlblGreenIcon",
                "text": "Z",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTightIconWrapper.add(lblTightIcon);
            var lblSuccessNotification = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageWithReferenceNumber = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMessageWithReferenceNumber",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BGcss",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgHeaderClose1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "14dp",
                "id": "imgHeaderClose1",
                "isVisible": false,
                "right": "24dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "width": "14dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxImageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxImageHeader.setDefaultUnit(kony.flex.DP);
            var imgHeaderClose = new kony.ui.Button({
                "centerX": "50%",
                "centerY": "50%",
                "height": "24dp",
                "id": "imgHeaderClose",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknlblCloseGreen",
                "text": "L",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageHeader.add(imgHeaderClose);
            flxMessageInfo.add(flxDesign, cusStatusIcon, flxTightIconWrapper, lblSuccessNotification, lblMessageWithReferenceNumber, imgHeaderClose1, flxImageHeader);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": true,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "200dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "112dp",
                "skin": "sknLbl12pxBG4D4D4D",
                "text": "Overview",
                "top": "26dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var flxCntnr = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxCntnr",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknHeader",
                "top": "0dp",
                "width": "98%",
                "zIndex": 2
            }, {}, {});
            flxCntnr.setDefaultUnit(kony.flex.DP);
            var imgSrrc2 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgSrrc2",
                "isVisible": true,
                "right": "170px",
                "skin": "slImage",
                "src": "notification.png",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgChatBot = new kony.ui.Image2({
                "height": "100%",
                "id": "imgChatBot",
                "isVisible": true,
                "right": "240px",
                "skin": "slImage",
                "src": "hdr44.png",
                "top": "1dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgLogo = new kony.ui.Image2({
                "height": "100%",
                "id": "imgLogo",
                "isVisible": false,
                "left": "83dp",
                "onTouchEnd": controller.AS_Image_g3a3ad30dcc840259581915b82f3bc25,
                "skin": "slImage",
                "src": "temlogo_1.png",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgmenu = new kony.ui.Image2({
                "height": "100%",
                "id": "imgmenu",
                "isVisible": false,
                "left": "10dp",
                "onDownloadComplete": controller.AS_Image_e4ad9687710a4d369b0ad2401f72b235,
                "skin": "slImage",
                "src": "menu_header.png",
                "top": "0dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "580px"
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var imgSrc = new kony.ui.Image2({
                "centerY": "50%",
                "height": "40px",
                "id": "imgSrc",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "search.png",
                "width": "60px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtField1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "focusSkin": "defTextBoxFocus",
                "height": "40px",
                "id": "txtField1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Search Customer",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "skngreybg",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [10, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxSearch.add(imgSrc, txtField1);
            var btnLogin = new kony.ui.Button({
                "focusSkin": "CopybtnDefProfile2",
                "height": "50dp",
                "id": "btnLogin",
                "isVisible": false,
                "onClick": controller.AS_Button_c344b4b2a60f41dd896473d657819e08,
                "right": "40dp",
                "skin": "defBtnNormal",
                "text": "Logout",
                "top": "28dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCntnr.add(imgSrrc2, imgChatBot, imgLogo, imgmenu, flxSearch, btnLogin);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "8dp",
                "width": "98%",
                "zIndex": 2,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ContractRequestDescription = new com.lending.contractRequestDescription.ContractRequestDescription({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ContractRequestDescription",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxBGffffffBorderCCE3F7",
                "top": "16dp",
                "width": "98%",
                "zIndex": 5,
                "overrides": {
                    "ContractRequestDescription": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "height": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var MainTabs = new com.lending.MainTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxHeight": "42dp",
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "0dp",
                "width": "98%",
                "zIndex": 2,
                "overrides": {
                    "MainTabs": {
                        "height": "64dp",
                        "left": "0dp",
                        "maxHeight": "42dp",
                        "top": "0dp",
                        "zIndex": 2
                    },
                    "btnAccounts2": {
                        "left": "40dp"
                    },
                    "btnAccounts3": {
                        "left": "40dp"
                    },
                    "flxContainerAccount": {
                        "height": "2dp"
                    },
                    "flxContainerdeposits": {
                        "height": "2dp",
                        "left": "147dp"
                    },
                    "flxontainerLoans": {
                        "left": "267dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var DPWSelectLoan = new kony.ui.CustomWidget({
                "id": "DPWSelectLoan",
                "isVisible": false,
                "right": "20dp",
                "top": "-64dp",
                "width": "260dp",
                "height": "64dp",
                "zIndex": 2,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "disabled": false,
                "labelText": "Create New Loan",
                "options": "[{\"label\": \"Select Type\", \"value\":\"None\"},{\"label\": \"Personal Loan\",\"value\":\"PERSONAL.LOAN.STANDARD\"},{\"label\": \"Mortgage Loan\",\"value\":\"MORTGAGE\"}]",
                "value": ""
            });
            var flxLoanButtonWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "40dp",
                "id": "flxLoanButtonWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "2%",
                "skin": "slFbox",
                "top": "-55dp",
                "width": "20%",
                "zIndex": 5
            }, {}, {});
            flxLoanButtonWrapper.setDefaultUnit(kony.flex.DP);
            var btnCreateLoan = new kony.ui.CustomWidget({
                "id": "btnCreateLoan",
                "isVisible": true,
                "right": "0dp",
                "top": "0dp",
                "width": "155dp",
                "height": 40,
                "zIndex": 2,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "add_box",
                "labelText": "Create New Loan",
                "outlined": false,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxLoanTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "72dp",
                "id": "flxLoanTypes",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoanBg",
                "top": "40px",
                "width": "155dp",
                "zIndex": 10
            }, {}, {});
            flxLoanTypes.setDefaultUnit(kony.flex.DP);
            var flxPersonal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "32dp",
                "id": "flxPersonal",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "2dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {
                "hoverSkin": "CopyslFbox0d6f2870369004e"
            });
            flxPersonal.setDefaultUnit(kony.flex.DP);
            var PersonalLoanIcon1 = new kony.ui.CustomWidget({
                "id": "PersonalLoanIcon1",
                "isVisible": false,
                "left": "0",
                "top": "0",
                "width": "25dp",
                "height": "25dp",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "",
                "iconName": "accessibility_new",
                "size": "small"
            });
            var PersonalLoanIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "PersonalLoanIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0i53ac713882c4f",
                "text": "V",
                "top": 0,
                "width": "25dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPersonalLoan = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblPersonalLoan",
                "isVisible": true,
                "left": 5,
                "skin": "CopydefLabel0f2830dae573948",
                "text": "Personal Loan",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPersonal.add(PersonalLoanIcon1, PersonalLoanIcon, lblPersonalLoan);
            var flxMortgage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "32dp",
                "id": "flxMortgage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "4dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {
                "hoverSkin": "CopyslFbox0d6f2870369004e"
            });
            flxMortgage.setDefaultUnit(kony.flex.DP);
            var MortgageIcon1 = new kony.ui.CustomWidget({
                "id": "MortgageIcon1",
                "isVisible": false,
                "left": "0",
                "top": "0",
                "width": "25dp",
                "height": "25dp",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "",
                "iconName": "home",
                "size": "small"
            });
            var MortgageIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "MortgageIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopydefLabel0i53ac713882c4f",
                "text": "F",
                "top": 0,
                "width": "25dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMortgageLoan = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblMortgageLoan",
                "isVisible": true,
                "left": 5,
                "skin": "CopydefLabel0f2830dae573948",
                "text": "Mortgage",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMortgage.add(MortgageIcon1, MortgageIcon, lblMortgageLoan);
            flxLoanTypes.add(flxPersonal, flxMortgage);
            flxLoanButtonWrapper.add(btnCreateLoan, flxLoanTypes);
            var flxDepositButtonWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "40dp",
                "id": "flxDepositButtonWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "2%",
                "skin": "slFbox",
                "top": "-55dp",
                "width": "20%",
                "zIndex": 5
            }, {}, {});
            flxDepositButtonWrapper.setDefaultUnit(kony.flex.DP);
            var btnCreateDeposit = new kony.ui.CustomWidget({
                "id": "btnCreateDeposit",
                "isVisible": true,
                "right": 0,
                "top": "0",
                "width": "170dp",
                "height": "40dp",
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "add_box",
                "labelText": "Create New Deposit",
                "outlined": false,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxDepositTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "72dp",
                "id": "flxDepositTypes",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoanBg",
                "top": "40px",
                "width": "155dp",
                "zIndex": 10
            }, {}, {});
            flxDepositTypes.setDefaultUnit(kony.flex.DP);
            var flxShort = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "32dp",
                "id": "flxShort",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "4dp",
                "width": "90%",
                "zIndex": 5
            }, {}, {});
            flxShort.setDefaultUnit(kony.flex.DP);
            var shortDepositIcon1 = new kony.ui.CustomWidget({
                "id": "shortDepositIcon1",
                "isVisible": false,
                "left": "0",
                "top": "0",
                "width": "25dp",
                "height": "25dp",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "",
                "iconName": "accessibility_new",
                "size": "small"
            });
            var shortDepositIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "shortDepositIcon",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0i53ac713882c4f",
                "text": "b",
                "top": 0,
                "width": "25dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblShortDeposit = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblShortDeposit",
                "isVisible": true,
                "left": 5,
                "skin": "CopydefLabel0f2830dae573948",
                "text": "Short Term",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxShort.add(shortDepositIcon1, shortDepositIcon, lblShortDeposit);
            var flxLong = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "32dp",
                "id": "flxLong",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "4dp",
                "width": "90%",
                "zIndex": 5
            }, {}, {});
            flxLong.setDefaultUnit(kony.flex.DP);
            var longDepositIcon1 = new kony.ui.CustomWidget({
                "id": "longDepositIcon1",
                "isVisible": false,
                "left": "0",
                "top": "0",
                "width": "25dp",
                "height": "25dp",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "",
                "iconName": "home",
                "size": "small"
            });
            var longDepositIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "longDepositIcon",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0i53ac713882c4f",
                "text": "b",
                "top": 0,
                "width": "25dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLongDeposit = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblLongDeposit",
                "isVisible": true,
                "left": 5,
                "skin": "CopydefLabel0f2830dae573948",
                "text": "Long Term",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLong.add(longDepositIcon1, longDepositIcon, lblLongDeposit);
            flxDepositTypes.add(flxShort, flxLong);
            flxDepositButtonWrapper.add(btnCreateDeposit, flxDepositTypes);
            var flxNow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxNow",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxNow.setDefaultUnit(kony.flex.DP);
            flxNow.add();
            var flxLoansHdrSeg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxLoansHdrSeg",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "sknGrey",
                "top": "20dp",
                "width": "96%"
            }, {}, {});
            flxLoansHdrSeg.setDefaultUnit(kony.flex.DP);
            var lblAcNum = new kony.ui.Label({
                "height": "100%",
                "id": "lblAcNum",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLoansHeading",
                "text": "Account Type",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAcType = new kony.ui.Label({
                "height": "100%",
                "id": "lblAcType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLoansHeading",
                "text": "Account Number",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCCY = new kony.ui.Label({
                "height": "100%",
                "id": "lblCCY",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLoansHeading",
                "text": "CCY",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblClearBal = new kony.ui.Label({
                "height": "100%",
                "id": "lblClearBal",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLoansHeading",
                "text": "Loan Balance",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            var lblLockedAc = new kony.ui.Label({
                "height": "100%",
                "id": "lblLockedAc",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLoansHeading",
                "text": "Loan Start Date",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAvlBal = new kony.ui.Label({
                "height": "100%",
                "id": "lblAvlBal",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLoansHeading",
                "text": "ArrangementId",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAcServices = new kony.ui.Label({
                "height": "100%",
                "id": "lblAcServices",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLoansHeading",
                "text": "Service Action",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLoansHdrSeg.add(lblAcNum, lblAcType, lblCCY, lblClearBal, lblLockedAc, lblAvlBal, lblAcServices);
            var FlexContainer0gedc2f7c182c4c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "FlexContainer0gedc2f7c182c4c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLine",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0gedc2f7c182c4c.setDefaultUnit(kony.flex.DP);
            FlexContainer0gedc2f7c182c4c.add();
            var SegOne = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "Calendar0hdacae9433e24c": {
                        "dateComponents": null,
                        "dateFormat": "dd/MM/yyyy"
                    },
                    "CopyListBox0i4da39afd4404c": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ]
                    },
                    "CopylblEur0j19f012f82814c": "",
                    "CopylblNext0e3352c17f17645": "",
                    "CopylblNext0f22d02f0470044": "",
                    "ListBox0On": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ]
                    },
                    "ListBox0c039746869c84b": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ]
                    },
                    "btnCancel": "",
                    "btnConfirm": "",
                    "lblAcNum": "",
                    "lblAcNumOn": "",
                    "lblAcType": "",
                    "lblAcTypeOn": "",
                    "lblAvlBal": "",
                    "lblAvlBalOn": "",
                    "lblCCY": "",
                    "lblCCYOn": "",
                    "lblClearBal": "",
                    "lblClearBalOn": "",
                    "lblCurrentPayment": "",
                    "lblDefhu": "",
                    "lblEur": "",
                    "lblHdr": "",
                    "lblHoliday": "",
                    "lblLockedAc": "",
                    "lblLockedAcOn": "",
                    "lblMaturityDate": "",
                    "lblNext": "",
                    "lblPayAmount": "",
                    "lblPayAmountOpted": "",
                    "lblRecalulate": ""
                }],
                "groupCells": false,
                "height": "400dp",
                "id": "SegOne",
                "isVisible": false,
                "left": "18dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "CopyCopyseg",
                "rowTemplate": "flxInitial1",
                "sectionHeaderSkin": "CopysliPhoneSegmentHeader0da4b0f6ff2a940",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "Calendar0hdacae9433e24c": "Calendar0hdacae9433e24c",
                    "CopyListBox0i4da39afd4404c": "CopyListBox0i4da39afd4404c",
                    "CopylblEur0j19f012f82814c": "CopylblEur0j19f012f82814c",
                    "CopylblNext0e3352c17f17645": "CopylblNext0e3352c17f17645",
                    "CopylblNext0f22d02f0470044": "CopylblNext0f22d02f0470044",
                    "FlxCurrentPaymentDetailss": "FlxCurrentPaymentDetailss",
                    "FlxDAteRecalculate": "FlxDAteRecalculate",
                    "ListBox0On": "ListBox0On",
                    "ListBox0c039746869c84b": "ListBox0c039746869c84b",
                    "btnCancel": "btnCancel",
                    "btnConfirm": "btnConfirm",
                    "flxDetails": "flxDetails",
                    "flxInitial1": "flxInitial1",
                    "flxNow": "flxNow",
                    "flxRecalculate": "flxRecalculate",
                    "flxStartDate": "flxStartDate",
                    "flxbgBlue": "flxbgBlue",
                    "flxdividerline1": "flxdividerline1",
                    "flxgraph": "flxgraph",
                    "lblAcNum": "lblAcNum",
                    "lblAcNumOn": "lblAcNumOn",
                    "lblAcType": "lblAcType",
                    "lblAcTypeOn": "lblAcTypeOn",
                    "lblAvlBal": "lblAvlBal",
                    "lblAvlBalOn": "lblAvlBalOn",
                    "lblCCY": "lblCCY",
                    "lblCCYOn": "lblCCYOn",
                    "lblClearBal": "lblClearBal",
                    "lblClearBalOn": "lblClearBalOn",
                    "lblCurrentPayment": "lblCurrentPayment",
                    "lblDefhu": "lblDefhu",
                    "lblEur": "lblEur",
                    "lblHdr": "lblHdr",
                    "lblHoliday": "lblHoliday",
                    "lblLockedAc": "lblLockedAc",
                    "lblLockedAcOn": "lblLockedAcOn",
                    "lblMaturityDate": "lblMaturityDate",
                    "lblNext": "lblNext",
                    "lblPayAmount": "lblPayAmount",
                    "lblPayAmountOpted": "lblPayAmountOpted",
                    "lblRecalulate": "lblRecalulate"
                },
                "width": "96%",
                "zIndex": 4
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segMin2 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAvailableLimit": "",
                    "lblCCY": "",
                    "lblClearedBalance": "",
                    "lblHdr": "",
                    "lblHeaderAN": "",
                    "lblHeaderAccountType": "",
                    "lblLockedAnalysis": "",
                    "lblServices": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ]
                    },
                    "lstBox": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ]
                    }
                }],
                "groupCells": false,
                "height": "400dp",
                "id": "segMin2",
                "isVisible": false,
                "left": "18px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "CopyCopyseg",
                "rowTemplate": "CopyflxTemp0d4943827dcd74a",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d3ddea00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "CopyflxTemp0d4943827dcd74a": "CopyflxTemp0d4943827dcd74a",
                    "FlexContainer0e9ca59adb7ed43": "FlexContainer0e9ca59adb7ed43",
                    "lblAvailableLimit": "lblAvailableLimit",
                    "lblCCY": "lblCCY",
                    "lblClearedBalance": "lblClearedBalance",
                    "lblHdr": "lblHdr",
                    "lblHeaderAN": "lblHeaderAN",
                    "lblHeaderAccountType": "lblHeaderAccountType",
                    "lblLockedAnalysis": "lblLockedAnalysis",
                    "lblServices": "lblServices",
                    "lstBox": "lstBox"
                },
                "width": "96%",
                "zIndex": 4
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segDeposits = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "Calendar0hdacae9433e24c": {
                        "dateComponents": null,
                        "dateFormat": "dd/MM/yyyy"
                    },
                    "CopyListBox0i4da39afd4404c": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ]
                    },
                    "CopylblEur0j19f012f82814c": "",
                    "CopylblNext0e3352c17f17645": "",
                    "CopylblNext0f22d02f0470044": "",
                    "ListBox0On": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ]
                    },
                    "ListBox0c039746869c84b": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ]
                    },
                    "btnCancel": "",
                    "btnConfirm": "",
                    "lblAcNum": "",
                    "lblAcNumOn": "",
                    "lblAcType": "",
                    "lblAcTypeOn": "",
                    "lblAvlBal": "",
                    "lblAvlBalOn": "",
                    "lblCCY": "",
                    "lblCCYOn": "",
                    "lblClearBal": "",
                    "lblClearBalOn": "",
                    "lblCurrentPayment": "",
                    "lblDefhu": "",
                    "lblEur": "",
                    "lblHdr": "",
                    "lblHoliday": "",
                    "lblLockedAc": "",
                    "lblLockedAcOn": "",
                    "lblMaturityDate": "",
                    "lblNext": "",
                    "lblPayAmount": "",
                    "lblPayAmountOpted": "",
                    "lblRecalulate": ""
                }],
                "groupCells": false,
                "height": "400dp",
                "id": "segDeposits",
                "isVisible": false,
                "left": "18dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "CopyCopyseg",
                "rowTemplate": "flxDepositsRow",
                "sectionHeaderSkin": "CopysliPhoneSegmentHeader0da4b0f6ff2a940",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "Calendar0hdacae9433e24c": "Calendar0hdacae9433e24c",
                    "CopyListBox0i4da39afd4404c": "CopyListBox0i4da39afd4404c",
                    "CopylblEur0j19f012f82814c": "CopylblEur0j19f012f82814c",
                    "CopylblNext0e3352c17f17645": "CopylblNext0e3352c17f17645",
                    "CopylblNext0f22d02f0470044": "CopylblNext0f22d02f0470044",
                    "FlxCurrentPaymentDetailss": "FlxCurrentPaymentDetailss",
                    "FlxDAteRecalculate": "FlxDAteRecalculate",
                    "ListBox0On": "ListBox0On",
                    "ListBox0c039746869c84b": "ListBox0c039746869c84b",
                    "btnCancel": "btnCancel",
                    "btnConfirm": "btnConfirm",
                    "flxDepositsRow": "flxDepositsRow",
                    "flxDetails": "flxDetails",
                    "flxNow": "flxNow",
                    "flxRecalculate": "flxRecalculate",
                    "flxStartDate": "flxStartDate",
                    "flxbgBlue": "flxbgBlue",
                    "flxdividerline1": "flxdividerline1",
                    "flxgraph": "flxgraph",
                    "lblAcNum": "lblAcNum",
                    "lblAcNumOn": "lblAcNumOn",
                    "lblAcType": "lblAcType",
                    "lblAcTypeOn": "lblAcTypeOn",
                    "lblAvlBal": "lblAvlBal",
                    "lblAvlBalOn": "lblAvlBalOn",
                    "lblCCY": "lblCCY",
                    "lblCCYOn": "lblCCYOn",
                    "lblClearBal": "lblClearBal",
                    "lblClearBalOn": "lblClearBalOn",
                    "lblCurrentPayment": "lblCurrentPayment",
                    "lblDefhu": "lblDefhu",
                    "lblEur": "lblEur",
                    "lblHdr": "lblHdr",
                    "lblHoliday": "lblHoliday",
                    "lblLockedAc": "lblLockedAc",
                    "lblLockedAcOn": "lblLockedAcOn",
                    "lblMaturityDate": "lblMaturityDate",
                    "lblNext": "lblNext",
                    "lblPayAmount": "lblPayAmount",
                    "lblPayAmountOpted": "lblPayAmountOpted",
                    "lblRecalulate": "lblRecalulate"
                },
                "width": "96%",
                "zIndex": 4
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoProducts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxNoProducts",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxNoProducts.setDefaultUnit(kony.flex.DP);
            var lblIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblIcon",
                "isVisible": true,
                "left": "48dp",
                "skin": "CopydefLabel0c779ee6578e943",
                "text": "n",
                "top": "15dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoProducts = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNoProducts",
                "isVisible": true,
                "left": "2dp",
                "skin": "CopydefLabel0cee1ae9496304d",
                "text": "There is no existing Deposit for this customer,\nClick on create new deposit to explore options",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoProducts.add(lblIcon, lblNoProducts);
            flxA.add(flxMessageInfo, flxHeaderMenu, flxCntnr, custInfo, ContractRequestDescription, MainTabs, DPWSelectLoan, flxLoanButtonWrapper, flxDepositButtonWrapper, flxNow, flxLoansHdrSeg, FlexContainer0gedc2f7c182c4c, SegOne, segMin2, segDeposits, flxNoProducts);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "82dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "82dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
                "1366": {
                    "btnLogin": {
                        "height": {
                            "type": "string",
                            "value": "40dp"
                        },
                        "skin": "CopybtnDefProfile2",
                        "segmentProps": []
                    }
                }
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0c70bfb5d2e724d",
                    "skin2": "CopydefBtnNormal0i897dea905994f",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ContractRequestDescription": {
                    "right": "",
                    "bottom": "",
                    "height": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "MainTabs": {
                    "height": "64dp",
                    "left": "0dp",
                    "maxHeight": "42dp",
                    "top": "0dp",
                    "zIndex": 2
                },
                "MainTabs.btnAccounts2": {
                    "left": "40dp"
                },
                "MainTabs.btnAccounts3": {
                    "left": "40dp"
                },
                "MainTabs.flxContainerAccount": {
                    "height": "2dp"
                },
                "MainTabs.flxContainerdeposits": {
                    "height": "2dp",
                    "left": "147dp"
                },
                "MainTabs.flxontainerLoans": {
                    "left": "267dp"
                },
                "ErrorAllert": {
                    "left": "82dp",
                    "width": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(uuxNavigationRail, flxA, ErrorAllert, ProgressIndicator, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerAccountsDetails,
            "enabledForIdleTimeout": false,
            "id": "frmCustomerAccountsDetails",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1277, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});