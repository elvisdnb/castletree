define("userfrmSearchCustomerController", {
    //Type your controller code here 
    getCustomerInfo: [],
    _navObj: "",
    previousForm: "",
    custphoneNumber: "",
    custEmail: "",
    custFirstName: "",
    custlastName: "",
    custdateOfBirth: "",
    custaccountOfficerName: "",
    onNavigate: function(navObj) {
        if (navObj) {
            this._navObj = navObj;
            this.previousForm = Application.validation.isNullUndefinedObj(this._navObj.previousform);
        }
        this.bindEvents();
    },
    bindEvents: function() {
        this.view.postShow = this.postShowFunctionCall;
        this.view.custCancel1.onclickEvent = this.clearFields;
        this.view.custProceed1.onclickEvent = this.getCustomerDetails;
        this.view.searchSegments.onRowClick = this.SegRowClick;
        this.view.btnNext.onTouchEnd = this.showChangeRequestListData;
        this.view.btnPrev.onTouchEnd = this.showChangeRequestListData;
    },
    postShowFunctionCall: function() {
        try {
            this.dataPerPageNumber = 8;
            this.view.ProgressIndicator.isVisible = false;
            //       this.view.inputCustomerId.value ="500104";
            //       this.view.inputFirstName.value ="Erik";
            //       this.view.inputDOB.value ="1983-10-04";
            //       this.view.inputEmail.value="erikvdl198@gmail.com";
            //       this.view.inputPhoneNumber.value="2088113408";
            //this.getCustomerDetails();
            this.view.custCustomerId.options = '[{"label": "Equal","value":"equal"},{"label": "Contains","value":"CT"},{"label": "BeginsWith","value":"BW"},{"label": "Like","value":"MT"}]';
            this.view.custCustomerId.value = "equal";
            this.view.custFirstName.options = '[{"label": "Equal","value":"equal"},{"label": "Contains","value":"CT"},{"label": "BeginsWith","value":"BW"},{"label": "Like","value":"MT"}]';
            this.view.custFirstName.value = "equal";
            this.view.custLastName.options = '[{"label": "Equal","value":"equal"},{"label": "Contains","value":"CT"},{"label": "BeginsWith","value":"BW"},{"label": "Like","value":"MT"}]';
            this.view.custLastName.value = "equal";
            this.view.custDOB.options = '[{"label": "Equal","value":"equal"},{"label": "Contains","value":"CT"},{"label": "BeginsWith","value":"BW"},{"label": "Like","value":"MT"}]';
            this.view.custDOB.value = "equal";
            this.view.custPhoneNumber.options = '[{"label": "Equal","value":"equal"},{"label": "Contains","value":"CT"},{"label": "BeginsWith","value":"BW"},{"label": "Like","value":"MT"}]';
            this.view.custPhoneNumber.value = "equal";
            this.view.custEmail.options = '[{"label": "Equal","value":"equal"},{"label": "Contains","value":"CT"},{"label": "BeginsWith","value":"BW"},{"label": "Like","value":"MT"}]';
            this.view.custEmail.value = "equal";
            this.view.custAccountOfficer.options = '[{"label": "Equal","value":"equal"},{"label": "Contains","value":"CT"},{"label": "BeginsWith","value":"BW"},{"label": "Like","value":"MT"}]';
            this.view.custAccountOfficer.value = "equal";
            this.view.inputAccountOfficer.options = '[{"label": "Please Select","value":"Select"},{"label": "Retail Banking Mgr","value":"2"}, {"label": "Retail Banking-3","value":"3"}, {"label": "Customer Service Agent","value":"26"}, {"label": "Retail Credit Officer","value":"28"}, {"label": "Retail Credit Manager","value":"29"}]';
            this.view.inputAccountOfficer.value = "Select";
            //this.view.inputDOB.placeholder ="YYYY-MM-DD";
            this.view.FlexContainer0e48343e0b1f644.height = "800dp";
            this.view.flxSearchWrap.height = "620dp";
            this.view.flxCustomerDetailsSearch.height = "650dp";
            this.view.lblSearchResults.isVisible = false;
            this.view.lblNoResults.isVisible = false;
            this.view.flxSegWrapper.isVisible = false;
            this.clearFields();
        } catch (err) {
            alert("Error in postFunctionCall:::" + err);
        }
    },
    validatefield: function(dropdownvalues, fieldvalues) {
        var Valdropdown = "";
        if (dropdownvalues === "equal") {
            Valdropdown = fieldvalues;
        } else {
            Valdropdown = fieldvalues + "" + dropdownvalues;
        }
        return Valdropdown;
    },
    changeDOBFormat: function(date) {
        var anyDate = new Date(date);
        var day = anyDate.getDate(date);
        var monthName = (anyDate.getMonth() + 1);
        monthName = (monthName <= 8) ? "0" + monthName : monthName;
        var year = anyDate.getFullYear();
        return "" + year + "-" + monthName + "-" + day;
    },
    getCustomerDetails: function() {
        this.view.ProgressIndicator.isVisible = true;
        var dropdownValue1 = this.view.custCustomerId.value;
        var dropdownValue2 = this.view.custFirstName.value;
        var dropdownValue3 = this.view.custLastName.value;
        var dropdownValue4 = this.view.custDOB.value;
        var dropdownValue5 = this.view.custPhoneNumber.value;
        var dropdownValue6 = this.view.custEmail.value;
        var dropdownValue7 = this.view.custAccountOfficer.value;
        var getcustomerID = this.view.inputCustomerId.value;
        var getFirstName = this.view.inputFirstName.value;
        var getLastName = this.view.inputLastName.value;
        var getDOB = this.view.inputDOB.value;
        if (getDOB !== "") {
            getDOB = this.changeDOBFormat(getDOB);
        }
        var getEmail = this.view.inputEmail.value;
        var getPhoneNumber = this.view.inputPhoneNumber.value;
        var getAccountOfficer = this.view.inputAccountOfficer.value;
        var serviceName = "LendingNew";
        var operationName = "searchCustomer";
        var headers = {
            "companyId": "NL0020001",
            "page_size": "500",
            "content-type": "application/json"
        };
        if (getcustomerID !== "") {
            var customerId = this.validatefield(dropdownValue1, "customerId");
            headers[customerId] = getcustomerID;
        }
        if (getFirstName !== "") {
            var firstName = this.validatefield(dropdownValue2, "customerName");
            headers[firstName] = getFirstName;
        }
        if (getLastName !== "") {
            var lastName = this.validatefield(dropdownValue3, "lastName");
            headers[lastName] = getLastName;
        }
        if (getDOB !== "") {
            var dateOfBirth = this.validatefield(dropdownValue4, "dateOfBirth");
            headers[dateOfBirth] = getDOB;
        }
        if (getPhoneNumber !== "") {
            var phoneNumber = this.validatefield(dropdownValue5, "phoneNumber");
            headers[phoneNumber] = getPhoneNumber;
        }
        if (getEmail !== "") {
            var email = this.validatefield(dropdownValue6, "email");
            headers[email] = getEmail;
        }
        if (getAccountOfficer !== "" && getAccountOfficer !== "Select") {
            var accountOfficerId = this.validatefield(dropdownValue7, "accountOfficerId");
            headers[accountOfficerId] = getAccountOfficer;
        }
        var inputParams = {};
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackbills, this.failureCallBackbills);
    },
    successCallBackbills: function(response) {
        if (response.body !== undefined) {
            var getCustomerInfoLength = response.body.length;
            //alert(getCustomerInfoLength);
            var length = getCustomerInfoLength.toString();
            //     if(length < 8){
            //       var height = 40;
            //       var headerHeight = 60;
            //       var overallheight = (length * height);
            //       this.view.searchSegments.height = overallheight+"dp";
            //       this.view.flxSegWrapper.height = (overallheight + headerHeight)+"dp";
            //       //this.view.flxSegmentHeaderInfo.width ="96%";
            //     }else{
            //       //this.view.flxSegmentHeaderInfo.width ="94%";
            //     }
            if (length === "0") {
                //alert("0");
                this.view.ProgressIndicator.isVisible = false;
                this.view.FlexContainer0e48343e0b1f644.height = "800dp";
                this.view.flxSearchWrap.height = "650dp";
                this.view.flxCustomerDetailsSearch.height = "590dp";
                this.view.lblSearchResults.isVisible = true;
                this.view.lblNoResults.isVisible = true;
                this.view.flxSegWrapper.isVisible = false;
            } else {
                this.view.lblSearchResults.isVisible = true;
                this.view.flxSegWrapper.isVisible = true;
                this.getCustomerInfo = [],
                    this.view.searchSegments.removeAll();
                for (i = 0; i < getCustomerInfoLength; i++) {
                    this.ValidationCheck(response);
                    this.getCustomerInfo.push({
                        CustomerID: response.body[i].customerId,
                        FirstName: this.customerName,
                        LastName: this.customerMnemonic,
                        DateofBirth: this.custdateOfBirth,
                        PhoneNumber: this.custphoneNumber,
                        Email: this.custEmail,
                        AccountOfficer: this.custaccountOfficerName
                    });
                }
                kony.print(this.getCustomerInfo);
                this.segCustomerDetails = this.getCustomerInfo;
                this.showChangeRequestListData("");
                this.view.FlexContainer0e48343e0b1f644.height = "1100dp";
                this.view.flxSearchWrap.height = "980dp";
                this.view.flxCustomerDetailsSearch.height = "920dp";
                this.view.lblNoResults.isVisible = false;
                this.view.flxSegWrapper.isVisible = true;
            }
        } else {
            this.view.ProgressIndicator.isVisible = false;
            this.view.FlexContainer0e48343e0b1f644.height = "800dp";
            this.view.flxSearchWrap.height = "650dp";
            this.view.flxCustomerDetailsSearch.height = "590dp";
            this.view.lblSearchResults.isVisible = true;
            this.view.lblNoResults.isVisible = true;
            this.view.flxSegWrapper.isVisible = false;
        }
    },
    failureCallBackbills: function(response) {
        this.view.ProgressIndicator.isVisible = false;
        //alert(JSON.stringify(response));
    },
    ValidationCheck: function(response) {
        if (response.body[i].contactDetails) {
            if (response.body[i].contactDetails[0].emails) {
                var emailArraylength = response.body[i].contactDetails[0].emails.length;
                if (emailArraylength === 0) {
                    this.custEmail = "";
                } else {
                    var custEMail = response.body[i].contactDetails[0].emails[0].email;
                    if (custEMail.length > 22) {
                        this.custEmail = (custEMail.substring(0, 22)) + "..";
                    } else {
                        this.custEmail = custEMail;
                    }
                }
            } else {
                this.custEmail = "";
            }
            if (response.body[i].contactDetails[0].phoneNumbers) {
                var phoneArraylength = response.body[i].contactDetails[0].phoneNumbers.length;
                if (phoneArraylength === 0) {
                    this.custphoneNumber = "";
                } else {
                    this.custphoneNumber = response.body[i].contactDetails[0].phoneNumbers[0].phoneNumber;
                }
            } else {
                this.custphoneNumber = "";
            }
        } else {
            this.custphoneNumber = "";
            this.custEmail = "";
        }
        if (response.body[i].customerName === undefined) {
            this.customerName = "";
        } else {
            var custFirstName = response.body[i].customerName;
            if (custFirstName.length > 27) {
                this.customerName = (custFirstName.substring(0, 27)) + "..";
            } else {
                this.customerName = custFirstName;
            }
        }
        if (response.body[i].customerMnemonic === undefined) {
            this.customerMnemonic = "";
        } else {
            this.customerMnemonic = response.body[i].customerMnemonic;
        }
        if (response.body[i].dateOfBirth === undefined) {
            this.custdateOfBirth = "";
        } else {
            this.custdateOfBirth = response.body[i].dateOfBirth;
            this.custdateOfBirth = this.toShortFormat(this.custdateOfBirth);
        }
        if (response.body[i].accountOfficerName === undefined) {
            this.custaccountOfficerName = "";
        } else {
            this.custaccountOfficerName = response.body[i].accountOfficerName;
        }
    },
    toShortFormat: function(date) {
        var anyDate = new Date(date);
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var day = anyDate.getDate(date);
        var monthIndex = anyDate.getMonth();
        var monthName = monthNames[monthIndex];
        var year = anyDate.getFullYear();
        return "" + day + " " + monthName + " " + year;
    },
    clearFields: function() {
        this.view.inputCustomerId.value = "";
        this.view.inputFirstName.value = "";
        this.view.inputLastName.value = "";
        this.view.inputDOB.value = "";
        this.view.inputEmail.value = "";
        this.view.inputPhoneNumber.value = "";
        this.view.inputAccountOfficer.value = "";
    },
    showPaginationSegmentData: function(dataPerPage, arrayOfData, btnName) {
        kony.print("showPaginationSegmentData===>");
        var noOfItemsPerPage = dataPerPage;
        var tmpData = [];
        try {
            var segVal = arrayOfData;
            var len = segVal.length;
            this.totalpage = parseInt(len / noOfItemsPerPage);
            var remainingItems = parseInt(len) % noOfItemsPerPage;
            this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
            if (this.totalpage > 1) {
                this.view.flxPagination.isVisible = true;
            } else {
                this.view.flxPagination.isVisible = false;
            }
            if (btnName === "btnPrev" && this.gblSegPageCount > 1) {
                this.gblSegPageCount--;
            } else if (btnName === "btnNext" && this.gblSegPageCount < this.totalpage) {
                this.gblSegPageCount++;
            } else {
                this.gblSegPageCount = 1;
            }
            this.view.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;
            if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;            
            } else if (this.gblSegPageCount === 1 && this.totalpage > 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(true);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnFocus";
                //this.view.lblSearchPage.isVisible = true;           
            } else if (this.gblSegPageCount === 1 && this.totalpage === 1) {
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.lblSearchPage.text = "";
            } else if (this.gblSegPageCount === this.totalpage && this.totalpage > 1) {
                this.view.btnPrev.setEnabled(true);
                this.view.btnNext.setEnabled(false);
                this.view.btnPrev.skin = "sknBtnFocus";
                this.view.btnNext.skin = "sknBtnDisable";
                //this.view.lblSearchPage.isVisible = true;            
            } else {
                this.view.btnPrev.skin = "sknBtnDisable";
                this.view.btnNext.skin = "sknBtnDisable";
                this.view.btnPrev.setEnabled(false);
                this.view.btnNext.setEnabled(false);
                this.view.lblSearchPage.text = "";
            }
            var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
            var total = this.gblSegPageCount * noOfItemsPerPage;
            for (var j = i; j < total; j++) {
                if (j < len) {
                    tmpData.push(segVal[j]);
                }
            }
            return tmpData;
        } catch (err) {
            kony.print("Error in next previous method " + err);
        }
    },
    showChangeRequestListData: function(buttonevent) {
        if (this.segCustomerDetails.length > 0) {
            var segmentData = this.showPaginationSegmentData(this.dataPerPageNumber, this.segCustomerDetails, buttonevent.id);
            var widgetData = {
                "lblCustomerID": "CustomerID",
                "lblFirstName": "FirstName",
                "lblLastName": "LastName",
                "lblDateOfBirth": "DateofBirth",
                "lblPhoneNumber": "PhoneNumber",
                "lblEmail": "Email",
                "lblAccountOfficer": "AccountOfficer"
            };
            this.view.searchSegments.rowTemplate = "flxSearchSegments";
            this.view.searchSegments.widgetDataMap = widgetData;
            this.view.searchSegments.setData(segmentData);
            this.view.forceLayout();
            this.view.ProgressIndicator.isVisible = false;
        }
        //dismissLoader();
    },
    SegRowClick: function() {
        var segreow = this.view.searchSegments.selectedRowItems[0];
        this.navigateToAccountFunc(segreow.CustomerID);
    },
    navigateToAccountFunc: function(customerId) {
        var navObjet = {};
        navObjet.customerId = customerId;
        if (this.previousForm === "createContractLog") {
            var navToContractPage = new kony.mvc.Navigation("createContractLog");
            navToContractPage.navigate(navObjet);
        } else {
            var navToAccountPage = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            navObjet.isShowLoans = "true";
            navToAccountPage.navigate(navObjet);
        }
        //navObjet.isShowLoans = "true";
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    }
});
define("frmSearchCustomerControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_TPW_f937666cd8aa4daebf54a21779a11059: function AS_TPW_f937666cd8aa4daebf54a21779a11059(eventobject) {
        var self = this;
        return
    },
    AS_UWI_f605ce00bb2c4bed9d800b1d625a8816: function AS_UWI_f605ce00bb2c4bed9d800b1d625a8816(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_g0e79ea551834e2d93d067e6cc4d196b: function AS_UWI_g0e79ea551834e2d93d067e6cc4d196b(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmSearchCustomerController", ["userfrmSearchCustomerController", "frmSearchCustomerControllerActions"], function() {
    var controller = require("userfrmSearchCustomerController");
    var controllerActions = ["frmSearchCustomerControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
