define("CustomerBranch", function() {
    return function(controller) {
        var CustomerBranch = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "CustomerBranch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        CustomerBranch.setDefaultUnit(kony.flex.DP);
        var flxBranchWrap = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxBranchWrap",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxBranchWrap.setDefaultUnit(kony.flex.DP);
        var lblCustomerID = new kony.ui.Label({
            "height": "100%",
            "id": "lblCustomerID",
            "isVisible": true,
            "left": "1%",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "101146",
            "top": "0",
            "width": "25.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFirstName = new kony.ui.Label({
            "height": "100%",
            "id": "lblFirstName",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Rolf Gerieng",
            "top": "0dp",
            "width": "33.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRm = new kony.ui.Label({
            "height": "100%",
            "id": "lblRm",
            "isVisible": true,
            "left": 0,
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Rolf Gerieng",
            "top": "0dp",
            "width": "30.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbllogout = new kony.ui.Label({
            "centerY": "50%",
            "id": "lbllogout",
            "isVisible": false,
            "left": "0",
            "onTouchEnd": controller.AS_Label_a033b0d563eb486e90a70bd7e742cb14,
            "skin": "CopydefLabel0ed14ffd1e15e4c",
            "text": "i",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnLogout = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0fc8f9fa705014a",
            "height": "30dp",
            "id": "btnLogout",
            "isVisible": true,
            "right": 0,
            "skin": "CopydefBtnNormal0fc8f9fa705014a",
            "text": "i",
            "top": "0dp",
            "width": "30dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxBranchWrap.add(lblCustomerID, lblFirstName, lblRm, lbllogout, btnLogout);
        CustomerBranch.add(flxBranchWrap);
        return CustomerBranch;
    }
})