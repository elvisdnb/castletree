define("flxLoan7daysMain", function() {
    return function(controller) {
        var flxLoan7daysMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxLoan7daysMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {
            "hoverSkin": "CopyslFbox0d6f2870369004e"
        });
        flxLoan7daysMain.setDefaultUnit(kony.flex.DP);
        var lblLoanCustID = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLoanCustID",
            "isVisible": true,
            "left": "1%",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "100336",
            "top": "0dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 2, 0],
            "paddingInPixel": false
        }, {});
        var lblLoanName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLoanName",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Rolf Gerling",
            "top": "0dp",
            "width": "14.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLoanAccountID = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLoanAccountID",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "51090891",
            "top": "0dp",
            "width": "13.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        var lblLoanProduct = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLoanProduct",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "Long term deposite",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLoanCurrency = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLoanCurrency",
            "isVisible": true,
            "left": "15dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "USD",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLoanAmt = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLoanAmt",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "100,000.00",
            "top": "0dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLoanDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLoanDate",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopydefLabel0i4e8e3ef9d424d",
            "text": "20 Mar 2021",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLoan7daysMain.add(lblLoanCustID, lblLoanName, lblLoanAccountID, lblLoanProduct, lblLoanCurrency, lblLoanAmt, lblLoanDate);
        return flxLoan7daysMain;
    }
})