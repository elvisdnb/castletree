define("userfrmDisburseController", {
    //Type your controller code here
    _loanObj: {},
    currentOverride: {},
    _creditCurrency: "",
    _arrangementId: "",
    _glblEffectiveDate: transactDate,
    popupSegData: gblAllaccountarray,
    onNavigate: function(navObj) {
        this.bindEvents();
        this.pauseNavigation();
        this._duplicateId = "";
        this._loanObj = {};
        this.setLoanHeaderDetails(navObj);
        this.view.custAviableCommit.currencyCode = this._loanObj.currencyId;
        this.view.custTotalLoanAmount.currencyCode = this._loanObj.currencyId;
        this.view.SegmentPopup.isVisible = false;
        this.view.ErrorAllert.isVisiblefalse = false;
        this.view.custDisburseAmount.value = "";
        //this.view.tbxPayOutAccount.text="";
        this.view.flxpayInOutPOP.isVisible = false;
        this.view.lblErrorMsg.isVisible = false;
        this.view.custDisbursementDate.value = this._glblEffectiveDate;
        if (navObj && navObj.lblArrangementId) {
            this._arrangementId = navObj.lblArrangementId; //"AA20079J0GJ0";
            this.fetchDisbursementHistory(this._arrangementId);
            this.getPayInDefaultAccountDetails(this._arrangementId);
        }
        // this.resumeNavigation();
        // kony.print("date:::::"+this._glbEffectiveDate);
        this.view.flxPayoutComp.segAccountList.removeAll();
    },
    bindEvents: function() {
        this.view.postShow = this.postShowFunctionCall;
        this.view.custDisburseAmount.onKeyUp = this.reformatOnValChange;
        this.view.custButtonConfirm.onclickEvent = this.onClickSubmitData;
        this.view.custButtonCancel.onclickEvent = this.onClickCancel;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.closeErrorAllert;
        this.view.flxpayOutSearch.onTouchEnd = this.popupAccountList;
        this.view.SegmentPopup.imgClose.onTouchEnd = this.closeSegmentOverrite;
        this.view.SegmentPopup.btnProceed.onClick = this.onClickSubmitData;
        this.view.SegmentPopup.btnCancel.onClick = this.closeSegmentOverrite;
        //this.view.tbxPayOutAccount.onTouchStart=this.acctflexPayout;
        this.view.floatCreditAccount.downArrowLbl.onTouchStart = this.acctflexPayout;
        this.view.flxPayoutComp.segAccountList.onRowClick = this.getAccountSeginfo;
        this.view.popupPayinPayout.txtSearchBox.onKeyUp = this.doCRSearchWithinSeg;
        this.view.popupPayinPayout.txtSearchBox.onDone = this.doCRSearchWithinSeg;
        this.view.flxpayInOutPOP.flxPayout.popupPayinPayout.segPayinPayout.onRowClick = this.getSegRowinfo;
        this.view.popupPayinPayout.imgClose.onTouchEnd = this.closePayAccountDetails;
        this.view.floatCreditAccount.txtFloatText.onTouchStart = this.validateFieldState;
        this.view.floatCreditAccount.txtFloatText.onTextChange = this.onFocusField;
        this.view.lblBreadBoardHomeValue.onTouchEnd = this.navigateHome;
        this.view.lblBreadBoardOverviewvalue.onTouchEnd = this.navigateOverview;
    },
    getCurrentTransactDate: function(transactDate) {
        try {
            var today = new Date(transactDate);
            var dd = today.getDate();
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var todayConverted = dd + " " + shortMonth + " " + yyyy;
            this.view.custDisbursementDate.value = todayConverted;
            this.view.custDisbursementDate.displayFormat = "dd MMM yyyy";
            this.view.custDisbursementDate.min = todayConverted;
        } catch (err) {
            kony.print("Error in CreateContractLog getCurrentDate:::" + err);
        }
    },
    postShowFunctionCall: function() {
        try {
            this.initializeCustomComponent();
            kony.print("today::::" + this._glblEffectiveDate);
            this.getCurrentTransactDate(this._glblEffectiveDate);
            //this.view.custDisbursementDate.value=this._glblEffectiveDate;
            kony.timer.schedule("timer4", this.accountDynamicLoad, 3, false);
            var creditAcct = this.view.floatCreditAccount.txtFloatText.text;
            if (creditAcct !== "") {
                this.view.floatCreditAccount.animateComponent();
            }
        } catch (err) {
            alert("Error in CustomerLoanInterestChangeController postFunctionCall:::" + err);
        }
    },
    accountDynamicLoad: function() {
        //kony.print("accountvalue"+JSON.stringify(gblCutacctArray));
        if (gblCutacctArray.length === 0) {
            this.view.flxPayoutComp.lblNoAccounts.isVisible = true;
            this.view.flxPayoutComp.segAccountList.isVisible = false;
        } else {
            this.view.flxPayoutComp.segAccountList.widgetDataMap = gblCutacctWidgetDate;
            this.view.flxPayoutComp.segAccountList.setData(gblCutacctArray);
            this.view.flxPayoutComp.lblNoAccounts.isVisible = false;
            this.view.flxPayoutComp.segAccountList.isVisible = true;
        }
    },
    setLoanHeaderDetails: function(loanSegRow) {
        this._loanObj.currencyId = loanSegRow.lblCCY;
        this._loanObj.arrangementId = loanSegRow.lblArrangementId;
        this._loanObj.customerId = loanSegRow.customerId;
        this._loanObj.produtId = loanSegRow.loanProduct;
        this._loanObj.loanAccountId = loanSegRow.lblHeaderAN;
        this.view.loanDetails.lblAccountNumberResponse.text = loanSegRow.lblHeaderAN;
        this.view.loanDetails.lblLoanTypeResponse.text = loanSegRow.lblHeaderAccountType; //"2345678";
        this.view.loanDetails.lblCurrencyValueResponse.text = loanSegRow.lblCCY;
        this.view.loanDetails.lblAmountValueResponse.text = loanSegRow.lblClearedBalance;
        this.view.loanDetails.lblStartDateResponse.text = loanSegRow.lblAvailableLimit;
        this.view.loanDetails.lblMaturityDateResponse.text = loanSegRow.lblHdr;
    },
    closeSegmentOverrite: function() {
        this.view.SegmentPopup.isVisible = false;
        this.currentOverride = {};
        this.view.forceLayout();
    },
    acctflexPayout: function() {
        if (this.view.flxPayoutComp.isVisible) {
            this.view.flxPayoutComp.isVisible = false;
        } else {
            this.view.flxPayoutComp.isVisible = true;
        }
    },
    getAccountSeginfo: function() {
        var getAcctId = this.view.flxPayoutComp.segAccountList.selectedRowItems[0].accountid;
        //this.view.tbxPayOutAccount.text=getAcctId;
        this.view.floatCreditAccount.txtFloatText.text = getAcctId;
        this.view.floatCreditAccount.animateComponent();
        this.view.flxPayoutComp.isVisible = false;
    },
    onClickCancel: function() {
        var navToHome = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navToHome.navigate(navObjet);
    },
    closeErrorAllert: function() {
        this.view.ErrorAllert.isVisible = false;
        this.view.flxHeaderMenu.isVisible = true;
    },
    closePayAccountDetails: function() {
        kony.print("enter:::::");
        this.view.flxpayInOutPOP.isVisible = false;
        this.view.custDisbursementDate.isVisible = true;
        this.view.iconPayoutsearch.isVisible = true;
        this.view.custTotalLoanAmount.isVisible = true;
        this.view.custAviableCommit.isVisible = true;
        this.view.custDisburseAmount.isVisible = true;
        //this.view.flxpayOutSearch.isVisible=true;
        //this.view.tbxPayOutAccount.isVisible=true;
    },
    initializeCustomComponent: function() {
        this.view.iconPayoutsearch.color = "temenos-light";
        //this.view.custAviableCommit.disabled=true;
        //this.view.custTotalLoanAmount.disabled=true;
    },
    popupAccountList: function() {
        this.accttype = "payout";
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.ProgressIndicator.isVisible = true;
        this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate;
        this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        this.view.flxpayInOutPOP.isVisible = true;
        this.view.ProgressIndicator.isVisible = false;
    },
    fetchDisbursementHistory: function(arrangementId) {
        kony.print("arrangementId::::" + arrangementId);
        var serviceName = "LendingNew";
        var operationName = "getDisbursement";
        var headers = "";
        var inputParams = {
            "arrangementId": arrangementId
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunction, this.failureCallBackfunction);
    },
    successCallBackfunction: function(res) {
        var totCommitment = res.body[0].balanceType;
        // var avlCommitment=res.body[1].balanceType;
        //for(var i = 0 ; i < res.body.length ; i++){
        //  balanceType=res.body[i].balanceType;     
        if (totCommitment === "Commitment (Total)") {
            this.view.custTotalLoanAmount.value = parseInt(res.body[0].balanceAmount.replace(/,/g, ''));
            // this.view.custTotalLoanAmount.decimals="2";
        }
        if (res.body[1] !== undefined) {
            this.view.custAviableCommit.value = parseInt(res.body[1].balanceAmount.replace(/,/g, ''));
            // this.view.custAviableCommit.decimals="2";
        } else {
            this.view.custAviableCommit.value = "0";
            // this.view.custAviableCommit.decimals="2";
        }
        // }
        kony.print("this.res.body=====>" + JSON.stringify(res.body));
        this.resumeNavigation();
    },
    failureCallBackfunction: function(res) {
        this.resumeNavigation();
        kony.print("Error in fetching total loan amount...." + JSON.stringify(res));
    },
    getSegRowinfo: function() {
        var getAcctId = this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].accountid;
        var curId = this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].currency;
        this._creditCurrency = curId;
        this.view.flxpayInOutPOP.isVisible = false;
        //this.view.tbxPayOutAccount.text=getAcctId; 
        this.view.floatCreditAccount.txtFloatText.text = getAcctId;
        this.view.floatCreditAccount.animateComponent();
        this.view.popupPayinPayout.txtSearchBox.text = "";
        this.view.popupPayinPayout.segPayinPayout.removeAll();
        this.view.popupPayinPayout.segPayinPayout.setData(this.popupSegData);
        // this.view.custPayIn.options = JSON.stringify(accountArray);
    },
    onClickSubmitData: function() {
        var disuburseAmount = this.view.custDisburseAmount.value;
        var creditAccountNumber = this.view.floatCreditAccount.txtFloatText.text;
        var isValidData = this.validateData();
        if (isValidData === 0) {
            if (disuburseAmount !== "" && creditAccountNumber !== "") {
                var currenecy = this._creditCurrency;
                this.view.lblErrorMsg.isVisible = false;
                this.view.SegmentPopup.isVisible = false;
                var disbursementDate = this.view.custDisbursementDate.value;
                var d = new Date(disbursementDate);
                var YYYY = d.getFullYear();
                var MM = ("0" + (d.getMonth() + 1)).slice(-2);
                var date = "" + d.getDate();
                date = date.length > 1 ? date : '0' + date;
                disbursementDate = YYYY + "-" + MM + "-" + date;
                var serviceName = "LendingNew";
                var operationName = "postLoanDisubursment";
                var headers = "";
                var inputParams = {};
                inputParams.debitAccountId = this._loanObj.loanAccountId;
                inputParams.debitCurrency = this._loanObj.currencyId;
                inputParams.debitValueDate = disbursementDate;
                inputParams.creditAccountId = creditAccountNumber;
                inputParams.paymentCurrencyId = currenecy;
                inputParams.amount = disuburseAmount;
                inputParams.executionDate = disbursementDate;
                inputParams.overrideDetails = this.currentOverride;
                MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunction90res, this.failureCallBackfunction90res);
            }
            this.view.ProgressIndicator.isVisible = true;
        } else {
            this.view.lblErrorMsg.isVisible = true;
            if (creditAccountNumber === "") {
                this.view.floatCreditAccount.validateEMptyFiled();
            }
        }
    },
    successCallBackfunction90res: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        this.currentOverride = {};
        var refId = res.header["id"];
        var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navObjet.messageInfo = "Disbursement completed successfully!   Reference :" + refId;
        navToChangeInterest1.navigate(navObjet);
    },
    failureCallBackfunction90res: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        kony.print("Error in post service for loan disbursement...." + JSON.stringify(res));
        if (res.errorDetails && res.errorDetails.length !== 0) {
            this.view.flxHeaderMenu.isVisible = false;
            this.view.ErrorAllert.isVisible = true;
            this.view.SegmentPopup.isVisible = false;
            kony.print("message::::" + JSON.stringify(res.errorDetails));
            kony.print("message::::" + JSON.stringify(res.errorDetails[0]));
            kony.print("message::::" + JSON.stringify(res.errorDetails[0].message));
            this.view.ErrorAllert.lblMessage.text = res.errorDetails[0].message;
        }
        if (res.override && !isNullorEmpty(res.override.overrideDetails)) {
            var overwriteDetails = [];
            var overWriteData = [];
            var segDataForOverrite = [];
            this.currentOverride = res.override.overrideDetails;
            overwriteDetails = res.override.overrideDetails;
            for (var a in overwriteDetails) {
                overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                if (overwriteDetails[a].id === "DUP.CONTRACT") {
                    // this._duplicateId=overwriteDetails[a].id;
                    //kony.print("Id:::::"+this._duplicateId);
                }
                var json = {
                    "lblSerial": "*",
                    "lblInfo": overwriteDetails[a].description
                };
                segDataForOverrite.push(json);
            }
            this.view.SegmentPopup.segOverride.widgetDataMap = {
                "lblSerial": "lblSerial",
                "lblInfo": "lblInfo"
            };
            this.view.SegmentPopup.lblPaymentInfo.text = "Override Confirmation";
            this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
            this.view.SegmentPopup.isVisible = true;
            kony.print("overWriteData::::" + JSON.stringify(overWriteData));
            kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
            this.view.forceLayout();
        }
    },
    getPayInDefaultAccountDetails: function(arrangementId) {
        try {
            var serviceName = "LendingNew";
            var operationName = "getPayInAccountDetails";
            var headers = {};
            var inputParams = {
                "arrangementID": arrangementId
            };
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgetPayInDefaultAccountDetails, this.errorCBgetPayInDefaultAccountDetails);
        } catch (err) {
            kony.print("Error in RepaymentController getPayInDefaultAccountDetails::" + err);
        }
    },
    successCBgetPayInDefaultAccountDetails: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print("success response:::" + JSON.stringify(res));
            //this.getAccountBasedonCustomer(); // calling other Account Info
            var responseData = res.body;
            if (responseData.length > 0) {
                var accountDetails = responseData[0].payoutAccount;
                kony.print("accountDetails:::" + JSON.stringify(accountDetails));
                //this.view.floatReadOnlylText.txtFloatText.text = accountDetails;
                this.view.floatCreditAccount.txtFloatText.text = accountDetails;
                this.view.floatCreditAccount.animateComponent();
                //this.view.forceLayout();
            }
        } catch (err) {
            kony.print("Error in RepaymentController successCBgetPayInDefaultAccountDetails::" + err);
        }
    },
    errorCBgetPayInDefaultAccountDetails: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            kony.print("error response:::" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in RepaymentController errorCBgetPayInDefaultAccountDetails::" + err);
        }
    },
    doCRSearchWithinSeg: function() {
        kony.print('edit');
        var stringForSearch = this.view.popupPayinPayout.txtSearchBox.text.toLowerCase();
        if (stringForSearch.length === 0) {
            //Reset to show the full claims search results global variable
            // disable the clear search x mark, if any
            this.view.popupPayinPayout.segPayinPayout.removeAll();
            this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
        } else {
            //empty search string now
            // visible on for the clear search x mark
        }
        if (stringForSearch.length >= 3) {
            //this.view.popupPayinPayout.segPayinPayout
            var segData = gblAllaccountarray; // make a copy not response
            var finalData = [];
            if (segData && segData === []) {
                return;
            }
            for (var i = 0; i < segData.length; i++) {
                var aRowData = segData[i];
                var aRowDataStrigified = JSON.stringify(aRowData);
                if (aRowDataStrigified.toLowerCase().indexOf(stringForSearch) >= 0) {
                    finalData.push(aRowData);
                } else {
                    //no need to push it - as does not meet criteria
                }
            }
            this.view.popupPayinPayout.segPayinPayout.removeAll();
            this.view.popupPayinPayout.segPayinPayout.setData(finalData);
        }
    },
    reformatOnValChange: function(event) {
        //already existing value in tex box
        var str = this.view.custDisburseAmount.value;
        if (str !== "" && !isNaN(str)) {
            //already a number is in the box
            if (event.keyCode === 84) {
                this.view.custDisburseAmount.value = str + "000";
            } else if (event.keyCode === 77) {
                this.view.custDisburseAmount.value = str + "000000";
            } else {
                //do not do anything
            }
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    validateData: function() {
        var Requiredflag = 0;
        var FormId = document.getElementsByTagName("form")[0].getAttribute("id");
        var form = document.getElementById(FormId);
        var Requiredfilter = form.querySelectorAll('*[required]');
        for (var i = 0; i < Requiredfilter.length; i++) {
            if (Requiredfilter[i].disabled !== true && Requiredfilter[i].value === '') {
                var ElementId = Requiredfilter[i].id;
                var Element = document.getElementById(ElementId);
                Element.reportValidity();
                Requiredflag = 1;
            }
        }
        return Requiredflag;
    },
    validateFieldState: function() {
        var creditAccount = this.view.floatCreditAccount.txtFloatText.text;
        if (creditAccount === "") {
            this.view.floatCreditAccount.onFocusValidateField();
        }
    },
    onFocusField: function() {
        this.view.floatCreditAccount.onFocusOutValidateField();
    },
    navigateHome: function() {
        var navToHome = new kony.mvc.Navigation("frmDashboard");
        var navObjet = {};
        navObjet.clientId = "";
        navToHome.navigate(navObjet);
    },
    navigateOverview: function() {
        var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this._loanObj.customerId;
        navObjet.isShowLoans = "true";
        navToChangeInterest1.navigate(navObjet);
    },
});
define("frmDisburseControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Image_ae9428a3f7d54919b7720ac15c9c7941: function AS_Image_ae9428a3f7d54919b7720ac15c9c7941(eventobject, x, y) {
        var self = this;
        return self.closePayAccountDetails.call(this);
    },
    AS_Segment_cd66c81de52e4de29e68381495b002db: function AS_Segment_cd66c81de52e4de29e68381495b002db(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.getSegRowinfo.call(this);
    },
    AS_UWI_b113ea7d342147d3aed08d12d072c086: function AS_UWI_b113ea7d342147d3aed08d12d072c086(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_b7da5a251fac4ab0a3f9940d5ff973fa: function AS_UWI_b7da5a251fac4ab0a3f9940d5ff973fa(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmDisburseController", ["userfrmDisburseController", "frmDisburseControllerActions"], function() {
    var controller = require("userfrmDisburseController");
    var controllerActions = ["frmDisburseControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
