define("frmRepayment", function() {
    return function(controller) {
        function addWidgetsfrmRepayment() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "pagingEnabled": false,
                "right": "24dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxContainerBGF0a6fcc72d729345",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "300dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.CustomWidget({
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "112dp",
                "skin": "CopysknLbl0edbf4e2f83c247",
                "text": "Overview",
                "top": "26dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardChangeInterest = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardChangeInterest",
                "isVisible": true,
                "left": "200dp",
                "skin": "sknLblLoanServiceBlacl12px",
                "text": "Repay",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadboardChangeInterestSplash = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadboardChangeInterestSplash",
                "isVisible": true,
                "left": "180dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "26dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue, lblBreadBoardChangeInterest, lblBreadboardChangeInterestSplash);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "70dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "98%",
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "width": "98%"
                    },
                    "imgCloseBackup": {
                        "left": "1230dp",
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRepaymentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": false,
                "height": "485dp",
                "id": "flxRepaymentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "right": "24dp",
                "skin": "CopysknFlxMultiBGCBE0i3354513ab0840",
                "top": "420dp",
                "width": "98%",
                "zIndex": 2
            }, {}, {});
            flxRepaymentWrapper.setDefaultUnit(kony.flex.DP);
            var lblLoanRepaymentInfo = new kony.ui.Label({
                "height": "20dp",
                "id": "lblLoanRepaymentInfo",
                "isVisible": false,
                "left": "10%",
                "skin": "sknLbl16pxBG003E75",
                "text": "Loan Repayment",
                "top": "29dp",
                "width": "124dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxInnerRepaymentWrapper2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxInnerRepaymentWrapper2",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxInnerRepaymentWrapper2.setDefaultUnit(kony.flex.DP);
            flxInnerRepaymentWrapper2.add();
            var flxInnerRepaymentWrapper1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxInnerRepaymentWrapper1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "67dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerRepaymentWrapper1.setDefaultUnit(kony.flex.DP);
            var cusTextPaymentDueDuplicate = new kony.ui.CustomWidget({
                "id": "cusTextPaymentDueDuplicate",
                "isVisible": false,
                "left": "10%",
                "top": "5dp",
                "width": "23%",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": false,
                "disabled": true,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "labelText": "Payment Due",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "readonly": false,
                "trailingButtonClick": "",
                "validateRequired": "",
                "validationMessage": "",
                "value": "1,184.44"
            });
            flxInnerRepaymentWrapper1.add(cusTextPaymentDueDuplicate);
            var flxInnerRepaymentWrapper3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxInnerRepaymentWrapper3",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerRepaymentWrapper3.setDefaultUnit(kony.flex.DP);
            flxInnerRepaymentWrapper3.add();
            var cusCalendarDueDate = new kony.ui.CustomWidget({
                "id": "cusCalendarDueDate",
                "isVisible": false,
                "left": "99dp",
                "top": "200dp",
                "width": "200dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": false,
                "disabled": true,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Payment Date",
                "max": "",
                "min": "1900-01-01",
                "onDone": "",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "17 Nov 2020",
                "weekLabel": "Wk"
            });
            var cusTextPrepaymentFee = new kony.ui.CustomWidget({
                "id": "cusTextPrepaymentFee",
                "isVisible": false,
                "left": "68%",
                "top": "210dp",
                "width": "23%",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": false,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "search",
                "labelText": "Prepayment Fee",
                "maxLength": "",
                "placeholder": "",
                "readonly": false,
                "showIconButtonTrailing": true,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": "0"
            });
            var flxLoanRepayment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxLoanRepayment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "1250dp",
                "zIndex": 1
            }, {}, {});
            flxLoanRepayment.setDefaultUnit(kony.flex.DP);
            var TitleLabel = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "TitleLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "TitleLabel": {
                        "centerX": "viz.val_cleared",
                        "height": "70dp",
                        "isVisible": true
                    },
                    "flxTitle": {
                        "isVisible": false
                    },
                    "lblTitle": {
                        "centerX": "viz.val_cleared",
                        "left": "150dp",
                        "text": "Loan Repayment"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var cusTextPaymentDue = new kony.ui.CustomWidget({
                "id": "cusTextPaymentDue",
                "isVisible": true,
                "left": "150dp",
                "top": 100,
                "width": "291dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 0,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Payment Due",
                "locale": "",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "suffix": "",
                "validateRequired": "",
                "value": ""
            });
            var flxViewBillDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxViewBillDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "486dp",
                "isModalContainer": false,
                "skin": "sknFlxOutstanding20pxRadius",
                "top": 100,
                "width": "291dp",
                "zIndex": 1
            }, {}, {});
            flxViewBillDetails.setDefaultUnit(kony.flex.DP);
            var lblTotalBillCountInfo = new kony.ui.Label({
                "height": "20dp",
                "id": "lblTotalBillCountInfo",
                "isVisible": true,
                "left": "8%",
                "skin": "sknBillDetailCountLabel",
                "text": "2",
                "top": "10dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewBillInfo = new kony.ui.Label({
                "height": "30dp",
                "id": "lblViewBillInfo",
                "isVisible": true,
                "left": "30%",
                "skin": "sknlblBG0075DBpx12",
                "text": "View Bills Outstanding",
                "top": "5dp",
                "width": "125dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBillIcon = new kony.ui.CustomWidget({
                "id": "cusBillIcon",
                "isVisible": true,
                "left": "85%",
                "top": "8dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "receipt",
                "size": "small"
            });
            flxViewBillDetails.add(lblTotalBillCountInfo, lblViewBillInfo, cusBillIcon);
            var cusListBoxPaymentMode = new kony.ui.CustomWidget({
                "id": "cusListBoxPaymentMode",
                "isVisible": true,
                "left": "822dp",
                "top": 100,
                "width": "291dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Payment Mode",
                "options": "",
                "readonly": true,
                "validateRequired": "",
                "value": ""
            });
            var cusTextPaymentAmount = new kony.ui.CustomWidget({
                "id": "cusTextPaymentAmount",
                "isVisible": true,
                "left": "150dp",
                "top": "180dp",
                "width": "291dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Payment Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "suffix": "",
                "validateRequired": "required",
                "value": ""
            });
            var flxDebitAccountTextBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxDebitAccountTextBox",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "486dp",
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "180dp",
                "width": "291dp",
                "zIndex": 1
            }, {}, {});
            flxDebitAccountTextBox.setDefaultUnit(kony.flex.DP);
            var cusTextDebitAccountNumber = new kony.ui.CustomWidget({
                "id": "cusTextDebitAccountNumber",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "260dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Debit Account Number",
                "maxLength": "",
                "placeholder": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            var flxDebitAccountInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDebitAccountInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf2f2f2BGBorder",
                "top": "0dp",
                "width": "31dp",
                "zIndex": 1
            }, {}, {});
            flxDebitAccountInnerWrapper.setDefaultUnit(kony.flex.DP);
            var floatReadOnlylText = new com.lending.floatingReadOnlyText.floatReadOnlylText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "100%",
                "id": "floatReadOnlylText",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxWhiteBg",
                "top": "0dp",
                "width": "100%",
                "zIndex": 4,
                "overrides": {
                    "floatReadOnlylText": {
                        "height": "100%",
                        "isVisible": false,
                        "width": "100%"
                    },
                    "lblFloatLabel": {
                        "text": "Debit Account"
                    },
                    "txtFloatText": {
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var floatLabelText = new com.lending.floatingTextBox.floatLabelText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "54dp",
                "id": "floatLabelText",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxWhiteBg",
                "top": "0dp",
                "width": "100%",
                "zIndex": 4,
                "overrides": {
                    "downArrowLbl": {
                        "centerY": "40%",
                        "isVisible": true,
                        "right": "5%",
                        "zIndex": 3
                    },
                    "floatLabelText": {
                        "height": "54dp",
                        "isVisible": false
                    },
                    "flxBgColor": {
                        "height": "100%"
                    },
                    "flxBottomLine": {
                        "top": "viz.val_cleared",
                        "zIndex": 5
                    },
                    "flxFloatLableGrp": {
                        "centerY": "50%",
                        "height": "56dp",
                        "top": "viz.val_cleared"
                    },
                    "lblFloatLabel": {
                        "centerY": "40%",
                        "text": "Debit Account Number",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "rtAsterix": {
                        "centerY": "40%",
                        "left": "0dp"
                    },
                    "txtFloatText": {
                        "height": "54dp",
                        "isVisible": true,
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var downArrowLbl = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "downArrowLbl",
                "isVisible": true,
                "left": "0dp",
                "right": "5%",
                "skin": "sknLblIcon12px",
                "text": "c",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDebitAccountInnerWrapper.add(floatReadOnlylText, floatLabelText, downArrowLbl);
            flxDebitAccountTextBox.add(cusTextDebitAccountNumber, flxDebitAccountInnerWrapper);
            var cusCalenderPaymentDate = new kony.ui.CustomWidget({
                "id": "cusCalenderPaymentDate",
                "isVisible": true,
                "left": "822dp",
                "top": "180dp",
                "width": "291dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Payment Date",
                "max": "",
                "min": "1900-01-01",
                "onDone": "",
                "showWeekNumber": false,
                "validateRequired": "",
                "value": "17 Nov 2020",
                "weekLabel": "Wk"
            });
            var cusTextPaymentAmount1 = new kony.ui.CustomWidget({
                "id": "cusTextPaymentAmount1",
                "isVisible": false,
                "left": "150dp",
                "top": "210dp",
                "width": "291dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Payment Amount",
                "maxLength": "",
                "placeholder": "",
                "readonly": false,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            var flxSubmit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSubmit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "874dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "400dp",
                "width": "220dp",
                "zIndex": 1
            }, {}, {});
            flxSubmit.setDefaultUnit(kony.flex.DP);
            var cusButtonConfirm = new kony.ui.CustomWidget({
                "id": "cusButtonConfirm",
                "isVisible": true,
                "left": "15dp",
                "top": "0dp",
                "width": "204dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": " Confirm      ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxSubmit.add(cusButtonConfirm);
            var lblErrorMsg = new kony.ui.Label({
                "height": "50dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "150dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "350dp",
                "width": "280dp",
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "650dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "400dp",
                "width": "216dp",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            var cusButtonCancel = new kony.ui.CustomWidget({
                "id": "cusButtonCancel",
                "isVisible": true,
                "left": "15dp",
                "top": "0dp",
                "width": "196dp",
                "height": "40dp",
                "minWidth": "200dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxCancel.add(cusButtonCancel);
            var flxWrapperPaymentApplicationPriority = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxWrapperPaymentApplicationPriority",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "290dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxWrapperPaymentApplicationPriority.setDefaultUnit(kony.flex.DP);
            var lblPaymentApplicationlabelInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblPaymentApplicationlabelInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl12px000000",
                "text": "Payment Application Priority",
                "top": "5dp",
                "width": "160dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPenaltyInterestlabelInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblPenaltyInterestlabelInfo",
                "isVisible": true,
                "left": "176dp",
                "skin": "sknLblBG757575px12",
                "text": "1. Penalty Interest",
                "top": "5dp",
                "width": "103dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPrincipalInterestLabelInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblPrincipalInterestLabelInfo",
                "isVisible": true,
                "left": "296dp",
                "skin": "sknLblBG757575px12",
                "text": "2. Principal Interest",
                "top": "5dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChargesLabelInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblChargesLabelInfo",
                "isVisible": true,
                "left": "423dp",
                "skin": "sknLblBG757575px12",
                "text": "3. Charges",
                "top": "5dp",
                "width": "61dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPrincipalDueLabelInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblPrincipalDueLabelInfo",
                "isVisible": true,
                "left": "176dp",
                "skin": "sknLblBG757575px12",
                "text": "4. Principal Due",
                "top": "25dp",
                "width": "103dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCreditBalanceLabelInfo = new kony.ui.Label({
                "height": "18dp",
                "id": "lblCreditBalanceLabelInfo",
                "isVisible": true,
                "left": "296dp",
                "skin": "sknLblBG757575px12",
                "text": "5. Unspecified Credit Balance",
                "top": "25dp",
                "width": "180dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWrapperPaymentApplicationPriority.add(lblPaymentApplicationlabelInfo, lblPenaltyInterestlabelInfo, lblPrincipalInterestLabelInfo, lblChargesLabelInfo, lblPrincipalDueLabelInfo, lblCreditBalanceLabelInfo);
            var flxDebitAccountSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "240dp",
                "id": "flxDebitAccountSegment",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "586dp",
                "isModalContainer": false,
                "skin": "sknflxWhiteBGBorder0075db",
                "top": "260dp",
                "width": "291dp",
                "zIndex": 5
            }, {}, {});
            flxDebitAccountSegment.setDefaultUnit(kony.flex.DP);
            var flxSegDebitAccountHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSegDebitAccountHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBGF2F7FDBorder",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegDebitAccountHeader.setDefaultUnit(kony.flex.DP);
            var lblAccountNumber = new kony.ui.Label({
                "height": "40dp",
                "id": "lblAccountNumber",
                "isVisible": true,
                "left": "2%",
                "skin": "sknlblBG0075DBpx14",
                "text": "Account Number",
                "top": "0dp",
                "width": "38%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAvaliableBalance = new kony.ui.Label({
                "height": "40dp",
                "id": "lblAvaliableBalance",
                "isVisible": true,
                "left": "5%",
                "skin": "sknlblBG0075DBpx14",
                "text": "Available Balance",
                "top": "0dp",
                "width": "45%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegDebitAccountHeader.add(lblAccountNumber, lblAvaliableBalance);
            var segDebitAccount = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }, {
                    "lblAccountNumber": "Label",
                    "lblAvaliableBalance": "Label"
                }],
                "groupCells": false,
                "height": "200dp",
                "id": "segDebitAccount",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxSegDebitAccount",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "40dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSegDebitAccount": "flxSegDebitAccount",
                    "lblAccountNumber": "lblAccountNumber",
                    "lblAvaliableBalance": "lblAvaliableBalance"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDebitAccountSegment.add(flxSegDebitAccountHeader, segDebitAccount);
            var AccountList = new com.konymp.AccountList.AccountList({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "240dp",
                "id": "AccountList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "486dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxWhiteBGBorder0075db",
                "top": "222dp",
                "width": "400dp",
                "zIndex": 10,
                "overrides": {
                    "AccountList": {
                        "isVisible": false,
                        "left": "486dp",
                        "top": "222dp",
                        "width": "400dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSeparatorLineGrey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLineGrey",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10%",
                "isModalContainer": false,
                "skin": "CopyslFbox0fda5754cd9f44b",
                "top": "70dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLineGrey.setDefaultUnit(kony.flex.DP);
            flxSeparatorLineGrey.add();
            flxLoanRepayment.add(TitleLabel, cusTextPaymentDue, flxViewBillDetails, cusListBoxPaymentMode, cusTextPaymentAmount, flxDebitAccountTextBox, cusCalenderPaymentDate, cusTextPaymentAmount1, flxSubmit, lblErrorMsg, flxCancel, flxWrapperPaymentApplicationPriority, flxDebitAccountSegment, AccountList, flxSeparatorLineGrey);
            flxRepaymentWrapper.add(lblLoanRepaymentInfo, flxInnerRepaymentWrapper2, flxInnerRepaymentWrapper1, flxInnerRepaymentWrapper3, cusCalendarDueDate, cusTextPrepaymentFee, flxLoanRepayment);
            var MainTabs = new com.lending.MainTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "227dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "MainTabs": {
                        "left": "0dp",
                        "top": "227dp",
                        "width": "100%"
                    },
                    "btnAccounts1": {
                        "left": 40
                    },
                    "flxContainerAccount": {
                        "left": "27dp"
                    },
                    "flxontainerLoans": {
                        "left": "247dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var loanDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "loanDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "291dp",
                "width": "98%",
                "overrides": {
                    "FlexContainer0ebe191bc1cf946": {
                        "width": "270dp"
                    },
                    "flxListBoxSelectedValue": {
                        "left": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "flxLoanDetailsvalue": {
                        "height": "63dp"
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "left": "58dp",
                        "src": "drop_down_arrow.png"
                    },
                    "lblAccountNumberInfo": {
                        "width": "12.50%"
                    },
                    "lblAccountNumberResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblAccountServiceInfo": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "12.50%"
                    },
                    "lblAmountValueInfo": {
                        "width": "12.50%"
                    },
                    "lblAmountValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblCurrencyValueInfo": {
                        "width": "12.50%"
                    },
                    "lblCurrencyValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Repay"
                    },
                    "lblLoanTypeInfo": {
                        "width": "13.20%"
                    },
                    "lblLoanTypeResponse": {
                        "centerY": "50%",
                        "left": "15dp",
                        "width": "13.60%"
                    },
                    "lblMaturityDateInfo": {
                        "width": "12.50%"
                    },
                    "lblMaturityDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblStartDateInfo": {
                        "width": "12.50%"
                    },
                    "lblStartDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "loanDetails": {
                        "height": "128dp",
                        "left": "18dp",
                        "top": "291dp",
                        "width": "98%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContainer.add(flxHeaderMenu, custInfo, ErrorAllert, flxRepaymentWrapper, MainTabs, loanDetails);
            var flxOutstandingRepaymentOutsideWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "968dp",
                "id": "flxOutstandingRepaymentOutsideWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxOutstandingRepaymentOutsideWrapper.setDefaultUnit(kony.flex.DP);
            var flxOutstandingRepaymentInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOutstandingRepaymentInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "15%",
                "isModalContainer": false,
                "skin": "sknFlxOutstanding6pxRadius",
                "top": "320dp",
                "width": "72%",
                "zIndex": 1
            }, {}, {});
            flxOutstandingRepaymentInnerWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblHeaderInfo = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblHeaderInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl24px005096BG",
                "text": "Outstanding Repayments",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusIconPrinter = new kony.ui.CustomWidget({
                "id": "cusIconPrinter",
                "isVisible": true,
                "right": "80dp",
                "top": "2dp",
                "width": "26dp",
                "height": "26dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "print",
                "size": "small"
            });
            var flxHeaderSegmentOutstandingClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeaderSegmentOutstandingClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "19dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderSegmentOutstandingClose.setDefaultUnit(kony.flex.DP);
            var cusIconClose = new kony.ui.CustomWidget({
                "id": "cusIconClose",
                "isVisible": false,
                "left": "0dp",
                "top": "2dp",
                "width": "100%",
                "height": "26dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "highlight_off",
                "size": "small"
            });
            var imgIconClose1 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgIconClose1",
                "isVisible": false,
                "left": "0dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgIconClose = new kony.ui.Button({
                "height": "40dp",
                "id": "imgIconClose",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknBtnImgClose",
                "text": "L",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSegmentOutstandingClose.add(cusIconClose, imgIconClose1, imgIconClose);
            flxHeaderInfo.add(lblHeaderInfo, cusIconPrinter, flxHeaderSegmentOutstandingClose);
            var flxSegmentHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSegmentHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSegmentHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblBillIdInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblBillIdInfo",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Bill ID",
                "top": "2dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDueDateInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblDueDateInfo",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Due Date",
                "top": "2dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTypeInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblTypeInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Type",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilledInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblBilledInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Billed",
                "top": "2dp",
                "width": "12%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOutstandingInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblOutstandingInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Outstanding",
                "top": "2dp",
                "width": "12%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatusInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblStatusInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Status",
                "top": "2dp",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentHeaderInfo.add(lblBillIdInfo, lblDueDateInfo, lblTypeInfo, lblBilledInfo, lblOutstandingInfo, lblStatusInfo);
            var flxOutstandingRepaymentSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOutstandingRepaymentSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "8dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxOutstandingRepaymentSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var segOutstandingBill = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblBillIdInfo": "Bill ID",
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBillIdInfo": "Bill ID",
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBillIdInfo": "Bill ID",
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBillIdInfo": "Bill ID",
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }, {
                    "lblBillIdInfo": "Bill ID",
                    "lblBilledInfo": "Billed",
                    "lblDueDateInfo": "Due Date",
                    "lblOutstandingInfo": "Outstanding",
                    "lblStatusInfo": "Status",
                    "lblTypeInfo": "Type"
                }],
                "groupCells": false,
                "id": "segOutstandingBill",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxOutstandingBillDetails",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOutstandingBillDetails": "flxOutstandingBillDetails",
                    "lblBillIdInfo": "lblBillIdInfo",
                    "lblBilledInfo": "lblBilledInfo",
                    "lblDueDateInfo": "lblDueDateInfo",
                    "lblOutstandingInfo": "lblOutstandingInfo",
                    "lblStatusInfo": "lblStatusInfo",
                    "lblTypeInfo": "lblTypeInfo"
                },
                "width": "100%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOutstandingRepaymentSegmentWrapper.add(segOutstandingBill);
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "34dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var lblPaginationInfo = new kony.ui.Label({
                "height": "40dp",
                "id": "lblPaginationInfo",
                "isVisible": false,
                "right": "270dp",
                "skin": "sknlbl14PX434343BG",
                "text": "Rows per page :",
                "top": "0dp",
                "width": "103dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtDataPerPageNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "txtDataPerPageNumber",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "placeholder": "Placeholder",
                "right": "220dp",
                "secureTextEntry": false,
                "skin": "sknTxt14px434343BGffffff",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var lblSearchPage1 = new kony.ui.Label({
                "height": "40dp",
                "id": "lblSearchPage1",
                "isVisible": false,
                "right": "80dp",
                "skin": "sknlbl14PX434343BG",
                "text": "1-2 of 2",
                "top": "0dp",
                "width": "70dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPrev1 = new kony.ui.Button({
                "height": "40dp",
                "id": "btnPrev1",
                "isVisible": false,
                "right": "40dp",
                "skin": "sknBtnDisable",
                "text": "<",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext1 = new kony.ui.Button({
                "height": "40dp",
                "id": "btnNext1",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknBtnFocus",
                "text": ">",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPaginationNetSettlementAmountInfo = new kony.ui.Label({
                "height": "40dp",
                "id": "flxPaginationNetSettlementAmountInfo",
                "isVisible": true,
                "left": "0%",
                "skin": "sknLblBg434343px14RobotoMedium",
                "text": "Net Settlement :",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPaginationNetSettlementAmountResponse = new kony.ui.Label({
                "height": "40dp",
                "id": "flxPaginationNetSettlementAmountResponse",
                "isVisible": true,
                "left": "121dp",
                "skin": "sknLbl14px005096BG",
                "text": "1,125.58",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext = new kony.ui.Button({
                "height": "40dp",
                "id": "btnNext",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknBtnFocus",
                "text": "N",
                "top": "0dp",
                "width": 25,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPrev = new kony.ui.Button({
                "height": "40dp",
                "id": "btnPrev",
                "isVisible": true,
                "right": "40dp",
                "skin": "sknBtnDisable",
                "text": "O",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPage = new kony.ui.Label({
                "centerY": "50%",
                "height": "40dp",
                "id": "lblSearchPage",
                "isVisible": true,
                "right": "80dp",
                "skin": "CopydefLabel0b044b7d672a44a",
                "text": "1-6 of 18",
                "top": "10dp",
                "width": "70dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPagination.add(lblPaginationInfo, txtDataPerPageNumber, lblSearchPage1, btnPrev1, btnNext1, flxPaginationNetSettlementAmountInfo, flxPaginationNetSettlementAmountResponse, btnNext, btnPrev, lblSearchPage);
            var flxNoOutstandingBillDetailsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoOutstandingBillDetailsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxNoOutstandingBillDetailsWrapper.setDefaultUnit(kony.flex.DP);
            var lblNoOutstandingBillDetails = new kony.ui.Label({
                "height": "80dp",
                "id": "lblNoOutstandingBillDetails",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblBG4D4D4Dpx24",
                "text": "No Bills outstanding",
                "top": "9dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoOutstandingBillDetailsWrapper.add(lblNoOutstandingBillDetails);
            var flxDummyBelowOutstandingRepay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDummyBelowOutstandingRepay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDummyBelowOutstandingRepay.setDefaultUnit(kony.flex.DP);
            flxDummyBelowOutstandingRepay.add();
            flxOutstandingRepaymentInnerWrapper.add(flxHeaderInfo, flxSegmentHeaderInfo, flxOutstandingRepaymentSegmentWrapper, flxPagination, flxNoOutstandingBillDetailsWrapper, flxDummyBelowOutstandingRepay);
            flxOutstandingRepaymentOutsideWrapper.add(flxOutstandingRepaymentInnerWrapper);
            var flxGenericBillDetailsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "968dp",
                "id": "flxGenericBillDetailsWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBlocked16OP",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxGenericBillDetailsWrapper.setDefaultUnit(kony.flex.DP);
            var flxBillDetailsInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBillDetailsInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "30%",
                "isModalContainer": false,
                "skin": "sknFlxWhiteBg",
                "top": "270dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsInnerWrapper.setDefaultUnit(kony.flex.DP);
            var flxBillDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxBillDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsHeader.setDefaultUnit(kony.flex.DP);
            var flxNavigationBillDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNavigationBillDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {}, {});
            flxNavigationBillDetailsHeader.setDefaultUnit(kony.flex.DP);
            var cusIconHeaderBack = new kony.ui.Label({
                "height": "14dp",
                "id": "cusIconHeaderBack",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLblIcon12px",
                "text": "O",
                "top": "13dp",
                "width": "14dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAllOutstandingBillNavigationInfo = new kony.ui.Label({
                "height": "100%",
                "id": "lblAllOutstandingBillNavigationInfo",
                "isVisible": true,
                "left": "27dp",
                "skin": "sknLbl16px005096BGUnderline",
                "text": "All Outstanding Bills",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNavigationBillDetailsHeader.add(cusIconHeaderBack, lblAllOutstandingBillNavigationInfo);
            var lblColonInfo = new kony.ui.Label({
                "height": "100%",
                "id": "lblColonInfo",
                "isVisible": true,
                "left": "1dp",
                "skin": "sknLbl24px005096BG",
                "text": "/",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeaderBillNumberResponse = new kony.ui.Label({
                "height": "100%",
                "id": "lblHeaderBillNumberResponse",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlbl16PX434343BG",
                "text": "Bill 1241552",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBillDetailsClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxBillDetailsClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsClose.setDefaultUnit(kony.flex.DP);
            var iconCloaseButtonBillDetails = new kony.ui.CustomWidget({
                "id": "iconCloaseButtonBillDetails",
                "isVisible": false,
                "left": "0dp",
                "top": "8dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "highlight_off",
                "size": "small"
            });
            var imgCloseButtonBillDetails1 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgCloseButtonBillDetails1",
                "isVisible": false,
                "left": "0dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCloseButtonBillDetails = new kony.ui.Button({
                "height": "40dp",
                "id": "imgCloseButtonBillDetails",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknBtnImgClose",
                "text": "L",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBillDetailsClose.add(iconCloaseButtonBillDetails, imgCloseButtonBillDetails1, imgCloseButtonBillDetails);
            flxBillDetailsHeader.add(flxNavigationBillDetailsHeader, lblColonInfo, lblHeaderBillNumberResponse, flxBillDetailsClose);
            var flxInnerBillDetailsInfoInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "370dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper.setDefaultUnit(kony.flex.DP);
            var flxInnerBillDetailsInfoInnerWrapper01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper01",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper01.setDefaultUnit(kony.flex.DP);
            var lblPrincipalInfo = new kony.ui.Label({
                "height": "25dp",
                "id": "lblPrincipalInfo",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLblBg000000px14",
                "text": "Principal",
                "top": "0dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrencyinfo = new kony.ui.Label({
                "height": "25dp",
                "id": "lblCurrencyinfo",
                "isVisible": true,
                "left": "30%",
                "skin": "sknLbl14px005096BG",
                "text": "EUR",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPrincipalResponse = new kony.ui.Label({
                "height": "25dp",
                "id": "lblPrincipalResponse",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLbl14px005096BG",
                "text": "1,125.58",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInnerBillDetailsInfoInnerWrapper01.add(lblPrincipalInfo, lblCurrencyinfo, lblPrincipalResponse);
            var flxInnerBillDetailsInfoInnerWrapper02 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper02",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper02.setDefaultUnit(kony.flex.DP);
            var lblPrincipalInterestInfo = new kony.ui.Label({
                "height": "25dp",
                "id": "lblPrincipalInterestInfo",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLblBg000000px14",
                "text": "Principal Interest",
                "top": "0dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrencyinfo02 = new kony.ui.Label({
                "height": "25dp",
                "id": "lblCurrencyinfo02",
                "isVisible": true,
                "left": "19%",
                "skin": "sknLbl14px005096BG",
                "text": "EUR",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPrincipalInterestResponse = new kony.ui.Label({
                "height": "25dp",
                "id": "lblPrincipalInterestResponse",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLbl14px005096BG",
                "text": "1,125.58",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInnerBillDetailsInfoInnerWrapper02.add(lblPrincipalInterestInfo, lblCurrencyinfo02, lblPrincipalInterestResponse);
            var flxInnerBillDetailsInfoInnerWrapper03 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper03",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper03.setDefaultUnit(kony.flex.DP);
            var lblPenaltyInterestInfo = new kony.ui.Label({
                "height": "25dp",
                "id": "lblPenaltyInterestInfo",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLblBg000000px14",
                "text": "Penalty Interest",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrencyinfo03 = new kony.ui.Label({
                "height": "25dp",
                "id": "lblCurrencyinfo03",
                "isVisible": true,
                "left": "21%",
                "skin": "sknLbl14px005096BG",
                "text": "EUR",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPenaltyInterestResponse = new kony.ui.Label({
                "height": "25dp",
                "id": "lblPenaltyInterestResponse",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLbl14px005096BG",
                "text": "1,125.58",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInnerBillDetailsInfoInnerWrapper03.add(lblPenaltyInterestInfo, lblCurrencyinfo03, lblPenaltyInterestResponse);
            var flxInnerBillDetailsInfoInnerWrapper04 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper04",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper04.setDefaultUnit(kony.flex.DP);
            var lblChargeInfo = new kony.ui.Label({
                "height": "25dp",
                "id": "lblChargeInfo",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLblBg000000px14",
                "text": "Charge",
                "top": "0dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrencyinfo04 = new kony.ui.Label({
                "height": "25dp",
                "id": "lblCurrencyinfo04",
                "isVisible": true,
                "left": "30%",
                "skin": "sknLbl14px005096BG",
                "text": "EUR",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChargeResponse = new kony.ui.Label({
                "height": "25dp",
                "id": "lblChargeResponse",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLbl14px005096BG",
                "text": "1,125.58",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInnerBillDetailsInfoInnerWrapper04.add(lblChargeInfo, lblCurrencyinfo04, lblChargeResponse);
            flxInnerBillDetailsInfoInnerWrapper.add(flxInnerBillDetailsInfoInnerWrapper01, flxInnerBillDetailsInfoInnerWrapper02, flxInnerBillDetailsInfoInnerWrapper03, flxInnerBillDetailsInfoInnerWrapper04);
            var flxBillDetailSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxBillDetailSegment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "5%",
                "isModalContainer": false,
                "skin": "sknflxTopBorder979797",
                "top": "10dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxBillDetailSegment.setDefaultUnit(kony.flex.DP);
            var segParticularBillDetail = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAmount": "Label",
                    "lblCurrency": "Label",
                    "lblTypeInfo": "Label"
                }, {
                    "lblAmount": "Label",
                    "lblCurrency": "Label",
                    "lblTypeInfo": "Label"
                }, {
                    "lblAmount": "Label",
                    "lblCurrency": "Label",
                    "lblTypeInfo": "Label"
                }, {
                    "lblAmount": "Label",
                    "lblCurrency": "Label",
                    "lblTypeInfo": "Label"
                }],
                "groupCells": false,
                "id": "segParticularBillDetail",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxSegParticularBillDetails",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "10dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSegParticularBillDetails": "flxSegParticularBillDetails",
                    "lblAmount": "lblAmount",
                    "lblCurrency": "lblCurrency",
                    "lblTypeInfo": "lblTypeInfo"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxInnerBillDetailsInfoInnerWrapper05 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper05",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": false,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBGE5F6FF",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper05.setDefaultUnit(kony.flex.DP);
            var lblNetSettlementInfo = new kony.ui.Label({
                "height": "50dp",
                "id": "lblNetSettlementInfo",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLblBg000000px14",
                "text": "Net Settlement",
                "top": "0dp",
                "width": "28%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBillDetailsReciptIcon = new kony.ui.CustomWidget({
                "id": "cusBillDetailsReciptIcon",
                "isVisible": false,
                "left": "8dp",
                "top": "11dp",
                "width": "25dp",
                "height": "25dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "receipt",
                "size": "small"
            });
            var lblReceiptionAlternative = new kony.ui.Label({
                "height": "24dp",
                "id": "lblReceiptionAlternative",
                "isVisible": true,
                "left": "8dp",
                "skin": "sknLblIconReceipt",
                "text": "K",
                "top": "13dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrencyinfo05 = new kony.ui.Label({
                "height": "50dp",
                "id": "lblCurrencyinfo05",
                "isVisible": true,
                "left": "28%",
                "skin": "sknLbl14px005096BG",
                "text": "EUR",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNetSettlementResponse = new kony.ui.Label({
                "height": "50dp",
                "id": "lblNetSettlementResponse",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLbl14px005096BG",
                "text": "1,5.58",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            flxInnerBillDetailsInfoInnerWrapper05.add(lblNetSettlementInfo, cusBillDetailsReciptIcon, lblReceiptionAlternative, lblCurrencyinfo05, lblNetSettlementResponse);
            var flxInnerBillDetailsInfoInnerWrapper06 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper06",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper06.setDefaultUnit(kony.flex.DP);
            var lblIssuesDateInfo = new kony.ui.Label({
                "height": "25dp",
                "id": "lblIssuesDateInfo",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLblBg000000px14",
                "text": "Issued Date",
                "top": "0dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIssuesDateResponse = new kony.ui.Label({
                "height": "25dp",
                "id": "lblIssuesDateResponse",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl14px005096BG",
                "text": "13 Oct 2020",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInnerBillDetailsInfoInnerWrapper06.add(lblIssuesDateInfo, lblIssuesDateResponse);
            var flxInnerBillDetailsInfoInnerWrapper07 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper07",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper07.setDefaultUnit(kony.flex.DP);
            var lblDueDateBillInfo = new kony.ui.Label({
                "height": "25dp",
                "id": "lblDueDateBillInfo",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLblBg000000px14",
                "text": "Due Date",
                "top": "0dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDueDateBillResponse = new kony.ui.Label({
                "height": "25dp",
                "id": "lblDueDateBillResponse",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl14px005096BG",
                "text": "14 Oct 2020",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInnerBillDetailsInfoInnerWrapper07.add(lblDueDateBillInfo, lblDueDateBillResponse);
            var flxInnerBillDetailsInfoInnerWrapper08 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInnerBillDetailsInfoInnerWrapper08",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerBillDetailsInfoInnerWrapper08.setDefaultUnit(kony.flex.DP);
            var lblAgeingInfo = new kony.ui.Label({
                "height": "25dp",
                "id": "lblAgeingInfo",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLblBg000000px14",
                "text": "Ageing",
                "top": "0dp",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAgeingResponse = new kony.ui.Label({
                "height": "25dp",
                "id": "lblAgeingResponse",
                "isVisible": true,
                "right": "0%",
                "skin": "sknLbl14px005096BG",
                "text": "Grace - 3 days",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInnerBillDetailsInfoInnerWrapper08.add(lblAgeingInfo, lblAgeingResponse);
            flxBillDetailSegment.add(segParticularBillDetail, flxInnerBillDetailsInfoInnerWrapper05, flxInnerBillDetailsInfoInnerWrapper06, flxInnerBillDetailsInfoInnerWrapper07, flxInnerBillDetailsInfoInnerWrapper08);
            flxBillDetailsInnerWrapper.add(flxBillDetailsHeader, flxInnerBillDetailsInfoInnerWrapper, flxBillDetailSegment);
            flxGenericBillDetailsWrapper.add(flxBillDetailsInnerWrapper);
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "968dp",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopup": {
                        "isVisible": false
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var Popup = new com.konymp.flxoverridepopup.Popup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "986dp",
                "id": "Popup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "Popup": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0c696e058632d4d",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0bc689671019341"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0g2d9727a6d884c"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_ic0b5f44bf3a4729b845e7850b39b467;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_gf34c04ce94246f3854345b0e71f3975;
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ErrorAllert": {
                    "width": "98%"
                },
                "ErrorAllert.imgCloseBackup": {
                    "left": "1230dp",
                    "src": "ico_close.png"
                },
                "TitleLabel": {
                    "centerX": "",
                    "height": "70dp"
                },
                "TitleLabel.lblTitle": {
                    "centerX": "",
                    "left": "150dp",
                    "text": "Loan Repayment"
                },
                "floatReadOnlylText": {
                    "height": "100%",
                    "width": "100%"
                },
                "floatReadOnlylText.lblFloatLabel": {
                    "text": "Debit Account"
                },
                "floatLabelText.downArrowLbl": {
                    "centerY": "40%",
                    "right": "5%",
                    "zIndex": 3
                },
                "floatLabelText": {
                    "height": "54dp"
                },
                "floatLabelText.flxBgColor": {
                    "height": "100%"
                },
                "floatLabelText.flxBottomLine": {
                    "top": "",
                    "zIndex": 5
                },
                "floatLabelText.flxFloatLableGrp": {
                    "centerY": "50%",
                    "height": "56dp",
                    "top": ""
                },
                "floatLabelText.lblFloatLabel": {
                    "centerY": "40%",
                    "text": "Debit Account Number",
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "floatLabelText.rtAsterix": {
                    "centerY": "40%",
                    "left": "0dp"
                },
                "floatLabelText.txtFloatText": {
                    "height": "54dp",
                    "width": "100%"
                },
                "AccountList": {
                    "left": "486dp",
                    "top": "222dp",
                    "width": "400dp"
                },
                "MainTabs": {
                    "left": "0dp",
                    "top": "227dp",
                    "width": "100%"
                },
                "MainTabs.btnAccounts1": {
                    "left": 40
                },
                "MainTabs.flxContainerAccount": {
                    "left": "27dp"
                },
                "MainTabs.flxontainerLoans": {
                    "left": "247dp"
                },
                "loanDetails.FlexContainer0ebe191bc1cf946": {
                    "width": "270dp"
                },
                "loanDetails.flxListBoxSelectedValue": {
                    "left": "",
                    "minWidth": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.flxLoanDetailsvalue": {
                    "height": "63dp"
                },
                "loanDetails.imgListBoxSelectedDropDown": {
                    "left": "58dp",
                    "src": "drop_down_arrow.png"
                },
                "loanDetails.lblAccountNumberInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAccountNumberResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountServiceInfo": {
                    "left": "",
                    "right": "20dp",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblListBoxSelectedValue": {
                    "text": "Repay"
                },
                "loanDetails.lblLoanTypeInfo": {
                    "width": "13.20%"
                },
                "loanDetails.lblLoanTypeResponse": {
                    "centerY": "50%",
                    "left": "15dp",
                    "width": "13.60%"
                },
                "loanDetails.lblMaturityDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails": {
                    "height": "128dp",
                    "left": "18dp",
                    "top": "291dp",
                    "width": "98%"
                },
                "SegmentPopup.imgClose": {
                    "src": "ico_close.png"
                },
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0bc689671019341",
                    "skin2": "CopydefBtnNormal0g2d9727a6d884c",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMainContainer, flxOutstandingRepaymentOutsideWrapper, flxGenericBillDetailsWrapper, SegmentPopup, Popup, uuxNavigationRail, ProgressIndicator, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmRepayment,
            "enabledForIdleTimeout": false,
            "id": "frmRepayment",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});