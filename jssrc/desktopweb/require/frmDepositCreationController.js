define("userfrmDepositCreationController", {
    //   BeneficiaryName:'',
    //   addRowArray:[],
    //   idtocreate:'',
    //   selectedLoanType:'',
    //   popupSegData:gblAllaccountarray,
    //   accttype:'',
    //   acctflex:'',
    //   productLoanId:'' ,
    //   currencyval:'',
    //   dataPerPageNumber : 0,
    //   partyIds:'',
    //   custName:'',
    //   Apidate:transactDate,
    //   loadbasic:0,
    //   loanTypeName:'',
    //   accountFlag:0,
    gblCid: null,
    companyId: "",
    depositTerm: null,
    scheduleData: [],
    rolesData: [],
    custInfo: {},
    onNavigate: function(navigationObj) {
        if (navigationObj !== null && navigationObj !== undefined) {
            navigationObj.customerId = navigationObj.customerId ? navigationObj.customerId : "500003";
        } else {
            navigationObj = {
                "customerId": "500003"
            };
        }
        if (navigationObj) {
            if (this.gblCid != navigationObj.customerId) {
                this.view.custInfo.restCustomerValues();
                this.gblCid = navigationObj.customerId;
                this.getCustomerDetails();
            }
            this.companyId = navigationObj.companyId;
        }
        this.depositTerm = navigationObj.selectedValue || "SHORT_TERM";
        this.view.preShow = this.frmPreShowFunction;
        this.view.postShow = this.frmPostShowFunction;
    },
    getCustomerDetails: function() {
        var self = this;
        this.custInfo = {};
        var serviceName = "LendingNew";
        // Get an instance of SDK
        var client = kony.sdk.getCurrentInstance();
        var integrationSvc = client.getIntegrationService(serviceName);
        var operationName = "getCustomers";
        var params = {
            "customerId": self.gblCid
        };
        var headersParam = {
            "customerId": self.gblCid
        };
        var options = {
            "httpRequestOptions": {
                "timeoutIntervalForRequest": 60,
                "timeoutIntervalForResource": 600
            }
        };
        integrationSvc.invokeOperation(operationName, headersParam, params, this.getCustomerDetailsSCB, this.getCustomerDetailsFCB, options);
    },
    getCustomerDetailsSCB: function(response) {
        this.view.custInfo.setCustomerValues(response);
        this.custInfo = response;
        this.view.TPWBenName.value = this.custInfo.customerName;
    },
    getCustomerDetailsFCB: function(error) {
        kony.print("Integration Service Failure:" + JSON.stringify(error));
    },
    frmPreShowFunction: function() {
        this.loadProductCurrency();
        this.loadCustomerRoles();
        this.view.custProceed.onclickEvent = this.onProceedClick.bind(this);
        this.view.custCancel.onclickEvent = this.onClickCancel.bind(this);
        this.view.custSubmit.onclickEvent = this.onSubmitClick.bind(this);
        this.view.flxSeePayment.onTouchEnd = this.showSchedule.bind(this);
        this.view.btnRowAdd.onclickEvent = this.addCustomerRow.bind(this);
        this.view.segAddRow.setVisibility(false);
        var widgetData = {
            "lblRole": "lblRoles",
            "lblRole1": "lblRoles1",
            "lbxBenRole": "lbxBenRole",
            "lblDeleteIcon": "lblDeleteIcon",
            "lblDelete": "lblDelete",
            "flxDelete": "flxDelete",
        };
        this.view.segAddRow.widgetDataMap = widgetData;
        this.view.segAddRow.setData([]);
        this.initializeChangeInterest();
    },
    loadProductCurrency: function() {
        var serviceName = "LendingNew";
        var operationName = "getProductCurrency";
        var headers = "";
        var productId = "";
        if (this.depositTerm === "SHORT_TERM") productId = "DEPOSIT.SHORT";
        else productId = "DEPOSIT.LONG";
        var inputParams = {
            "productId": productId
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackfunc1, this.failureCallBackfunc1);
        this.view.ProgressIndicator.isVisible = true;
    },
    successCallBackfunc1: function(response) {
        //kony.print("response from post increase commitment"+JSON.stringify(res));
        var currencyResponse = response.ccySpecific.length;
        //alert(JSON.stringify(currencyArray));
        var currencyArray = [{
            "label": "Select",
            "value": "none"
        }];
        for (i = 0; i < currencyResponse; i++) {
            currencyArray.push({
                label: response.ccySpecific[i].currency,
                value: response.ccySpecific[i].currency
            });
        }
        this.view.custCurrency.options = JSON.stringify(currencyArray);
        this.view.ProgressIndicator.isVisible = false;
    },
    failureCallBackfunc1: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        //alert("Error in Fetching getProductCurrency "+JSON.stringify(res));
        if (res.error && res.error.length !== 0) {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = res.error.message;
        }
    },
    loadCustomerRoles: function() {
        var serviceName = "DepositCreation";
        var operationName = "getCustomerRoles";
        data = {};
        var companyId = this.companyId ? this.companyId : "NL0020001";
        headers = {
            "companyId": companyId
        };
        var inputParams = {};
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.loadCustomerRolesSC, this.loadCustomerRolesFC);
    },
    loadCustomerRolesSC: function(response) {
        var roles = [];
        if (response && response.body) {
            response.body = response.body.filter(function(row) {
                return row.customerRole !== "OWNER";
            });
            roles = response.body.map(function(row) {
                return [row.customerRole, row.description];
            });
        }
        roles.unshift(["none", "Select"]);
        this.rolesData = roles;
    },
    loadCustomerRolesFC: function(res) {
        var roles = [];
        roles.unshift(["none", "Select"]);
        this.rolesData = roles;
        //alert("Error in Fetching getProductCurrency "+JSON.stringify(res));
        if (res.error && res.error.length !== 0) {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = res.error.message;
        }
    },
    frmPostShowFunction: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        this.view.frmDepositSpecifiesTab.setVisibility(false);
        this.view.custSubmit.setVisibility(false);
        this.view.flxBasicDetails.setVisibility(true);
        this.view.custProceed.setVisibility(true);
        this.view.flxViewSchedule.setVisibility(false);
        this.view.segRevised.setData([]);
        this.view.custDepositType.value = this.depositTerm === "SHORT_TERM" ? "Short Term" : "Long Term";
        this.view.flxStepClick.skin = "skinBlue";
        this.view.flxStepClick2.skin = "stepUnActive";
        this.hardCodeData();
        this.view.lblCurrencyErr.setVisibility(false);
        this.view.lblDepositAmountErr.setVisibility(false);
        this.view.lblDepositTermErr.setVisibility(false);
        this.view.lblErrorMsg.setVisibility(false);
        this.view.lblProceedErr.setVisibility(false);
        this.view.lblSubmitErr.setVisibility(false);
        this.view.TPWBenName.value = this.custInfo.customerName;
        this.view.custCurrency.value = "none";
        this.view.custDepositAmount.value = "";
        this.view.custDepositTerm.value = "";
        this.view.forceLayout();
    },
    initializeChangeInterest: function() {
        try {
            this.view.cusRadioChangeInterestType.options = [{
                "label": "No",
                "value": 0
            }, {
                "label": "Yes",
                "value": 1
            }, ];
            this.view.cusRadioChangeInterestType.value = 0;
            this.view.cusRadioChangeInterestType.onRadioChange = this.enableDisableInterestFreq;
            this.view.ErrorAllert.setVisibility(false);
        } catch (err) {
            kony.print("Exception in initializeChangeInterest: " + err);
        }
    },
    enableDisableInterestFreq: function(param) {
        if (param === 1) {
            this.view.custInterestFrequency.setVisibility(false);
            this.view.lblInterestPayoutErr.setVisibility(false);
        } else {
            this.view.custInterestFrequency.setVisibility(true);
        }
        this.view.forceLayout();
    },
    showSchedule: function() {
        var self = this;
        this.setScheduleData();
        this.view.flxViewSchedule.setVisibility(true);
        this.view.lblClose.onTouchEnd = function() {
            self.view.flxViewSchedule.setVisibility(false);
        };
        this.view.lblPrint.onTouchEnd = function() {
            window.print();
        }
        this.view.imgPrint.onTouchEnd = function() {
            window.print();
        }
        this.view.imgClose1.onTouchEnd = function() {
            self.view.flxViewSchedule.setVisibility(false);
        };
    },
    addCustomerRow: function() {
        var self = this;
        //     var segData = this.view.segAddRow.data;
        var newRow = {
            "lblRoles": "",
            "lblRoles1": "",
            "lbxBenRole": {
                masterData: this.rolesData,
                selectedKey: "none"
            },
            "flxDelete": {
                isVisible: true,
                onClick: function(row) {
                    self.view.segAddRow.removeAt(self.view.segAddRow.selectedRowIndex[1]);
                    self.view.forceLayout();
                }
            },
            "lblDeleteIcon": "Q",
            "lblDelete": "Delete",
        };
        //     segData.push(newRow);
        this.view.segAddRow.addAll([newRow]);
        //     this.view.segAddRow.setData(segData);
        this.view.segAddRow.setVisibility(true);
        this.view.forceLayout();
    },
    onProceedClick: function() {
        if (this.view.custCurrency.value === "none" || this.view.custDepositAmount.value === "" || this.view.custDepositTerm.value === "") {
            this.view.lblCurrencyErr.setVisibility(this.view.custCurrency.value === "none");
            this.view.lblDepositAmountErr.setVisibility(this.view.custDepositAmount.value === "");
            this.view.lblDepositTermErr.setVisibility(this.view.custDepositTerm.value === "");
            this.view.lblProceedErr.setVisibility(true);
            this.view.lblSubmitErr.setVisibility(false);
            return;
        }
        this.view.lblCurrencyErr.setVisibility(false);
        this.view.lblDepositAmountErr.setVisibility(false);
        this.view.lblDepositTermErr.setVisibility(false);
        this.view.lblProceedErr.setVisibility(false);
        this.view.lblSubmitErr.setVisibility(false);
        //Make service call now..    
        this.makeBasicDetailsServiceCall();
    },
    makeBasicDetailsServiceCall: function() {
        //Make the service call here..
        //--
        this.view.ProgressIndicator.isVisible = true;
        this.makeBasicDetailsServiceCallSC(); //Temp code..
    },
    makeBasicDetailsServiceCallSC: function(response) {
        //Write API returned data logic here..
        //--
        this.view.flxBasicDetails.setVisibility(false);
        this.view.frmDepositSpecifiesTab.setVisibility(true);
        this.view.custProceed.setVisibility(false);
        this.view.custSubmit.setVisibility(true);
        this.view.custDepositAmount.setVisibility(true);
        this.view.flxStepClick.skin = "skinGreen";
        this.view.flxStepClick2.skin = "skinBlue";
        this.view.lblInterestPayoutErr.setVisibility(false);
        this.view.lblPayoutAcctErr.setVisibility(false);
        this.view.lblPayinAcctErr.setVisibility(false);
        this.view.tbxPayOutAccount.txtFloatText.text = "";
        this.view.tbxPayInAccount.txtFloatText.text = "";
        this.view.cusRadioChangeInterestType.value = 0;
        this.enableDisableInterestFreq();
        this.view.custInterestFrequency.value = "";
        this.view.ProgressIndicator.isVisible = false;
    },
    makeBasicDetailsServiceCallFC: function(res) {
        this.view.ProgressIndicator.isVisible = false;
        if (res.error && res.error.length !== 0) {
            this.view.ErrorAllert.isVisible = true;
            this.view.ErrorAllert.lblMessage.text = res.error.message;
        }
    },
    setScheduleData: function() {
        var self = this;
        this.view.segRevised.widgetDataMap = {
            "lblPaymentDate": "date",
            "lblAmount": "amount",
            "lblPrincipal": "principal",
            "lblInterest": "interest",
            "lblTax": "tax",
            "lbltotaloutstanding": "outstanding"
        };
        var scheduleData = this.scheduleData;
        var scheduleLength = this.scheduleData.length;
        var start = 1,
            end = 0; //scheduleLength>=6 ? 6 : scheduleLength;
        function nextSixRows() {
            start = end + 1;
            end = (end + 6) <= scheduleLength ? end + 6 : scheduleLength;
            var segData = scheduleData.slice(start - 1, end);
            self.view.segRevised.setData(segData);
            self.view.lblSearchPage.text = start + "-" + end + " of " + scheduleLength;
        }

        function prevSixRows() {
            start = start - 6;
            end = (start + 5) <= scheduleLength ? start + 5 : scheduleLength;
            var segData = scheduleData.slice(start - 1, end);
            self.view.segRevised.setData(segData);
            self.view.lblSearchPage.text = start + "-" + end + " of " + scheduleLength;
        }
        nextSixRows();
        this.view.btnPrev.onTouchEnd = function() {
            if (start > 1) prevSixRows();
        };
        this.view.btnNext.onTouchEnd = function() {
            if (end < scheduleLength) nextSixRows();
        };
    },
    onSubmitClick: function() {
        if (this.view.tbxPayOutAccount.txtFloatText.text === "" || this.view.tbxPayInAccount.txtFloatText.text === "" || (this.view.cusRadioChangeInterestType.value === 0 && this.view.custInterestFrequency.value === "")) {
            this.view.lblPayoutAcctErr.setVisibility(this.view.tbxPayOutAccount.txtFloatText.text === "");
            this.view.lblPayinAcctErr.setVisibility(this.view.tbxPayInAccount.txtFloatText.text === "");
            this.view.lblInterestPayoutErr.setVisibility((this.view.cusRadioChangeInterestType.value === 0 && this.view.custInterestFrequency.value === ""));
            this.view.lblProceedErr.setVisibility(false);
            this.view.lblSubmitErr.setVisibility(true);
            return;
        }
        this.view.lblProceedErr.setVisibility(false);
        this.view.lblSubmitErr.setVisibility(false);
        this.view.lblInterestPayoutErr.setVisibility(false);
        this.view.lblPayoutAcctErr.setVisibility(false);
        this.view.lblPayinAcctErr.setVisibility(false);
        this.view.forceLayout();
        //Make final Service call now..
        alert("API call pending.."); //Temp code..
    },
    onClickCancel: function() {
        var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = this.gblCid;
        navObjet.isShowDeposits = "true";
        navObjet.showMessageInfo = false;
        navObjet.messageInfo = "";
        navToChangeInterest1.navigate(navObjet);
    },
    hardCodeData: function() {
        var data = [];
        data.push({
            "date": "01 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "1700"
        });
        data.push({
            "date": "02 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "1600"
        });
        data.push({
            "date": "03 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "1500"
        });
        data.push({
            "date": "04 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "1400"
        });
        data.push({
            "date": "05 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "1300"
        });
        data.push({
            "date": "06 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "1200"
        });
        data.push({
            "date": "07 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "1100"
        });
        data.push({
            "date": "08 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "1000"
        });
        data.push({
            "date": "09 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "900"
        });
        data.push({
            "date": "10 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "800"
        });
        data.push({
            "date": "11 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "700"
        });
        data.push({
            "date": "12 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "600"
        });
        data.push({
            "date": "13 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "500"
        });
        data.push({
            "date": "14 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "400"
        });
        data.push({
            "date": "15 Jan 2021",
            "amount": "100",
            "principal": "90",
            "interest": "9",
            "tax": "1",
            "outstanding": "300"
        });
        this.scheduleData = data;
    },
    //   postShowFunctionCall : function() { 
    //     this.dataPerPageNumber = 6;
    //     this.loadBeneficiary();   
    //     this.loadProductCurrency();   
    //     this.onLoadloanSpecifies();
    //     this.onLoadBasicDetails();
    //     this.view.custLoanType.value =  this.loanTypeName;
    //     this.view.custCurrency.value ="none";
    //     this.view.TPWBenName.value="";
    //     //this.loadPayAccounts();
    //     // this.loadAllPayAccounts();
    //     this.view.ErrorAllert.isVisible=false;
    //     this.view.flxStepClick.skin ="skinBlue";
    //     this.view.flxStepClick2.skin ="stepUnActive";   
    //      kony.timer.schedule("timer4",this.accountDynamicLoad, 3, false);
    //   },
    //   accountDynamicLoad : function(){
    //     this.view.AccountList.segAccountList.widgetDataMap = gblCutacctWidgetDate; 
    //     this.view.AccountList.segAccountList.setData(gblCutacctArray);
    //     this.view.AccountList2.segAccountList.widgetDataMap = gblCutacctWidgetDate; 
    //     this.view.AccountList2.segAccountList.setData(gblCutacctArray); 
    //   },
    //   navtoAccountLsit:function(){
    //     if(this.loadbasic===0){
    //       var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
    //       var navObjet = {};
    //       navObjet.customerId = this.partyIds;
    //       navObjet.isShowLoans = "true";
    //       navObjet.messageInfo = "";
    //       navToChangeInterest1.navigate(navObjet); 
    //     }else{	
    //       this.onLoadBasicDetails();
    //     }
    //   },
    //   onLoadBasicDetails : function(){
    //     if(this.selectedLoanType === "PERSONAL.LOAN.STANDARD"){
    //       this.loanTypeName =  "Personal Loan"; 
    //       this.productName= "personalLoans";
    //     }else{
    //       this.loanTypeName =  "Mortgage Loan"; 
    //       this.productName= "mortgages";
    //     }
    //     this.loadbasic=0;
    //     //this.view.custLoanType.value =  this.selectedLoanType;
    //     this.view.custLoanType.disabled=true;
    //     this.view.frmLoanSpecifiesTab.isVisible=false;
    //     this.view.TPWBenName.disabled=true;
    //     this.view.TPWBenName.iconButtonIcon="search";
    //     this.view.TPWBenName.showIconButtonTrailing=true;
    //     this.view.TPWBenRole.disabled=true;
    //     this.view.TPWBenRole.iconButtonIcon="search";
    //     this.view.TPWBenRole.showIconButtonTrailing=true;
    //     this.view.TPWFlag1.color="temenos-lightest";
    //     this.view.TPWFlag2.color="temenos-primary";
    //     this.view.TPWArrow.color ="temenos-primary";
    //     this.view.iconPayoutsearch.color="temenos-light";
    //     this.view.iconPayinsearch.color="temenos-light";
    //     this.view.custSubmit.isVisible=false;
    //     this.view.flxSeePayment.isVisible=false;
    //     this.view.custCurrency.isVisible=true;
    //     this.view.custPayout.isVisible=false;
    //     this.view.custPayIn.isVisible=false;
    //     this.view.flxBasicDetails.isVisible=true;
    //     this.view.custProceed.isVisible=true;   
    //     this.view.flxPayinComp.isVisible=false;
    //     this.view.flxPayoutComp.isVisible=false;
    //   },
    //   onLoadloanSpecifies: function(){
    //     //this.view.custLoanTerm.options = '[{"label": "Select","value":"None"},{"label": "1Y","value":"1Y"},{"label": "2Y","value":"2Y"}]';
    //     // this.view.custRepaymentFrequency.options = '[{"label": "Select","value":"None"},{"label": "e0Y e1M e0W e0D e0F","value":"e0Y e1M e0W e0D e0F"}]';
    //     this.view.custPrincipalInterest.disabled=true;
    //     this.view.custPenaltyInterest.disabled=true;
    //     this.view.custMatDate.disabled=true;
    //     //this.view.custRepaymentFrequency.value="";
    //     // this.view.custLoanAmount.iconButtonIcon="attach_money";
    //     //this.view.custLoanAmount.showIconButtonTrailing=true;
    //   },
    //   payInOutPopclose:function(){
    //     this.view.flxpayInOutPOP.isVisible=false;
    //     this.view.popupPayinPayout.txtSearchBox.text="";
    //     this.view.popupPayinPayout.segPayinPayout.removeAll();
    //     this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate; 
    //     this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    //   },
    //   paymentScheduleclose:function(){
    //     this.view.flxPaymentSchedule.isVisible=false;
    //   },
    //   acctflexPayout:function(){  
    //     this.acctflex="payout";
    //     if(this.view.flxPayoutComp.isVisible){
    //       this.view.flxPayoutComp.isVisible=false;
    //     } else {
    //       this.view.flxPayoutComp.isVisible=true;
    //     }
    //     //this.view.flxClickEnable.left="-5%";
    //   },
    //   acctflexPayout1:function(){
    //     this.acctflex="payin";
    //     if(this.view.flxPayinComp.isVisible){
    //       this.view.flxPayinComp.isVisible = false;
    //     } else {
    //       this.view.flxPayinComp.isVisible = true;
    //     }
    //     //this.view.flxClickEnable.left="-5%";
    //   },
    //   popupAccountList:function(){
    //     this.view.flxPayinComp.isVisible=false;   
    //     this.view.flxPayoutComp.isVisible=false; 
    //     this.accttype="payout"; 
    //     this.view.popupPayinPayout.txtSearchBox.text="";
    //     this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate; 
    //     this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    //     this.view.ProgressIndicator.isVisible=true;
    //     this.view.flxpayInOutPOP.isVisible=true;
    //     this.view.ProgressIndicator.isVisible=false;
    //   },
    //   popupAccountList1:function(){
    //     this.view.flxPayinComp.isVisible=false;   
    //     this.view.flxPayoutComp.isVisible=false; 
    //     this.accttype="payin";   
    //     this.view.popupPayinPayout.txtSearchBox.text="";
    //     this.view.ProgressIndicator.isVisible=true;
    //     this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate; 
    //     this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    //     this.view.flxpayInOutPOP.isVisible=true;
    //     this.view.ProgressIndicator.isVisible=false;
    //   },
    //   getCurrencyValues: function(){
    //     this.currencyval=this.view.custCurrency.value;
    //     this.view.lblErrorMsg.isVisible=false;
    //   },
    //   onSubmitClick : function(){
    //     if(this.view.custLoanAmount.value === "" || this.view.custRepaymentFrequency.value ==="" || (this.view.custRepaymentFrequency.value).toLowerCase() ==="none" || this.view.tbxPayOutAccount.txtFloatText.text ==="" || this.view.tbxPayOutAccount.txtFloatText.text ==="none" || this.view.tbxPayInAccount.txtFloatText.text ==="" || this.view.tbxPayInAccount.txtFloatText.text ==="none"){
    //       this.view.lblErrorMsg.isVisible=true;
    //     }else{
    //       this.view.lblErrorMsg.isVisible=false;
    //       var partyRole = this.view.TPWBenRole.value;
    //       //  var loanType =  this.view.custLoanType.value;
    //       var loanAmount = this.view.custLoanAmount.value;
    //       var loanTerm = this.view.custLoanTerm.value;
    //       var loanMaturityDate = this.view.custMaturityDate.value;
    //       var loanRepayFrequency = this.view.custRepaymentFrequency.value;
    //       var custPayout = this.view.tbxPayOutAccount.txtFloatText.text;
    //       var custPayIn = this.view.tbxPayInAccount.txtFloatText.text;
    //       //var loanPrincipalInterest = this.view.custPrincipalInterest.value;
    //       //var loanPenaltyInterest = this.view.custPenaltyInterest.value;
    //       var serviceName="LendingNew";
    //       var operationName;
    //       if(this.productLoanId ==="PERSONAL.LOAN.STANDARD"){
    //         operationName="postNewPersonalLoan";
    //       }else{
    //         operationName="postNewMortgageLoan";
    //       }
    //       var headers="";
    //       var inputParams={};
    //       inputParams.partyId=this.partyIds;
    //       inputParams.partyRole="OWNER";
    //       inputParams.currency=this.currencyval;
    //       inputParams.arrangementEffectiveDate=this.Apidate.replace( /-/g, "" );
    //       inputParams.amount=loanAmount;
    //       inputParams.term=loanTerm;
    //       inputParams.paymentFrequency=loanRepayFrequency;
    //       inputParams.payinAccount=custPayIn;
    //       inputParams.payoutAccount=custPayout;
    //       kony.print(JSON.stringify(inputParams));
    //       MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams,this.successCallBackloan,this.failureCallBackloan);
    //       this.view.ProgressIndicator.isVisible=true;
    //     }
    //   },
    //   successCallBackloan : function(response) {
    //     this.view.ProgressIndicator.isVisible=false;
    //     var loanArrangementId =  (response.body.arrangementActivity.arrangementId);
    //     //this.view.flxMessageInfo.isVisible = true;
    //     var navToLoanPage = new kony.mvc.Navigation("frmCustomerAccountsDetails");
    //     var navObjet = {};
    //     navObjet.arrangementID = loanArrangementId;
    //     navObjet.customerId =  this.partyIds;
    //     navObjet.isShowLoans = "true";
    //     navObjet.messageInfo =  "New loan account " + loanArrangementId + " created for "+this.custName;
    //     navToLoanPage.navigate(navObjet); 
    //   },
    //   failureCallBackloan : function(response){
    //     this.view.ProgressIndicator.isVisible=false;
    //     if(response.error.errorDetails && response.error.errorDetails.length !== 0 ){
    //       this.view.ErrorAllert.isVisible=true;
    //       this.view.ErrorAllert.lblMessage.text=response.error.errorDetails[0].message;
    //     }else{
    //       this.view.ErrorAllert.isVisible=true;
    //       this.view.ErrorAllert.lblMessage.text=response.errmsg;
    //     }
    //   },
    //   onCloseErrorAllert : function(){
    //     this.view.ErrorAllert.isVisible=false;
    //   },
    //   PaymentSchedule:function(){    
    //     if(this.view.custLoanAmount.value === "" || this.view.custRepaymentFrequency.value ==="" || (this.view.custRepaymentFrequency.value).toLowerCase() ==="none" || this.view.tbxPayOutAccount.txtFloatText.text ==="" || this.view.tbxPayOutAccount.txtFloatText.text ==="none" || this.view.tbxPayInAccount.txtFloatText.text ==="" || this.view.tbxPayInAccount.txtFloatText.text ==="none"){
    //       this.view.lblErrorMsg.isVisible=true;
    //     }else{
    //       this.view.lblErrorMsg.isVisible=false;
    //       var partyRole = this.view.TPWBenRole.value;
    //       var loanAmount = this.view.custLoanAmount.value;
    //       var loanTerm = this.view.custLoanTerm.value;
    //       var loanMaturityDate = this.view.custMaturityDate.value;
    //       var loanRepayFrequency = this.view.custRepaymentFrequency.value;
    //       var custPayout = this.view.tbxPayOutAccount.txtFloatText.text;
    //       var custPayIn = this.view.tbxPayInAccount.txtFloatText.text; 
    //       var serviceName="LendingNew";
    //       var operationName;
    //       if(this.productLoanId ==="PERSONAL.LOAN.STANDARD"){
    //         operationName="postPersonalSimulation";
    //       }else{
    //         operationName="postMortgageSimulation";
    //       }
    //       var headers="";
    //       var inputParams={};
    //       inputParams.partyId=this.partyIds;
    //       inputParams.partyRole="OWNER";
    //       inputParams.currency=this.currencyval;
    //       inputParams.arrangementEffectiveDate=this.Apidate.replace( /-/g, "" );
    //       inputParams.amount=loanAmount;
    //       inputParams.term=loanTerm;
    //       inputParams.paymentFrequency=loanRepayFrequency;
    //       inputParams.payinAccount=custPayIn;
    //       inputParams.payoutAccount=custPayout;
    //       kony.print(JSON.stringify(inputParams));
    //       MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams,this.successCallPaymentSchedule,this.failureCallPaymentSchedule);
    //       this.view.ProgressIndicator.isVisible=true;
    //     }
    //   },
    //   successCallPaymentSchedule : function(response) {
    //     this.view.ProgressIndicator.isVisible=false;
    //     //alert(JSON.stringify(response));
    //     var poparray=[];
    //     var  responseData  = response.schedules;
    //     if(responseData.length> 0)
    //     { for(var i in responseData)
    //     {
    //       var jsonData = {
    //         lblPaymentDate : responseData[i].paymentDueDate,
    //         lblAmount : responseData[i].totalPayment,
    //         lblPrincipal : responseData[i].totalPayment,
    //         lblInterest : responseData[i].prinipalInterest,
    //         lblTax : "0.0",
    //         lbltotaloutstanding:  responseData[i].outstandingAmount
    //       };  poparray.push(jsonData);
    //     }    } 
    //     kony.print(JSON.stringify(poparray));
    //     this._scheduledRevisedPayment = poparray;
    //     this.showChangeRequestListData("");
    //   },
    //   failureCallPaymentSchedule : function(response){
    //     this.view.ProgressIndicator.isVisible=false;
    //     if(response.errorDetails && response.errorDetails.length !== 0 ){
    //       this.view.ErrorAllert.isVisible=true;
    //       this.view.ErrorAllert.lblMessage.text=response.errorDetails[0].message;
    //     }else{
    //       this.view.ErrorAllert.isVisible=true;
    //       this.view.ErrorAllert.lblMessage.text=response.errmsg;
    //     }
    //   },
    //   termValidation : function(){
    //     if(this.view.custLoanTerm.value!==""){
    //       var termValues  = this.view.custLoanTerm.value;
    //       var termText= termValues.match(/[a-zA-Z]+/g);
    //       var termNumber = termValues.match(/\d+/g);
    //       if(termText !== null && termNumber !== null){
    //         var termString = (termText.toString()).toLowerCase();
    //         var termNumberString = termNumber.toString();
    //         if(termString.length === 1 && (termString === "d" ||  termString === "m" || termString === "y" || termString === "w" ) && termNumberString>=1 && kony.string.isNumeric(termNumberString)){
    //           var textvalue=termString;
    //           var textnumber=termNumberString;
    //           var newdates;
    //           var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
    //                             "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    //           //var APIdate='20210128';
    //           //         var year = this.Apidate.substring(0, 4);
    //           //         var month = this.Apidate.substring(4, 6);
    //           //         var day = this.Apidate.substring(6, 8);
    //           //         var formattedDate= year+"-"+month+"-"+day;
    //           var curDate = new Date(this.Apidate);
    //           if(textvalue=== "d")
    //           { newdates = new Date(curDate.setDate(curDate.getDate()+parseInt(textnumber)));}
    //           if(textvalue=== "w")          
    //           {  textnumber=textnumber*7; newdates = new Date(curDate.setDate(curDate.getDate()+parseInt(textnumber)));}
    //           if(textvalue=== "m")
    //           { newdates = new Date(curDate.setMonth(curDate.getMonth()+parseInt(textnumber)));}
    //           if(textvalue=== "y")
    //           { newdates = new Date(curDate.setFullYear(curDate.getFullYear()+parseInt(textnumber)));}
    //           //var txt="";
    //           var dd = newdates.getDate();
    //           var mm = monthNames[newdates.getMonth()];
    //           var yy = newdates.getFullYear();
    //           var maturityDate= ("0"+dd).slice(-2)+" "+mm+" "+yy;
    //           //this.view.custMaturityDate.displayFormat = "dd-MMM-yyyy";
    //           this.view.custMatDate.value = maturityDate;
    //           //if(termString=== "w"){txt="W";} if(termString=== "y"){txt="Y";}  if(termString=== "d"){txt="D";}if(termString=== "m"){txt="M";}
    //           //this.view.custLoanTerm.value=termNumber+""+txt;
    //         }
    //         else{
    //           this.view.custMatDate.value="-";
    //           this.view.ErrorAllert.isVisible=true;
    //           this.view.ErrorAllert.lblMessage.text="Last Letter of Loan Term Should be D,W,M,Y";
    //         }
    //       }
    //       else{  
    //         this.view.custMatDate.value="-";
    //         this.view.ErrorAllert.isVisible=true;
    //         this.view.ErrorAllert.lblMessage.text="Enter the valid Loan Term (10D,10W,10M,10Y)";
    //       }
    //     }
    //   },
    //   //   loadAddRow:function(){  
    //   //     var json_data = {
    //   //       "lblRoles" : {text:this.BeneficiaryName },
    //   //       "lblRoles1" : {text:"Beneficial Owner"}
    //   //     };
    //   //     this.addRowArray.push(json_data);
    //   //     var widgetData = {
    //   //       "lblRole" : "lblRoles",
    //   //       "lblRole1" : "lblRoles1"
    //   //     };
    //   //     this.view.segAddRow.rowTemplate = "flxAddRowWrap";
    //   //     //this.view.cusTierListResponse.value = interestType;
    //   //     this.view.segAddRow.widgetDataMap = widgetData;
    //   //     this.view.segAddRow.setData(this.addRowArray);
    //   //   },
    //   //   kannan:function(){
    //   //     var json_data = {
    //   //       "lblRoles" : "",
    //   //       "lblRoles1" : ""
    //   //     };
    //   //     this.view.segAddRow.addDataAt(json_data,1);  
    //   //   },
    //   AddRowDyn:function(){
    //     this.idtocreate++;
    //     var flxBenRow = new kony.ui.FlexContainer({
    //       "autogrowMode": kony.flex.AUTOGROW_NONE,
    //       "clipBounds": true,
    //       "height": "65px",
    //       "id": "flxBenRow"+this.idtocreate,
    //       "isVisible": true,
    //       "layoutType": kony.flex.FLOW_HORIZONTAL,
    //       "left": "0",
    //       "isModalContainer": false,
    //       "skin": "slFbox",
    //       "top": "0",
    //       "width": "100%"
    //     }, {}, {});
    //     flxBenRow.setDefaultUnit(kony.flex.DP);
    //     var TPWBenName = new kony.ui.CustomWidget({
    //       "id": "TPWBenName"+this.idtocreate,
    //       "isVisible": true,
    //       "left": "0",
    //       "right": "2%",
    //       "top": "5px",
    //       "width": "295px",
    //       "height": "50px",
    //     }, {
    //       "padding": [0, 0, 0, 0],
    //       "paddingInPixel": false,
    //       "skin": "addRowBg"
    //     }, {
    //       "widgetName": "uuxTextField",
    //       "fieldType": {
    //         "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
    //         "selectedValue": "search"
    //       },
    //       "iconButtonIcon": "search",
    //       "labelText": "Beneficiary Name",
    //       "showIconButtonTrailing": true,
    //       "showIconButtonTrailingWithValue": false,
    //       "value": ""
    //     });
    //     var TPWBenRole = new kony.ui.CustomWidget({
    //       "id": "TPWBenRole"+this.idtocreate,
    //       "isVisible": true,
    //       "left": "2%",
    //       "top": "5px",
    //       "width": "315px",
    //       "height": "50px", 
    //     }, {
    //       "padding": [0, 0, 0, 0],
    //       "paddingInPixel": false,
    //       "skin": "addRowBg" 
    //     }, {
    //       "widgetName": "uuxTextField",
    //       "fieldType": {
    //         "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
    //         "selectedValue": "search"
    //       },
    //       "iconButtonIcon": "",
    //       "labelText": "Role",
    //       "showIconButtonTrailing": false,
    //       "showIconButtonTrailingWithValue": false,
    //       "value": ""
    //     });
    //     flxBenRow.add(TPWBenName, TPWBenRole);
    //     this.view.flxTopBenRows.add(flxBenRow);
    //   },
    //   loadBeneficiary:function(){
    //     var custid=this.partyIds;
    //     var serviceName="LendingNew";
    //     var operationName="getCustomers";
    //     var headers="";
    //     var inputParams={"customerId":custid};
    //     MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams,
    //                                        this.successCallBackcustomer,
    //                                        this.failureCallBackfunccustomer);
    //     this.view.ProgressIndicator.isVisible=true;
    //   },
    //   successCallBackcustomer : function(res) {
    //     //kony.print("response from post increase commitment"+JSON.stringify(res));
    //     var respone = res;
    //     var customerName =  (respone.customerName);
    //     this.view.TPWBenName.value = customerName;
    //     this.view.lblCustomerName.text=customerName;
    //     this.custName=customerName;
    //     //this.BeneficiaryName = customerName;
    //     //this.loadAddRow();
    //     this.view.ProgressIndicator.isVisible=false;
    //   },
    //   failureCallBackfunccustomer : function(res){
    //     this.view.ProgressIndicator.isVisible=false;  
    //     alert("Error"+JSON.stringify(res));
    //   },
    //   loadProductConditions:function(){
    //     var serviceName="LendingNew";
    //     var operationName="getProductConditions";
    //     var headers="";
    //     var inputParams={"pid":this.productLoanId,"pname":this.productName};
    //     MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams,
    //                                        this.successCallBackfuncPc,
    //                                        this.failureCallBackfuncPc);
    //     this.view.ProgressIndicator.isVisible=true;
    //   },
    //   successCallBackfuncPc : function(response) {
    //     var res= response.body;    
    //     var arrayindex= res.findIndex(this.findArrayindex);
    //     var PrincipalInterest= response.body[arrayindex].principal+"% - " +response.body[arrayindex].rateType+" Rate";
    //     var PenaltyInterest=response.body[arrayindex].interestRate+"% - "+response.body[arrayindex].rateType+" Rate";
    //     var Repayment=response.body[arrayindex].frequency; 
    //     //alert(Repayment);
    //     this.view.custPrincipalInterest.value = PrincipalInterest;
    //     this.view.custPenaltyInterest.value = PenaltyInterest;
    //     //this.view.custRepaymentFrequency.value="e0Y e1M e0W e0D e0F";
    //     this.view.custRepaymentFrequency.value=(Repayment === "Monthly") ? "e0Y e1M e0W e0D e0F" :  "";
    //     //TODO : DV - default the service response frequency
    //     // For not not defaulting to anything
    //     //DV//this.view.custRepaymentFrequency.options = '[{"label": "Select","value":"None"},{"label":"'+Repayment+'","value":"e0Y e1M e0W e0D e0F"}]';
    //     //DV//this.view.custRepaymentFrequency.value = Repayment;
    //     //this.view.custLoanTerm.value="test";
    //     //alert(+response.body[0].principal[0]); 
    //     this.view.ProgressIndicator.isVisible=false;
    //   },
    //   findArrayindex:function(res1){ 
    //     var currvalue=this.currencyval;
    //     return res1.currency === currvalue;
    //   },
    //   failureCallBackfuncPc : function(response) {
    //     this.view.ProgressIndicator.isVisible=false; 
    //     if(response.error && response.error.length !== 0 ){
    //       this.view.ErrorAllert.isVisible=true;
    //       this.view.ErrorAllert.lblMessage.text=response.error.message;
    //     }
    //   },    
    //   getAccountSeginfo: function(){
    //     var getAcctId=this.view.AccountList.segAccountList.selectedRowItems[0].accountid;
    //     if(this.acctflex === "payin"){   
    //       this.view.tbxPayInAccount.txtFloatText.text=getAcctId; 
    //       this.view.tbxPayInAccount.animateComponent();
    //     }
    //     this.view.flxPayinComp.isVisible=false;
    //     this.view.flxClickEnable.left="-210%";
    //   },
    //   getAccountSeginfo1: function(){
    //     var getAcctId=this.view.AccountList2.segAccountList.selectedRowItems[0].accountid;
    //     if(this.acctflex === "payout"){       
    //       this.view.tbxPayOutAccount.txtFloatText.text=getAcctId;   
    //       this.view.tbxPayOutAccount.animateComponent();
    //     }this.view.flxPayoutComp.isVisible=false;
    //     this.view.flxClickEnable.left="-210%";
    //   },
    //   getSegRowinfo: function(){
    //     var getAcctId=this.view.popupPayinPayout.segPayinPayout.selectedRowItems[0].accountid;
    //     if(this.accttype === "payout"){   
    //       this.view.tbxPayOutAccount.txtFloatText.text=getAcctId;  
    //       this.view.tbxPayOutAccount.animateComponent();
    //     }else{
    //       this.view.tbxPayInAccount.txtFloatText.text=getAcctId;//
    //       this.view.tbxPayInAccount.animateComponent();
    //     }
    //     this.view.flxpayInOutPOP.isVisible=false;
    //     this.view.popupPayinPayout.txtSearchBox.text="";
    //     this.view.popupPayinPayout.segPayinPayout.removeAll();
    //     this.view.popupPayinPayout.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate; 
    //     this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    //   },
    //   showPaginationSegmentData : function(dataPerPage, arrayOfData, btnName)
    //   {
    //     kony.print("showPaginationSegmentData===>");
    //     var noOfItemsPerPage = dataPerPage;
    //     var tmpData = [];
    //     try {    	
    //       var segVal = arrayOfData;
    //       var len = segVal.length;
    //       this.totalpage = parseInt(len / noOfItemsPerPage);
    //       var remainingItems = parseInt(len) % noOfItemsPerPage;
    //       this.totalpage = remainingItems > 0 ? this.totalpage + 1 : this.totalpage;
    //       if (this.totalpage > 1) {
    //         this.view.flxPagination.isVisible = true;            
    //       }else {
    //         this.view.flxPagination.isVisible = false;         
    //       }
    //       if (btnName === "btnPrev" && this.gblSegPageCount > 1) {
    //         this.gblSegPageCount--;
    //       }else if (btnName === "btnNext" && this.gblSegPageCount < this.totalpage) {
    //         this.gblSegPageCount++;
    //       }else {
    //         this.gblSegPageCount = 1;
    //       }
    //       this.view.lblSearchPage.text = this.gblSegPageCount + "  " + "of" + "  " + this.totalpage;     
    //       if (this.gblSegPageCount > 1 && this.gblSegPageCount < this.totalpage) {     
    //         this.view.btnPrev.setEnabled(true);
    //         this.view.btnNext.setEnabled(true);
    //         this.view.btnPrev.skin = "sknBtnFocus";
    //         this.view.btnNext.skin = "sknBtnFocus";
    //         //this.view.lblSearchPage.isVisible = true;            
    //       } else if (this.gblSegPageCount === 1 && this.totalpage > 1) {
    //         this.view.btnPrev.setEnabled(false);
    //         this.view.btnNext.setEnabled(true);
    //         this.view.btnPrev.skin = "sknBtnDisable";
    //         this.view.btnNext.skin = "sknBtnFocus";
    //         //this.view.lblSearchPage.isVisible = true;           
    //       } else if (this.gblSegPageCount === 1 && this.totalpage === 1) {
    //         this.view.btnPrev.setEnabled(false);
    //         this.view.btnNext.setEnabled(false);
    //         this.view.btnPrev.skin = "sknBtnDisable";
    //         this.view.btnNext.skin = "sknBtnDisable";
    //         this.view.lblSearchPage.text = "";          
    //       } else if (this.gblSegPageCount === this.totalpage && this.totalpage > 1) {
    //         this.view.btnPrev.setEnabled(true);
    //         this.view.btnNext.setEnabled(false);
    //         this.view.btnPrev.skin = "sknBtnFocus";
    //         this.view.btnNext.skin = "sknBtnDisable";
    //         //this.view.lblSearchPage.isVisible = true;            
    //       } else {
    //         this.view.btnPrev.skin = "sknBtnDisable";
    //         this.view.btnNext.skin = "sknBtnDisable";
    //         this.view.btnPrev.setEnabled(false);
    //         this.view.btnNext.setEnabled(false);
    //         this.view.lblSearchPage.text ="";     
    //       }
    //       var i = (this.gblSegPageCount - 1) * noOfItemsPerPage;
    //       var total = this.gblSegPageCount * noOfItemsPerPage;
    //       for (var j = i; j < total; j++) {
    //         if (j < len) {
    //           tmpData.push(segVal[j]);
    //         }
    //       }     
    //       return tmpData;
    //     } catch (err) {
    //       kony.print("Error in next previous method "+err);
    //     }
    //   },
    //   showChangeRequestListData : function(buttonevent)
    //   {
    //     kony.print("showChangeRequestListData===>");
    //     //showloader();
    //     if(this._scheduledRevisedPayment.length > 0 )
    //     {
    //       var segmentData = this.showPaginationSegmentData(this.dataPerPageNumber, this._scheduledRevisedPayment, buttonevent.id);
    //       this.view.segRevised.setData(segmentData);
    //       this.view.flxPaymentSchedule.isVisible = true;
    //       kony.print("segDataSet ====>>"+JSON.stringify(segmentData));
    //       this.view.forceLayout();
    //     }
    //     //dismissLoader();
    //   },
    //   hideAccount: function(){
    //     if(this.view.flxPayoutComp.isVisible){
    //       this.view.flxPayoutComp.isVisible=false;
    //     }
    //     if(this.view.flxPayinComp.isVisible){
    //       this.view.flxPayinComp.isVisible=false;
    //     }
    //     this.view.flxClickEnable.left="-210%";
    //   },
    //   doCRSearchWithinSeg : function(){
    //       kony.print('edit');
    //       var stringForSearch =this.view.popupPayinPayout.txtSearchBox.text.toLowerCase();
    //       if(stringForSearch.length === 0) {
    //         //Reset to show the full claims search results global variable
    //         // disable the clear search x mark, if any
    //         this.view.popupPayinPayout.segPayinPayout.removeAll();
    //         this.view.popupPayinPayout.segPayinPayout.setData(gblAllaccountarray);
    //       }
    //       else {
    //         //empty search string now
    //         // visible on for the clear search x mark
    //       }
    //       if(stringForSearch.length>=3)
    //       { 
    //         //this.view.popupPayinPayout.segPayinPayout
    //         var segData=gblAllaccountarray; // make a copy not response
    //         var finalData =[];
    //         if(segData && segData === []){
    //           return;
    //         }
    //         for(var i=0; i<segData.length; i++){
    //           var aRowData =  segData[i];
    //           var aRowDataStrigified = JSON.stringify(aRowData);
    //           if(aRowDataStrigified.toLowerCase().indexOf(stringForSearch) >= 0 ){
    //             finalData.push(aRowData);
    //           } else {
    //             //no need to push it - as does not meet criteria
    //           }
    //         }
    //         this.view.popupPayinPayout.segPayinPayout.removeAll();
    //         this.view.popupPayinPayout.segPayinPayout.setData(finalData);
    //       }
    //     }
    //   //   valueSeperator : function(){  
    //   //   var incAmount = this.view.custLoanAmount.value;
    //   //     if(incAmount !== ""){
    //   //       var Amountvalues = incAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    //   //       this.view.custLoanAmount.value =Amountvalues;
    //   //       }    
    //   //   },
});
define("frmDepositCreationControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_TPW_f937666cd8aa4daebf54a21779a11059: function AS_TPW_f937666cd8aa4daebf54a21779a11059(eventobject) {
        var self = this;
        return
    },
});
define("frmDepositCreationController", ["userfrmDepositCreationController", "frmDepositCreationControllerActions"], function() {
    var controller = require("userfrmDepositCreationController");
    var controllerActions = ["frmDepositCreationControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
