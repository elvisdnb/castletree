define("frmDepositCreation", function() {
    return function(controller) {
        function addWidgetsfrmDepositCreation() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "6.50%",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "0dp",
                "width": "90%",
                "zIndex": 9
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoansSuccessMessage = new kony.ui.Label({
                "height": "20dp",
                "id": "lblLoansSuccessMessage",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BG",
                "text": "New Deposit created for John Doe",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblLoansSuccessMessage);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "pagingEnabled": false,
                "right": "24dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxContainerBGF0hc984c66b6a545",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var FlexContainer0d8781aeee9e447 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "968dp",
                "id": "FlexContainer0d8781aeee9e447",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "98%",
                "zIndex": 9
            }, {}, {});
            FlexContainer0d8781aeee9e447.setDefaultUnit(kony.flex.DP);
            var flxMainWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "50dp",
                "clipBounds": true,
                "id": "flxMainWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "200dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.CustomWidget({
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "109dp",
                "skin": "sknLbl12pxBG4D4D4D",
                "text": "Overview",
                "top": "26dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "8dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDepositCreationTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": false,
                "height": "600dp",
                "id": "flxDepositCreationTab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a0d1087168f844",
                "top": "5dp",
                "width": "98%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxDepositCreationTab.setDefaultUnit(kony.flex.DP);
            var flxTopWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "centerX": "50.23%",
                "clipBounds": false,
                "id": "flxTopWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxTopWrapper.setDefaultUnit(kony.flex.DP);
            var flxNewDepositTitles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNewDepositTitles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNewDepositTitles.setDefaultUnit(kony.flex.DP);
            var flxDepositTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxDepositTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "28dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3dp",
                "width": "252dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxDepositTitle.setDefaultUnit(kony.flex.DP);
            var lblNewLoans = new kony.ui.Label({
                "height": "29dp",
                "id": "lblNewLoans",
                "isVisible": true,
                "left": "0",
                "skin": "primary24px",
                "text": "New Deposit Creation",
                "top": "0",
                "width": "250dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerName = new kony.ui.Label({
                "id": "lblCustomerName",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0g1225c742c9844",
                "text": "for John Doe",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDepositTitle.add(lblNewLoans, lblCustomerName);
            var flxDepositSteps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxDepositSteps",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "122dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "550dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxDepositSteps.setDefaultUnit(kony.flex.DP);
            var flxStep1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "146.15%",
                "id": "flxStep1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-25dp",
                "width": "140px"
            }, {}, {});
            flxStep1.setDefaultUnit(kony.flex.DP);
            var flxStepClick = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStepClick",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "skinBlue",
                "top": "0",
                "width": "40dp"
            }, {}, {});
            flxStepClick.setDefaultUnit(kony.flex.DP);
            var TPWFlag1 = new kony.ui.CustomWidget({
                "id": "TPWFlag1",
                "isVisible": true,
                "left": "0",
                "top": "0",
                "width": "24px",
                "height": "24px",
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-light",
                "iconName": "flag",
                "size": "small"
            });
            flxStepClick.add(TPWFlag1);
            var Label0f0949fa7b1ca45 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "Label0f0949fa7b1ca45",
                "isVisible": true,
                "left": "10dp",
                "skin": "primary14pxregular",
                "text": "Basic details",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStep1.add(flxStepClick, Label0f0949fa7b1ca45);
            var flxArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxArrow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "5dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "135dp"
            }, {}, {});
            flxArrow.setDefaultUnit(kony.flex.DP);
            var TPWArrow = new kony.ui.CustomWidget({
                "id": "TPWArrow",
                "isVisible": false,
                "left": "0",
                "top": "0",
                "width": "120px",
                "height": "50px",
                "centerX": "50.00%",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-primary",
                "iconName": "trending_flat",
                "size": "large"
            });
            var imgLoanArrow = new kony.ui.Image2({
                "height": "40dp",
                "id": "imgLoanArrow",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "loanarrow.png",
                "top": "0",
                "width": "130dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrow.add(TPWArrow, imgLoanArrow);
            var flxDepositSpecifies = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDepositSpecifies",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "16dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": -12,
                "width": "170px"
            }, {}, {});
            flxDepositSpecifies.setDefaultUnit(kony.flex.DP);
            var flxStepClick2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStepClick2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "stepUnActive",
                "top": "0",
                "width": "40dp"
            }, {}, {});
            flxStepClick2.setDefaultUnit(kony.flex.DP);
            var TPWFlag2 = new kony.ui.CustomWidget({
                "id": "TPWFlag2",
                "isVisible": true,
                "left": "0",
                "top": "0",
                "width": "24px",
                "height": "24px",
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-primary",
                "iconName": "flag",
                "size": "small"
            });
            flxStepClick2.add(TPWFlag2);
            var lblDepositSpecifies = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblDepositSpecifies",
                "isVisible": true,
                "left": "10dp",
                "skin": "primary14pxregular",
                "text": "Deposit specifics",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDepositSpecifies.add(flxStepClick2, lblDepositSpecifies);
            flxDepositSteps.add(flxStep1, flxArrow, flxDepositSpecifies);
            flxNewDepositTitles.add(flxDepositTitle, flxDepositSteps);
            var flxBasicDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "centerX": "50%",
                "clipBounds": false,
                "height": "330dp",
                "id": "flxBasicDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "83dp",
                "width": "1100dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxBasicDetails.setDefaultUnit(kony.flex.DP);
            var flxTopRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "330dp",
                "id": "flxTopRow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "750dp",
                "zIndex": 1
            }, {}, {});
            flxTopRow.setDefaultUnit(kony.flex.DP);
            var flxTopBenRows = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "270dp",
                "horizontalScrollIndicator": true,
                "id": "flxTopBenRows",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "maxHeight": "200dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxTopBenRows.setDefaultUnit(kony.flex.DP);
            var flxBenRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxBenRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "minHeight": 250,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxBenRow.setDefaultUnit(kony.flex.DP);
            var flxFirstRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFirstRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxFirstRow.setDefaultUnit(kony.flex.DP);
            var TPWBenName = new kony.ui.CustomWidget({
                "id": "TPWBenName",
                "isVisible": true,
                "left": "0dp",
                "top": "5px",
                "width": "295px",
                "height": "56px",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "search",
                "labelText": "Beneficiary Name",
                "onKeyDown": "",
                "onKeyUp": "",
                "readonly": true,
                "showIconButtonTrailing": true,
                "showIconButtonTrailingWithValue": false,
                "value": "John Doe"
            });
            var TPWBenRole = new kony.ui.CustomWidget({
                "id": "TPWBenRole",
                "isVisible": true,
                "left": "20dp",
                "top": "5px",
                "width": "295dp",
                "height": "56px",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "search"
                },
                "iconButtonIcon": "",
                "labelText": "Role",
                "onKeyDown": "",
                "onKeyUp": "",
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": "Beneficial Owner"
            });
            flxFirstRow.add(TPWBenName, TPWBenRole);
            var segAddRow = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }, {
                    "lblBenLabel": "Beneficiary Name",
                    "lblBenLabel1": "Role",
                    "lblDelete": "Delete",
                    "lblDeleteIcon": "Q",
                    "lblRole": {
                        "placeholder": "",
                        "text": ""
                    },
                    "lbxBenRole": {
                        "masterData": [
                            ["lb1", "Placeholder One"],
                            ["lb2", "Placeholder Two"],
                            ["lb3", "Placeholder Three"]
                        ],
                        "selectedKey": null,
                        "selectedKeys": null
                    }
                }],
                "groupCells": false,
                "id": "segAddRow",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAddCutomerRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ffffff00",
                "separatorRequired": true,
                "separatorThickness": 15,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddCutomerRow": "flxAddCutomerRow",
                    "flxBenName": "flxBenName",
                    "flxBenRole": "flxBenRole",
                    "flxDelete": "flxDelete",
                    "flxImgClicks": "flxImgClicks",
                    "lblBenLabel": "lblBenLabel",
                    "lblBenLabel1": "lblBenLabel1",
                    "lblDelete": "lblDelete",
                    "lblDeleteIcon": "lblDeleteIcon",
                    "lblRole": "lblRole",
                    "lbxBenRole": "lbxBenRole"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBenRow.add(flxFirstRow, segAddRow);
            flxTopBenRows.add(flxBenRow);
            var flxAddRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAddRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxAddRow.setDefaultUnit(kony.flex.DP);
            var btnRowAdd = new kony.ui.CustomWidget({
                "id": "btnRowAdd",
                "isVisible": true,
                "left": "0",
                "top": "0",
                "width": "150px",
                "height": "40px",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "add_circle",
                "labelText": "Add Row",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxAddRow.add(btnRowAdd);
            flxTopRow.add(flxTopBenRows, flxAddRow);
            var flxRightpart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxRightpart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "340dp",
                "zIndex": 1
            }, {}, {});
            flxRightpart.setDefaultUnit(kony.flex.DP);
            var custDepositType = new kony.ui.CustomWidget({
                "id": "custDepositType",
                "isVisible": true,
                "right": "3dp",
                "top": "20dp",
                "width": "303dp",
                "height": "55px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Deposit Type",
                "onKeyDown": "",
                "onKeyUp": "",
                "readonly": true,
                "showIconButtonTrailing": true,
                "showIconButtonTrailingWithValue": false,
                "value": "Short Term"
            });
            var custLoanType1 = new kony.ui.CustomWidget({
                "id": "custLoanType1",
                "isVisible": false,
                "right": "3dp",
                "top": "50dp",
                "width": "300px",
                "height": "65px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Loan Type",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custCurrency = new kony.ui.CustomWidget({
                "id": "custCurrency",
                "isVisible": true,
                "right": "3dp",
                "top": "100dp",
                "width": "303dp",
                "height": "55dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Currency",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": "USD"
            });
            var lblCurrencyErr = new kony.ui.Label({
                "id": "lblCurrencyErr",
                "isVisible": false,
                "left": "40dp",
                "skin": "ErrSkin",
                "text": "A Selection is Mandatory",
                "top": "155dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custDepositAmount = new kony.ui.CustomWidget({
                "id": "custDepositAmount",
                "isVisible": true,
                "right": "3dp",
                "top": "180dp",
                "width": "303dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Deposit Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "suffix": "",
                "validateRequired": "required",
                "value": ""
            });
            var lblDepositAmountErr = new kony.ui.Label({
                "id": "lblDepositAmountErr",
                "isVisible": false,
                "left": "40dp",
                "skin": "ErrSkin",
                "text": "This has to be filled",
                "top": "235dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custDepositTerm = new kony.ui.CustomWidget({
                "id": "custDepositTerm",
                "isVisible": true,
                "right": "3dp",
                "top": "260dp",
                "width": "303dp",
                "height": "55dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Deposit Term",
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": true,
                "validateRequired": "required",
                "value": ""
            });
            var lblDepositTermErr = new kony.ui.Label({
                "id": "lblDepositTermErr",
                "isVisible": false,
                "left": "40dp",
                "skin": "ErrSkin",
                "text": "This has to be filled",
                "top": "315dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightpart.add(custDepositType, custLoanType1, custCurrency, lblCurrencyErr, custDepositAmount, lblDepositAmountErr, custDepositTerm, lblDepositTermErr);
            flxBasicDetails.add(flxTopRow, flxRightpart);
            var frmDepositSpecifiesTab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "centerX": "50%",
                "clipBounds": false,
                "height": "300dp",
                "id": "frmDepositSpecifiesTab",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "103dp",
                "width": "1100dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            frmDepositSpecifiesTab.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var custLoanAmount1 = new kony.ui.CustomWidget({
                "id": "custLoanAmount1",
                "isVisible": false,
                "left": "32dp",
                "top": "0dp",
                "width": "273dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Loan Amount",
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": true,
                "validateRequired": "required",
                "value": ""
            });
            var custLoanTerm1 = new kony.ui.CustomWidget({
                "id": "custLoanTerm1",
                "isVisible": false,
                "left": "275dp",
                "right": "3dp",
                "top": "0dp",
                "width": "255dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "custLoanTerm",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custMaturityDate = new kony.ui.CustomWidget({
                "id": "custMaturityDate",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "273dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": true,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Maturity date",
                "max": "",
                "min": "-",
                "showWeekNumber": false,
                "value": "-",
                "weekLabel": "Wk"
            });
            var custMatDate = new kony.ui.CustomWidget({
                "id": "custMatDate",
                "isVisible": false,
                "left": "67%",
                "top": "0dp",
                "width": "273dp",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Maturity Date",
                "onKeyDown": "",
                "onKeyUp": "",
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": " "
            });
            var custDepositInterest = new kony.ui.CustomWidget({
                "id": "custDepositInterest",
                "isVisible": true,
                "left": "320dp",
                "top": "0dp",
                "width": "273px",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Deposit Interest",
                "onKeyDown": "",
                "onKeyUp": "",
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": " 10%"
            });
            var custPenaltyInterest = new kony.ui.CustomWidget({
                "id": "custPenaltyInterest",
                "isVisible": false,
                "left": "35%",
                "top": "74dp",
                "width": "273dp",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Penalty Interest",
                "onKeyDown": "",
                "onKeyUp": "",
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": " "
            });
            var custRepaymentFrequencyold = new kony.ui.CustomWidget({
                "id": "custRepaymentFrequencyold",
                "isVisible": false,
                "left": "72%",
                "top": "74dp",
                "width": "273dp",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Repayment Frequency",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            var custInterestFrequency = new kony.ui.CustomWidget({
                "id": "custInterestFrequency",
                "isVisible": true,
                "left": "500dp",
                "top": "90dp",
                "width": "297dp",
                "height": "56dp",
                "zIndex": 2,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRecurrenceDatePicker",
                "dailyMax": "",
                "dense": true,
                "formattedValue": "",
                "lableText": "Interest Payout Frequency",
                "messagePrefix": "",
                "monthlyDayMax": "",
                "monthlyMax": "",
                "value": "",
                "weeklyMax": "",
                "yearlyDayMax": "",
                "yearlyMax": ""
            });
            var custLimitID = new kony.ui.CustomWidget({
                "id": "custLimitID",
                "isVisible": false,
                "left": "825px",
                "top": "70dp",
                "width": "255dp",
                "height": "50px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Limit ID",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var flxpayOutSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayOutSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "skin": "searchflex",
                "top": 180,
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxpayOutSearch.setDefaultUnit(kony.flex.DP);
            var iconPayoutsearch = new kony.ui.CustomWidget({
                "id": "iconPayoutsearch",
                "isVisible": false,
                "left": "0dp",
                "top": "0",
                "width": "40dp",
                "height": "40dp",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-light",
                "iconName": "search",
                "size": "medium"
            });
            var lblSearchIcon1 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblSearchIcon1",
                "isVisible": true,
                "left": "0%",
                "skin": "iconFontPurple20px",
                "text": "k",
                "top": "0",
                "width": "20dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxpayOutSearch.add(iconPayoutsearch, lblSearchIcon1);
            var flxpayInSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayInSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "570dp",
                "isModalContainer": false,
                "skin": "searchflex",
                "top": 180,
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxpayInSearch.setDefaultUnit(kony.flex.DP);
            var iconPayinsearch = new kony.ui.CustomWidget({
                "id": "iconPayinsearch",
                "isVisible": false,
                "left": "0",
                "top": "0",
                "width": "40px",
                "height": "40px",
                "centerY": "50%",
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": " temenos-light",
                "iconName": "search",
                "size": "medium"
            });
            var lblSearchIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblSearchIcon",
                "isVisible": true,
                "left": "0%",
                "skin": "iconFontPurple20px",
                "text": "k",
                "top": 0,
                "width": "26dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxpayInSearch.add(iconPayinsearch, lblSearchIcon);
            var tbxPayOutAccount1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "CopydefTextBoxNormal0e65805a6ac6e46",
                "height": "56px",
                "id": "tbxPayOutAccount1",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "31dp",
                "onTouchStart": controller.AS_TextField_j4b048bc619c4ce1b26bba5669ca874f,
                "placeholder": "PayOut",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0e65805a6ac6e46",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "188dp",
                "width": "233dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "CopydefTextBoxPlaceholder0jb990765c75e41"
            });
            var flxpayout1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxpayout1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "150dp",
                "width": "220dp",
                "zIndex": 1
            }, {}, {});
            flxpayout1.setDefaultUnit(kony.flex.DP);
            flxpayout1.add();
            var tbxPayInAccount1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "56dp",
                "id": "tbxPayInAccount1",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "329dp",
                "placeholder": "PayIn",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0c202ab592b5649",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "188dp",
                "width": "233dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var tbxPayOutAccount = new com.lending.floatingTextBox.floatLabelText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "tbxPayOutAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "flxTemenosLighest",
                "top": "180dp",
                "width": "230dp",
                "zIndex": 1,
                "overrides": {
                    "downArrowLbl": {
                        "isVisible": true,
                        "zIndex": 5
                    },
                    "floatLabelText": {
                        "centerX": "viz.val_cleared",
                        "height": "40dp",
                        "isVisible": true,
                        "left": "0dp",
                        "top": "180dp",
                        "width": "230dp",
                        "zIndex": 1
                    },
                    "lblFloatLabel": {
                        "left": "5%",
                        "text": "Pay-out Account "
                    },
                    "txtFloatText": {
                        "centerY": "49.19%",
                        "isVisible": true,
                        "left": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var tbxPayInAccount = new com.lending.floatingTextBox.floatLabelText({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "tbxPayInAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "28.84%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "flxTemenosLighest",
                "top": "180dp",
                "width": "253dp",
                "zIndex": 1,
                "overrides": {
                    "downArrowLbl": {
                        "isVisible": true,
                        "zIndex": 5
                    },
                    "floatLabelText": {
                        "centerX": "viz.val_cleared",
                        "height": "40dp",
                        "isVisible": true,
                        "left": "28.84%",
                        "top": "180dp",
                        "width": "253dp",
                        "zIndex": 1
                    },
                    "lblFloatLabel": {
                        "left": "5%",
                        "text": "Pay-in Account "
                    },
                    "txtFloatText": {
                        "centerY": "50%",
                        "left": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxPayinComp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "235dp",
                "id": "flxPayinComp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "28.89%",
                "isModalContainer": false,
                "skin": "CopyslFbox0a1f15f55625746",
                "top": "220dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxPayinComp.setDefaultUnit(kony.flex.DP);
            var AccountList = new com.konymp.AccountList.AccountList({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "height": "235dp",
                "id": "AccountList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxWhiteBGBorder0075db",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "AccountList": {
                        "height": "235dp",
                        "width": "100%"
                    },
                    "segAccountList": {
                        "height": "190dp",
                        "top": "40dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPayinComp.add(AccountList);
            var flxPayoutComp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "235dp",
                "id": "flxPayoutComp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fdd0bec932a740",
                "top": "220dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxPayoutComp.setDefaultUnit(kony.flex.DP);
            var AccountList2 = new com.konymp.AccountList2.AccountList2({
                "bottom": "0dp",
                "centerX": "50%",
                "height": "235dp",
                "id": "AccountList2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0e816618aedef47",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "AccountList2": {
                        "bottom": "0dp",
                        "height": "235dp",
                        "isVisible": true
                    },
                    "segAccountList": {
                        "height": "195dp",
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPayoutComp.add(AccountList2);
            var flxSeePayment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxSeePayment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "630dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13.64%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxSeePayment.setDefaultUnit(kony.flex.DP);
            var TPW0hb1a91d98e404c = new kony.ui.CustomWidget({
                "id": "TPW0hb1a91d98e404c",
                "isVisible": true,
                "left": "10dp",
                "top": "15dp",
                "width": "30px",
                "height": "25px",
                "centerY": "40%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "autorenew",
                "size": "small"
            });
            var lblViewSchedule = new kony.ui.Label({
                "centerY": "40%",
                "id": "lblViewSchedule",
                "isVisible": true,
                "left": "0dp",
                "skin": "primary14pxregular",
                "text": "View Schedule",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeePayment.add(TPW0hb1a91d98e404c, lblViewSchedule);
            var lblSelectModeInfo = new kony.ui.Label({
                "height": "56dp",
                "id": "lblSelectModeInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopydefLabel0g1225c742c9844",
                "text": "Pay Interest on Maturity",
                "top": "100dp",
                "width": "17%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusRadioChangeInterestType = new kony.ui.CustomWidget({
                "id": "cusRadioChangeInterestType",
                "isVisible": true,
                "left": "20%",
                "top": "88dp",
                "width": "15%",
                "height": "60dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRadioGroup",
                "labelText": "",
                "options": {
                    "selectedValue": ""
                },
                "value": ""
            });
            var lblInterestPayoutErr = new kony.ui.Label({
                "id": "lblInterestPayoutErr",
                "isVisible": false,
                "left": "633dp",
                "skin": "ErrSkin",
                "text": "This has to be Selected",
                "top": "145dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPayoutAcctErr = new kony.ui.Label({
                "id": "lblPayoutAcctErr",
                "isVisible": false,
                "left": "12dp",
                "skin": "ErrSkin",
                "text": "A Selection is Mandatory",
                "top": "220dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPayinAcctErr = new kony.ui.Label({
                "id": "lblPayinAcctErr",
                "isVisible": false,
                "left": "333dp",
                "skin": "ErrSkin",
                "text": "A Selection is Mandatory",
                "top": "220dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRow1.add(custLoanAmount1, custLoanTerm1, custMaturityDate, custMatDate, custDepositInterest, custPenaltyInterest, custRepaymentFrequencyold, custInterestFrequency, custLimitID, flxpayOutSearch, flxpayInSearch, tbxPayOutAccount1, flxpayout1, tbxPayInAccount1, tbxPayOutAccount, tbxPayInAccount, flxPayinComp, flxPayoutComp, flxSeePayment, lblSelectModeInfo, cusRadioChangeInterestType, lblInterestPayoutErr, lblPayoutAcctErr, lblPayinAcctErr);
            frmDepositSpecifiesTab.add(flxRow1);
            var lblErrorMsg = new kony.ui.Label({
                "height": "21dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "70dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "320dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50.04%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "440dp",
                "width": "1100dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxCustButtons.setDefaultUnit(kony.flex.DP);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "46.36%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3dp",
                "width": "620dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var custCancel = new kony.ui.CustomWidget({
                "id": "custCancel",
                "isVisible": true,
                "right": "330dp",
                "top": "0dp",
                "width": "196dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custProceed = new kony.ui.CustomWidget({
                "id": "custProceed",
                "isVisible": true,
                "right": "110dp",
                "top": "0dp",
                "width": "196dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "onclickEvent": controller.AS_TPW_f937666cd8aa4daebf54a21779a11059,
                "labelText": "Proceed",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custSubmit = new kony.ui.CustomWidget({
                "id": "custSubmit",
                "isVisible": true,
                "right": "110dp",
                "top": "0dp",
                "width": "196dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Submit",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxButtons.add(custCancel, custProceed, custSubmit);
            var lblProceedErr = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblProceedErr",
                "isVisible": false,
                "left": "49dp",
                "skin": "lblProceedErr",
                "text": "* All mandatory fields needs to be filled ",
                "top": "27dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubmitErr = new kony.ui.Label({
                "centerY": "60%",
                "height": "20dp",
                "id": "lblSubmitErr",
                "isVisible": false,
                "left": "59dp",
                "skin": "lblProceedErr",
                "text": "* All mandatory fields needs to be filled ",
                "top": "27dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustButtons.add(flxButtons, lblProceedErr, lblSubmitErr);
            flxTopWrapper.add(flxNewDepositTitles, flxBasicDetails, frmDepositSpecifiesTab, lblErrorMsg, flxCustButtons);
            var flxClickEnable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "530dp",
                "id": "flxClickEnable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-210%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 30,
                "width": "105%",
                "zIndex": 1
            }, {}, {});
            flxClickEnable.setDefaultUnit(kony.flex.DP);
            flxClickEnable.add();
            var FlexContainer0g9617a8127cf4c = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "FlexContainer0g9617a8127cf4c",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fda5754cd9f44b",
                "top": "90dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            FlexContainer0g9617a8127cf4c.setDefaultUnit(kony.flex.DP);
            FlexContainer0g9617a8127cf4c.add();
            flxDepositCreationTab.add(flxTopWrapper, flxClickEnable, FlexContainer0g9617a8127cf4c);
            flxMainWrapper.add(flxHeaderMenu, custInfo, flxDepositCreationTab);
            var custPayout = new kony.ui.CustomWidget({
                "id": "custPayout",
                "isVisible": false,
                "left": "-600px",
                "top": "510dp",
                "width": "225dp",
                "height": "50px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Pay-out Account",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custPayIn = new kony.ui.CustomWidget({
                "id": "custPayIn",
                "isVisible": false,
                "left": "-600px",
                "top": "510dp",
                "width": "225dp",
                "height": "50px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "Pay-in Account",
                "options": "",
                "readonly": false,
                "value": ""
            });
            FlexContainer0d8781aeee9e447.add(flxMainWrapper, custPayout, custPayIn);
            flxMainContainer.add(FlexContainer0d8781aeee9e447);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "6.50%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "92.50%",
                "zIndex": 1,
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "6.50%",
                        "width": "92.50%",
                        "zIndex": 1
                    },
                    "imgCloseBackup": {
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxViewSchedule = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewSchedule",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxViewSchedule.setDefaultUnit(kony.flex.DP);
            var flxRevisedPaymentSchedule = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxRevisedPaymentSchedule",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0h870f661235f4e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRevisedPaymentSchedule.setDefaultUnit(kony.flex.DP);
            var flxRevisedPayment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "400dp",
                "id": "flxRevisedPayment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "CopyslFbox0g3fb5b22c13341",
                "width": "835dp",
                "zIndex": 1
            }, {}, {});
            flxRevisedPayment.setDefaultUnit(kony.flex.DP);
            var flxRevisedPmtHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRevisedPmtHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "24dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "796dp",
                "zIndex": 1
            }, {}, {});
            flxRevisedPmtHeader.setDefaultUnit(kony.flex.DP);
            var lblRevisedPayment = new kony.ui.Label({
                "height": "29dp",
                "id": "lblRevisedPayment",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopydefLabel0h891708545fc44",
                "text": "Payment Schedule",
                "top": "2dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgPrint = new kony.ui.Image2({
                "height": "18dp",
                "id": "imgPrint",
                "isVisible": true,
                "left": "718dp",
                "skin": "slImage",
                "src": "ico_print.png",
                "top": "2dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgClose1 = new kony.ui.Image2({
                "height": "18dp",
                "id": "imgClose1",
                "isVisible": false,
                "left": "764dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "top": "2dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPrint = new kony.ui.Label({
                "id": "lblPrint",
                "isVisible": false,
                "left": 718,
                "skin": "castleicon",
                "text": "P",
                "top": 2,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblClose = new kony.ui.Label({
                "id": "lblClose",
                "isVisible": true,
                "left": 764,
                "skin": "castleicon",
                "text": "H",
                "top": 2,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRevisedPmtHeader.add(lblRevisedPayment, imgPrint, imgClose1, lblPrint, lblClose);
            var flxRevisedPaymentDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "250dp",
                "id": "flxRevisedPaymentDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxRevisedPaymentDetails.setDefaultUnit(kony.flex.DP);
            var lblPaymentDate = new kony.ui.Label({
                "height": "21dp",
                "id": "lblPaymentDate",
                "isVisible": true,
                "left": "22dp",
                "skin": "sknPaymentDate14pxBlack",
                "text": "Payment Date",
                "top": "0dp",
                "width": "91dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount = new kony.ui.Label({
                "height": "21dp",
                "id": "lblAmount",
                "isVisible": true,
                "left": "150dp",
                "skin": "CopydefLabel0a1c18abb82e548",
                "text": "Amount",
                "top": "0dp",
                "width": "51dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPrincipal = new kony.ui.Label({
                "height": "21dp",
                "id": "lblPrincipal",
                "isVisible": true,
                "left": "275dp",
                "skin": "CopydefLabel0ba975c405e2243",
                "text": "Principal",
                "top": "0dp",
                "width": "58dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInterest = new kony.ui.Label({
                "height": "21dp",
                "id": "lblInterest",
                "isVisible": true,
                "left": "400dp",
                "skin": "CopydefLabel0ib9513418e894b",
                "text": "Interest",
                "top": "0dp",
                "width": "51dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTax = new kony.ui.Label({
                "height": "21dp",
                "id": "lblTax",
                "isVisible": true,
                "left": "490dp",
                "skin": "CopydefLabel0cbd5cbba4be547",
                "text": "Tax",
                "top": "0dp",
                "width": "51dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTotoutstanding = new kony.ui.Label({
                "height": "21dp",
                "id": "lblTotoutstanding",
                "isVisible": true,
                "left": "620dp",
                "skin": "CopydefLabel0f3bfc6cdfe724c",
                "text": "Total Outstanding",
                "top": "0dp",
                "width": "116dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segRevised = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "data": [{
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }],
                "groupCells": false,
                "height": "210dp",
                "id": "segRevised",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 20,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "Flex0i494021d9e244d",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "30dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "Flex0i494021d9e244d": "Flex0i494021d9e244d",
                    "flxScheduleheader": "flxScheduleheader",
                    "lblAmount": "lblAmount",
                    "lblInterest": "lblInterest",
                    "lblPaymentDate": "lblPaymentDate",
                    "lblPrincipal": "lblPrincipal",
                    "lblTax": "lblTax",
                    "lbltotaloutstanding": "lbltotaloutstanding"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRevisedPaymentDetails.add(lblPaymentDate, lblAmount, lblPrincipal, lblInterest, lblTax, lblTotoutstanding, segRevised);
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "340dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var btnPrev = new kony.ui.Image2({
                "height": "17dp",
                "id": "btnPrev",
                "isVisible": true,
                "left": "655dp",
                "skin": "slImage",
                "src": "arrow_back.png",
                "top": "10dp",
                "width": "19dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext = new kony.ui.Image2({
                "height": "17dp",
                "id": "btnNext",
                "isVisible": true,
                "left": "673dp",
                "skin": "slImage",
                "src": "arrow_forward.png",
                "top": "10dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPage = new kony.ui.Label({
                "id": "lblSearchPage",
                "isVisible": true,
                "left": "548dp",
                "skin": "CopydefLabel0b044b7d672a44a",
                "text": "1-6 of 18",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRowPerPageNumber = new kony.ui.Label({
                "id": "lblRowPerPageNumber",
                "isVisible": false,
                "left": "354dp",
                "skin": "CopydefLabel0bfa17407e4df49",
                "text": "Rows Per Page",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextField0a340c02fa20749 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "20dp",
                "id": "TextField0a340c02fa20749",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "494dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ca88ed6f06d246",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxPagination.add(btnPrev, btnNext, lblSearchPage, lblRowPerPageNumber, TextField0a340c02fa20749);
            flxRevisedPayment.add(flxRevisedPmtHeader, flxRevisedPaymentDetails, flxPagination);
            flxRevisedPaymentSchedule.add(flxRevisedPayment);
            flxViewSchedule.add(flxRevisedPaymentSchedule);
            var searchPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "searchPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0g815ca6a691d4d",
                "top": "0",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            searchPopup.setDefaultUnit(kony.flex.DP);
            var FlexContainer0c8da61cffdb244 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "500dp",
                "id": "FlexContainer0c8da61cffdb244",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0e5562f0a71eb42",
                "top": "0",
                "width": "60%"
            }, {}, {});
            FlexContainer0c8da61cffdb244.setDefaultUnit(kony.flex.DP);
            var popupPayinPayout = new com.konymp.popupPayinPayout.popupPayinPayout({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popupPayinPayout",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0e816618aedef47",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxInnerSearch": {
                        "height": "400dp"
                    },
                    "imgClose1": {
                        "src": "ico_close.png"
                    },
                    "imgSrc1": {
                        "src": "search.png"
                    },
                    "popupPayinPayout": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            FlexContainer0c8da61cffdb244.add(popupPayinPayout);
            searchPopup.add(FlexContainer0c8da61cffdb244);
            var PaymentConfirm = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "PaymentConfirm",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            PaymentConfirm.setDefaultUnit(kony.flex.DP);
            PaymentConfirm.add();
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20,
                "overrides": {
                    "ProgressIndicator": {
                        "isVisible": false,
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "tbxPayOutAccount.downArrowLbl": {
                    "zIndex": 5
                },
                "tbxPayOutAccount": {
                    "centerX": "",
                    "height": "40dp",
                    "left": "0dp",
                    "top": "180dp",
                    "width": "230dp",
                    "zIndex": 1
                },
                "tbxPayOutAccount.lblFloatLabel": {
                    "left": "5%",
                    "text": "Pay-out Account "
                },
                "tbxPayOutAccount.txtFloatText": {
                    "centerY": "49.19%",
                    "left": "0dp",
                    "width": "100%"
                },
                "tbxPayInAccount.downArrowLbl": {
                    "zIndex": 5
                },
                "tbxPayInAccount": {
                    "centerX": "",
                    "height": "40dp",
                    "left": "28.84%",
                    "top": "180dp",
                    "width": "253dp",
                    "zIndex": 1
                },
                "tbxPayInAccount.lblFloatLabel": {
                    "left": "5%",
                    "text": "Pay-in Account "
                },
                "tbxPayInAccount.txtFloatText": {
                    "centerY": "50%",
                    "left": "0dp"
                },
                "AccountList": {
                    "height": "235dp",
                    "width": "100%"
                },
                "AccountList.segAccountList": {
                    "height": "190dp",
                    "top": "40dp"
                },
                "AccountList2": {
                    "bottom": "0dp",
                    "height": "235dp"
                },
                "AccountList2.segAccountList": {
                    "height": "195dp",
                    "left": "0",
                    "top": "0"
                },
                "ErrorAllert": {
                    "left": "6.50%",
                    "width": "92.50%",
                    "zIndex": 1
                },
                "ErrorAllert.imgCloseBackup": {
                    "src": "ico_close.png"
                },
                "popupPayinPayout.flxInnerSearch": {
                    "height": "400dp"
                },
                "popupPayinPayout.imgClose1": {
                    "src": "ico_close.png"
                },
                "popupPayinPayout.imgSrc1": {
                    "src": "search.png"
                },
                "popupPayinPayout": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "height": "100%",
                    "left": "0dp",
                    "width": "100%"
                },
                "uuxNavigationRail": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMessageInfo, flxMainContainer, ErrorAllert, flxViewSchedule, searchPopup, PaymentConfirm, uuxNavigationRail, ProgressIndicator);
        };
        return [{
            "addWidgets": addWidgetsfrmDepositCreation,
            "enabledForIdleTimeout": false,
            "id": "frmDepositCreation",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1280, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});