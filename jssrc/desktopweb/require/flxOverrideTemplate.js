define("flxOverrideTemplate", function() {
    return function(controller) {
        var flxOverrideTemplate = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOverrideTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxOverrideTemplate.setDefaultUnit(kony.flex.DP);
        var lblSerial = new kony.ui.Label({
            "id": "lblSerial",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl16px000000BG",
            "text": "1",
            "top": "0dp",
            "width": "3%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 1, 0, 1],
            "paddingInPixel": false
        }, {});
        var lblInfo = new kony.ui.Label({
            "id": "lblInfo",
            "isVisible": true,
            "left": "10%",
            "skin": "sknLbl16px000000BG",
            "text": "Label",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 1, 0, 1],
            "paddingInPixel": false
        }, {});
        flxOverrideTemplate.add(lblSerial, lblInfo);
        return flxOverrideTemplate;
    }
})