define("CopyflxDown", function() {
    return function(controller) {
        var CopyflxDown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "CopyflxDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyCopyslFbox5",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        CopyflxDown.setDefaultUnit(kony.flex.DP);
        var FlexGroup0d7f2ca3295ba45 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "FlexGroup0d7f2ca3295ba45",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0j9fbcb48b22443",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        FlexGroup0d7f2ca3295ba45.setDefaultUnit(kony.flex.DP);
        var lblShelves = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblShelves",
            "isVisible": true,
            "left": "1%",
            "skin": "CopyCopydefLabel5",
            "text": "Kony ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgLocation = new kony.ui.Image2({
            "centerY": "50%",
            "height": "25dp",
            "id": "imgLocation",
            "isVisible": false,
            "right": "1dp",
            "skin": "slImage",
            "src": "blue_downarrow_2.png",
            "width": "25dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDown = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDown",
            "isVisible": true,
            "right": "10dp",
            "skin": "CopydefLabel0fd64b1ed49de46",
            "text": "J",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTitle = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "CopydefBtnNormal0bdd421c02b9f4e",
            "height": "20dp",
            "id": "lblTitle",
            "isVisible": true,
            "left": "25%",
            "onClick": controller.AS_Button_f42c7b756fbe4e39b96d71ba572d6458,
            "skin": "CopydefBtnNormal0bdd421c02b9f4e",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnUpArrow = new kony.ui.Button({
            "focusSkin": "CopydefBtnNormal0c52cc0f8f66644",
            "height": "100%",
            "id": "btnUpArrow",
            "isVisible": true,
            "onClick": controller.AS_Button_j53dfc8b7d164d138b81f33a7be07466,
            "right": "0dp",
            "skin": "CopydefBtnNormal0c52cc0f8f66644",
            "top": "0dp",
            "width": "35dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        FlexGroup0d7f2ca3295ba45.add(lblShelves, imgLocation, lblDown, lblTitle, btnUpArrow);
        CopyflxDown.add(FlexGroup0d7f2ca3295ba45);
        return CopyflxDown;
    }
})