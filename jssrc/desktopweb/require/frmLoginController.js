define("userfrmLoginController", {
    userRole: '',
    roleResponce: '',
    userSignOnName: '',
    userpassword: '123456',
    //Type your controller code here 
    onNavigate: function() {
        this.bindEvents();
        this.view.custSelectUser.value = "none";
        this.view.custPassword.value = "";
    },
    bindEvents: function() {
        this.view.preShow = this.Preshow;
        // this.postShow = this.postShowAccoutDetails;
        this.view.custLogin.onclickEvent = this.navToHomefunc1;
        this.view.custSelectUser.valueChanged = this.getusername;
        this.view.lblUrl.onTouchEnd = this.navigateTemenosPage;
        //this.view.DPWSelectLoan.valueChanged = this.navigateToCreateLoan;
    },
    Preshow: function() {
        this.view.ProgressIndicator.isVisible = true;
        this.getusers();
        this.getTransactDate();
        this.bindEvents();
    },
    navigateTemenosPage: function() {
        kony.application.openURL("http://www.temenos.com");
    },
    getusers: function() {
        MFHelper.Utils.commonMFServiceCall("LendingNew", "getUsername", "", "", this.successCBUser, this.failureCBUser);
    },
    successCBUser: function(response) {
        this.roleResponce = response;
        var res = response.body.length;
        userArray = [{
            "label": "Select",
            "value": "none"
        }];
        for (i = 0; i < res; i++) {
            userArray.push({
                label: response.body[i].userName,
                value: response.body[i].userId
            });
            //
        }
        this.view.custSelectUser.options = JSON.stringify(userArray);
        this.view.ProgressIndicator.isVisible = false;
    },
    failureCBUser: function(response) {
        this.view.ProgressIndicator.isVisible = false;
        alert(JSON.stringify(response));
    },
    getusername: function() {
        this.userRole = this.view.custSelectUser.value;
        if (this.userRole !== "" && this.userRole !== "none") {
            var res = this.roleResponce;
            // alert(JSON.stringify(res));
            var indexval = res.body.findIndex(this.findArrayindex);
            var topcustomer = res.body[indexval].userSignOnName;
            this.userSignOnName = topcustomer + "/" + this.userpassword;
        }
    },
    findArrayindex: function(res1) {
        return res1.userId === this.userRole;
    },
    navToHomefunc1: function() {
        var selectval = this.view.custSelectUser.value;
        if (selectval !== "" && selectval !== "none") {
            gblDashboardObj = "";
            gblAllaccountWidgetDate = "";
            gblAllaccountarray = "";
            var navToHome = new kony.mvc.Navigation("frmDashboardLending");
            var navObjet = {};
            navObjet.customerId = "";
            navObjet.clientId = "";
            navObjet.userRole = this.userRole;
            navObjet.userSignOnName = this.userSignOnName;
            navToHome.navigate(navObjet);
        } else {
            alert("Please Select the User Role");
        }
    },
    getTransactDate: function() {
        MFHelper.Utils.commonMFServiceCall("LendingNew", "getTransactDate", "", "", this.successCBDate, this.failureCBDate);
    },
    successCBDate: function(response) {
        //alert(JSON.stringify(response.body[0].currentWorkingDate));
        transactDate = response.body[0].currentWorkingDate;
        kony.print("Today's T24 Date ==" + transactDate);
        //alert(getCurrentdate);
    },
    failureCBDate: function(response) {
        this.view.ProgressIndicator.isVisible = false;
        //alert(JSON.stringify(response));
    }
});
define("frmLoginControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for frmLogin **/
    AS_Form_j0579fc5bf0f484f845fda0d79d7adbb: function AS_Form_j0579fc5bf0f484f845fda0d79d7adbb(eventobject) {
        var self = this;
        kony.print("'ibnpreshow of frmlogin");
    }
});
define("frmLoginController", ["userfrmLoginController", "frmLoginControllerActions"], function() {
    var controller = require("userfrmLoginController");
    var controllerActions = ["frmLoginControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
