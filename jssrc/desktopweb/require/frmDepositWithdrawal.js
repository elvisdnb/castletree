define("frmDepositWithdrawal", function() {
    return function(controller) {
        function addWidgetsfrmDepositWithdrawal() {
            this.setDefaultUnit(kony.flex.DP);
            var flxNavigationRail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNavigationRail",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "zIndex": 10
            }, {}, {});
            flxNavigationRail.setDefaultUnit(kony.flex.DP);
            var cusNavigateRail = new kony.ui.CustomWidget({
                "id": "cusNavigateRail",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "64dp",
                "height": "968dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxNavigationRail",
                "bottomBtnIcon": "",
                "bottomBtnItems": "",
                "hideBottomBtn": false,
                "logoAlt": "Temenos logo",
                "menuItems": "",
                "positionValue": {
                    "optionsList": ["left", "right"],
                    "selectedValue": "left"
                }
            });
            flxNavigationRail.add(cusNavigateRail);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0c444a067f87d41",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "right": "32dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": true,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": true,
                "right": "168dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.CustomWidget({
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "0dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopysknLbl0e711f55de4794e",
                "text": "Home",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopysknLbl0c3756e5a60504a",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "8dp",
                "skin": "CopysknLbl5",
                "text": "Overview",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "96%",
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "left": "1%",
                        "right": "viz.val_cleared",
                        "width": "96%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "1%",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "70dp",
                "width": "96%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxWithdrawalContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "793dp",
                "id": "flxWithdrawalContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "227dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxWithdrawalContainer.setDefaultUnit(kony.flex.DP);
            var MainTabs = new com.lending.MainTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "MainTabs": {
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var FundDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "129dp",
                "id": "FundDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "64dp",
                "width": "100%",
                "overrides": {
                    "FlexContainer0ebe191bc1cf946": {
                        "left": "-144dp"
                    },
                    "flxListBoxSelectedValue": {
                        "width": "160dp"
                    },
                    "flxLoanDetailsvalue": {
                        "left": "0dp"
                    },
                    "lblAccountNumberResponse": {
                        "width": "160dp"
                    },
                    "lblAccountServiceInfo": {
                        "text": "Deposit Services"
                    },
                    "lblCurrencyValueInfo": {
                        "left": "20dp",
                        "width": "96dp"
                    },
                    "lblCurrencyValueResponse": {
                        "width": "96dp"
                    },
                    "lblListBoxSelectedValue": {
                        "text": "Withdraw Deposit"
                    },
                    "lblLoanTypeInfo": {
                        "left": "16dp",
                        "text": "Account  Type",
                        "width": "168dp"
                    },
                    "lblLoanTypeResponse": {
                        "left": "16dp",
                        "width": "168dp"
                    },
                    "lblMaturityDateInfo": {
                        "width": "136dp"
                    },
                    "lblMaturityDateResponse": {
                        "width": "136dp"
                    },
                    "lblStartDateInfo": {
                        "width": "136dp"
                    },
                    "lblStartDateResponse": {
                        "width": "136dp"
                    },
                    "loanDetails": {
                        "left": "0dp",
                        "top": "64dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxFundWithdrawalWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "600dp",
                "id": "flxFundWithdrawalWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "32dp",
                "skin": "CopysknFlxBorderCCE1",
                "top": "193dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFundWithdrawalWrapper.setDefaultUnit(kony.flex.DP);
            var lblFundWithdrawInfo = new kony.ui.Label({
                "height": "20dp",
                "id": "lblFundWithdrawInfo",
                "isVisible": true,
                "left": "120dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "Withdraw Deposit",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLine1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopysknSeparatorGrey",
                "top": "90dp",
                "zIndex": 1
            }, {}, {});
            flxLine1.setDefaultUnit(kony.flex.DP);
            flxLine1.add();
            var flxWithdrawType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44dp",
                "id": "flxWithdrawType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "120dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "125dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxWithdrawType.setDefaultUnit(kony.flex.DP);
            var lblSelectModeInfo = new kony.ui.Label({
                "id": "lblSelectModeInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopyCopysknLblBg",
                "text": "Withdrawal type",
                "top": "11dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusRadioWithdrawalType = new kony.ui.CustomWidget({
                "id": "cusRadioWithdrawalType",
                "isVisible": true,
                "left": "44dp",
                "top": "0dp",
                "width": "70%",
                "height": "40dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRadioGroup",
                "labelText": "",
                "options": {
                    "selectedValue": ""
                },
                "stacked": false,
                "value": "1"
            });
            flxWithdrawType.add(lblSelectModeInfo, cusRadioWithdrawalType);
            var cusDatePickerWithdrawalDate = new kony.ui.CustomWidget({
                "id": "cusDatePickerWithdrawalDate",
                "isVisible": true,
                "left": "120dp",
                "top": "195dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Withdrawal Date",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "value": "",
                "weekLabel": "Wk"
            });
            var flxPartialWithdrawal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "164dp",
                "id": "flxPartialWithdrawal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "120dp",
                "isModalContainer": false,
                "right": "32dp",
                "skin": "slFbox",
                "top": "195dp",
                "zIndex": 1
            }, {}, {});
            flxPartialWithdrawal.setDefaultUnit(kony.flex.DP);
            var flxAmountPartialWithdrawal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "42dp",
                "id": "flxAmountPartialWithdrawal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "304dp",
                "zIndex": 1
            }, {}, {});
            flxAmountPartialWithdrawal.setDefaultUnit(kony.flex.DP);
            var cusTextFieldAmount = new kony.ui.CustomWidget({
                "id": "cusTextFieldAmount",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": false,
                "endAligned": false,
                "labelDescription": "Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": false,
                "required": "required",
                "suffix": "",
                "validateRequired": "required",
                "value": "0.00"
            });
            flxAmountPartialWithdrawal.add(cusTextFieldAmount);
            var cusTextFieldPrincipalAmnt = new kony.ui.CustomWidget({
                "id": "cusTextFieldPrincipalAmnt",
                "isVisible": true,
                "left": "0dp",
                "top": "90dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "Principal after Withdrawal",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "required": "required",
                "suffix": "",
                "value": "0.00"
            });
            var cusTextFieldWithdrawalFee = new kony.ui.CustomWidget({
                "id": "cusTextFieldWithdrawalFee",
                "isVisible": true,
                "left": "680dp",
                "top": "0dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "Withdrawal Fee",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "required": "required",
                "suffix": "",
                "value": "0.00"
            });
            var cusTextFieldOriginalInterest = new kony.ui.CustomWidget({
                "id": "cusTextFieldOriginalInterest",
                "isVisible": true,
                "left": "340dp",
                "top": "90dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "Original Interest at Maturity",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "required": "required",
                "suffix": "",
                "value": "0.00"
            });
            var cusTextFieldNewInterest = new kony.ui.CustomWidget({
                "id": "cusTextFieldNewInterest",
                "isVisible": true,
                "left": "680dp",
                "top": "90dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "New Interest at Maturity",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "required": "required",
                "suffix": "",
                "value": "0.00"
            });
            flxPartialWithdrawal.add(flxAmountPartialWithdrawal, cusTextFieldPrincipalAmnt, cusTextFieldWithdrawalFee, cusTextFieldOriginalInterest, cusTextFieldNewInterest);
            var flxFullWithdrawal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFullWithdrawal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "460dp",
                "isModalContainer": false,
                "right": "80dp",
                "skin": "slFbox",
                "top": "195dp",
                "zIndex": 1
            }, {}, {});
            flxFullWithdrawal.setDefaultUnit(kony.flex.DP);
            var cusTextFieldRedemptionFee = new kony.ui.CustomWidget({
                "id": "cusTextFieldRedemptionFee",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "Redemption Fee",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "required": "required",
                "suffix": "",
                "validateRequired": "",
                "value": "0.00"
            });
            var cusTextFieldFullWithdralAmount = new kony.ui.CustomWidget({
                "id": "cusTextFieldFullWithdralAmount",
                "isVisible": true,
                "left": "340dp",
                "top": "0dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxAmountComponent",
                "currencyCode": "",
                "decimals": 2,
                "dense": true,
                "disabled": true,
                "endAligned": false,
                "labelDescription": "Amount",
                "locale": "en-US",
                "placeholder": "",
                "prefix": "",
                "readonly": true,
                "required": "required",
                "suffix": "",
                "value": "0.00"
            });
            flxFullWithdrawal.add(cusTextFieldRedemptionFee, cusTextFieldFullWithdralAmount);
            var flxLine2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "120dp",
                "isModalContainer": false,
                "right": "120dp",
                "skin": "CopysknSeparatorGrey",
                "top": "380dp",
                "zIndex": 1
            }, {}, {});
            flxLine2.setDefaultUnit(kony.flex.DP);
            flxLine2.add();
            var flxCommonFields = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "48dp",
                "id": "flxCommonFields",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "120dp",
                "isModalContainer": false,
                "right": "32dp",
                "skin": "slFbox",
                "top": "408dp",
                "zIndex": 1
            }, {}, {});
            flxCommonFields.setDefaultUnit(kony.flex.DP);
            var tempModeOfSettlement1 = new com.lending.floatingReadOnlyText.floatReadOnlylTextCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "56dp",
                "id": "tempModeOfSettlement1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopysknFlxWhiteBg",
                "top": "0dp",
                "width": "248dp",
                "zIndex": 4,
                "overrides": {
                    "floatReadOnlylTextCopy": {
                        "centerX": "viz.val_cleared",
                        "height": "56dp",
                        "isVisible": false,
                        "left": "0dp",
                        "top": "0dp",
                        "width": "248dp"
                    },
                    "lblFloatLabel": {
                        "text": "Mode of Settlement"
                    },
                    "txtFloatText": {
                        "text": "Account Transfer"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var tempModeOfSettlement = new com.lending.floatingTextBox.floatLabelTextCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "56dp",
                "id": "tempModeOfSettlement",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopysknFlxWhiteBg",
                "top": "0dp",
                "width": "304dp",
                "zIndex": 4,
                "overrides": {
                    "floatLabelTextCopy": {
                        "centerX": "viz.val_cleared",
                        "height": "56dp",
                        "isVisible": false,
                        "width": "304dp"
                    },
                    "lblFloatLabel": {
                        "left": "5%",
                        "text": "Mode of Settlement"
                    },
                    "rtAsterix": {
                        "isVisible": false
                    },
                    "txtFloatText": {
                        "text": "Account Transfer"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var cusTextBoxModeOfSettlement = new kony.ui.CustomWidget({
                "id": "cusTextBoxModeOfSettlement",
                "isVisible": true,
                "left": "0dp",
                "top": "0dp",
                "width": "304dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Mode of Settlement",
                "maxLength": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": "Account Transfer"
            });
            var flxModeOfSettlementDownArrow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "48dp",
                "id": "flxModeOfSettlementDownArrow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "245dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 4
            }, {}, {});
            flxModeOfSettlementDownArrow1.setDefaultUnit(kony.flex.DP);
            var imgDownArrow1 = new kony.ui.Image2({
                "height": "36dp",
                "id": "imgDownArrow1",
                "isVisible": false,
                "left": "0dp",
                "skin": "slImage",
                "src": "drop_down_arrow_1.png",
                "top": "10dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var downArrowLbl = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "downArrowLbl",
                "isVisible": true,
                "right": "23%",
                "skin": "Copycastleicon",
                "text": "c",
                "width": "40dp",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxModeOfSettlementDownArrow1.add(imgDownArrow1, downArrowLbl);
            var tempSettlementAccount = new com.lending.floatingTextBox.floatLabelTextCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "42dp",
                "id": "tempSettlementAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopysknFlxWhiteBg",
                "top": "0dp",
                "width": "260dp",
                "zIndex": 4,
                "overrides": {
                    "floatLabelTextCopy": {
                        "centerX": "viz.val_cleared",
                        "height": "42dp",
                        "left": "340dp",
                        "top": "0dp",
                        "width": "260dp"
                    },
                    "flxBgColor": {
                        "height": "100%"
                    },
                    "lblFloatLabel": {
                        "left": "5%",
                        "text": "Settlement Account"
                    },
                    "rtAsterix": {
                        "isVisible": false
                    },
                    "txtFloatText": {
                        "centerY": "50%",
                        "height": "100%",
                        "left": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSettlementAccountDropDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSettlementAccountDropDown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "554dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 4
            }, {}, {});
            flxSettlementAccountDropDown.setDefaultUnit(kony.flex.DP);
            var imgDownArrow2 = new kony.ui.Image2({
                "height": "36dp",
                "id": "imgDownArrow2",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "drop_down_arrow_1.png",
                "top": "5dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSettlementAccountDropDown.add(imgDownArrow2);
            var flxSettlementAccountSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSettlementAccountSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "600dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 4
            }, {}, {});
            flxSettlementAccountSearch.setDefaultUnit(kony.flex.DP);
            var imgSettlementAccountSearch = new kony.ui.Image2({
                "height": "36dp",
                "id": "imgSettlementAccountSearch",
                "isVisible": false,
                "left": "0dp",
                "skin": "slImage",
                "src": "search_1.png",
                "top": "10dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusIconSettlementAccountSearch = new kony.ui.CustomWidget({
                "id": "cusIconSettlementAccountSearch",
                "isVisible": false,
                "left": "0dp",
                "top": "5dp",
                "width": "100%",
                "height": "100%",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "search",
                "size": "medium"
            });
            var CopydownArrowLbl0b550f1dd544b4f = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "CopydownArrowLbl0b550f1dd544b4f",
                "isVisible": true,
                "right": "5%",
                "skin": "Copycastleicon",
                "text": "k",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSettlementAccountSearch.add(imgSettlementAccountSearch, cusIconSettlementAccountSearch, CopydownArrowLbl0b550f1dd544b4f);
            var flxViewBillDetailsLink = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "42dp",
                "id": "flxViewBillDetailsLink",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "680dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "304dp",
                "zIndex": 4
            }, {}, {});
            flxViewBillDetailsLink.setDefaultUnit(kony.flex.DP);
            var cusIconRefreshViewDetails = new kony.ui.CustomWidget({
                "id": "cusIconRefreshViewDetails",
                "isVisible": true,
                "left": "0dp",
                "top": "5dp",
                "width": "36dp",
                "height": "36dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "grey-light",
                "iconName": "cached",
                "size": "medium"
            });
            var lblViewDetailsLink = new kony.ui.Label({
                "centerY": "50%",
                "height": "24dp",
                "id": "lblViewDetailsLink",
                "isVisible": true,
                "left": "48dp",
                "skin": "skinLabelLinkDisableFont333333",
                "text": "View Bill Details",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewBillDetailsLink.add(cusIconRefreshViewDetails, lblViewDetailsLink);
            flxCommonFields.add(tempModeOfSettlement1, tempModeOfSettlement, cusTextBoxModeOfSettlement, flxModeOfSettlementDownArrow1, tempSettlementAccount, flxSettlementAccountDropDown, flxSettlementAccountSearch, flxViewBillDetailsLink);
            var lblErrorMessge = new kony.ui.Label({
                "id": "lblErrorMessge",
                "isVisible": false,
                "left": "120dp",
                "skin": "CopysknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "480dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "50dp",
                "clipBounds": false,
                "height": "43dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "6%",
                "skin": "slFbox",
                "top": "500dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var cusButtonCancel = new kony.ui.CustomWidget({
                "id": "cusButtonCancel",
                "isVisible": true,
                "right": "200dp",
                "top": "1dp",
                "width": "184dp",
                "height": "45dp",
                "minWidth": 184,
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusButtonConfirm = new kony.ui.CustomWidget({
                "id": "cusButtonConfirm",
                "isVisible": true,
                "right": "0dp",
                "top": "1dp",
                "width": "184dp",
                "height": "45dp",
                "minWidth": 184,
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "X-large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxButtons.add(cusButtonCancel, cusButtonConfirm);
            var flxDownArrowList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "134dp",
                "id": "flxDownArrowList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "464dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "454dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxDownArrowList.setDefaultUnit(kony.flex.DP);
            var AccountList2 = new com.konymp.AccountList.AccountList({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "240dp",
                "id": "AccountList2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxWhiteBGBorder0075db",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDownArrowList.add(AccountList2);
            flxFundWithdrawalWrapper.add(lblFundWithdrawInfo, flxLine1, flxWithdrawType, cusDatePickerWithdrawalDate, flxPartialWithdrawal, flxFullWithdrawal, flxLine2, flxCommonFields, lblErrorMessge, flxButtons, flxDownArrowList);
            flxWithdrawalContainer.add(MainTabs, FundDetails, flxFundWithdrawalWrapper);
            flxMainContainer.add(flxHeaderMenu, ErrorAllert, custInfo, flxWithdrawalContainer);
            var flxDropDownAccountListOutsideWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDropDownAccountListOutsideWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2.86%",
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDropDownAccountListOutsideWrapper.setDefaultUnit(kony.flex.DP);
            var flxDropDownAccountListInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxDropDownAccountListInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "CopysknFlxOutstanding",
                "width": "65%",
                "zIndex": 1
            }, {}, {});
            flxDropDownAccountListInnerWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblHeaderInfo = new kony.ui.Label({
                "id": "lblHeaderInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknLbl1",
                "text": "Accounts List",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxHeaderSegmentOutstandingClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxHeaderSegmentOutstandingClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "19dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderSegmentOutstandingClose.setDefaultUnit(kony.flex.DP);
            var imgIconClose = new kony.ui.Image2({
                "height": "100%",
                "id": "imgIconClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "ico_close_1.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSegmentOutstandingClose.add(imgIconClose);
            flxHeaderInfo.add(lblHeaderInfo, flxHeaderSegmentOutstandingClose);
            var flxSegmentHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSegmentHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxSegmentHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblAccountIDInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblAccountIDInfo",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Account ID",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerIDInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblCustomerIDInfo",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Customer ID",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProductInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblProductInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Product",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCCYInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblCCYInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "CCY",
                "top": "2dp",
                "width": "12%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUsableBalanceInfo = new kony.ui.Label({
                "height": "26dp",
                "id": "lblUsableBalanceInfo",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Usable Balance",
                "top": "2dp",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentHeaderInfo.add(lblAccountIDInfo, lblCustomerIDInfo, lblProductInfo, lblCCYInfo, lblUsableBalanceInfo);
            var flxAccountsSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountsSegmentWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "8dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxAccountsSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var segAccountsList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }, {
                    "lblAccID": "Account ID",
                    "lblCCY": "CCY",
                    "lblCustomerID": "Customer ID",
                    "lblMnemonic": "Mnenonic",
                    "lblProduct": "Product",
                    "lblUsableBalance": "Usable Balance",
                    "lblUsableBlance": "Usable Balance"
                }],
                "groupCells": false,
                "id": "segAccountsList",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxSegAccNoList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxSegAccNoList": "flxSegAccNoList",
                    "lblAccID": "lblAccID",
                    "lblCCY": "lblCCY",
                    "lblCustomerID": "lblCustomerID",
                    "lblMnemonic": "lblMnemonic",
                    "lblProduct": "lblProduct",
                    "lblUsableBalance": "lblUsableBalance",
                    "lblUsableBlance": "lblUsableBlance"
                },
                "width": "100%",
                "zIndex": 2
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountsSegmentWrapper.add(segAccountsList);
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "34dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var lblPaginationInfo = new kony.ui.Label({
                "height": "40dp",
                "id": "lblPaginationInfo",
                "isVisible": false,
                "right": "270dp",
                "skin": "Copysknlbl4",
                "text": "Rows per page :",
                "top": "0dp",
                "width": "103dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtDataPerPageNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "txtDataPerPageNumber",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "placeholder": "Placeholder",
                "right": "220dp",
                "secureTextEntry": false,
                "skin": "CopysknTxt",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var lblSearchPage = new kony.ui.Label({
                "height": "40dp",
                "id": "lblSearchPage",
                "isVisible": true,
                "right": "140dp",
                "skin": "Copysknlbl4",
                "text": "1-2 of 2",
                "top": "0dp",
                "width": "70dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPrev = new kony.ui.Button({
                "height": "40dp",
                "id": "btnPrev",
                "isVisible": true,
                "right": "90dp",
                "skin": "CopysknBtnDisable",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext = new kony.ui.Button({
                "height": "40dp",
                "id": "btnNext",
                "isVisible": true,
                "right": "50dp",
                "skin": "CopysknBtnFocus",
                "text": ">",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPagination.add(lblPaginationInfo, txtDataPerPageNumber, lblSearchPage, btnPrev, btnNext);
            var flxDummyBelowOutstandingRepay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDummyBelowOutstandingRepay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDummyBelowOutstandingRepay.setDefaultUnit(kony.flex.DP);
            flxDummyBelowOutstandingRepay.add();
            flxDropDownAccountListInnerWrapper.add(flxHeaderInfo, flxSegmentHeaderInfo, flxAccountsSegmentWrapper, flxPagination, flxDummyBelowOutstandingRepay);
            flxDropDownAccountListOutsideWrapper.add(flxDropDownAccountListInnerWrapper);
            var flxSearchAccountDuplicate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchAccountDuplicate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxSearchAccountDuplicate.setDefaultUnit(kony.flex.DP);
            var flxSearchInnerWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxSearchInnerWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "CopysknFlxOutstanding",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxSearchInnerWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderInfoSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxHeaderInfoSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxHeaderInfoSearch.setDefaultUnit(kony.flex.DP);
            var lblAccount = new kony.ui.Label({
                "id": "lblAccount",
                "isVisible": true,
                "left": "0dp",
                "skin": "CopysknLbl1",
                "text": "Accounts List",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxHeaderSearchOutstandingClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxHeaderSearchOutstandingClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "19dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderSearchOutstandingClose.setDefaultUnit(kony.flex.DP);
            var imgIconCloseSearch = new kony.ui.Image2({
                "height": "100%",
                "id": "imgIconCloseSearch",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "ico_close_1.png",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSearchOutstandingClose.add(imgIconCloseSearch);
            flxHeaderInfoSearch.add(lblAccount, flxHeaderSearchOutstandingClose);
            var flxAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxAccounts.setDefaultUnit(kony.flex.DP);
            var lblAccounts = new kony.ui.Label({
                "height": "26dp",
                "id": "lblAccounts",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Accounts",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServices = new kony.ui.ListBox({
                "centerY": "50.00%",
                "height": "100%",
                "id": "lblServices",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["select", "Please Select"],
                    ["AccountEquals", "Equals"],
                    ["AccountMatches", "Matches"],
                    ["AccountNotMatches", "Not Matches"],
                    ["AccountNotEquals", "Not Equals"],
                    ["AccountGreaterThan", "Greater Than"],
                    ["AccountGreaterOrEqual", "Greater Than Or Equal"],
                    ["AccountLessThan", "Less Than"],
                    ["AccountLessOrEqual", "Less Than Or Equal"],
                    ["AccountBetween", "Between"],
                    ["AccountNotBetween", "Not Between"],
                    ["AccountContain", "Contains"],
                    ["AccountNotContain", "Not Containing"],
                    ["AccountBeginsWith", "Begins With"],
                    ["AccountEndsWith", "Ends With"],
                    ["AccountDoesNotBegin", "Does Not Begins With"],
                    ["AccountDoesNotEnd", "Does Not Ends With"]
                ],
                "selectedKey": "select",
                "skin": "CopyCopydefListBoxNormal",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var txtAccount = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "100%",
                "id": "txtAccount",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5dp",
                "secureTextEntry": false,
                "skin": "CopysknTxtBoxNormal1",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "37%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxAccounts.add(lblAccounts, lblServices, txtAccount);
            var flxCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxCustomer.setDefaultUnit(kony.flex.DP);
            var lblCustomer = new kony.ui.Label({
                "height": "26dp",
                "id": "lblCustomer",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Customer",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstCustomer = new kony.ui.ListBox({
                "centerY": "50.00%",
                "height": "100%",
                "id": "lstCustomer",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["select", "Please Select"],
                    ["CustomerEquals", "Equals"],
                    ["CustomerMatches", "Matches"],
                    ["CustomerNotMatches", "Not Matches"],
                    ["CustomerNotEquals", "Not Equals"],
                    ["CustomerGreaterThan", "Greater Than"],
                    ["CustomerGreaterOrEqual", "Greater Than Or Equal"],
                    ["CustomerLessThan", "Less Than"],
                    ["CustomerLessOrEqual", "Less Than Or Equal"],
                    ["CustomerBetween", "Between"],
                    ["CustomerNotBetween", "Not Between"],
                    ["CustomerContain", "Contains"],
                    ["CustomerNotContain", "Not Containing"],
                    ["CustomerBeginsWith", "Begins With"],
                    ["CustomerEndsWith", "Ends With"],
                    ["CustomerDoesNotBegin", "Does Not Begins With"],
                    ["CustomerDoesNotEnd", "Does Not Ends With"]
                ],
                "selectedKey": "select",
                "skin": "CopyCopydefListBoxNormal",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var txtCustomer = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "100%",
                "id": "txtCustomer",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5dp",
                "secureTextEntry": false,
                "skin": "CopysknTxtBoxNormal1",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "37%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxCustomer.add(lblCustomer, lstCustomer, txtCustomer);
            var flxCurrency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCurrency",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "23dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxCurrency.setDefaultUnit(kony.flex.DP);
            var lblCurrency = new kony.ui.Label({
                "height": "26dp",
                "id": "lblCurrency",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Currency",
                "top": "2dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstCurrency = new kony.ui.ListBox({
                "centerY": "50.00%",
                "height": "100%",
                "id": "lstCurrency",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["select", "Please Select"],
                    ["CurrencyEquals", "Equals"],
                    ["CurrencyMatches", "Matches"],
                    ["CurrencyNotMatches", "Not Matches"],
                    ["CurrencyNotEquals", "Not Equals"],
                    ["CurrencyGreaterThan", "Greater Than"],
                    ["CurrencyGreaterOrEqual", "Greater Than Or Equal"],
                    ["CurrencyLessThan", "Less Than"],
                    ["CurrencyLessOrEqual", "Less Than Or Equal"],
                    ["CurrencyBetween", "Between"],
                    ["CurrencyNotBetween", "Not Between"],
                    ["CurrencyContain", "Contains"],
                    ["CurrencyNotContain", "Not Containing"],
                    ["CurrencyBeginsWith", "Begins With"],
                    ["CurrencyEndsWith", "Ends With"],
                    ["CurrencyDoesNotBegin", "Does Not Begins With"],
                    ["CurrencyDoesNotEnd", "Does Not Ends With"]
                ],
                "selectedKey": "select",
                "skin": "CopyCopydefListBoxNormal",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var txtCurrency = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "100%",
                "id": "txtCurrency",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5dp",
                "secureTextEntry": false,
                "skin": "CopysknTxtBoxNormal1",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "37%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxCurrency.add(lblCurrency, lstCurrency, txtCurrency);
            var flxOk = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxOk",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0%",
                "skin": "slFbox",
                "top": "10dp",
                "width": "147dp",
                "zIndex": 500
            }, {}, {});
            flxOk.setDefaultUnit(kony.flex.DP);
            var cusButtonOk = new kony.ui.CustomWidget({
                "id": "cusButtonOk",
                "isVisible": true,
                "left": 0,
                "right": "0%",
                "top": "0dp",
                "width": "179dp",
                "height": "100%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": " Confirm      ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxOk.add(cusButtonOk);
            var flxDummyBelow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDummyBelow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDummyBelow.setDefaultUnit(kony.flex.DP);
            flxDummyBelow.add();
            flxSearchInnerWrapper.add(flxHeaderInfoSearch, flxAccounts, flxCustomer, flxCurrency, flxOk, flxDummyBelow);
            flxSearchAccountDuplicate.add(flxSearchInnerWrapper);
            var OverrideSegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopupCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "OverrideSegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "SegmentPopupCopy": {
                        "height": "100%",
                        "isVisible": false
                    },
                    "flxPopup": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "left": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "imgClose": {
                        "src": "ico_close_1.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicatorCopy({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicatorCopy": {
                        "height": "100%",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxViewBillDetailsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewBillDetailsPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxViewBillDetailsPopup.setDefaultUnit(kony.flex.DP);
            var flxViewBillDetailsPopupWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxViewBillDetailsPopupWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "CopysknFlxOutstanding",
                "width": "48%",
                "zIndex": 1
            }, {}, {});
            flxViewBillDetailsPopupWrapper.setDefaultUnit(kony.flex.DP);
            var flxBillDetailsHeaderInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxBillDetailsHeaderInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "8dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsHeaderInfo.setDefaultUnit(kony.flex.DP);
            var lblBillDetailsPartial = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBillDetailsPartial",
                "isVisible": true,
                "left": "10dp",
                "skin": "CopysknLbl1",
                "text": "Bill Details",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusIconPrintBillDetails = new kony.ui.CustomWidget({
                "id": "cusIconPrintBillDetails",
                "isVisible": true,
                "left": "112dp",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 1, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "local_printshop",
                "size": "medium"
            });
            var flxBillDetailsIconClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxBillDetailsIconClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsIconClose.setDefaultUnit(kony.flex.DP);
            var cusBillDetailsIconClose = new kony.ui.CustomWidget({
                "id": "cusBillDetailsIconClose",
                "isVisible": false,
                "left": 0,
                "top": "0dp",
                "width": "50dp",
                "height": "50dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "highlight_off",
                "size": "large"
            });
            var imgBillDetailsClose1 = new kony.ui.Image2({
                "height": "50dp",
                "id": "imgBillDetailsClose1",
                "isVisible": false,
                "left": "0dp",
                "skin": "slImage",
                "src": "ico_close_1.png",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgBillDetailsClose = new kony.ui.Label({
                "height": "50dp",
                "id": "imgBillDetailsClose",
                "isVisible": true,
                "right": "0dp",
                "skin": "castleicon",
                "text": "H",
                "top": 0,
                "width": "50dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBillDetailsIconClose.add(cusBillDetailsIconClose, imgBillDetailsClose1, imgBillDetailsClose);
            flxBillDetailsHeaderInfo.add(lblBillDetailsPartial, cusIconPrintBillDetails, flxBillDetailsIconClose);
            var flxBillDetailsDivider = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxBillDetailsDivider",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "9dp",
                "isModalContainer": false,
                "skin": "CopysknLine",
                "top": "8dp",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsDivider.setDefaultUnit(kony.flex.DP);
            flxBillDetailsDivider.add();
            var flxBillDetailsPrincipal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "flxBillDetailsPrincipal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "24dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsPrincipal.setDefaultUnit(kony.flex.DP);
            var lblBilldetailsPrincipalText = new kony.ui.Label({
                "id": "lblBilldetailsPrincipalText",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Principal",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilldetailsCurrency1 = new kony.ui.Label({
                "id": "lblBilldetailsCurrency1",
                "isVisible": true,
                "left": "296dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "EUR",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilldetailsPrincipalAmount = new kony.ui.Label({
                "id": "lblBilldetailsPrincipalAmount",
                "isVisible": true,
                "left": "340dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "250,000.00",
                "top": "8dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHiddenValue1 = new kony.ui.Label({
                "id": "lblHiddenValue1",
                "isVisible": false,
                "left": "340dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "250,000.00",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBillDetailsPrincipal.add(lblBilldetailsPrincipalText, lblBilldetailsCurrency1, lblBilldetailsPrincipalAmount, lblHiddenValue1);
            var flxBillDetailsWithdrawlFee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "flxBillDetailsWithdrawlFee",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsWithdrawlFee.setDefaultUnit(kony.flex.DP);
            var lblBilldetailsWithdrawlFeeText = new kony.ui.Label({
                "id": "lblBilldetailsWithdrawlFeeText",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Withdrawal Fee",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilldetailsWithdrawlFeeCurrency = new kony.ui.Label({
                "id": "lblBilldetailsWithdrawlFeeCurrency",
                "isVisible": true,
                "left": "296dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "EUR",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilldetailsWithdrawlFee = new kony.ui.Label({
                "id": "lblBilldetailsWithdrawlFee",
                "isVisible": true,
                "left": 340,
                "skin": "sknLabelRed16",
                "text": "(35.00)",
                "top": "8dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHiddenValue2 = new kony.ui.Label({
                "id": "lblHiddenValue2",
                "isVisible": false,
                "left": "340dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "250,000.00",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBillDetailsWithdrawlFee.add(lblBilldetailsWithdrawlFeeText, lblBilldetailsWithdrawlFeeCurrency, lblBilldetailsWithdrawlFee, lblHiddenValue2);
            var flxBillDetailsInterestAccrued = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "flxBillDetailsInterestAccrued",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxBillDetailsInterestAccrued.setDefaultUnit(kony.flex.DP);
            var lblBilldetailsInterestAccrued = new kony.ui.Label({
                "id": "lblBilldetailsInterestAccrued",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Interest Accrued",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBillDetailsInterestAccruedCurrency = new kony.ui.Label({
                "id": "lblBillDetailsInterestAccruedCurrency",
                "isVisible": true,
                "left": "296dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "EUR",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBillDetailsInterestAccruedAmount = new kony.ui.Label({
                "id": "lblBillDetailsInterestAccruedAmount",
                "isVisible": true,
                "left": 340,
                "skin": "CopysknLbl0abb67161bf3c45",
                "text": "1449.15",
                "top": "8dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBillDetailsInterestAccrued.add(lblBilldetailsInterestAccrued, lblBillDetailsInterestAccruedCurrency, lblBillDetailsInterestAccruedAmount);
            var flxBilldetailsTax = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "flxBilldetailsTax",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxBilldetailsTax.setDefaultUnit(kony.flex.DP);
            var lblBilldetailsTaxText = new kony.ui.Label({
                "id": "lblBilldetailsTaxText",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Tax",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilldetailsTaxCurrency = new kony.ui.Label({
                "id": "lblBilldetailsTaxCurrency",
                "isVisible": true,
                "left": "296dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "EUR",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilldetailsTaxAmount = new kony.ui.Label({
                "id": "lblBilldetailsTaxAmount",
                "isVisible": true,
                "left": 340,
                "skin": "sknLabelRed16",
                "text": "(144.91)",
                "top": "8dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBilldetailsTax.add(lblBilldetailsTaxText, lblBilldetailsTaxCurrency, lblBilldetailsTaxAmount);
            var flxBilldetailsNetSettlement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "48dp",
                "id": "flxBilldetailsNetSettlement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "CopysknBlueBorder",
                "top": "0dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxBilldetailsNetSettlement.setDefaultUnit(kony.flex.DP);
            var lblBilldetailsNetSettlement = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBilldetailsNetSettlement",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknLblBg000000px14RoboMedium",
                "text": "Net Settlement",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusIconNetSettlementReciept = new kony.ui.CustomWidget({
                "id": "cusIconNetSettlementReciept",
                "isVisible": true,
                "left": "150dp",
                "width": "36dp",
                "height": "36dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-primary",
                "iconName": "receipt",
                "size": "medium"
            });
            var lblBilldetailsNetSettlementCurrency = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBilldetailsNetSettlementCurrency",
                "isVisible": true,
                "left": "296dp",
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "EUR",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBilldetailsNetSettlementAmount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBilldetailsNetSettlementAmount",
                "isVisible": true,
                "left": 340,
                "skin": "CopysknLbl0f41b58e3ed3b4e",
                "text": "144.91",
                "top": "8dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBilldetailsNetSettlement.add(lblBilldetailsNetSettlement, cusIconNetSettlementReciept, lblBilldetailsNetSettlementCurrency, lblBilldetailsNetSettlementAmount);
            var lblMessage = new kony.ui.Label({
                "id": "lblMessage",
                "isVisible": false,
                "left": "108dp",
                "skin": "CopydefLabel0j8ce059075894d",
                "text": "Interest Forgone due to Early closure",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0a8df3221b50244 = new kony.ui.Label({
                "id": "Label0a8df3221b50244",
                "isVisible": false,
                "left": "108dp",
                "skin": "CopydefLabel0c602264df8864c",
                "text": "EUR 47,001.70",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusButtonBillDetailsOkay = new kony.ui.CustomWidget({
                "id": "cusButtonBillDetailsOkay",
                "isVisible": true,
                "bottom": "32dp",
                "top": "50dp",
                "width": "160dp",
                "height": "50dp",
                "centerX": "50%",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Okay",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxViewBillDetailsPopupWrapper.add(flxBillDetailsHeaderInfo, flxBillDetailsDivider, flxBillDetailsPrincipal, flxBillDetailsWithdrawlFee, flxBillDetailsInterestAccrued, flxBilldetailsTax, flxBilldetailsNetSettlement, lblMessage, Label0a8df3221b50244, cusButtonBillDetailsOkay);
            flxViewBillDetailsPopup.add(flxViewBillDetailsPopupWrapper);
            var flxSearchAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "CopysknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxSearchAccount.setDefaultUnit(kony.flex.DP);
            var CopyflxSearchInnerWrapper0ad5ab50fb7994d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "CopyflxSearchInnerWrapper0ad5ab50fb7994d",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "CopysknFlxOutstanding",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            CopyflxSearchInnerWrapper0ad5ab50fb7994d.setDefaultUnit(kony.flex.DP);
            var popupPayinPayout = new com.konymp.popupPayinPayout.popupPayinPayoutCopy({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "id": "popupPayinPayout",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyCopyslFbox0a1af6ccf824346",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "FlexContainer0d563d2dda18c41": {
                        "width": "92%"
                    },
                    "flxInnerSearch": {
                        "centerX": "50.00%",
                        "top": "0dp"
                    },
                    "flxPopUpClose": {
                        "left": "16dp"
                    },
                    "imgClose": {
                        "src": "ico_close_1.png"
                    },
                    "imgSrc": {
                        "src": "search_2.png"
                    },
                    "lblAccountIdhead": {
                        "width": "23.80%"
                    },
                    "lblCurreny": {
                        "width": "9%"
                    },
                    "lblCustomerIdhead": {
                        "width": "14.50%"
                    },
                    "lblProductName": {
                        "width": "28.50%"
                    },
                    "segPayinPayout": {
                        "data": [{
                            "lblAccountId": "4324242342",
                            "lblCurrency": "EER",
                            "lblCustomerId": "434354545",
                            "lblProductId": "Sefemkgen Akferg",
                            "lblWorkingBal": "3224242.5354"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }, {
                            "lblAccountId": "Label",
                            "lblCurrency": "Label",
                            "lblCustomerId": "Label",
                            "lblProductId": "-",
                            "lblWorkingBal": "-"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var CopyflxDummyBelow0i4d96f1d840244 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "CopyflxDummyBelow0i4d96f1d840244",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxDummyBelow0i4d96f1d840244.setDefaultUnit(kony.flex.DP);
            CopyflxDummyBelow0i4d96f1d840244.add();
            CopyflxSearchInnerWrapper0ad5ab50fb7994d.add(popupPayinPayout, CopyflxDummyBelow0i4d96f1d840244);
            flxSearchAccount.add(CopyflxSearchInnerWrapper0ad5ab50fb7994d);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyflxNavRailGradientSkin0j4b7bcb578604a",
                "top": "0%",
                "width": "64dp",
                "overrides": {
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "ErrorAllert": {
                    "left": "1%",
                    "right": "",
                    "width": "96%"
                },
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "MainTabs": {
                    "width": "100%"
                },
                "FundDetails.FlexContainer0ebe191bc1cf946": {
                    "left": "-144dp"
                },
                "FundDetails.flxListBoxSelectedValue": {
                    "width": "160dp"
                },
                "FundDetails.flxLoanDetailsvalue": {
                    "left": "0dp"
                },
                "FundDetails.lblAccountNumberResponse": {
                    "width": "160dp"
                },
                "FundDetails.lblAccountServiceInfo": {
                    "text": "Deposit Services"
                },
                "FundDetails.lblCurrencyValueInfo": {
                    "left": "20dp",
                    "width": "96dp"
                },
                "FundDetails.lblCurrencyValueResponse": {
                    "width": "96dp"
                },
                "FundDetails.lblListBoxSelectedValue": {
                    "text": "Withdraw Deposit"
                },
                "FundDetails.lblLoanTypeInfo": {
                    "left": "16dp",
                    "text": "Account  Type",
                    "width": "168dp"
                },
                "FundDetails.lblLoanTypeResponse": {
                    "left": "16dp",
                    "width": "168dp"
                },
                "FundDetails.lblMaturityDateInfo": {
                    "width": "136dp"
                },
                "FundDetails.lblMaturityDateResponse": {
                    "width": "136dp"
                },
                "FundDetails.lblStartDateInfo": {
                    "width": "136dp"
                },
                "FundDetails.lblStartDateResponse": {
                    "width": "136dp"
                },
                "FundDetails": {
                    "left": "0dp",
                    "top": "64dp"
                },
                "tempModeOfSettlement1": {
                    "centerX": "",
                    "height": "56dp",
                    "left": "0dp",
                    "top": "0dp",
                    "width": "248dp"
                },
                "tempModeOfSettlement1.lblFloatLabel": {
                    "text": "Mode of Settlement"
                },
                "tempModeOfSettlement1.txtFloatText": {
                    "text": "Account Transfer"
                },
                "tempModeOfSettlement": {
                    "centerX": "",
                    "height": "56dp",
                    "width": "304dp"
                },
                "tempModeOfSettlement.lblFloatLabel": {
                    "left": "5%",
                    "text": "Mode of Settlement"
                },
                "tempModeOfSettlement.txtFloatText": {
                    "text": "Account Transfer"
                },
                "tempSettlementAccount": {
                    "centerX": "",
                    "height": "42dp",
                    "left": "340dp",
                    "top": "0dp",
                    "width": "260dp"
                },
                "tempSettlementAccount.flxBgColor": {
                    "height": "100%"
                },
                "tempSettlementAccount.lblFloatLabel": {
                    "left": "5%",
                    "text": "Settlement Account"
                },
                "tempSettlementAccount.txtFloatText": {
                    "centerY": "50%",
                    "height": "100%",
                    "left": "0dp"
                },
                "OverrideSegmentPopup": {
                    "height": "100%"
                },
                "OverrideSegmentPopup.flxPopup": {
                    "centerX": "50%",
                    "centerY": "50%",
                    "left": "",
                    "top": ""
                },
                "OverrideSegmentPopup.imgClose": {
                    "src": "ico_close_1.png"
                },
                "ProgressIndicator": {
                    "height": "100%"
                },
                "popupPayinPayout.FlexContainer0d563d2dda18c41": {
                    "width": "92%"
                },
                "popupPayinPayout.flxInnerSearch": {
                    "centerX": "50.00%",
                    "top": "0dp"
                },
                "popupPayinPayout.flxPopUpClose": {
                    "left": "16dp"
                },
                "popupPayinPayout.imgClose": {
                    "src": "ico_close_1.png"
                },
                "popupPayinPayout.imgSrc": {
                    "src": "search_2.png"
                },
                "popupPayinPayout.lblAccountIdhead": {
                    "width": "23.80%"
                },
                "popupPayinPayout.lblCurreny": {
                    "width": "9%"
                },
                "popupPayinPayout.lblCustomerIdhead": {
                    "width": "14.50%"
                },
                "popupPayinPayout.lblProductName": {
                    "width": "28.50%"
                },
                "popupPayinPayout.segPayinPayout": {
                    "data": [{
                        "lblAccountId": "4324242342",
                        "lblCurrency": "EER",
                        "lblCustomerId": "434354545",
                        "lblProductId": "Sefemkgen Akferg",
                        "lblWorkingBal": "3224242.5354"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }, {
                        "lblAccountId": "Label",
                        "lblCurrency": "Label",
                        "lblCustomerId": "Label",
                        "lblProductId": "-",
                        "lblWorkingBal": "-"
                    }]
                },
                "uuxNavigationRail": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxNavigationRail, flxMainContainer, flxDropDownAccountListOutsideWrapper, flxSearchAccountDuplicate, OverrideSegmentPopup, ProgressIndicator, flxViewBillDetailsPopup, flxSearchAccount, uuxNavigationRail);
        };
        return [{
            "addWidgets": addWidgetsfrmDepositWithdrawal,
            "enabledForIdleTimeout": false,
            "id": "frmDepositWithdrawal",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1277, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});