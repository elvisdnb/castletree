define("frmSearchCustomer", function() {
    return function(controller) {
        function addWidgetsfrmSearchCustomer() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "9.50%",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "0dp",
                "width": "86%",
                "zIndex": 9
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLoansSuccessMessage = new kony.ui.Label({
                "height": "20dp",
                "id": "lblLoansSuccessMessage",
                "isVisible": true,
                "left": "222dp",
                "right": "32dp",
                "skin": "sknlbl16PX434343BG",
                "text": "New loan account 51237781231 created for John Doe",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblLoansSuccessMessage);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0",
                "verticalScrollIndicator": true,
                "width": "95%"
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var FlexContainer0e48343e0b1f644 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1100dp",
                "id": "FlexContainer0e48343e0b1f644",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "96%"
            }, {}, {});
            FlexContainer0e48343e0b1f644.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "60%",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.CustomWidget({
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "centerY": "50%",
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "centerY": "50%",
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblBreadBoardHomeValue = new kony.ui.Label({
                "centerY": "50%",
                "height": "18dp",
                "id": "CopylblBreadBoardHomeValue",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl12pxBG4D4D4D",
                "text": "Search for a Customer",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, lblBreadBoardHomeValue, lblBreadBoardSplashValue, CopylblBreadBoardHomeValue);
            var flxDocWrap = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxDocWrap",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0",
                "width": "40%"
            }, {}, {});
            flxDocWrap.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": true,
                "right": 0,
                "top": "0dp",
                "width": "152dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": true,
                "right": "10dp",
                "top": "16dp",
                "width": "160dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxDocWrap.add(cusFillDetailsButton, cusViewDocumentButton);
            flxHeaderMenu.add(flxBack, flxDocWrap);
            var flxSearchWrap = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "980dp",
                "id": "flxSearchWrap",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "right": "24dp",
                "skin": "CopyslFbox0f98caa83ee8e42",
                "top": "0",
                "width": "1150dp"
            }, {}, {});
            flxSearchWrap.setDefaultUnit(kony.flex.DP);
            var flxCustomerDetailsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 30,
                "centerX": "50%",
                "clipBounds": true,
                "height": "920dp",
                "id": "flxCustomerDetailsSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 30,
                "width": "94%"
            }, {}, {});
            flxCustomerDetailsSearch.setDefaultUnit(kony.flex.DP);
            var flxEnquirySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEnquirySearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxEnquirySearch.setDefaultUnit(kony.flex.DP);
            var lblEnterCustomer = new kony.ui.Label({
                "height": "20dp",
                "id": "lblEnterCustomer",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0ad632ad1376b4f",
                "text": "Enter  Customer Details",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "2px",
                "id": "flxBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0i25b5f25af734e",
                "top": 12,
                "width": "100%"
            }, {}, {});
            flxBar.setDefaultUnit(kony.flex.DP);
            flxBar.add();
            flxEnquirySearch.add(lblEnterCustomer, flxBar);
            var lblCustomerID = new kony.ui.Label({
                "id": "lblCustomerID",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0he6cc43795e64c",
                "text": "Customer ID",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custCustomerId = new kony.ui.CustomWidget({
                "id": "custCustomerId",
                "isVisible": true,
                "left": "0dp",
                "top": "80dp",
                "width": "120px",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Operand",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var inputCustomerId = new kony.ui.CustomWidget({
                "id": "inputCustomerId",
                "isVisible": true,
                "left": "140dp",
                "top": "80dp",
                "width": "200px",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Customer ID",
                "maxLength": 6,
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": true,
                "value": ""
            });
            var lblFirstName = new kony.ui.Label({
                "id": "lblFirstName",
                "isVisible": true,
                "left": "368dp",
                "skin": "CopydefLabel0he6cc43795e64c",
                "text": "First Name",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custFirstName = new kony.ui.CustomWidget({
                "id": "custFirstName",
                "isVisible": true,
                "left": "368dp",
                "top": "80dp",
                "width": "120dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Operand",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var inputFirstName = new kony.ui.CustomWidget({
                "id": "inputFirstName",
                "isVisible": true,
                "left": "508dp",
                "top": "80dp",
                "width": "200dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "First Name",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": true,
                "value": ""
            });
            var lblLastName = new kony.ui.Label({
                "id": "lblLastName",
                "isVisible": true,
                "left": "736dp",
                "skin": "CopydefLabel0he6cc43795e64c",
                "text": "Last Name",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custLastName = new kony.ui.CustomWidget({
                "id": "custLastName",
                "isVisible": true,
                "left": "736dp",
                "top": "80dp",
                "width": "120dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Operand",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var inputLastName = new kony.ui.CustomWidget({
                "id": "inputLastName",
                "isVisible": true,
                "left": "876dp",
                "top": "80dp",
                "width": "200dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Last Name",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": true,
                "value": ""
            });
            var lblDOB = new kony.ui.Label({
                "id": "lblDOB",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0he6cc43795e64c",
                "text": "Date of Birth",
                "top": "156dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custDOB = new kony.ui.CustomWidget({
                "id": "custDOB",
                "isVisible": true,
                "left": "0dp",
                "top": "186dp",
                "width": "120px",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Operand",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var inputDOB1 = new kony.ui.CustomWidget({
                "id": "inputDOB1",
                "isVisible": false,
                "left": "140dp",
                "top": "186dp",
                "width": "200px",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Date of Birth",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": true,
                "value": ""
            });
            var inputDOB = new kony.ui.CustomWidget({
                "id": "inputDOB",
                "isVisible": true,
                "left": "140dp",
                "top": "186dp",
                "width": "200dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "dd-MMM-yyyy",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Date of Birth",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var lblPhoneNumber = new kony.ui.Label({
                "id": "lblPhoneNumber",
                "isVisible": true,
                "left": "368dp",
                "skin": "CopydefLabel0he6cc43795e64c",
                "text": "Phone Number",
                "top": "156dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custPhoneNumber = new kony.ui.CustomWidget({
                "id": "custPhoneNumber",
                "isVisible": true,
                "left": "368dp",
                "top": "186dp",
                "width": "120dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Operand",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var inputPhoneNumber = new kony.ui.CustomWidget({
                "id": "inputPhoneNumber",
                "isVisible": true,
                "left": "508dp",
                "top": "186dp",
                "width": "200dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Phone Number",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": true,
                "validateOnInitialRender": "",
                "validationMessage": "",
                "value": ""
            });
            var lblEmail = new kony.ui.Label({
                "id": "lblEmail",
                "isVisible": true,
                "left": "736dp",
                "skin": "CopydefLabel0he6cc43795e64c",
                "text": "Email Address",
                "top": "156dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custEmail = new kony.ui.CustomWidget({
                "id": "custEmail",
                "isVisible": true,
                "left": "736dp",
                "top": "186dp",
                "width": "120dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Operand",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var inputEmail = new kony.ui.CustomWidget({
                "id": "inputEmail",
                "isVisible": true,
                "left": "876dp",
                "top": "186dp",
                "width": "200dp",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "email"
                },
                "iconButtonIcon": "",
                "labelText": "Email Address",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": true,
                "value": ""
            });
            var lblAccountOfficer = new kony.ui.Label({
                "id": "lblAccountOfficer",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0he6cc43795e64c",
                "text": "Account Officer",
                "top": "262dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custAccountOfficer = new kony.ui.CustomWidget({
                "id": "custAccountOfficer",
                "isVisible": true,
                "left": "0dp",
                "top": "292dp",
                "width": "120px",
                "height": "56px",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Operand",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var inputAccountOfficer1 = new kony.ui.CustomWidget({
                "id": "inputAccountOfficer1",
                "isVisible": false,
                "left": "144dp",
                "top": "292dp",
                "width": "200px",
                "height": "56px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Account Officer",
                "onKeyDown": "",
                "onKeyUp": "",
                "placeholder": "",
                "showIconButtonTrailing": true,
                "value": ""
            });
            var inputAccountOfficer = new kony.ui.CustomWidget({
                "id": "inputAccountOfficer",
                "isVisible": true,
                "left": "140dp",
                "top": "292dp",
                "width": "200dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Account Officer",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var flxCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 21,
                "skin": "slFbox",
                "top": "400dp",
                "width": "47.07%",
                "zIndex": 1
            }, {}, {});
            flxCustButtons.setDefaultUnit(kony.flex.DP);
            var custCancel1 = new kony.ui.CustomWidget({
                "id": "custCancel1",
                "isVisible": true,
                "right": 216,
                "top": "0dp",
                "width": "183dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Clear Fields ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custProceed1 = new kony.ui.CustomWidget({
                "id": "custProceed1",
                "isVisible": true,
                "right": 0,
                "top": "0dp",
                "width": "192dp",
                "height": "40dp",
                "centerY": "50%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "onclickEvent": controller.AS_TPW_f937666cd8aa4daebf54a21779a11059,
                "labelText": "Find Customer",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxCustButtons.add(custCancel1, custProceed1);
            var lblSearchResults = new kony.ui.Label({
                "height": "20dp",
                "id": "lblSearchResults",
                "isVisible": true,
                "left": "0",
                "skin": "CopydefLabel0ad632ad1376b4f",
                "text": "Search Results",
                "top": 450,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoResults = new kony.ui.Label({
                "id": "lblNoResults",
                "isVisible": false,
                "left": "0",
                "skin": "CopydefLabel0j05c565974c84a",
                "text": "No Results Found",
                "top": 490,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "430dp",
                "id": "flxSegWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0gc58ab0dd58f4d",
                "top": "485dp",
                "width": "100%"
            }, {}, {});
            flxSegWrapper.setDefaultUnit(kony.flex.DP);
            var flxCustomerWrap = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "58dp",
                "id": "flxCustomerWrap",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxCustomerWrap.setDefaultUnit(kony.flex.DP);
            var lblheaderCustomerID = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderCustomerID",
                "isVisible": true,
                "left": "0",
                "skin": "segBlueColor",
                "text": "Customer ID",
                "top": "0",
                "width": "120dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderFirstName = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderFirstName",
                "isVisible": true,
                "left": "25dp",
                "skin": "segBlueColor",
                "text": "First Name",
                "top": "0dp",
                "width": "120dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderLastName = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderLastName",
                "isVisible": true,
                "left": "25dp",
                "skin": "segBlueColor",
                "text": "Mnemonic",
                "top": "0dp",
                "width": "120dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderDOB = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderDOB",
                "isVisible": true,
                "left": "25dp",
                "skin": "segBlueColor",
                "text": "Date of Birth",
                "top": "0dp",
                "width": "100dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderPhone = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderPhone",
                "isVisible": true,
                "left": "25dp",
                "skin": "segBlueColor",
                "text": "Phone Number",
                "top": "0dp",
                "width": "140dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderEmail = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderEmail",
                "isVisible": true,
                "left": "25dp",
                "skin": "segBlueColor",
                "text": "Email",
                "top": "0dp",
                "width": "180dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblheaderAccountOfficer = new kony.ui.Label({
                "height": "100%",
                "id": "lblheaderAccountOfficer",
                "isVisible": true,
                "left": "25dp",
                "skin": "segBlueColor",
                "text": "Account Officer",
                "top": "0dp",
                "width": "160dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerWrap.add(lblheaderCustomerID, lblheaderFirstName, lblheaderLastName, lblheaderDOB, lblheaderPhone, lblheaderEmail, lblheaderAccountOfficer);
            var flxSegBars = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSegBars",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0ed3ba296696549",
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxSegBars.setDefaultUnit(kony.flex.DP);
            flxSegBars.add();
            var searchSegments = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }, {
                    "lblAccountOfficer": "Patrik Owen",
                    "lblCustomerID": "101146",
                    "lblDateOfBirth": "04 Oct 1975",
                    "lblEmail": "ericdl@gmail.com",
                    "lblFirstName": "Rolf Gerieng",
                    "lblLastName": "Rolf Gerieng",
                    "lblPhoneNumber": "+3197010281305"
                }],
                "groupCells": false,
                "height": "320dp",
                "id": "searchSegments",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "Copyseg0bf8f154f62d540",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxSearchSegments",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCustomerWrap": "flxCustomerWrap",
                    "flxSearchSegments": "flxSearchSegments",
                    "lblAccountOfficer": "lblAccountOfficer",
                    "lblCustomerID": "lblCustomerID",
                    "lblDateOfBirth": "lblDateOfBirth",
                    "lblEmail": "lblEmail",
                    "lblFirstName": "lblFirstName",
                    "lblLastName": "lblLastName",
                    "lblPhoneNumber": "lblPhoneNumber"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyflxSegBars0f60a065955ef44 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "CopyflxSegBars0f60a065955ef44",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "CopyslFbox0ed3ba296696549",
                "top": 2,
                "width": "100%"
            }, {}, {});
            CopyflxSegBars0f60a065955ef44.setDefaultUnit(kony.flex.DP);
            CopyflxSegBars0f60a065955ef44.add();
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var btnPrev1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnPrev1",
                "isVisible": false,
                "right": "45dp",
                "skin": "slImage",
                "src": "arrow_back.png",
                "top": "5dp",
                "width": "19dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "17dp",
                "id": "btnNext1",
                "isVisible": false,
                "right": "15dp",
                "skin": "slImage",
                "src": "arrow_forward.png",
                "top": "5dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchPage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSearchPage",
                "isVisible": true,
                "right": "90dp",
                "skin": "CopydefLabel0b044b7d672a44a",
                "text": "1-6 of 18",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRowPerPageNumber = new kony.ui.Label({
                "id": "lblRowPerPageNumber",
                "isVisible": false,
                "left": "354dp",
                "skin": "CopydefLabel0bfa17407e4df49",
                "text": "Rows Per Page",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TextField0a340c02fa20749 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "20dp",
                "id": "TextField0a340c02fa20749",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "494dp",
                "placeholder": "Placeholder",
                "secureTextEntry": false,
                "skin": "CopydefTextBoxNormal0ca88ed6f06d246",
                "text": "6",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "10dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var btnPrev = new kony.ui.Button({
                "height": "30dp",
                "id": "btnPrev",
                "isVisible": true,
                "right": "45dp",
                "skin": "sknBtnDisable",
                "text": "O",
                "top": "10dp",
                "width": 30,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNext = new kony.ui.Button({
                "height": "30dp",
                "id": "btnNext",
                "isVisible": true,
                "right": "15dp",
                "skin": "sknBtnFocus",
                "text": "N",
                "top": "10dp",
                "width": 30,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPagination.add(btnPrev1, btnNext1, lblSearchPage, lblRowPerPageNumber, TextField0a340c02fa20749, btnPrev, btnNext);
            flxSegWrapper.add(flxCustomerWrap, flxSegBars, searchSegments, CopyflxSegBars0f60a065955ef44, flxPagination);
            flxCustomerDetailsSearch.add(flxEnquirySearch, lblCustomerID, custCustomerId, inputCustomerId, lblFirstName, custFirstName, inputFirstName, lblLastName, custLastName, inputLastName, lblDOB, custDOB, inputDOB1, inputDOB, lblPhoneNumber, custPhoneNumber, inputPhoneNumber, lblEmail, custEmail, inputEmail, lblAccountOfficer, custAccountOfficer, inputAccountOfficer1, inputAccountOfficer, flxCustButtons, lblSearchResults, lblNoResults, flxSegWrapper);
            flxSearchWrap.add(flxCustomerDetailsSearch);
            FlexContainer0e48343e0b1f644.add(flxHeaderMenu, flxSearchWrap);
            flxMainContainer.add(FlexContainer0e48343e0b1f644);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0eefe0e9cca4e46"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0f2b6a314a8bb46"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_f605ce00bb2c4bed9d800b1d625a8816;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_g0e79ea551834e2d93d067e6cc4d196b;
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0eefe0e9cca4e46",
                    "skin2": "CopydefBtnNormal0f2b6a314a8bb46",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMessageInfo, flxMainContainer, uuxNavigationRail, ProgressIndicator, help, helpCenter);
        };
        return [{
            "addWidgets": addWidgetsfrmSearchCustomer,
            "enabledForIdleTimeout": false,
            "id": "frmSearchCustomer",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [14, 640, 1024, 1336, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});