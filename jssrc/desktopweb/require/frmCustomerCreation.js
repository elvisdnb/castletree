define("frmCustomerCreation", function() {
    return function(controller) {
        function addWidgetsfrmCustomerCreation() {
            this.setDefaultUnit(kony.flex.DP);
            var SegmentPopup = new com.konymp.flxoverrideSegmentpopup.SegmentPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "968dp",
                "id": "SegmentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "overrides": {
                    "SegmentPopup": {
                        "isVisible": false,
                        "right": "viz.val_cleared",
                        "width": "100%",
                        "zIndex": kony.flex.ZINDEX_AUTO
                    },
                    "imgClose": {
                        "src": "ico_close.png"
                    },
                    "segOverride": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "65dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFbox0db234f0301294a",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "72dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "32dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "5%",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.Label({
                "height": "24dp",
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "32dp",
                "skin": "CopydefLabel0i3920d41f8a845",
                "text": "O",
                "top": "24dp",
                "width": "24dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusBackIcon1 = new kony.ui.CustomWidget({
                "id": "cusBackIcon1",
                "isVisible": false,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "119dp",
                "skin": "sknLbl12pxBG4D4D4D",
                "text": "New Client Onboarding",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, cusBackIcon1, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(flxBack);
            var flxMessageInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxMessageInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxMessageInfoBorderGreen",
                "top": "2dp",
                "width": "1256dp",
                "zIndex": 1
            }, {}, {});
            flxMessageInfo.setDefaultUnit(kony.flex.DP);
            var cusStatusIcon = new kony.ui.CustomWidget({
                "id": "cusStatusIcon",
                "isVisible": true,
                "left": "22dp",
                "top": "14dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "status-success-primary",
                "iconName": "check_circle",
                "size": "small"
            });
            var lblSuccessNotification = new kony.ui.Label({
                "id": "lblSuccessNotification",
                "isVisible": true,
                "left": "61dp",
                "skin": "sknLbl16px000000BG",
                "text": "Success Notification",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageWithReferenceNumber = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMessageWithReferenceNumber",
                "isVisible": true,
                "left": "222dp",
                "right": "320dp",
                "skin": "sknlbl16PX434343BG",
                "top": "17dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custViewCustomerButton = new kony.ui.CustomWidget({
                "id": "custViewCustomerButton",
                "isVisible": true,
                "left": "1020dp",
                "top": "17dp",
                "width": "150dp",
                "height": "32dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "View Client",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var imgClose = new kony.ui.Image2({
                "height": "14dp",
                "id": "imgClose",
                "isVisible": true,
                "left": "1200dp",
                "skin": "slImage",
                "src": "ico_close.png",
                "top": "27dp",
                "width": "14dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageInfo.add(cusStatusIcon, lblSuccessNotification, lblMessageWithReferenceNumber, custViewCustomerButton, imgClose);
            var flxCustomerDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": 1022,
                "id": "flxCustomerDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0cc51b62fbfd947",
                "top": "72dp",
                "width": "1000dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxCustomerDetails.setDefaultUnit(kony.flex.DP);
            var flxBasicDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "306dp",
                "id": "flxBasicDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e96a806e0a3241",
                "top": "0dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxBasicDetails.setDefaultUnit(kony.flex.DP);
            var TitleLabel = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100dp",
                "id": "TitleLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "TitleLabel": {
                        "zIndex": kony.flex.ZINDEX_AUTO
                    },
                    "lblTitle": {
                        "centerY": "60%",
                        "left": "8%",
                        "right": "viz.val_cleared",
                        "text": "Basic Details",
                        "top": "10dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblBasicDetails = new kony.ui.Label({
                "height": "20dp",
                "id": "lblBasicDetails",
                "isVisible": false,
                "left": "150dp",
                "skin": "sknLblBlue16px",
                "text": "Basic Details",
                "top": "8dp",
                "width": "97dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBasicDtls = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxBasicDtls",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0c62f59ec8f4c46",
                "top": "43dp",
                "width": "870dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxBasicDtls.setDefaultUnit(kony.flex.DP);
            flxBasicDtls.add();
            var flxTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "42dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxTitle.setDefaultUnit(kony.flex.DP);
            flxTitle.add();
            var nationalityDropDownList = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "60dp",
                "id": "nationalityDropDownList",
                "isVisible": false,
                "left": "71dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "defListBoxNormal",
                "top": "210dp",
                "width": "275dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var residenceDropdownList = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "60dp",
                "id": "residenceDropdownList",
                "isVisible": false,
                "left": "400dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "defListBoxNormal",
                "top": "210dp",
                "width": "275dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAlterRefName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxAlterRefName",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "4dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "130dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxAlterRefName.setDefaultUnit(kony.flex.DP);
            flxAlterRefName.add();
            var flxNationality = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxNationality",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "4dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "199dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxNationality.setDefaultUnit(kony.flex.DP);
            flxNationality.add();
            flxBasicDetails.add(TitleLabel, lblBasicDetails, flxBasicDtls, flxTitle, nationalityDropDownList, residenceDropdownList, flxAlterRefName, flxNationality);
            var flxContactDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350dp",
                "id": "flxContactDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "280dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxContactDetails.setDefaultUnit(kony.flex.DP);
            var TitleLabel1 = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100dp",
                "id": "TitleLabel1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "TitleLabel": {
                        "zIndex": kony.flex.ZINDEX_AUTO
                    },
                    "lblTitle": {
                        "centerY": "60%",
                        "left": "8%",
                        "text": "Contact Details",
                        "top": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblContactDetails = new kony.ui.Label({
                "id": "lblContactDetails",
                "isVisible": false,
                "left": "150dp",
                "skin": "sknLblBlue16px",
                "text": "Contact Details",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContactDtls = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxContactDtls",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0c00200ca5ace49",
                "top": "35dp",
                "width": "870dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxContactDtls.setDefaultUnit(kony.flex.DP);
            flxContactDtls.add();
            var custCityDropDown = new kony.ui.CustomWidget({
                "id": "custCityDropDown",
                "isVisible": false,
                "left": "71dp",
                "top": "200dp",
                "width": "275dp",
                "height": "60dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "",
                "disabled": false,
                "labelText": "City",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var countryListDropdown = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "60dp",
                "id": "countryListDropdown",
                "isVisible": false,
                "left": "400dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "defListBoxNormal",
                "top": "200dp",
                "width": "275dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxContactDetails.add(TitleLabel1, lblContactDetails, flxContactDtls, custCityDropDown, countryListDropdown);
            var flxEmployeementDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "190dp",
                "id": "flxEmployeementDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "566dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxEmployeementDetails.setDefaultUnit(kony.flex.DP);
            var TitleLabel2 = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100dp",
                "id": "TitleLabel2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "TitleLabel": {
                        "zIndex": kony.flex.ZINDEX_AUTO
                    },
                    "lblTitle": {
                        "centerY": "60%",
                        "left": "8%",
                        "text": "Employment Details"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblEmploymentDetails = new kony.ui.Label({
                "id": "lblEmploymentDetails",
                "isVisible": false,
                "left": "150dp",
                "skin": "sknLblBlue16px",
                "text": "Employment Details",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEmploymentDtls = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEmploymentDtls",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e43ecb0f642141",
                "top": "35dp",
                "width": "870dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxEmploymentDtls.setDefaultUnit(kony.flex.DP);
            flxEmploymentDtls.add();
            var employmentStatusListDropdown = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "60dp",
                "id": "employmentStatusListDropdown",
                "isVisible": false,
                "left": "71dp",
                "masterData": [
                    ["EMPLOYED", "Hired"],
                    ["RETIRED", "Retired"],
                    ["SELF-EMPLOYED", "Freelance"],
                    ["STUDENT", "Educatee"],
                    ["UNEMPLOYED", "Unemployed"],
                    ["OTHER", "Other"]
                ],
                "skin": "defListBoxNormal",
                "top": "40dp",
                "width": "275dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxEmployeementDetails.add(TitleLabel2, lblEmploymentDetails, flxEmploymentDtls, employmentStatusListDropdown);
            var flxOtherDetials = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "148dp",
                "id": "flxOtherDetials",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "740dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxOtherDetials.setDefaultUnit(kony.flex.DP);
            var TitleLabel3 = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100dp",
                "id": "TitleLabel3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "TitleLabel": {
                        "zIndex": kony.flex.ZINDEX_AUTO
                    },
                    "lblTitle": {
                        "centerY": "60%",
                        "left": "8%",
                        "text": "Other Details"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblOtherDetails = new kony.ui.Label({
                "id": "lblOtherDetails",
                "isVisible": false,
                "left": "150dp",
                "skin": "sknLblBlue16px",
                "text": "Other Details",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxOtherDtls = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxOtherDtls",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "150dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i000f045e9a346",
                "top": "35dp",
                "width": "870dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxOtherDtls.setDefaultUnit(kony.flex.DP);
            flxOtherDtls.add();
            var accountOfficerListDropdown = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "60dp",
                "id": "accountOfficerListDropdown",
                "isVisible": false,
                "left": "71dp",
                "masterData": [
                    ["2", "Retail Banking Mgr"],
                    ["3", "Retail Banking-3"],
                    ["26", "Customer Service Agent"],
                    ["28", "Retail Credit Officer"],
                    ["29", "Retail Credit Manager"]
                ],
                "skin": "defListBoxNormal",
                "top": "40dp",
                "width": "275dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var sectorListDropDown = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "60dp",
                "id": "sectorListDropDown",
                "isVisible": false,
                "left": "400dp",
                "masterData": [
                    ["1001", "Individual"],
                    ["1002", "Staff"],
                    ["1003", "High Networth"],
                    ["1004", "Director"],
                    ["1005", "Principal Shareholder"],
                    ["1006", "Executive Officer"],
                    ["1007", "Director (Employee)"],
                    ["1008", "Principal Shareholder (Employee)"],
                    ["1009", "Individuals with entrepreneurial activity (Self-employed"],
                    ["1010", "Executive Officer (Employee)"],
                    ["9000", "Others"]
                ],
                "skin": "defListBoxNormal",
                "top": "40dp",
                "width": "275dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxOtherDetials.add(TitleLabel3, lblOtherDetails, flxOtherDtls, accountOfficerListDropdown, sectorListDropDown);
            var custTitleDropDown = new kony.ui.CustomWidget({
                "id": "custTitleDropDown",
                "isVisible": true,
                "left": "80dp",
                "top": "120px",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Title",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var custFirstName = new kony.ui.CustomWidget({
                "id": "custFirstName",
                "isVisible": true,
                "left": "371dp",
                "top": "120px",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "First Name",
                "maxLength": 35,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            var custLastName = new kony.ui.CustomWidget({
                "id": "custLastName",
                "isVisible": true,
                "left": "662dp",
                "top": "120px",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Last Name",
                "maxLength": 35,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "required",
                "validationMessage": "",
                "value": ""
            });
            var custAlternateRefName = new kony.ui.CustomWidget({
                "id": "custAlternateRefName",
                "isVisible": true,
                "left": "80dp",
                "top": "180dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Alternate Reference Name",
                "maxLength": 10,
                "readonly": true,
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "",
                "validationMessage": "",
                "value": " "
            });
            var custGenderDropDown = new kony.ui.CustomWidget({
                "id": "custGenderDropDown",
                "isVisible": true,
                "left": "371dp",
                "top": "180dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Gender",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var custDateOfBirth = new kony.ui.CustomWidget({
                "id": "custDateOfBirth",
                "isVisible": true,
                "left": "662dp",
                "top": "180dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDatePicker",
                "dense": true,
                "disabled": false,
                "disabledDates": "",
                "disabledDays": "",
                "displayFormat": "dd mmm yyyy",
                "firstDayOfWeek": 0,
                "hideHeader": false,
                "iconButtonTrailingAriaLabel": "Open date calendar",
                "labelText": "Date of Birth",
                "max": "",
                "min": "1900-01-01",
                "showWeekNumber": false,
                "validateRequired": "required",
                "value": "",
                "weekLabel": "Wk"
            });
            var custNationalityDropDown = new kony.ui.CustomWidget({
                "id": "custNationalityDropDown",
                "isVisible": true,
                "left": "80dp",
                "top": "240dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Nationality",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var custResidenceDropDown = new kony.ui.CustomWidget({
                "id": "custResidenceDropDown",
                "isVisible": true,
                "left": "371dp",
                "top": "240dp",
                "width": "275dp",
                "height": "56dp",
                "maxHeight": "100dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Residence",
                "options": "",
                "readonly": false,
                "validateRequired": "",
                "value": ""
            });
            var custEmail = new kony.ui.CustomWidget({
                "id": "custEmail",
                "isVisible": true,
                "left": "80dp",
                "top": "400dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "email"
                },
                "iconButtonIcon": "",
                "labelText": "Email",
                "maxLength": 50,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "false",
                "validationMessage": "",
                "value": ""
            });
            var custPhoneNumber = new kony.ui.CustomWidget({
                "id": "custPhoneNumber",
                "isVisible": true,
                "left": "371dp",
                "top": "400dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "number"
                },
                "iconButtonIcon": "",
                "labelText": "Phone Number",
                "maxLength": 17,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "validateRequired": "required",
                "value": ""
            });
            var custStreet = new kony.ui.CustomWidget({
                "id": "custStreet",
                "isVisible": true,
                "left": "80dp",
                "top": "460dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Street",
                "maxLength": 35,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var custAddress = new kony.ui.CustomWidget({
                "id": "custAddress",
                "isVisible": true,
                "left": "371dp",
                "top": "460dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Address",
                "maxLength": 35,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var custTown = new kony.ui.CustomWidget({
                "id": "custTown",
                "isVisible": true,
                "left": "662dp",
                "top": "460dp",
                "width": "275dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Town",
                "maxLength": 35,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var custCityTextField = new kony.ui.CustomWidget({
                "id": "custCityTextField",
                "isVisible": true,
                "left": "80dp",
                "top": "520dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "City",
                "maxLength": 35,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var custCountryDropDown = new kony.ui.CustomWidget({
                "id": "custCountryDropDown",
                "isVisible": true,
                "left": "371dp",
                "top": "520dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Country",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custPostalCode = new kony.ui.CustomWidget({
                "id": "custPostalCode",
                "isVisible": true,
                "left": "662dp",
                "top": "520dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Postal Code",
                "maxLength": 35,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var custEmploymentStatusDropDown = new kony.ui.CustomWidget({
                "id": "custEmploymentStatusDropDown",
                "isVisible": true,
                "left": "80dp",
                "top": "685dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Employment Status",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custEmployersName = new kony.ui.CustomWidget({
                "id": "custEmployersName",
                "isVisible": true,
                "left": "371dp",
                "top": "685dp",
                "width": "275dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxTextField",
                "dense": true,
                "disabled": false,
                "fieldType": {
                    "optionsList": ["text", "search", "tel", "url", "email", "password", "date", "month", "week", "time", "datetime-local", "number"],
                    "selectedValue": "text"
                },
                "iconButtonIcon": "",
                "labelText": "Employer's Name",
                "maxLength": 35,
                "onKeyDown": "",
                "onKeyUp": "",
                "showIconButtonTrailing": false,
                "showIconButtonTrailingWithValue": false,
                "value": ""
            });
            var custAccountOfficerDropDown = new kony.ui.CustomWidget({
                "id": "custAccountOfficerDropDown",
                "isVisible": true,
                "left": "80dp",
                "top": "860dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Account Officer",
                "options": "",
                "readonly": false,
                "value": ""
            });
            var custSectorDropDown = new kony.ui.CustomWidget({
                "id": "custSectorDropDown",
                "isVisible": true,
                "left": "371dp",
                "top": "860dp",
                "width": "275dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "clicked": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Sector",
                "options": "",
                "readonly": false,
                "validateRequired": "required",
                "value": ""
            });
            var flxCustButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxCustButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "910dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxCustButtons.setDefaultUnit(kony.flex.DP);
            var FlexContainer0cd07ec561f3245 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "FlexContainer0cd07ec561f3245",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0e43ecb0f642141",
                "top": "20dp",
                "width": "100%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            FlexContainer0cd07ec561f3245.setDefaultUnit(kony.flex.DP);
            FlexContainer0cd07ec561f3245.add();
            var lblErrorMsg = new kony.ui.Label({
                "height": "21dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "80dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "25dp",
                "width": "257dp",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var custCancel = new kony.ui.CustomWidget({
                "id": "custCancel",
                "isVisible": true,
                "left": "514dp",
                "top": "1%",
                "width": "196dp",
                "height": "40dp",
                "centerY": "70%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var custConfirm = new kony.ui.CustomWidget({
                "id": "custConfirm",
                "isVisible": true,
                "left": "734dp",
                "top": "1%",
                "width": "204dp",
                "height": "40dp",
                "centerY": "70%",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": "Confirm",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            flxCustButtons.add(FlexContainer0cd07ec561f3245, lblErrorMsg, custCancel, custConfirm);
            flxCustomerDetails.add(flxBasicDetails, flxContactDetails, flxEmployeementDetails, flxOtherDetials, custTitleDropDown, custFirstName, custLastName, custAlternateRefName, custGenderDropDown, custDateOfBirth, custNationalityDropDown, custResidenceDropDown, custEmail, custPhoneNumber, custStreet, custAddress, custTown, custCityTextField, custCountryDropDown, custPostalCode, custEmploymentStatusDropDown, custEmployersName, custAccountOfficerDropDown, custSectorDropDown, flxCustButtons);
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "65dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "ErrorAllert": {
                        "height": "65dp",
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "flxError": {
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    },
                    "imgCloseBackup": {
                        "centerY": "50%",
                        "height": "18dp",
                        "left": "viz.val_cleared",
                        "right": 50,
                        "src": "ico_close.png",
                        "top": "viz.val_cleared"
                    },
                    "lblMessage": {
                        "centerY": "50%",
                        "isVisible": true,
                        "text": "Subtitle text goes here",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContainer.add(flxHeaderMenu, flxMessageInfo, flxCustomerDetails, ErrorAllert);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0i404288d19b746",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "btnExplorer": {
                        "skin": "CopydefBtnNormal0a57401d0335e4d"
                    },
                    "btnHelp": {
                        "skin": "CopydefBtnNormal0e45c0702887047"
                    },
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            uuxNavigationRail.onClickBtnExplorer = controller.AS_UWI_c52782fa925f42ce84d1141a49d10d65;
            uuxNavigationRail.onClickBtnHelp = controller.AS_UWI_fb1cefdc91dd404ab8b02f8c76e05629;
            var help = new com.konymp.help({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "help",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0f1d22019362442",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "help": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var helpCenter = new com.konymp.helpCenter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "helpCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0a65c3cacd3214e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "helpCenter": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": false,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "SegmentPopup": {
                    "right": "",
                    "width": "100%",
                    "zIndex": kony.flex.ZINDEX_AUTO
                },
                "SegmentPopup.imgClose": {
                    "src": "ico_close.png"
                },
                "TitleLabel": {
                    "zIndex": kony.flex.ZINDEX_AUTO
                },
                "TitleLabel.lblTitle": {
                    "centerY": "60%",
                    "left": "8%",
                    "right": "",
                    "text": "Basic Details",
                    "top": "10dp"
                },
                "TitleLabel1": {
                    "zIndex": kony.flex.ZINDEX_AUTO
                },
                "TitleLabel1.lblTitle": {
                    "centerY": "60%",
                    "left": "8%",
                    "text": "Contact Details",
                    "top": 10
                },
                "TitleLabel2": {
                    "zIndex": kony.flex.ZINDEX_AUTO
                },
                "TitleLabel2.lblTitle": {
                    "centerY": "60%",
                    "left": "8%",
                    "text": "Employment Details"
                },
                "TitleLabel3": {
                    "zIndex": kony.flex.ZINDEX_AUTO
                },
                "TitleLabel3.lblTitle": {
                    "centerY": "60%",
                    "left": "8%",
                    "text": "Other Details"
                },
                "ErrorAllert": {
                    "height": "65dp",
                    "left": "0dp"
                },
                "ErrorAllert.flxError": {
                    "centerY": "50%",
                    "top": ""
                },
                "ErrorAllert.imgCloseBackup": {
                    "centerY": "50%",
                    "height": "18dp",
                    "left": "",
                    "right": 50,
                    "src": "ico_close.png",
                    "top": ""
                },
                "ErrorAllert.lblMessage": {
                    "centerY": "50%",
                    "text": "Subtitle text goes here",
                    "top": ""
                },
                "uuxNavigationRail": {
                    "skin1": "CopydefBtnNormal0a57401d0335e4d",
                    "skin2": "CopydefBtnNormal0e45c0702887047",
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(SegmentPopup, flxMainContainer, uuxNavigationRail, help, helpCenter, ProgressIndicator);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerCreation,
            "enabledForIdleTimeout": false,
            "id": "frmCustomerCreation",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});