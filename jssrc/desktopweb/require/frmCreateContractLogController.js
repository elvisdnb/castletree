define("userfrmCreateContractLogController", {
    //Type your controller code here 
    currentOverride: {},
    accountId: "",
    onNavigate: function(navobject) {
        this.accountId = "";
        var navigationObject = Application.validation.isNullUndefinedObj(navobject.customerId);
        if (navigationObject !== "") {
            this.accountId = Application.validation.isNullUndefinedObj(navobject.customerId);
        } else {
            this.getContractStaffDetails();
            this.clearDataInComponent();
        }
        //this.getContractStaffDetails();
        //this.pauseNavigation();
        this.bindEvents();
    },
    bindEvents: function() {
        this.getCurrentDate(transactDate);
        this.initializeComponent();
        this.view.lblBreadBoardHomeValue.onTouchEnd = this.navToDashboard;
        this.view.cusButtonCancel.onTouchEnd = this.navToDashboard;
        this.view.cusButtonSubmit.onTouchEnd = this.validateGivenDataAndPostCall;
        this.view.imgSearch.onTouchEnd = this.navToCustomerSearch;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.closeErrorAlert;
    },
    closeErrorAlert: function() {
        this.view.ErrorAllert.isVisible = false;
    },
    navToDashboard: function() {
        this.clearDataInComponent();
        var params = {};
        params.isPopupShow = "false";
        var navObject = new kony.mvc.Navigation("frmDashboard");
        navObject.navigate(params);
    },
    initializeComponent: function() {
        try {
            this.view.cusDropDownContractStatus.labelText = "Contact Status";
            this.view.cusDropDownContractStatus.options = '[{"label":"","value":""},{"label":"New","value":"NEW"},{"label":"Pending","value":"PENDING"},{"label":"Cancelled","value":"CANCELLED"}]';
            this.view.cusDropDownContractType.labelText = "Contact Type";
            this.view.cusDropDownContractType.options = '[{"label":"","value":""},{"value":"CALLCENTRE","label":"Call Centre"},{"value":"COMPLAINT","label":"Complaint"},{"value":"DIGITAL.MARKETING","label":"Digital Marketing"},{"value":"EMAIL","label":"Email"},{"value":"LETTER","label":"Letter"},{"value":"MEETING","label":"Meeting"},{"value":"NETBANKING","label":"NetBanking User"},{"value":"PERSONAL","label":"In Person"},{"value":"TELEPHONE","label":"Telephone"},{"value":"TEXT","label":"Text"}]';
            this.view.cusDropDownContractChannel.labelText = "Contact Channel";
            this.view.cusDropDownContractChannel.options = '[{"label":"","value":""},{"value":"BRANCH","label":"Branch"},{"value":"CALLCENTRE","label":"Call Centre"},{"value":"CHATBOT","label":"Chatbot"},{"value":"EMAIL","label":"E-mail"},{"value":"INTERNET.BANKING","label":"Internet Banking"},{"value":"MOBILE.BANKING","label":"Mobile Banking"},{"value":"NETBANKING","label":"NetBanking User"},{"value":"OTHER","label":"Other Channels"},{"value":"PERSONAL","label":"Personal Meeting"},{"value":"PHONE","label":"Phone"},{"value":"POST","label":"Post"},{"value":"SMS","label":"SMS"}]';
            this.view.ErrorAllert.isVisible = false;
            this.view.lblErrorMsg.isVisible = false;
            //this.view.cusTextAreaRequestDescription.validationMessage = "Please Enter the Request Description";
            var customerIdPopulate = Application.validation.isNullUndefinedObj(this.accountId);
            if (customerIdPopulate !== null || customerIdPopulate !== undefined || customerIdPopulate !== "") {
                this.view.cusTextContactClient.value = customerIdPopulate;
            }
        } catch (err) {
            kony.print("Error in CreateContractLog initializeComponent:::" + err);
        }
    },
    clearDataInComponent: function() {
        this.view.cusDropDownContractStatus.value = "";
        this.view.cusDropDownContractType.value = "";
        this.view.cusDropDownContractChannel.value = "";
        this.view.cusTextContactClient.value = "";
        this.view.cusTextAreaAdditionalNotes.value = "";
        this.view.cusTextAreaRequestDescription.value = "";
        this.view.cusTextContractTime.value = "";
    },
    navToCustomerSearch: function() {
        var params = {
            previousform: "createContractLog"
        };
        var navObject = new kony.mvc.Navigation("frmSearchCustomer");
        navObject.navigate(params);
    },
    getContractStaffDetails: function() {
        try {
            this.view.ProgressIndicator.isVisible = true;
            var serviceName = "LendingNew";
            var operationName = "getLoginPersonList";
            var headers = {};
            var inputParams = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgetContractStaffDetails, this.failureCBgetContractStaffDetails);
        } catch (err) {
            kony.print("Error in CreateContractLog getContractStaffDetails:::" + err);
        }
    },
    successCBgetContractStaffDetails: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false
            kony.print(" Success Response---->" + JSON.stringify(res));
            var responseBody = res.body;
            if (responseBody.length > 0) {
                var staffArray = [];
                staffArray.push({
                    "label": "",
                    "value": ""
                })
                for (var a in responseBody) {
                    var json = {
                        "label": responseBody[a].userName,
                        "value": responseBody[a].userId
                    };
                    staffArray.push(json);
                }
                kony.print("staffArray----->" + JSON.stringify(staffArray));
                this.view.cusDropDownContractStaff.labelText = "Contact Staff";
                this.view.cusDropDownContractStaff.options = JSON.stringify(staffArray);
                this.view.cusDropDownContractStaff.value = "";
                this.view.cusDropDownContractStaff.validateRequired = 'required';
                //this.resumeNavigation();
                //this.view.cusDropDownContractStaff.validateRequired = "required";
            }
        } catch (err) {
            kony.print("Error in CreateContractLog successCBgetContractStaffDetails:::" + err);
        }
    },
    failureCBgetContractStaffDetails: function(res) {
        try {
            this.view.ProgressIndicator.isVisible = false;
            //this.resumeNavigation();
            kony.print(" Failure Response---->" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in CreateContractLog failureCBgetContractStaffDetails:::" + err);
        }
    },
    getCurrentDate: function(transactDate) {
        try {
            var today = new Date(transactDate);
            var dd = today.getDate();
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var todayConverted = dd + "-" + shortMonth + "-" + yyyy;
            this.view.cusCalendarContractDate.displayFormat = "dd-MMM-yyyy";
            this.view.cusCalendarContractDate.value = todayConverted;
            this.view.cusCalendarContractDate.min = todayConverted;
        } catch (err) {
            kony.print("Error in CreateContractLog getCurrentDate:::" + err);
        }
    },
    validateGivenDataAndPostCall: function() {
        try {
            var isvalid = true;
            var contractTime = Application.validation.isNullUndefinedObj(this.view.cusTextContractTime.value);
            var isValidDate = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(contractTime);
            var contactClient = Application.validation.isNullUndefinedObj(this.view.cusTextContactClient.value);
            var requestedDesc = Application.validation.isNullUndefinedObj(this.view.cusTextAreaRequestDescription.value);
            var additionalNotes = Application.validation.isNullUndefinedObj(this.view.cusTextAreaAdditionalNotes.value);
            var contentType = Application.validation.isNullUndefinedObj(this.view.cusDropDownContractType.value);
            var contractChannel = Application.validation.isNullUndefinedObj(this.view.cusDropDownContractChannel.value);
            var contactDate = Application.validation.isNullUndefinedObj(this.view.cusCalendarContractDate.value);
            var formattedDate = this.formatDate(contactDate);
            var contractStaff = Application.validation.isNullUndefinedObj(this.view.cusDropDownContractStaff.value);
            var contractStatus = Application.validation.isNullUndefinedObj(this.view.cusDropDownContractStatus.value);
            if (contactClient === "") {
                isvalid = false;
            } else if (requestedDesc === "") {
                isvalid = false;
            } else if (contentType === "" || contentType === "select") {
                isvalid = false;
            } else if (contractChannel === "" || contractChannel === "select") {
                isvalid = false;
            } else if (contactDate === "") {
                isvalid = false;
            } else if (!isValidDate) {
                isvalid = false;
            } else if (contractStaff === "" || contractStaff === "select") {
                isvalid = false;
            } else if (contractStatus === "" || contractStatus === "select") {
                isvalid = false;
            }
            var isValidData = this.validateData();
            if (isValidData === 0) {
                this.view.lblErrorMsg.isVisible = false;
                this.view.ProgressIndicator.isVisible = true;
                var serviceName = "LendingNew";
                var operationName = "postNewContractLog";
                var headers = {};
                var inputParams = {};
                inputParams.customerId = contactClient;
                inputParams.contactType = contentType;
                inputParams.contactStatus = contractStatus;
                inputParams.contactDescription = requestedDesc;
                inputParams.contactStaff = contractStaff;
                inputParams.contactChannel = contractChannel;
                inputParams.contactDate = formattedDate;
                inputParams.contactTime = contractTime;
                MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBPostCreateContractLog, this.failureCBPostCreateContractLog);
            } else {
                this.view.lblErrorMsg.isVisible = true;
            }
        } catch (err) {
            kony.print("Error in CreateContractLog validateGivenDataAndPostCall:::" + err);
        }
    },
    formatDate: function(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return year + "" + month + "" + day;
    },
    successCBPostCreateContractLog: function(res) {
        try {
            //Application.loader.dismissLoader();
            this.clearDataInComponent();
            this.currentOverride = {};
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success CallBack :::" + JSON.stringify(res));
            if (res === "" || res === null || res === undefined) {
                kony.print("Response is null or undefined");
            } else {
                var resId = res.header;
                kony.print("RES___ID***" + JSON.stringify(resId));
                var data_id = Application.validation.isNullUndefinedObj(resId.id);
                //alert("New Contract Log Created Successfully!   Reference : "+data_id);
                //this.view.lblMessageWithReferenceNumber.text = "Interest Change applied successfully!      Reference : Interest Change applied successfully!      Reference : "+data_id;
                var navToChangeInterest1 = new kony.mvc.Navigation("frmDashboard");
                var navObjet = {};
                //navObjet.customerId = this._loanObj.customerId;
                //navObjet.isShowLoans = "true";
                navObjet.isPopupShow = "true";
                navObjet.messageInfo = "New Contact Log Created Successfully!   Reference : " + data_id;
                navToChangeInterest1.navigate(navObjet);
            }
        } catch (err) {
            kony.print("Error in CreateContractLog successCBPostCreateContractLog:::" + err);
        }
    },
    failureCBPostCreateContractLog: function(res) {
        try {
            this.currentOverride = {};
            //Application.loader.dismissLoader();
            this.view.ProgressIndicator.isVisible = false;
            //alert("Service failure");
            kony.print("Inside ErrorDetails=====>" + JSON.stringify(res.error.errorDetails));
            kony.print("Inside ErrorDetails2=====>" + JSON.stringify(Application.validation.isNullUndefinedObj(res.override)));
            if (Application.validation.isNullUndefinedObj(res.override) !== "") {
                var overwriteDetails = [];
                var overWriteData = [];
                var segDataForOverrite = [];
                this.currentOverride = res.override.overrideDetails;
                overwriteDetails = res.override.overrideDetails;
                if (overwriteDetails.length > 0) {
                    for (var a in overwriteDetails) {
                        overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                        var json = {
                            "lblSerial": "*",
                            "lblInfo": overwriteDetails[a].description
                        }
                        segDataForOverrite.push(json);
                    }
                    this.view.SegmentPopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.view.SegmentPopup.lblPaymentInfo.text = "override Confirmation";
                    this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
                    this.view.SegmentPopup.isVisible = true;
                    kony.print("overWriteData::::" + JSON.stringify(overWriteData));
                    kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
                    this.view.forceLayout();
                }
            }
            if (res.error.errorDetails !== []) { // !== "" || res.error.errorDetails !== null || res.error.errorDetails !== undefined || res.error.errorDetails !== []) {
                kony.print("Inside ErrorDetails=====>");
                this.view.ProgressIndicator.isVisible = false;
                if (res.error.errorDetails[0] === "" || res.error.errorDetails[0] === [] || res.error.errorDetails[0] === undefined || res.error.errorDetails[0] === null) {
                    kony.print("Inside ErrorDetails Empty=====>");
                } else {
                    this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message; //error.errorDetails[0].message
                    this.view.ErrorAllert.isVisible = true;
                    kony.print("Service Failure:::" + JSON.stringify(res.error.errorDetails));
                    this.view.flxHeaderMenu.isVisible = false;
                    //this.view.flxMessageInfo.isVisible = false;
                    this.view.forceLayout();
                }
            }
        } catch (err) {
            kony.print("Error in CreateContractLog failureCBPostCreateContractLog:::" + err);
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    validateData: function() {
        var Requiredflag = 0;
        var FormId = document.getElementsByTagName("form")[0].getAttribute("id");
        var form = document.getElementById(FormId);
        var Requiredfilter = form.querySelectorAll('*[required]');
        for (var i = 0; i < Requiredfilter.length; i++) {
            if (Requiredfilter[i].disabled !== true && Requiredfilter[i].value === '') {
                var ElementId = Requiredfilter[i].id;
                var Element = document.getElementById(ElementId);
                Element.reportValidity();
                Requiredflag = 1;
            }
        }
        return Requiredflag;
    },
});
define("frmCreateContractLogControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_g0dc773a41c94c5f9aaaa0086637ad4d: function AS_UWI_g0dc773a41c94c5f9aaaa0086637ad4d(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_fe2d9f3cb5114e7dbe2d8c6113b2ca05: function AS_UWI_fe2d9f3cb5114e7dbe2d8c6113b2ca05(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmCreateContractLogController", ["userfrmCreateContractLogController", "frmCreateContractLogControllerActions"], function() {
    var controller = require("userfrmCreateContractLogController");
    var controllerActions = ["frmCreateContractLogControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
