define("frmpopups", function() {
    return function(controller) {
        function addWidgetsfrmpopups() {
            this.setDefaultUnit(kony.flex.PX);
            var flxpop1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "220px",
                "id": "flxpop1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5px",
                "isModalContainer": false,
                "skin": "sknFlxWhiteRdCorners",
                "top": "0px",
                "width": "38%",
                "zIndex": 1
            }, {}, {});
            flxpop1.setDefaultUnit(kony.flex.DP);
            var btnViewDocuments = new kony.ui.Button({
                "bottom": "20px",
                "height": "40px",
                "id": "btnViewDocuments",
                "isVisible": true,
                "right": "40px",
                "skin": "CopybtnDefProfile4",
                "text": "Explore Now",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFullDetails = new kony.ui.Button({
                "bottom": "20px",
                "height": "40px",
                "id": "lblFullDetails",
                "isVisible": true,
                "left": "40px",
                "skin": "CopybtnDefProfile2",
                "text": "Later",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblmessage = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblmessage",
                "isVisible": true,
                "left": "0dp",
                "skin": "skn22Grey",
                "text": "Explore more lending services",
                "top": "34%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxpop1.add(btnViewDocuments, lblFullDetails, lblmessage);
            var flxsuccess = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "240px",
                "id": "flxsuccess",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxWhiteRdCorners",
                "top": "240px",
                "width": "38%",
                "zIndex": 1
            }, {}, {});
            flxsuccess.setDefaultUnit(kony.flex.DP);
            var btnok = new kony.ui.Button({
                "bottom": "20px",
                "centerX": "50%",
                "height": "40px",
                "id": "btnok",
                "isVisible": true,
                "onClick": controller.AS_Button_g2d5567449e342ab8e73d01984457d72,
                "right": "40px",
                "skin": "CopybtnDefProfile4",
                "text": "Ok",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblsuccesmessage = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblsuccesmessage",
                "isVisible": true,
                "left": "0dp",
                "skin": "skn22Grey",
                "text": "Payment Holiday applied successfully",
                "top": "42%",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgsuccess = new kony.ui.Image2({
                "centerX": "50%",
                "height": "50dp",
                "id": "imgsuccess",
                "isVisible": true,
                "skin": "slImage",
                "src": "success.png",
                "top": "10%",
                "width": "150dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxsuccess.add(btnok, lblsuccesmessage, imgsuccess);
            var flxRevisedPaymentSchedule = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxRevisedPaymentSchedule",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "680dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteRdCorners",
                "top": "60dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxRevisedPaymentSchedule.setDefaultUnit(kony.flex.DP);
            var Copyflxheader0e3f2de0659b74f = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "Copyflxheader0e3f2de0659b74f",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            Copyflxheader0e3f2de0659b74f.setDefaultUnit(kony.flex.DP);
            var CopylblRevisedPaymentSchedule0e00f500432d44d = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "CopylblRevisedPaymentSchedule0e00f500432d44d",
                "isVisible": true,
                "left": "20px",
                "skin": "skn22Grey",
                "text": "Skip Upcoming payments",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Copyblnclose0j1ad0326d44c4b = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50%",
                "id": "Copyblnclose0j1ad0326d44c4b",
                "isVisible": true,
                "right": "20px",
                "skin": "sknclose",
                "text": "Button",
                "top": 20,
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {});
            Copyflxheader0e3f2de0659b74f.add(CopylblRevisedPaymentSchedule0e00f500432d44d, Copyblnclose0j1ad0326d44c4b);
            var FlexGroup0d0971cfba59745 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "FlexGroup0d0971cfba59745",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            FlexGroup0d0971cfba59745.setDefaultUnit(kony.flex.DP);
            var lblRPSDate = new kony.ui.Label({
                "height": "44px",
                "id": "lblRPSDate",
                "isVisible": true,
                "left": 61,
                "skin": "skn15RegGr",
                "text": "Payment Date",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {});
            var lblRPSAmount = new kony.ui.Label({
                "height": "44px",
                "id": "lblRPSAmount",
                "isVisible": true,
                "left": 67,
                "skin": "skn15RegGr",
                "text": "Amount",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {});
            FlexGroup0d0971cfba59745.add(lblRPSDate, lblRPSAmount);
            var flxrow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxrow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxrow1.setDefaultUnit(kony.flex.DP);
            var btnSelect1 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "44dp",
                "id": "btnSelect1",
                "isVisible": true,
                "left": "2%",
                "skin": "sknicongreen",
                "text": "",
                "top": "0dp",
                "width": "44px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDate1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblDate1",
                "isVisible": true,
                "left": "2%",
                "skin": "sknBlue15px",
                "text": "19 Dec 2020",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblAmount1",
                "isVisible": true,
                "left": "2%",
                "skin": "skngreen15px",
                "text": "8,581.30",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxrow1.add(btnSelect1, lblDate1, lblAmount1);
            var flxrow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxrow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxrow2.setDefaultUnit(kony.flex.DP);
            var btnSelect2 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "44dp",
                "id": "btnSelect2",
                "isVisible": true,
                "left": "2%",
                "skin": "skniconGrey",
                "text": "",
                "top": "0dp",
                "width": "44px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDate2 = new kony.ui.Label({
                "height": "100%",
                "id": "lblDate2",
                "isVisible": true,
                "left": "2%",
                "skin": "sknBlue15px",
                "text": "19 Dec 2021",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount2 = new kony.ui.Label({
                "height": "100%",
                "id": "lblAmount2",
                "isVisible": true,
                "left": "2%",
                "skin": "skngreen15px",
                "text": "8,581.30",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxrow2.add(btnSelect2, lblDate2, lblAmount2);
            var flxrow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxrow3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxrow3.setDefaultUnit(kony.flex.DP);
            var btnSelect3 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "44dp",
                "id": "btnSelect3",
                "isVisible": true,
                "left": "2%",
                "skin": "skniconGrey",
                "text": "",
                "top": "0dp",
                "width": "44px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDate3 = new kony.ui.Label({
                "height": "100%",
                "id": "lblDate3",
                "isVisible": true,
                "left": "2%",
                "skin": "sknBlue15px",
                "text": "19 Dec 2021",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount3 = new kony.ui.Label({
                "height": "100%",
                "id": "lblAmount3",
                "isVisible": true,
                "left": "2%",
                "skin": "skngreen15px",
                "text": "8,581.30",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxrow3.add(btnSelect3, lblDate3, lblAmount3);
            var flxrow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxrow4",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxrow4.setDefaultUnit(kony.flex.DP);
            var btnSelect4 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "44dp",
                "id": "btnSelect4",
                "isVisible": true,
                "left": "2%",
                "skin": "skniconGrey",
                "text": "",
                "top": "0dp",
                "width": "44px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDate4 = new kony.ui.Label({
                "height": "100%",
                "id": "lblDate4",
                "isVisible": true,
                "left": "2%",
                "skin": "sknBlue15px",
                "text": "19 Mar 2021",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount4 = new kony.ui.Label({
                "height": "100%",
                "id": "lblAmount4",
                "isVisible": true,
                "left": "2%",
                "skin": "skngreen15px",
                "text": "8,581.30",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxrow4.add(btnSelect4, lblDate4, lblAmount4);
            var flxrow5 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxrow5",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxrow5.setDefaultUnit(kony.flex.DP);
            var btnSelect5 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "44dp",
                "id": "btnSelect5",
                "isVisible": true,
                "left": "2%",
                "skin": "skniconGrey",
                "text": "",
                "top": "0dp",
                "width": "44px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDate5 = new kony.ui.Label({
                "height": "100%",
                "id": "lblDate5",
                "isVisible": true,
                "left": "2%",
                "skin": "sknBlue15px",
                "text": "19 Apr 2021",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount5 = new kony.ui.Label({
                "height": "100%",
                "id": "lblAmount5",
                "isVisible": true,
                "left": "2%",
                "skin": "skngreen15px",
                "text": "8,581.30",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxrow5.add(btnSelect5, lblDate5, lblAmount5);
            var flxrow6 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxrow6",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxrow6.setDefaultUnit(kony.flex.DP);
            var btnSelect6 = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "44dp",
                "id": "btnSelect6",
                "isVisible": true,
                "left": "2%",
                "skin": "skniconGrey",
                "text": "",
                "top": "0dp",
                "width": "44px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDate6 = new kony.ui.Label({
                "height": "100%",
                "id": "lblDate6",
                "isVisible": true,
                "left": "2%",
                "skin": "sknBlue15px",
                "text": "12 Jun 2021",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount6 = new kony.ui.Label({
                "height": "100%",
                "id": "lblAmount6",
                "isVisible": true,
                "left": "2%",
                "skin": "skngreen15px",
                "text": "8,581.30",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxrow6.add(btnSelect6, lblDate6, lblAmount6);
            var flxbuttons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxbuttons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0bad2280f4bd345",
                "top": "20px",
                "width": "100%"
            }, {}, {});
            flxbuttons.setDefaultUnit(kony.flex.DP);
            var btnSimulate = new kony.ui.Button({
                "bottom": "20px",
                "height": "40px",
                "id": "btnSimulate",
                "isVisible": true,
                "right": "20px",
                "skin": "CopybtnDefProfile4",
                "text": "Simulate",
                "width": "180px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnCancel = new kony.ui.Button({
                "bottom": "20px",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "right": "2%",
                "skin": "CopybtnDefProfile2",
                "text": "Cancel",
                "width": "180px",
                "zIndex": 3
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxbuttons.add(btnSimulate, btnCancel);
            flxRevisedPaymentSchedule.add(Copyflxheader0e3f2de0659b74f, FlexGroup0d0971cfba59745, flxrow1, flxrow2, flxrow3, flxrow4, flxrow5, flxrow6, flxbuttons);
            var flxSchedule = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "500dp",
                "id": "flxSchedule",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "250dp",
                "isModalContainer": false,
                "skin": "sknFlxWhiteRdCorners",
                "top": "570dp",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxSchedule.setDefaultUnit(kony.flex.DP);
            var flxheader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxheader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxheader.setDefaultUnit(kony.flex.DP);
            var lblRevisedPaymentSchedule = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblRevisedPaymentSchedule",
                "isVisible": true,
                "left": "20px",
                "skin": "skn22Grey",
                "text": "Revised Payment Schedule",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var blnclose = new kony.ui.Button({
                "focusSkin": "defBtnFocus",
                "height": "50%",
                "id": "blnclose",
                "isVisible": true,
                "right": "20px",
                "skin": "sknclose",
                "text": "Button",
                "top": 20,
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {});
            var btnprint = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "defBtnFocus",
                "height": "50%",
                "id": "btnprint",
                "isVisible": true,
                "right": "60px",
                "skin": "sknPrint",
                "text": "Button",
                "top": "0px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxheader.add(lblRevisedPaymentSchedule, blnclose, btnprint);
            var flxScheduleheader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "44px",
                "id": "flxScheduleheader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "2%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "96%"
            }, {}, {});
            flxScheduleheader.setDefaultUnit(kony.flex.DP);
            var lblPaymentDate = new kony.ui.Label({
                "height": "44px",
                "id": "lblPaymentDate",
                "isVisible": true,
                "left": "0%",
                "skin": "skn15RegGr",
                "text": "Payment Date",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAmount = new kony.ui.Label({
                "height": "44px",
                "id": "lblAmount",
                "isVisible": true,
                "left": "2%",
                "skin": "skn15RegGr",
                "text": "Amount",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPrincipal = new kony.ui.Label({
                "height": "44px",
                "id": "lblPrincipal",
                "isVisible": true,
                "left": "2%",
                "skin": "skn15RegGr",
                "text": "Principal",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInterest = new kony.ui.Label({
                "height": "44px",
                "id": "lblInterest",
                "isVisible": true,
                "left": "2%",
                "skin": "skn15RegGr",
                "text": "Interest",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTax = new kony.ui.Label({
                "height": "44px",
                "id": "lblTax",
                "isVisible": true,
                "left": "2%",
                "skin": "skn15RegGr",
                "text": "Tax",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltotaloutstanding = new kony.ui.Label({
                "height": "44px",
                "id": "lbltotaloutstanding",
                "isVisible": true,
                "left": "2%",
                "skin": "skn15RegGr",
                "text": "Total Outstanding",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScheduleheader.add(lblPaymentDate, lblAmount, lblPrincipal, lblInterest, lblTax, lbltotaloutstanding);
            var Segment0c0e1d9bbba4c4c = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }, {
                    "lblAmount": "0.00",
                    "lblInterest": "0.00%",
                    "lblPaymentDate": "19 Dec 2020",
                    "lblPrincipal": "0.00",
                    "lblTax": "0.00",
                    "lbltotaloutstanding": "0.00"
                }],
                "groupCells": false,
                "id": "Segment0c0e1d9bbba4c4c",
                "isVisible": true,
                "left": "2%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "Flex0i494021d9e244d",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ffffff00",
                "separatorRequired": true,
                "separatorThickness": 10,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "Flex0i494021d9e244d": "Flex0i494021d9e244d",
                    "flxScheduleheader": "flxScheduleheader",
                    "lblAmount": "lblAmount",
                    "lblInterest": "lblInterest",
                    "lblPaymentDate": "lblPaymentDate",
                    "lblPrincipal": "lblPrincipal",
                    "lblTax": "lblTax",
                    "lbltotaloutstanding": "lbltotaloutstanding"
                },
                "width": "96%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSchedule.add(flxheader, flxScheduleheader, Segment0c0e1d9bbba4c4c);
            this.compInstData = {}
            this.add(flxpop1, flxsuccess, flxRevisedPaymentSchedule, flxSchedule);
        };
        return [{
            "addWidgets": addWidgetsfrmpopups,
            "enabledForIdleTimeout": false,
            "id": "frmpopups",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "slForm",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});