define("CopyFlxAccountList", function() {
    return function(controller) {
        var CopyFlxAccountList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "CopyFlxAccountList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        CopyFlxAccountList.setDefaultUnit(kony.flex.DP);
        var lblAccountId = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblAccountId",
            "isVisible": true,
            "left": "2%",
            "skin": "CopysknflxFont",
            "text": "-",
            "top": "0",
            "width": "31.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbltype = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lbltype",
            "isVisible": true,
            "left": "0",
            "skin": "CopysknflxFont",
            "text": "-",
            "top": "0",
            "width": "33.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblWorkingBal = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblWorkingBal",
            "isVisible": true,
            "left": "0",
            "skin": "CopysknflxFontD",
            "text": "Final",
            "top": "0",
            "width": "33.30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        CopyFlxAccountList.add(lblAccountId, lbltype, lblWorkingBal);
        return CopyFlxAccountList;
    }
})