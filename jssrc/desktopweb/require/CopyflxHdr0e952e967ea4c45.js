define("CopyflxHdr0e952e967ea4c45", function() {
    return function(controller) {
        var CopyflxHdr0e952e967ea4c45 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "CopyflxHdr0e952e967ea4c45",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        CopyflxHdr0e952e967ea4c45.setDefaultUnit(kony.flex.DP);
        var lblGroup = new kony.ui.Label({
            "bottom": "6px",
            "id": "lblGroup",
            "isVisible": true,
            "left": "20px",
            "skin": "CopysknLblRowHeading0e214d37a97604c",
            "text": "101146",
            "top": "6px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblExpires = new kony.ui.Label({
            "bottom": "6px",
            "height": "100%",
            "id": "lblExpires",
            "isVisible": true,
            "left": "100px",
            "skin": "Copyskn0g32621a463e84a",
            "text": "Requested new card",
            "top": "5px",
            "width": "150px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "bottom": "6px",
            "id": "lblDate",
            "isVisible": true,
            "left": "270px",
            "skin": "skn15RegGr",
            "text": "Nov 22 10:10AM",
            "top": "6px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblChannel = new kony.ui.Label({
            "bottom": "6px",
            "id": "lblChannel",
            "isVisible": true,
            "left": "400px",
            "skin": "skn15RegGr",
            "text": "OLB",
            "top": "6px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        CopyflxHdr0e952e967ea4c45.add(lblGroup, lblExpires, lblDate, lblChannel);
        return CopyflxHdr0e952e967ea4c45;
    }
})