define("userflxMoreHeaderController", {
    //Type your controller code here 
    onQuesSectionClick: function(context) {
        this.executeOnParent("quesSectionClicked", context);
    },
});
define("flxMoreHeaderControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_db7214693412436293d629e64223d100: function AS_FlexContainer_db7214693412436293d629e64223d100(eventobject, context) {
        var self = this;
        return self.onQuesSectionClick.call(this, context);
    }
});
define("flxMoreHeaderController", ["userflxMoreHeaderController", "flxMoreHeaderControllerActions"], function() {
    var controller = require("userflxMoreHeaderController");
    var controllerActions = ["flxMoreHeaderControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
