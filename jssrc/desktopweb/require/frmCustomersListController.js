define("userfrmCustomersListController", {
    //Type your controller code here 
    onNavigate: function() {
        this.view.seg1.setVisibility(true);
        this.view.seg1.onRowClick = this.onRowClickSeg1;
        this.doServiecCallAndpopulateList();
    },
    doServiecCallAndpopulateList: function() {
        var serviceName = "LendingNew";
        var operationName = "getCustomers";
        var inputParams = {};
        var headers = {};
        kony.application.showLoadingScreen("blockUI3", "Loading", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {});
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.getcustomersSCB, this.getCustomersFCB);
    },
    getcustomersSCB: function(response) {
        var dataMap = {
            "lbl1": "customerId",
            "lbl2": "customerName",
            "lbl3": "statusName",
            "lbl4": "accountOfficerName"
        };
        this.view.seg1.widgetDataMap = dataMap;
        this.view.seg1.setData(response.body);
        this.view.seg1.setVisibility(true);
        kony.application.dismissLoadingScreen();
    },
    getcustomersFCB: function(response) {
        kony.application.dismissLoadingScreen();
        alert("error in searhing customers");
    },
    onRowClickSeg1: function(eventobject, sectionNumber, rowNumber) {
        var customerId = this.view.seg1.data[rowNumber].customerId; //this.view.segMin2.data[0];//selectedRowItems[0];  
        var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        var navObjet = {};
        navObjet.customerId = customerId;
        navObjet.isShowLoans = "true";
        navToChangeInterest1.navigate(navObjet);
    }
});
define("frmCustomersListControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("frmCustomersListController", ["userfrmCustomersListController", "frmCustomersListControllerActions"], function() {
    var controller = require("userfrmCustomersListController");
    var controllerActions = ["frmCustomersListControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
