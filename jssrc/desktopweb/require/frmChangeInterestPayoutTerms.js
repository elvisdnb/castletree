define("frmChangeInterestPayoutTerms", function() {
    return function(controller) {
        function addWidgetsfrmChangeInterestPayoutTerms() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMainContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5%",
                "pagingEnabled": false,
                "right": "24dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopysknFlxContainerBGF0a6fcc72d729345",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxHeaderMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxHeaderMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxHeaderMenu.setDefaultUnit(kony.flex.DP);
            var cusFillDetailsButton = new kony.ui.CustomWidget({
                "id": "cusFillDetailsButton",
                "isVisible": false,
                "right": "0dp",
                "top": "16dp",
                "width": "152dp",
                "height": "40dp",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "filter_list",
                "labelText": "Full Details",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var cusViewDocumentButton = new kony.ui.CustomWidget({
                "id": "cusViewDocumentButton",
                "isVisible": false,
                "right": "150dp",
                "top": "16dp",
                "width": "170dp",
                "height": "40dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": true,
                "cta": false,
                "disabled": false,
                "icon": "text_snippet",
                "labelText": "View Documents",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": true,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "button"
                }
            });
            var flxBack = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxBack",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5%",
                "width": "200dp",
                "zIndex": 5
            }, {}, {});
            flxBack.setDefaultUnit(kony.flex.DP);
            var cusBackIcon = new kony.ui.CustomWidget({
                "id": "cusBackIcon",
                "isVisible": true,
                "left": "27dp",
                "top": "24dp",
                "width": "24dp",
                "height": "24dp",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxIcon",
                "color": "temenos-dark",
                "iconName": "chevron_left",
                "size": "small"
            });
            var lblBreadBoardHomeValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardHomeValue",
                "isVisible": true,
                "left": "59dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "Home",
                "top": "26dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardSplashValue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardSplashValue",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknLbl12pxBG005AA0",
                "text": "/",
                "top": "25dp",
                "width": "6dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadBoardOverviewvalue = new kony.ui.Label({
                "height": "18dp",
                "id": "lblBreadBoardOverviewvalue",
                "isVisible": true,
                "left": "112dp",
                "skin": "sknLbl12pxBG4D4D4D",
                "text": "Overview",
                "top": "26dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBack.add(cusBackIcon, lblBreadBoardHomeValue, lblBreadBoardSplashValue, lblBreadBoardOverviewvalue);
            flxHeaderMenu.add(cusFillDetailsButton, cusViewDocumentButton, flxBack);
            var custInfo = new com.lending.customerDetails.custInfo({
                "height": "157dp",
                "id": "custInfo",
                "isVisible": true,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBorderCCE3F7",
                "top": "70dp",
                "width": "98%",
                "zIndex": 1,
                "overrides": {
                    "custInfo": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ErrorAllert = new com.lending.FlxErrorAllert.ErrorAllert.ErrorAllert({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "72dp",
                "id": "ErrorAllert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 32,
                "skin": "CopyslFbox0i274505141e54c",
                "top": "0dp",
                "width": "98%",
                "overrides": {
                    "ErrorAllert": {
                        "isVisible": false,
                        "width": "98%"
                    },
                    "imgCloseBackup": {
                        "left": "1230dp",
                        "src": "ico_close.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxChangeInterestWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": false,
                "height": "450dp",
                "id": "flxChangeInterestWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "isModalContainer": false,
                "right": "24dp",
                "skin": "CopysknFlxMultiBGCBE0i3354513ab0840",
                "top": "420dp",
                "width": "98%",
                "zIndex": 2
            }, {}, {});
            flxChangeInterestWrapper.setDefaultUnit(kony.flex.DP);
            var flxChangeInterest = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxChangeInterest",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "1250dp",
                "zIndex": 1
            }, {}, {});
            flxChangeInterest.setDefaultUnit(kony.flex.DP);
            var TitleLabel = new com.lending.FlxTitle.titleLabel.TitleLabel({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "TitleLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "minHeight": "56dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "TitleLabel": {
                        "centerX": "viz.val_cleared",
                        "height": "70dp",
                        "isVisible": true
                    },
                    "flxTitle": {
                        "height": "2dp"
                    },
                    "lblTitle": {
                        "centerX": "viz.val_cleared",
                        "left": "150dp",
                        "text": "Change Interest Pay-out Terms"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblSelectModeInfo = new kony.ui.Label({
                "height": "30dp",
                "id": "lblSelectModeInfo",
                "isVisible": true,
                "left": "12%",
                "skin": "CopydefLabel0g1225c742c9844",
                "text": "Interest on Maturity",
                "top": "130dp",
                "width": "14%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var cusRadioChangeInterestType = new kony.ui.CustomWidget({
                "id": "cusRadioChangeInterestType",
                "isVisible": true,
                "left": "27%",
                "top": "120dp",
                "width": "15%",
                "height": "60dp",
                "zIndex": 1,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRadioGroup",
                "labelText": "",
                "options": {
                    "selectedValue": ""
                },
                "value": ""
            });
            var cusInterestPayoutFrequency = new kony.ui.CustomWidget({
                "id": "cusInterestPayoutFrequency",
                "isVisible": true,
                "left": "43%",
                "top": "123dp",
                "width": "25%",
                "height": "60dp",
                "zIndex": 2,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxRecurrenceDatePicker",
                "dailyMax": "",
                "dense": true,
                "formattedValue": "",
                "lableText": "Interest Payout Frequency",
                "messagePrefix": "",
                "monthlyDayMax": "",
                "monthlyMax": "",
                "value": "",
                "weeklyMax": "",
                "yearlyDayMax": "",
                "yearlyMax": ""
            });
            var cusPaymentMethod = new kony.ui.CustomWidget({
                "id": "cusPaymentMethod",
                "isVisible": true,
                "left": "69%",
                "top": "123dp",
                "width": "291dp",
                "height": "56dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxDropdown",
                "compact": "",
                "dense": "dense",
                "disabled": false,
                "labelText": "Payment Method",
                "options": "",
                "readonly": true,
                "validateRequired": "required",
                "value": ""
            });
            var lblErrorMsg = new kony.ui.Label({
                "height": "50dp",
                "id": "lblErrorMsg",
                "isVisible": false,
                "left": "150dp",
                "skin": "sknLabelErrorMessage",
                "text": "* All mandatory fields need to be filled",
                "top": "230dp",
                "width": "280dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "1%",
                "skin": "slFbox",
                "top": "250dp",
                "width": "40%",
                "zIndex": kony.flex.ZINDEX_AUTO
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var flxCancel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": false,
                "height": "40dp",
                "id": "flxCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "1dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "216dp",
                "zIndex": 1
            }, {}, {});
            flxCancel.setDefaultUnit(kony.flex.DP);
            var cusButtonCancel = new kony.ui.CustomWidget({
                "id": "cusButtonCancel",
                "isVisible": true,
                "left": "15dp",
                "top": "0dp",
                "width": "180dp",
                "height": "40dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": false
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": false,
                "disabled": false,
                "icon": "",
                "labelText": "Cancel",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxCancel.add(cusButtonCancel);
            var flxSubmit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": false,
                "height": "40dp",
                "id": "flxSubmit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "216dp",
                "zIndex": 1
            }, {}, {});
            flxSubmit.setDefaultUnit(kony.flex.DP);
            var cusButtonConfirm = new kony.ui.CustomWidget({
                "id": "cusButtonConfirm",
                "isVisible": true,
                "left": "15dp",
                "top": "0dp",
                "width": "210dp",
                "height": "40dp",
                "zIndex": kony.flex.ZINDEX_AUTO,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "uuxButton",
                "compact": false,
                "contained": false,
                "cta": true,
                "disabled": false,
                "icon": "",
                "labelText": " Confirm      ",
                "outlined": true,
                "size": {
                    "optionsList": ["small", "medium", "large", "X-large"],
                    "selectedValue": "large"
                },
                "trailingIcon": false,
                "typeButton": {
                    "optionsList": ["button", "submit", "reset"],
                    "selectedValue": "submit"
                }
            });
            flxSubmit.add(cusButtonConfirm);
            flxButtons.add(flxCancel, flxSubmit);
            flxChangeInterest.add(TitleLabel, lblSelectModeInfo, cusRadioChangeInterestType, cusInterestPayoutFrequency, cusPaymentMethod, lblErrorMsg, flxButtons);
            flxChangeInterestWrapper.add(flxChangeInterest);
            var MainTabs = new com.lending.MainTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "64dp",
                "id": "MainTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxContainerBGF2F7FD",
                "top": "227dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "MainTabs": {
                        "left": "0dp",
                        "top": "227dp",
                        "width": "100%"
                    },
                    "btnAccounts1": {
                        "left": 40
                    },
                    "flxContainerAccount": {
                        "left": "27dp"
                    },
                    "flxContainerdeposits": {
                        "left": "142dp"
                    },
                    "flxontainerLoans": {
                        "left": "247dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var loanDetails = new com.lending.loanDetails.loanDetails({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "128dp",
                "id": "loanDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "18dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "291dp",
                "width": "98%",
                "overrides": {
                    "FlexContainer0ebe191bc1cf946": {
                        "left": "-200dp",
                        "width": "270dp"
                    },
                    "flxListBoxSelectedValue": {
                        "minWidth": "viz.val_cleared",
                        "width": "17%"
                    },
                    "flxLoanDetailsvalue": {
                        "height": "63dp"
                    },
                    "imgListBoxSelectedDropDown": {
                        "isVisible": false,
                        "left": "58dp",
                        "src": "drop_down_arrow.png"
                    },
                    "lblAccountNumberInfo": {
                        "width": "12.50%"
                    },
                    "lblAccountNumberResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblAccountServiceInfo": {
                        "text": "Deposit Services",
                        "width": "12.50%"
                    },
                    "lblAmountValueInfo": {
                        "width": "12.50%"
                    },
                    "lblAmountValueResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblCurrencyValueInfo": {
                        "width": "8%"
                    },
                    "lblCurrencyValueResponse": {
                        "centerY": "50%",
                        "width": "8%"
                    },
                    "lblListBoxSelectedValue": {
                        "left": "5dp",
                        "text": "Change Interest Payout Terms"
                    },
                    "lblLoanTypeInfo": {
                        "text": " Type",
                        "width": "12.50%"
                    },
                    "lblLoanTypeResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "lblMaturityDateInfo": {
                        "width": "12%"
                    },
                    "lblMaturityDateResponse": {
                        "centerY": "50%",
                        "width": "12%"
                    },
                    "lblStartDateInfo": {
                        "width": "12.50%"
                    },
                    "lblStartDateResponse": {
                        "centerY": "50%",
                        "width": "12.50%"
                    },
                    "loanDetails": {
                        "height": "128dp",
                        "left": "18dp",
                        "top": "291dp",
                        "width": "98%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainContainer.add(flxHeaderMenu, custInfo, ErrorAllert, flxChangeInterestWrapper, MainTabs, loanDetails);
            var uuxNavigationRail = new uuxNavigationRailComponent.uuxNavigationRail({
                "height": "100%",
                "id": "uuxNavigationRail",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "CopyslFbox0c696e058632d4d",
                "top": "0dp",
                "width": "64dp",
                "overrides": {
                    "uuxNavigationRail": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ProgressIndicator = new com.lending.uuxProgressIndicator.ProgressIndicator({
                "height": "100%",
                "id": "ProgressIndicator",
                "isVisible": true,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknlblBlockedSeduled",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ProgressIndicator": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            this.breakpointResetData = {};
            this.breakpointData = {
                maxBreakpointWidth: 1400,
            }
            this.compInstData = {
                "custInfo": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ErrorAllert": {
                    "width": "98%"
                },
                "ErrorAllert.imgCloseBackup": {
                    "left": "1230dp",
                    "src": "ico_close.png"
                },
                "TitleLabel": {
                    "centerX": "",
                    "height": "70dp"
                },
                "TitleLabel.flxTitle": {
                    "height": "2dp"
                },
                "TitleLabel.lblTitle": {
                    "centerX": "",
                    "left": "150dp",
                    "text": "Change Interest Pay-out Terms"
                },
                "MainTabs": {
                    "left": "0dp",
                    "top": "227dp",
                    "width": "100%"
                },
                "MainTabs.btnAccounts1": {
                    "left": 40
                },
                "MainTabs.flxContainerAccount": {
                    "left": "27dp"
                },
                "MainTabs.flxContainerdeposits": {
                    "left": "142dp"
                },
                "MainTabs.flxontainerLoans": {
                    "left": "247dp"
                },
                "loanDetails.FlexContainer0ebe191bc1cf946": {
                    "left": "-200dp",
                    "width": "270dp"
                },
                "loanDetails.flxListBoxSelectedValue": {
                    "minWidth": "",
                    "width": "17%"
                },
                "loanDetails.flxLoanDetailsvalue": {
                    "height": "63dp"
                },
                "loanDetails.imgListBoxSelectedDropDown": {
                    "left": "58dp",
                    "src": "drop_down_arrow.png"
                },
                "loanDetails.lblAccountNumberInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAccountNumberResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblAccountServiceInfo": {
                    "text": "Deposit Services",
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblAmountValueResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblCurrencyValueInfo": {
                    "width": "8%"
                },
                "loanDetails.lblCurrencyValueResponse": {
                    "centerY": "50%",
                    "width": "8%"
                },
                "loanDetails.lblListBoxSelectedValue": {
                    "left": "5dp",
                    "text": "Change Interest Payout Terms"
                },
                "loanDetails.lblLoanTypeInfo": {
                    "text": " Type",
                    "width": "12.50%"
                },
                "loanDetails.lblLoanTypeResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails.lblMaturityDateInfo": {
                    "width": "12%"
                },
                "loanDetails.lblMaturityDateResponse": {
                    "centerY": "50%",
                    "width": "12%"
                },
                "loanDetails.lblStartDateInfo": {
                    "width": "12.50%"
                },
                "loanDetails.lblStartDateResponse": {
                    "centerY": "50%",
                    "width": "12.50%"
                },
                "loanDetails": {
                    "height": "128dp",
                    "left": "18dp",
                    "top": "291dp",
                    "width": "98%"
                },
                "uuxNavigationRail": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                },
                "ProgressIndicator": {
                    "right": "",
                    "bottom": "",
                    "minWidth": "",
                    "minHeight": "",
                    "maxWidth": "",
                    "maxHeight": "",
                    "centerX": "",
                    "centerY": ""
                }
            }
            this.add(flxMainContainer, uuxNavigationRail, ProgressIndicator);
        };
        return [{
            "addWidgets": addWidgetsfrmChangeInterestPayoutTerms,
            "enabledForIdleTimeout": false,
            "id": "frmChangeInterestPayoutTerms",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "skin": "CopyslForm0jb8b54adac7b49",
            "onBreakpointHandler": onBreakpointHandler,
            "breakpoints": [640, 1024, 1366, 1400]
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});