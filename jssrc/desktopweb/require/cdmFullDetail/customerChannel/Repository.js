define([], function() {
    var BaseRepository = kony.mvc.Data.BaseRepository;
    //Create the Repository Class
    function customerChannelRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
        BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
    };
    //Setting BaseRepository as Parent to this Repository
    customerChannelRepository.prototype = Object.create(BaseRepository.prototype);
    customerChannelRepository.prototype.constructor = customerChannelRepository;
    //For Operation 'getCustomerChannels' with service id 'getCustomerPersonalProfiles3533'
    customerChannelRepository.prototype.getCustomerChannels = function(params, onCompletion) {
        return customerChannelRepository.prototype.customVerb('getCustomerChannels', params, onCompletion);
    };
    return customerChannelRepository;
})