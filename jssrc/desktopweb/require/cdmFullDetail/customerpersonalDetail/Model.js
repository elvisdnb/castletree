/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {
        "object": "customerpersonalDetail",
        "objectService": "cdmFullDetail"
    };
    var setterFunctions = {
        customerId: function(val, state) {
            context["field"] = "customerId";
            context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
            state['customerId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        isInternetBanking: function(val, state) {
            context["field"] = "isInternetBanking";
            context["metadata"] = (objectMetadata ? objectMetadata["isInternetBanking"] : null);
            state['isInternetBanking'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        isMobileBanking: function(val, state) {
            context["field"] = "isMobileBanking";
            context["metadata"] = (objectMetadata ? objectMetadata["isMobileBanking"] : null);
            state['isMobileBanking'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        isSecureMessage: function(val, state) {
            context["field"] = "isSecureMessage";
            context["metadata"] = (objectMetadata ? objectMetadata["isSecureMessage"] : null);
            state['isSecureMessage'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };
    //Create the Model Class
    function customerpersonalDetail(defaultValues) {
        var privateState = {};
        context["field"] = "customerId";
        context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
        privateState.customerId = defaultValues ? (defaultValues["customerId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerId"], context) : null) : null;
        context["field"] = "isInternetBanking";
        context["metadata"] = (objectMetadata ? objectMetadata["isInternetBanking"] : null);
        privateState.isInternetBanking = defaultValues ? (defaultValues["isInternetBanking"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isInternetBanking"], context) : null) : null;
        context["field"] = "isMobileBanking";
        context["metadata"] = (objectMetadata ? objectMetadata["isMobileBanking"] : null);
        privateState.isMobileBanking = defaultValues ? (defaultValues["isMobileBanking"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isMobileBanking"], context) : null) : null;
        context["field"] = "isSecureMessage";
        context["metadata"] = (objectMetadata ? objectMetadata["isSecureMessage"] : null);
        privateState.isSecureMessage = defaultValues ? (defaultValues["isSecureMessage"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isSecureMessage"], context) : null) : null;
        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);
        //Defining Getter/Setters
        Object.defineProperties(this, {
            "customerId": {
                get: function() {
                    context["field"] = "customerId";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerId, context);
                },
                set: function(val) {
                    setterFunctions['customerId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "isInternetBanking": {
                get: function() {
                    context["field"] = "isInternetBanking";
                    context["metadata"] = (objectMetadata ? objectMetadata["isInternetBanking"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isInternetBanking, context);
                },
                set: function(val) {
                    setterFunctions['isInternetBanking'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "isMobileBanking": {
                get: function() {
                    context["field"] = "isMobileBanking";
                    context["metadata"] = (objectMetadata ? objectMetadata["isMobileBanking"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isMobileBanking, context);
                },
                set: function(val) {
                    setterFunctions['isMobileBanking'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "isSecureMessage": {
                get: function() {
                    context["field"] = "isSecureMessage";
                    context["metadata"] = (objectMetadata ? objectMetadata["isSecureMessage"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isSecureMessage, context);
                },
                set: function(val) {
                    setterFunctions['isSecureMessage'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });
        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };
        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.customerId = value ? (value["customerId"] ? value["customerId"] : null) : null;
            privateState.isInternetBanking = value ? (value["isInternetBanking"] ? value["isInternetBanking"] : null) : null;
            privateState.isMobileBanking = value ? (value["isMobileBanking"] ? value["isMobileBanking"] : null) : null;
            privateState.isSecureMessage = value ? (value["isSecureMessage"] ? value["isSecureMessage"] : null) : null;
        };
    }
    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(customerpersonalDetail);
    //Create new class level validator object
    BaseModel.Validator.call(customerpersonalDetail);
    var registerValidatorBackup = customerpersonalDetail.registerValidator;
    customerpersonalDetail.registerValidator = function() {
            var propName = arguments[0];
            if (!setterFunctions[propName].changed) {
                var setterBackup = setterFunctions[propName];
                setterFunctions[arguments[0]] = function() {
                    if (customerpersonalDetail.isValid(this, propName, val)) {
                        return setterBackup.apply(null, arguments);
                    } else {
                        throw Error("Validation failed for " + propName + " : " + val);
                    }
                }
                setterFunctions[arguments[0]].changed = true;
            }
            return registerValidatorBackup.apply(null, arguments);
        }
        //Extending Model for custom operations
        //For Operation 'getCDMCustomerProfile' with service id 'getCustomerPersonalProfiles1888'
    customerpersonalDetail.getCDMCustomerProfile = function(params, onCompletion) {
        return customerpersonalDetail.customVerb('getCDMCustomerProfile', params, onCompletion);
    };
    var relations = [];
    customerpersonalDetail.relations = relations;
    customerpersonalDetail.prototype.isValid = function() {
        return customerpersonalDetail.isValid(this);
    };
    customerpersonalDetail.prototype.objModelName = "customerpersonalDetail";
    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    customerpersonalDetail.registerProcessors = function(options, successCallback, failureCallback) {
        if (!options) {
            options = {};
        }
        if (options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }
        if (options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }
        kony.mvc.util.ProcessorUtils.getMetadataForObject("cdmFullDetail", "customerpersonalDetail", options, metaDataSuccess, metaDataFailure);
    };
    //clone the object provided in argument.
    customerpersonalDetail.clone = function(objectToClone) {
        var clonedObj = new customerpersonalDetail();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };
    return customerpersonalDetail;
});