define([], function() {
    var BaseRepository = kony.mvc.Data.BaseRepository;
    //Create the Repository Class
    function customerpersonalDetailRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
        BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
    };
    //Setting BaseRepository as Parent to this Repository
    customerpersonalDetailRepository.prototype = Object.create(BaseRepository.prototype);
    customerpersonalDetailRepository.prototype.constructor = customerpersonalDetailRepository;
    //For Operation 'getCDMCustomerProfile' with service id 'getCustomerPersonalProfiles1888'
    customerpersonalDetailRepository.prototype.getCDMCustomerProfile = function(params, onCompletion) {
        return customerpersonalDetailRepository.prototype.customVerb('getCDMCustomerProfile', params, onCompletion);
    };
    return customerpersonalDetailRepository;
})