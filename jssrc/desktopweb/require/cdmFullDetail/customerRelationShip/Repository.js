define([], function() {
    var BaseRepository = kony.mvc.Data.BaseRepository;
    //Create the Repository Class
    function customerRelationShipRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
        BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
    };
    //Setting BaseRepository as Parent to this Repository
    customerRelationShipRepository.prototype = Object.create(BaseRepository.prototype);
    customerRelationShipRepository.prototype.constructor = customerRelationShipRepository;
    //For Operation 'getCustomerRelationShip' with service id 'getPartyRelationship5661'
    customerRelationShipRepository.prototype.getCustomerRelationShip = function(params, onCompletion) {
        return customerRelationShipRepository.prototype.customVerb('getCustomerRelationShip', params, onCompletion);
    };
    //For Operation 'getCDMCustomerRelationShip' with service id 'getPartyRelationship1918'
    customerRelationShipRepository.prototype.getCDMCustomerRelationShip = function(params, onCompletion) {
        return customerRelationShipRepository.prototype.customVerb('getCDMCustomerRelationShip', params, onCompletion);
    };
    return customerRelationShipRepository;
})