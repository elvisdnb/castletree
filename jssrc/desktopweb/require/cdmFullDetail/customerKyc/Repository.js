define([], function() {
    var BaseRepository = kony.mvc.Data.BaseRepository;
    //Create the Repository Class
    function customerKycRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
        BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
    };
    //Setting BaseRepository as Parent to this Repository
    customerKycRepository.prototype = Object.create(BaseRepository.prototype);
    customerKycRepository.prototype.constructor = customerKycRepository;
    //For Operation 'getCKYC' with service id 'getCustomerReportingStatus8128'
    customerKycRepository.prototype.getCKYC = function(params, onCompletion) {
        return customerKycRepository.prototype.customVerb('getCKYC', params, onCompletion);
    };
    return customerKycRepository;
})