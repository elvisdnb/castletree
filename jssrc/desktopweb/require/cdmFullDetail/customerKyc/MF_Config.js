/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var mappings = {
        "customerId": "customerId",
        "postingRestriction": "postingRestriction",
        "amlRisk": "amlRisk",
        "crsJurisdiction": "crsJurisdiction",
        "crsStatus": "crsStatus",
        "factaStatus": "factaStatus",
    };
    Object.freeze(mappings);
    var typings = {
        "customerId": "string",
        "postingRestriction": "string",
        "amlRisk": "string",
        "crsJurisdiction": "string",
        "crsStatus": "string",
        "factaStatus": "string",
    }
    Object.freeze(typings);
    var primaryKeys = ["customerId", ];
    Object.freeze(primaryKeys);
    var config = {
        mappings: mappings,
        typings: typings,
        primaryKeys: primaryKeys,
        serviceName: "cdmFullDetail",
        tableName: "customerKyc"
    };
    Object.freeze(config);
    return config;
})