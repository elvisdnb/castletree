/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {
        "object": "customerDocumentUpload",
        "objectService": "cdmFullDetail"
    };
    var setterFunctions = {
        customerId: function(val, state) {
            context["field"] = "customerId";
            context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
            state['customerId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        imageType: function(val, state) {
            context["field"] = "imageType";
            context["metadata"] = (objectMetadata ? objectMetadata["imageType"] : null);
            state['imageType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        imageApplication: function(val, state) {
            context["field"] = "imageApplication";
            context["metadata"] = (objectMetadata ? objectMetadata["imageApplication"] : null);
            state['imageApplication'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        multiMediaType: function(val, state) {
            context["field"] = "multiMediaType";
            context["metadata"] = (objectMetadata ? objectMetadata["multiMediaType"] : null);
            state['multiMediaType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        displayName: function(val, state) {
            context["field"] = "displayName";
            context["metadata"] = (objectMetadata ? objectMetadata["displayName"] : null);
            state['displayName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        description: function(val, state) {
            context["field"] = "description";
            context["metadata"] = (objectMetadata ? objectMetadata["description"] : null);
            state['description'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        id: function(val, state) {
            context["field"] = "id";
            context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
            state['id'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        status: function(val, state) {
            context["field"] = "status";
            context["metadata"] = (objectMetadata ? objectMetadata["status"] : null);
            state['status'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        fileName: function(val, state) {
            context["field"] = "fileName";
            context["metadata"] = (objectMetadata ? objectMetadata["fileName"] : null);
            state['fileName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        imageContent: function(val, state) {
            context["field"] = "imageContent";
            context["metadata"] = (objectMetadata ? objectMetadata["imageContent"] : null);
            state['imageContent'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        imagePath: function(val, state) {
            context["field"] = "imagePath";
            context["metadata"] = (objectMetadata ? objectMetadata["imagePath"] : null);
            state['imagePath'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };
    //Create the Model Class
    function customerDocumentUpload(defaultValues) {
        var privateState = {};
        context["field"] = "customerId";
        context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
        privateState.customerId = defaultValues ? (defaultValues["customerId"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerId"], context) : null) : null;
        context["field"] = "imageType";
        context["metadata"] = (objectMetadata ? objectMetadata["imageType"] : null);
        privateState.imageType = defaultValues ? (defaultValues["imageType"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["imageType"], context) : null) : null;
        context["field"] = "imageApplication";
        context["metadata"] = (objectMetadata ? objectMetadata["imageApplication"] : null);
        privateState.imageApplication = defaultValues ? (defaultValues["imageApplication"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["imageApplication"], context) : null) : null;
        context["field"] = "multiMediaType";
        context["metadata"] = (objectMetadata ? objectMetadata["multiMediaType"] : null);
        privateState.multiMediaType = defaultValues ? (defaultValues["multiMediaType"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["multiMediaType"], context) : null) : null;
        context["field"] = "displayName";
        context["metadata"] = (objectMetadata ? objectMetadata["displayName"] : null);
        privateState.displayName = defaultValues ? (defaultValues["displayName"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["displayName"], context) : null) : null;
        context["field"] = "description";
        context["metadata"] = (objectMetadata ? objectMetadata["description"] : null);
        privateState.description = defaultValues ? (defaultValues["description"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["description"], context) : null) : null;
        context["field"] = "id";
        context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
        privateState.id = defaultValues ? (defaultValues["id"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["id"], context) : null) : null;
        context["field"] = "status";
        context["metadata"] = (objectMetadata ? objectMetadata["status"] : null);
        privateState.status = defaultValues ? (defaultValues["status"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["status"], context) : null) : null;
        context["field"] = "fileName";
        context["metadata"] = (objectMetadata ? objectMetadata["fileName"] : null);
        privateState.fileName = defaultValues ? (defaultValues["fileName"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["fileName"], context) : null) : null;
        context["field"] = "imageContent";
        context["metadata"] = (objectMetadata ? objectMetadata["imageContent"] : null);
        privateState.imageContent = defaultValues ? (defaultValues["imageContent"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["imageContent"], context) : null) : null;
        context["field"] = "imagePath";
        context["metadata"] = (objectMetadata ? objectMetadata["imagePath"] : null);
        privateState.imagePath = defaultValues ? (defaultValues["imagePath"] ? kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["imagePath"], context) : null) : null;
        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);
        //Defining Getter/Setters
        Object.defineProperties(this, {
            "customerId": {
                get: function() {
                    context["field"] = "customerId";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerId, context);
                },
                set: function(val) {
                    setterFunctions['customerId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "imageType": {
                get: function() {
                    context["field"] = "imageType";
                    context["metadata"] = (objectMetadata ? objectMetadata["imageType"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.imageType, context);
                },
                set: function(val) {
                    setterFunctions['imageType'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "imageApplication": {
                get: function() {
                    context["field"] = "imageApplication";
                    context["metadata"] = (objectMetadata ? objectMetadata["imageApplication"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.imageApplication, context);
                },
                set: function(val) {
                    setterFunctions['imageApplication'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "multiMediaType": {
                get: function() {
                    context["field"] = "multiMediaType";
                    context["metadata"] = (objectMetadata ? objectMetadata["multiMediaType"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.multiMediaType, context);
                },
                set: function(val) {
                    setterFunctions['multiMediaType'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "displayName": {
                get: function() {
                    context["field"] = "displayName";
                    context["metadata"] = (objectMetadata ? objectMetadata["displayName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.displayName, context);
                },
                set: function(val) {
                    setterFunctions['displayName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "description": {
                get: function() {
                    context["field"] = "description";
                    context["metadata"] = (objectMetadata ? objectMetadata["description"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.description, context);
                },
                set: function(val) {
                    setterFunctions['description'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "id": {
                get: function() {
                    context["field"] = "id";
                    context["metadata"] = (objectMetadata ? objectMetadata["id"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.id, context);
                },
                set: function(val) {
                    setterFunctions['id'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "status": {
                get: function() {
                    context["field"] = "status";
                    context["metadata"] = (objectMetadata ? objectMetadata["status"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.status, context);
                },
                set: function(val) {
                    setterFunctions['status'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "fileName": {
                get: function() {
                    context["field"] = "fileName";
                    context["metadata"] = (objectMetadata ? objectMetadata["fileName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.fileName, context);
                },
                set: function(val) {
                    setterFunctions['fileName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "imageContent": {
                get: function() {
                    context["field"] = "imageContent";
                    context["metadata"] = (objectMetadata ? objectMetadata["imageContent"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.imageContent, context);
                },
                set: function(val) {
                    setterFunctions['imageContent'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "imagePath": {
                get: function() {
                    context["field"] = "imagePath";
                    context["metadata"] = (objectMetadata ? objectMetadata["imagePath"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.imagePath, context);
                },
                set: function(val) {
                    setterFunctions['imagePath'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });
        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };
        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.customerId = value ? (value["customerId"] ? value["customerId"] : null) : null;
            privateState.imageType = value ? (value["imageType"] ? value["imageType"] : null) : null;
            privateState.imageApplication = value ? (value["imageApplication"] ? value["imageApplication"] : null) : null;
            privateState.multiMediaType = value ? (value["multiMediaType"] ? value["multiMediaType"] : null) : null;
            privateState.displayName = value ? (value["displayName"] ? value["displayName"] : null) : null;
            privateState.description = value ? (value["description"] ? value["description"] : null) : null;
            privateState.id = value ? (value["id"] ? value["id"] : null) : null;
            privateState.status = value ? (value["status"] ? value["status"] : null) : null;
            privateState.fileName = value ? (value["fileName"] ? value["fileName"] : null) : null;
            privateState.imageContent = value ? (value["imageContent"] ? value["imageContent"] : null) : null;
            privateState.imagePath = value ? (value["imagePath"] ? value["imagePath"] : null) : null;
        };
    }
    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(customerDocumentUpload);
    //Create new class level validator object
    BaseModel.Validator.call(customerDocumentUpload);
    var registerValidatorBackup = customerDocumentUpload.registerValidator;
    customerDocumentUpload.registerValidator = function() {
            var propName = arguments[0];
            if (!setterFunctions[propName].changed) {
                var setterBackup = setterFunctions[propName];
                setterFunctions[arguments[0]] = function() {
                    if (customerDocumentUpload.isValid(this, propName, val)) {
                        return setterBackup.apply(null, arguments);
                    } else {
                        throw Error("Validation failed for " + propName + " : " + val);
                    }
                }
                setterFunctions[arguments[0]].changed = true;
            }
            return registerValidatorBackup.apply(null, arguments);
        }
        //Extending Model for custom operations
        //For Operation 'createDocumentUpload2' with service id 'docUpload7440'
    customerDocumentUpload.createDocumentUpload2 = function(params, onCompletion) {
        return customerDocumentUpload.customVerb('createDocumentUpload2', params, onCompletion);
    };
    //For Operation 'createUploadVerify' with service id 'uploadVerify1592'
    customerDocumentUpload.createUploadVerify = function(params, onCompletion) {
        return customerDocumentUpload.customVerb('createUploadVerify', params, onCompletion);
    };
    var relations = [];
    customerDocumentUpload.relations = relations;
    customerDocumentUpload.prototype.isValid = function() {
        return customerDocumentUpload.isValid(this);
    };
    customerDocumentUpload.prototype.objModelName = "customerDocumentUpload";
    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    customerDocumentUpload.registerProcessors = function(options, successCallback, failureCallback) {
        if (!options) {
            options = {};
        }
        if (options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }
        if (options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }
        kony.mvc.util.ProcessorUtils.getMetadataForObject("cdmFullDetail", "customerDocumentUpload", options, metaDataSuccess, metaDataFailure);
    };
    //clone the object provided in argument.
    customerDocumentUpload.clone = function(objectToClone) {
        var clonedObj = new customerDocumentUpload();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };
    return customerDocumentUpload;
});