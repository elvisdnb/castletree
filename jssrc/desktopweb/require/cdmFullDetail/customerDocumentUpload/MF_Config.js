/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var mappings = {
        "customerId": "customerId",
        "imageType": "imageType",
        "imageApplication": "imageApplication",
        "multiMediaType": "multiMediaType",
        "displayName": "displayName",
        "description": "description",
        "id": "id",
        "status": "status",
        "fileName": "fileName",
        "imageContent": "imageContent",
        "imagePath": "imagePath",
    };
    Object.freeze(mappings);
    var typings = {
        "customerId": "string",
        "imageType": "string",
        "imageApplication": "string",
        "multiMediaType": "string",
        "displayName": "string",
        "description": "string",
        "id": "string",
        "status": "string",
        "fileName": "string",
        "imageContent": "string",
        "imagePath": "string",
    }
    Object.freeze(typings);
    var primaryKeys = ["customerId", ];
    Object.freeze(primaryKeys);
    var config = {
        mappings: mappings,
        typings: typings,
        primaryKeys: primaryKeys,
        serviceName: "cdmFullDetail",
        tableName: "customerDocumentUpload"
    };
    Object.freeze(config);
    return config;
})