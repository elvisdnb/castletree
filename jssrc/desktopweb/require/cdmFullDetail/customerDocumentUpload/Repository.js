define([], function() {
    var BaseRepository = kony.mvc.Data.BaseRepository;
    //Create the Repository Class
    function customerDocumentUploadRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
        BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
    };
    //Setting BaseRepository as Parent to this Repository
    customerDocumentUploadRepository.prototype = Object.create(BaseRepository.prototype);
    customerDocumentUploadRepository.prototype.constructor = customerDocumentUploadRepository;
    //For Operation 'createDocumentUpload2' with service id 'docUpload7440'
    customerDocumentUploadRepository.prototype.createDocumentUpload2 = function(params, onCompletion) {
        return customerDocumentUploadRepository.prototype.customVerb('createDocumentUpload2', params, onCompletion);
    };
    //For Operation 'createUploadVerify' with service id 'uploadVerify1592'
    customerDocumentUploadRepository.prototype.createUploadVerify = function(params, onCompletion) {
        return customerDocumentUploadRepository.prototype.customVerb('createUploadVerify', params, onCompletion);
    };
    return customerDocumentUploadRepository;
})