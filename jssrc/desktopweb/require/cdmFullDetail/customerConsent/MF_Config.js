/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var mappings = {
        "customerId": "customerId",
        "isDirectMarketing": "isDirectMarketing",
        "isCreditCheck": "isCreditCheck",
        "arrangementId": "arrangementId",
        "isAuthorised": "isAuthorised",
        "consentTypes": "consentTypes",
        "isConsentGiven": "isConsentGiven",
        "consentType": "consentType",
    };
    Object.freeze(mappings);
    var typings = {
        "customerId": "string",
        "isDirectMarketing": "string",
        "isCreditCheck": "string",
        "arrangementId": "string",
        "isAuthorised": "string",
        "consentTypes": "string",
        "isConsentGiven": "string",
        "consentType": "string",
    }
    Object.freeze(typings);
    var primaryKeys = ["customerId", ];
    Object.freeze(primaryKeys);
    var config = {
        mappings: mappings,
        typings: typings,
        primaryKeys: primaryKeys,
        serviceName: "cdmFullDetail",
        tableName: "customerConsent"
    };
    Object.freeze(config);
    return config;
})