define([], function() {
    var BaseRepository = kony.mvc.Data.BaseRepository;
    //Create the Repository Class
    function customerConsentRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
        BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
    };
    //Setting BaseRepository as Parent to this Repository
    customerConsentRepository.prototype = Object.create(BaseRepository.prototype);
    customerConsentRepository.prototype.constructor = customerConsentRepository;
    //For Operation 'getCConsent' with service id 'getCustomerPersonalConsents6928'
    customerConsentRepository.prototype.getCConsent = function(params, onCompletion) {
        return customerConsentRepository.prototype.customVerb('getCConsent', params, onCompletion);
    };
    //For Operation 'getCustomerConsent' with service id 'getCustomerPersonalConsents7295'
    customerConsentRepository.prototype.getCustomerConsent = function(params, onCompletion) {
        return customerConsentRepository.prototype.customVerb('getCustomerConsent', params, onCompletion);
    };
    return customerConsentRepository;
})