define("userfrmBlockUnblockAccountsController", {
    gblAccountNumber: "", //50003167", //50006859",
    //gblArrangementId: "ACLK2102842132",
    gblReferenceId: "",
    overrideContent: {
        "code": "",
        "description": "",
        "id": ""
    },
    gblSelectedRowIndex: undefined,
    gblActionSubmit: "create",
    gblCurrentDate: "",
    overrideNavFlow: "",
    gblCustomerId: "",
    gblCurrency: "",
    onNavigate: function(navObj) {
        try {
            kony.print("Navigate to frmBlockUnblock: " + JSON.stringify(navObj));
            this.initializeValues(navObj);
            this.bindEvents(navObj);
        } catch (err) {
            kony.print("Exception in bindEvents: " + err);
            this.closeProgressIndicator();
        }
    },
    bindEvents: function(navObj) {
        try {
            var scope = this;
            this.view.postShow = function() {
                scope.view.HeaderBlockUnlock.lblAccountTypeResponse.text = navObj.lblAcNum;
                scope.view.HeaderBlockUnlock.lblAccountNumberResponse.text = navObj.lblAcType;
                scope.view.HeaderBlockUnlock.lblCurrencyValueResponse.text = navObj.lblCCY;
                scope.view.HeaderBlockUnlock.lblAmountValueResponse.text = navObj.lblClearBal;
                scope.view.HeaderBlockUnlock.lblAvailableLimitResponse.text = navObj.lblAvlBal;
                scope.postShowForm();
            }
            this.view.custButtonConfirm.onclickEvent = this.onClickSubmit;
            this.view.custButtonCancel.onclickEvent = this.onClickCancel;
            //this.view.cusDropdownReasons.onSelection = this.selectReasonForBlockFund;
            //this.view.BlockUnblockLabels.segBlockFundsList.onRowClick = this.segmentRowClick;
            this.view.OverridePopup.btnProceed.onClick = this.onClickProceedOverride;
            this.view.OverridePopup.imgClose.onTouchEnd = this.closeOverridePopup;
            this.view.OverridePopup.btnCancel.onClick = this.closeOverridePopup;
            this.view.ErrorAllert.imgClose.onTouchEnd = function() {
                scope.view.ErrorAllert.isVisible = false;
            };
        } catch (err) {
            kony.print("Exception in bindEvents: " + err);
            this.closeProgressIndicator();
        }
    },
    initializeValues: function(navObj) {
        try {
            this.gblActionSubmit = "create";
            this.view.cusDateFromDate.disabled = false;
            this.view.cusDatePickerEnd.disabled = false;
            this.view.cusAmountBlockAmount.disabled = false;
            this.view.cusDropdownReasons.disabled = false;
            this.view.MainTabs.btnAccounts1.skin = "sknTabSelected";
            this.view.MainTabs.btnAccounts2.skin = "sknTabUnselected";
            this.view.MainTabs.btnAccounts3.skin = "sknTabUnselected";
            this.view.MainTabs.flxContainerAccount.skin = "sknSelected";
            this.view.MainTabs.flxontainerLoans.skin = "sknNotselected";
            this.view.MainTabs.flxContainerdeposits.skin = "sknNotselected";
            this.gblCurrency = navObj.lblCCY;
            this.gblCustomerId = navObj.customerId;
            this.gblAccountNumber = navObj.lblAcType;
            /**
            var clearBalance = navObj.lblClearBal;
            if(navObj.lblClearBal === "0.00" || navObj.lblClearBal === 0.00 || navObj.lblClearBal === "0,00"){
              if(this.gblCurrency === "EUR") clearBalance = "0,00";
            }else{
              clearBalance = clearBalance.replace(/[\s,]+/g,'');
              clearBalance = Application.numberFormater.convertForDispaly(navObj.lblClearBal, this.gblCurrency);
            }
            this.view.HeaderBlockUnlock.lblAmountValueResponse.text = clearBalance; */
            this.assignDateToCalender();
            this.getLockAccountEventType();
        } catch (err) {
            kony.print("Exception in initializeValues: " + err);
            this.closeProgressIndicator();
        }
    },
    postShowForm: function() {
        try {
            this.view.cusAmountBlockAmount.decimals = 2;
            this.view.cusAmountBlockAmount.value = "0.00";
            this.view.cusAmountBlockAmount.validateRequired = "required";
            this.view.cusDatePickerEnd.value = "";
            this.view.cusDropdownReasons.labelText = "Reason";
            this.view.cusDropdownReasons.value = "select";
            this.view.cusAmountBlockAmount.currencyCode = this.gblCurrency;
            this.clearUIErrors();
            this.view.forceLayout();
        } catch (err) {
            kony.print("postShow Exception: " + err);
            this.closeProgressIndicator();
        }
    },
    onClickSubmit: function() {
        try {
            var flag = false;
            if (this.isFieldEmpty(this.view.cusDateFromDate.value)) {
                flag = true;
            }
            if (this.isFieldEmpty(this.view.cusAmountBlockAmount.value)) {
                this.view.flxAmount.skin = "sknFlxWhiteBgRedBorder";
                flag = true;
            } else {
                this.view.flxAmount.skin = "slFbox";
            }
            if (this.isFieldEmpty(this.view.cusDropdownReasons.value) || this.view.cusDropdownReasons.value === "select") {
                this.view.flxReasonDropdown.skin = "sknFlxWhiteBgRedBorder";
                this.view.flxReasonDropdown.isVisible = true;
                flag = true;
            } else {
                this.view.flxReasonDropdown.skin = "slFbox";
                this.view.flxReasonDropdown.isVisible = false;
            }
            if (flag) {
                this.view.lblErrorMessage.isVisible = true;
                return;
            }
            this.clearUIErrors();
            this.overrideContent = {
                    "code": "",
                    "description": "",
                    "id": ""
                },
                kony.print("gblActionSubmit Value: " + this.gblActionSubmit);
            if (this.gblActionSubmit === "create") {
                this.createAccountLock();
            } else if (this.gblActionSubmit === "update") {
                this.updateAccountLock();
            } else if (this.gblActionSubmit === "delete") {
                this.deleteAccountLock();
            }
        } catch (err) {
            kony.print("Exception in onClickSubmit :::" + err);
            this.closeProgressIndicator();
        }
    },
    createAccountLock: function() {
        try {
            var serviceName = "BlockUnblock";
            var operationName = "createAccountLock";
            var headers = {
                "companyId": "NL0020001",
            };
            var fromDate = this.convertDateFormat(this.view.cusDateFromDate.value);
            var toDate = this.convertDateFormat(this.view.cusDatePickerEnd.value);
            //var reason = this.view.lbxReason.selectedKey;
            var reason = this.view.cusDropdownReasons.value;
            kony.print("Reason: " + reason);
            var inputParams = {
                "blockAmount": this.view.cusAmountBlockAmount.value,
                "accountNumber": this.gblAccountNumber,
                "fromDate": fromDate,
                "toDate": toDate,
                "reason": reason,
                "code": this.overrideContent.code,
                "description": this.overrideContent.description,
                "overrideId": this.overrideContent.id
            };
            kony.print("createAccountLock Input Data: " + JSON.stringify(inputParams));
            this.showProgressIndicator();
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBcreateAccountLock, this.errorCBcreateAccountLock);
        } catch (err) {
            kony.print("Exception in createAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    successCBcreateAccountLock: function(res) {
        try {
            this.closeProgressIndicator();
            kony.print("successCBcreateAccountLock response::: " + JSON.stringify(res));
            var refId = res.header["id"];
            var navToFundDeposit = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObj = {};
            navObj.customerId = this.gblCustomerId;
            navObj.showMessageInfo = true;
            navObj.isShowAccounts = "true";
            navObj.messageInfo = "Funds blocked successfully! Ref :" + refId;
            navToFundDeposit.navigate(navObj);
        } catch (err) {
            kony.print("Exception in successCBcreateAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    errorCBcreateAccountLock: function(error) {
        try {
            this.closeProgressIndicator();
            kony.print("errorCBcreateAccountLock response::: " + JSON.stringify(error));
            //handle the Override
            if (error.override) {
                if (error.override.overrideDetails.length > 0) {
                    //this.overrideContent = error.override;
                    var overrideDetails = error.override.overrideDetails;
                    this.overrideContent.code = overrideDetails[0].code;
                    this.overrideContent.description = overrideDetails[0].description;
                    this.overrideContent.id = overrideDetails[0].id;
                    var segData = [];
                    for (var i = 0; i < overrideDetails.length; i++) {
                        var rowData = {
                            "lblSerial": "*",
                            "lblInfo": overrideDetails[i].description
                        };
                        segData.push(rowData);
                    }
                    this.view.OverridePopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.overrideNavFlow = "create";
                    this.view.OverridePopup.lblPaymentInfo.text = "Override Confirmation";
                    this.view.OverridePopup.segOverride.setData(segData);
                    this.view.OverridePopup.isVisible = true;
                    this.view.forceLayout();
                } else {
                    this.view.ErrorAllert.lblMessage.text = error.errmsg;
                    this.view.ErrorAllert.setVisibility(true);
                    this.view.ErrorAllert.setFocus(true);
                }
            } else if (error.error && error.error.errorDetails) {
                var errorDetail = error.error.errorDetails[0];
                if (errorDetail.message) this.view.ErrorAllert.lblMessage.text = errorDetail.code + " " + errorDetail.message;
                else this.view.ErrorAllert.lblMessage.text = error.errmsg;
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            } else if (error.errmsg) {
                this.view.ErrorAllert.lblMessage.text = error.errmsg;
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            }
        } catch (err) {
            kony.print("Exception in errorCBcreateAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    getLockAccountEventType: function() {
        try {
            var serviceName = "BlockUnblock";
            var operationName = "getAcLockEventType";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {};
            //this.showProgressIndicator();
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgetLockAccountEventType, this.errorCBgetLockAccountEventType);
        } catch (err) {
            kony.print("Error in frmBlockUnblockController getLockAccountEventType:::" + err);
            this.closeProgressIndicator();
        }
    },
    successCBgetLockAccountEventType: function(res) {
        try {
            //this.closeProgressIndicator();
            kony.print("successCBgetLockAccountEventType Success response: " + JSON.stringify(res));
            if (res.header.status === "success") {
                var LockedAccEventTypeList = res.body;
                var len = LockedAccEventTypeList.length;
                var reasonsList = [];
                reasonsList.push({
                    "label": "",
                    "value": "select"
                });
                for (var i = 0; i < len; i++) {
                    var keyValue = {
                        "label": this.formatAccountLockType(LockedAccEventTypeList[i].lockTypeId),
                        "value": LockedAccEventTypeList[i].lockTypeId
                    };
                    reasonsList.push(keyValue);
                }
                kony.print("reasonsList:: " + JSON.stringify(reasonsList));
                this.view.cusDropdownReasons.options = JSON.stringify(reasonsList);
                //this.view.cusDropdownReasons.value = "select";
                this.view.flxBlockUnblockChange.forceLayout();
            }
            /**
            if(res.header.status == "success") {
              var LockedAccEventTypeList  = res.body;
              var len = LockedAccEventTypeList.length;
              var reasonsList = [];
              var keyValue = ["select","Reason"];
              reasonsList.push(keyValue);
              for(var i=0; i<len ; i++){
                keyValue = [];
                keyValue.push(LockedAccEventTypeList[i].lockTypeId);
                keyValue.push(this.formatAccountLockType(LockedAccEventTypeList[i].lockTypeId)); //LockedAccEventTypeList[i].description
                //var accountLink = LockedAccEventTypeList[i].accountLink
                reasonsList.push(keyValue);
              }
              this.view.lbxReason.selectedKey = "select";
              this.view.lbxReason.masterData = reasonsList;
              this.view.flxBlockUnblockChange.forceLayout();
            } **/
        } catch (err) {
            kony.print("Error in frmBlockUnblockController getLockAccountEventType::" + err);
            this.closeProgressIndicator();
        }
    },
    formatAccountLockType: function(str) {
        try {
            if (this.isFieldEmpty(str)) return "";
            var formattedStr = "";
            var accountTypeArr = [];
            if (str.includes(".")) {
                accountTypeArr = str.split(".");
            } else {
                accountTypeArr.push(str);
            }
            for (var i = 0; i < accountTypeArr.length; i++) {
                var txt = accountTypeArr[i];
                formattedStr = formattedStr + " " + txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
            var resultStr = formattedStr.trim();
            kony.print("Result String: " + resultStr);
            return resultStr;
        } catch (err) {
            kony.print("Error in frmBlockUnblockController formatAccountLockType::" + err);
            this.closeProgressIndicator();
        }
    },
    selectReasonForBlockFund: function(widget, context) {
        try {
            var selectedKey = widget.selectedKey;
            kony.print("Selected Key: " + selectedKey);
            if (selectedKey === "select") {} else {}
            /*
              if(selectedKey === "DepositService1") {
                var navToFundDeposit = new kony.mvc.Navigation("frmFundDeposit");
                navToFundDeposit.navigate();
              } else if(selectedKey === "DepositService2") {
                var navToDepositWithdrawal = new kony.mvc.Navigation("frmDepositWithdrawal");
                navToDepositWithdrawal.navigate();
              }else if(selectedKey === "DepositService4") {
                var navToBlockUnblock = new kony.mvc.Navigation("frmBlockUnblockDeposits");
                navToBlockUnblock.navigate();
              } else{
                kony.print("Do nothing");
              }
              */
        } catch (err) {
            kony.print("Exception in selectReasonForBlockFund: " + err);
            this.closeProgressIndicator();
        }
    },
    errorCBgetLockAccountEventType: function(res) {
        try {
            this.closeProgressIndicator();
            kony.print("error response:::" + JSON.stringify(res));
            if (res.error && res.error.errorDetails) {
                var errorDetail = res.error.errorDetails[0];
                if (errorDetail.message) this.view.ErrorAllert.lblMessage.text = errorDetail.code + " " + errorDetail.message;
                else this.view.ErrorAllert.lblMessage.text = res.errmsg;
            } else if (res.errmsg) {
                this.view.ErrorAllert.lblMessage.text = res.errmsg;
            } else {
                this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity.";
            }
            this.view.ErrorAllert.setVisibility(true);
            this.view.ErrorAllert.setFocus(true);
        } catch (err) {
            kony.print("Error in frmBlockUnblockController getLockAccountEventType::" + err);
            this.closeProgressIndicator();
        }
    },
    deleteAccountLock: function() {
        try {
            var serviceName = "BlockUnblock";
            var operationName = "deleteAccountLock";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {
                "id": this.gblReferenceId
            };
            kony.print("Input Params: " + JSON.stringify(inputParams));
            this.showProgressIndicator();
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBdeleteAccountLock, this.errorCBdeleteAccountLock);
        } catch (err) {
            kony.print("Exception in deleteAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    successCBdeleteAccountLock: function(res) {
        try {
            this.closeProgressIndicator();
            kony.print("successCBdeleteAccountLock Response: " + JSON.stringify(res));
            var refId = res.header["id"];
            var navToFundDeposit = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObj = {};
            navObj.customerId = this.gblCustomerId;
            navObj.showMessageInfo = true;
            navObj.isShowAccounts = "true";
            navObj.messageInfo = "Funds unblocked successfully! Ref :" + refId;
            navToFundDeposit.navigate(navObj);
        } catch (err) {
            kony.print("Exception in successCBdeleteAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    errorCBdeleteAccountLock: function(res) {
        try {
            this.closeProgressIndicator();
            kony.print("errorCBdeleteAccountLock Response::: " + JSON.stringify(res));
            if (res.error && res.error.errorDetails) {
                var errorDetail = res.error.errorDetails[0];
                if (errorDetail.message) this.view.ErrorAllert.lblMessage.text = errorDetail.code + " " + errorDetail.message;
                else this.view.ErrorAllert.lblMessage.text = res.errmsg;
            } else if (res.errmsg) {
                this.view.ErrorAllert.lblMessage.text = res.errmsg;
            } else {
                this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity.";
            }
            this.view.ErrorAllert.setVisibility(true);
            this.view.ErrorAllert.setFocus(true);
        } catch (err) {
            kony.print("Exception in errorCBdeleteAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    fetchLockedFunds: function() {
        try {
            var serviceName = "BlockUnblock";
            var operationName = "getLockedFunds";
            var headers = {
                "companyId": "NL0020001",
                "page_size": 500
            };
            var inputParams = {
                "accountNo": this.gblAccountNumber
            };
            kony.print("fetchLockedFunds Input Params: " + JSON.stringify(inputParams));
            //this.showProgressIndicator();
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBfetchLockedFunds, this.errorCBfetchLockedFunds);
        } catch (err) {
            kony.print("Exception in deleteAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    successCBfetchLockedFunds: function(res) {
        try {
            this.closeProgressIndicator();
            kony.print("successCBfetchLockedFunds Response: " + JSON.stringify(res));
            if (res.header.status === "success") {
                this.gblSelectedRowIndex = undefined;
                if (res.body && res.body.length > 0) {
                    var BlockedFundsList = res.body;
                    var len = BlockedFundsList.length;
                    this.view.BlockUnblockLabels.lblNoRecord.isVisible = false;
                    this.view.BlockUnblockLabels.segBlockFundsList.isVisible = true;
                    this.view.HeaderBlockUnlock.lblFundHeldResponse.isVisible = true;
                    this.view.HeaderBlockUnlock.lblAmountValueResponse.width = "128dp";
                    var segData = [];
                    var self = this;
                    var totalBlockedAmount = 0;
                    for (var i = 0; i < len; i++) {
                        var rowData = {};
                        rowData = {
                            "lblReferenceNo": {
                                "text": BlockedFundsList[i].transactionRef
                            },
                            "lblStartDate": {
                                "text": BlockedFundsList[i].fromDate
                            },
                            "lblEndDate": {
                                "text": self.isFieldEmpty(BlockedFundsList[i].toDate) ? "" : BlockedFundsList[i].toDate
                            },
                            "lblAmount": {
                                "text": self.formatAmountForCurrency(BlockedFundsList[i].lockedAmount)
                            },
                            "lblReason": {
                                "text": self.formatAccountLockType(BlockedFundsList[i].reason)
                            },
                            "lblReasonHidden": self.isFieldEmpty(BlockedFundsList[i].reason) ? "" : BlockedFundsList[i].reason,
                            //"lblAccountNumberResponse": BlockedFundsList.account
                            //"imgEdit": {"src":"ico_filter.png","onTouchEnd": self.onEditOfFund},
                            //"imgUnblock": {"src":"ico_close.png","onTouchEnd": self.onUnblockOfFund}
                            "btnEdit": {
                                "onClick": self.onEditOfFund
                            },
                            "btnUnblock": {
                                "onClick": self.onUnblockOfFund
                            },
                            "flxRowBlockFunds": {
                                "skin": "slFbox"
                            }
                        };
                        if (!this.isFieldEmpty(BlockedFundsList[i].toDate) && !this.compareDateWithCurrentDate(BlockedFundsList[i].toDate)) {
                            kony.print("disable edit/unblock actions and change the skinof segment rows");
                            rowData.btnEdit.enable = false;
                            rowData.btnUnblock.enable = false;
                            //rowData.flxRowBlockFunds.skin = "slFbox";
                            rowData.flxRowBlockFunds.hoverSkin = "slFbox";
                            rowData.lblReferenceNo.skin = "sknLblDisableBgA9A9ABpx14";
                            rowData.lblStartDate.skin = "sknLblDisableBgA9A9ABpx14";
                            rowData.lblEndDate.skin = "sknLblDisableBgA9A9ABpx14";
                            rowData.lblAmount.skin = "sknLblDisableBgA9A9ABpx14";
                            rowData.lblReason.skin = "sknLblDisableBgA9A9ABpx14";
                            rowData.btnEdit.skin = "skinButtonIconsDisable";
                            rowData.btnUnblock.skin = "skinButtonIconsDisable";
                        }
                        segData.push(rowData);
                    }
                    //this.gblSegmentData = segData;
                    kony.print("segData Length:: " + segData.length);
                    this.view.BlockUnblockLabels.segBlockFundsList.setData(segData);
                } else {
                    this.view.BlockUnblockLabels.segBlockFundsList.isVisible = false;
                    this.view.BlockUnblockLabels.lblNoRecord.isVisible = true;
                    this.view.HeaderBlockUnlock.lblFundHeldResponse.isVisible = false;
                    this.view.HeaderBlockUnlock.lblAmountValueResponse.width = "220dp";
                }
            } else {
                if (res.errmsg) this.view.ErrorAllert.lblMessage.text = res.errmsg;
                else this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity.";
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            }
        } catch (err) {
            this.closeProgressIndicator();
            kony.print("Exception in successCBfetchLockedFunds:: " + err);
        }
    },
    onEditOfFund: function(widget, context) {
        try {
            var selectedContext = context["rowIndex"];
            kony.print("onEditOfFund selectedContext: " + selectedContext);
            var selectedRowItem = this.view.BlockUnblockLabels.segBlockFundsList.data[selectedContext];
            kony.print("onEditOfFund # selected item---" + JSON.stringify(selectedRowItem));
            /* {"lblReferenceNo":"ACLK2102845041","lblStartDate":"28 JAN 2021","lblEndDate":"23 APR 2021","lblAmount":"155.00","lblReason":"Loan Repayment",
            "lblReasonHidden":"LOAN.REPAYMENT","btnEdit":{},"btnUnblock":{},"flxRowBlockFunds":{"skin":"slFbox"}} */
            //selectedRowItem.flxRowBlockFunds.skin = "sknFlxRowFocusBg";
            if (this.gblSelectedRowIndex !== undefined) { //set default colour to prev selected item
                var selectedPrevRowItem = this.view.BlockUnblockLabels.segBlockFundsList.data[this.gblSelectedRowIndex];
                selectedPrevRowItem.flxRowBlockFunds.skin = "slFbox";
                //this.view.BlockUnblockLabels.segBlockFundsList.removeAt(this.gblSelectedRowIndex, 0);
                this.view.BlockUnblockLabels.segBlockFundsList.setDataAt(selectedPrevRowItem, this.gblSelectedRowIndex, 0);
            }
            this.gblSelectedRowIndex = selectedContext;
            selectedRowItem.flxRowBlockFunds.skin = "sknFlxRowBgE3D3FE";
            this.view.BlockUnblockLabels.segBlockFundsList.setDataAt(selectedRowItem, selectedContext, 0);
            this.gblActionSubmit = "update";
            this.gblReferenceId = selectedRowItem.lblReferenceNo.text;
            this.view.cusDateFromDate.value = selectedRowItem.lblStartDate.text;
            if (selectedRowItem.lblEndDate.text) this.view.cusDatePickerEnd.value = selectedRowItem.lblEndDate.text;
            else this.view.cusDatePickerEnd.value = "";
            this.view.cusAmountBlockAmount.value = selectedRowItem.lblAmount.text;
            if (selectedRowItem.lblReasonHidden) this.view.cusDropdownReasons.value = selectedRowItem.lblReasonHidden;
            else this.view.cusDropdownReasons.value = "select";
            this.view.cusDateFromDate.disabled = false;
            this.view.cusDatePickerEnd.disabled = false;
            this.view.cusAmountBlockAmount.disabled = false;
            this.view.cusDropdownReasons.disabled = true;
            //this.view.BlockUnblockLabels.segBlockFundsList.removeAll();
            //this.view.BlockUnblockLabels.segBlockFundsList.setData(this.gblSegmentData);
            this.view.flxBlockFundDetails.forceLayout();
        } catch (err) {
            kony.print("Exception in onEditOfFund:: " + err);
            this.closeProgressIndicator();
        }
    },
    onUnblockOfFund: function(widget, context) {
        try {
            var selectedContext = context["rowIndex"];
            kony.print("onUnblockOfFund selectedContext: " + selectedContext);
            var selectedRowItem = this.view.BlockUnblockLabels.segBlockFundsList.data[selectedContext];
            kony.print("onUnblockOfFund # selected item---" + JSON.stringify(selectedRowItem));
            if (this.gblSelectedRowIndex >= 0) { //set default colour to prev selected item
                var selectedPrevRowItem = this.view.BlockUnblockLabels.segBlockFundsList.data[this.gblSelectedRowIndex];
                selectedPrevRowItem.flxRowBlockFunds.skin = "slFbox";
                this.view.BlockUnblockLabels.segBlockFundsList.setDataAt(selectedPrevRowItem, this.gblSelectedRowIndex, 0);
            }
            this.gblSelectedRowIndex = selectedContext;
            selectedRowItem.flxRowBlockFunds.skin = "sknFlxRowBgE3D3FE";
            this.view.BlockUnblockLabels.segBlockFundsList.setDataAt(selectedRowItem, selectedContext, 0);
            this.gblActionSubmit = "delete";
            this.gblReferenceId = selectedRowItem.lblReferenceNo.text;
            this.view.cusDateFromDate.value = selectedRowItem.lblStartDate.text;
            if (selectedRowItem.lblEndDate.text) this.view.cusDatePickerEnd.value = selectedRowItem.lblEndDate.text;
            else this.view.cusDatePickerEnd.value = "";
            this.view.cusAmountBlockAmount.value = selectedRowItem.lblAmount.text;
            if (selectedRowItem.lblReasonHidden) this.view.cusDropdownReasons.value = selectedRowItem.lblReasonHidden;
            else this.view.cusDropdownReasons.value = "select";
            this.view.cusDateFromDate.disabled = true;
            this.view.cusDatePickerEnd.disabled = true;
            this.view.cusAmountBlockAmount.disabled = true;
            this.view.cusDropdownReasons.disabled = true;
            //this.view.BlockUnblockLabels.segBlockFundsList.removeAll();
            //this.view.BlockUnblockLabels.segBlockFundsList.setData(this.gblSegmentData);
            this.view.flxBlockFundDetails.forceLayout();
        } catch (err) {
            kony.print("Exception in onUnblockOfFund:: " + err);
            this.closeProgressIndicator();
        }
    },
    errorCBfetchLockedFunds: function(res) {
        try {
            this.closeProgressIndicator();
            kony.print("errorCBfetchLockedFunds Response::: " + JSON.stringify(res));
            if (res.error && res.error.errorDetails) {
                var errorDetail = res.error.errorDetails[0];
                if (errorDetail.message) this.view.ErrorAllert.lblMessage.text = errorDetail.code + " " + errorDetail.message;
                else this.view.ErrorAllert.lblMessage.text = res.errmsg;
            } else if (res.errmsg) {
                this.view.ErrorAllert.lblMessage.text = res.errmsg;
            } else {
                this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity.";
            }
            this.view.ErrorAllert.setVisibility(true);
            this.view.ErrorAllert.setFocus(true);
        } catch (err) {
            kony.print("Exception in errorCBfetchLockedFunds:: " + err);
            this.closeProgressIndicator();
        }
    },
    updateAccountLock: function() {
        try {
            var serviceName = "BlockUnblock";
            var operationName = "updateAccountLock";
            var headers = {
                "companyId": "NL0020001",
            };
            var fromDate = this.convertDateFormat(this.view.cusDateFromDate.value);
            var toDate = this.convertDateFormat(this.view.cusDatePickerEnd.value);
            var reason = this.view.cusDropdownReasons.value;
            var inputParams = {
                "id": this.gblReferenceId,
                "lockedAmount": this.removeAmountFormat(this.view.cusAmountBlockAmount.value, this.gblCurrency),
                "accountNumber": this.gblAccountNumber,
                "fromDate": fromDate,
                "toDate": toDate,
                //"reason": this.view.cusReason.value,
                "code": this.overrideContent.code,
                "description": this.overrideContent.description,
                "overrideId": this.overrideContent.id
            };
            kony.print("Update Locked fund Input Params: " + JSON.stringify(inputParams));
            this.showProgressIndicator();
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBupdateAccountLock, this.errorCBupdateAccountLock);
        } catch (err) {
            kony.print("Exception in updateAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    successCBupdateAccountLock: function(res) {
        try {
            this.closeProgressIndicator();
            kony.print("successCBupdateAccountLock response::: " + JSON.stringify(res));
            var refId = res.header["id"];
            var navToFundDeposit = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObj = {};
            navObj.customerId = this.gblCustomerId;
            navObj.showMessageInfo = true;
            navObj.isShowAccounts = "true";
            navObj.messageInfo = "Block Funds Modified successfully! Ref :" + refId;
            navToFundDeposit.navigate(navObj);
        } catch (err) {
            kony.print("Exception in successCBupdateAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    errorCBupdateAccountLock: function(error) {
        try {
            this.closeProgressIndicator();
            kony.print("errorCBupdateAccountLock response::: " + JSON.stringify(error));
            if (error.override) {
                if (error.override.overrideDetails.length > 0) {
                    //override
                    //this.overrideContent = error.override;
                    var overrideDetails = error.override.overrideDetails;
                    this.overrideContent.code = overrideDetails[0].code;
                    this.overrideContent.description = overrideDetails[0].description;
                    this.overrideContent.id = overrideDetails[0].id;
                    var segData = [];
                    for (var i = 0; i < overrideDetails.length; i++) {
                        var rowData = {
                            "lblSerial": "*",
                            "lblInfo": overrideDetails[i].description
                        };
                        segData.push(rowData);
                    }
                    this.view.OverridePopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.overrideNavFlow = "update";
                    this.view.OverridePopup.lblPaymentInfo.text = "Override Confirmation";
                    this.view.OverridePopup.segOverride.setData(segData);
                    this.view.OverridePopup.isVisible = true;
                    this.view.forceLayout();
                } else {
                    this.view.ErrorAllert.lblMessage.text = error.errmsg;
                    this.view.ErrorAllert.setVisibility(true);
                    this.view.ErrorAllert.setFocus(true);
                }
            } else if (error.error && error.error.errorDetails) {
                var errorDetail = error.error.errorDetails[0];
                if (errorDetail.message) this.view.ErrorAllert.lblMessage.text = errorDetail.code + " " + errorDetail.message;
                else this.view.ErrorAllert.lblMessage.text = error.errmsg;
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            } else if (error.errmsg) {
                this.view.ErrorAllert.lblMessage.text = error.errmsg;
                this.view.ErrorAllert.setVisibility(true);
                this.view.ErrorAllert.setFocus(true);
            }
        } catch (err) {
            kony.print("Exception in errorCBupdateAccountLock:: " + err);
            this.closeProgressIndicator();
        }
    },
    assignDateToCalender: function() {
        try {
            this.showProgressIndicator();
            var serviceName = "FundsDeposit";
            var operationName = "getDate";
            var headers = {
                "companyId": "NL0020001",
            };
            var inputParams = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgeDate, this.errorCBgetDate);
        } catch (err) {
            kony.print("Error in Withdrawal assignDateToCalender::: " + err);
            this.closeProgressIndicator();
        }
    },
    successCBgeDate: function(res) {
        try {
            var today = res.body[0].currentWorkingDate;
            kony.print("currentWorkingDate::: " + today);
            this.gblCurrentDate = today; //"2021-03-30";
            this.fetchLockedFunds();
            //var today = new Date();
            var dd = today.slice(8, 10);
            var shortMonth = today.toLocaleString('en-us', {
                month: 'short'
            });
            shortMonth = today.slice(5, 7);
            switch (shortMonth) {
                case "01":
                    shortMonth = "Jan";
                    break;
                case "02":
                    shortMonth = "Feb";
                    break;
                case "03":
                    shortMonth = "Mar";
                    break;
                case "04":
                    shortMonth = "Apr";
                    break;
                case "05":
                    shortMonth = "May";
                    break;
                case "06":
                    shortMonth = "Jun";
                    break;
                case "07":
                    shortMonth = "Jul";
                    break;
                case "08":
                    shortMonth = "Aug";
                    break;
                case "09":
                    shortMonth = "Sep";
                    break;
                case "10":
                    shortMonth = "Oct";
                    break;
                case "11":
                    shortMonth = "Nov";
                    break;
                case "12":
                    shortMonth = "Dec";
                    break;
                default:
                    shortMonth = "";
            }
            var yyyy = today.slice(0, 4);
            if (dd < 10) {
                dd = '0' + dd;
            }
            var dateConverted = dd + " " + shortMonth + " " + yyyy;
            this.view.cusDateFromDate.value = dateConverted;
            this.view.cusDateFromDate.displayFormat = "dd MMM yyyy";
            this.view.cusDateFromDate.min = dateConverted;
            this.view.cusDatePickerEnd.displayFormat = "dd MMM yyyy";
            this.view.cusDatePickerEnd.min = dateConverted;
        } catch (err) {
            kony.print("Error in Withdrawal Controller successCBgeDate::: " + err);
            this.closeProgressIndicator();
        }
    },
    convertDateFormat: function(dateStr) {
        try {
            if (this.isFieldEmpty(dateStr)) return "";
            //dateStr = "15 Apr 2021"
            kony.print("dateStr: " + dateStr);
            //var today = new Date();
            var dd = dateStr.split(" ");
            //var shortMonth = today.toLocaleString('en-us', { month: 'short' });
            if (dd.length !== 3) return;
            var shortDay = dd[0];
            var shortMonth = dd[1];
            var shortYear = dd[2];
            if (shortMonth === "Jan" || shortMonth === "JAN" || shortMonth === "jan") {
                shortMonth = "01";
            } else if (shortMonth === "Feb" || shortMonth === "FEB" || shortMonth === "feb") {
                shortMonth = "02";
            } else if (shortMonth === "Mar" || shortMonth === "MAR" || shortMonth === "mar") {
                shortMonth = "03";
            } else if (shortMonth === "Apr" || shortMonth === "APR" || shortMonth === "apr") {
                shortMonth = "04";
            } else if (shortMonth === "May" || shortMonth === "MAY" || shortMonth === "may") {
                shortMonth = "05";
            } else if (shortMonth === "Jun" || shortMonth === "JUN" || shortMonth === "jun") {
                shortMonth = "06";
            } else if (shortMonth === "Jul" || shortMonth === "JUL" || shortMonth === "jul") {
                shortMonth = "07";
            } else if (shortMonth === "Aug" || shortMonth === "AUG" || shortMonth === "aug") {
                shortMonth = "08";
            } else if (shortMonth === "Sep" || shortMonth === "SEP" || shortMonth === "sep") {
                shortMonth = "09";
            } else if (shortMonth === "Oct" || shortMonth === "OCT" || shortMonth === "oct") {
                shortMonth = "10";
            } else if (shortMonth === "Nov" || shortMonth === "NOV" || shortMonth === "nov") {
                shortMonth = "11";
            } else if (shortMonth === "Dec" || shortMonth === "DEC" || shortMonth === "dec") {
                shortMonth = "12";
            }
            if (shortDay.length < 2) shortDay = "0" + shortDay;
            var dateConverted = shortYear + "-" + shortMonth + "-" + shortDay;
            return dateConverted;
        } catch (err) {
            kony.print("Error in Withdrawal Controller convertDateFormat::: " + err);
            this.closeProgressIndicator();
        }
    },
    errorCBgetDate: function(error) {
        try {
            this.closeProgressIndicator();
            this.gblCurrentDate = new Date();
            this.fetchLockedFunds();
            kony.print("error response::: " + JSON.stringify(error));
            if (error.errmsg) this.view.ErrorAllert.lblMessage.text = error.errmsg;
            else this.view.ErrorAllert.lblMessage.text = "An error occurred while making the request. Please check device connectivity.";
            this.view.ErrorAllert.setVisibility(true);
            this.view.ErrorAllert.setFocus(true);
        } catch (err) {
            kony.print("Error in Withdrawal Controller errorCBgetDate::: " + err);
            this.closeProgressIndicator();
        }
    },
    onClickProceedOverride: function() {
        try {
            kony.print("onClickProceedOverride ### overrideNavFlow::: " + this.overrideNavFlow);
            if (this.overrideNavFlow === "create") {
                this.createAccountLock();
            } else if (this.overrideNavFlow === "update") {
                this.updateAccountLock();
            }
            this.closeOverridePopup();
        } catch (err) {
            kony.print("Exception in onClickProceedOverride: " + err);
            this.closeProgressIndicator();
        }
    },
    closeOverridePopup: function() {
        this.view.OverridePopup.isVisible = false;
    },
    onClickCancel: function() {
        try {
            var navToHome = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObjet = {};
            navObjet.customerId = this.gblCustomerId;
            //navObjet.isShowLoans = "false";
            navObjet.showMessageInfo = false;
            navObjet.isShowAccounts = "true";
            navToHome.navigate(navObjet);
        } catch (err) {
            kony.print("Exception in onClickCancel: " + err);
        }
    },
    compareDateWithCurrentDate: function(dateStr) {
        try {
            kony.print("Current Date::: " + this.gblCurrentDate);
            var d1 = new Date(this.gblCurrentDate);
            var d2 = new Date(dateStr);
            var flag = true;
            var day1 = d1.getDate();
            var month1 = d1.getMonth() + 1;
            var year1 = d1.getFullYear();
            var day2 = d2.getDate();
            var month2 = d2.getMonth() + 1;
            var year2 = d2.getFullYear();
            if (year2 > year1) {
                flag = true;
            } else {
                if (month2 > month1) {
                    flag = true;
                } else {
                    if (day2 >= day1) {
                        flag = true;
                    } else {
                        flag = false;
                    }
                }
            }
            /**
            if(d2.getTime() < d1.getTime()) {
              flag = false;
            }else if(d2.getTime() >= d1.getTime()){
              flag = true;
            } */
            return flag;
        } catch (err) {
            kony.print("Exception in compareTwoDates: " + err);
            this.closeProgressIndicator();
        }
    },
    //________________________________________________________________
    getBlockUnblockType: function(param) {
        try {
            kony.print("getBlockUnblockType # param: " + JSON.stringify(param));
            var scope = this;
            if (param === 1) { //for Block
            } else if (param === 2) { //Full Withdrawal
            }
            //this.view.flxFundWithdrawalWrapper.forceLayout();
        } catch (err) {
            kony.print("Exception in getBlockUnblockType: " + err);
            this.closeProgressIndicator();
        }
    },
    //________________________________________________________________
    clearUIErrors: function() {
        this.view.lblErrorMessage.isVisible = false;
        this.view.flxAmount.skin = "slFbox";
        this.view.flxReasonDropdown.skin = "slFbox";
        this.view.flxReasonDropdown.isVisible = false;
        this.view.ErrorAllert.setVisibility(false);
    },
    isFieldEmpty: function(str) {
        try {
            kony.print("Input str: " + str);
            if (str && str != "0.00" && str != "0" && str != "00" && str != "NULL" && str != "Null" && str != "null") {
                return false;
            } else {
                return true;
            }
        } catch (err) {
            kony.print("Exception in isFieldEmpty: " + err);
            this.closeProgressIndicator();
        }
    },
    formatAmountForCurrency: function(amount) {
        try {
            if (amount === 0 || amount === "0" || amount === "0.00" || amount === 0.00 || amount === "0,00") {
                if (this.gblCurrency === "EUR" || this.gblCurrency === "eur") amount = "0,00";
                else amount = "0.00";
            } else {
                amount = amount.replace(/[\s,]+/g, ''); //remove commas
                amount = Application.numberFormater.convertForDispaly(amount, this.gblCurrency);
            }
            return amount;
        } catch (err) {
            kony.print("Exception in formatAmountForCurrency: " + err);
        }
    },
    formatAmount: function(x) {
        try {
            if (!x) return "0.00";
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            if (parts.length === 1) {
                parts[1] = "00";
            } else if (parts.length === 2) {
                if (parts[1].length === 1) parts[1] = parts[1] + "0";
            }
            return parts.join(".");
        } catch (err) {
            kony.print("Exception in formatAmount: " + err);
        }
    },
    /*EUR
    input format: 10,000.50 or 10.000,50
    output formt: 10000.50

    USD
    input format: 10,000.50
    output formt: 10000.50
    */
    removeAmountFormat: function(amnt, currency) {
        try {
            if (amnt === "0.00" || amnt === 0.00 || amnt === "0,00") return "0.00";
            var amount = "";
            if (amnt) {
                if (amnt.charAt(amnt.length - 3) === ",") {
                    amount = amnt.replace(/\./g, ""); //remove all dots
                    amount = amount.replace(/[\s,]+/g, '.').trim(); //replace comma wih dot
                } else {
                    amount = amnt.replace(/[\s,]+/g, ""); //remove all commas
                }
            }
            kony.print("Amount after removing all formats: " + amount);
            return amount;
        } catch (err) {
            kony.print("Exception in removeAmountFormat: " + err);
        }
    },
    /**
    removeAmountFormat: function(x) {
      try{
        x = x.replace(/[\s,]+/g,'');
        return x;
      }catch(err){
        kony.print("Exception in removeAmountFormat: "+err);
      }
    }, */
    showProgressIndicator: function() {
        this.view.ProgressIndicator.isVisible = true;
    },
    closeProgressIndicator: function() {
        this.view.ProgressIndicator.isVisible = false;
    }
});
define("frmBlockUnblockAccountsControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("frmBlockUnblockAccountsController", ["userfrmBlockUnblockAccountsController", "frmBlockUnblockAccountsControllerActions"], function() {
    var controller = require("userfrmBlockUnblockAccountsController");
    var controllerActions = ["frmBlockUnblockAccountsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
