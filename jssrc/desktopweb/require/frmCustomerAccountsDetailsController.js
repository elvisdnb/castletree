define("userfrmCustomerAccountsDetailsController", {
    rowinfo: null,
    countnumber: null,
    fromDate: null,
    holidaynext: null,
    reviseddate: null,
    datefilter: null,
    duedatefilter: null,
    graphvalues: null,
    revisedgraph: [],
    gblCid: null,
    segData: [],
    _clientId: "",
    _navObj: {},
    tabToShow: "Accounts", //default show accounts tab
    contractLogData: [],
    accountsData: [],
    depositsData: [],
    loansData: [],
    accountsDataLoaded: false,
    depositsDataLoaded: false,
    loansDataLoaded: false,
    companyId: "NL0020001",
    onNavigate: function(navigationObj) {
        if (navigationObj !== null && navigationObj !== undefined) {
            navigationObj.customerId = "500003";
        } else {
            navigationObj = {
                "customerId": "500003"
            };
        }
        this.view.flxNoProducts.setVisibility(false);
        this.view.segDeposits.setVisibility(false);
        this.view.segMin2.setVisibility(false);
        this.view.SegOne.setVisibility(false);
        this.view.flxNow.setVisibility(false);
        this.contractLogData = [];
        this.accountsData = [];
        this.depositsData = [];
        this.loansData = [];
        this.accountsDataLoaded = false;
        this.depositsDataLoaded = false;
        this.loansDataLoaded = false;
        this._navObj = navigationObj;
        this.view.ContractRequestDescription.isVisible = false;
        this.view.flxMessageInfo.isVisible = false;
        //this.view.lblMessageWithReferenceNumber.t
        // Service call and load Customer info section global component
        if (navigationObj) {
            if (this.gblCid != navigationObj.customerId) {
                this.view.custInfo.restCustomerValues();
                this.gblCid = navigationObj.customerId;
                this.getCustomerDetails();
            }
            if (Application.validation.isNullUndefinedObj(navigationObj.contractLog) !== "") {
                gblCurrentContractLog = navigationObj.contractLog;
                this.contractLogData = navigationObj.contractLog;
                kony.print("contractLogData====>" + JSON.stringify(this.contractLogData));
                this.loadingDataToRequestLog();
                this.view.ContractRequestDescription.isVisible = true;
                if (gblCurrentContractLog[0].contactStatus === "COMPLETED" || gblCurrentContractLog[0].contactStatus === "Completed") {
                    this.view.ContractRequestDescription.isVisible = false;
                }
            }
        }
        //TODO : Add function call to  eset the form fields ??
        //e.g. workflwow params ?
        this.bindEvents();
        //calling both accounts and loans ? to store in gbl variable ?
        //based on incoming flow, show the tab.. call the service anyways...
        //to show reflected data. in segment after a service.
        if (navigationObj && navigationObj.customerId) {
            //set the tab to showt
            // call the services for the tabs
            //Note loadAccounts is called to fill a global variable
            this.view.ProgressIndicator.isVisible = true;
            this.gblCid = navigationObj.customerId;
            this.loadLoans();
            this.loadAccounts();
            this.loadDeposits();
        } else {
            // There is not customer id in the navigation parameters ?
            //something wrong ?
        }
    },
    setTabToShow: function() {
        //set the tab to showt
        if (this.loansDataLoaded && this.accountsDataLoaded && this.depositsDataLoaded) {
            if (this._navObj.isShowLoans === "true") {
                this.showLoansList();
            } else if (this._navObj.isShowDeposits === "true" && this._navObj.showMessageInfo === false) {
                this.showDepositsList();
                this.view.flxMessageInfo.isVisible = false;
            } else if (this._navObj.isShowDeposits === "true" && this._navObj.showMessageInfo === true) {
                this.showDepositsList();
                this.view.flxMessageInfo.isVisible = true;
            } else { // default view is accounts
                this.showAccountsList();
            }
        } else {
            return;
        }
    },
    noPreshow: function() {},
    postShowAccoutDetails: function() {
        this.view.ErrorAllert.isVisible = false;
        //TODO : write code here to show the success message container- may be with timer, using navigation params
        //dont write visible on/ofg logics in on navigate.
        //e.g. showToastFor10Seconds
        var navigationObj = this._navObj;
        if (navigationObj.messageInfo) {
            this.view.flxCntnr.isVisible = false;
            this.view.flxHeaderMenu.isVisible = false;
            this.view.flxMessageInfo.isVisible = true;
            this.view.flxMessageInfo.lblMessageWithReferenceNumber.text = navigationObj.messageInfo;
            if (gblCurrentContractLog.length > 0) {
                if (gblCurrentContractLog[0].contactStatus === "COMPLETED" || gblCurrentContractLog[0].contactStatus === "Completed") {
                    kony.print("Already Completed the workflow");
                } else {
                    this.UpdateCompleteContractRequestLog();
                }
            }
            //this.showSuccessWithTime(navigationObj.messageInfo);
        }
        if (navigationObj.clientId) {
            this.view.flxCntnr.isVisible = false;
            this.view.flxHeaderMenu.isVisible = false;
            this.view.flxMessageInfo.isVisible = true;
            this.view.flxMessageInfo.lblMessageWithReferenceNumber.text = "New client record created Client ID - " + navigationObj.clientId;
            if (gblCurrentContractLog.length > 0) {
                if (gblCurrentContractLog[0].contactStatus === "COMPLETED" || gblCurrentContractLog[0].contactStatus === "Completed") {
                    kony.print("Already Completed the workflow");
                } else {
                    this.UpdateCompleteContractRequestLog();
                }
            }
            //this.showSuccessWithTime("New client record created Client ID - " + navigationObj.clientId);
        }
        if (navigationObj.isPopupShow === "false") {
            this.view.flxMessageInfo.isVisible = false;
        }
        if (this.tabToShow === "Accounts") {
            this.view.flxLoanButtonWrapper.isVisible = false;
            this.view.flxDepositButtonWrapper.isVisible = false;
        }
    },
    bindEvents: function() {
        this.view.postShow = this.postShowAccoutDetails;
        this.view.flxLoanTypes.isVisible = false;
        this.view.flxDepositTypes.isVisible = false;
        this.view.btnCreateLoan.right = "2%";
        // this.view.DPWSelectLoan.options = '[{"label": "Select","value":"None"},{"label": "Personal Loan","value":"PERSONAL.LOAN.STANDARD"},{"label": "Mortgage Loan","value":"MORTGAGE"}]';
        this.view.txtField1.onDone = this.reloadLoansForNewCustomer;
        this.view.flxMessageInfo.flxImageHeader.imgHeaderClose.onTouchEnd = this.closeSuccessAllert;
        this.view.MainTabs.btnAccounts1.onClick = this.showAccountsList;
        this.view.MainTabs.btnAccounts3.onClick = this.showLoansList;
        this.view.MainTabs.btnAccounts2.onClick = this.showDepositsList;
        this.view.flxPersonal.onTouchEnd = this.navigateToCreateLoan;
        this.view.flxMortgage.onTouchEnd = this.navigateToCreateLoan;
        this.view.btnCreateLoan.onclickEvent = this.loanCreationstab;
        this.view.btnCreateDeposit.onclickEvent = this.depositCreationstab;
        this.view.flxShort.onTouchEnd = this.navigateToCreateDeposit;
        this.view.flxLong.onTouchEnd = this.navigateToCreateDeposit;
        this.view.flxBack.onTouchEnd = this.navToDashboard;
        //CR Log Modification
        this.view.ContractRequestDescription.onClickSubmit = this.onUpdateContractRequestLog;
        this.view.ContractRequestDescription.arrowOnTouchEnd = this.closeExpansionContractRequestLog;
        this.view.ErrorAllert.imgClose.onTouchEnd = this.closeErrorAlertPopup;
        this.view.cusFillDetailsButton.onclickEvent = this.navigateToFullDetails;
    },
    closeErrorAlertPopup: function() {
        this.view.ErrorAllert.isVisible = false;
    },
    navigateToFullDetails: function() {
        var params = {};
        params.cid = this.gblCid;
        var navObject = new kony.mvc.Navigation("frmFullDetails");
        navObject.navigate(params);
    },
    navToDashboard: function() {
        var params = {};
        params.isPopupShow = "false";
        var navObject = new kony.mvc.Navigation("frmDashboard");
        navObject.navigate(params);
    },
    showAccountsList: function() {
        this.view.flxNoProducts.setVisibility(false);
        this.view.flxNow.setVisibility(false);
        this.tabToShow = "Accounts";
        this.doLayoutForAccounts();
        this.view.flxLoanButtonWrapper.isVisible = false;
        this.view.flxDepositButtonWrapper.isVisible = false;
    },
    showDepositsList: function() {
        this.view.flxNoProducts.setVisibility(false);
        this.view.flxNow.setVisibility(false);
        this.tabToShow = "Deposits";
        this.doLayoutForDeposits();
        this.onTouchImgDropDown();
        this.advancedSearchAccounts();
        this.getWithdrawalAccounts();
    },
    showLoansList: function() {
        this.view.flxNoProducts.setVisibility(false);
        this.view.flxNow.setVisibility(false);
        this.tabToShow = "Loans";
        this.doLayoutForLoans();
    },
    doLayoutForLoans: function() {
        //hide
        this.view.MainTabs.flxContainerAccount.skin = sknNotselected;
        this.view.MainTabs.flxContainerAccount.setVisibility(false);
        this.view.MainTabs.flxontainerLoans.setVisibility(true);
        this.view.MainTabs.flxontainerLoans.skin = sknSelected;
        this.view.MainTabs.flxContainerdeposits.setVisibility(false);
        this.view.MainTabs.flxContainerdeposits.skin = sknNotselected;
        this.view.flxLoanTypes.isVisible = false;
        this.view.flxDepositTypes.isVisible = false;
        this.view.SegOne.setVisibility(false);
        this.view.segDeposits.setVisibility(false);
        this.view.flxLoanButtonWrapper.isVisible = true;
        this.view.flxDepositButtonWrapper.isVisible = false;
        //this.view.btnCreateLoan.isVisible=true;
        this.view.btnCreateLoan.right = "2%";
        this.view.flxLoansHdrSeg.setVisibility(true);
        if (this.loansData.length > 0) {
            this.view.flxNoProducts.setVisibility(false);
            this.view.flxLoansHdrSeg.setVisibility(true);
            this.view.segMin2.setVisibility(true);
        } else {
            this.view.flxNoProducts.setVisibility(true);
            this.view.lblNoProducts.text = "There is no existing Loan for this customer,\nClick on create new loan to explore options";
            this.view.segMin2.setVisibility(false);
            this.view.flxLoansHdrSeg.setVisibility(false);
        }
        this.view.MainTabs.btnAccounts1.skin = sknTabUnselected;
        this.view.MainTabs.btnAccounts3.skin = sknTabSelected;
        this.view.MainTabs.btnAccounts2.skin = sknTabUnselected;
        // ui adjustments for list header
        this.view.lblAcNum.width = "20%";
        this.view.lblAcType.left = "0%";
        this.view.lblAcType.width = "15%";
        this.view.lblCCY.width = "10%";
        this.view.lblClearBal.width = "15%";
        this.view.lblLockedAc.width = "15%";
        this.view.lblAcServices.left = "5%";
        this.view.lblAcServices.width = "15%";
        this.view.lblAcNum.text = "Account Type";
        this.view.lblAcType.text = "Account Number";
        this.view.lblClearBal.text = "Loan balance";
        this.view.lblLockedAc.text = "Loan Start Date";
        this.view.lblAvlBal.setVisibility(false);
        this.view.lblAcServices.text = "Service Action";
        this.view.lblAcNum.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
        this.view.lblAcType.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblCCY.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblClearBal.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        this.view.lblLockedAc.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblAcServices.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    },
    doLayoutForAccounts: function() {
        //hide
        this.view.MainTabs.flxontainerLoans.setVisibility(false);
        this.view.MainTabs.flxontainerLoans.skin = sknNotselected;
        this.view.MainTabs.flxContainerdeposits.setVisibility(false);
        this.view.MainTabs.flxContainerdeposits.skin = sknNotselected;
        this.view.MainTabs.flxContainerAccount.setVisibility(true);
        this.view.MainTabs.flxContainerAccount.skin = sknSelected;
        //     this.view.flxLoanButtonWrapper.isVisible = false;
        //     this.view.flxDepositButtonWrapper.isVisible = false;
        //this.view.btnCreateLoan.right = "-20%";
        this.view.flxLoanTypes.isVisible = false;
        this.view.flxDepositTypes.isVisible = false;
        //this.view.flxLoansHdrSeg.setVisibility(false);
        this.view.segMin2.setVisibility(false);
        //show
        if (this.accountsData.length > 0) {
            this.view.flxNoProducts.setVisibility(false);
            this.view.SegOne.setVisibility(true);
            this.view.flxLoansHdrSeg.setVisibility(true);
        } else {
            this.view.flxNoProducts.setVisibility(true);
            this.view.lblNoProducts.text = "There is no existing Account for this customer,\nClick on create new account to explore options";
            this.view.SegOne.setVisibility(false);
            this.view.flxLoansHdrSeg.setVisibility(false);
        }
        this.view.segDeposits.setVisibility(false);
        //this.view.SegOne.removeAll();
        //this.view.SegOne.setData(this.accountsData);
        this.view.MainTabs.btnAccounts1.skin = sknTabSelected;
        this.view.MainTabs.btnAccounts3.skin = sknTabUnselected;
        this.view.MainTabs.btnAccounts2.skin = sknTabUnselected;
        // ui adjustments for list header
        this.view.lblAcNum.width = "16%";
        this.view.lblAcType.left = "0%";
        this.view.lblAcType.width = "10.3%";
        this.view.lblCCY.width = "12.8%";
        this.view.lblClearBal.width = "12.5%";
        this.view.lblLockedAc.width = "12%";
        this.view.lblAvlBal.left = "0%";
        this.view.lblAvlBal.width = "12.6%";
        this.view.lblAcServices.left = "2%";
        this.view.lblAcServices.width = "22%";
        this.view.lblAcNum.text = "Account Type";
        this.view.lblAcType.text = "Account Number";
        this.view.lblClearBal.text = "Ledger balance";
        this.view.lblLockedAc.text = "Cleared balance";
        this.view.lblAvlBal.setVisibility(true);
        this.view.lblAvlBal.text = "Available Limit";
        this.view.lblAcServices.text = "Account Services";
        this.view.lblAcNum.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
        this.view.lblAcType.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblCCY.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblClearBal.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        this.view.lblLockedAc.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        this.view.lblAvlBal.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        this.view.lblAcServices.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    },
    doLayoutForDeposits: function() { //TODO
        //hide
        this.view.MainTabs.flxontainerLoans.setVisibility(false);
        this.view.MainTabs.flxContainerAccount.setVisibility(false);
        this.view.MainTabs.flxontainerLoans.skin = sknNotselected;
        this.view.MainTabs.flxContainerAccount.skin = sknNotselected;
        this.view.MainTabs.flxContainerdeposits.setVisibility(true);
        this.view.MainTabs.flxContainerdeposits.skin = sknSelected;
        this.view.flxLoanButtonWrapper.isVisible = false;
        this.view.flxDepositButtonWrapper.isVisible = true;
        this.view.btnCreateDeposit.isVisible = true;
        this.view.flxDepositButtonWrapper.forceLayout();
        //this.view.btnCreateLoan.right = "-20%";
        this.view.flxLoanTypes.isVisible = false;
        this.view.flxDepositTypes.isVisible = false;
        //this.view.flxLoansHdrSeg.setVisibility(false);
        this.view.segMin2.setVisibility(false);
        //show
        this.view.SegOne.setVisibility(false);
        if (this.depositsData.length > 0) {
            this.view.flxNoProducts.setVisibility(false);
            this.view.segDeposits.setVisibility(true);
            this.view.flxLoansHdrSeg.setVisibility(true);
        } else {
            this.view.flxNoProducts.setVisibility(true);
            this.view.lblNoProducts.text = "There is no existing Deposit for this customer,\nClick on create new deposit to explore options";
            this.view.segDeposits.setVisibility(false);
            this.view.flxLoansHdrSeg.setVisibility(false);
        }
        //this.view.SegOne.removeAll();
        //this.view.SegOne.setData(this.depositsData);
        this.view.MainTabs.btnAccounts2.skin = sknTabSelected;
        this.view.MainTabs.btnAccounts1.skin = sknTabUnselected;
        this.view.MainTabs.btnAccounts3.skin = sknTabUnselected;
        // ui adjustments for list header
        this.view.lblAcNum.width = "10%";
        this.view.lblAcType.left = "1.8%";
        this.view.lblAcType.width = "11.8%";
        this.view.lblCCY.width = "10%";
        this.view.lblClearBal.width = "13.7%";
        this.view.lblLockedAc.width = "13%";
        this.view.lblAvlBal.left = "3.3%";
        this.view.lblAvlBal.width = "8%";
        this.view.lblAcServices.left = "2.6%";
        this.view.lblAcServices.width = "22%";
        this.view.lblAcNum.text = "Account";
        this.view.lblAcType.text = "Deposit Type";
        this.view.lblClearBal.text = "Balance";
        this.view.lblLockedAc.text = "Start Date";
        this.view.lblAvlBal.setVisibility(true);
        this.view.lblAvlBal.text = "Maturity Date";
        this.view.lblAcServices.text = "Deposit Services";
        this.view.lblAcNum.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblAcType.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
        this.view.lblCCY.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblClearBal.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_RIGHT;
        this.view.lblLockedAc.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblAvlBal.contentAlignment = constants.CONTENT_ALIGN_CENTER;
        this.view.lblAcServices.contentAlignment = constants.CONTENT_ALIGN_MIDDLE_LEFT;
    },
    depositServiceListSelection: function(widget, context) {
        try {
            var selectedKey = widget.selectedKey;
            var selectedContext = context["rowIndex"];
            kony.print("Selected Key: " + selectedKey);
            var selectedSegData = this.depositsData[selectedContext];
            selectedSegData.customerId = this.gblCid;
            //       alert("selectedSegdata::::" + JSON.stringify(selectedSegData));
            if (selectedKey === "DepositService1") {
                var navToFundDeposit = new kony.mvc.Navigation("frmFundDeposit");
                navToFundDeposit.navigate(selectedSegData);
            } else if (selectedKey === "DepositService2") {
                var navToDepositWithdrawal = new kony.mvc.Navigation("frmDepositWithdrawal");
                navToDepositWithdrawal.navigate(selectedSegData);
            } else if (selectedKey === "DepositService4") {
                var navToBlockUnblock = new kony.mvc.Navigation("frmBlockUnblockDeposits");
                navToBlockUnblock.navigate(selectedSegData);
            } else if (selectedKey === "DepositService5") {
                var navToChangeInterest = new kony.mvc.Navigation("frmChangeInterestPayoutTerms");
                navToChangeInterest.navigate(selectedSegData);
            } else {
                kony.print("Do nothing");
            }
        } catch (err) {
            kony.print("Exception in depositServiceListSelection: " + err);
        }
    },
    getCustomerDetails: function() {
        var self = this;
        var serviceName = "LendingNew";
        // Get an instance of SDK
        var client = kony.sdk.getCurrentInstance();
        var integrationSvc = client.getIntegrationService(serviceName);
        var operationName = "getCustomers";
        var params = {
            "customerId": self.gblCid
        };
        var headersParam = {
            "customerId": self.gblCid
        };
        var options = {
            "httpRequestOptions": {
                "timeoutIntervalForRequest": 60,
                "timeoutIntervalForResource": 600
            }
        };
        integrationSvc.invokeOperation(operationName, headersParam, params, this.getCustomerDetailsSCB, this.getCustomerDetailsFCB, options);
    },
    getCustomerDetailsSCB: function(response) {
        this.view.custInfo.setCustomerValues(response);
    },
    getCustomerDetailsFCB: function(error) {
        kony.print("Integration Service Failure:" + JSON.stringify(error));
    },
    //TODO : move this to utility
    formatAmount: function(amount) {
        var num, flag = false;
        if (amount === null || amount === undefined || isNaN(amount)) {
            return;
        }
        amount = Number(amount).toFixed(2);
        var decimal = "."; //this.getDecimalSeparator(locale);
        if (amount.indexOf(".") != -1 || amount.indexOf(",") != -1) {
            if (amount.indexOf(".") != -1) {
                amount = amount.replace(".", decimal);
            } else if (amount.indexOf(",") != -1) {
                amount = amount.replace(",", decimal);
            }
            num = amount.split(decimal)[0];
            var dec = amount.split(decimal)[1];
            if (num.indexOf("-") != -1) {
                num = num.split("-")[1];
                flag = true;
            }
            if (num.length > 3) {
                for (var i = num.length - 1; i >= 0;) {
                    if (i >= 3) {
                        num = num.substring(0, i - 2) + "," + num.substring(i - 2, num.length);
                    }
                    i = i - 3;
                }
            }
            if (flag === true) {
                return "-" + num + decimal + dec;
            }
            return num + decimal + dec;
        } else {
            return amount;
        }
    },
    loadLoans: function() {
        var self = this;
        var serviceName = "LendingNew";
        // Get an instance of SDK
        var client = kony.sdk.getCurrentInstance();
        var integrationSvc = client.getIntegrationService(serviceName);
        var operationName = "getCustomerLoans";
        var params = {
            "customerId": self.gblCid
        };
        var options = {
            "httpRequestOptions": {
                "timeoutIntervalForRequest": 60,
                "timeoutIntervalForResource": 600
            }
        };
        integrationSvc.invokeOperation(operationName, " ", params, this.getCustomerLoansSCB, this.getCustomerLoansFCB, options);
    },
    getCustomerLoansSCB: function(response) {
        this.view.segMin2.removeAll();
        var loansSegData = [];
        this.loansDataLoaded = true;
        var loansSegDataMap = {
            "lblHeaderAN": "lblHeaderAN",
            "lblHeaderAccountType": "lblHeaderAccountType",
            "lblCCY": "lblCCY",
            "lblClearedBalance": "lblClearedBalance",
            "lblAvailableLimit": "lblAvailableLimit",
            "lblHdr": "lblHdr",
            "lblLoanAccountId": "lblLoanAccountId",
            "lblArrangementId": "lblArrangementId",
            "lblServices": "lblServices"
        };
        if (response.body.length > 5) {
            this.view.segMin2.width = "98%";
        } else {
            this.view.segMin2.width = "96%";
        }
        for (var i = 0; i < response.body.length; i++) {
            var currentBalance = Application.numberFormater.convertForDispaly(this.removeMinus(response.body[i].loanBalance), response.body[i].loanCurrency);
            if (currentBalance === "NaN") {
                currentBalance = "";
            }
            var data = {
                "lblHeaderAN": response.body[i].loanAccountId,
                "lblHeaderAccountType": response.body[i].productName,
                "loanProduct": response.body[i].loanProduct,
                "customerId": this.gblCid,
                "lblCCY": response.body[i].loanCurrency,
                //"lblClearedBalance":this.formatAmount(this.removeMinus(response.body[i].loanBalance)), - commented for implementing currency conversation
                "lblClearedBalance": currentBalance,
                "lblAvailableLimit": this.toShortFormat(response.body[i].loanStartDate),
                "lblHdr": this.toShortFormat(response.body[i].loanEndDate),
                "lblLoanAccountId": response.body[i].loanAccountId,
                "lblArrangementId": response.body[i].arrangementId,
                "lblServices": {
                    "masterData": loanAccountServiceType,
                    "selectedKey": "select",
                    "onSelection": this.serviceListSelection
                } //, "onSelection" : this.onLoanServiceTypeSelection}
            };
            //kony.application.getCurrentForm().segMin.setVisibility(false); 
            loansSegData.push(data);
        }
        this.loansData = loansSegData;
        this.view.segMin2.widgetDataMap = loansSegDataMap;
        this.view.segMin2.setData(loansSegData);
        this.setTabToShow();
        if (this.loansDataLoaded && this.accountsDataLoaded && this.depositsDataLoaded) {
            this.view.ProgressIndicator.isVisible = false;
        }
        //     // Show the loans tab
        //     if(this.tabToShow == "Loans"){
        //       this.view.ProgressIndicator.isVisible=false;
        //       this.doLayoutForLoans();
        //     }
    },
    toShortFormat: function(date) {
        var anyDate = new Date(date);
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var day = anyDate.getDate(date);
        var monthIndex = anyDate.getMonth();
        var monthName = monthNames[monthIndex];
        var year = anyDate.getFullYear();
        return "" + day + " " + monthName + " " + year;
    },
    getCustomerLoansFCB: function(error) {
        this.loansDataLoaded = true;
        if (this.loansDataLoaded && this.accountsDataLoaded && this.depositsDataLoaded) {
            this.view.ProgressIndicator.isVisible = false;
        }
        if (error.opstatus == "8009") {}
        kony.print("No loans exist for the customer");
        this.view.segMin2.setData([]);
        //this.doLayoutForLoans()
    },
    serviceListSelection: function(widget, context) {
        kony.print("################list box is fired");
        var eventobj = widget.selectedKey;
        var selectedContext = context["rowIndex"];
        //alert("selectedContext::"+JSON.stringify(selectedContext));
        var selectedSegData = context.widgetInfo.data[selectedContext];
        //alert("selectedContext::"+JSON.stringify(selectedSegData));
        if (eventobj === "loanService1") {
            var navToChangeInterest6 = new kony.mvc.Navigation("holidayPayment");
            navToChangeInterest6.navigate(selectedSegData);
        } else if (eventobj === "loanService2") {
            var navToChangeInterest = new kony.mvc.Navigation("CustomerLoanInterestChange");
            navToChangeInterest.navigate(selectedSegData);
        } else if (eventobj === "loanService3") {
            if (selectedSegData.loanProduct === "PERSONAL.LOAN.STANDARD") {
                this.view.ErrorAllert.lblMessage.text = "Manual Disbursement Not Allowed";
                this.view.ErrorAllert.isVisible = true;
            } else {
                var navToChangeInterest3 = new kony.mvc.Navigation("frmDisburse");
                navToChangeInterest3.navigate(selectedSegData);
            }
        } else if (eventobj === "loanService4") {
            var messageInfo = "";
            selectedSegData.messageInfo = messageInfo;
            var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerLoanIncreaseAmt");
            navToChangeInterest1.navigate(selectedSegData);
        } else if (eventobj === "loanService5") {
            //       if(selectedSegData.lblClearedBalance === "0.00" || selectedSegData.lblClearedBalance === "0,00"){
            //         this.view.ErrorAllert.lblMessage.text = "There is no outstanding balance to Payoff";
            //         this.view.ErrorAllert.isVisible = true;
            //         widget.selectedKey ="select";
            //       }else if(selectedSegData.lblClearedBalance === undefined || selectedSegData.lblClearedBalance === ""){
            //         this.view.ErrorAllert.lblMessage.text = "Loan is not yet disbursed";
            //         this.view.ErrorAllert.isVisible = true;
            //         widget.selectedKey ="select";
            //       }else{
            var navToChangeInterest5 = new kony.mvc.Navigation("frmLoanPayoff");
            navToChangeInterest5.navigate(selectedSegData);
            //       }
        } else if (eventobj === "loanService6") {
            this.view.ProgressIndicator.isVisible = true;
            var navToChangeInterest2 = new kony.mvc.Navigation("repayment");
            navToChangeInterest2.navigate(selectedSegData);
        } else {
            //kony.application.getCurrentForm().flxOnclickDummy.onClick(context);
            this.getLoad();
            //kony.application.getCurrentForm().segMin.setVisibility(false); 
        }
    },
    removeMinus: function(x) {
        return Math.abs(x);
    },
    reloadLoansForNewCustomer: function() {
        var changedText = this.view.txtField1.text;
        if (changedText && changedText != "") {
            var customerId = changedText; //this.view.segMin2.data[0];//selectedRowItems[0];  
            var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerAccountsDetails");
            var navObjet = {};
            navObjet.customerId = customerId;
            navObjet.isShowLoans = "true";
            navToChangeInterest1.navigate(navObjet);
        }
    },
    showSuccessWithTime: function(toasmsg) {
        try {
            this.view.flxCntnr.isVisible = false;
            this.view.flxHeaderMenu.isVisible = false;
            this.view.flxMessageInfo.isVisible = true;
            this.view.flxMessageInfo.lblMessageWithReferenceNumber.text = toasmsg;
            //TODO - comment below line in case we do not want to close automatically
            kony.timer.schedule("toastTimer", this.timerCallbackCloseToast, 20, false);
            this.view.scrollToBeginning();
        } catch (err) {
            kony.print("Exception in showSuccessWithTime: " + err);
        }
    },
    timerCallbackCloseToast: function() {
        try {
            kony.timer.cancel("toastTimer");
        } catch (err) {
            //not able to cancel timer
        }
        //important
        this.closeSuccessAllert();
    },
    closeSuccessAllert: function() {
        //this.view.flxCntnr.isVisible=true;
        this.view.flxMessageInfo.isVisible = false;
    },
    loadAccounts: function() {
        gblCutacctArray = [];
        gblCutacctWidgetDate = [];
        // var custid=this.partyIds;
        var serviceName = "SCVServices";
        var operationName = "getSCVAccounts";
        var headers = {
            "companyId": "NL0020001"
        };
        var inputParams = {
            "customerId": this.gblCid
        };
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackaccount, this.failureCallBackaccount);
    },
    successCallBackaccount: function(response) {
        try {
            this.view.SegOne.removeAll();
            var scope = this;
            var dataForGlobalDropdown = [];
            this.accountsDataLoaded = true;
            var accountServicesData = [
                ["select", "Select Service"],
                ["AccountService1", "Add/Remove Party"],
                ["AccountService2", "Block/Unblock Funds"],
                ["AccountService3", "Change OD Limit"],
                ["AccountService4", "Change Product"],
                ["AccountService5", "Make A Payment"],
                ["AccountService6", "Restrict Posting"]
            ];
            var dataMap = {
                "lblAcNum": "lblAcNum",
                "lblAcType": "lblAcType",
                "lblCCY": "lblCCY",
                "lblClearBal": "lblClearBal",
                "lblLockedAc": "lblLockedAc",
                "lblAvlBal": "lblAvlBal",
                "ListBox0c039746869c84b": "ListBox0c039746869c84b"
            };
            //SET the global variables
            var widgetDataMapForGlobalDropdown = {
                "lblAccountId": "accountid",
                "lblWorkingBal": "balance",
                "lbltype": "type"
            };
            this.accountsData = [];
            var accountType;
            var accountNumber;
            var currency;
            var ledgerBalance;
            var clearedBalance;
            var availableLimit;
            for (var i = 0; i < response.body.length; i++) {
                accountType = response.body[i].displayName;
                accountType = Application.validation.isNullUndefinedObj(accountType);
                accountNumber = response.body[i].accountId;
                accountNumber = Application.validation.isNullUndefinedObj(accountNumber);
                if (accountNumber !== "") {
                    accountNumber = this.maskAccountNumber(accountNumber);
                }
                currency = response.body[i].currencyId;
                currency = Application.validation.isNullUndefinedObj(currency);
                ledgerBalance = response.body[i].onlineActualBalance;
                ledgerBalance = Application.validation.isNullUndefinedObj(ledgerBalance);
                if (ledgerBalance !== "") {
                    ledgerBalance = Application.numberFormater.convertForDispaly(this.removeMinus(ledgerBalance), currency);
                    if (ledgerBalance === "NaN") {
                        ledgerBalance = "0.00";
                    }
                } else {
                    ledgerBalance = Application.numberFormater.convertForDispaly(this.removeMinus("0"), currency);
                }
                clearedBalance = response.body[i].onlineClearedBalance;
                clearedBalance = Application.validation.isNullUndefinedObj(clearedBalance);
                if (clearedBalance !== "") {
                    clearedBalance = Application.numberFormater.convertForDispaly(this.removeMinus(clearedBalance), currency);
                    if (clearedBalance === "NaN") {
                        clearedBalance = "0.00";
                    }
                } else {
                    clearedBalance = Application.numberFormater.convertForDispaly(this.removeMinus("0"), currency);
                }
                availableLimit = response.body[i].availableLimit;
                availableLimit = Application.validation.isNullUndefinedObj(availableLimit);
                if (availableLimit === "NOLIMIT" || availableLimit === "") {
                    availableLimit = Application.numberFormater.convertForDispaly(this.removeMinus("0"), currency);
                    if (availableLimit === "NaN") {
                        availableLimit = "0.00";
                    }
                } else {
                    availableLimit = Application.numberFormater.convertForDispaly(this.removeMinus(availableLimit), currency);
                    if (availableLimit === "NaN") {
                        availableLimit = "0.00";
                    }
                }
                var data = {
                    "lblAcNum": accountType,
                    "lblAcType": accountNumber,
                    "lblCCY": currency,
                    "lblClearBal": ledgerBalance,
                    "lblLockedAc": clearedBalance,
                    "lblAvlBal": availableLimit,
                    "ListBox0c039746869c84b": {
                        "masterData": accountServicesData,
                        "selectedKey": "select",
                        "onSelection": scope.accountServiceListSelection
                    }
                };
                var currentBalance2 = Application.numberFormater.convertForDispaly(response.body[i].onlineActualBalance, response.body[i].currencyId);
                if (currentBalance2 === "NaN") {
                    currentBalance2 = "";
                }
                var stringConcat = currentBalance2.substring(0, 2);
                var currentSymbol = Application.numberFormater.getCurrencySymbolBasedonCurrencyCode(response.body[i].currencyId);
                var mathSignDetermine = Math.sign(stringConcat);
                var avaliableAmount = {};
                if (mathSignDetermine === -1 || mathSignDetermine === 0) {
                    avaliableAmount = {
                        text: currentSymbol + " " + currentBalance2,
                        skin: "sknflxFontD32E2FPX12"
                    };
                } else {
                    avaliableAmount = {
                        text: currentSymbol + " " + currentBalance2,
                        skin: "sknflxFont434343px12"
                    };
                }
                var newElt = {
                    accountid: response.body[i].accountId,
                    balance: avaliableAmount,
                    type: response.body[i].displayName
                };
                dataForGlobalDropdown.push(newElt);
                this.accountsData.push(data);
            } //End for loop
            //set the accounts segment data 
            this.view.SegOne.widgetDataMap = dataMap;
            this.view.SegOne.removeAll();
            this.view.SegOne.setData(this.accountsData);
            this.setTabToShow();
            //       if(gblCutacctWidgetDate.length === 0 && gblCutacctArray.length === 0){
            gblCutacctWidgetDate = widgetDataMapForGlobalDropdown;
            gblCutacctArray = dataForGlobalDropdown;
            //       }
            if (this.loansDataLoaded && this.accountsDataLoaded && this.depositsDataLoaded) {
                this.view.ProgressIndicator.isVisible = false;
            }
            //     // Show the accounts tab
            //     if(this.tabToShow === "Accounts"){
            //       this.view.ProgressIndicator.isVisible=false;
            //       this.doLayoutForAccounts();
            //     }
        } catch (err) {
            kony.print("Exception in successCallBackaccount: " + err);
        }
    },
    failureCallBackaccount: function(res) {
        this.accountsDataLoaded = true;
        if (this.loansDataLoaded && this.accountsDataLoaded && this.depositsDataLoaded) {
            this.view.ProgressIndicator.isVisible = false;
        }
        kony.print("Error" + JSON.stringify(res));
        this.view.SegOne.setData([]);
        //this.doLayoutForAccounts();
    },
    accountServiceListSelection: function(widget, context) {
        try {
            kony.print("################list box is fired");
            var eventobj = widget.selectedKey;
            var selectedContext = context["rowIndex"];
            kony.print("selected Row Index:: " + JSON.stringify(selectedContext));
            var selectedSegData = context.widgetInfo.data[selectedContext];
            selectedSegData.customerId = this.gblCid;
            kony.print("selected Row Data:: " + JSON.stringify(selectedSegData));
            if (eventobj === "AccountService1") {
                var navToAddRemoveOwner = new kony.mvc.Navigation("");
                navToAddRemoveOwner.navigate(selectedSegData);
            } else if (eventobj === "AccountService2") {
                var navToBlockUnblockFunds = new kony.mvc.Navigation("frmBlockUnblockAccounts");
                navToBlockUnblockFunds.navigate(selectedSegData);
            } else if (eventobj === "AccountService3") {
                var navToChangeInterest3 = new kony.mvc.Navigation("frmDisburse");
                navToChangeInterest3.navigate(selectedSegData);
            } else if (eventobj === "AccountService4") {
                var messageInfo = "";
                selectedSegData.messageInfo = messageInfo;
                var navToChangeInterest1 = new kony.mvc.Navigation("frmCustomerLoanIncreaseAmt");
                navToChangeInterest1.navigate(selectedSegData);
            } else if (eventobj === "AccountService5") {
                var navToChangeInterest5 = new kony.mvc.Navigation("frmLoanPayoff");
                navToChangeInterest5.navigate(selectedSegData);
            } else if (eventobj === "AccountService6") {
                this.view.ProgressIndicator.isVisible = true;
                var navToChangeInterest6 = new kony.mvc.Navigation("repayment");
                navToChangeInterest6.navigate(selectedSegData);
            } else {}
        } catch (err) {
            kony.print("Exception in accountServiceListSelection: " + err);
        }
    },
    loadDeposits: function() {
        // var custid=this.partyIds;
        var serviceName = "SCVServices";
        var operationName = "getSCVDeposits";
        //this.gblCid = "500003";
        var headers = {
            "companyId": "NL0020001",
            "customerId": this.gblCid
        };
        var inputParams = {};
        MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackDeposits, this.failureCallBackDeposits);
    },
    successCallBackDeposits: function(response) {
        var self = this;
        var dataForGlobalDropdown = [];
        this.depositsDataLoaded = true;
        var dataMap = {
            "lblAcNum": "lblAcNum",
            "lblAcType": "lblAcType",
            "lblCCY": "lblCCY",
            "lblClearBal": "lblClearBal",
            "lblLockedAc": "lblLockedAc",
            "lblAvlBal": "lblAvlBal",
            "lblArrangementID": "lblArrangementID",
            "ListBox0c039746869c84b": "ListBox0c039746869c84b"
        };
        //SET the global variables
        var widgetDataMapForGlobalDropdown = {
            "lblAccountId": "accountid",
            "lblWorkingBal": "balance",
            "lbltype": "type"
        };
        depositServicesData = [
            ["select", "Select Service"],
            ["DepositService1", "Fund Deposit"],
            ["DepositService2", "Withdraw Deposit"],
            //         ["DepositService3", "Change Interest Rate"],
            ["DepositService4", "Block/Unblock Funds"],
            ["DepositService5", "Change Interest Payout Terms"]
        ];
        this.depositsData = [];
        var depAccountId;
        var depProductDescription;
        var depCurrency;
        var depBalance;
        var depStartDate;
        var depEndDate;
        var depArrangementID;
        for (var i = 0; i < response.body.length; i++) {
            depAccountId = response.body[i].depAccountId;
            depAccountId = Application.validation.isNullUndefinedObj(depAccountId);
            depProductDescription = response.body[i].depProductDescription;
            depProductDescription = Application.validation.isNullUndefinedObj(depProductDescription);
            depCurrency = response.body[i].depCurrency;
            depCurrency = Application.validation.isNullUndefinedObj(depCurrency);
            depBalance = response.body[i].depBalance;
            depBalance = Application.validation.isNullUndefinedObj(depBalance);
            if (depBalance !== "") {
                depBalance = Application.numberFormater.convertForDispaly(this.removeMinus(depBalance), depCurrency);
                if (depBalance === "NaN") {
                    depBalance = "0.00";
                }
            } else {
                depBalance = Application.numberFormater.convertForDispaly(this.removeMinus("0"), depCurrency);
            }
            depStartDate = response.body[i].depStartDate;
            depStartDate = Application.validation.isNullUndefinedObj(depStartDate);
            if (depStartDate !== "") {
                depStartDate = this.formatDateToDDMMMYYYY(depStartDate);
            }
            depEndDate = response.body[i].depEndDate;
            depEndDate = Application.validation.isNullUndefinedObj(depEndDate);
            if (depEndDate !== "") {
                depEndDate = this.formatDateToDDMMMYYYY(depEndDate);
            }
            depArrangementID = response.body[i].depArrangementId;
            depArrangementID = Application.validation.isNullUndefinedObj(depArrangementID);
            var data = {
                "lblAcNum": depAccountId,
                "lblAcType": depProductDescription,
                "lblCCY": depCurrency,
                "lblClearBal": depBalance,
                "lblLockedAc": depStartDate,
                "lblAvlBal": depEndDate,
                "lblArrangementID": depArrangementID,
                "ListBox0c039746869c84b": {
                    "masterData": depositServicesData,
                    "selectedKey": "select",
                    "onSelection": self.depositServiceListSelection
                }
            };
            var mathSignDetermine = Math.sign(response.body[i].availableBalance);
            var avaliableAmount = {};
            if (mathSignDetermine === -1 || mathSignDetermine === 0) {
                avaliableAmount = {
                    text: "" + response.body[i].availableBalance,
                    skin: "sknflxFontD32E2FPX12"
                };
            } else {
                avaliableAmount = {
                    text: "" + response.body[i].availableBalance,
                    skin: "sknflxFont434343px12"
                };
            }
            var newElt = {
                accountid: response.body[i].accountId,
                balance: avaliableAmount,
                type: response.body[i].productName
            };
            dataForGlobalDropdown.push(newElt);
            this.depositsData.push(data);
        } //End for loop
        //set the deposits segment data 
        this.view.segDeposits.widgetDataMap = dataMap;
        this.view.segDeposits.removeAll();
        this.view.segDeposits.setData(this.depositsData);
        this.setTabToShow();
        //this.view.SegOne.setData(this.depositsData);
        if (gblCutacctWidgetDate.length === 0 && gblCutacctArray.length === 0) {
            gblCutacctWidgetDate = widgetDataMapForGlobalDropdown;
            gblCutacctArray = dataForGlobalDropdown;
        }
        if (this.loansDataLoaded && this.accountsDataLoaded && this.depositsDataLoaded) {
            this.view.ProgressIndicator.isVisible = false;
        }
    },
    failureCallBackDeposits: function(res) {
        this.depositsDataLoaded = true;
        if (this.loansDataLoaded && this.accountsDataLoaded && this.depositsDataLoaded) {
            this.view.ProgressIndicator.isVisible = false;
        }
        kony.print("Error" + JSON.stringify(res));
        this.view.SegOne.setData([]);
        //this.doLayoutForDeposits();
    },
    //Create Loan
    loanCreationstab: function() {
        this.view.flxLoanTypes.isVisible = true;
    },
    depositCreationstab: function() {
        this.view.flxDepositTypes.isVisible = true;
    },
    navigateToCreateLoan: function(eventObject) {
        var eventId = eventObject.id;
        var navigationParams = {};
        if (eventId === "flxPersonal") {
            navigationParams.selectedValues = "PERSONAL.LOAN.STANDARD";
        } else {
            navigationParams.selectedValues = "MORTGAGE";
        }
        // alert( navigationParams.selectedValues);
        navigationParams.customerId = this.gblCid;
        navigationParams.companyId = this.companyId;
        var navigatetoloan = new kony.mvc.Navigation("frmLoanCreation");
        navigatetoloan.navigate(navigationParams);
    },
    navigateToCreateDeposit: function(eventObject) {
        var eventId = eventObject.id;
        var navigationParams = {};
        if (eventId === "flxShort") {
            navigationParams.selectedValue = "SHORT_TERM";
        } else {
            navigationParams.selectedValue = "LONG_TERM";
        }
        // alert( navigationParams.selectedValues);
        navigationParams.customerId = this.gblCid;
        var navigatetoloan = new kony.mvc.Navigation("frmDepositCreation");
        navigatetoloan.navigate(navigationParams);
    },
    closeExpansionContractRequestLog: function() {
        var textValue = this.view.ContractRequestDescription.arrowChangeText;
        if (textValue === "J") {
            this.view.ContractRequestDescription.collapseResponseContainer();
            this.view.ContractRequestDescription.arrowChangeText = "M";
        } else if (textValue === "M") {
            this.view.ContractRequestDescription.expandREsponseContainer();
            this.view.ContractRequestDescription.arrowChangeText = "J";
        }
    },
    formatDate: function(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return year + "" + month + "" + day;
    },
    onUpdateContractRequestLog: function() {
        try {
            var isvalid = true;
            this.view.ProgressIndicator.isVisible = true;
            this.view.ErrorAllert.isVisible = false;
            var contractTime = Application.validation.isNullUndefinedObj(this.contractLogData[0].lastContactTime);
            //var isValidDate = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(contractTime);
            var contactClient = Application.validation.isNullUndefinedObj(this.contractLogData[0].customerId);
            var requestedDesc = Application.validation.isNullUndefinedObj(this.view.ContractRequestDescription.descriptionText);
            var additionalNotes = Application.validation.isNullUndefinedObj(this.view.ContractRequestDescription.additionalNotesValue);
            var contentType = Application.validation.isNullUndefinedObj(this.contractLogData[0].contactType);
            var contractChannel = Application.validation.isNullUndefinedObj(this.contractLogData[0].channel);
            var contactDate = Application.validation.isNullUndefinedObj(this.contractLogData[0].date);
            var formattedDate = this.formatDate(contactDate);
            var contractStaff = Application.validation.isNullUndefinedObj(this.contractLogData[0].staffs);
            var contractStatus = this.view.ContractRequestDescription.getStatusSelectKey();
            var serviceName = "LendingNew";
            var operationName = "updateContractLog";
            var headers = {};
            var inputParams = {};
            inputParams.customerId = contactClient;
            inputParams.contactType = contentType;
            inputParams.contactStatus = contractStatus;
            inputParams.contactDescription = requestedDesc;
            inputParams.contactStaff = contractStaff;
            inputParams.contactChannel = contractChannel;
            inputParams.contactDate = formattedDate;
            inputParams.contactTime = contractTime;
            inputParams.contactHistoryId = this.contractLogData[0].contactId;
            inputParams.contactNote = additionalNotes;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBPostCreateContractLog, this.failureCBPostCreateContractLog);
            //}
        } catch (err) {
            kony.print("Error in CustomerAccountDetails onUpdateContractRequestLog:::" + err);
        }
    },
    UpdateCompleteContractRequestLog: function() {
        try {
            var isvalid = true;
            this.view.ProgressIndicator.isVisible = true;
            this.view.ErrorAllert.isVisible = false;
            var contractTime = Application.validation.isNullUndefinedObj(gblCurrentContractLog[0].lastContactTime);
            //var isValidDate = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(contractTime);
            var contactClient = Application.validation.isNullUndefinedObj(gblCurrentContractLog[0].customerId);
            var requestedDesc = Application.validation.isNullUndefinedObj(gblCurrentContractLog[0].description);
            //var additionalNotes = Application.validation.isNullUndefinedObj(this.contractLogData[0].lastContactTime);
            var contentType = Application.validation.isNullUndefinedObj(gblCurrentContractLog[0].contactType);
            var contractChannel = Application.validation.isNullUndefinedObj(gblCurrentContractLog[0].channel);
            var contactDate = Application.validation.isNullUndefinedObj(gblCurrentContractLog[0].date);
            var formattedDate = this.formatDate(contactDate);
            var contractStaff = Application.validation.isNullUndefinedObj(gblCurrentContractLog[0].staffs);
            var contractStatus = "COMPLETED";
            var serviceName = "LendingNew";
            var operationName = "updateContractLog";
            var headers = {};
            var inputParams = {};
            inputParams.customerId = contactClient;
            inputParams.contactType = contentType;
            inputParams.contactStatus = contractStatus;
            inputParams.contactDescription = requestedDesc;
            inputParams.contactStaff = contractStaff;
            inputParams.contactChannel = contractChannel;
            inputParams.contactDate = formattedDate;
            inputParams.contactTime = contractTime;
            inputParams.contactHistoryId = gblCurrentContractLog[0].contactId;
            inputParams.contactNote = "";
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBPostCreateContractLogUpdate, this.failureCBPostCreateContractLog);
            //}
        } catch (err) {
            kony.print("Error in CustomerAccountDetails UpdateCompleteContractRequestLog:::" + err);
        }
    },
    successCBPostCreateContractLog: function(res) {
        try {
            gblCurrentContractLog = [];
            //Application.loader.dismissLoader();
            this.view.ErrorAllert.isVisible = false;
            this.currentOverride = {};
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success CallBack :::" + JSON.stringify(res));
            if (res === "" || res === null || res === undefined) {
                kony.print("Response is null or undefined");
            } else {
                var resId = res.header;
                kony.print("RES___ID***" + JSON.stringify(resId));
                var data_id = Application.validation.isNullUndefinedObj(resId.id);
                //alert("New Contract Log Created Successfully!   Reference : "+data_id);
                //this.view.lblMessageWithReferenceNumber.text = "Interest Change applied successfully!      Reference : Interest Change applied successfully!      Reference : "+data_id;
                var navToChangeInterest1 = new kony.mvc.Navigation("frmDashboard");
                var navObjet = {};
                //navObjet.customerId = this._loanObj.customerId;
                //navObjet.isShowLoans = "true";
                navObjet.isPopupShow = "true";
                navObjet.messageInfo = "Contract Log Updated Successfully!   Reference : " + data_id;
                navToChangeInterest1.navigate(navObjet);
            }
        } catch (err) {
            kony.print("Error in CreateContractLog successCBPostCreateContractLog:::" + err);
        }
    },
    successCBPostCreateContractLogUpdate: function(res) {
        try {
            gblCurrentContractLog = [];
            //Application.loader.dismissLoader();
            this.view.ErrorAllert.isVisible = false;
            this.currentOverride = {};
            this.view.ProgressIndicator.isVisible = false;
            kony.print("Success CallBack :::" + JSON.stringify(res));
        } catch (err) {
            kony.print("Error in CreateContractLog successCBPostCreateContractLog:::" + err);
        }
    },
    failureCBPostCreateContractLog: function(res) {
        try {
            this.currentOverride = {};
            //Application.loader.dismissLoader();
            this.view.ProgressIndicator.isVisible = false;
            //alert("Service failure");
            kony.print("Inside ErrorDetails=====>" + JSON.stringify(res.error.errorDetails));
            kony.print("Inside ErrorDetails2=====>" + JSON.stringify(Application.validation.isNullUndefinedObj(res.override)));
            if (res.override != []) {
                var overwriteDetails = [];
                var overWriteData = [];
                var segDataForOverrite = [];
                this.currentOverride = res.override.overrideDetails;
                overwriteDetails = res.override.overrideDetails;
                if (overwriteDetails.length > 0) {
                    for (var a in overwriteDetails) {
                        overWriteData.push(overwriteDetails[a].description); //override.overrideDetails[0].description
                        var json = {
                            "lblSerial": "*",
                            "lblInfo": overwriteDetails[a].description
                        }
                        segDataForOverrite.push(json);
                    }
                    this.view.SegmentPopup.segOverride.widgetDataMap = {
                        "lblSerial": "lblSerial",
                        "lblInfo": "lblInfo"
                    };
                    this.view.SegmentPopup.lblPaymentInfo.text = "override Confirmation";
                    this.view.SegmentPopup.segOverride.setData(segDataForOverrite);
                    this.view.SegmentPopup.isVisible = true;
                    kony.print("overWriteData::::" + JSON.stringify(overWriteData));
                    kony.print("this.currentOverride::::" + JSON.stringify(this.currentOverride));
                    this.view.forceLayout();
                }
            }
            if (res.error.errorDetails !== []) { // !== "" || res.error.errorDetails !== null || res.error.errorDetails !== undefined || res.error.errorDetails !== []) {
                kony.print("Inside ErrorDetails=====>");
                this.view.ProgressIndicator.isVisible = false;
                if (res.error.errorDetails[0] === "" || res.error.errorDetails[0] === [] || res.error.errorDetails[0] === undefined || res.error.errorDetails[0] === null) {
                    kony.print("Inside ErrorDetails Empty=====>");
                } else {
                    this.view.ErrorAllert.lblMessage.text = res.error.errorDetails[0].message; //error.errorDetails[0].message
                    this.view.ErrorAllert.isVisible = true;
                    kony.print("Service Failure:::" + JSON.stringify(res.error.errorDetails));
                    //this.view.flxHeaderMenu.isVisible = false;
                    //this.view.flxMessageInfo.isVisible = false;
                    this.view.forceLayout();
                }
            }
        } catch (err) {
            kony.print("Error in CreateContractLog failureCBPostCreateContractLog:::" + err);
        }
    },
    loadingDataToRequestLog: function() {
        try {
            if (Application.validation.isNullUndefinedObj(this.contractLogData) !== []) {
                var status = this.contractLogData[0].contactStatus; //this.passingDataToListBox(this.contractLogData[0].contactStatus);
                status = status.toUpperCase();
                this.view.ContractRequestDescription.setStatusSelectedKey(status);
                var desc = this.contractLogData[0].description;
                var additionalNote = this.contractLogData[0].notes;
                //var contractType = this.contractLogData[0].contactType;
                //var contractChannel = this.contractLogData[0].channel;
                var contractType = this.contractLogData[0].fullDescription;
                var contractChannel = this.contractLogData[0].channelDescription;
                var contractDate = this.toShortFormat(this.contractLogData[0].date);
                //this.contractLogData[0].contactStatus;//this.passingDataToListBox(this.contractLogData[0].contactStatus);
                var note = Application.validation.isNullUndefinedObj(additionalNote);
                if (note.length > 0) {
                    additionalNote = additionalNote[0].note;
                } else {
                    additionalNote = "";
                }
                this.view.ContractRequestDescription.descriptionHeaderResponse = desc.substring(0, 30);
                this.view.ContractRequestDescription.populateDataToResponseContainer(desc, additionalNote, contractType, contractChannel, contractDate, status);
            }
        } catch (err) {
            kony.print("Error in CreateContractLog loadingDataToRequestLog:::" + err);
        }
    },
    passingDataToListBox: function(data) {
        var listboxData = [{
            "key": "NEW",
            "value": "New"
        }, {
            "key": "PENDING",
            "value": "Pending"
        }, {
            "key": "CANCELLED",
            "value": "Cancelled"
        }];
        for (var a in listboxData) {
            if (data === listboxData[a].key) {
                kony.print("listboxData[a].value====>" + listboxData[a].value);
                return listboxData[a].value;
            }
        }
    },
    passingDataToService: function(data) {
        var listboxData = [{
            "key": "NEW",
            "value": "New"
        }, {
            "key": "PENDING",
            "value": "Pending"
        }, {
            "key": "CANCELLED",
            "value": "Cancelled"
        }];
        for (var a in listboxData) {
            if (data === listboxData[a].value) {
                kony.print("listboxData[a].value====>" + listboxData[a].key);
                return listboxData[a].key;
            }
        }
    },
    onClickBtnHelp: function() {
        this.view.help.isVisible = true;
        this.view.help.flxServices.isVisible = true;
        this.view.help.flxBottom.isVisible = false;
        this.view.help.flxServiceType.isVisible = false;
        this.view.help.flxSeg.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.help.flxBack.isVisible = false;
        this.view.help.flxPlayVideo.isVisible = false;
        this.view.help.flxLoan.skin = "sknFlxTypeUnselect";
    },
    onClickExplorer: function() {
        this.view.helpCenter.isVisible = true;
        this.view.help.isVisible = false;
        this.view.getStarted.isVisible = false;
    },
    maskAccountNumber: function(accNum) {
        var maskedAccNum;
        if (accNum.length === 12) {
            maskedAccNum = accNum.substring(0, 4) + "****" + accNum.substring(8);
        } else { // return original
            maskedAccNum = accNum;
        }
        return maskedAccNum;
    },
    formatDateToSlashFormat: function(date) { // return DD/MM/YYYY
        var d = new Date(date);
        month = '' + (d.getMonth() + 1);
        day = '' + d.getDate();
        year = d.getFullYear();
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
        return day + "/" + month + "/" + year;
    },
    formatDateToDDMMMYYYY: function(date) { // return DD MMM YYYY
        var d = new Date(date);
        month = '' + d.getMonth();
        day = '' + d.getDate();
        year = d.getFullYear();
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        if (day.length < 2) day = '0' + day;
        return day + " " + monthNames[month] + " " + year;
    },
    //..................................merged code....................................
    onTouchImgDropDown: function() {
        try {
            // this.view.flxSearchAccount.setVisibility(false);
            var serviceName = "FundsDeposit";
            var operationName = "getAccounts";
            var headers = {
                "companyId": this.companyId,
                "customerId": this._navObj.customerId
            };
            var inputParams = {};
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgetAccounts, this.errorCBgetAccounts);
        } catch (err) {
            kony.print("Error in fund deposite controller::" + err);
        }
    },
    // *************************************************************************************
    // Modified Date : 25/03/2021
    // MOdified by : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose :Success CB for accounts list
    // *************************************************************************************
    successCBgetAccounts: function(res) {
        try {
            var Data = [];
            var widgetData = {
                // "lblAccountId" : "customerId",
                "lblAccountId": "lblAccountId",
                "lbltype": "lbltype",
                // "lblCurrency":"currencyId",
                "lblWorkingBal": "lblWorkingBal"
            };
            if (res.body.length > 0) {
                for (i = 0; i < res.body.length; i++) {
                    res.body[i].workingBalanceString = res.body[i].workingBalance + "";
                    var usableBalance = res.body[i].workingBalance;
                    kony.print("usableBalance:::" + JSON.stringify(usableBalance));
                    // var Balance = usableBalance.toString();
                    var accountId = res.body[i].accountId;
                    // var customerId = res.body[k].customerId;
                    var Product = res.body[i].productName;
                    // var CCY = res.body[k].currencyId;
                    // kony.print("Balance:::" + JSON.stringify(Balance));
                    if ((usableBalance !== null && usableBalance !== undefined && usableBalance !== "") && (accountId !== null && accountId !== undefined && accountId !== "") && (Product !== null && Product !== undefined && Product !== "")) {
                        Data.push({
                            "lblAccountId": accountId,
                            // "lblCustomerID":customerId,
                            "lbltype": Product,
                            // "lblCCY":CCY,
                            "lblWorkingBal": usableBalance.toString()
                        });
                    } else {}
                }
            }
            mode = "FD";
            gblCutacctWidgetDate = widgetData;
            gblCutacctArray = Data;
        } catch (err) {
            kony.print("Error in Fund Deposit Controller segmentAccountList:::" + err);
        }
    },
    // *************************************************************************************
    // Modified Date : 25/03/2021
    // Modified by : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose :Failure CB of getAccounts
    // *************************************************************************************
    errorCBgetAccounts: function(res) {
        try {
            kony.print(("error response:::" + JSON.stringify(res)));
            this.view.ProgressIndicator.isVisible = false;
        } catch (err) {
            kony.print("Error in fundDepositeController ::" + err);
        }
    },
    advancedSearchAccounts: function() {
        try {
            var serviceName = "FundsDeposit";
            var operationName = "getAccountSearch";
            var headers = {
                "companyId": this.companyId
            };
            var inputParams = {};
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCallBackAllaccount, this.failureCallBackAllaccount);
            this.view.ProgressIndicator.isVisible = true;
        } catch (err) {
            kony.print("Error in FundDeposit controller advancedSearchAccounts " + err);
        }
    },
    successCallBackAllaccount: function(response) {
        try {
            kony.print(response);
            var Data = [];
            var allAccountResponse = response.body.length;
            var widgetData = {
                "lblAccountId": "lblAccountId",
                "lblCustomerId": "lblCustomerId",
                "lblProductId": "lblProductId",
                "lblCurrency": "lblCurrency",
                "lblWorkingBal": "lblWorkingBal"
            };
            if (allAccountResponse > 0) {
                for (i = 0; i < allAccountResponse; i++) {
                    // response.body[i].workingBalanceString = res.body[i].workingBalance+"";
                    var usableBalance = response.body[i].workingBalance;
                    kony.print("usableBalance:::" + JSON.stringify(usableBalance));
                    // var Balance = usableBalance.toString();
                    var accountId = response.body[i].accountId;
                    var customerId = response.body[i].customerId;
                    var Product = response.body[i].productName;
                    var CCY = response.body[i].currencyId;
                    // kony.print("Balance:::" + JSON.stringify(Balance));
                    if ((usableBalance !== null && usableBalance !== undefined && usableBalance !== "") && (accountId !== null && accountId !== undefined && accountId !== "") && (Product !== null && Product !== undefined && Product !== "") && (customerId !== null && customerId !== undefined && customerId !== "") && (CCY !== null && CCY !== undefined && CCY !== "")) {
                        Data.push({
                            "lblAccountId": accountId,
                            "lblCustomerId": customerId,
                            "lblProductId": Product,
                            "lblCurrency": CCY,
                            "lblWorkingBal": usableBalance.toString()
                        });
                    } else {}
                }
            }
            gblAllaccountWidgetDate = widgetData;
            gblAllaccountarray = Data;
            this.view.ProgressIndicator.isVisible = false;
        } catch (err) {
            kony.print("Error in frmFundDeposit Controller successCallBackAllaccount ::::" + err);
        }
    },
    failureCallBackAllaccount: function(res) {
        // this.view.ProgressIndicator.isVisible=false;
        if (res.error !== []) { // !== "" || res.error.errorDetails !== null || res.error.errorDetails !== undefined || res.error.errorDetails !== []) {
            kony.print("Inside ErrorDetails=====>");
            this.view.ProgressIndicator.isVisible = false;
            if (res.error === "" || res.error === [] || res.error === undefined || res.error === null) {
                kony.print("Inside ErrorDetails Empty=====>");
            } else {
                this.view.ErrorAllert.lblMessage.text = res.error.message; //error.errorDetails[0].message
                this.view.ErrorAllert.isVisible = true;
                this.view.ErrorAllert.setFocus(true);
                kony.print("Service Failure:::" + JSON.stringify(res.error.message));
                this.view.forceLayout();
            }
        }
        kony.print("Error" + JSON.stringify(res));
    },
    // *************************************************************************************
    // Modified Date : 26/03/2021
    // MOdified by : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose :getting all the withdrawal accounts list
    // *************************************************************************************
    getWithdrawalAccounts: function() {
        try {
            // this.view.flxSearchAccount.setVisibility(false);
            var serviceName = "FundsDeposit";
            var operationName = "getAccounts";
            var headers = {
                "companyId": this.companyId,
                "customerId": this._navObj.customerId
            };
            var inputParams = {};
            this.view.ProgressIndicator.isVisible = true;
            MFHelper.Utils.commonMFServiceCall(serviceName, operationName, headers, inputParams, this.successCBgetWithdrawalAccounts, this.errorCBgetWithdrawalAccounts);
        } catch (err) {
            kony.print("Error in CustomerAccountDetails Controller controller::" + err);
        }
    },
    // *************************************************************************************
    // Modified Date : 26/03/2021
    // MOdified by : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose :Success CB for withdrawal accounts list
    // *************************************************************************************
    successCBgetWithdrawalAccounts: function(res) {
        try {
            var Data = [];
            var widgetData = {
                // "lblAccountId" : "customerId",
                "lblAccountId": "lblAccountId",
                "lbltype": "lbltype",
                // "lblCurrency":"currencyId",
                "lblWorkingBal": "lblWorkingBal"
            };
            if (res.body.length > 0) {
                for (i = 0; i < res.body.length; i++) {
                    res.body[i].workingBalanceString = res.body[i].workingBalance + "";
                    var usableBalance = res.body[i].workingBalance;
                    kony.print("usableBalance:::" + JSON.stringify(usableBalance));
                    // var Balance = usableBalance.toString();
                    var accountId = res.body[i].accountId;
                    // var customerId = res.body[k].customerId;
                    var Product = res.body[i].productName;
                    // var CCY = res.body[k].currencyId;
                    // kony.print("Balance:::" + JSON.stringify(Balance));
                    if ((usableBalance !== null && usableBalance !== undefined && usableBalance !== "") && (accountId !== null && accountId !== undefined && accountId !== "") && (Product !== null && Product !== undefined && Product !== "")) {
                        Data.push({
                            "lblAccountId": accountId,
                            // "lblCustomerID":customerId,
                            "lbltype": Product,
                            // "lblCCY":CCY,
                            "lblWorkingBal": usableBalance.toString()
                        });
                    } else {}
                }
            }
            mode = "withdrawal";
            gblCutacctWithdrawalWidgetDate = widgetData;
            gblCutacctWithdrawalArray = Data;
        } catch (err) {
            kony.print("Error in CustomerAccountDetails Controller successCBgetWithdrawalAccounts:::" + err);
        }
    },
    // *************************************************************************************
    // Modified Date : 26/03/2021
    // Modified by : Nandhini Srinivasulu
    // Organization : ITSS
    // Function purpose :Failure CB of withdrawal account list
    // *************************************************************************************
    errorCBgetWithdrawalAccounts: function(res) {
        try {
            kony.print(("error response:::" + JSON.stringify(res)));
            this.view.ProgressIndicator.isVisible = false;
        } catch (err) {
            kony.print("Error in CustomerAccountDetails Controllerr errorCBgetWithdrawalAccounts::" + err);
        }
    },
});
define("frmCustomerAccountsDetailsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_UWI_be20201bc2ec4a94b104c0c955d5f1ea: function AS_UWI_be20201bc2ec4a94b104c0c955d5f1ea(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    AS_UWI_b5fdff74b36a4ea69d10cb4a2eee5257: function AS_UWI_b5fdff74b36a4ea69d10cb4a2eee5257(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});
define("frmCustomerAccountsDetailsController", ["userfrmCustomerAccountsDetailsController", "frmCustomerAccountsDetailsControllerActions"], function() {
    var controller = require("userfrmCustomerAccountsDetailsController");
    var controllerActions = ["frmCustomerAccountsDetailsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
