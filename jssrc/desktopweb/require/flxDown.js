define("flxDown", function() {
    return function(controller) {
        var flxDown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d0a299e30fbb421ca1666a91fde1e7fc,
            "skin": "CopyslFbox0c29cae56cd8340",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxDown.setDefaultUnit(kony.flex.DP);
        var FlexGroup0d7f2ca3295ba45 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "FlexGroup0d7f2ca3295ba45",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        FlexGroup0d7f2ca3295ba45.setDefaultUnit(kony.flex.DP);
        var lblShelves = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblShelves",
            "isVisible": true,
            "left": "1%",
            "skin": "CopydefLabel0ce1f91c7a63b4e",
            "text": "Kony ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgLocation = new kony.ui.Image2({
            "centerY": "50%",
            "height": "25dp",
            "id": "imgLocation",
            "isVisible": false,
            "right": "1dp",
            "skin": "slImage",
            "src": "blue_downarrow.png",
            "width": "25dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTitle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTitle",
            "isVisible": true,
            "left": "20%",
            "skin": "CopyCopydefLabel0bcabaec60c7842",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        var lblDown = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDown",
            "isVisible": true,
            "right": "10dp",
            "skin": "CopydefLabel0fd64b1ed49de46",
            "text": "J",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        FlexGroup0d7f2ca3295ba45.add(lblShelves, imgLocation, lblTitle, lblDown);
        flxDown.add(FlexGroup0d7f2ca3295ba45);
        return flxDown;
    }
})