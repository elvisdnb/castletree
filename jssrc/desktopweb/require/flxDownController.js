define("userflxDownController", {
    //Type your controller code here 
});
define("flxDownControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_d0a299e30fbb421ca1666a91fde1e7fc: function AS_FlexContainer_d0a299e30fbb421ca1666a91fde1e7fc(eventobject, context) {
        var self = this;
        this.executeOnParent("onClick", eventobject, context);
    }
});
define("flxDownController", ["userflxDownController", "flxDownControllerActions"], function() {
    var controller = require("userflxDownController");
    var controllerActions = ["flxDownControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
