define("userCopyflxDownController", {
    onSectionClick: function(context) {
        this.executeOnParent("sectionClicked", context);
    },
    onBtnClick: function(context) {
        this.executeOnParent('btnClick', context._kwebfw_.prop.text);
    }
});
define("CopyflxDownControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_j53dfc8b7d164d138b81f33a7be07466: function AS_Button_j53dfc8b7d164d138b81f33a7be07466(eventobject, context) {
        var self = this;
        return self.onSectionClick.call(this, context);
    },
    AS_Button_f42c7b756fbe4e39b96d71ba572d6458: function AS_Button_f42c7b756fbe4e39b96d71ba572d6458(eventobject, context) {
        var self = this;
        return self.onBtnClick.call(this, eventobject);
    }
});
define("CopyflxDownController", ["userCopyflxDownController", "CopyflxDownControllerActions"], function() {
    var controller = require("userCopyflxDownController");
    var controllerActions = ["CopyflxDownControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
