define("flxHdr", function() {
    return function(controller) {
        var flxHdr = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxHdr",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {
            "hoverSkin": "CopyslFbox0d6f2870369004e"
        });
        flxHdr.setDefaultUnit(kony.flex.DP);
        var flxRowAct = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxRowAct",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxRowAct.setDefaultUnit(kony.flex.DP);
        var flxName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60dp",
            "id": "flxName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "1%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "24%"
        }, {}, {});
        flxName.setDefaultUnit(kony.flex.DP);
        var lblGroup = new kony.ui.Label({
            "id": "lblGroup",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopysknLblRowHeading0e214d37a97604c",
            "text": "Industrial group Ltd",
            "top": "10px",
            "width": "140dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCid = new kony.ui.Label({
            "bottom": "10px",
            "id": "lblCid",
            "isVisible": true,
            "left": "0px",
            "skin": "skn15RegGr",
            "text": "101146",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxName.add(lblGroup, lblCid);
        var lblActivity = new kony.ui.Label({
            "centerY": "50%",
            "height": "60px",
            "id": "lblActivity",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknGreyBold",
            "text": "Expires Power of attorney fdgfdgfdgfdgfdgdfgfdgdfgfdgfdgfdgdgfdgfdgfd",
            "top": "0dp",
            "width": "27%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 2, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDate",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "skn15RegGr",
            "text": "22 Nov 2020",
            "top": "0dp",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "24%"
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var flxStatusinner = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "32dp",
            "id": "flxStatusinner",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0cff92267df714b",
            "top": "0",
            "width": "80%"
        }, {}, {});
        flxStatusinner.setDefaultUnit(kony.flex.DP);
        flxStatusinner.add();
        var lblInformed = new kony.ui.Label({
            "centerY": "50%",
            "height": "30dp",
            "id": "lblInformed",
            "isVisible": true,
            "left": "2%",
            "skin": "sknStatusNew",
            "text": "Not Informed",
            "top": "10dp",
            "width": "120dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [5, 1, 1, 1],
            "paddingInPixel": false
        }, {});
        flxStatus.add(flxStatusinner, lblInformed);
        flxRowAct.add(flxName, lblActivity, lblDate, flxStatus);
        flxHdr.add(flxRowAct);
        return flxHdr;
    }
})