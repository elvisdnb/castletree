define("flxRowPayinPayout", function() {
    return function(controller) {
        var flxRowPayinPayout = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxRowPayinPayout",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxRowPayinPayout.setDefaultUnit(kony.flex.DP);
        var lblAccountId = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblAccountId",
            "isVisible": true,
            "left": "1%",
            "skin": "CopydefLabel0a337ff4527814a",
            "text": "-",
            "top": "0",
            "width": "24%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCurrency = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrency",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0ac82775fe62c49",
            "text": "Label",
            "top": "0",
            "width": "10%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblProductId = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblProductId",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0ac82775fe62c49",
            "text": "-",
            "top": "0",
            "width": "30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustomerId = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblCustomerId",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0ac82775fe62c49",
            "text": "-",
            "top": "0",
            "width": "15%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblWorkingBal = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblWorkingBal",
            "isVisible": true,
            "left": "0",
            "skin": "CopydefLabel0ac82775fe62c49",
            "text": "Final",
            "top": "0",
            "width": "20%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRowPayinPayout.add(lblAccountId, lblCurrency, lblProductId, lblCustomerId, lblWorkingBal);
        return flxRowPayinPayout;
    }
})