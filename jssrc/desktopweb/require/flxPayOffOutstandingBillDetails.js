define("flxPayOffOutstandingBillDetails", function() {
    return function(controller) {
        var flxPayOffOutstandingBillDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPayOffOutstandingBillDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "breakpoints": [640, 1024, 1366]
        }, {}, {});
        flxPayOffOutstandingBillDetails.setDefaultUnit(kony.flex.DP);
        var lblDueDateInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblDueDateInfo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl14PX434343BG",
            "top": "2dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTypeInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblTypeInfo",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlbl14PX434343BG",
            "top": "2dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblBilledInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblBilledInfo",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlbl14PX434343BG",
            "top": "2dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOutstandingInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblOutstandingInfo",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlbl14PX434343BG",
            "top": "2dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatusInfo = new kony.ui.Label({
            "height": "36dp",
            "id": "lblStatusInfo",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblFFF1F1BG12pxRedBorder",
            "top": "2dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPayOffOutstandingBillDetails.add(lblDueDateInfo, lblTypeInfo, lblBilledInfo, lblOutstandingInfo, lblStatusInfo);
        return flxPayOffOutstandingBillDetails;
    }
})