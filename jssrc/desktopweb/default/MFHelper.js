//Type your code here
function intializeSDKwithParams() {
    return;
}

function intializeSDKwithParams2() {
    /*
    var appkey = "";
    var appsecret = "";
    var serviceUrl = "http://";
    var initOptions = {};
    var initOptions = {"MFAppVersion": "<your-Fabric-app-version>"}; //This is an optional argument to be used if you want to switch to Fabric-app version other than default app version.

    */
    // Get an instance of SDK
    var appkey = "";
    var appsecret = "";
    var serviceUrl = "";
    var initOptions = {};
    // var initOptions = {"MFAppVersion": "<your-Fabric-app-version>"}; //This is an optional argument to be used if you want to switch to Fabric-app version other than default app version.
    var client = new kony.sdk();
    // initialize SDK
    client.init(appkey, appsecret, serviceUrl, function(response) {
        kony.print("Init success: " + JSON.stringify(response));
    }, function(error) {
        kony.print("Init failed: " + JSON.stringify(error));
    }, initOptions);
}
if (typeof(MFHelper) === 'undefined' || typeof(MFHelper) === string || typeof(MFHelper) === number || typeof(MFHelper) === boolean) {
    MFHelper = {};
}
if (typeof(Application) === 'undefined' || typeof(Application) === string || typeof(Application) === number || typeof(Application) === boolean) {
    Application = {};
}
MFHelper.Utils = {
    commonMFServiceCall: function(serviceName, operationName, headers, inputParams, successCallBackfunc, failureCallBackfunc) {
        kony.print("commonMFServiceCall::--->");
        kony.print("serviceName::: " + serviceName);
        kony.print("operationName::: " + operationName);
        kony.print("inputParams::: " + JSON.stringify(inputParams));
        var konyRef = kony.sdk.getCurrentInstance();
        kony.print("konyRef. " + konyRef);
        //Success Callback for integration services
        var successCallBack = function successCallBack(resultset) {
            //kony.print("commonMFServiceCall_successCallBack:::==>");
            //kony.print("commonMFServiceCall_successCallBack"+JSON.stringify(resultset));
            successCallBackfunc(resultset);
        };
        //Failure Callback for integration services
        var failureCallBack = function failureCallBack(resultset) {
            //kony.print("commonMFServiceCall_failureCallBack:::==>");
            //kony.print("commonMFServiceCall_failureCallBack"+JSON.stringify(resultset));
            if (resultset !== null && resultset !== undefined && resultset !== "" && resultset !== "null") {
                if (resultset.hasOwnProperty("opstatus")) {
                    var opstatus = resultset.opstatus;
                    if (undefined !== opstatus && null !== opstatus && ("1011" == opstatus || 1011 == opstatus)) {
                        Application.alert.showOkAlert("The Network is unavailable at this time.", null);
                    } else if (opstatus === 17005 || opstatus === "17005" || resultset.errmsg === "Not authorized") {
                        Application.alert.showOkAlert("Sorry Session has been expired.Please Login again.", null);
                    } else {
                        failureCallBackfunc(resultset);
                    }
                } else {
                    failureCallBackfunc(resultset);
                }
            } else {
                Application.alert.showOkAlert("Sorry something went wrong. Please try again later..", null);
            }
            Application.loader.dismissLoader();
        };
        //Success Callback for refreshing claimstoken 
        KNYMobileFabric = kony.sdk.getCurrentInstance();
        var integrationObj = KNYMobileFabric.getIntegrationService(serviceName);
        integrationObj.invokeOperation(operationName, headers, inputParams, successCallBack, failureCallBack);
    },
};
Application.alert = {
    // show Alert for information
    showOkAlert: function(msg, handler) {
        var alertConf = {
            message: msg,
            alertType: constants.ALERT_TYPE_INFO,
            alertTitle: "ProjectCastle",
            yeslabel: "OK",
            nolabel: null,
            alertIcon: null,
            alertHandler: handler
        };
        var pspConf = {};
        kony.ui.Alert(alertConf, pspConf);
        kony.application.dismissLoadingScreen();
    },
    // show Alert for confirmation
    showOkCancelAlert: function(msg, handler) {
        var alertConf = {
            message: msg,
            alertType: constants.ALERT_TYPE_CONFIRMATION,
            alertTitle: "ProjectCastle",
            yeslabel: "OK",
            nolabel: "Cancel",
            alertIcon: null,
            alertHandler: handler
        };
        var pspConf = {};
        var alertData = kony.ui.Alert(alertConf, pspConf);
        return alertData;
    },
    ConfirmDelete: function(msg) {
        var x = confirm(msg);
        if (x) return true;
        else return false;
    }
};
Application.loader = {
    // show Application loader
    showLoader: function(loaderText) {
        kony.print("Loading...");
        //loaderText ="\n\n\n\n\n "+loaderText;
        kony.application.showLoadingScreen("blockUI3", loaderText, constants.LOADING_SCREEN_POSITION_FULL_SCREEN,
            //constants.LOADING_SCREEN_POSITION_ONLY_CENTER,
            true, true, {});
    },
    // dismiss Application loader
    dismissLoader: function() {
        kony.print("Loader dismissed::: ");
        kony.application.dismissLoadingScreen();
    }
};
Application.validation = {
    isNullUndefinedObj: function(value) {
        //  Application.Utils.Dlog("isNullUndefinedObj ::kony:: ==>");
        if ((null !== value && undefined !== value && "" !== value && value !== "null")) {
            return value;
        } else {
            return "";
        }
    },
    isNullUndefinedNumber: function(value) {
        //  Application.Utils.Dlog("isNullUndefinedObj ::kony:: ==>");
        if ((null !== value && undefined !== value && "" !== value && value !== "null")) {
            return value;
        } else {
            return 0;
        }
    },
    isNullUndefinedObjSendNULL: function(value) {
        //  Application.Utils.Dlog("isNullUndefinedObj ::kony:: ==>");
        if ((null !== value && undefined !== value && "" !== value && value !== "null")) {
            return value;
        } else {
            return null;
        }
    },
};
Application.numberFormater = {
    convertForDispaly: function(number, currency) {
        var numberFormat = Application.validation.isNullUndefinedObj(number);
        var currencyFormat = Application.validation.isNullUndefinedObj(currency);
        var getCurrencyDetails = this.getOtherDetailBasedonCurrencyCode(currency);
        if (numberFormat !== "" && currencyFormat !== "" && getCurrencyDetails.currencycode !== "") {
            var formatted = "" + new Intl.NumberFormat(getCurrencyDetails.locale, {
                style: 'currency',
                currency: currency
            }).format(number);
            formatted = formatted.replace(getCurrencyDetails.currencySymbol, "");
            formatted = formatted.trim();
            return formatted;
        } else {
            return "";
        }
    },
    getOtherDetailBasedonCurrencyCode: function(currencyCode) {
        var currencycode = Application.validation.isNullUndefinedObj(currencyCode);
        if (currencycode !== "" && gblCurrencyArray.length > 0) {
            for (var a in gblCurrencyArray) {
                if (currencycode === gblCurrencyArray[a].currencycode) {
                    return gblCurrencyArray[a];
                }
            }
        }
    },
    getCurrencySymbolBasedonCurrencyCode: function(currencyCode) {
        var currencycode = Application.validation.isNullUndefinedObj(currencyCode);
        if (currencycode !== "" && gblCurrencyArray.length > 0) {
            for (var a in gblCurrencyArray) {
                if (currencycode === gblCurrencyArray[a].currencycode) {
                    return gblCurrencyArray[a].currencySymbol;
                }
            }
        }
    }
};