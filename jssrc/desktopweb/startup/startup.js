kony.appinit.setApplicationMetaConfiguration("appid", "ProjectCastle");
kony.appinit.setApplicationMetaConfiguration("apptitle", "ProjectCastle");
kony.appinit.setApplicationMetaConfiguration("build", "debug");
kony.appinit.setApplicationMetaConfiguration("locales", []);
kony.appinit.setApplicationMetaConfiguration("i18nArray", []);
//startup.js
var appConfig = {
    appId: "ProjectCastle",
    appName: "PCLoans",
    appVersion: "3.1.2",
    isturlbase: "http://ap-n1ffx1aerm97.temenos.cloud:8080/services",
    isDebug: true,
    isMFApp: true,
    appKey: "57ae1c1168dc8126e5137c906e0b42aa",
    appSecret: "acb355090c685f4f4f33dbd97db7d6a",
    serviceUrl: "http://ap-n1ffx1aerm97.temenos.cloud:8080/authService/100000002/appconfig",
    svcDoc: {
        "selflink": "http://ap-n1ffx1aerm97.temenos.cloud:8080/authService/100000002/appconfig",
        "app_version": "1.0",
        "integsvc": {
            "AddRemoveParty": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/AddRemoveParty",
            "_internal_logout": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/IST",
            "DepositCreation": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/DepositCreation",
            "BlockUnblock": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/BlockUnblock",
            "WithdrawalOrchestration": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/WithdrawalOrchestration",
            "LendingNew": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/LendingNew",
            "PostingRestriction": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/PostingRestriction",
            "FundsDeposit": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/FundsDeposit",
            "FundsWithdrawal": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/FundsWithdrawal",
            "ChangeInterestPayouts": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/ChangeInterestPayouts",
            "SCVServices": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/SCVServices",
            "DashbordLiability": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/DashbordLiability"
        },
        "service_doc_etag": "00000178E52EE0F0",
        "appId": "f27f859c-0d10-4109-83d8-12569325af29",
        "identity_features": {
            "reporting_params_header_allowed": true
        },
        "name": "ProjectCastle",
        "reportingsvc": {
            "session": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/IST",
            "custom": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/CMS"
        },
        "baseId": "d217edad-1fa7-42c7-a5f0-2a696118a235",
        "app_default_version": "1.0",
        "services_meta": {
            "DepositCreation": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/DepositCreation"
            },
            "WithdrawalOrchestration": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/WithdrawalOrchestration"
            },
            "LendingNew": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/LendingNew"
            },
            "PostingRestriction": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/PostingRestriction"
            },
            "FundsDepositObject": {
                "offline": false,
                "metadata_url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/metadata/v1/FundsDepositObject",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/data/v1/FundsDepositObject"
            },
            "FundsWithdrawalObject": {
                "offline": false,
                "metadata_url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/metadata/v1/FundsWithdrawalObject",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/data/v1/FundsWithdrawalObject"
            },
            "SCVServices": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/SCVServices"
            },
            "AddRemoveParty": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/AddRemoveParty"
            },
            "BlockUnblock": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/BlockUnblock"
            },
            "BlockUnblockObject": {
                "offline": false,
                "metadata_url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/metadata/v1/BlockUnblockObject",
                "type": "objectsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/data/v1/BlockUnblockObject"
            },
            "FundsDeposit": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/FundsDeposit"
            },
            "FundsWithdrawal": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/FundsWithdrawal"
            },
            "ChangeInterestPayouts": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/ChangeInterestPayouts"
            },
            "DashbordLiability": {
                "type": "integsvc",
                "version": "1.0",
                "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/services/DashbordLiability"
            }
        },
        "Webapp": {
            "url": "http://ap-n1ffx1aerm97.temenos.cloud:8080/apps/ProjectCastle"
        }
    },
    runtimeAppVersion: "1.0",
    eventTypes: ["FormEntry", "Error", "Crash"],
};
sessionID = "";

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        isMVC: true,
        responsive: true,
        buttonAsLabel: true,
        APILevel: 9100,
    })
};

function themeCallBack() {
    initializeGlobalVariables();
    requirejs.config({
        baseUrl: kony.appinit.getStaticContentPath() + 'desktopweb/appjs'
    });
    require(['kvmodules'], function() {
        applicationController = require("applicationController");
        kony.application.setApplicationInitializationEvents({
            init: applicationController.appInit,
            postappinit: applicationController.postAppInitCallBack,
            showstartupform: function() {
                new kony.mvc.Navigation("frmLogin").navigate();
            }
        });
    });
};

function loadResources() {
    kony.theme.packagedthemes(["default", "purpleTheme", "temenosPrimary"]);
    globalhttpheaders = {};
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    spaAPM && spaAPM.startTracking();
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}

function initializeApp() {
    kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
    //This is the entry point for the application.When Locale comes,Local API call will be the entry point.
    loadResources();
};
									function getSPARequireModulesList(){ return ['kvmodules']; }
								