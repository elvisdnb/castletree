define(function() {

  return {
    constructor : function() {
      // will be called when form having this component is initialized
      this.bindEvents();
      this.setArrayValues();
    },

    bindEvents : function(){
      this.view.txtSearchBox.onKeyUp = this.doCRSearchWithinSeg;
      this.view.txtSearchBox.onDone = this.doCRSearchWithinSeg;
      // this.view.imgClose.onTouchEnd=  this.payInOutPopclose;  
    },
    setArrayValues : function(){        
      this.view.segPayinPayout.widgetDataMap = gblAllaccountWidgetDate; 
      this.view.segPayinPayout.setData(gblAllaccountarray);
    },

    doCRSearchWithinSeg : function(){
      kony.print('edit');
      var stringForSearch = this.view.txtSearchBox.text.toLowerCase();
      if(stringForSearch.length === 0) {
        //Reset to show the full claims search results global variable
        // disable the clear search x mark, if any
        this.view.segPayinPayout.removeAll();
        this.view.segPayinPayout.setData(gblAllaccountarray);
      }
      else {
        //empty search string now
        // visible on for the clear search x mark
      }

      if(stringForSearch.length>=3)
      { 
        //this.view.popupPayinPayout.segPayinPayout
        var segData=gblAllaccountarray; // make a copy not response
        var finalData =[];
        if(segData && segData === []){
          return;
        }
        for(var i=0; i<segData.length; i++){
          var aRowData =  segData[i];
          var aRowDataStrigified = JSON.stringify(aRowData);
          if(aRowDataStrigified.toLowerCase().indexOf(stringForSearch) >= 0 ){
            finalData.push(aRowData);
          } else {
            //no need to push it - as does not meet criteria
          }
        }
        this.view.segPayinPayout.removeAll();
        this.view.segPayinPayout.setData(finalData);
      }
    }

  };
});