define(function() {

  return {
   
    
    initGettersSetters : function() {
      this.setCustomerValuesFromCache();
    },
    
    restCustomerValues : function(){  
      var newCustomerObj = {};
      gblCustomerObj = {};
      this.setCustomerValues();
    },

    setCustomerValues : function(){
      this.view.lblCustomerName.text = "Eric VanDe Laar";
      this.view.lblCustomerNumberResponse.text = "101146";
      this.view.lblEmailResponse.text = "ericvdl@gmail.com";
      this.view.lblPhoneResponse.text = "+3197010281305";
      this.view.lblProfileInfo.text =  "Profile";
      this.view.lblProfileResponse.text = "Customer rating - A";
      this.view.lblRelationResponse.text = "Dominic Laar - Child";
      this.view.lblLocationResponse.text =  "Rotterdam, Netherland";
      this.view.lblDOBResponse.text = "04 Oct, 1975";
      this.view.lblAccountOfficerResponse.text = "Patrick Owen";
      this.view.lblMobileBankingResponse.text = "-";
      this.view.lblInternetBankingResponse.text = "-";
//       gblCustomerObj =  customerObj;
    },
    
    setCustomerValuesFromCache : function(){
     this.view.lblCustomerName.text = "Eric VanDe Laar";
      this.view.lblCustomerNumberResponse.text = "101146";
      this.view.lblEmailResponse.text = "ericvdl@gmail.com";
      this.view.lblPhoneResponse.text = "+3197010281305";
      this.view.lblProfileInfo.text =  "Profile";
      this.view.lblProfileResponse.text = "Customer rating - A";
      this.view.lblRelationResponse.text = "Dominic Laar - Child";
      this.view.lblLocationResponse.text =  "Rotterdam, Netherland";
      this.view.lblDOBResponse.text = "04 Oct, 1975";
      this.view.lblAccountOfficerResponse.text = "Patrick Owen";
      this.view.lblMobileBankingResponse.text = "-";
      this.view.lblInternetBankingResponse.text = "-";
    }

  };
});