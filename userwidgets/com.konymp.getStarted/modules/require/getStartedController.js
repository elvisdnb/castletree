define(function() {

  return {
    closeHelp:function(){
      kony.store.setItem("demoFlag", "1");
      this.view.isVisible = false;
    },

    btnStart:function(){
      
      if(this.view.lblHi.text === "Hi There"){
        this.view.lblHi.text = "Explore";
        this.view.lbl2.text = "Take an interactive tour of all things lending from origination to servicing";
        this.view.btnStart.text = "Next";
        this.view.img1.src = "start1.png";
        this.view.CircleDark2.skin = "sknIndicatorSelected";
        this.view.CircleDark1.skin = "sknIndicatorUnselect";
        this.view.CircleDark3.skin = "sknIndicatorUnselect";
        this.view.flxInidcator1.isVisible = true;
        this.view.flxInidcator1.bottom = "175dp";
      }else if(this.view.lblHi.text === "Explore"){
        this.view.lblHi.text = "Help Center";
        this.view.lbl2.text = "Lost your way? Your go to place for all information";
        this.view.btnStart.text = "Get Started";
        this.view.btnStart.isVisible = false;
        this.view.btnStart1.isVisible = true;
        this.view.btnStart1.text = "Get Started";
        this.view.forceLayout();
        this.view.img1.src = "start0.png";
        this.view.flxInidcator1.isVisible = true;
        this.view.flxInidcator1.bottom = "125dp";
        this.view.CircleDark3.skin = "sknIndicatorSelected";
        this.view.CircleDark1.skin = "sknIndicatorUnselect";
        this.view.CircleDark2.skin = "sknIndicatorUnselect";

      }else{
        return;
      }       

      //         if(this.view.btnStart.text === "Get Started"){
      //           this.view.isVisible = false;
      //         }else{
      //           this.view.isVisible = true;
      //         }
    }
  };
});