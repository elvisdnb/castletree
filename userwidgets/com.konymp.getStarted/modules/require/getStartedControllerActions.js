define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnStart **/
    AS_Button_ca5647f3ac24432caf1db8b764acd1e3: function AS_Button_ca5647f3ac24432caf1db8b764acd1e3(eventobject) {
        var self = this;
        return self.btnStart.call(this);
    },
    /** onClick defined for btnStart1 **/
    AS_Button_g1a747a124684ca2b9c14fa374143a38: function AS_Button_g1a747a124684ca2b9c14fa374143a38(eventobject) {
        var self = this;
        kony.store.setItem("demoFlag", "1");
        this.view.isVisible = false;
    },
    /** onClick defined for btnCloseGetStarted **/
    AS_Button_gb211ccc462d48ee942a2bf2659d5a28: function AS_Button_gb211ccc462d48ee942a2bf2659d5a28(eventobject) {
        var self = this;
        return self.closeHelp.call(this);
    },
    /** onClick defined for getStarted **/
    AS_FlexContainer_ee0e875e848244fd927cde18fce984a0: function AS_FlexContainer_ee0e875e848244fd927cde18fce984a0(eventobject) {
        var self = this;
        kony.store.setItem("demoFlag", "1");
        this.view.isVisible = false;
    }
});