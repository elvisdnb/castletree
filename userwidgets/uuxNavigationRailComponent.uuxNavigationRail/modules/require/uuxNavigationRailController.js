define(function() {

  return {
    constructor: function(baseConfig, layoutConfig, pspConfig) {

      this.bindEvents();
    },
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {

    },

    bindEvents : function(){
      this.view.flxHome.onTouchEnd = this.navigateToDashboard;
      this.view.flxSearchCust.onTouchEnd = this.navigateToSearchCustomer;
      this.view.FlxLogout.onTouchEnd = this.navigateLogin;
    },

    navigateToDashboard : function(){
      var navToDashboard = new kony.mvc.Navigation("frmDashboardLending");
      var navObjet = {};
      navObjet.isPopupShow = "false"; // TODO added newly to dismiss success popup
      navToDashboard.navigate(navObjet); 
    },

    navigateToSearchCustomer : function(){
      var navToSearchCustomer = new kony.mvc.Navigation("frmSearchCustomer");
      var navObjet = {};
      navToSearchCustomer.navigate(navObjet); 
    },
    
    navigateLogin : function(){
      var navToSearchCustomer = new kony.mvc.Navigation("frmLogin");
      var navObjet = {};
      navToSearchCustomer.navigate(navObjet); 
  },

  };
});