define(function() {

  return {
    constructor:function(baseConfig, layoutConfig, pspConfig){
      this.view.txtFloatText.onEndEditing = this.compTxtOnEndEditing;
      this.view.txtFloatText.onBeginEditing = this.compTxtOnBeginEditing;
      //this.view.txtFloatText.onTextChange = this.onTxtChanged;
    },
    
    compTxtOnEndEditing : function(){
      this.reverseAnimateComponent();
    },
    
    //Call this only for Numeric Input fields
    onTxtChanged:function(){
      //strip the last character
      var str = this.view.txtFloatText.text;
      var withoutLastChar = str.substring(0, str.length - 1);
       
         if(isNaN(str) && withoutLastChar !== "" && !isNaN(withoutLastChar) ){
           //is a number
           if(str.endsWith("m") || str.endsWith("M")){
               this.view.txtFloatText.text =withoutLastChar+"000000";
           } else  if(str.endsWith("t") || str.endsWith("T")){
             this.view.txtFloatText.text = withoutLastChar+"000";
           } else {
             //do not add the non numeric input
             //check for addl component properties
             this.view.txtFloatText.text = withoutLastChar;
           }
         } else {
           if(isNaN(str)){
             //non a numeric input
             this.view.txtFloatText.text = withoutLastChar;
           } else {
             //str is a pure number
             this.view.txtFloatText.text = str;
           }
           
         }
    },
    
    compTxtOnBeginEditing : function(){
      this.animateComponent();
    },
    
    setErrorModeSkin : function(){
      this.view.skin = "sknFlxWhiteBgRedBorder";
    },
    clearErrormodeSkin : function() {
      this.view.skin = "sknFlxWhiteBg";
    },
    
    defaultSkin: function() {
      this.view.flxFloatLableGrp.centerY="50%";
      this.view.lblFloatLabel.skin="sknFloatLblPreInput";
    },
    onTxtOnChng : function(callbackFn){
      //execute base first
      this.onTxtChanged();
      kony.print("**VIVEK **** customonEndEdit");
      if(typeof callbackFn === 'function'){
        //then the custom fn
         callbackFn();
      }
    },
    
    animateComponent:function(){
      this.clearErrormodeSkin();
      if(this.view.flxFloatLableGrp.centerY!=="23%"){
        this.view.lblFloatLabel.skin="sknFloatLblPostInput";
        this.view.flxFloatLableGrp.animate(
          kony.ui.createAnimation({
            "100":{
              centerY:"23%",
              "stepConfig": {
                "timingFunction": kony.anim.EASE
              }
            }
          }), {
            "delay":0,
            "iterationCount":1,
            "duration":0.1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS
          }, {
            "animationEnd":function() {
              kony.print("****Animation End****");
            }
          });
      }
    },
    reverseAnimateComponent:function(){
      kony.print("VIVEK *** onend editing of original template");
      try{
        var txt=this.view.txtFloatText.text;
        if(txt.length===0){
          this.reverseAnimateComponentUtil();
        }
      }catch(e){
        this.reverseAnimateComponentUtil();
      }
    },
    
    reverseAnimateComponentUtil:function(){
      var txtLabel = this.view.txtFloatText;
      if(txtLabel!==null){
        this.view.txtFloatText.text="";
        var txt = txtLabel.text;
        if(kony.string.equalsIgnoreCase(txt,"")){
          this.view.flxFloatLableGrp.animate(
            kony.ui.createAnimation({
              "100":{
                centerY:"50%",
                "stepConfig": {
                  "timingFunction": kony.anim.EASE
                }
              }
            }), {
              "delay":0,
              "iterationCount":1,
              "duration":0.1,
              "fillMode": kony.anim.FILL_MODE_FORWARDS
            }, {
              "animationEnd":function() {
                kony.print("****Animation End****");
              }
            });

          this.view.lblFloatLabel.skin="sknFloatLblPreInput";
        }
      }

    }
  };
});