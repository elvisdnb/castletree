define(function() {

  return {

    initGettersSetters: function() {
            this.view.flxInnerCollapseWrapper.isVisible = false;
            this.view.lblArrow.text = 'M';
            /*defineGetter(this, 'selectedKey', () => {
                return this._selectedKey;
            });*/
            /*defineSetter(this, 'selectedKey', value => {
                this._selectedKey = value;
            });*/
        },

    collapseResponseContainer : function() {
      this.view.flxInnerCollapseWrapper.isVisible = false;
      this.view.lblDescriptionNotesHeaderResponse.isVisible = true;
    },

    expandREsponseContainer : function() {
      this.view.flxInnerCollapseWrapper.isVisible = true;
      this.view.lblDescriptionNotesHeaderResponse.isVisible = false;
    },

    populateDataToResponseContainer : function(desc,additionalNote,contractType,contractChannel,contractDate,status) {
      this.view.cusTextAreaDescriptionResponse.setEnabled(false);
      this.view.cusTextAreaDescriptionResponse.text = Application.validation.isNullUndefinedObj(desc);
      this.view.cusTextAreaAdditionalResponse.text = Application.validation.isNullUndefinedObj(additionalNote);
      this.view.lblAppointmentResponse.text = Application.validation.isNullUndefinedObj(contractType);
      this.view.lblContractChannelResponse.text = Application.validation.isNullUndefinedObj(contractChannel);
      this.view.lblContractDateResponse.text = Application.validation.isNullUndefinedObj(contractDate);
      kony.print("status"+status);
      if(Application.validation.isNullUndefinedObj(status) !== "") {
        
        //this.view.cusDropDownStatus.selectedKey = status;
      }

    },
    
    getStatusSelectKey : function() {
      return this.view.cusDropDownStatus.selectedKey;
    },
    
     setStatusSelectedKey : function(key) {
       if(!Application.validation.isNullUndefinedObj(key) !=="") {
       this.view.cusDropDownStatus.selectedKey = key;
       }
    },
    
    
  };
});