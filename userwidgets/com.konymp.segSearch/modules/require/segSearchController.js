define(function() {

  return {

    changeIntrestData :

    [

      [{lblTitle:"Change Interest",lblShelves:"Interest Rate Type ",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The default interest rate type agreed in the loan contract is displayed on the screen <br>&#9679;     An option to choose either Fixed or Floating rate is provided to alter the interest rate type<br>&#9679;     Entering the interest rate type is mandatory and cannot be left blank.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Tier Type",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"&#9679;     The tier type for a loan contract can be on a level, banded or single. <br>&#9679;     Tier type is only displayed and cannot be edited<br>&#9679;     It is automatically picked up from the loan contract<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Effective Date",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The effective date from which the interest rate changes are likely to be reflected is to be entered in this field <br>&#9679;     A calendar icon is provided to help choose the date which will then be replacing the current date.<br>&#9679;     It is mandatory to choose the effective date to procced with the change interest option.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Fixed Rate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Refers to the existing fixed interest rate of the loan<br>&#9679;     It can be edited and left blank if the loan is initially based on floating rate of interest <br>&#9679;     You can subsequently choose fixed option and fill in new fixed rate if the customer wishes to switch over<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Operand",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The operand field is provided to add or reduce the margins to the interest rates<br>&#9679;     A drop-down list with two options: add and subtract is provided for you to choose Operand is an optional field and can be edited as per what has been agreed with the customer<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Margin",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Margin refers to the margins that are added or subtracted from the interest rates. These are usually included in the initial loan contract itself.<br>&#9679;     Margins can be edited but not compulsory for the change interest option<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Effective Rate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The effective rate field calculates and displays the revised effective rate after adding or subtracting the margin from the interest rate specified for the level<br>&#9679;     Effective rate is automatically calculated and cannot be edited. It can be left blank if it is not defined<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Upto Amount",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The tier amount (if loan on tier basis) specified in the loan contract level is displayed in this field<br>&#9679;     It cannot be edited and can be blank if it is not defined<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Floating Rate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The Floating rate displays the default floating index from the loan<br>&#9679;     This can be edited and left blank when the loan is on fixed rate</p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"View Payment Schedule",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Revised payment schedule details are displayed upon selecting the View Payment Schedule<br>&#9679;     The Revised payment schedule is displayed only when interest type or rates is changed<br>&#9679;     The Revised payment schedule displays the Payment Date, Amount, Principal, Interest, Tax and Total Outstanding<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Amount",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the total repayment amount for the corresponding date<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Principal",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Principal amount for the corresponding repayment date<br>&#9679;     This can be edited by the User but an optional field<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Effective Rate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Calculates the effective rate including margin and displays the effective rate<br>&#9679;     Cannot be edited by the user<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Upto Amount",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The tier amount (if loan on tier basis) as defined at the product/contract level needs to be displayed as default<br>&#9679;     It cannot be edited by the Bank User<br>&#9679;     It can be blank if not defined<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"View Payment Schedule",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     An icon or a button to view the revised payment schedule details The Revised payment schedule is displayed only when the Bank User changes the Interest type or rates<br>&#9679;     The Revised payment schedule displays the Payment Date, Amount, Principal, Interest, Tax and Total Outstanding <br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Payment Date",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the date of repayment in DD-MMM-YYYY format<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Amount",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the total repayment amount for the corresponding date<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Principal",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Principal amount for the corresponding repayment date<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Interest",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Interest amount for the corresponding repayment date<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Tax",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Tax amount for the corresponding repayment date<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Change Interest",lblShelves:"Total Outstanding",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Total Outstanding amount for the corresponding repayment date<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]]
    ],

    loanPayoffData :  

    [

      [{lblTitle:"Loan Payoff",lblShelves:"Arrangement ID",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Refers to the loan account number or the contract arrangement number.<br>&#9679;     The Interest rate type field is a radio button with 2 options: Fixed or Floating<br>&#9679;     Though it is not displayed on the screen, this id is required for background processing <br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Payoff Statement",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"&#9679;     By default, it refers to the current date <br>&#9679;     It is a compulsory field and can be edited <br>&#9679;     Clicking this will enable a Calendar icon from where the date can be chosen<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Proceed",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Proceed take you to the Payoff screen from the statement screen<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Cancel",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Cancel takes you back to the customer loan dashboard with list of loans<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Total Outstanding",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the total amount of loan outstanding as of the selected effective date <br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Settlement Mode",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;    The payment mode should allows you to make the payment via the following options:<br>&#9679;     Account to account transfer - Based on selection, the amount must be transferred from the customer’s savings/current account to the loan account.<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Debit Account",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The PAYING ACCOUNT to be defaulted from the loan contractWhere more than one Paying account is mapped to the contract, the account number in the 1st Multi-value shall be displayed by default<br>&#9679;     A search icon is provided to select an account that is not mapped to settlement account <br>&#9679;     Standard Transact account enquiry to be mapped<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Payment Value date",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;    Should be defaulted from the effective date selection<br>&#9679;    Cannot be edited<br></p>",

         template:"CopyflxParent", flxRow:{height:"80dp"}}]]     



    ],

    sectionClicked:function(context){
      console.log(context);
      var index = context.sectionIndex;
      var tempData = this.view.seg1.data;
      if(tempData[index][0].lblDown === "M"){

        tempData[index][0].lblDown = "J";

        tempData[index][1] = [];
      }else{

        tempData[index][0].lblDown = "M";

        tempData[index][1] = this.changeIntrestData[index][1];
        console.log("index-- "+this.changeIntrestData[index][1]);
      }
      this.view.seg1.setData(tempData);
    },


    //       mData.forEach((txt)=>{
    //           txt[0].lblDown = "J";  
    //           //txt[0].template = "CopyflxDown";
    //         });
    //         mData.forEach(([,txt])=>{
    //           txt.forEach((lbl)=>{
    //             lbl.flxRow.height = "80dp";
    //             //lbl.template = "CopyflxParent";
    //           });
    //         });
  };
});