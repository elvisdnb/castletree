define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onRowClick defined for seg1 **/
    AS_Segment_c493fc5848a547fd99347df2d6f0e327: function AS_Segment_c493fc5848a547fd99347df2d6f0e327(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.onClickRow.call(this);
    }
});