define(function() {

  return {


    initGettersSetters : function() {
      this.setCustomerValuesFromCache();
    },

    restCustomerValues : function(){  
      var newCustomerObj = {};
      gblCustomerObj = {};
      this.setCustomerValues(newCustomerObj);
    },

    setCustomerValues : function(customerObj){
      this.view.lblCustomerName.text = customerObj.customerName;
      this.view.lblCustomerNumberResponse.text = customerObj.customerId;
      if(customerObj.email && customerObj.email.length > 18){
        customerObj.email = customerObj.email.substring(0,18) + "..";
      }
      this.view.lblEmailResponse.text = customerObj.email;
      this.view.lblPhoneResponse.text = customerObj.phoneNumber
      this.view.lblProfileInfo.text =  customerObj.profileTypeName;
      this.view.lblProfileResponse.text = customerObj.profileName;
      this.view.lblRelationResponse.text = customerObj.sectorName;
      this.view.lblNationalityResponse.text = customerObj.nationalityName;
      this.view.lblEmplomentStatusResponse.text= customerObj.employmentStatus;
      this.view.lblEmployerResponse.text= customerObj.employerName;
      this.view.lblLocationResponse.text =  customerObj.residenceName;
      this.view.lblDOBResponse.text = customerObj.dateOfBirth;
      if(customerObj.accountOfficerName && customerObj.accountOfficerName.length > 18){
        customerObj.accountOfficerName = customerObj.accountOfficerName.substring(0,18) + "..";
      }

      this.view.lblAccountOfficerResponse.text = customerObj.accountOfficerName;
      this.view.lblMobileBankingResponse.text = customerObj.isMobileBankingService;
      this.view.lblInternetBankingResponse.text = customerObj.isInternetBankingService;
      gblCustomerObj =  customerObj;
    },

    setCustomerValuesFromCache : function(){
      this.view.lblCustomerName.text = gblCustomerObj.customerName;
      this.view.lblCustomerNumberResponse.text = gblCustomerObj.customerId;
      this.view.lblEmailResponse.text =gblCustomerObj.email;
      this.view.lblPhoneResponse.text = gblCustomerObj.phoneNumber;
      this.view.lblProfileInfo.text =  gblCustomerObj.profileTypeName;
      this.view.lblProfileResponse.text = gblCustomerObj.profileName;
      this.view.lblRelationResponse.text = gblCustomerObj.sectorName;
      this.view.lblNationalityResponse.text = gblCustomerObj.nationalityName;
      this.view.lblEmplomentStatusResponse.text= gblCustomerObj.employmentStatus;
      this.view.lblEmployerResponse.text= gblCustomerObj.employerName;
      this.view.lblLocationResponse.text =  gblCustomerObj.residenceName;
      this.view.lblDOBResponse.text = gblCustomerObj.dateOfBirth;
      this.view.lblAccountOfficerResponse.text = gblCustomerObj.accountOfficerName;
      this.view.lblMobileBankingResponse.text = gblCustomerObj.isMobileBankingService;
      this.view.lblInternetBankingResponse.text = gblCustomerObj.isInternetBankingService;
    }

  };
});