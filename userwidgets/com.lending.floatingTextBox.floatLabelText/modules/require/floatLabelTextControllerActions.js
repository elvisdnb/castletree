define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onBeginEditing defined for txtFloatText **/
    AS_TextField_a1e3082132434504a05b669394cc9a45: function AS_TextField_a1e3082132434504a05b669394cc9a45(eventobject, changedtext) {
        var self = this;
        return self.compTxtOnBeginEditing.call(this);
    },
    /** onEndEditing defined for txtFloatText **/
    AS_TextField_g91c489dce3c4c0289780ed6ea6b06e6: function AS_TextField_g91c489dce3c4c0289780ed6ea6b06e6(eventobject, changedtext) {
        var self = this;
        return self.compTxtOnEndEditing.call(this);
    }
});