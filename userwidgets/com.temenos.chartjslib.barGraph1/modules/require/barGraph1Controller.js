define(function() {

	return {
		constructor: function(baseConfig, layoutConfig, pspConfig) {
		},
		//Logic for getters/setters of custom properties
		initGettersSetters: function() {

		},
      
       updateGraph : function(){
         kony.print("update graph function called on component, will this trigger even to tpw");
         //Please set valid  properties before calling this property update
         //e.g. Do not hide graph on initial rendering
         // var mydatasets = [{"label":"lbl1","backgroundColor":"#FF9800","data":[10,11,9]},{"label":"lbl2","backgroundColor":"#2C8631","data":[8,4,5]}]
		// var xlabels = ["a", "b", "c"]
         this.view.TPWbargraph1.updateBars =  true;
       }
	};
});