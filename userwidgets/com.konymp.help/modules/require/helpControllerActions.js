define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnPlus2 **/
    AS_Button_a61dda0d6ec3460eab26afb79ec07dd8: function AS_Button_a61dda0d6ec3460eab26afb79ec07dd8(eventobject) {
        var self = this;
        return self.expandFunc2.call(this);
    },
    /** onClick defined for btnLoan **/
    AS_Button_a9960c3e36454247b50ad32401d3af17: function AS_Button_a9960c3e36454247b50ad32401d3af17(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    /** onClick defined for btn4 **/
    AS_Button_af8b0070cab044c2a847ff5bf8a5ffbc: function AS_Button_af8b0070cab044c2a847ff5bf8a5ffbc(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    /** onClick defined for btnPlus4 **/
    AS_Button_b050c26801874b91aa86dd60d1de7653: function AS_Button_b050c26801874b91aa86dd60d1de7653(eventobject) {
        var self = this;
        return self.expandFunc4.call(this);
    },
    /** onClick defined for btnVideo **/
    AS_Button_b5a11f28e4e94e3f87e7addb97961710: function AS_Button_b5a11f28e4e94e3f87e7addb97961710(eventobject) {
        var self = this;
        return self.enableVideo.call(this);
    },
    /** onClick defined for btnClearTxt **/
    AS_Button_bdd5065cfdef4fdfa7fbc7fc7a53bcbc: function AS_Button_bdd5065cfdef4fdfa7fbc7fc7a53bcbc(eventobject) {
        var self = this;
        return self.clearTxtBxSearch.call(this);
    },
    /** onClick defined for btnCloseHelp **/
    AS_Button_c7621bafc21e49edb500dc952504f498: function AS_Button_c7621bafc21e49edb500dc952504f498(eventobject) {
        var self = this;
        return self.onClickBtnCloseHelp.call(this);
    },
    /** onClick defined for btnBackShowMore **/
    AS_Button_d59462caa3844de598357c8c69b3cb69: function AS_Button_d59462caa3844de598357c8c69b3cb69(eventobject) {
        var self = this;
        return self.onClickBckShowMore.call(this);
    },
    /** onClick defined for btn6 **/
    AS_Button_d5e7355265e04a868c79b333c07ad831: function AS_Button_d5e7355265e04a868c79b333c07ad831(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    /** onClick defined for btnDeposit **/
    AS_Button_d7b96f0d523e4992a759010fb2db87ba: function AS_Button_d7b96f0d523e4992a759010fb2db87ba(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    /** onClick defined for lblLoanServices **/
    AS_Button_e4d554bce5874df89a1425170d266b94: function AS_Button_e4d554bce5874df89a1425170d266b94(eventobject) {
        var self = this;
        return self.loanServicesHome.call(this);
    },
    /** onClick defined for btn1 **/
    AS_Button_e4ea719cfadc4f008f35b898932b2808: function AS_Button_e4ea719cfadc4f008f35b898932b2808(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    /** onClick defined for btn2 **/
    AS_Button_e5c26499c6f243f585ceeb0565124ad8: function AS_Button_e5c26499c6f243f585ceeb0565124ad8(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    /** onClick defined for btnPlus3 **/
    AS_Button_e8ebbe79c01b4e9a85b59a3b86968214: function AS_Button_e8ebbe79c01b4e9a85b59a3b86968214(eventobject) {
        var self = this;
        return self.expandFunc3.call(this);
    },
    /** onClick defined for btnShowMore **/
    AS_Button_eb7d3dbba998435e9040ecab21dbf2b6: function AS_Button_eb7d3dbba998435e9040ecab21dbf2b6(eventobject) {
        var self = this;
        return self.showMore.call(this);
    },
    /** onClick defined for btn7 **/
    AS_Button_f366aa14dac14f8aac543153bab98f79: function AS_Button_f366aa14dac14f8aac543153bab98f79(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    /** onClick defined for btn3 **/
    AS_Button_f44e332a93af4fad874a55484222a065: function AS_Button_f44e332a93af4fad874a55484222a065(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    /** onClick defined for btnHomeHelp **/
    AS_Button_fb2ffec1480e4a91ab402dbc345ce18a: function AS_Button_fb2ffec1480e4a91ab402dbc345ce18a(eventobject) {
        var self = this;
        return self.helpHome.call(this);
    },
    /** onClick defined for btn5 **/
    AS_Button_fd84a072e5794071b3d4481a0ffb4ef3: function AS_Button_fd84a072e5794071b3d4481a0ffb4ef3(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    /** onClick defined for btnCloseVideo **/
    AS_Button_fe5de1f853c64277a5b6644bcff72b14: function AS_Button_fe5de1f853c64277a5b6644bcff72b14(eventobject) {
        var self = this;
        return self.onClickBckVideo.call(this);
    },
    /** onClick defined for btnAccount **/
    AS_Button_g07438ebeb6143949f2e5d43b002caf3: function AS_Button_g07438ebeb6143949f2e5d43b002caf3(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    /** onClick defined for btnPlus1 **/
    AS_Button_gd593ce330574612903e381494a26fe8: function AS_Button_gd593ce330574612903e381494a26fe8(eventobject) {
        var self = this;
        return self.expandFunc.call(this);
    },
    /** onClick defined for btnBckType **/
    AS_Button_hb78e04627a942f8b3cf4aabac4b8772: function AS_Button_hb78e04627a942f8b3cf4aabac4b8772(eventobject) {
        var self = this;
        return self.onClickBckVideo.call(this);
    },
    /** onClick defined for btn8 **/
    AS_Button_j9534b4701d14de78630876d593c6806: function AS_Button_j9534b4701d14de78630876d593c6806(eventobject) {
        var self = this;
        return self.selectedServiceType.call(this, eventobject.text);
    },
    /** onClick defined for flxAccount **/
    AS_FlexContainer_cb20c973e91b4730806650c7d0361ea1: function AS_FlexContainer_cb20c973e91b4730806650c7d0361ea1(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    /** onClick defined for flxExpand4 **/
    AS_FlexContainer_f2df1befbb2c462ea205c7061ea7e186: function AS_FlexContainer_f2df1befbb2c462ea205c7061ea7e186(eventobject) {
        var self = this;
        return self.expandFunc4.call(this);
    },
    /** onClick defined for CopyflxBck0c8bd2742893248 **/
    AS_FlexContainer_j1d35ca4ca3d4010a3897ab58d163b89: function AS_FlexContainer_j1d35ca4ca3d4010a3897ab58d163b89(eventobject) {
        var self = this;
        return self.onClickBckAnswers.call(this);
    },
    /** onClick defined for flxDeposits **/
    AS_FlexContainer_jab0b7944625463ab8d4906fc027d099: function AS_FlexContainer_jab0b7944625463ab8d4906fc027d099(eventobject) {
        var self = this;
        return self.helpServiceType.call(this, eventobject.id);
    },
    /** onKeyUp defined for txtPartialText **/
    AS_TextField_dbdfcb90acd04036b190f4b10a2b8783: function AS_TextField_dbdfcb90acd04036b190f4b10a2b8783(eventobject) {
        var self = this;
        self.searchData.call(this);
    }
});