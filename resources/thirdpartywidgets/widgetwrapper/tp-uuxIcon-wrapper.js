uuxIcon = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-icon id='"+widgetModel.parent.id+widgetModel.id+"'size='"+widgetModel.size+"'color='"+widgetModel.color+"'>"+widgetModel.iconName+"</uwc-icon>";

    var element = parentNode.lastElementChild;
    element.size = widgetModel.size;
    element.color = widgetModel.color;
    element.iconName = widgetModel.iconName;
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    let element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element)
    {
      if(propertyChanged==="size"){
        element.size = propertyValue;
      }
      if(propertyChanged==="iconName"){
        element.iconName = propertyValue;
      }
      if(propertyChanged==="color"){
        element.color = propertyValue;
      }
    }
  }
};