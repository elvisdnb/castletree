uuxRecurrenceDatePicker = {
	//TODO :DV - we should not do this, what if 2 of these CWs are included in same viz form ?
  _domElement:null,
  initializeWidget: function(parentNode, widgetModel) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.style.overflow="visible";
    const widgetId = widgetModel.parent.id+widgetModel.id;

    parentNode.innerHTML = "<uwc-date-recurrence-picker id="+widgetId+"></uwc-date-recurrence-picker>";
    const element = parentNode.lastChild;
    this._domElement = element;

    if(widgetModel.lableText){
      element.label = widgetModel.lableText;
    }
    if(widgetModel.dense){
      element.dense = widgetModel.dense;
    }
    var supportedProperties = ["dailyMax","monthlyDayMax","monthlyMax","value","weeklyMax","yearlyDayMax","yearlyMax","messagePrefix","formattedValue"];

    supportedProperties.forEach((propertyName)=>{
      element[propertyName] = widgetModel[propertyName];
    });

    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
      widgetModel.formattedValue = event.target.formattedValue;
    });
	
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
	 var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
	  
    if(propertyChanged==="lableText"){
      element["label"] = propertyValue;
    }
    if(propertyChanged==="value"){
		element["value"] = propertyValue;
		if((typeof widgetModel.onValueSelected)==="function"){
			widgetModel.onValueSelected(widgetModel);
		}
    }
  }
};