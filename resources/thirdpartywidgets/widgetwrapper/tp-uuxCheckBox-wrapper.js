  uuxCheckBox = {

  /**
   * @function
   *
   * @param parentNode 
   * @param widgetModel 
   * @param config 
   */
  initializeWidget: function(parentNode, widgetModel, config) {

    parentNode.innerHTML = "<uwc-checkbox id="+widgetModel.parent.id+widgetModel.id+" label='"+widgetModel.labelText+"'></uwc-checkbox>"
    var element = parentNode.lastElementChild;
    if(widgetModel.value){
      element.value = widgetModel.value;
      element.checked =(widgetModel.value==="true")?true:false; 
    }
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });
  },
  /**
   * @function
   *
   * @param widgetModel 
   * @param propertyChanged 
   * @param propertyValue 
   */
  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element){
      if(propertyChanged==="labelText"){
        element.label = propertyValue;
      }else if(propertyChanged==="value"){
        if(propertyValue==="true"){
          element.value = "true";
          element.checked = true;
        }else{ 
          element.checked = false;
          element.value = "false";
        }
        //raising customEvent
        if(widgetModel.valueChanged){
          widgetModel.valueChanged(propertyValue);
        }
      }else element[propertyChanged] = propertyValue;
    }
  }
};