uuxSlider = {

    initializeWidget: function(parentNode, widgetModel, config) {
        //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-slider id="+widgetModel.parent.id+widgetModel.id+"  style='margin-left:8px'></uwc-slider>";
    const element = parentNode.lastChild;
    this._domElement = element;

    if(widgetModel.labelText)
    {
      element.label = widgetModel.labelText;
    }

    var commonProperties = ["step", "max", "min","dataType","currencySymbol"];
    commonProperties.forEach((property) => {
      element[property] = widgetModel[property];
    })

    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
    });
        
    },

    modelChange: function(widgetModel, propertyChanged, propertyValue) 
  {
        //Handle widget property changes to update widget's view and
        //trigger custom events based on widget state.
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element){
      if(propertyChanged === "labelText"){
        element.label = propertyValue;
      }else element[propertyChanged] = propertyValue;
    }
  }
    
};