uuxAmountComponent = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
	
	var element = document.createElement("uwc-amount-field");
    element.id  = widgetModel.parent.id+widgetModel.id;

	if(widgetModel.labelDescription){
		element.label = widgetModel.labelDescription;
	}
	if(widgetModel.currencyCode){
		element.currencyCode = widgetModel.currencyCode;
	}
	if(widgetModel.decimals){
		element.decimals = widgetModel.decimals;
	}
	if(widgetModel.readonly  == true){
      element.setAttribute("readonly","");
    }
    if(widgetModel.disabled == true){
      element.setAttribute("disabled","");
    }
    if(widgetModel.dense == true){
      element.setAttribute("dense","");
    }
	if(widgetModel.validateRequired == "required"){
      element.setAttribute("required","");
    }
	if(widgetModel.required == true){
      element.setAttribute("required","");
    }

	if(widgetModel.value){
      element.value = widgetModel.value;
      element.checked =(widgetModel.value==="true")?true:false; 
    }
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });
    element.addEventListener('change',(evt)=>{
      widgetModel.currencyCode = evt.target.currencyCode;
    });

	
	var OlderElement = document.getElementById(element.id);
	if(OlderElement){parentNode.removeChild(OlderElement); //if exists
	}

	parentNode.append(element);
	
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);

    if(element){
      if(propertyChanged === "labelDescription"){
        element["label"] = propertyValue;  
      } else if(propertyChanged === "currencyCode"){
        element["currencyCode"] = propertyValue;  
      } else if(propertyChanged === "decimals"){
        element["decimals"] = propertyValue;  
      } else if(propertyChanged === "readonly"){
		  if(propertyValue == true){
			  element.setAttribute("readonly","");
		  } else {
			  element.removeAttribute("readonly");
		  }
      } else if(propertyChanged === "disabled"){
		   if(propertyValue == true){
			  element.setAttribute("disabled","");
		  } else {
			  element.removeAttribute("disabled");
		  }
      } else if(propertyChanged === "dense"){
		   if(propertyValue == true){
			  element.setAttribute("dense","");
		  } else {
			  element.removeAttribute("dense");
		  }
      } else if(propertyChanged === "validateRequired"){
		  if(propertyValue == "required"){
			  element.setAttribute("required","");
		  } else {
			  element.removeAttribute("required");
		  }
      } else if(propertyChanged === "required"){
		  if(propertyValue == true){
			   element.setAttribute("required","");
		  } else {
			  element.removeAttribute("required");
		  }
      } else {
		element[propertyChanged] = propertyValue;  
	  }
      
	  if(propertyChanged==="value"){
        if((typeof widgetModel.onChangeEvent)==="function"){
          widgetModel.onChangeEvent(propertyValue);
        }
      }
    }
  }
};