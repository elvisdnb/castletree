uuxTextField = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    if(widgetModel.readonly){
      widgetModel.readonly = 'readonly';
    }
    if(widgetModel.disabled){
      widgetModel.disabled = 'disabled';
    }
    parentNode.innerHTML = "<uwc-text-field id='"+widgetModel.parent.id+widgetModel.id+"' label='"+widgetModel.labelText+"' type='"+widgetModel.fieldType.selectedValue+"' value='"+widgetModel.value+"' validationMessage='"+widgetModel.validationMessage+"'" +widgetModel.validateRequired+" "+widgetModel.dense+"  "+widgetModel.readonly+"   "+widgetModel.disabled+"></uwc-text-field>";   
    var element = parentNode.lastElementChild;
    if(widgetModel.dense){
      element.dense = widgetModel.dense;
    }
    if(widgetModel.readonly){
      element.readonly = 'readonly';
    }
     if(widgetModel.maxLength == undefined){
       element.setAttribute("maxLength",widgetModel.maxLength); 
      //element.maxLength = 'readonly';
    }
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });

    element.trailingButtonClick = (event)=>{
      if((typeof widgetModel.trailingButtonClick)==="function"){
        //widgetModel.value = event.target.value;
        widgetModel.trailingButtonClick();
      }
    };

    element.addEventListener('keydown',(evt)=>{
      if((typeof widgetModel.onKeyDown)==="function"){widgetModel.onKeyDown(evt)}
    });

    element.addEventListener('keyup',(evt)=>{
      if((typeof widgetModel.onKeyUp)==="function"){widgetModel.onKeyUp(evt)}
    });

    element.validationMessage = widgetModel.validationMessage;
    //element.validationMessage = "Invalid Value";

    /**element.validityTransform = (newValue,nativeValidity) => {
      let validity = widgetModel.isValid(newValue);
      return {
        valid : validity,
        customError : !validity
      };
    }; **/   
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.

    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);

    if(element){
      if(propertyChanged === "labelText"){
        element["label"] = propertyValue;  
      } else if(propertyChanged === "validationMessage"){
        element.setAttribute("validationMessage",propertyValue);
      } else if(propertyChanged === "validateRequired"){
        element.setAttribute("required","");
      }  else if(propertyChanged === "maxLength"){
        element.setAttribute("maxLength",propertyValue); 
      } else if(propertyChanged === "fieldType"){
        element.setAttribute("type",propertyValue); 
      } 
      else element[propertyChanged] = propertyValue;

      if(propertyChanged==="value"){
        element.setAttribute("value",propertyValue); 
        if((typeof widgetModel.valueChanged)==="function"){
          widgetModel.valueChanged(propertyValue);
        }
        if((typeof widgetModel.onKeyUp)==="function"){
          widgetModel.onKeyUp(propertyValue);
        }

      }

    }
  }
};