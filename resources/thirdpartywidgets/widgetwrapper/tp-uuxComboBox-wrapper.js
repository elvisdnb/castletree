uuxComboBox = {

  initializeWidget: function(parentNode, widgetModel) {
    //Assign custom DOM to parentNode to render this widget.
    //Assign custom DOM to parentNode to render this widget.
    const widgetId = widgetModel.id+widgetModel.parent.id;
    parentNode.innerHTML = "<uwc-combobox id ="+widgetId+" ></uwc-combobox>";
    //var element = document.getElementById(widgetModel.id);
    parentNode.style.overflow = "visible";
    var element = parentNode.lastChild;

    element.options =widgetModel.options;
    element.disabled = widgetModel.disabled;

    element.readonly = widgetModel.readonly;
    element.required = widgetModel.required;
    element.label = widgetModel.labelText;
    element.value = widgetModel.value;
    element.mode = widgetModel.mode;
    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
    });

  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    let element =document.getElementById(widgetModel.id+widgetModel.parent.id);

    if(element)
    {if(propertyChanged==="value"){
      element.value = propertyValue;
      if(widgetModel.valueChanged){
        widgetModel.valueChanged(propertyValue);
      }
    }else if(propertyChanged==="options"){
      element.options = propertyValue;
    }else if(propertyChanged==="labelText"){
      element.label = propertyValue;
    }else element[propertyChanged] = propertyValue;
    }
  }
};