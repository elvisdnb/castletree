uuxFrequencyPicker = {
  _domElement:null,
  initializeWidget: function(parentNode, widgetModel) {
    const widgetId = widgetModel.id;
    /*messagePrefix is nullified to avoid showing default value "A notification will be sent*/
    parentNode.innerHTML = "<uwc-date-frequency-picker messagePrefix='' id="+widgetId+"></uwc-date-frequency-picker>";
    //Setting overflow as visible
     parentNode.style.overflow="visible";
    const element = parentNode.lastChild;
    this._domElement = element;
    if(widgetModel.labelText){
      element.label = widgetModel.labelText;
    }
    if(widgetModel.value){
      element.value = widgetModel.value;
      widgetModel.formattedValue = element.formattedValue;
    }
    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
        widgetModel.formattedValue = event.target.formattedValue;
    });
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
	this._domElement[propertyChanged] = propertyValue;
  }
};