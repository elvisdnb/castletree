uuxButton = {

  initializeWidget: function(parentNode, widgetModel, config) {
	 //Assign custom DOM to parentNode to render this widget.
    var element = document.createElement("uwc-button");
    
	element.id  = widgetModel.parent.id+widgetModel.id;

	if(widgetModel.labelText){
      element.label = widgetModel.labelText;
    }
	if(widgetModel.typeButton){
      element.type = widgetModel.typeButton.selectedValue;
    }
	if(widgetModel.size){
      element.size = widgetModel.size.selectedValue;
    }
	if(widgetModel.icon){
      element.icon = widgetModel.icon;
    }
	if(widgetModel.contained && widgetModel.contained == true){
      element.setAttribute("contained","");
    }
	if(widgetModel.outlined && widgetModel.outlined == true){
      element.setAttribute("outlined","");
    }
	if(widgetModel.cta && widgetModel.cta == true){
      element.setAttribute("cta","");
    }
	if(widgetModel.compact && widgetModel.compact == true){
      element.setAttribute("compact","");
    }
	if(widgetModel.disabled){
      element.disabled = widgetModel.disabled;
    }
	if(widgetModel.trailingIcon){
      element.trailingIcon = widgetModel.trailingIcon;
    }
	
    //element.addEventListener
    element.onclick = (event)=>{
      if((typeof widgetModel.onclickEvent)==="function"){
        //widgetModel.value = event.target.value;
        widgetModel.onclickEvent();
      }
    }
    
    /**element.addEventListener('onclick', (event) => {
      //alert(JSON.stringify(event));
      widgetModel.onclickEvent = event.target.onclick;
    });**/




	var OlderElement = document.getElementById(element.id);
	if(OlderElement){parentNode.removeChild(OlderElement); //if exists
	}
	
	parentNode.append(element);


  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    let element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element)
    {
      if(propertyChanged==="disabled"){
        element.disabled = propertyValue;
      }
      if(propertyChanged==="cta"){
        element.cta = propertyValue;
      }
      if(propertyChanged==="outlined"){
        element.outlined = propertyValue;
      }
      if(propertyChanged==="compact"){
        element.compact = propertyValue;
      }
      if(widgetModel.onclickEvent){
        widgetModel.onclickEvent(propertyValue);
      }

    }
  }
};