uuxTextArea = {
  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-text-area id='"+widgetModel.parent.id+widgetModel.id+"' label='"+widgetModel.labelText+"' charCounter maxLength='"+widgetModel.maxLength+"' value='"+widgetModel.value+"' validationMessage='"+widgetModel.validationMessage+"'" +widgetModel.validateRequired+" "+widgetModel.disabled+"></uwc-text-area>";
    var element = parentNode.lastElementChild;
    if(widgetModel.disabled) {
      element.disabled = widgetModel.disabled;
    }
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });    
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element){
      if(propertyChanged === "labelText"){
        element["label"] = propertyValue;  
      }else if(propertyChanged === "validationMessage"){
        element["validationMessage"] = propertyValue;  
      } else if(propertyChanged === "validateRequired"){
        element["validateRequired"] = propertyValue;  
      }else element[propertyChanged] = propertyValue;
      
      if(propertyChanged==="value"){
        if((typeof widgetModel.valueChanged)==="function"){
           widgetModel.valueChanged(propertyValue);
        }
      }
    }
  }
};