uuxNavigationRail = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    var element = document.createElement("uwc-navigation-rail");
    element.id  = widgetModel.parent.id+widgetModel.id;
    
    if(widgetModel.positionValue){
      element.position = widgetModel.positionValue;
    }
	if(widgetModel.activeIndex){
      element.activeIndex = widgetModel.activeIndex;
    }
    if(widgetModel.menuItems){
      element.menuItems = JSON.parse(widgetModel.menuItems);
    }
    if(widgetModel.bottomBtnIcon){
      element.bottomBtnIcon = widgetModel.bottomBtnIcon;
    }
    if(widgetModel.bottomBtnItems){
      element.bottomBtnItems = JSON.parse(widgetModel.bottomBtnItems);
    }
    if(widgetModel.hideBottomBtn){
      element.hideBottomBtn = widgetModel.hideBottomBtn;
    }
    if(widgetModel.logoAlt){
      element.logoAlt = widgetModel.logoAlt;
    }
	
	element.addEventListener('selected', (evt,idx) => {
      //if on menuitemselect function is defined then call it
	  widgetModel.activeIndex = evt.target.activeIndex;
    });
	
	 element.addEventListener('change', (evt) => {
      //if on menuitemselect function is defined then call it
	  widgetModel.activeIndex = evt.target.activeIndex;
    });
	
	 element.addEventListener('click', (evt) => {
      //if on menuitemselect function is defined then call it
	  widgetModel.activeIndex = evt.target.activeIndex;
    });
	
		 element.addEventListener('selectionchange', (evt) => {
      //if on menuitemselect function is defined then call it
	  widgetModel.activeIndex = evt.target.activeIndex;
    });
	
	 
	
	var OlderElement = document.getElementById(element.id);
	if(OlderElement){parentNode.removeChild(OlderElement); //if exists
	}
	
    parentNode.append(element);
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
	 var element;
    if(widgetModel.parent !== "" && widgetModel.parent !== undefined && widgetModel.parent!== null)
    {
    element = document.getElementById(widgetModel.parent.id+"_"+widgetModel.id);
    }
    if(element){
	}

  }
};