uuxAmountField = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-amount-field id='"+widgetModel.parent.id+widgetModel.id+"'label='"+widgetModel.labelValue+"' decimals='"+widgetModel.decimals+"'></uwc-amount-field>";
    var element = parentNode.lastElementChild;
    element.label = widgetModel.labelValue;
    element.decimals = widgetModel.decimals;
    if(widgetModel.value){
      element.value = widgetModel.value;
      element.checked =(widgetModel.value==="true")?true:false; 
    }
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });

  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    let element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element)
    {
      if(propertyChanged==="value"){
        element.value = propertyValue;
      }
      if(propertyChanged==="suffix"){
        element.suffix = propertyValue;
      }
      if(propertyChanged==="readonly"){
        element.readonly = propertyValue;
      }
      if(propertyChanged==="required"){
        element.required = propertyValue;
      }
      if(propertyChanged==="prefix"){
        element.prefix = propertyValue;
      }
      if(propertyChanged==="placeholderValue"){
        element.placeholderValue = propertyValue;
      }
      if(propertyChanged==="locale"){
        element.locale = propertyValue;
      }
      if(propertyChanged==="endAligned"){
        element.endAligned = propertyValue;
      }
      if(propertyChanged==="disabled"){
        element.disabled = propertyValue;
      }
      if(propertyChanged==="decimals"){
        element.decimals = propertyValue;
      }
      if(widgetModel.onChangeEvent){
        widgetModel.onChangeEvent(propertyValue);
      }

    }
  }
};