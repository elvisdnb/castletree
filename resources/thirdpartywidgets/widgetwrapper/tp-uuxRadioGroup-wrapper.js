uuxRadioGroup = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.style.overflow = "visible";
    parentNode.innerHTML ="<uwc-radio-group id="+widgetModel.parent.id+widgetModel.id+" ></uwc-radio-group>";

    var element = parentNode.lastChild;
    this._domElement = element;

    if(widgetModel.labelText){
      element.label = widgetModel.labelText;
    }

    if(widgetModel.options){
      element.options = widgetModel.options;
    }

    if(widgetModel.value){
      element.value = widgetModel.value;
    }

    element.addEventListener('change', (event) => {
      //alert(JSON.stringify(event));
      widgetModel.value = event.target.value;
    });

  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    let element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element)
    {
      if(propertyChanged==="value"){
        element.value = propertyValue;
      }
      if(propertyChanged==="options"){
        element.options = propertyValue;
      }
      if(propertyChanged==="labelText"){
        element.label = propertyValue;
      }
      if(widgetModel.onRadioChange){
        widgetModel.onRadioChange(propertyValue);
      }
    }
  }
};