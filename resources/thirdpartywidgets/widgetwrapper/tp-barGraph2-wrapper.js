barGraph2 = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    var element = document.createElement("canvas");
    element.id  = widgetModel.parent.id+widgetModel.id;
    this.elementId = element.id;
    element.width = widgetModel.canvasWidth;
    element.height = widgetModel.canvasHeight;

    var OlderElement = document.getElementById(element.id);
    if(OlderElement){parentNode.removeChild(OlderElement); //if exists
                    }
    parentNode.append(element);

    var ctx = document.getElementById(element.id);

    Chart.defaults.scale.ticks.beginAtZero = widgetModel.yBeginsAtZero;
    Chart.defaults.global.elements.line.fill  = widgetModel.lineAreaFill;
    Chart.defaults.scale.ticks.suggestedMin = widgetModel.ySuggestedMin;

    widgetModel.chartObj = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: JSON.parse(widgetModel.xlabelsArray),
        datasets: JSON.parse(widgetModel.datasetsArray),
      },
      options: {
        animation: {
          duration: 10,
        },
        hover: {          
          mode: 'index',
          intersect: false,
        },
        tooltips: {
          //mode: 'label',
          mode: 'index',
          intersect: false,
          callbacks: {
            label: function(tooltipItem, data) { 
              return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel;
            }
          }
        },
        scales: {
          xAxes: [{ 
            stacked: false, 
            gridLines: { display: false },
          }],
          yAxes: [{ 
            ticks: {
              min:widgetModel.ySuggestedMin,
              stepSize: widgetModel.yStepSize
            },
            stacked: false,

          }],
        }, // scales
        legend: {display: false}
      } // options
    }
                                    );


  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.

    var element;
    if(widgetModel.parent !== "" && widgetModel.parent !== undefined && widgetModel.parent!== null)
    {
      element = document.getElementById(this.elementId);
    }
    if(element){
      switch(propertyChanged){
        case "updateBars" :{
          if(propertyValue == true){
            var gd = JSON.parse(widgetModel.datasetsArray);
            var xl = JSON.parse(widgetModel.xlabelsArray);
            this.updateBars(widgetModel, gd,xl);
          }
          break;
        }
        case "datasets":{
          //no need to update the web graph immediately if dataset is changed
          break; 
        }
        case "xlabels":{
          //no need to update the web graph immediately if dataset is changed
          break;
        }
        case "lineAreaFill":{
          Chart.defaults.global.elements.line.fill  = propertyValue;
          break;
        }
        case "yBeginsAtZero" :{
          Chart.defaults.scale.ticks.beginAtZero = propertyValue;
          break;
        } 
        case "ySuggestedMin" : {
          Chart.defaults.scale.ticks.min = propertyValue;
          break;
        }
      }
    }
  },

  updateBars : function(widgetModel, myGraphData, xlabels){
    widgetModel.chartObj.data.labels = xlabels;
    widgetModel.chartObj.data.datasets= myGraphData;
    widgetModel.chartObj.options.scales.yAxes[0].ticks.min = widgetModel.ySuggestedMin;
    if(widgetModel.yStepSize){
      widgetModel.chartObj.options.scales.yAxes[0].ticks.stepSize = widgetModel.yStepSize;
    }
    widgetModel.chartObj.update();
  }
};