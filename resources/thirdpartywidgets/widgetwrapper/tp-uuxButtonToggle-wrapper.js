uuxButtonToggle = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    var element = document.createElement("uwc-button-toggle");
    element.id  = widgetModel.parent.id+widgetModel.id;

    if(widgetModel.options){
      element.options = JSON.parse(widgetModel.options);
    }
    if(widgetModel.compact && widgetModel.compact === true){
      element.setAttribute("compact","");
    }
    
    element.addEventListener('change', (evt) => {
      //if on menuitemselect function is defined then call it
      widgetModel.value = evt.target.value;
    });
    element.onclick = (event)=>{
      if((typeof widgetModel.onclickEvent)==="function"){
        //widgetModel.value = event.target.value;
        widgetModel.onclickEvent();
      }
    };
	
	var OlderElement = document.getElementById(element.id);
	if(OlderElement){parentNode.removeChild(OlderElement); //if exists
	}

    parentNode.append(element);
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    
    let element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element)
    {
		if(propertyChanged==="value"){
			  element.value = propertyValue;
			  if((typeof widgetModel.onclickEvent)==="function"){
				widgetModel.onclickEvent(propertyValue);
			  }
		}

    }

  }
};