uuxChip = {
  _domElement:null,
  initializeWidget: function(parentNode, widgetModel) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-chip-group  id ="+widgetModel.parent.id+widgetModel.id+"></uwc-chip-group>";

    var element = parentNode.lastChild;
    //assigining element from above creations
    this._domElement = element;
    //Array to generate chips
    element.options = widgetModel.options;
    //boolean value for selecting multiple chips as filter 
    element.filter = widgetModel.filter;
    //returns sleected items in string separated by comma
    element.value = widgetModel.value;
    //enables the icon at trail
    element.removable = widgetModel.removable;  
    element.compact = widgetModel.compact;
    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
    });
    element.addEventListener('chipRemoved', (event) => {
      if(widgetModel.removeChip){
        widgetModel.removeChip(event.detail.index);
      }
    });

  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    let element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if (element) {
      if (propertyChanged === "value" && widgetModel.value !== "") {
        element.value = propertyValue;
      }
    }
  }
};