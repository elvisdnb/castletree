uuxTable = {

    initializeWidget: function(parentNode, widgetModel, config) {
        //Assign custom DOM to parentNode to render this widget.
	var currentElement = document.createElement("uwc-table");
    currentElement.columns = widgetModel.columns;
    currentElement.compact = widgetModel.compact;
    currentElement.data = widgetModel.data;
    currentElement.paginationLabel = widgetModel.paginationLabel;
    currentElement.radioLabel = widgetModel.radioLabel;
  
    currentElement.addEventListener('change',(event)=>{
      widgetModel.value = event.target.value;
    });
    parentNode.append(currentElement);

        
    },
        

    modelChange: function(widgetModel, propertyChanged, propertyValue) {
        //Handle widget property changes to update widget's view and
        //trigger custom events based on widget state.

    }
};