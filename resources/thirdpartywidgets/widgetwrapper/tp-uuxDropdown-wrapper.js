uuxDropdown = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
     if(widgetModel.readonly){
      widgetModel.readonly = 'readonly';
    }
    if(widgetModel.disabled){
      widgetModel.disabled = 'disabled';
    }
    parentNode.style.overflow = 'visible';
    parentNode.innerHTML = "<uwc-select id='"+widgetModel.parent.id+"_"+widgetModel.id+"' label='"+widgetModel.labelText+"'" +widgetModel.validateRequired+" "+widgetModel.readonly+" "+widgetModel.dense+"></uwc-select>";
    var element = parentNode.lastElementChild;
    if(widgetModel.options && widgetModel.options!==""){
      element.options = JSON.parse(widgetModel.options);
      if(widgetModel.value){
        element.value = widgetModel.value;
      }
    }
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    var element;
    if(widgetModel.parent !== "" && widgetModel.parent !== undefined && widgetModel.parent!== null)
    {
    element = document.getElementById(widgetModel.parent.id+"_"+widgetModel.id);
    }
    if(element){
      switch(propertyChanged){
        case "labelText" :{
          element.label = propertyValue;
          break;
        }
        case "value" :{
          element.setAttribute("value",propertyValue); 
          //element.value = propertyValue;
          if((typeof widgetModel.valueChanged)==="function"){
            widgetModel.valueChanged(propertyValue);
          }
          break;
        }
        case "options":{
          element.options = JSON.parse(propertyValue);
          break;
        }
          case "validateRequired":{
          element.setAttribute("required","");
          break;
        }
        default:{
          element[propertyChanged] = propertyValue;
        }
      }
    }
  }
};