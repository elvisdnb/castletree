uuxLinearProgressStaging = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.

    parentNode.innerHTML = "<uwc-linear-progress progress='"+widgetModel.labelText+"'></uwc-linear-progress>";
    var element = parentNode.lastElementChild;
    element.progress = widgetModel.progress;
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element){
      if(propertyChanged==="progress"){
        element.progress = propertyValue;
      }
    }
  }
};