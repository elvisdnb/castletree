define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onBeginEditing defined for txtFloatText **/
    AS_TextField_gdfd6aef8724457197b91d8d1da7d798: function AS_TextField_gdfd6aef8724457197b91d8d1da7d798(eventobject, changedtext) {
        var self = this;
        return self.animateComponent.call(this);
    },
    /** onEndEditing defined for txtFloatText **/
    AS_TextField_fc9db36f4459428bbeedc556a01cb3d3: function AS_TextField_fc9db36f4459428bbeedc556a01cb3d3(eventobject, changedtext) {
        var self = this;
        return self.reverseAnimateComponent.call(this);
    }
});