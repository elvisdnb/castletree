define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_a17c69504bca401d94c89676af814122: function AS_UWI_a17c69504bca401d94c89676af814122(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_d6ec65ff007a4b488f3d880d25fb1bc9: function AS_UWI_d6ec65ff007a4b488f3d880d25fb1bc9(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});