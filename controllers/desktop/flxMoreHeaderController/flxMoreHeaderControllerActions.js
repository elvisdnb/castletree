define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxMoreHeader **/
    AS_FlexContainer_db7214693412436293d629e64223d100: function AS_FlexContainer_db7214693412436293d629e64223d100(eventobject, context) {
        var self = this;
        return self.onQuesSectionClick.call(this, context);
    }
});