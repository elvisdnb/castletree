define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnLogin **/
    AS_Button_c06315740020458b854f2190afcaf9fb: function AS_Button_c06315740020458b854f2190afcaf9fb(eventobject) {
        var self = this;
    },
    /** postShow defined for frmDashboard **/
    AS_Form_e7beed71d30141fbb9e094a4952749f8: function AS_Form_e7beed71d30141fbb9e094a4952749f8(eventobject) {
        var self = this;
        var myValue = kony.store.getItem("demoFlag");
        console.log("demoFlag -> " + myValue);
        if (myValue === null) {
            this.view.getStarted.isVisible = true;
        } else {
            this.view.getStarted.isVisible = false;
        }
    },
    /** preShow defined for frmDashboard **/
    AS_Form_i6dc5c383ac342f7889d0b9d5fceca10: function AS_Form_i6dc5c383ac342f7889d0b9d5fceca10(eventobject) {
        var self = this;
        this.view.help.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.getStarted.isVisible = true;
    },
    /** onRowClick defined for SegA **/
    AS_Segment_ad9c9c5e47f640edabeeea09fcd99501: function AS_Segment_ad9c9c5e47f640edabeeea09fcd99501(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return
    },
    /** onclickEvent defined for custBtnViewclient **/
    AS_TPW_c8139e225e664599b1e0387937220c04: function AS_TPW_c8139e225e664599b1e0387937220c04(eventobject) {
        var self = this;
    },
    /** onclickEvent defined for custProceed **/
    AS_TPW_f937666cd8aa4daebf54a21779a11059: function AS_TPW_f937666cd8aa4daebf54a21779a11059(eventobject) {
        var self = this;
        return
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_b9526842e3234c3e855d4309c59dd505: function AS_UWI_b9526842e3234c3e855d4309c59dd505(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_fb5eadf6e648460e9d3c74f1576952e8: function AS_UWI_fb5eadf6e648460e9d3c74f1576952e8(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    }
});