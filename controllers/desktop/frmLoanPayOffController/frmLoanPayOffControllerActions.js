define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_a47c5b20b2604db1b286d9916959a04c: function AS_UWI_a47c5b20b2604db1b286d9916959a04c(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_bb61b37313204be3833502157fa6d2d9: function AS_UWI_bb61b37313204be3833502157fa6d2d9(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    }
});