define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_gf34c04ce94246f3854345b0e71f3975: function AS_UWI_gf34c04ce94246f3854345b0e71f3975(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_ic0b5f44bf3a4729b845e7850b39b467: function AS_UWI_ic0b5f44bf3a4729b845e7850b39b467(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    }
});