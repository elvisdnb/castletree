define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onclickEvent defined for custProceed **/
    AS_TPW_f937666cd8aa4daebf54a21779a11059: function AS_TPW_f937666cd8aa4daebf54a21779a11059(eventobject) {
        var self = this;
        return
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_a64fb2afbc5c436db0490ee3a7636646: function AS_UWI_a64fb2afbc5c436db0490ee3a7636646(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_f1e6b3c2205a4598a4711462563af3fc: function AS_UWI_f1e6b3c2205a4598a4711462563af3fc(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});