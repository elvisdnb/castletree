define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_c52782fa925f42ce84d1141a49d10d65: function AS_UWI_c52782fa925f42ce84d1141a49d10d65(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_fb1cefdc91dd404ab8b02f8c76e05629: function AS_UWI_fb1cefdc91dd404ab8b02f8c76e05629(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});