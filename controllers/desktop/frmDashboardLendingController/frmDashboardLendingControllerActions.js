define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for btnLogin **/
    AS_Button_f7d6636ce9024e80972538ae6063f036: function AS_Button_f7d6636ce9024e80972538ae6063f036(eventobject) {
        var self = this;
    },
    /** preShow defined for frmDashboardLending **/
    AS_Form_a3eddd965ca54427bfcdec096694e3ac: function AS_Form_a3eddd965ca54427bfcdec096694e3ac(eventobject) {
        var self = this;
        this.view.help.isVisible = false;
        this.view.helpCenter.isVisible = false;
        this.view.getStarted.isVisible = true;
    },
    /** postShow defined for frmDashboardLending **/
    AS_Form_bac481fe2b5749628fc4131dcd664e50: function AS_Form_bac481fe2b5749628fc4131dcd664e50(eventobject) {
        var self = this;
        var myValue = kony.store.getItem("demoFlag");
        console.log("demoFlag -> " + myValue);
        if (myValue === null) {
            this.view.getStarted.isVisible = true;
        } else {
            this.view.getStarted.isVisible = false;
        }
    },
    /** onRowClick defined for SegA **/
    AS_Segment_f06ce1bf41dc42318d6880321792a368: function AS_Segment_f06ce1bf41dc42318d6880321792a368(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return
    },
    /** onKeyUp defined for txtPartialText **/
    AS_TextField_efbc02045cc9435b9277be510f5a2036: function AS_TextField_efbc02045cc9435b9277be510f5a2036(eventobject) {
        var self = this;
        self.searchData.call(this);
    },
    /** onclickEvent defined for custBtnViewclient **/
    AS_TPW_c8139e225e664599b1e0387937220c04: function AS_TPW_c8139e225e664599b1e0387937220c04(eventobject) {
        var self = this;
    },
    /** onclickEvent defined for custProceed **/
    AS_TPW_f937666cd8aa4daebf54a21779a11059: function AS_TPW_f937666cd8aa4daebf54a21779a11059(eventobject) {
        var self = this;
        return
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_c0c31b8b4ebc45e68edd56de07133aef: function AS_UWI_c0c31b8b4ebc45e68edd56de07133aef(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_ga1f56fa6663455ea4f1d6a216c6a166: function AS_UWI_ga1f56fa6663455ea4f1d6a216c6a166(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    }
});