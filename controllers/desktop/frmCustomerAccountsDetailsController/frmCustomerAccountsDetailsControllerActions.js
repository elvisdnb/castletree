define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_b5fdff74b36a4ea69d10cb4a2eee5257: function AS_UWI_b5fdff74b36a4ea69d10cb4a2eee5257(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_be20201bc2ec4a94b104c0c955d5f1ea: function AS_UWI_be20201bc2ec4a94b104c0c955d5f1ea(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    }
});