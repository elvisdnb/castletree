define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_d596066755f845b9b391133a3c31725b: function AS_UWI_d596066755f845b9b391133a3c31725b(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_e20af3cddd0b4c9ca1e2fe1437b0dde7: function AS_UWI_e20af3cddd0b4c9ca1e2fe1437b0dde7(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    }
});