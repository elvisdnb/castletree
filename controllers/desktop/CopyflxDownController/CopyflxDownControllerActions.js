define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for lblTitle **/
    AS_Button_f42c7b756fbe4e39b96d71ba572d6458: function AS_Button_f42c7b756fbe4e39b96d71ba572d6458(eventobject, context) {
        var self = this;
        return self.onBtnClick.call(this, eventobject);
    },
    /** onClick defined for btnUpArrow **/
    AS_Button_j53dfc8b7d164d138b81f33a7be07466: function AS_Button_j53dfc8b7d164d138b81f33a7be07466(eventobject, context) {
        var self = this;
        return self.onSectionClick.call(this, context);
    }
});