define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_dfaf8836f2794372be8ca22cf10d3a61: function AS_UWI_dfaf8836f2794372be8ca22cf10d3a61(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_gfcea63cafcf45a2b255c3654117fc85: function AS_UWI_gfcea63cafcf45a2b255c3654117fc85(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});