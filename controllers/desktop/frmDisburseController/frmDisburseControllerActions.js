define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onTouchEnd defined for imgClose1 **/
    AS_Image_ae9428a3f7d54919b7720ac15c9c7941: function AS_Image_ae9428a3f7d54919b7720ac15c9c7941(eventobject, x, y) {
        var self = this;
        return self.closePayAccountDetails.call(this);
    },
    /** onRowClick defined for segPayinPayout **/
    AS_Segment_cd66c81de52e4de29e68381495b002db: function AS_Segment_cd66c81de52e4de29e68381495b002db(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.getSegRowinfo.call(this);
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_b113ea7d342147d3aed08d12d072c086: function AS_UWI_b113ea7d342147d3aed08d12d072c086(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_b7da5a251fac4ab0a3f9940d5ff973fa: function AS_UWI_b7da5a251fac4ab0a3f9940d5ff973fa(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});