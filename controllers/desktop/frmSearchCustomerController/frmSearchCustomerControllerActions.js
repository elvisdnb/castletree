define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onclickEvent defined for custProceed1 **/
    AS_TPW_f937666cd8aa4daebf54a21779a11059: function AS_TPW_f937666cd8aa4daebf54a21779a11059(eventobject) {
        var self = this;
        return
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_f605ce00bb2c4bed9d800b1d625a8816: function AS_UWI_f605ce00bb2c4bed9d800b1d625a8816(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_g0e79ea551834e2d93d067e6cc4d196b: function AS_UWI_g0e79ea551834e2d93d067e6cc4d196b(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});