define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_fe2d9f3cb5114e7dbe2d8c6113b2ca05: function AS_UWI_fe2d9f3cb5114e7dbe2d8c6113b2ca05(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    },
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_g0dc773a41c94c5f9aaaa0086637ad4d: function AS_UWI_g0dc773a41c94c5f9aaaa0086637ad4d(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    }
});