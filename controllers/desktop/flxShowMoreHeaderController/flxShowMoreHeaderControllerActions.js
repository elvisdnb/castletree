define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClick defined for flxShowMoreHeader **/
    AS_FlexContainer_f4fb312f30db4fb985a099723a15d7af: function AS_FlexContainer_f4fb312f30db4fb985a099723a15d7af(eventobject, context) {
        var self = this;
        return self.onShowMoreSectionClick.call(this, context);
    }
});