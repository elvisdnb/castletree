define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    /** onClickBtnExplorer defined for uuxNavigationRail **/
    AS_UWI_b6da1c9231af46438a5fb2acd9dacea7: function AS_UWI_b6da1c9231af46438a5fb2acd9dacea7(eventobject) {
        var self = this;
        return self.onClickExplorer.call(this);
    },
    /** onClickBtnHelp defined for uuxNavigationRail **/
    AS_UWI_bbd783e212c6498b83b01a8438674b10: function AS_UWI_bbd783e212c6498b83b01a8438674b10(eventobject) {
        var self = this;
        return self.onClickBtnHelp.call(this);
    }
});