/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"id": "id",
		"name": "name",
		"file": "file",
		"modifiedBy": "modifiedBy",
		"createdBy": "createdBy",
		"status": "status",
	};

	Object.freeze(mappings);

	var typings = {
		"id": "number",
		"name": "string",
		"file": "string",
		"modifiedBy": "string",
		"createdBy": "string",
		"status": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"id",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "t24ShareFolder",
		tableName: "fileUpload"
	};

	Object.freeze(config);

	return config;
})