define([], function(){
	var BaseRepository = kony.mvc.Data.BaseRepository;

	//Create the Repository Class
	function customerObjectRepository(modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource) {
		BaseRepository.call(this, modelDefinition, config, defaultAppMode, dataSourceFactory, injectedDataSource);
	};

	//Setting BaseRepository as Parent to this Repository
	customerObjectRepository.prototype = Object.create(BaseRepository.prototype);
	customerObjectRepository.prototype.constructor = customerObjectRepository;

	//For Operation 'getCDMcustomerQuery' with service id 'getCustomerInformation9282'
	customerObjectRepository.prototype.getCDMcustomerQuery = function(params, onCompletion){
		return customerObjectRepository.prototype.customVerb('getCDMcustomerQuery', params, onCompletion);
	};

	//For Operation 'getCustomers2' with service id 'getCustomerInformation4683'
	customerObjectRepository.prototype.getCustomers2 = function(params, onCompletion){
		return customerObjectRepository.prototype.customVerb('getCustomers2', params, onCompletion);
	};

	//For Operation 'getCustomers' with service id 'getCustomers2635'
	customerObjectRepository.prototype.getCustomers = function(params, onCompletion){
		return customerObjectRepository.prototype.customVerb('getCustomers', params, onCompletion);
	};

	return customerObjectRepository;
})