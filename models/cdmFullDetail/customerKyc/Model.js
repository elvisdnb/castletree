/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "customerKyc", "objectService" : "cdmFullDetail"};

    var setterFunctions = {
        customerId: function(val, state) {
            context["field"] = "customerId";
            context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
            state['customerId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        postingRestriction: function(val, state) {
            context["field"] = "postingRestriction";
            context["metadata"] = (objectMetadata ? objectMetadata["postingRestriction"] : null);
            state['postingRestriction'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        amlRisk: function(val, state) {
            context["field"] = "amlRisk";
            context["metadata"] = (objectMetadata ? objectMetadata["amlRisk"] : null);
            state['amlRisk'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        crsJurisdiction: function(val, state) {
            context["field"] = "crsJurisdiction";
            context["metadata"] = (objectMetadata ? objectMetadata["crsJurisdiction"] : null);
            state['crsJurisdiction'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        crsStatus: function(val, state) {
            context["field"] = "crsStatus";
            context["metadata"] = (objectMetadata ? objectMetadata["crsStatus"] : null);
            state['crsStatus'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        factaStatus: function(val, state) {
            context["field"] = "factaStatus";
            context["metadata"] = (objectMetadata ? objectMetadata["factaStatus"] : null);
            state['factaStatus'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };

    //Create the Model Class
    function customerKyc(defaultValues) {
        var privateState = {};
        context["field"] = "customerId";
        context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
        privateState.customerId = defaultValues ?
            (defaultValues["customerId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerId"], context) :
                null) :
            null;

        context["field"] = "postingRestriction";
        context["metadata"] = (objectMetadata ? objectMetadata["postingRestriction"] : null);
        privateState.postingRestriction = defaultValues ?
            (defaultValues["postingRestriction"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["postingRestriction"], context) :
                null) :
            null;

        context["field"] = "amlRisk";
        context["metadata"] = (objectMetadata ? objectMetadata["amlRisk"] : null);
        privateState.amlRisk = defaultValues ?
            (defaultValues["amlRisk"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["amlRisk"], context) :
                null) :
            null;

        context["field"] = "crsJurisdiction";
        context["metadata"] = (objectMetadata ? objectMetadata["crsJurisdiction"] : null);
        privateState.crsJurisdiction = defaultValues ?
            (defaultValues["crsJurisdiction"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["crsJurisdiction"], context) :
                null) :
            null;

        context["field"] = "crsStatus";
        context["metadata"] = (objectMetadata ? objectMetadata["crsStatus"] : null);
        privateState.crsStatus = defaultValues ?
            (defaultValues["crsStatus"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["crsStatus"], context) :
                null) :
            null;

        context["field"] = "factaStatus";
        context["metadata"] = (objectMetadata ? objectMetadata["factaStatus"] : null);
        privateState.factaStatus = defaultValues ?
            (defaultValues["factaStatus"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["factaStatus"], context) :
                null) :
            null;


        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);

        //Defining Getter/Setters
        Object.defineProperties(this, {
            "customerId": {
                get: function() {
                    context["field"] = "customerId";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerId, context);
                },
                set: function(val) {
                    setterFunctions['customerId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "postingRestriction": {
                get: function() {
                    context["field"] = "postingRestriction";
                    context["metadata"] = (objectMetadata ? objectMetadata["postingRestriction"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.postingRestriction, context);
                },
                set: function(val) {
                    setterFunctions['postingRestriction'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "amlRisk": {
                get: function() {
                    context["field"] = "amlRisk";
                    context["metadata"] = (objectMetadata ? objectMetadata["amlRisk"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.amlRisk, context);
                },
                set: function(val) {
                    setterFunctions['amlRisk'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "crsJurisdiction": {
                get: function() {
                    context["field"] = "crsJurisdiction";
                    context["metadata"] = (objectMetadata ? objectMetadata["crsJurisdiction"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.crsJurisdiction, context);
                },
                set: function(val) {
                    setterFunctions['crsJurisdiction'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "crsStatus": {
                get: function() {
                    context["field"] = "crsStatus";
                    context["metadata"] = (objectMetadata ? objectMetadata["crsStatus"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.crsStatus, context);
                },
                set: function(val) {
                    setterFunctions['crsStatus'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "factaStatus": {
                get: function() {
                    context["field"] = "factaStatus";
                    context["metadata"] = (objectMetadata ? objectMetadata["factaStatus"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.factaStatus, context);
                },
                set: function(val) {
                    setterFunctions['factaStatus'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });

        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };

        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.customerId = value ? (value["customerId"] ? value["customerId"] : null) : null;
            privateState.postingRestriction = value ? (value["postingRestriction"] ? value["postingRestriction"] : null) : null;
            privateState.amlRisk = value ? (value["amlRisk"] ? value["amlRisk"] : null) : null;
            privateState.crsJurisdiction = value ? (value["crsJurisdiction"] ? value["crsJurisdiction"] : null) : null;
            privateState.crsStatus = value ? (value["crsStatus"] ? value["crsStatus"] : null) : null;
            privateState.factaStatus = value ? (value["factaStatus"] ? value["factaStatus"] : null) : null;
        };
    }

    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(customerKyc);

    //Create new class level validator object
    BaseModel.Validator.call(customerKyc);

    var registerValidatorBackup = customerKyc.registerValidator;

    customerKyc.registerValidator = function() {
        var propName = arguments[0];
        if(!setterFunctions[propName].changed) {
            var setterBackup = setterFunctions[propName];
            setterFunctions[arguments[0]] = function() {
                if(customerKyc.isValid(this, propName, val)) {
                    return setterBackup.apply(null, arguments);
                } else {
                    throw Error("Validation failed for " + propName + " : " + val);
                }
            }
            setterFunctions[arguments[0]].changed = true;
        }
        return registerValidatorBackup.apply(null, arguments);
    }

    //Extending Model for custom operations
    //For Operation 'getCKYC' with service id 'getCustomerReportingStatus8128'
     customerKyc.getCKYC = function(params, onCompletion){
        return customerKyc.customVerb('getCKYC', params, onCompletion);
     };

    var relations = [];

    customerKyc.relations = relations;

    customerKyc.prototype.isValid = function() {
        return customerKyc.isValid(this);
    };

    customerKyc.prototype.objModelName = "customerKyc";

    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    customerKyc.registerProcessors = function(options, successCallback, failureCallback) {

        if(!options) {
            options = {};
        }

        if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }

        if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }

        kony.mvc.util.ProcessorUtils.getMetadataForObject("cdmFullDetail", "customerKyc", options, metaDataSuccess, metaDataFailure);
    };

    //clone the object provided in argument.
    customerKyc.clone = function(objectToClone) {
        var clonedObj = new customerKyc();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };

    return customerKyc;
});