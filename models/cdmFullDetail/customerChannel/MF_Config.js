/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"customerId": "customerId",
		"isInternetBanking": "isInternetBanking",
		"isMobileBanking": "isMobileBanking",
		"isSecureChannel": "isSecureChannel",
	};

	Object.freeze(mappings);

	var typings = {
		"customerId": "string",
		"isInternetBanking": "string",
		"isMobileBanking": "string",
		"isSecureChannel": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"customerId",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "cdmFullDetail",
		tableName: "customerChannel"
	};

	Object.freeze(config);

	return config;
})