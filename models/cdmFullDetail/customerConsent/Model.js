/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "customerConsent", "objectService" : "cdmFullDetail"};

    var setterFunctions = {
        customerId: function(val, state) {
            context["field"] = "customerId";
            context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
            state['customerId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        isDirectMarketing: function(val, state) {
            context["field"] = "isDirectMarketing";
            context["metadata"] = (objectMetadata ? objectMetadata["isDirectMarketing"] : null);
            state['isDirectMarketing'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        isCreditCheck: function(val, state) {
            context["field"] = "isCreditCheck";
            context["metadata"] = (objectMetadata ? objectMetadata["isCreditCheck"] : null);
            state['isCreditCheck'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        arrangementId: function(val, state) {
            context["field"] = "arrangementId";
            context["metadata"] = (objectMetadata ? objectMetadata["arrangementId"] : null);
            state['arrangementId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        isAuthorised: function(val, state) {
            context["field"] = "isAuthorised";
            context["metadata"] = (objectMetadata ? objectMetadata["isAuthorised"] : null);
            state['isAuthorised'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        consentTypes: function(val, state) {
            context["field"] = "consentTypes";
            context["metadata"] = (objectMetadata ? objectMetadata["consentTypes"] : null);
            state['consentTypes'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        isConsentGiven: function(val, state) {
            context["field"] = "isConsentGiven";
            context["metadata"] = (objectMetadata ? objectMetadata["isConsentGiven"] : null);
            state['isConsentGiven'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        consentType: function(val, state) {
            context["field"] = "consentType";
            context["metadata"] = (objectMetadata ? objectMetadata["consentType"] : null);
            state['consentType'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };

    //Create the Model Class
    function customerConsent(defaultValues) {
        var privateState = {};
        context["field"] = "customerId";
        context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
        privateState.customerId = defaultValues ?
            (defaultValues["customerId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerId"], context) :
                null) :
            null;

        context["field"] = "isDirectMarketing";
        context["metadata"] = (objectMetadata ? objectMetadata["isDirectMarketing"] : null);
        privateState.isDirectMarketing = defaultValues ?
            (defaultValues["isDirectMarketing"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isDirectMarketing"], context) :
                null) :
            null;

        context["field"] = "isCreditCheck";
        context["metadata"] = (objectMetadata ? objectMetadata["isCreditCheck"] : null);
        privateState.isCreditCheck = defaultValues ?
            (defaultValues["isCreditCheck"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isCreditCheck"], context) :
                null) :
            null;

        context["field"] = "arrangementId";
        context["metadata"] = (objectMetadata ? objectMetadata["arrangementId"] : null);
        privateState.arrangementId = defaultValues ?
            (defaultValues["arrangementId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["arrangementId"], context) :
                null) :
            null;

        context["field"] = "isAuthorised";
        context["metadata"] = (objectMetadata ? objectMetadata["isAuthorised"] : null);
        privateState.isAuthorised = defaultValues ?
            (defaultValues["isAuthorised"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isAuthorised"], context) :
                null) :
            null;

        context["field"] = "consentTypes";
        context["metadata"] = (objectMetadata ? objectMetadata["consentTypes"] : null);
        privateState.consentTypes = defaultValues ?
            (defaultValues["consentTypes"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["consentTypes"], context) :
                null) :
            null;

        context["field"] = "isConsentGiven";
        context["metadata"] = (objectMetadata ? objectMetadata["isConsentGiven"] : null);
        privateState.isConsentGiven = defaultValues ?
            (defaultValues["isConsentGiven"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["isConsentGiven"], context) :
                null) :
            null;

        context["field"] = "consentType";
        context["metadata"] = (objectMetadata ? objectMetadata["consentType"] : null);
        privateState.consentType = defaultValues ?
            (defaultValues["consentType"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["consentType"], context) :
                null) :
            null;


        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);

        //Defining Getter/Setters
        Object.defineProperties(this, {
            "customerId": {
                get: function() {
                    context["field"] = "customerId";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerId, context);
                },
                set: function(val) {
                    setterFunctions['customerId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "isDirectMarketing": {
                get: function() {
                    context["field"] = "isDirectMarketing";
                    context["metadata"] = (objectMetadata ? objectMetadata["isDirectMarketing"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isDirectMarketing, context);
                },
                set: function(val) {
                    setterFunctions['isDirectMarketing'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "isCreditCheck": {
                get: function() {
                    context["field"] = "isCreditCheck";
                    context["metadata"] = (objectMetadata ? objectMetadata["isCreditCheck"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isCreditCheck, context);
                },
                set: function(val) {
                    setterFunctions['isCreditCheck'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "arrangementId": {
                get: function() {
                    context["field"] = "arrangementId";
                    context["metadata"] = (objectMetadata ? objectMetadata["arrangementId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.arrangementId, context);
                },
                set: function(val) {
                    setterFunctions['arrangementId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "isAuthorised": {
                get: function() {
                    context["field"] = "isAuthorised";
                    context["metadata"] = (objectMetadata ? objectMetadata["isAuthorised"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isAuthorised, context);
                },
                set: function(val) {
                    setterFunctions['isAuthorised'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "consentTypes": {
                get: function() {
                    context["field"] = "consentTypes";
                    context["metadata"] = (objectMetadata ? objectMetadata["consentTypes"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.consentTypes, context);
                },
                set: function(val) {
                    setterFunctions['consentTypes'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "isConsentGiven": {
                get: function() {
                    context["field"] = "isConsentGiven";
                    context["metadata"] = (objectMetadata ? objectMetadata["isConsentGiven"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.isConsentGiven, context);
                },
                set: function(val) {
                    setterFunctions['isConsentGiven'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "consentType": {
                get: function() {
                    context["field"] = "consentType";
                    context["metadata"] = (objectMetadata ? objectMetadata["consentType"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.consentType, context);
                },
                set: function(val) {
                    setterFunctions['consentType'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });

        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };

        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.customerId = value ? (value["customerId"] ? value["customerId"] : null) : null;
            privateState.isDirectMarketing = value ? (value["isDirectMarketing"] ? value["isDirectMarketing"] : null) : null;
            privateState.isCreditCheck = value ? (value["isCreditCheck"] ? value["isCreditCheck"] : null) : null;
            privateState.arrangementId = value ? (value["arrangementId"] ? value["arrangementId"] : null) : null;
            privateState.isAuthorised = value ? (value["isAuthorised"] ? value["isAuthorised"] : null) : null;
            privateState.consentTypes = value ? (value["consentTypes"] ? value["consentTypes"] : null) : null;
            privateState.isConsentGiven = value ? (value["isConsentGiven"] ? value["isConsentGiven"] : null) : null;
            privateState.consentType = value ? (value["consentType"] ? value["consentType"] : null) : null;
        };
    }

    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(customerConsent);

    //Create new class level validator object
    BaseModel.Validator.call(customerConsent);

    var registerValidatorBackup = customerConsent.registerValidator;

    customerConsent.registerValidator = function() {
        var propName = arguments[0];
        if(!setterFunctions[propName].changed) {
            var setterBackup = setterFunctions[propName];
            setterFunctions[arguments[0]] = function() {
                if(customerConsent.isValid(this, propName, val)) {
                    return setterBackup.apply(null, arguments);
                } else {
                    throw Error("Validation failed for " + propName + " : " + val);
                }
            }
            setterFunctions[arguments[0]].changed = true;
        }
        return registerValidatorBackup.apply(null, arguments);
    }

    //Extending Model for custom operations
    //For Operation 'getCConsent' with service id 'getCustomerPersonalConsents6928'
     customerConsent.getCConsent = function(params, onCompletion){
        return customerConsent.customVerb('getCConsent', params, onCompletion);
     };

    //For Operation 'getCustomerConsent' with service id 'getCustomerPersonalConsents7295'
     customerConsent.getCustomerConsent = function(params, onCompletion){
        return customerConsent.customVerb('getCustomerConsent', params, onCompletion);
     };

    var relations = [];

    customerConsent.relations = relations;

    customerConsent.prototype.isValid = function() {
        return customerConsent.isValid(this);
    };

    customerConsent.prototype.objModelName = "customerConsent";

    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    customerConsent.registerProcessors = function(options, successCallback, failureCallback) {

        if(!options) {
            options = {};
        }

        if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }

        if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }

        kony.mvc.util.ProcessorUtils.getMetadataForObject("cdmFullDetail", "customerConsent", options, metaDataSuccess, metaDataFailure);
    };

    //clone the object provided in argument.
    customerConsent.clone = function(objectToClone) {
        var clonedObj = new customerConsent();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };

    return customerConsent;
});