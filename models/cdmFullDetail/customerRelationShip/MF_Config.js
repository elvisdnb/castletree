/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"customerId": "customerId",
		"relationshipDetails": "relationshipDetails",
		"relatedAs": "relatedAs",
		"relatedPartyName": "relatedPartyName",
		"relationPartyId": "relationPartyId",
	};

	Object.freeze(mappings);

	var typings = {
		"customerId": "string",
		"relationshipDetails": "string",
		"relatedAs": "string",
		"relatedPartyName": "string",
		"relationPartyId": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"customerId",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "cdmFullDetail",
		tableName: "customerRelationShip"
	};

	Object.freeze(config);

	return config;
})