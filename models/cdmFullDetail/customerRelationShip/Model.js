/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "customerRelationShip", "objectService" : "cdmFullDetail"};

    var setterFunctions = {
        customerId: function(val, state) {
            context["field"] = "customerId";
            context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
            state['customerId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        relationshipDetails: function(val, state) {
            context["field"] = "relationshipDetails";
            context["metadata"] = (objectMetadata ? objectMetadata["relationshipDetails"] : null);
            state['relationshipDetails'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        relatedAs: function(val, state) {
            context["field"] = "relatedAs";
            context["metadata"] = (objectMetadata ? objectMetadata["relatedAs"] : null);
            state['relatedAs'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        relatedPartyName: function(val, state) {
            context["field"] = "relatedPartyName";
            context["metadata"] = (objectMetadata ? objectMetadata["relatedPartyName"] : null);
            state['relatedPartyName'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        relationPartyId: function(val, state) {
            context["field"] = "relationPartyId";
            context["metadata"] = (objectMetadata ? objectMetadata["relationPartyId"] : null);
            state['relationPartyId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };

    //Create the Model Class
    function customerRelationShip(defaultValues) {
        var privateState = {};
        context["field"] = "customerId";
        context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
        privateState.customerId = defaultValues ?
            (defaultValues["customerId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["customerId"], context) :
                null) :
            null;

        context["field"] = "relationshipDetails";
        context["metadata"] = (objectMetadata ? objectMetadata["relationshipDetails"] : null);
        privateState.relationshipDetails = defaultValues ?
            (defaultValues["relationshipDetails"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["relationshipDetails"], context) :
                null) :
            null;

        context["field"] = "relatedAs";
        context["metadata"] = (objectMetadata ? objectMetadata["relatedAs"] : null);
        privateState.relatedAs = defaultValues ?
            (defaultValues["relatedAs"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["relatedAs"], context) :
                null) :
            null;

        context["field"] = "relatedPartyName";
        context["metadata"] = (objectMetadata ? objectMetadata["relatedPartyName"] : null);
        privateState.relatedPartyName = defaultValues ?
            (defaultValues["relatedPartyName"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["relatedPartyName"], context) :
                null) :
            null;

        context["field"] = "relationPartyId";
        context["metadata"] = (objectMetadata ? objectMetadata["relationPartyId"] : null);
        privateState.relationPartyId = defaultValues ?
            (defaultValues["relationPartyId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["relationPartyId"], context) :
                null) :
            null;


        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);

        //Defining Getter/Setters
        Object.defineProperties(this, {
            "customerId": {
                get: function() {
                    context["field"] = "customerId";
                    context["metadata"] = (objectMetadata ? objectMetadata["customerId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.customerId, context);
                },
                set: function(val) {
                    setterFunctions['customerId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "relationshipDetails": {
                get: function() {
                    context["field"] = "relationshipDetails";
                    context["metadata"] = (objectMetadata ? objectMetadata["relationshipDetails"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.relationshipDetails, context);
                },
                set: function(val) {
                    setterFunctions['relationshipDetails'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "relatedAs": {
                get: function() {
                    context["field"] = "relatedAs";
                    context["metadata"] = (objectMetadata ? objectMetadata["relatedAs"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.relatedAs, context);
                },
                set: function(val) {
                    setterFunctions['relatedAs'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "relatedPartyName": {
                get: function() {
                    context["field"] = "relatedPartyName";
                    context["metadata"] = (objectMetadata ? objectMetadata["relatedPartyName"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.relatedPartyName, context);
                },
                set: function(val) {
                    setterFunctions['relatedPartyName'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "relationPartyId": {
                get: function() {
                    context["field"] = "relationPartyId";
                    context["metadata"] = (objectMetadata ? objectMetadata["relationPartyId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.relationPartyId, context);
                },
                set: function(val) {
                    setterFunctions['relationPartyId'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });

        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };

        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.customerId = value ? (value["customerId"] ? value["customerId"] : null) : null;
            privateState.relationshipDetails = value ? (value["relationshipDetails"] ? value["relationshipDetails"] : null) : null;
            privateState.relatedAs = value ? (value["relatedAs"] ? value["relatedAs"] : null) : null;
            privateState.relatedPartyName = value ? (value["relatedPartyName"] ? value["relatedPartyName"] : null) : null;
            privateState.relationPartyId = value ? (value["relationPartyId"] ? value["relationPartyId"] : null) : null;
        };
    }

    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(customerRelationShip);

    //Create new class level validator object
    BaseModel.Validator.call(customerRelationShip);

    var registerValidatorBackup = customerRelationShip.registerValidator;

    customerRelationShip.registerValidator = function() {
        var propName = arguments[0];
        if(!setterFunctions[propName].changed) {
            var setterBackup = setterFunctions[propName];
            setterFunctions[arguments[0]] = function() {
                if(customerRelationShip.isValid(this, propName, val)) {
                    return setterBackup.apply(null, arguments);
                } else {
                    throw Error("Validation failed for " + propName + " : " + val);
                }
            }
            setterFunctions[arguments[0]].changed = true;
        }
        return registerValidatorBackup.apply(null, arguments);
    }

    //Extending Model for custom operations
    //For Operation 'getCustomerRelationShip' with service id 'getPartyRelationship5661'
     customerRelationShip.getCustomerRelationShip = function(params, onCompletion){
        return customerRelationShip.customVerb('getCustomerRelationShip', params, onCompletion);
     };

    //For Operation 'getCDMCustomerRelationShip' with service id 'getPartyRelationship1918'
     customerRelationShip.getCDMCustomerRelationShip = function(params, onCompletion){
        return customerRelationShip.customVerb('getCDMCustomerRelationShip', params, onCompletion);
     };

    var relations = [];

    customerRelationShip.relations = relations;

    customerRelationShip.prototype.isValid = function() {
        return customerRelationShip.isValid(this);
    };

    customerRelationShip.prototype.objModelName = "customerRelationShip";

    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    customerRelationShip.registerProcessors = function(options, successCallback, failureCallback) {

        if(!options) {
            options = {};
        }

        if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }

        if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }

        kony.mvc.util.ProcessorUtils.getMetadataForObject("cdmFullDetail", "customerRelationShip", options, metaDataSuccess, metaDataFailure);
    };

    //clone the object provided in argument.
    customerRelationShip.clone = function(objectToClone) {
        var clonedObj = new customerRelationShip();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };

    return customerRelationShip;
});