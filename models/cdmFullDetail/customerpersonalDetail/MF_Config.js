/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
	var mappings = {
		"customerId": "customerId",
		"isInternetBanking": "isInternetBanking",
		"isMobileBanking": "isMobileBanking",
		"isSecureMessage": "isSecureMessage",
	};

	Object.freeze(mappings);

	var typings = {
		"customerId": "string",
		"isInternetBanking": "string",
		"isMobileBanking": "string",
		"isSecureMessage": "string",
	}

	Object.freeze(typings);

	var primaryKeys = [
					"customerId",
	];

	Object.freeze(primaryKeys);

	var config = {
		mappings: mappings,
		typings: typings,
		primaryKeys: primaryKeys,
		serviceName: "cdmFullDetail",
		tableName: "customerpersonalDetail"
	};

	Object.freeze(config);

	return config;
})