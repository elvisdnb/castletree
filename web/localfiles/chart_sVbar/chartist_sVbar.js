/**
 * Created by Team Kony.
 * Copyright (c) 2017 Kony Inc. All rights reserved.
 */
konymp = {};
konymp.charts = konymp.charts || {};

konymp.charts.sVbar = function(){
  
};

konymp.charts.sVbar.prototype.createClass = function(name, rules) {
  	var style = document.createElement('style');
    style.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(style);
    if(!(style.sheet||{}).insertRule) 
        (style.styleSheet || style.sheet).addRule(name, rules);
    else
        style.sheet.insertRule(name+"{"+rules+"}",0);
};

konymp.charts.sVbar.prototype.createVbarChartS_UI = function(labels, series, colors, properties) {
  	var myNode = document.getElementById("legends");
  	while (myNode.firstChild) {
    	myNode.removeChild(myNode.firstChild);
  	}
  	var pluginSet = [];
  	pluginSet.push(Chartist.plugins.ctAxisTitle({
    	axisX: {
        	axisTitle: properties._xAxisTitle,
        	axisClass: 'ct-axis-title',
        	offset: {
          		x: 0,
          		y: 15
       		},
        	textAnchor: 'middle'
      	},
      	axisY: {
        	axisTitle: properties._yAxisTitle,
        	axisClass: 'ct-axis-title',
        	offset: {
          		x: 0,
          		y: 13.5
        	},
        	textAnchor: 'middle',
        	flipTitle: true
      	}
    }));
  	if(properties._enableLegends) {
    	pluginSet.push(Chartist.plugins.legend({
        	horizontalAlign: "right",
        	clickable: false,
        	position: document.getElementById('legends')
      	}));
    }
	var chart = new Chartist.Bar('#chart', {
    	labels: labels,
    	series: series
  	}, {
      	stackBars: true,
    	axisX: {
      		showLabel: true,
      		showGrid: false,
    	},
    	axisY: {
      		showLabel: true,
      		showGrid: false,
    	},
    	low: parseFloat(properties._lowValue),
  		high: parseFloat(properties._highValue),
    	showArea: false,
      	plugins: pluginSet,
       	chartPadding: { 	  
  			right: 10
 		}
  	});
  	
  	var seq = 0, delay = 80, duration = 300;
 	
  	chart.on('created', function() {
      	seq = 0;
    });
  	chart.on('draw', function(context) {
      	if(!properties._enableGrid) {
          	if(context.type === 'grid' && context.index !== 0) {
      			context.element.remove();
    		} 
        }     
      	if(properties._enableGridAnimation===true && properties._enableGrid===true) {
	      	seq++;
		}
      	if(!properties._enableChartAnimation) {
          return;
        }
    	if(context.type === 'bar') {
          	if(properties._enableGridAnimation === false) {
              	seq++;
            }
          	context.element.attr({
        		style: 'stroke-width: 0px;'
      		});
      		var strokeWidth = 12;
      		context.element.animate({
        		y2: {
          			begin: duration + (seq*duration)/3,
          			dur: duration,
          			from: context.y1,
          			to: context.y2,
          			easing: Chartist.Svg.Easing.easeOutSine
        		},
        		'stroke-width': {
          			begin:  duration + (seq*duration)/3,
          			dur: 1,
          			from: 0,
          			to: strokeWidth,
          			fill: 'freeze'
        		}
      		}, false);	
    	}
      	 if(properties._enableGrid===true && properties._enableGridAnimation === true && context.type === 'grid') {
    		var pos1Animation = {
      			begin: seq * delay,
      			dur: duration,
      			from: context[context.axis.units.pos + '1'] - 30,
      			to: context[context.axis.units.pos + '1'],
      			easing: 'easeOutQuart'
    		};
    		var pos2Animation = {
      			begin: seq * delay,
      			dur: duration,
      			from: context[context.axis.units.pos + '2'] - 100,
      			to: context[context.axis.units.pos + '2'],
      			easing: 'easeOutQuart'
    		};
    		var animations = {};
    		animations[context.axis.units.pos + '1'] = pos1Animation;
    		animations[context.axis.units.pos + '2'] = pos2Animation;
    		animations['opacity'] = {
      			begin: seq * delay,
        		dur: duration,
      			from: 0,
      			to: 1,
      			easing: 'easeOutQuart'
    		};
    		context.element.animate(animations);
  		}
    });
  chart.on('created', function() {
  		if(window.__exampleAnimateTimeout) {
    		clearTimeout(window.__exampleAnimateTimeout);
    		window.__exampleAnimateTimeout = null;
  		} 	
	});
};

konymp.charts.sVbar.prototype.Updatecss = function(colors, properties){
	var regColorcode = /^(#)?([0-9a-fA-F]{3})([0-9a-fA-F]{3})?$/;
  	try {
      	for(var i in colors) {
          	if(colors[i] !== "" && regColorcode.test(colors[i])) {
              	this.createClass('.ct-legend .ct-series-'+i,'font-family:Arial, Helvetica, sans-serif; color:'+ properties._legendFontColor+'; font-size:'+ properties._legendFontSize + ';');
              	this.createClass('.ct-legend .ct-series-'+i+':before',"  background-color:"+colors[i]+"; border-color:"+colors[i]+";");
              	var _char = String.fromCharCode(parseInt(97 + Number(i)));
              	this.createClass('.ct-series-' + _char + ' .ct-bar', " stroke: " + colors[i] + ";");
            }
          	else {
              	throw {"Error": "InvalidColorCode", "message": "Color code for bars should be in hex format. Eg.:#000000"};
            }
        }
    }
  	catch(exception) {
      	if(exception.Error === "InvalidColorCode") {
          	throw(exception);
        }
    }
};

konymp.charts.sVbar.prototype.Generate_sVbarChart = function(title, labels, data, colors, properties) {
	if(document.readyState === "complete") {
    	document.ontouchmove = function(e) {
  			e.preventDefault();
		};
   		document.getElementById('lblTitle').style.color = properties._titleFontColor || '#000000';
   		document.getElementById('lblTitle').style.fontSize = properties._titleFontSize !== undefined ? parseInt(properties._titleFontSize)*10+'%' : '120%';
   		document.getElementById('lblTitle').style.fontFamily = 'Arial, Helvetica, sans-serif';
   		document.getElementById('lblTitle').innerHTML = title;
   		document.body.style.backgroundColor = properties._bgColor || '#FFFFFF'; 
      	this.Updatecss(colors, properties);
    	this.createVbarChartS_UI(labels, data, colors, properties);
    	return true;
  	}
  	else {
    	return false;
  	}
};

window.onload = function() {
   	console.log(navigator.userAgent);
  	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    	return true;
	}
  	var x = new konymp.charts.sVbar();
   	var data = [
                 {"colorCode": "#053E75", "label": "Branch"},
                 {"colorCode": "#727BF7", "label": "Internet Banking"},
                 {"colorCode": "#727BF7", "label": "Mobile Banking"},
				 {"colorCode": "#053E75", "label": "Chatbot"},
				 {"colorCode": "#053E75", "label": "Email"},
               ];
			   
		var y = new konymp.charts.sVbar();
   	var data = [
                 {"colorCode": "#053E75", "label": "Branch"},
                 {"colorCode": "#727BF7", "label": "Internet Banking"},
                 {"colorCode": "#727BF7", "label": "Mobile Banking"},
				 {"colorCode": "#053E75", "label": "Chatbot"},
				 {"colorCode": "#053E75", "label": "Email"},
               ];		   
  	var series = [
      {name: "Potential Customers", data: [5, 1, 4, 3],"colorCode": "#727BF7"},
      {name: "Existing Custoemrs", data: [2, 4, 5, 3],"colorCode": "#053E75"},
     // {name: "orange", data: [1, 2, 4, 6]}
  	];
    var labels = data.map(function(obj){
    	return obj.label;
  	});
  	//labels.push("data4");
   	var colors = data.map(function(obj){
    	return obj.colorCode;
    });
  	var properties = {
      	_titleFontSize: 12,
        _titleFontColor: "#000000",
        _bgColor: "#fff",
        _lowValue: 0,
        _highValue: 15,
      	_xAxisTitle: '',
      	_yAxisTitle: '',
      	_enableGrid: true,
        _enableGridAnimation: false,
        _enableChartAnimation: true,
        _enableLegends: true,
      	_legendFontColor: "#000000",
      	_legendFontSize: "95%",
      	_enableStaticPreview: true
    };
  	x.Generate_sVbarChart("Weekly Visitors", labels, series, colors, properties);
};