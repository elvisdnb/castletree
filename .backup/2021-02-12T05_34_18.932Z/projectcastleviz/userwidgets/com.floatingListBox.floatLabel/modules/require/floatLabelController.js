define(function() {

  return {
    constructor:function(baseConfig, layoutConfig, pspConfig){
    //this.view.floatLabel.shadowDepth = 4;
    },
    
    defaultSkin: function() {
      this.view.lblFloat.centerY="50%";
      this.view.lblFloat.skin="lblSknReverseAnimate";
    },
    
    animateComponent:function(){
      if(this.view.lblFloat.centerY!=="25%"){
        //   this.view.lblFloat.isVisible=true;
        this.view.txtFloatLabel.opacity="100%";
        this.view.lblFloat.skin="lblAnimate";
        //this.view.txtFloatLabel.placeholder="";
        this.view.lblFloat.animate(
          kony.ui.createAnimation({
            "100":{
              centerY:"25%",
              "stepConfig": {
                "timingFunction": kony.anim.EASE
              }
            }
          }), {
            "delay":0,
            "iterationCount":1,
            "duration":0.5,
            "fillMode": kony.anim.FILL_MODE_FORWARDS
          }, {
            "animationEnd":function() {
              kony.print("****Animation End****");
            }
          });
      }
    },
    reverseAnimateComponent:function(){
      try{
        var txt=this.view.txtFloatLabel.text;
        if(txt.length===0){
          this.reverseAnimateComponentUtil();
        }
      }catch(e){
        this.reverseAnimateComponentUtil();
      }
    },
    
    reverseAnimateComponentUtil:function(){
      var txtLabel = this.view.txtFloatLabel;
      if(txtLabel!==null){
        this.view.txtFloatLabel.text="";
        var txt = txtLabel.text;
        if(kony.string.equalsIgnoreCase(txt,"")){
          this.view.lblFloat.animate(
            kony.ui.createAnimation({
              "100":{
                centerY:"50%",
                "stepConfig": {
                  "timingFunction": kony.anim.EASE
                }
              }
            }), {
              "delay":0,
              "iterationCount":1,
              "duration":0.5,
              "fillMode": kony.anim.FILL_MODE_FORWARDS
            }, {
              "animationEnd":function() {
                kony.print("****Animation End****");
              }
            });

          this.view.lblFloat.skin="lblSknReverseAnimate";
        }
      }

    }
  };
});