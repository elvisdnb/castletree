define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for txtFloatLabel **/
    AS_TextField_c87ef6df7c1d4c27b663447b63edf242: function AS_TextField_c87ef6df7c1d4c27b663447b63edf242(eventobject, x, y) {
        var self = this;
        return self.animateComponent.call(this);
    },
    /** onEndEditing defined for txtFloatLabel **/
    AS_TextField_a76b3125a9e04bbb8a440945d7d7f8e7: function AS_TextField_a76b3125a9e04bbb8a440945d7d7f8e7(eventobject, changedtext) {
        var self = this;
        if (this.view.txtFloatLabel === null) this.reverseAnimateComponent();
        if (this.view.txtFloatLabel !== null && kony.string.equalsIgnoreCase(this.view.txtFloatLabel.text, "")) {
            this.reverseAnimateComponent();
        }
    }
});