define({ 

  onLoadFunctions : function(){
    
    
   // this.getCustomerDetails();
    this.getLoansDetails();
    //this.getholidaysList();
   // this.view.segSkipPayments.onRowClick = this.segSkipPayOnRowClick;
  },
  getCustomerDetails : function()

  {
    var self=this;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getCustomer";
    var params = {
      "customerId": "500009",
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      //alert("Integration Service Response is: " + JSON.stringify(response));
      var customerDetails = response.body[0];
      self.view.lblValResidence.text =customerDetails.residenceName;
      self.view.lblHd.text =customerDetails.customerName;
      self.view.lblHdr.text =customerDetails.customerId;
      self.view.lblRelated.text =customerDetails.nationalityName;
      self.view.lblEmpStatus.text =customerDetails.employmentDetails[0].employmentStatus;
      self.view.lblProfiles.text =customerDetails.profileTypes[0].profileTypeName;
      self.view.lblValAccountOfficer.text =customerDetails.accountOfficerName;
      self.view.lblDOB.text =customerDetails.dateOfBirth;
      self.view.lblValEmail.text =customerDetails.contactDetails[0].emails[0].email;
      self.view.lblValPhone.text =customerDetails.contactDetails[0].mobilePhoneNumbers[0].mobilePhoneNumber;
      //self.view.lblValEmployerName.text =customerDetails.employmentDetails[0].employerName;
     //self.view.lblValEmployerName.text =customerDetails.employmentDetails[0].employerName;
      //self.view.lblValEmployerName.text =customerDetails.nationalityName; 

    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },

  getLoansDetails : function() 
  {
    var self=this;

    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getFullLoansDetails";
    var params = {
      "customerId": "500009"
//       "loanAccountId" : "50003191"
    };

    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
    //alert("Integration Service Response is: " + JSON.stringify(response));
      
var combData = [];

      
  var dataMap = {
    "lblHeaderAN":"lblHeaderAN",
    "lblHeaderAccountType":"lblHeaderAccountType",
    "lblCCY":"lblCCY",
    "lblClearedBalance":"lblClearedBalance",
    "lblAvailableLimit":"lblAvailableLimit",
    "lblHdr":"lblHdr",  
    "lblLoanAccountId":"lblLoanAccountId",
    "lblArrangementId":"lblArrangementId"
  };
  self.view.segMin.widgetDataMap = dataMap;
    
  for(var i = 0; i<response.body.length;i++){
    //var loanbalance = response.body[i].loanBalance.toString().replace('-','');
    
     var data = {
    "lblHeaderAN":response.body[i].loanAccountId,
    "lblHeaderAccountType":response.body[i].productName,
    "lblCCY":response.body[i].loanCurrency,
    "lblClearedBalance":self.formatAmount(self.removeMinus(response.body[i].loanBalance)),
    "lblAvailableLimit":response.body[i].loanStartDate,
    "lblHdr":response.body[i].loanEndDate,
    "lblLoanAccountId":response.body[i].loanAccountId,
    "lblArrangementId":response.body[i].arrangementId
  };
    combData.push(data);
  }
    self.view.segMin.setData(combData);
    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },
  
  removeMinus : function(x){
    return Math.abs(x);  
},
  
  onLoanSegmentClick : function(){
    rowInfo=this.view.segMin.selectedRowItems[0];  
    this.getCustomerLoan(rowInfo);
    this.getPaymentDate(rowInfo);
 },
  
  getCustomerLoan : function(rowInfo)

  {
    var self=this;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getLoans";
    var params = {
      "customerId": "500009",
      "loanAccountId" : rowInfo.lblLoanAccountId,
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      //alert("Integration Service Response is: " + JSON.stringify(response));
      var customerLoanDetails = response.body[0];
      self.view.lblEur.text =customerLoanDetails.loanCurrency + " " +  self.formatAmount(customerLoanDetails.loanAmount);

    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },
  
  getPaymentDate : function(rowInfo)

  {
    var self=this;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getNextPaymentDate";
    var params = {
      "loanAccountId" : rowInfo.lblLoanAccountId,
      "arrangementId" : rowInfo.lblArrangementId
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      //alert("Integration Service Response is: " + JSON.stringify(response));
      var customerLoanDetails = response.body[0];
      self.view.lblNextPaymentDate.text = customerLoanDetails.nextPaymentDate;
	self.view.lblNextMaturityDate.text = customerLoanDetails.maturityDate;
    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },
  
  getholidaysList : function(){
    var self=this;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getPaymentScheduleDate";
    var params = {
      //"customerId": "500009",
      "loanAccountId": "50003191",
      "arrangementId": "AA2007988FCD",
      "dateFrom": "20200706",
      "dateTo": ""
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {

      var combData = [];
      var dataMap = {
        "lblAmount1":"lblAmount1",
        "lblDate1":"lblDate1",
        "lblArrow":"lblArrow",
      };
      self.view.segSkipPayments.widgetDataMap = dataMap;

      for(var i = 0; i<response.body.length;i++){
        var data = {
          "lblAmount1":response.body[i].principal,
          "lblDate1":response.body[i].date,
          "lblArrow":{skin:"sknlblArrowGrey"},
        };
        combData.push(data);
      }
      self.view.segSkipPayments.setData(combData);

    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },

  segSkipPayOnRowClick : function(){
    var  data = this.view.segSkipPayments.data;
    var  index =this.view.segSkipPayments.selectedRowIndex[1];
    if(data[index].lblArrow.skin  == "sknlblArrowGrey"){
      data[index].lblArrow.skin  = "sknlblArrowGreen";
    }else{
      data[index].lblArrow.skin  = "sknlblArrowGrey";
    }
    this.view.segSkipPayments.setDataAt(data[index], index, 0);
  },
  
  formatAmount : function(amount){
    var num,flag=false;
    if(amount === null || amount === undefined || isNaN(amount)) {
        return;
    }
    amount = Number(amount).toFixed(2);
    var decimal = ".";//this.getDecimalSeparator(locale);
    if(amount.indexOf(".")!=-1||amount.indexOf(",")!=-1)
    {
        if(amount.indexOf(".")!=-1)
        {            
            amount = amount.replace(".",decimal);                
        }
        else if(amount.indexOf(",")!=-1){
            amount = amount.replace(",",decimal);             
        }        
        num = amount.split(decimal)[0];
        var dec = amount.split(decimal)[1];
      if(num.indexOf("-")!=-1)
        {
          num=num.split("-")[1];
          flag=true;
        }
      if(num.length > 3){
        for(var i=num.length-1;i>=0;){

 

          if(i>=3)
          {      
            num=num.substring(0,i-2) + "," + num.substring(i-2,num.length);    
          }
          i=i-3; 
        }          
      }
      if(flag===true)
        {
          return "-"+num + decimal + dec;
        }
        return num + decimal + dec;    
    }
    else{
        return amount;
    }    
}

});