define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_ListBox_c97e7f8e2afe410e824c90ecb88719aa: function AS_ListBox_c97e7f8e2afe410e824c90ecb88719aa(eventobject) {
        var self = this;
        this.view.CopyflxNow0ec67451d2cdf43.setVisibility(false);
        this.view.flxPaymentholidaydetails.setVisibility(true);
    },
    AS_ListBox_e93d745a436541e6ba6ac2aac3f27a8e: function AS_ListBox_e93d745a436541e6ba6ac2aac3f27a8e(eventobject) {
        var self = this;
        this.view.CopyflxNow0ec67451d2cdf43.setVisibility(false);
        this.view.flxPaymentholidaydetails.setVisibility(true);
    },
    AS_Button_i248e0dd88504708b0c3a2d52bee72c8: function AS_Button_i248e0dd88504708b0c3a2d52bee72c8(eventobject) {
        var self = this;
        this.view.flxUpcomingPaymentWrapper.setVisibility(false);
    },
    AS_Button_f384e09862584b52a6d669d07b3960e5: function AS_Button_f384e09862584b52a6d669d07b3960e5(eventobject) {
        var self = this;
        this.view.flxUpcomingPaymentWrapper.setVisibility(false);
    },
    AS_Button_b52103949bd04f66a41b53813a2b4582: function AS_Button_b52103949bd04f66a41b53813a2b4582(eventobject) {
        var self = this;
        this.view.flxpopUpExploreLendingServciesWrapper.setVisibility(false);
    },
    AS_FlexContainer_e08b22bfced64b4ca9b7181df2c463ba: function AS_FlexContainer_e08b22bfced64b4ca9b7181df2c463ba(eventobject, x, y) {
        var self = this;
        this.view.flxPaymentholidaydetails.setVisibility(true);
        this.view.CopyflxNow0ec67451d2cdf43.setVisibility(false);
    },
    AS_FlexContainer_h24dd4fb088346bda1581ad4a17025f7: function AS_FlexContainer_h24dd4fb088346bda1581ad4a17025f7(eventobject, x, y) {
        var self = this;
        this.view.flxPaymentholidaydetails.setVisibility(true);
        this.view.CopyflxNow0ec67451d2cdf43.setVisibility(false);
    },
    AS_Button_a7aabee240a0497984e4831f95d1dfc7: function AS_Button_a7aabee240a0497984e4831f95d1dfc7(eventobject) {
        var self = this;
        this.view.flxpopUpExploreLendingServciesWrapper.setVisibility(false);
    },
    AS_Button_j5d333de41924daeb875c426b8342113: function AS_Button_j5d333de41924daeb875c426b8342113(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        ntf.navigate();
    },
    AS_Button_b1a61bb5710c4bfa987af8d833e0b729: function AS_Button_b1a61bb5710c4bfa987af8d833e0b729(eventobject) {
        var self = this;
        this.view.flxpop1.setVisibility(true);
    },
    AS_Button_abdc5b9a44ad4fc0a94bd2cf6825d711: function AS_Button_abdc5b9a44ad4fc0a94bd2cf6825d711(eventobject) {
        var self = this;
        this.view.flxRevisedPaymentSchedule.setVisibility(true);
    },
    AS_Button_b44399a27cf54f3e9cc2161724cf7226: function AS_Button_b44399a27cf54f3e9cc2161724cf7226(eventobject) {
        var self = this;
        this.view.flxsuccess.setVisibility(true);
    },
    AS_Button_d39a9e4aaa1c49358518ff5987634095: function AS_Button_d39a9e4aaa1c49358518ff5987634095(eventobject) {
        var self = this;
        this.view.flxPaymentHolidaysSuccessfullysWrapper.setVisibility(true);
        this.view.flxUpcomingPaymentWrapper.setVisibility(false);
    },
    AS_Button_ddb129cab65b402a8426329f0811367b: function AS_Button_ddb129cab65b402a8426329f0811367b(eventobject) {
        var self = this;
        this.view.flxpopUpExploreLendingServciesWrapper.setVisibility(true);
        this.view.flxPaymentHolidaysSuccessfullysWrapper.setVisibility(false);
    },
    AS_FlexContainer_d4b99854c84b4a34ae41db7acf017912: function AS_FlexContainer_d4b99854c84b4a34ae41db7acf017912(eventobject) {
        var self = this;
        this.view.flxRevisedPaymentSchedule.setVisibility(true);
    },
    AS_FlexContainer_daa00570501643e3ad9a7fd1c94b38b0: function AS_FlexContainer_daa00570501643e3ad9a7fd1c94b38b0(eventobject) {
        var self = this;
        this.view.flxRevisedPaymentsWrapper.setVisibility(true);
    },
    AS_FlexContainer_c0d2f1b602bf4848852a982883527341: function AS_FlexContainer_c0d2f1b602bf4848852a982883527341(eventobject) {
        var self = this;
        this.view.flxUpcomingPaymentWrapper.setVisibility(true);
    },
    AS_Form_ebd04822663d466f9bcf1c31123fee43: function AS_Form_ebd04822663d466f9bcf1c31123fee43(eventobject) {
        var self = this;
        return self.onLoadFunctions.call(this);
    },
    AS_Segment_db00eed52c504a8fbaa3bc4be82527b6: function AS_Segment_db00eed52c504a8fbaa3bc4be82527b6(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.onLoanSegmentClick();
        this.view.flxPaymentholidaydetails.setVisibility(true);
    },
    AS_Segment_c7ec5bfdb6cf45f59d7a1f0e45750440: function AS_Segment_c7ec5bfdb6cf45f59d7a1f0e45750440(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.segSkipPayOnRowClick.call(this);
    }
});