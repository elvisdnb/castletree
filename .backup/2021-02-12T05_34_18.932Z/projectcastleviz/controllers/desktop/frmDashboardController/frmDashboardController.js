define({ 
  gblCustomerID:null,
  onPostShow : function(){
   this.getActivities();
  },
  
  

  getNavDetails : function(){
   
    var self=this;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getActivities";
    var params = {
    // "customerId": "500009",
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      var segamentData = [];
      var dataMap = {
        "lblGroup":"lblGroup",
        "lblExpires":"lblExpires",
        "lblCid":"lblCid",
        "lblDate":"lblDate",
        "lblInformed":"lblInformed",
        "imgSrc":"imgSrc"
      };
      self.view.SegA.widgetDataMap = dataMap;
      for(var i = 0; i<response.body.length;i++){
        var data = {
          "lblGroup":response.body[i].customerShortName,
          "lblExpires":response.body[i].displayName,
          "lblCid":response.body[i].customerId,
          "lblDate": response.body[i].contactDate.split(" ")[1]+" "+response.body[i].contactDate.split(" ")[0],
          "lblInformed":response.body[i].contactStatus,
          "imgSrc" :{src:self.getImage(response.body[i].contactStatus)}
        };

        segamentData.push(data);
      }
      self.view.SegA.setData(segamentData);
     // self.gblCustomerID=this.view.SegA.selectedRowItems[0].lblCid;
      //self.view.SegA.onRowClick = ;
      //self.view.lblNew.text=lblCid;
      //alert("123"+self.view.lblNew.text+"1234");
    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },
  getImage:function(status){
    if(status == "Pending"){
      return "orange1.png";
    }else if(status == "New"){
      return "blue1.png";
    }
    
  },
  getActivities: function(){
    var self=this;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getDonut";
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", {}, function(response) {
      var total =  response.body.length;
      var completedCount = 0;
      var pendingCount = 0;
      var newCount = 0;
      for(i = 0;i<total;i++){
        if(response.body[i].status == "COMPLETED"){
          completedCount += 1;
        }if(response.body[i].status == "PENDING"){
          pendingCount += 1;
        }if(response.body[i].status == "NEW"){
          newCount += 1;
        }
      }
//       self.view.lblall.text = total;
//       self.view.Copylblall0bc00429b2ca949.text = newCount;
//       self.view.Copylblall0a48e445eb92344.text = pendingCount;
//       self.view.Copylblall0f69b13f2295947.text = completedCount;
      
      var completedPercent = Math.round((completedCount/total)*100);
      var pendingPercent =  Math.round((pendingCount/total)*100);
      var newPercent =  Math.round((newCount/total)*100);



      var DonutChart = new com.konymp.donutchart(
        {
          "clipBounds": true,
          "id": "DonutChart",
          "height": "130%",
          "width": "130%",
          "top": "-3%",
          "left": "-20%",
          "isVisible": true,
          "zIndex": 1
        }, {}, {});


      DonutChart.bgColor = "#FFFFFF";
      DonutChart.enableStaticPreview = true;
      DonutChart.chartData =
        {
        "data":
        [
          {"colorCode": "#6DD400", "label": "Completed", "value": completedPercent},
          {"colorCode": "#F7B500", "label": "Processing", "value": pendingPercent},
          {"colorCode": "#0091FF", "label": "In Review", "value": newPercent}

        ]

      };
      DonutChart.enableLegend = false;
      self.view.flxDonut.add(DonutChart);
      self.view.lblPercent.text = completedPercent +"%";
    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);
  },

  navigatefunction :function(){
    

  this.gblCustomerID=this.view.SegA.selectedRowItems[0].lblCid;
//alert("GBX"+this.gblCustomerID);
var ntpl = new kony.mvc.Navigation("frmCustomerAccountsDetails");
ntpl.navigate(this.gblCustomerID);
       
    },
});