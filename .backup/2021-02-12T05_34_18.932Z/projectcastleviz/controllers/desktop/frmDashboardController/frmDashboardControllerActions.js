define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Segment_ad9c9c5e47f640edabeeea09fcd99501: function AS_Segment_ad9c9c5e47f640edabeeea09fcd99501(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.navigatefunction.call(this);
    },
    AS_Button_c06315740020458b854f2190afcaf9fb: function AS_Button_c06315740020458b854f2190afcaf9fb(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmLoginforCastle");
        ntf.navigate();
    },
    AS_FlexContainer_g049e3c41e8642b2a66281594c2acb06: function AS_FlexContainer_g049e3c41e8642b2a66281594c2acb06(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmCustomerAccountsDetails");
        ntf.navigate();
    },
    AS_Form_e7beed71d30141fbb9e094a4952749f8: function AS_Form_e7beed71d30141fbb9e094a4952749f8(eventobject) {
        var self = this;
        return self.onPostShow.call(this);
    },
    AS_Form_d35fe1224d8d4340ab50761eff24f7d2: function AS_Form_d35fe1224d8d4340ab50761eff24f7d2(eventobject) {
        var self = this;
        return self.getNavDetails.call(this);
    }
});