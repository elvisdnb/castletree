define({ 
	rowinfo : null,
    countnumber: null,
    fromDate: null,
    holidaynext: null,
    reviseddate:null,
  	datefilter:null,
  	duedatefilter:null,
    graphvalues:null,
    revisedgraph:[],
     gblCid:null,
  
  onNavigate:function(ntpl2){
    this.gblCid = ntpl2;
   // this.view.flxA.setVisibility(false);
   // alert(this.gblCid);
  },
  onNavigationSearchCustomer: function(){
     this.view.flxNow.setVisibility(true);
        this.view.SegOne.setVisibility(true);
        this.view.flxHdr123.setVisibility(false);
        this.view.segMin2.setVisibility(false);
        this.view.flxContainerAccount.setVisibility(true);
        this.view.flxontainerLoans.setVisibility(false);
        this.view.flxPaymentholidaydetails.setVisibility(false);
		this.view.flxontainerLoans.skin = sknNotselected;
        this.view.flxContainerAccount.skin = sknSelected;
    
    // this.view.flxA.setVisibility(true);
    this.view.flxOnclickDummy.onClick = function(cnt){
      this.onLoanSegmentClick(cnt);
    }.bind(this);
    this.getCustomerDetails();
    this.getAccountDetails();
    this.view.flxRevisedPayment.setVisibility(false);
    this.view.btnRevisedPopop.setVisibility(false);
    
  },
      getFunForSplit : function(Totaldue)
  {
    a=Totaldue.toString();
    a=a.replace(/\,/g,''); 
    a=parseInt(a,10);
    return a;
  }, 
  getFunctionForPayment2 : function(rowInfo,holidaynext){
   var self = this;
    self.holidaynext = String(holidaynext);
    self.view.flxPaymentholidaydetails.setVisibility(true);
    self.view.lblEur.setVisibility(false);
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getPaymentScheduleDate";
   var params = {
      "loanAccountId": rowInfo.lblLoanAccountId,//"50003191",
      "arrangementId": rowInfo.lblArrangementId,//"AA2007988FCD",
      "dateFrom": self.holidaynext,//"20200706",
      "dateTo": ""
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
  
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      var allvalues=response.body;
     // alert(allvalues);
      self.graphvalues=[];
      var graphmax = [];
      for(var j = 0; j<allvalues.length;j++){
        var data = {
          "label":allvalues[j].date.split(" ")[1]+" "+allvalues[j].date.split(" ")[2].substring(2,4),
          "dataPoint1":self.getFunForSplit(allvalues[j].principal)
        };
        var data1 = {
          "dataPoint1":self.getFunForSplit(allvalues[j].principal)
        };
        self.graphvalues.push(data);
       
        graphmax.push(data1.dataPoint1);
       
         
      }
      var maxvalue =  Math.round(Math.max.apply(null,graphmax)/100)*100;

      
      var multiline = new com.konymp.multiline(

        {

          "autogrowMode": kony.flex.AUTOGROW_NONE,

          "clipBounds": true,

          "height": "70%",

          "id": "multiline",

          "isVisible": true,

          "layoutType": kony.flex.FREE_FORM,

          "left": "0%",

          "masterType": constants.MASTER_TYPE_USERWIDGET,

          "skin": "slFbox",

          "top": "5%",

          "width": "100%"

        }, {}, {});

      multiline.chartTitle = "Current repayment";
      multiline.bgColor = "#003e75";

      multiline.chartData = 
      
        {
			data: self.graphvalues
      };

      multiline.enableGrid = true;

      multiline.enableLegends = false;


      multiline.lineDetails =

        {

        "data":

        [

          {"color": "#1B9ED9", "legendName": "Apple"}

//           {"color": "#76C044", "legendName": "Banana"},

        ],

      };

      multiline.xAxisTitle = "u";            

      multiline.enableStaticPreview = true;

      multiline.legendFontSize = "95%";

      multiline.enableGridAnimation = true;

      multiline.titleFontSize = "12";

      multiline.lowValue = "0";

      multiline.titleFontColor = "#ffffff";
    

      multiline.legendFontColor = "#ffffff";

      multiline.highValue = "maxvalue";

      multiline.bgColor = "#314ebc";

      multiline.enableChartAnimation = true;

      self.view.flxgraphNew.add(multiline);
      } ,function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);
  },


  
  getCustomerDetails : function(){

    var self=this;
    self.view.lblEur.setVisibility(false);
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getCustomer";
    var params = {
      "customerId": self.gblCid
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      var customerDetails = response.body[0];
      self.view.lblValResidence.text =customerDetails.residenceName;//customerDetails.contactDetails[0].mobilePhoneNumbers[0].mobilePhoneNumber;//customerDetails.residenceName;
      self.view.lblHd.text =customerDetails.customerName;
      self.view.lblHdr.text =customerDetails.customerId;
      self.view.lblRelated.text =customerDetails.nationalityName;
      self.view.lblEmpStatus.text =customerDetails.contactDetails[0].emails[0].email;//customerDetails.residenceName;//customerDetails.employmentDetails[0].employmentStatus;
      self.view.lblProfiles.text =customerDetails.employmentDetails[0].employmentStatus;//customerDetails.profileTypes[0].profileTypeName;
      self.view.lblValAccountOfficer.text =customerDetails.accountOfficerName;
      self.view.lblDOB.text =customerDetails.dateOfBirth;
      self.view.lblValEmail.text =customerDetails.profileTypes[0].profileName;//customerDetails.contactDetails[0].emails[0].email;
      //self.view.lblRotterDam.text = customerDetails.contactDetails[0].mobilePhoneNumbers[0].mobilePhoneNumber;//customerDetails.employmentDetails[0].employmentStatus;
      //self.view.lblValPhone.text =customerDetails.contactDetails[0].mobilePhoneNumbers[0].mobilePhoneNumber;
      // self.view.lblEmpName.text =customerDetails.employmentDetails[0].employerName;
      self.view.lblEmpNameValue.text =customerDetails.employmentDetails[0].employerName;
    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);
  },

  getAccountDetails : function(){
    var self=this;
    var serviceName = "projectCastleTKO";
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getAccounts";
    var params = {
      "customerId": self.gblCid,
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      var segamentData = [];
      var dataMap = {
        "lblAcNum":"lblAcNum",
        "lblAcType":"lblAcType",
        "lblCCY":"lblCCY",
        "lblClearBal":"lblClearBal",
        "lblAvlBal":"lblAvlBal"
      };
      self.view.SegOne.widgetDataMap = dataMap;
      for(var i = 0; i<response.body.length;i++){
        var data = {
          "lblAcNum":response.body[i].displayName,
          "lblAcType":response.body[i].accountId,
          "lblCCY":response.body[i].leadCurrency,
          "lblClearBal": self.formatAmount(response.body[i].onlineClearedBalance+""),
          "lblAvlBal":self.formatAmount(response.body[i].onlineActualBalance + "")
        };
        segamentData.push(data);
      }
     // self.view.SegOne.setVisbility(true);
      self.view.SegOne.setData(segamentData);

    }, function(error) {
      if(error.opstatus=="8009"){}
      alert("No Accounts exist for the customer");
      //alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);
  },

  formatAmount : function(amount){
    var num,flag=false;
    if(amount === null || amount === undefined || isNaN(amount)) {
      return;
    }
    amount = Number(amount).toFixed(2);
    var decimal = ".";//this.getDecimalSeparator(locale);
    if(amount.indexOf(".")!=-1||amount.indexOf(",")!=-1)
    {
      if(amount.indexOf(".")!=-1)
      {			
        amount = amount.replace(".",decimal);            	
      }
      else if(amount.indexOf(",")!=-1){
        amount = amount.replace(",",decimal);	 		
      }		
      num = amount.split(decimal)[0];
      var dec = amount.split(decimal)[1];
      if(num.indexOf("-")!=-1)
      {
        num=num.split("-")[1];
        flag=true;
      }
      if(num.length > 3){
        for(var i=num.length-1;i>=0;){

          if(i>=3)
          {      
            num=num.substring(0,i-2) + "," + num.substring(i-2,num.length);	
          }
          i=i-3; 
        }      	
      }
      if(flag===true)
      {
        return "-"+num + decimal + dec;
      }
      return num + decimal + dec;	
    }
    else{
      return amount;
    }	
  },

//alpha..
  onLoadFunctions : function(){
    this.getCustomerDetails();
    this.getLoansDetails();
     
   
    
  },

  getLoansDetails : function() {
    var self=this;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getFullLoansDetails";
    var params = {
      "customerId": self.gblCid
    };

    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      var combData = [];
      var dataMap = {
        "lblHeaderAN":"lblHeaderAN",
        "lblHeaderAccountType":"lblHeaderAccountType",
        "lblCCY":"lblCCY",
        "lblClearedBalance":"lblClearedBalance",
        "lblAvailableLimit":"lblAvailableLimit",
        "lblHdr":"lblHdr",  
        "lblLoanAccountId":"lblLoanAccountId",
        "lblArrangementId":"lblArrangementId"
      };
      self.view.segMin2.widgetDataMap = dataMap;
      for(var i = 0; i<response.body.length;i++){
        var data = {
          "lblHeaderAN":response.body[i].loanAccountId,
          "lblHeaderAccountType":response.body[i].productName,
          "lblCCY":response.body[i].loanCurrency,
          "lblClearedBalance":self.formatAmount(self.removeMinus(response.body[i].loanBalance)),
          "lblAvailableLimit":response.body[i].loanStartDate,
          "lblHdr":response.body[i].loanEndDate,
          "lblLoanAccountId":response.body[i].loanAccountId,
          "lblArrangementId":response.body[i].arrangementId
         // "lblServices":{"onTouchStart":self.onLoanSegmentClick.bind(self,i)}
        };
//kony.application.getCurrentForm().segMin.setVisibility(false); 

        combData.push(data);
      }
      self.view.segMin2.setData(combData);
    }, function(error) {
      if(error.opstatus=="8009"){}
      alert("No loans exist for the customer");
    //  alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },

  removeMinus : function(x){
    return Math.abs(x);  
  },

  onLoanSegmentClick : function(ind){
    this.rowinfo=this.view.segMin2.data[ind.rowIndex];//this.view.segMin2.data[0];//selectedRowItems[0];  
    this.getCustomerLoan(this.rowinfo);
    //this.getFunctionForPayment2();
      },

  getCustomerLoan : function(rowInfo)

  {
    var self=this;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getLoans";
    var params = {
      "customerId": self.gblCid,
      "loanAccountId" : rowInfo.lblLoanAccountId,
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
     // var customerLoanDetails = response.body[0];
      //self.view.lblCur.text =customerLoanDetails.loanCurrency;
      self.view.lblNextPaymentDate.text = response.body[0].loanNextPayDate;
      self.view.lblNextMaturityDate.text = response.body[0].loanEndDate;
      self.view.lblDefhu.text=response.body[0].loanNextPayDate;
      var str   = response.body[0].loanNextPayDate;
      
      var day =  str.substring(0, 4);
      var day1 =  str.substring(0, 2);
      var month =  str.substring(5, 7);
      var year =   str.substring(8, 10);
      
      self.reviseddate =year+"/"+month+"/"+day1;
      self.datefilter = day1+month+year;
      self.holidaynext = day+month+year;
      self.getholidaysList(self.rowinfo,self.holidaynext);
      self.getFunctionForPayment2(self.rowinfo,self.holidaynext);
	
    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },

  getholidaysList : function(rowInfo,holidaynext){    
    var self=this;
    self.holidaynext = String(holidaynext);
    var serviceName = "projectCastleTKO";
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "getPaymentScheduleDate";
    var params = {
      "loanAccountId": rowInfo.lblLoanAccountId,//"50003191",
      "arrangementId": rowInfo.lblArrangementId,//"AA2007988FCD",
      "dateFrom": self.holidaynext,//"20200706",
      "dateTo": ""
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      var combData = [];
      var dataMap = {
        "lblAmount1":"lblAmount1",
        "lblDate1":"lblDate1",
        "lblArrow":"lblArrow",
      };
      self.view.segSkipPayments.widgetDataMap = dataMap;
      var displayLength = response.body.length;
      if(displayLength > 6){
        displayLength = 6;
      }

      for(var i = 0; i<displayLength ;i++){
        var data = {
          "lblAmount1":response.body[i].totalDue,
          "lblDate1":response.body[i].date,
          "lblArrow":{skin:"sknlblArrowGrey"},
        };
        combData.push(data);
      }
      self.view.segSkipPayments.setData(combData);
      self.view.lblCur.text = "€ " + response.body[0].totalDue;

    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },

  segSkipPayOnRowClick : function(){
    var  data = this.view.segSkipPayments.data;
    var  index =this.view.segSkipPayments.selectedRowIndex[1];
    if(data[index].lblArrow.skin  == "sknlblArrowGrey"){
      data[index].lblArrow.skin  = "sknlblArrowGreen";
    }else{
      data[index].lblArrow.skin  = "sknlblArrowGrey";
    }
    this.view.segSkipPayments.setDataAt(data[index], index, 0);
  },

   getLoadForSimualte:function()
  {
     this.view.flxUpcomingPaymentWrapper.setVisibility(false);
   // kony.application.getCurrentForm().flxPaymentholidaydetails.setVisibility(false);
    kony.application.showLoadingScreen("blockUI3", "Loading", constants.LOADING_SCREEN_POSITION_ONLY_CENTER, true, true, {});
    kony.timer.schedule("timerid9", this.dismissPool9,5, false);
},
  
   dismissPool9:function(){
    kony.application.dismissLoadingScreen();
    kony.timer.cancel("timerid9");
      this.view.flxUpcomingPaymentWrapper.setVisibility(false);
   //kony.application.getCurrentForm().flxPaymentholidaydetails.setVisibility(true);
   // this.getActivities();
  },
  
  onSimulateOnClick: function() {
    var data = this.view.segSkipPayments.data;
    //var self =this;
    this.getLoadForSimualte();
    
    //var segmentdata = this.view.segMin2.data;
    //var index = this.view.segMin2.selectedRowIndex[0];
    var count = 0;
    for (var i = 0; i < data.length; i++) {
      if (data[i].lblArrow.skin == "sknlblArrowGreen") {
        count = count + 1;
      }
    }
    this.countnumber= count;
    this.view.lblSkipCount.text = this.countnumber +"";
    this.getRevisedLoans(this.countnumber,this.rowinfo);
    this.view.flxUpcomingPaymentWrapper.setVisibility(false);
    this.afterSimulationForPayment2(this.countnumber,this.rowinfo);
  },

  getRevisedLoans : function(countNumber,rowinfos) {
    var self=this;
    var dateFrom = new Date();
    var dd = String(dateFrom.getDate()).padStart(2, '0');
    var mm = String(dateFrom.getMonth() + 1).padStart(2, '0');
    var yyyy = dateFrom.getFullYear();
    fromDate = yyyy+mm+dd;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "UpdatePaymentDate";
    var params = {
      "customerId":self.gblCid,
      "arrangementId": rowinfos.lblArrangementId,//"AA2007988FCD",
      "fromDate": self.holidaynext,//"20200706",
      "term": countNumber+""
    };

    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };


    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      var lastMaturityDates = response.body.length;
      //var dateTwo = response.body[lastMaturityDates - 1];
      
      var revisedvalue =[];
      var revisedPayment=[];
      var allvalues = [];
      for(var i=0; i<response.body.length;i++){
        var duedate3 = response.body[i].dueDate;
        
		var datefilterdtes = self.datefilter;
        
      var day1 =  duedate3.substring(0, 2);
      var month =  duedate3.substring(3, 5);
      var year =   "20"+duedate3.substring(6, 8);
               
      var year2 =  "20"+datefilterdtes.substring(0, 2);
      var month2 =  datefilterdtes.substring(2, 4);
      var day2 =   datefilterdtes.substring(4, 6);
     
        var d1 = new Date(year2,month2,day2);
        var d2 = new Date(year,month,day1);
        
        if (d1 < d2) {
          revisedvalue.push(duedate3);
          revisedPayment.push(response.body[i].scheduleDetails[0].dueAmount);
          allvalues.push(response.body[i]);
          self.revisedgraph.push(response.body[i]);
        }	        
      }
      var revisedPopupData = [];
      var dataMap = {
        "lblPaymentDate":"lblPaymentDate",
        "lblAmount":"lblAmount",
        "lblPrincipal":"lblPrincipal",
        "lblInterest":"lblInterest",
        //"lblTax":"lblTax",
        "lbltotaloutstanding":"lbltotaloutstanding"
      };
      self.view.revisedPaymentSeg.widgetDataMap = dataMap;
      for(var j = 0; j<allvalues.length;j++){
        var data = {
          "lblPaymentDate":allvalues[j].dueDate,
          "lblAmount":allvalues[j].scheduleDetails[0].dueAmount,
          "lblPrincipal":allvalues[j].scheduleDetails[0].totalPayment,
          "lblInterest":allvalues[j].scheduleDetails[1].propertyAmount,
          //"lblTax":allvalues[j].loanStartDate,
          "lbltotaloutstanding":allvalues[j].scheduleDetails[0].outstandingAmount
        };
        revisedPopupData.push(data);
      }
      self.view.revisedPaymentSeg.setData(revisedPopupData);
       var String1 = new Date(revisedvalue[0]);
       var String2 = new Date(response.body[lastMaturityDates - 1].dueDate);
//var String2 = "0";
        if(String1.getMonth()+1<=9){
        var month1 = "0"+(String1.getMonth()+1);}
        else{var month1 = (String1.getMonth()+1);}
        if(String1.getDate()<=9){
        var date1 = "0"+(String1.getDate());}
        else{var date1 = (String1.getDate());}
        var FullYear1 = (String1.getFullYear());
        var TotalDate = FullYear1+"-"+month1+"-"+date1;
//return TotalDate;
        if(String2.getMonth()+1<=9){
        var month2 = "0"+(String2.getMonth()+1);}
        else{var month2 = (String2.getMonth()+1);}
        if(String2.getDate()<=9){
        var date2 = "0"+(String2.getDate());}
        else{var date2 = (String2.getDate());}
        var FullYear2 = (String2.getFullYear());
        var TotalDate2 = FullYear2+"-"+month2+"-"+date2;
      
      self.view.lblRevisedNextPayDate.text = TotalDate;
      self.view.lblRevisedMaturityPayDate.text = TotalDate2;
      self.view.lblRevisedAmount.text = "€ " + revisedPayment[0];

      self.view.flxRevisedPayment.setVisibility(true);
    self.view.btnRevisedPopop.setVisibility(true);
      
    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);
    
// changeDate : function(varDate)
// {

// },

  },

   afterSimulationForPayment2 : function(countNumber,rowInfo){
   var self = this;
    self.view.flxPaymentholidaydetails.setVisibility(true);
    self.view.lblEur.setVisibility(false);
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
   var operationName = "UpdatePaymentDate";
    var params = {
      "customerId":self.gblCid,
      "arrangementId": rowInfo.lblArrangementId,//"AA2007988FCD",
      "fromDate": self.holidaynext,//"20200706",
      "term": countNumber+""
    };
    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };
  
    integrationSvc.invokeOperation(operationName, " ", params, function(response) {
      self.allgraphvalues=response.body;
      var simulatevalues=[];
      for(var j = 0; j < self.allgraphvalues.length;j++){
        
         var duedates = response.body[j].dueDate;
        
		var datefilterdtes = self.datefilter;
        
      var day1 =  duedates.substring(0, 2);
      var month =  duedates.substring(3, 5);
      var year =   "20"+duedates.substring(6, 8);
        
      var year2 =  "20"+datefilterdtes.substring(0, 2);
      var month2 =  datefilterdtes.substring(2, 4);
      var day2 =   datefilterdtes.substring(4, 6);
     
        var d1 = new Date(year2,month2,day2);
        var d2 = new Date(year,month,day1);
        if (d1 < d2) {
            var data = {
          "label": self.allgraphvalues[j].dueDate,
          "dataPoint1":self.getFunForSplit(self.graphvalue(self.graphvalues)),
          "dataPoint2":self.getFunForSplit( self.allgraphvalues[j].scheduleDetails[0].dueAmount)
        };
        simulatevalues.push(data);
        }
  
      }
      var multiline2 = new com.konymp.multiline(

        {

          "autogrowMode": kony.flex.AUTOGROW_NONE,

          "clipBounds": true,

          "height": "70%",

          "id": "multiline2",

          "isVisible": true,

          "layoutType": kony.flex.FREE_FORM,

          "left": "0%",

          "masterType": constants.MASTER_TYPE_USERWIDGET,

          "skin": "slFbox",

          "top": "5%",

          "width": "100%"

        }, {}, {});

      multiline2.chartTitle = "Current repayment";
      multiline2.bgColor = "#003e75";

      multiline2.chartData = 
      
        {
			data: simulatevalues
      };

      multiline2.enableGrid = true;

      multiline2.enableLegends = false;


      multiline2.lineDetails =

        {

        "data":

        [

          {"color": "#1B9ED9", "legendName": "Apple"},
          {"color": "#76C044", "legendName": "Banana"}

        ],

      };

      multiline2.xAxisTitle = "y";            

      multiline2.enableStaticPreview = true;

      multiline2.legendFontSize = "95%";

      multiline2.enableGridAnimation = true;

      multiline2.titleFontSize = "12";

      multiline2.lowValue = "0";

      multiline2.titleFontColor = "#ffffff";
    

      multiline2.legendFontColor = "#ffffff";

      multiline2.highValue = "2000";

      multiline2.bgColor = "#314ebc";

      multiline2.enableChartAnimation = true;

      self.view.flxgraphNew.add(multiline2);
      } ,function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);
  },
 graphvalue:function(allgraphvalues){
   for(var j = 0; j< allgraphvalues.length;j++){
     var variables =allgraphvalues[j].dataPoint1;
     return variables;
	}
 },
  
   getConfirmLoans : function() {	
	
    var self=this;
     
    rowinfos1 = self.rowinfo;
    var serviceName = "projectCastleTKO";
    // Get an instance of SDK
    var client = kony.sdk.getCurrentInstance();
    var integrationSvc = client.getIntegrationService(serviceName);
    var operationName = "confirmPaymentShedule";
    var params = {
      "customerId":self.gblCid,
      "arrangementId": rowinfos1.lblArrangementId,//"AA2007988FCD",
      "fromDate":  self.holidaynext,//"20200706",
      "term": this.countnumber+""
    };

    var options = {
      "httpRequestOptions": {
        "timeoutIntervalForRequest": 60,
        "timeoutIntervalForResource": 600
      }
    };


    integrationSvc.invokeOperation(operationName, " ", params, function() {
	
     self.view.flxPaymentHolidaysSuccessfullysWrapper.setVisibility(true);
      
    }, function(error) {
      alert("Integration Service Failure:" + JSON.stringify(error));
    }, options);

  },

});