define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_e7326365c91d402a9567f075b450a0c1: function AS_Button_e7326365c91d402a9567f075b450a0c1(eventobject) {
        var self = this;
        this.view.flxUpcomingPaymentWrapper.setVisibility(false);
    },
    AS_Button_ae65398d26c94147ac5ed8d8b4f13853: function AS_Button_ae65398d26c94147ac5ed8d8b4f13853(eventobject) {
        var self = this;
        this.view.flxUpcomingPaymentWrapper.setVisibility(false);
    },
    AS_Button_be0228ab3dad4ba59cd2a521f0b54c05: function AS_Button_be0228ab3dad4ba59cd2a521f0b54c05(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboard");
        ntf.navigate();
    },
    AS_Button_a9bba73ed6034bd2bd3ce03b93731c05: function AS_Button_a9bba73ed6034bd2bd3ce03b93731c05(eventobject) {
        var self = this;
        this.view.flxpopUpExploreLendingServciesWrapper.setVisibility(false);
        this.view.flxPaymentholidaydetails.setVisibility(false);
    },
    AS_Button_f939eb85808b4b4582f637cda8584ad9: function AS_Button_f939eb85808b4b4582f637cda8584ad9(eventobject) {
        var self = this;
        this.view.flxRevisedPaymentsWrapper.setVisibility(false);
    },
    AS_Button_g0e0d305a71442f89dad48ac65a9e8e4: function AS_Button_g0e0d305a71442f89dad48ac65a9e8e4(eventobject) {
        var self = this;
        this.view.flxNow.setVisibility(true);
        this.view.SegOne.setVisibility(true);
        this.view.flxHdr123.setVisibility(false);
        this.view.segMin2.setVisibility(false);
        this.view.flxContainerAccount.setVisibility(true);
        this.view.flxontainerLoans.setVisibility(false);
        this.view.flxPaymentholidaydetails.setVisibility(false);
    },
    AS_Button_gc53d0ffea37429ca4b07a9270874f2a: function AS_Button_gc53d0ffea37429ca4b07a9270874f2a(eventobject) {
        var self = this;
        this.view.flxontainerLoans.skin = sknSelected;
        this.view.flxContainerAccount.skin = sknNotselected;
        this.view.flxNow.setVisibility(false);
        this.view.SegOne.setVisibility(false);
        this.view.flxHdr123.setVisibility(true);
        this.view.segMin2.setVisibility(true);
        this.view.flxContainerAccount.setVisibility(false);
        this.view.flxontainerLoans.setVisibility(true);
        self.onLoadFunctions.call(this);
    },
    AS_Button_i6d3a047dd4f42c98d9550fa8acb7c46: function AS_Button_i6d3a047dd4f42c98d9550fa8acb7c46(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboard");
        ntf.navigate();
    },
    AS_Button_ee872918738d4457b5a447e80241a646: function AS_Button_ee872918738d4457b5a447e80241a646(eventobject) {
        var self = this;
        return self.getConfirmLoans.call(this);
    },
    AS_Button_c344b4b2a60f41dd896473d657819e08: function AS_Button_c344b4b2a60f41dd896473d657819e08(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmLoginforCastle");
        ntf.navigate();
    },
    AS_FlexContainer_a2896ace82464ebab0f77686cec7a181: function AS_FlexContainer_a2896ace82464ebab0f77686cec7a181(eventobject) {
        var self = this;
        this.view.flxRevisedPaymentsWrapper.setVisibility(true);
    },
    AS_Button_a78ba11234474957b5d0161c0c108a71: function AS_Button_a78ba11234474957b5d0161c0c108a71(eventobject) {
        var self = this;
        return self.onSimulateOnClick.call(this);
    },
    AS_Button_e526a391e4c34ff3b00f828d723144bb: function AS_Button_e526a391e4c34ff3b00f828d723144bb(eventobject) {
        var self = this;
        this.view.flxpopUpExploreLendingServciesWrapper.setVisibility(false);
        this.view.flxPaymentHolidaysSuccessfullysWrapper.setVisibility(false);
        var ntf = new kony.mvc.Navigation("frmDashboard");
        ntf.navigate();
    },
    AS_FlexContainer_ab6d8347e9d34e51be52659b51069f8c: function AS_FlexContainer_ab6d8347e9d34e51be52659b51069f8c(eventobject) {
        var self = this;
        this.view.flxRevisedPaymentSchedule.setVisibility(true);
    },
    AS_FlexContainer_abbf6153efdf46cd93f143554253f0a9: function AS_FlexContainer_abbf6153efdf46cd93f143554253f0a9(eventobject) {
        var self = this;
        this.view.flxRevisedPaymentsWrapper.setVisibility(true);
    },
    AS_FlexContainer_g2809bbe77944fa0b6d45d67a47e84b4: function AS_FlexContainer_g2809bbe77944fa0b6d45d67a47e84b4(eventobject) {
        var self = this;
        this.view.flxUpcomingPaymentWrapper.setVisibility(true);
    },
    AS_Form_f2c785dd8b044f52b267903e830d57a6: function AS_Form_f2c785dd8b044f52b267903e830d57a6(eventobject) {
        var self = this;
        return self.onNavigate.call(this, null);
    },
    AS_Form_e059b6bcbfcd4c70baca821c7ee0d8e5: function AS_Form_e059b6bcbfcd4c70baca821c7ee0d8e5(eventobject) {
        var self = this;
        return self.onNavigationSearchCustomer.call(this);
    },
    AS_Image_g3a3ad30dcc840259581915b82f3bc25: function AS_Image_g3a3ad30dcc840259581915b82f3bc25(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmDashboard");
        ntf.navigate();
    },
    AS_Segment_d31979feaacb48c186acdbe0dc2b7de5: function AS_Segment_d31979feaacb48c186acdbe0dc2b7de5(eventobject, sectionNumber, rowNumber) {
        var self = this;
        return self.segSkipPayOnRowClick.call(this);
    }
});