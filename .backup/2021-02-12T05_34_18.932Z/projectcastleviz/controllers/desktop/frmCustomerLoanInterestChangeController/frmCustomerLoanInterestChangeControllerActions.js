define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnAccounts1 **/
    AS_Button_fb872159826b4baf9bc1a6cccf31cc32: function AS_Button_fb872159826b4baf9bc1a6cccf31cc32(eventobject) {
        var self = this;
        this.view.flxNow.setVisibility(true);
        this.view.SegOne.setVisibility(true);
        this.view.flxHdr123.setVisibility(false);
        this.view.segMin2.setVisibility(false);
        this.view.flxContainerAccount.setVisibility(true);
        this.view.flxontainerLoans.setVisibility(false);
        this.view.flxPaymentholidaydetails.setVisibility(false);
    },
    /** onClick defined for btnAccounts3 **/
    AS_Button_b312ef910fe44c3da0ee24ff7d0c734d: function AS_Button_b312ef910fe44c3da0ee24ff7d0c734d(eventobject) {
        var self = this;
        this.view.flxontainerLoans.skin = sknSelected;
        this.view.flxContainerAccount.skin = sknNotselected;
        this.view.flxNow.setVisibility(false);
        this.view.SegOne.setVisibility(false);
        this.view.flxHdr123.setVisibility(true);
        this.view.segMin2.setVisibility(true);
        this.view.flxContainerAccount.setVisibility(false);
        this.view.flxontainerLoans.setVisibility(true);
        self.onLoadFunctions.call(this);
    }
});