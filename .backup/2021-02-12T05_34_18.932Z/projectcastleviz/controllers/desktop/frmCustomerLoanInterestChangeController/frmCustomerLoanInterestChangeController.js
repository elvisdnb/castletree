define({ 

  //Type your controller code here 
  onNavigate : function() {
    this.bindEvents();
  },

  bindEvents : function() {
    this.view.postShow = this.postShowFunctionCall;
  },

  postShowFunctionCall : function() {
    try {
      this.initializeCustomComponent();
      this.view.cusRadioButtonSelectionModeValue.onRadioChange = this.getSelectInterestType;
      //this.view.cusButtonConfirm.onclickEvent = this.onClickSubmitData;
    } catch(err) {
      alert("Error in CustomerLoanInterestChangeController postFunctionCall:::"+err);
    }
  },

  initializeCustomComponent : function () {
    try {
      this.view.cusBackIcon.color = "temenos-dark";
      this.view.cusBackIcon.size = "small";
      this.view.cusBackIcon.value = "chevron_left";

//       this.view.cusDownArrowIcon.color = "temenos-dark";
//       this.view.cusDownArrowIcon.size = "small";
//       this.view.cusDownArrowIcon.value = "south";

      this.view.cusRadioButtonSelectionModeValue.options = [
        {
          "label": "Fixed",
          "value": 1
        },
        {
          "label": "Floating",
          "value": 2
        }];
      this.view.cusButtonDelete.cta = false;
      this.view.cusButtonDelete.labelText = "Delete";
      this.view.cusButtonDelete.icon = "remove_circle_outline";
      this.view.cusButtonDelete.size = "large";
      this.view.cusButtonDelete.outlined = false;
      this.view.cusButtonDelete.disabled = false;
      this.view.cusButtonDelete.trailingIcon = false;
      this.view.cusButtonDelete.compact = false;

      this.view.cusNavigationRail.menuItems = '[{ "icon": "search", "label": "search" },{ "icon": "menu", "label": "menu" },{ "icon": "restore", "label": "restore" }]';
      this.view.cusNavigationRail.bottomBtnItems = '[{"label":"Settings"},{"label":"Last Updated"},{"label":"Logout"}]';

      this.view.cusRadioButtonSelectionModeValue.value = 1;
      this.view.flxFixedModeWrapper.isVisible = true;
      this.view.flxFloatingModeWrapper.isVisible = false;

      this.view.flxHeaderMenu.isVisible = true;
      this.view.flxMessageInfo.isVisible = false;

      this.populatingTierTypeDropDown();

    } catch(err) {
      alert("Error in CustomerLoanInterestChangeController initializeCustomComponent:::"+err);
    }
  },

  getSelectInterestType : function() {
    try {
      //alert("alert"+this.view.cusRadioButtonSelectionModeValue.value);
      var options = this.view.cusRadioButtonSelectionModeValue.value;
      if(options === 1) {
        this.onChangeFixedInterestEvent();
      } else if(options === 2) {
        this.onChangeFloatInterestEvent();
      }
    } catch(err) {
      alert("Error in CustomerLoanInterestChangeController getSelectInterestType:::"+err);
    }
  },

  onChangeFloatInterestEvent : function() {
    try {
      this.view.flxFixedModeWrapper.isVisible = false;
      this.view.flxFloatingModeWrapper.isVisible = true;
    } catch(err) {
      alert("Error in CustomerLoanInterestChangeController onChangeInterestEvent:::"+err);
    }
  },

  onChangeFixedInterestEvent : function() {
    try {
      this.view.flxFixedModeWrapper.isVisible = true;
      this.view.flxFloatingModeWrapper.isVisible = false;
    } catch(err) {
      alert("Error in CustomerLoanInterestChangeController onChangeFixedInterestEvent:::"+err);
    }
  },

  populatingTierTypeDropDown : function() {
    try {
      this.view.cusDropDownTierListResponse.options = '[{"label": "Select"},{"label": "Example Tier1"}, {"label": "Example Tier2"}, {"label": "Example Tier3"}]';
      this.view.cusDropDownTierListResponse.value =  "Select";
    } catch(err) {
      alert("Error in CustomerLoanInterestChangeController populatingTierTypeDropDown:::"+err);
    }
  },

  onClickSubmitData : function() {
    try {
      this.view.flxHeaderMenu.isVisible = false;
      this.view.flxMessageInfo.isVisible = true;

    } catch(err) {
      alert("Error in CustomerLoanInterestChangeController populatingTierTypeDropDown:::"+err);
    } 
  }

});