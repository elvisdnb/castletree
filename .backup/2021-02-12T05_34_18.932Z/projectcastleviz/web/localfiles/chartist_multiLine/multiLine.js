/**
 * Created by Team Kony.
 * Copyright (c) 2017 Kony Inc. All rights reserved.
 */
konymp = {};
konymp.charts = konymp.charts || {};

konymp.charts.lineChart = function() {
  
};

konymp.charts.lineChart.prototype.createClass = function(name, rules){
    var style = document.createElement('style');
    style.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(style);
    if(!(style.sheet||{}).insertRule) 
        (style.styleSheet || style.sheet).addRule(name, rules);
    else
        style.sheet.insertRule(name+"{"+rules+"}",0);
};

konymp.charts.lineChart.prototype.Updatecss = function(colors,properties) {
  	var regColorcode = /^(#)?([0-9a-fA-F]{3})([0-9a-fA-F]{3})?$/;
  	try {
      	for(var i in colors) {
          	if(colors[i] !== "" && regColorcode.test(colors[i])) { 
              	this.createClass('.ct-legend .ct-series-'+i,'font-family:Arial, Helvetica, sans-serif; color:'+ properties._legendFontColor+'; font-size:'+ properties._legendFontSize + ';');
              	this.createClass('.ct-legend .ct-series-'+i+':before',"  background-color:"+colors[i]+"; border-color:"+colors[i]+";");
              	var _char = String.fromCharCode(parseInt(97 + Number(i)));
              	this.createClass('.ct-series-' + _char + ' .ct-line', " stroke: " + colors[i] + ";");
              	this.createClass('.ct-series-' + _char + ' .ct-point', " stroke: " + colors[i] + ";");
            }
          	else {
              	throw {"Error": "InvalidColorCode", "message": "Color code for bars should be in hex format. Eg.:#000000"};
            }
        }
    }
  	catch(exception) {
      	if(exception.Error === "InvalidColorCode") {
          	throw(exception);
        }
    }
};

konymp.charts.lineChart.prototype.drawLineChart = function(title, labelNames, seriesSet, colors, properties) {
  	var myNode = document.getElementById("legends");
  	while (myNode.firstChild) {
    	myNode.removeChild(myNode.firstChild);
  	}
  	var plugins = [];
  	plugins[0] = Chartist.plugins.ctAxisTitle({
    	axisX: {
        	axisTitle: properties._xAxisTitle,
        	axisClass: 'ct-axis-title',
        	offset: {
          		x: 0,
          		y: 35
       		},
        	textAnchor: 'middle'
      	},
      	axisY: {
        	axisTitle: properties._yAxisTitle,
        	axisClass: 'ct-axis-title',
        	offset: {
          		x: 0,
          		y: 13.5
        	},
        	textAnchor: 'middle',
        	flipTitle: true
      	}
    });
  	if(properties._enableLegends) {
    	plugins[1] = Chartist.plugins.legend({
        	horizontalAlign: "right",
        	clickable: false,
        	position: document.getElementById('legends')
      	});
    }
	var chart = new Chartist.Line('.ct-chart', {	
		labels: labelNames,
  		series: seriesSet
	}, {
      	low: parseFloat(properties._lowValue),
      	high: parseFloat(properties._highValue),
  		fullWidth: true,
  		chartPadding: {
    		right: 40,
		},
      	plugins: plugins
	});
  
  	this.Updatecss(colors, properties);
  	
  	document.getElementById("lblTitle").style.color = properties._titleFontColor || '#000000';
  	document.getElementById("lblTitle").style.fontSize = properties._titleFontSize !== undefined ? (parseInt(properties._titleFontSize)*10) + '%' : '120%';
  	document.getElementById("lblTitle").style.fontFamily = 'Arial, Helvetica, sans-serif';
  	document.getElementById("lblTitle").innerHTML = title;
  	
  	document.body.style.backgroundColor = properties._bgColor;
  
  	var seq = 0, delays = 80, durations = 500;
  
  	chart.on('created', function() {
      	seq = 0;
    });
  
  	chart.on('draw', function(data) {
        if(properties._enableGrid !== true && data.type === 'grid' && data.index !== 0) {
      		data.element.remove();
    	} 
        if(!properties._enableChartAnimation){
          return;
        }
      	if(properties._enableGrid && properties._enableGridAnimation === true) {
      		seq++;
        }
      	if(data.type === 'line') {
          	data.element.animate({
              	opacity: {
                  	begin: seq * delays + 1000,
                  	dur: durations,
                  	from: 0,
                  	to: 1
                }
            });
        }
      	else if(data.type === 'label' && data.axis === 'x') {
    		data.element.animate({
      			y: {
        			begin: seq * delays,
        			dur: durations,
        			from: data.y + 100,
        			to: data.y,
        			easing: 'easeOutQuart'
      			}
    		});
  		} 
      	else if(data.type === 'label' && data.axis === 'y') {
    		data.element.animate({
      			x: {
        			begin: seq * delays,
        			dur: durations,
        			from: data.x - 100,
        			to: data.x,
        			easing: 'easeOutQuart'
      			}
    		});
  		} 
      	else if(data.type === 'point') {
          	if(properties._enableGridAnimation === false) {
              	seq++;
            }
    		data.element.animate({
      			x1: {
        			begin: seq * delays,
        			dur: durations,
        			from: data.x - 10,
        			to: data.x,
        			easing: 'easeOutQuart'
      			},
      			x2: {
        			begin: seq * delays,
        			dur: durations,
        			from: data.x - 10,
        			to: data.x,
        			easing: 'easeOutQuart'
      			},
      			opacity: {
        			begin: seq * delays,
        			dur: durations,
        			from: 0,
        			to: 1,
        			easing: 'easeOutQuart'
      			}
    		});
  		} 
      	else if(properties._enableGrid===true && properties._enableGridAnimation === true && data.type === 'grid') {
    		var pos1Animation = {
      			begin: seq * delays,
      			dur: durations,
      			from: data[data.axis.units.pos + '1'] - 30,
      			to: data[data.axis.units.pos + '1'],
      			easing: 'easeOutQuart'
    		};
    		var pos2Animation = {
      			begin: seq * delays,
      			dur: durations,
      			from: data[data.axis.units.pos + '2'] - 100,
      			to: data[data.axis.units.pos + '2'],
      			easing: 'easeOutQuart'
    		};
    		var animations = {};
    		animations[data.axis.units.pos + '1'] = pos1Animation;
    		animations[data.axis.units.pos + '2'] = pos2Animation;
    		animations['opacity'] = {
      			begin: seq * delays,
        		dur: durations,
      			from: 0,
      			to: 1,
      			easing: 'easeOutQuart'
    		};
    		data.element.animate(animations);
  		}
    });
  
  	chart.on('created', function() {
  		if(window.__exampleAnimateTimeout) {
    		clearTimeout(window.__exampleAnimateTimeout);
    		window.__exampleAnimateTimeout = null;
  		} 	
	});
};

var drawCanvasChart =  function(){
  	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    	return true;
	}
  	var labels = ["data1", "data2", "data3", "data4", "data5", "data6", "data7"];
  	var data = [
      			{name: 'blue', data: [12, 9, 7, 8, 5, 14, 10]},
              	{name: 'green', data: [2, 1, 3.5, 7, 3, 5, 8]},
                {name: 'orange', data: [1, 3, 4, 5 , 6, 9, 12]},
                {name: 'purple', data: [9, 2, 5, 12, 3, 7, 1]},
                {name: 'yellow', data: [3, 9, 2, 7, 5, 2.5, 6]}  
               ];
  	var colors = ["#1B9ED9", "#76C044", "#F26B29", "#7A54A3", "#FFC522"];
  	var prop = {
      	_lowValue: '0',
      	_highValue: '15',
      	_xAxisTitle: 'data', 
      	_yAxisTitle: 'value', 
      	_titleFontColor: '#FFFFFF',
      	_titleFontSize: 15,
      	_bgColor: '#ffffff',
      	_enableGrid: true,
      	_enableGridAnimation: false,
        _enableChartAnimation: true,
      	_enableLegends: true,
      	_legendFontColor: '#FFFFFF',
      	_legendFontSize: '95%',
      	_enableStaticPreview: true
    };
  	var x = new konymp.charts.lineChart();
 	x.drawLineChart('Line Chart - Stock Example', labels, data, colors, prop);
};
window.addEventListener("DOMContentLoaded", function() {
 setTimeout(onbodyload, 0);
}.bind(this), false);
onbodyload = function(){
 if(typeof kony !== "undefined") {
   kony.evaluateJavaScriptInNativeContext("chart_multiLine_defined_global('ready')");
 } else {
	drawCanvasChart();
 }
}.bind(this);

