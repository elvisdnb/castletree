Highcharts.chart('container', {
  chart: {
    type: 'spline'
  },
  title: {
    text: 'Monthly Average Temperature'
  },
  subtitle: {
    text: 'Source: WorldClimate.com'
  },
  xAxis: {
    categories: ['Dec 20', 'Jan 21', 'Feb 21', 'Mar 21', 'Apr 21', 'May 21',
      'June 21', 'July 21', 'Aug 21', 'Sep 21', 'Oct 21', 'Nov 21']
  },
  yAxis: {
    title: {
      text: ''
    },
    labels: {
      formatter: function () {
        return this.value + '�';
      }
    }
  },
  tooltip: {
    crosshairs: true,
    shared: true
  },
  plotOptions: {
    spline: {
      marker: {
        radius: 4,
        lineColor: '##32C5FF',
        lineWidth: 1
      }
    }
  },
  series: [{
    name: '---',
    marker: {
      symbol: 'square'
    },
    data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, {
      y: 26.5,
      marker: {
        symbol: 'url(https://www.highcharts.com/samples/graphics/sun.png)'
      }
    }, 23.3, 18.3, 13.9, 9.6]

  }, {
    name: '----',
    marker: {
      symbol: 'diamond'
    },
    data: [{
      y: 3.9,
      marker: {
        symbol: 'url(https://www.highcharts.com/samples/graphics/snow.png)'
      }
    }, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
  }]
});