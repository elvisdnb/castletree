UUXRuleDesignerInput = {
  _domElement:null,
  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.

    const widgetId = widgetModel.id;
    parentNode.innerHTML = "<uwc-rule-designer-input id="+widgetModel.parent.id+widgetModel.id+"></uwc-rule-designer-input>";
    const element = parentNode.lastChild;
    this._domElement = element;

    if(widgetModel.labelText)
    {
      element.label = widgetModel.labelText;
      element.dialogLabel = widgetModel.labelText;
    }

    if (widgetModel.rules)
    {
      var rules = widgetModel.rules;

      element.rules = rules;
    }

    var commonProperties = ["dense", "disabled", "value","required","ruleEditValuesOnly","ruleValue","ruleSupportLevel"];
    commonProperties.forEach((property) => {
      element[property] = widgetModel[property];
    })

    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
    });

  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) 
  {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.

    let element =document.getElementById(widgetModel.parent.id+widgetModel.id);

    if(element)
    {
      if(propertyChanged==="value")
      {
        element.value = propertyValue;
      }

      if(propertyChanged==="disabled")
      {
        element.disabled = propertyValue;
      }
      
      if(propertyChanged==="labelText")
      {
        element.label = propertyValue;
        element.dialogLabel = propertyValue;
      }
      
      if(propertyChanged==="rules")
      {
        element.rules = propertyValue;
      }
      if(propertyChanged==="dialogLabel"){
        element.dialogLabel = propertyValue;
      }
    }

  }
};