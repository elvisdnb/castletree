uuxRelativeDatePicker = {
  _domElement:null,
  initializeWidget: function(parentNode, widgetModel) {
    //Assign custom DOM to parentNode to render this widget.
    const widgetId = widgetModel.id;
    parentNode.innerHTML += "<uwc-date-relative-picker id="+widgetId+"></uwc-date-relative-picker>";
    const element = parentNode.lastChild;
    this._domElement = element;

    if(widgetModel.labelText){
      element.label = widgetModel.labelText;
    }

    var supportedProperties = ["disabledDates", "disabledDays","max", "min", "offsetMax", "value","formattedValue"];

    supportedProperties.forEach((propertyName)=>{
      element[propertyName] = widgetModel[propertyName];
    });

    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
      widgetModel.formattedValue = event.target.formattedValue;
    });

  },
  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    if(propertyChanged==="value"){
      widgetModel.onValueSelected(widgetModel);
    }
  }
};