uuxNavigationRail = {

  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-navigation-rail id='"+widgetModel.parent.id+widgetModel.id+"'position='"+widgetModel.positionValue+"'menuItems='"+widgetModel.menuItems+"'bottomBtnIcon='"+widgetModel.bottomBtnIcon+"'bottomBtnItems='"+widgetModel.bottomBtnItems+"'hideBottomBtn='"+widgetModel.hideBottomBtn+"'logo='"+widgetModel.logo+"'logoAlt='"+widgetModel.logoAlt+"'></uwc-navigation-rail>";
    
    var element = parentNode.lastElementChild;
    
	element.position = widgetModel.positionValue;
    //element.menuItems = widgetModel.menuItems;
    element.bottomBtnIcon = widgetModel.bottomBtnIcon;
	//element.bottomBtnItems = widgetModel.bottomBtnItems;
	element.hideBottomBtn = widgetModel.hideBottomBtn;
	element.logo = widgetModel.logo;
	element.logoAlt = widgetModel.logoAlt;

  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.

  }
};