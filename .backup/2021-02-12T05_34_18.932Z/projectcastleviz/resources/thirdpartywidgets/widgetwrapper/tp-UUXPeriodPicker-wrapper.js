UUXPeriodPicker = {
  _domElement:null,
  initializeWidget: function(parentNode, widgetModel) {
    const widgetId = widgetModel.id;
    parentNode.innerHTML += "<uwc-period-picker id="+widgetId+"></uwc-period-picker>";
    const element = parentNode.lastChild;
    this._domElement = element;

    //kony widget model labelText === UUX element label
    if(widgetModel.labelText){
      element.label = widgetModel.labelText;
    }
    var commonProperties = ["periodMaxLength", "periodType", "value"];
    commonProperties.forEach((property) => {
      element[property] = widgetModel[property];
    })

    element.addEventListener('change', (event) => {
      widgetModel.value = event.target.value;
    });

  },
  modelChange: function(widgetModel, propertyChanged, propertyValue) {
  }
};