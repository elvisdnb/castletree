uuxTextArea = {
  initializeWidget: function(parentNode, widgetModel, config) {
    //Assign custom DOM to parentNode to render this widget.
    parentNode.innerHTML = "<uwc-text-area id='"+widgetModel.parent.id+widgetModel.id+"' label='"+widgetModel.labelText+"' charCounter maxLength='"+widgetModel.maxLength+"' value='"+widgetModel.value+"'></uwc-text-area>";
    var element = parentNode.lastElementChild;
    element.addEventListener('change',(evt)=>{
      widgetModel.value = evt.target.value;
    });    
  },

  modelChange: function(widgetModel, propertyChanged, propertyValue) {
    //Handle widget property changes to update widget's view and
    //trigger custom events based on widget state.
    var element = document.getElementById(widgetModel.parent.id+widgetModel.id);
    if(element){
      if(propertyChanged === "labelText"){
        element["label"] = propertyValue;  
      }else element[propertyChanged] = propertyValue;
      
      if(propertyChanged==="value"){
        if((typeof widgetModel.valueChanged)==="function"){
           widgetModel.valueChanged(propertyValue);
        }
      }
    }
  }
};