var mData = "";
function showTableChangeInterest(){
  var changeIntrest =  

    [

      [{lblTitle:"Change Interest",lblShelves:"Interest Rate Type ",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The default interest rate type agreed in the loan contract is displayed on the screen. <br>&#9679;     An option to choose either Fixed or Floating rate is provided to alter the interest rate type.<br>&#9679;     Entering the interest rate type is mandatory and cannot be left blank.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Tier Type",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The tier type for a loan contract can be on a level, banded or single. <br>&#9679;     Tier type is only displayed and cannot be edited.<br>&#9679;     It is automatically picked up from the loan contract.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Effective Date",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The effective date from which the interest rate changes are likely to be reflected is to be entered in this field. <br>&#9679;     A calendar icon is provided to help choose the date which will then be replacing the current date.<br>&#9679;     It is mandatory to choose the effective date to procced with the change interest option.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Fixed Rate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Refers to the existing fixed interest rate of the loan.<br>&#9679;     It can be edited and left blank if the loan is initially based on floating rate of interest. <br>&#9679;     You can subsequently choose fixed option and fill in new fixed rate if the customer wishes to switch over.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Operand",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The operand field is provided to add or reduce the margins to the interest rates.<br>&#9679;     A drop-down list with two options: add and subtract is provided for you to choose.<br>&#9679;     Operand is an optional field and can be edited as per what has been agreed with the customer.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Margin",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Margin refers to the margins that are added or subtracted from the interest rates. These are usually included in the initial loan contract itself.<br>&#9679;     Margins can be edited but not compulsory for the change interest option.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Effective Rate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The effective rate field calculates and displays the revised effective rate after adding or subtracting the margin from the interest rate specified for the level.<br>&#9679;     Effective rate is automatically calculated and cannot be edited.<br>&#9679;     If no Margin is specified then the value in the Effective rate is the same as Fixed rate in case of Fixed rate set up and Interest rate in the case of Floating Index set up.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Upto Amount",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The tier amount (if loan on tier basis) specified in the loan contract level is displayed in this field.<br>&#9679;     It cannot be edited.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Floating Rate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The Floating rate displays the default floating index from the loan.<br>&#9679;     This can be edited and left blank when the loan is on fixed rate.</p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"View Payment Schedule",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Revised payment schedule details are displayed upon selecting the View Payment Schedule.<br>&#9679;     The Revised payment schedule is displayed only when interest type or rates is changed.<br>&#9679;     The Revised payment schedule displays the Payment Date, Amount, Principal, Interest, Tax and Total Outstanding.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Payment Date",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the date of repayment in DD-MMM-YYYY format.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Amount",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the total repayment amount for the corresponding date.<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Principal",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Principal amount for the corresponding repayment date.<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Interest",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Interest amount for the corresponding repayment date.<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Tax",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Tax amount for the corresponding repayment date.<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Change Interest",lblShelves:"Total Outstanding",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the Total Outstanding amount for the corresponding repayment date.<br>&#9679;     The amount will be automatically computed and displayed based on values in the Interest Type, Tier type, Floating rate (if the type chosen is floating), Interest rate and Upto Amount.<br></p>",

         template:"CopyflxParent"}]]
    ]; 
  return changeIntrest;
}

function showTableLoanPayOff(){
  var changeIntrest1 =  

    [

      [{lblTitle:"Loan Payoff",lblShelves:"Payoff Statement",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"&#9679;     By default, it refers to the current date. <br>&#9679;     It is a compulsory field and can be edited. <br>&#9679;     Clicking this will enable a Calendar icon from where the date can be chosen.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Proceed",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Proceed take you to the Payoff screen from the statement screen.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Cancel",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Cancel takes you back to the customer loan dashboard with list of loans.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Total Outstanding",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Displays the total amount of loan outstanding as of the selected effective date. <br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Settlement Mode",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;    The payment mode should allows you to make the payment via the following options:<br>&#9679;     Account to account transfer - Based on selection, the amount must be transferred from the customer’s savings/current account to the loan account.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Debit Account",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     The PAYING ACCOUNT to be defaulted from the loan contract.<br>&#9679;     Where more than one Paying account is mapped to the contract, the account number in the 1st Multi-value shall be displayed by default.<br>&#9679;     A search icon is provided to select an account that is not mapped to settlement account. <br>&#9679;     Standard Transact account enquiry to be mapped.<br></p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Loan Payoff",lblShelves:"Payment Value date",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;    Should be defaulted from the effective date selection.<br>&#9679;    Cannot be edited.<br></p>",

         template:"CopyflxParent"}]]     



    ] ;
  
  return changeIntrest1;
}

function showTablePaymentHoliday(){
  var paymentHolidayData =  

    [

      [{lblTitle:"Payment Holiday",lblShelves:"Holiday Start Date",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Refers to the date from which payment holiday is to be started.<br>&#9679;     Displays the date in DD MMM YYYY format.<br>&#9679;     You will be able to select dates from the pop-up that displays upcoming 6 repayments from the repayment schedule of the loan.<br>&#9679;     A Check box will be provided against each date for selecting the payments to be skipped.<br>&#9679;     The checks should be consecutive else it would throw an error.<br>&#9679;     Mentioning the holiday start date is compulsory for availing loan payment holiday.</p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Payment Holiday",lblShelves:"Account number",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Account number of the loan/ Arrangement ID of the loan.<br>&#9679;     This is not displayed in the screen but used for background processing.</p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Payment Holiday",lblShelves:"New Payment in Holiday",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     You will be able to fill the new payment amount.<br>&#9679;     As you check subsequent dates, the new payment amount will override the existing payments automatically in every row.<br>&#9679;     It is not compulsory for availing the service.<br>&#9679;     The first field checked for revised payment is the only field where you will be able to enter the details.</p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Payment Holiday",lblShelves:"Recalculate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     A compulsory selection field wherein you can select one from the 2 options provided in the drop-down list.<br>&#9679;     Term and Payment are the 2 options provided.</p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Payment Holiday",lblShelves:"Payment Holiday Opted",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Automatically updated based on the number of payments selected in the pop-up.</p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Payment Holiday",lblShelves:"Simulate",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;    Clicking on the button will simulate the payment holiday.</p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Payment Holiday",lblShelves:"See Revised Payment Schedule",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;     Option that pops up revised payment schedule based on the option chosen for holiday.</p>",

         template:"CopyflxParent"}]],

      [{lblTitle:"Payment Holiday",lblShelves:"Print icon",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;    Clicking this provides the option for printing the revised payment schedule.</p>",

         template:"CopyflxParent"}]],
      
      [{lblTitle:"Payment Holiday",lblShelves:"Confirm",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;    Clicking on confirm will execute the simulation.<br>&#9679;    Upon clicking, a success message of transaction will appear.<br>&#9679;    Clicking on Ok will take you to dashboard.</p>",

         template:"CopyflxParent"}]],
      
      [{lblTitle:"Payment Holiday",lblShelves:"Cancel",imgLocation:"",lblDown:"M",flxAnimate:{width:"70%"},template:"CopyflxDown"},

       [{lblAddress:"<p>&#9679;    Button to take you back to the payment holiday application.</p>",

         template:"CopyflxParent"}]]



    ] ;
  
  return paymentHolidayData;
}

function showTableQues(){
  var questions = [
          [{lblQues:"1. Can we define the date from which the interest rate change should be applicable?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, the field ‘Effective Date’ can be used to define the date from which the rate change is to be applied.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"2. Is it possible to change the type of interest for a loan i.e., fixed to floating and vice versa?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, the interest on an existing loan can be changed from Fixed type to floating type and vice versa. When the type of interest is changed, the earlier values are overwritten, and the new values take effect from the effective date of interest change",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"3. How do we know what kind of tier basis is followed for tier-based interest calculation?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"The field ‘Tier type’ displays the tier type based on which the loan was created, either level or band. This will be an info-only field. The ‘Upto Amount’ field shall be used to view the tier levels defined at the time of product creation.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
   
    [{lblQues:"4. Can we add/subtract margin to a linked index rate?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, the user can add/subtract margin using the fields Operand and Margin and view the final applicable rate in the field ‘Effective Rate’.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"5. Do we have options to map indices other than Prime Rate?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, a few values are provided in the drop-down of the field ‘Floating Rate’. Upon selection of the requisite index, the corresponding rate is displayed in the field ‘Interest Rate’ automatically",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]]
  ];
  return questions;
}

function showTableLoanQues(){
  var loanQuestions = [
          [{lblQues:"1. Can a loan be paid-off before its maturity date?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, loan can be paid-off any time.  However, paying-off the loan before maturity may attract a nominal fee.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"2. Where can I see the fee for payoff?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"While selecting the payoff option, under the “Payoff Settlement” section, you can see the payoff fee being charged.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"3. Will the system automatically calculate the payoff amount?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, upon selecting the payoff option from the dropdown, the system will simulate the loan payoff and show the net settlement amount to be paid by the customer on the screen.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
   
    [{lblQues:"4. Can I see the net settlement amount for a future date?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, the Payoff Simulation graph will show the net settlement amount for the next 5 days. You can move your mouse over on the pointers on the graph to view the net settlement amount.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"5. Why is the Debit Account field automatically populated with an account number?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"While creating the loan contract, the account number that the customer provided for settlement of loan will be automatically defaulted.  Using the search icon, you can search for other account from which the settlement proceeds are to be debited.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]]
  ];
  return loanQuestions;
}

function showTablePaymentQues(){
  var paymentQuestions = [
          [{lblQues:"1. Can we skip payments randomly?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"No, we cannot skip payments randomly. A payment holiday can only be availed for consecutive payments. ",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"2. How many payments can the user skip at a given point in time?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"At any given point of time, a list populating the upcoming 6 payments are displayed for the user to select his holiday period from. Once an application for a payment holiday is created for ‘n’ payments, the next 6 outstanding payments are listed starting from the ‘n’ time for applying a holiday.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"3. Do we have an option to pay a different amount for a specific period or should the entire payment be necessarily skipped during the holiday period?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Under this service, provision is available to skip the entire payment as well as pay a lesser amount for a specific holiday period. The user shall select the months and provide the revised amount that the customer would pay for such period, in the field ‘Revised amount’. The payment schedule will get recalculated according to the revised payment amount.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
   
    [{lblQues:"4. When we apply a payment holiday, how is the schedule recalculated?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"The user has the option of recalculating the following:<br>a. Payment – When the customer wants the payment to be recalculated and the term retained, the user can select ‘Payment Amount’ option from the field ‘Recalculate’<br>b. Term – When the customer wants the term to be recalculated and the payment amount retained, the user can select ‘Term’ option from the field ‘Recalculate’.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"5. Is there an option to view the revised terms of the loan when a payment holiday is opted for?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, the user can select the months for which payment needs to be skipped and simulate the payment holiday. The user can then view the details such as Next Payment Date, Payment Amount, New Maturity Date and the revised payment schedule under the header ‘Revised Payment Details’ on the right side of the screen.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"6. Can we see the revised payment schedule? How?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, we can see the revised payment schedule after the payment holiday terms are chosen. We can see the revised schedule by clicking on the hyperlink ‘See Revised Payment Schedule’.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]],
    [{lblQues:"7. Can another payment holiday be applied for the same loan?",lblUp:"M",template:"flxMoreHeader"},

       [{rtxAnswer:"Yes, there is no restriction on the number of payment holidays that can be applied for on the same loan.",

         template:"flxAnswers", flxSpace:{bottom:"5dp"}}]]
  ];
  return paymentQuestions;
}
