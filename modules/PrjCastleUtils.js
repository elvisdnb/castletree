function showLoadingIndicator(){
  kony.application.showLoadingScreen("blockUI3", "Loading...", constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, true, {});
};

function dismissLoadingIndiator(){
  kony.application.dismissLoadingScreen();
};


function isNullorEmpty(input){
  if(input === "" || input==" " ||input===null || input === undefined || input.length<=0)
    return true;
  else 
    return false;
}