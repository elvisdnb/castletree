//Type your code here

var gblCustomerObj={};
var transactDate="";
var gblAllaccountWidgetDate=[];
var gblAllaccountarray=[];
var gblCutacctWidgetDate= [];
var  gblCutacctArray= [];
var gblDashboardObj="";
var gblCurrentContractLog = [];
//merged code  
var gblCutacctWithdrawalWidgetDate =[];
var gblCutacctWithdrawalArray =[];
var mode ="";


// Used in UUX Amount Field Currency Data Mapping
var gblCurrencyArray = [{currencycode: "ARS", locale: "es", currencySymbol: "$", symbolPosition: "L" },
                        {currencycode:"AUD", locale: "en", currencySymbol: "$", symbolPosition: "L" },
                        {currencycode:"BRL", locale: "pt", currencySymbol: "R$", symbolPosition: "L" },
                        {currencycode:"CAD", locale: "en-CA", currencySymbol: "$", symbolPosition: "L" },
                        {currencycode:"CAD", locale: "fr-CA", currencySymbol: "$", symbolPosition: "R" },
                        {currencycode:"CLP", locale: "es-CL", currencySymbol: "$", symbolPosition: "L", decimals: "0" },
                        {currencycode:"CNY", locale: "zh-CN", currencySymbol: "¥", symbolPosition: "L" },
                        {currencycode:"COP", locale: "en", currencySymbol: "$", symbolPosition: "L" },
                        {currencycode:"CZK", locale: "cs", currencySymbol: "Kč", symbolPosition: "R" },
                        {currencycode:"DKK", locale: "da", currencySymbol: "kr.", symbolPosition: "L" },
                        {currencycode:"EUR", locale: "da", currencySymbol: "€", symbolPosition: "L" },
                        {currencycode:"HKD", locale: "en-HK", currencySymbol: "HK$", symbolPosition: "L" },
                        {currencycode:"HUF", locale: "hu", currencySymbol: "Ft", symbolPosition: "R" },
                        {currencycode:"INR", locale: "en-IN", currencySymbol: "₹", symbolPosition: "L" },
                        {currencycode:"ILS", locale: "he-IL", currencySymbol: "₪", symbolPosition: "L" },
                        {currencycode:"JPY", locale: "ja-JP", currencySymbol: "¥", symbolPosition: "L", decimals: "0" },
                        {currencycode:"KRW", locale: "ko", currencySymbol: "₩", symbolPosition: "L", decimals: "0" },
                        {currencycode:"MYR", locale: "ms-MY", currencySymbol: "RM", symbolPosition: "L" },
                        {currencycode:"MXN", locale: "es-MX", currencySymbol: "$", symbolPosition: "L" },
                        {currencycode:"MAD", locale: "ar-MA", currencySymbol: ".د.م.", symbolPosition: "R" },
                        {currencycode:"NZD", locale: "en-NZ", currencySymbol: "$", symbolPosition: "L" },
                        {currencycode:"NOK", locale: "nn-NO", currencySymbol: "kr.", symbolPosition: "L" },
                        {currencycode:"PHP", locale: "en-PH", currencySymbol: "₱", symbolPosition: "L" },
                        {currencycode:"PLN", locale: "pl-PL", currencySymbol: "zł", symbolPosition: "R" },
                        {currencycode:"RUB", locale: "ru-RU", currencySymbol: "p.", symbolPosition: "R" },
                        {currencycode:"SAR", locale: "ar", currencySymbol: "﷼", symbolPosition: "R" },
                        {currencycode:"SGD", locale: "en-SG", currencySymbol: "$", symbolPosition: "L" },
                        {currencycode:"ZAR", locale: "en-ZA", currencySymbol: "R", symbolPosition: "L" },
                        {currencycode:"SEK", locale: "sv-SE", currencySymbol: "kr", symbolPosition: "R" },
                        {currencycode:"CHF", locale: "gsw-CH", currencySymbol: "fr.", symbolPosition: "L" },
                        {currencycode:"TWD", locale: "zh-Hant-TW", currencySymbol: "元", symbolPosition: "L" },
                        {currencycode:"THB", locale: "th-TH", currencySymbol: "฿", symbolPosition: "R" },
                        {currencycode:"TRY", locale: "tr-TR", currencySymbol: "₺", symbolPosition: "R" },
                        {currencycode:"GBP", locale: "en-GB", currencySymbol: "£", symbolPosition: "L" },
                        {currencycode:"USD", locale: "en-US", currencySymbol: "$", symbolPosition: "L" },
                        {currencycode:"VND", locale: "vi", currencySymbol: "₫", symbolPosition: "R", decimals: "0" }];
